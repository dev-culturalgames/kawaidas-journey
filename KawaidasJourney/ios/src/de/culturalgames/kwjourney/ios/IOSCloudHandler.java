package de.culturalgames.kwjourney.ios;

import com.badlogic.gdx.utils.Array;

import de.culturalgames.kwjourney.save.KJCloudSaveHandler;
import de.culturalgames.kwjourney.util.KJTexts;
import de.golfgl.gdxgamesvcs.GameCenterClient;
import de.golfgl.gdxgamesvcs.IGameServiceClient.GameServiceFeature;
import de.golfgl.gdxgamesvcs.gamestate.IFetchGameStatesListResponseListener;
import de.golfgl.gdxgamesvcs.gamestate.ILoadGameStateResponseListener;
import de.golfgl.gdxgamesvcs.gamestate.ISaveGameStateResponseListener;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class IOSCloudHandler extends KJCloudSaveHandler {

    private IOSGameService gameService;

    private ILoadGameStateResponseListener loadListener = new ILoadGameStateResponseListener() {

        @Override
        public void gsGameStateLoaded(byte[] gameState) {
            game.getScreen().getMenu().showWaiting(false);

            loadGamestate(gameState);
        }
    };

    private ISaveGameStateResponseListener saveListener = new ISaveGameStateResponseListener() {

        @Override
        public void onGameStateSaved(boolean success, String errorCode) {
            game.getScreen().getMenu().showWaiting(false);

            if (success)
                savegameSaved();
            else {
                VJXLogger.log(LogCategory.ERROR, "IOS Cloud: Error saving game " + save.getName() + " - " + errorCode);
                showInfo("Error saving game\n" + save.getName() + " - " + errorCode);
            }

        }
    };

    public IOSCloudHandler(IOSGameService gameService) {
        this.gameService = gameService;
    }

    @Override
    public boolean isAvailable() {
        return false;
    }

    @Override
    public void showSavedGamesUI() {
        GameCenterClient client = gameService.getGameCenterClient();
        game.getScreen().getMenu().showWaiting(true);

        client.fetchGameStates(new IFetchGameStatesListResponseListener() {
            @Override
            public void onFetchGameStatesListResponse(Array<String> gameStates) {
                game.getScreen().getMenu().showWaiting(false);

                VJXLogger.log("Gamestates: ");
                if (gameStates == null) {
                    VJXLogger.log("null");
                    return;
                }
                if (gameStates.isEmpty()) {
                    VJXLogger.log("empty");
                    return;
                }
                for (String gameState : gameStates)
                    VJXLogger.log(gameState);
            }
        });
    }

    @Override
    public void deleteSave(final String saveName) {
        GameCenterClient client = gameService.getGameCenterClient();
        if (client.isFeatureSupported(GameServiceFeature.GameStateDelete)) {
            game.getScreen().getMenu().showWaiting(true);

            client.deleteGameState(saveName, new ISaveGameStateResponseListener() {
                @Override
                public void onGameStateSaved(boolean success, String errorCode) {
                    game.getScreen().getMenu().showWaiting(false);

                    if (success)
                        VJXLogger.log("Savegame deleted: " + saveName);
                    else VJXLogger.log(LogCategory.ERROR, "Could not delete savegame: " + saveName + " - " + errorCode);
                }
            });
        } else VJXLogger.log("Savegame deletion not supported.");
    }

    @Override
    protected boolean save() {
        GameCenterClient client = gameService.getGameCenterClient();
        if (client.isFeatureSupported(GameServiceFeature.GameStateStorage)) {
            game.getScreen().getMenu().showWaiting(true);

            VJXLogger.log(LogCategory.INFO, "IOS Cloud: Saving game " + save.getName());
            client.saveGameState(save.getName(),
                                 game.getSave().toBytes(),
                                 (long) (game.getPercentProgress() * 100),
                                 saveListener);
        } else showInfo(TextControl.getInstance().get(KJTexts.INFO_CLOUD_NOT_AVAILABLE_TITLE));
        return true;
    }

    @Override
    protected boolean load() {
        final GameCenterClient client = gameService.getGameCenterClient();
        if (client.isFeatureSupported(GameServiceFeature.GameStateStorage)) {
            game.getScreen().getMenu().showWaiting(true);

            VJXLogger.log(LogCategory.INFO, "IOS Cloud: Loading game " + save.getName());
            if (!client.fetchGameStates(new IFetchGameStatesListResponseListener() {

                @Override
                public void onFetchGameStatesListResponse(Array<String> gameStates) {
                    if (gameStates != null && gameStates.size > 0) {
                        VJXLogger.log(LogCategory.INFO, "IOS Cloud: Loading game " + save.getName());
                        client.loadGameState(save.getName(), loadListener);
                    } else {
                        VJXLogger.log(LogCategory.INFO, "IOS Cloud: No game data. Creating new...");
                        save();
                        savegameLoaded();
                        return;
                    }
                }
            })) {
                game.getScreen().getMenu().showWaiting(false);

                showError(TextControl.getInstance().get(KJTexts.INFO_FETCH_STATES_FAILED_TITLE));
            }
        } else {
            savegameLoaded();
            showInfo(TextControl.getInstance().get(KJTexts.INFO_CLOUD_NOT_AVAILABLE_TITLE));
        }
        return true;
    }
}