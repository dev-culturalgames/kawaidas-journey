package de.culturalgames.kwjourney.ios;

import java.util.ArrayList;

import org.robovm.apple.uikit.UIActivityViewController;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.apple.uikit.UIViewController;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;

import de.golfgl.gdxgamesvcs.GameCenterClient;
import de.golfgl.gdxgamesvcs.IGameServiceClient.GameServiceFeature;
import de.golfgl.gdxgamesvcs.IGameServiceListener;
import de.venjinx.ejx.util.EJXGameService;

public class IOSGameService extends EJXGameService
implements IGameServiceListener {

    private IOSApplication application;
    private GameCenterClient gsClient;

    public IOSGameService(IOSApplication application) {
        super();
        this.application = application;
    }

    @Override
    public void init() {
        gsClient = new GameCenterClient(application.getUIViewController());
        gsClient.setListener(this);
    }

    public GameCenterClient getGameCenterClient() {
        return gsClient;
    }

    @Override
    public void gsOnSessionActive() {
        onConnected();
    }

    @Override
    public void gsOnSessionInactive() {
        onDisconnected();
    }

    @Override
    public void gsShowErrorToUser(GsErrorType et, String msg, Throwable t) {
        //        gpsHandler.showError(t.getMessage() + " (" + et + "): " + msg);
    }

    @Override
    protected void connect() {
        game.getScreen().getMenu().showWaiting(true);

        gsClient.logIn();
    }

    @Override
    public void onConnected() {
        game.getScreen().getMenu().showWaiting(false);

        setConnected(true);
    }

    @Override
    protected void disconnect() {
        if (gsClient.isFeatureSupported(GameServiceFeature.PlayerLogOut)) {
            game.getScreen().getMenu().showWaiting(true);

            gsClient.logOff();
        }
    }

    @Override
    public void onDisconnected() {
        game.getScreen().getMenu().showWaiting(false);
        setConnected(false);
    }

    @Override
    public void share() {
        ArrayList<String> list = new ArrayList<>();
        list.add("https://www.kawaidasjourney.de/");

        UIViewController viewController = UIApplication.getSharedApplication()
                        .getKeyWindow().getRootViewController();


        UIActivityViewController activityController = new UIActivityViewController(
                        list, null);

        viewController.presentViewController(activityController, true, null);
    }
}