package de.culturalgames.kwjourney.ios.ads;

import java.util.Collections;
import java.util.HashMap;

import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.api.AdMobController;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.apple.uikit.UIViewController;
import org.robovm.pods.google.mobileads.GADMobileAds;
import org.robovm.pods.google.mobileads.GADRequest;
import org.robovm.pods.google.mobileads.GADRewardBasedVideoAd;

public class IOSAdController extends AdMobController {

    public static final String IOS_TEST_AD_ID = "ca-app-pub-3940256099942544/1712485313";
    public static final String IOS_CONTINUE_AD_ID = "ca-app-pub-1115107980762897/7305348910";
    public static final String IOS_DOUBLE_SUNS_AD_ID = "ca-app-pub-1115107980762897/7249508598";

    private UIViewController viewController;
    private final GADRequest adRequest;

    private final HashMap<String, GADRewardBasedVideoAd> rewardAds;
    private final HashMap<String, String> namesToAdIds;

    public IOSAdController(KJGame game) {
        super(game);
        namesToAdIds = new HashMap<>();
        namesToAdIds.put(KJString.AD_test, IOS_TEST_AD_ID);
        namesToAdIds.put(KJString.AD_continue, IOS_CONTINUE_AD_ID);
        namesToAdIds.put(KJString.AD_double_suns, IOS_DOUBLE_SUNS_AD_ID);

        adRequest = new GADRequest();
        if (game.isDevMode()) {
            namesToAdIds.put(KJString.AD_continue, IOS_TEST_AD_ID);
            namesToAdIds.put(KJString.AD_double_suns, IOS_TEST_AD_ID);
            adRequest.setTestDevices(Collections.singletonList(GADRequest.getSimulatorID()));
        }

        rewardAds = new HashMap<>();
        GADRewardBasedVideoAd rewardedAd = new GADRewardBasedVideoAd();
        rewardedAd.setDelegate(new TestAdListener(ejxListener, this));
        rewardAds.put(KJString.AD_test, rewardedAd);

        rewardedAd = new GADRewardBasedVideoAd();
        rewardedAd.setDelegate(new ContinueAdListener(ejxListener, this));
        rewardAds.put(KJString.AD_continue, rewardedAd);

        rewardedAd = new GADRewardBasedVideoAd();
        rewardedAd.setDelegate(new DoubleSunsAdListener(ejxListener, this));
        rewardAds.put(KJString.AD_double_suns, rewardedAd);

        GADMobileAds.disableSDKCrashReporting();
    }

    @Override
    public void loadAd(final String adName) {
        rewarded = false;

        if (!namesToAdIds.containsKey(adName)) return;
        if (isLoaded(adName) || currentLoadingAd != null &&
                                currentLoadingAd.equals(adName)) return;

        currentLoadingAd = adName;
        currentAd = null;

        GADRewardBasedVideoAd rewardedAd = rewardAds.get(adName);
        VJXLogger.log("Load ad: " + adName);
        String adId = namesToAdIds.get(adName);
        rewardedAd.loadRequest(adRequest, adId);
    }

    @Override
    public void showAd(String adName) {
        if (!namesToAdIds.containsKey(adName)) return;

        rewarded = false;

        GADRewardBasedVideoAd rewardedAd = rewardAds.get(adName);
        if (!rewardedAd.isReady()) {
            VJXLogger.log(LogCategory.ERROR, "Ad not loaded: " + adName);
            return;
        }

        if (viewController == null)
            viewController = UIApplication.getSharedApplication().getKeyWindow()
            .getRootViewController();

        VJXLogger.log("Show ad: " + adName);
        rewardedAd.present(viewController);
    }
}