package de.culturalgames.kwjourney.ios.ads;

import org.robovm.apple.foundation.NSObject;
import org.robovm.pods.google.mobileads.GADRewardBasedVideoAdDelegate;

import de.culturalgames.kwjourney.api.EJXRewardVideoAdListener;

public abstract class KJAdIOSListener extends NSObject
                implements GADRewardBasedVideoAdDelegate {

    protected EJXRewardVideoAdListener listener;
    protected IOSAdController controller;

    public KJAdIOSListener(EJXRewardVideoAdListener listener, IOSAdController controller) {
        this.listener = listener;
        this.controller = controller;
    }

    public abstract String getAdId();

    public abstract String getAdName();

}