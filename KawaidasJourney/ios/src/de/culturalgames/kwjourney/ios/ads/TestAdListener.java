package de.culturalgames.kwjourney.ios.ads;

import org.robovm.apple.foundation.NSError;
import org.robovm.pods.google.mobileads.GADAdReward;
import org.robovm.pods.google.mobileads.GADRewardBasedVideoAd;

import de.culturalgames.kwjourney.api.EJXRewardVideoAdListener;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class TestAdListener extends KJAdIOSListener {

    public static final String IOS_TEST_AD_ID = "ca-app-pub-3940256099942544/1712485313";

    public TestAdListener(EJXRewardVideoAdListener listener, IOSAdController controller) {
        super(listener, controller);
    }

    @Override
    public String getAdId() {
        return IOS_TEST_AD_ID;
    }

    @Override
    public String getAdName() {
        return KJString.AD_test;
    }

    @Override
    public void didStartPlaying(GADRewardBasedVideoAd rewardedAd) {
        listener.onRewardedVideoStarted(getAdName(), getAdId(), controller);
    }

    @Override
    public void didOpen(GADRewardBasedVideoAd rewardedAd) {
        listener.onRewardedVideoAdOpened(getAdName(), getAdId(), controller);
    }

    @Override
    public void didReceiveAd(GADRewardBasedVideoAd rewardedAd) {
        listener.onRewardedVideoAdLoaded(getAdName(), getAdId(), controller);
    }

    @Override
    public void willLeaveApplication(GADRewardBasedVideoAd rewardedAd) {
        listener.onRewardedVideoAdLeftApplication(getAdName(), getAdId(),
                        controller);
    }

    @Override
    public void didFailToLoad(GADRewardBasedVideoAd rewardedAd, NSError error) {
        listener.onRewardedVideoAdFailedToLoad(getAdName(), getAdId(),
                        error.getCode(), controller);
        VJXLogger.log(LogCategory.ERROR, "Error domain: " + error.getDomain());
    }

    @Override
    public void didClose(GADRewardBasedVideoAd rewardedAd) {
        listener.onRewardedVideoAdClosed(getAdName(), getAdId(), controller);
    }

    @Override
    public void didRewardUser(GADRewardBasedVideoAd rewardedAd,
                    GADAdReward rewardItem) {
        listener.onRewarded(getAdName(), getAdId(), rewardItem.getType(),
                        rewardItem.getAmount().intValue(), controller);
    }

    @Override
    public void rewardBasedVideoAdDidCompletePlaying(
                    GADRewardBasedVideoAd rewardedAd) {
        listener.onRewardedVideoCompleted(getAdName(), getAdId(), controller);
    }
}