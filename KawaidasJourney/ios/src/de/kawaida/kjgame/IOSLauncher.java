package de.kawaida.kjgame;

import de.culturalgames.kwjourney.ios.IOSCloudHandler;
import de.culturalgames.kwjourney.ios.IOSGameService;
import org.robovm.apple.avfoundation.AVAudioSession;
import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.foundation.NSErrorException;
import org.robovm.apple.uikit.UIApplication;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;
import com.badlogic.gdx.pay.ios.apple.PurchaseManageriOSApple;

import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.ios.ads.IOSAdController;

public class IOSLauncher extends IOSApplication.Delegate {

    // ./gradlew -Probovm.device.name=iphone8 launchIPhoneSimulator
    // ./gradlew -Probovm.arch=arm64 launchIPhoneSimulator

    @Override
    protected IOSApplication createApplication() {
        IOSApplicationConfiguration config = new IOSApplicationConfiguration();
        config.orientationLandscape = true;
        config.orientationPortrait = false;
        config.allowIpod = true;
        try {
            AVAudioSession.getSharedInstance().setPreferredIOBufferDuration(1);
        } catch (NSErrorException e) {
            e.printStackTrace();
        }

        String[] arg = null;

        if (arg == null || arg.length == 0)
            arg = new String[0];

        KJGame game = new KJGame(arg);
//        game.setDevMode(true);
        IOSApplication app = new IOSApplication(game, config);

        IOSGameService gameService = new IOSGameService(app);
        IOSCloudHandler cloudHandler = new IOSCloudHandler(gameService);
        PurchaseManageriOSApple pManager = new PurchaseManageriOSApple();
        game.setGameService(gameService);
        game.setCloudHandler(cloudHandler);
        game.setPurchaseManager(pManager);

        new IOSAdController(game);

        return app;
    }

    public static void main(String[] argv) {
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(argv, null, IOSLauncher.class);
        pool.close();
    }
}