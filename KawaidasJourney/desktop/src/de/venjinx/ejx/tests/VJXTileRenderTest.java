package de.venjinx.ejx.tests;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class VJXTileRenderTest {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration lwjglCfg = new LwjglApplicationConfiguration();

        lwjglCfg.title = "Tile Render Test";
        lwjglCfg.width = 1280;
        lwjglCfg.height = 720;

        Game g = new Game() {
            private OrthogonalTiledMapRenderer levelRenderer;
            private FitViewport view;
            private OrthographicCamera cam;
            private SpriteBatch batch;
            private ShapeRenderer shapeRenderer;

            private Vector2 position = new Vector2(256, 256);
            private SpriteDrawable sprite;

            @Override
            public void create() {
                cam = new OrthographicCamera();
                cam.position.x = Gdx.graphics.getWidth() / 2f;
                cam.position.y = Gdx.graphics.getHeight() / 2f;
                view = new FitViewport(Gdx.graphics.getWidth(),
                                Gdx.graphics.getHeight(), cam);
                view.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
                view.apply();

                TmxMapLoader loader = new TmxMapLoader();
                TiledMap map;
                // load test map here
                map = loader.load("tests/testMap.tmx");

                AssetManager assets = new AssetManager();
                // load assets here
                // assets.load("<pathToTexture>", Texture.class);
                assets.finishLoading();


                // Create map renderer
                levelRenderer = new OrthogonalTiledMapRenderer(map);
                batch = (SpriteBatch) levelRenderer.getBatch();

                shapeRenderer = new ShapeRenderer();
                shapeRenderer.setAutoShapeType(true);
            }

            @Override
            public void render() {
                Gdx.gl20.glClearColor(0, 0, 0, 0);
                Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
                if (Gdx.input.isKeyPressed(Keys.W)) {
                    cam.position.y += 10f;
                }

                if (Gdx.input.isKeyPressed(Keys.A)) {
                    cam.position.x -= 10f;
                }

                if (Gdx.input.isKeyPressed(Keys.S)) {
                    cam.position.y -= 10f;
                }

                if (Gdx.input.isKeyPressed(Keys.D)) {
                    cam.position.x += 10f;
                }

                view.apply();
                levelRenderer.setView(cam);
                levelRenderer.render();

                float w = sprite.getSprite().getWidth();
                float h = sprite.getSprite().getHeight();
                batch.begin();
                sprite.draw(batch, position.x, position.y, w, h);
                batch.end();
            }
        };

        new LwjglApplication(g, lwjglCfg);
    }
}