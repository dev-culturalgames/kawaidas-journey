package de.venjinx.ejx;

import de.culturalgames.kwjourney.save.KJCloudSaveHandler;

public class DummyCloudHandler extends KJCloudSaveHandler {

    public DummyCloudHandler() {
        super();
    }

    @Override
    public boolean isAvailable() {
        return false;
    }

    @Override
    protected boolean save() {
        game.getScreen().getMenu().showWaiting(false);
        savegameSaved();
        return true;
    }

    @Override
    public boolean load() {
        game.getScreen().getMenu().showWaiting(false);
        savegameLoaded();
        return true;
    }

    @Override
    public void showSavedGamesUI() {
    }

    @Override
    public void deleteSave(String saveName) {
    }
}