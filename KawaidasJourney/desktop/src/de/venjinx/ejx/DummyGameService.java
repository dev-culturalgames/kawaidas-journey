package de.venjinx.ejx;

import de.venjinx.ejx.util.EJXGameService;

public class DummyGameService extends EJXGameService {

    @Override
    protected void connect() {
        onConnected();
    }

    @Override
    public void onConnected() {
        game.getScreen().getMenu().showWaiting(false);
        setConnected(true);
    }

    @Override
    protected void disconnect() {
        onDisconnected();
    }

    @Override
    public void onDisconnected() {
        game.getScreen().getMenu().showWaiting(false);
        setConnected(false);
    }

    @Override
    public void share() {
    }
}