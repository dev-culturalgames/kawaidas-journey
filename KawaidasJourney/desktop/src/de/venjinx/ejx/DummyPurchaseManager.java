package de.venjinx.ejx;

import java.util.HashMap;

import com.badlogic.gdx.pay.Information;
import com.badlogic.gdx.pay.Offer;
import com.badlogic.gdx.pay.PurchaseManager;
import com.badlogic.gdx.pay.PurchaseManagerConfig;
import com.badlogic.gdx.pay.PurchaseObserver;
import com.badlogic.gdx.pay.Transaction;

import de.culturalgames.kwjourney.api.APILoader;
import de.culturalgames.kwjourney.api.VJXOffer;

public class DummyPurchaseManager implements PurchaseManager {

    private boolean available = true;
    private PurchaseObserver observer;
    private boolean installed = false;

    private HashMap<String, Offer> offers;

    @Override
    public String storeName() {
        return "DummyPurchaseManager";
    }

    @Override
    public void install(PurchaseObserver observer, PurchaseManagerConfig config,
                    boolean autoFetchInformation) {
        this.observer = observer;
        if (offers == null) offers = new HashMap<>();

        Offer offer;
        VJXOffer vjxOffer;
        int count = config.getOfferCount();
        for (int i = 0; i < count; i++) {
            offer = config.getOffer(i);
            if (offer instanceof VJXOffer) {
                vjxOffer = (VJXOffer) offer;

                if (vjxOffer.isPremium()) {
                    offers.put(vjxOffer.getIdentifier(), vjxOffer);
                }
            } else {
                offers.put(offer.getIdentifier(), offer);
            }
        }
        if (available) {
            installed = true;
            observer.handleInstall();
        } else observer.handleInstallError(
                        new Throwable("Error installing purchase manager - Not available"));
    }

    @Override
    public boolean installed() {
        return installed;
    }

    @Override
    public void dispose() {
        installed = false;
    }

    @Override
    public void purchase(String identifier) {
        if (!installed) {
            observer.handlePurchaseCanceled();
            return;
        }

        Offer offer = offers.get(identifier);
        //        ShopItem item = ShopItem.get(identifier);
        //        final VJXOffer offer = APILoader.getOffer(item.name, true);
        //        if (offer == null || !offer.isPremium()) {
        //            Throwable t = new Throwable(
        //                            "Offer " + identifier + " not found.");
        //            observer.handlePurchaseError(t);
        //            return;
        //        }

        if (offer == null) {
            Throwable t = new Throwable("Offer " + identifier + " not found.");
            observer.handlePurchaseError(t);
            return;
        }
        Transaction transaction = new Transaction();
        transaction.setIdentifier(identifier);
        observer.handlePurchase(transaction);
    }

    @Override
    public void purchaseRestore() {
        Transaction[] transactions = new Transaction[0];
        observer.handleRestore(transactions);
    }

    @Override
    public Information getInformation(String identifier) {
        VJXOffer vjxOffer = getVJXOffer(identifier);
        if (vjxOffer == null) { return Information.UNAVAILABLE; }

        Information information = new Information(identifier,
                        vjxOffer.getText(), vjxOffer.getStringPrice() + " €");
        return information;
    }

    private VJXOffer getVJXOffer(String identifier) {
        Offer offer = offers.get(identifier);
        if (offer == null) { return null; }

        VJXOffer vjxOffer;
        if (!(offer instanceof VJXOffer)) {
            vjxOffer = APILoader.getOffer(identifier, true);
        } else {
            vjxOffer = (VJXOffer) offer;
        }

        if (vjxOffer == null) { return null; }

        if (!vjxOffer.isAvailable()) { return null; }
        return vjxOffer;
    }
}