package de.venjinx.ejx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

import de.venjinx.ejx.util.VJXGraphix;
import de.venjinx.ejx.util.VJXLogger;

public class EJXTexturePacker {

    public static void packTextures(String input, String output) {
        Settings settings = new Settings();
        settings.filterMin = TextureFilter.Nearest;
        settings.filterMag = TextureFilter.Nearest;
        settings.maxWidth = 2304;
        settings.maxHeight = 2304;
        settings.combineSubdirectories = true;
        settings.duplicatePadding = true;
        settings.pot = false;

        settings.maxWidth = 2048;
        settings.maxHeight = 2048;
        settings.pot = true;

        settings.premultiplyAlpha = true;
        settings.stripWhitespaceX = true;
        settings.stripWhitespaceY = true;
        settings.scale = new float[] { .5f };

        packTextures(input, output, settings);
    }

    public static void packTextures(String input, String output, Settings settings) {
        input = EJXGame.getAssetsPath(true) + input;
        output = EJXGame.getAssetsPath(false) + output;

        if (input.isEmpty()) return;
        VJXLogger.log("Start texture packing...");

        TexturePacker.processIfModified(settings, input + "global/character/",
                        output + "global/", "global_character");

        TexturePacker.processIfModified(settings, input + "global/decoration/",
                        output + "global/", "global_decoration");

        TexturePacker.processIfModified(settings, input + "global/how/",
                        output + "global/", "global_how");

        TexturePacker.processIfModified(settings, input + "horizon/decoration/",
                        output + "horizon/", "horizon_decoration");

        TexturePacker.processIfModified(settings,
                        input + "foreground/decoration/",
                        output + "foreground/", "foreground_decoration");

        //        TexturePacker.processIfModified(settings,
        //                        input + "global/item/", output + "global/",
        //                        "global_item");

        TexturePacker.processIfModified(settings, input + "global/vehicle/",
                        output + "global/", "global_vehicle");

        settings.scale = new float[] { 1 };
        settings.filterMin = TextureFilter.Linear;
        settings.filterMag = TextureFilter.Linear;

        TexturePacker.processIfModified(settings, input + "shop/character/",
                        output + "shop/", "shop_character");

        TexturePacker.processIfModified(settings, input + "global/extra/",
                        output + "global/", "global_portraits");

        settings.premultiplyAlpha = false;

        TexturePacker.processIfModified(settings, input + "global/ui/",
                        output + "global", "global_ui");

        TexturePacker.processIfModified(settings, input + "global/hud/",
                        output + "global", "global_hud");

        //        convertTileSets();
        VJXLogger.log("Texture packing finished.");
    }

    public static void convertTileSets() {
        String tileSets[] = new String[] { "collision_tiles256.png",
                        //                        "fg_beach_forest_ser.png","fg_dar_day_night.png",
                        //                        "fg_st.png", "hr_beach_st_ser.png", "hr_dar_day.png",
                        //                        "hr_dar_night.png", "sky.png",
                        //                        "w_beach_forest.png",
                        //                        "w_dar_day.png",
                        //                        "w_dar_night.png",
                        //                        "w_ser.png",
                        //                        "w_st.png"
        };

        int scl = 1;
        // convert none padded tilesets
        //        String path = "D:/Development/Projekte/Kawaida/Dev/kawaidas-journey/"
        //                        + "KawaidasJourney/android/assets/tiles/new/";
        //        String path;

        String path = "D:/Development/Projekte/Kawaida/newAssets/tiles/128/";
        for (String s : tileSets) {
            VJXGraphix.padTilesInImage(Gdx.files.absolute(path + s), 128 * scl,
                            128 * scl, 2 * scl, 2 * scl, true, "processed", "",
                            false);
        }

        path = "D:/Development/Projekte/Kawaida/newAssets/tiles/256/";
        for (String s : tileSets) {
            VJXGraphix.padTilesInImage(Gdx.files.absolute(path + s), 256 * scl,
                            256 * scl, 4 * scl, 4 * scl, true, "processed", "",
                            false);
        }
        //
        //        // scale down tilesets
        //        path = "D:/Development/Projekte/Kawaida/Dev/kawaidas-journey/"
        //                        + "KawaidasJourney/android/rawAsets/tiles/";
        //        for (String s : tileSets) {
        //            VJXGraphix.padTilesInImage(Gdx.files.absolute(path + s), 256, 256,
        //                            2, 2, true, "processed", "", false);
        //        }
    }
}