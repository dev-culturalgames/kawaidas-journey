package de.kawaida.kjgame.desktop;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import de.culturalgames.kwjourney.KJGame;
import de.venjinx.ejx.DummyCloudHandler;
import de.venjinx.ejx.DummyGameService;
import de.venjinx.ejx.DummyPurchaseManager;
import de.venjinx.ejx.EJXTexturePacker;

public class DevLauncher {
    public static void main(String[] arg) {
        KJGame game = new KJGame(arg);
        game.setDevMode(true);
        game.setGameService(new DummyGameService());
        game.setCloudHandler(new DummyCloudHandler());
        game.setPurchaseManager(new DummyPurchaseManager());

        LwjglApplicationConfiguration lwjglCfg = new LwjglApplicationConfiguration();
        lwjglCfg.width = 1280;
        lwjglCfg.height = 720;
        lwjglCfg.addIcon("favIcon32x32.png", FileType.Internal);

        // Start Game
        new LwjglApplication(game, lwjglCfg);
        EJXTexturePacker.packTextures("sprites/", "spriteSheets/");
    }
}