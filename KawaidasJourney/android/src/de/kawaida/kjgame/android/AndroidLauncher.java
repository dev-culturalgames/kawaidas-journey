package de.kawaida.kjgame.android;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import com.android.billingclient.api.Purchase;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.pay.android.googlebilling.PurchaseManagerGoogleBilling;
import com.google.android.gms.ads.AdView;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.android.AndroidActivityHandler;
import de.culturalgames.kwjourney.android.AndroidCloudHandler;
import de.culturalgames.kwjourney.android.AndroidGameService;
import de.culturalgames.kwjourney.android.ads.AndroidAdController;
import de.venjinx.ejx.util.VJXLogger;

public class AndroidLauncher extends AndroidApplication {

    private KJGame game;
    private AndroidActivityHandler activityHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //gradlew generateEclipseDependencies
        //gradlew android:assembleDebug
        //cd Development\Projekte\Kawaida\Dev\kawaidas-journey\KawaidasJourney\
        //gradlew android:installDebug android:run --stacktrace
        //gradlew android:installRelease android:run -Dorg.gradle.java.home="C:/Program Files/Java/jdk1.8.0_172/"
        //keytool -exportcert -alias androiddebugkey -keystore C:/Users/KawaidaDev/.android/debug.keystore -list -v
        // -Dorg.gradle.java.home="C:/Program Files/Java/jdk1.8.0_172/"

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

        config.useAccelerometer = false;
        config.useCompass = false;
        config.useImmersiveMode = true;
        config.useGyroscope = false;
        config.numSamples = 2;

        String[] arg = null;

        if (arg == null || arg.length == 0) {
            //            arg = new String[1];
            //            arg[0] = "kizimKazi/level/kzk_lvl_1.tmx";
            //            arg[0] = "kizim_kazi/level/kzk_Aufbau_test_#02.tmx";
        }

        game = new KJGame(arg);
//        game.setDevMode(true);

        activityHandler = new AndroidActivityHandler(this);
        activityHandler.setGame(game);

        AndroidGameService gameService = new AndroidGameService(this);
        AndroidCloudHandler cloudHandler = new AndroidCloudHandler(this);
        PurchaseManagerGoogleBilling pManager = new PurchaseManagerGoogleBilling(this) {
            @Override
            public void onPurchasesUpdated(int responseCode,
                            List<Purchase> purchases) {
                super.onPurchasesUpdated(responseCode, purchases);
                VJXLogger.log("Purchases updated: " + responseCode);
                if (purchases == null) return;
                for (Purchase purchase : purchases) {
                    VJXLogger.log(purchase.getPackageName() + ": "
                                    + purchase.getSku() + ",  "
                                    + purchase.getPurchaseTime() + ", "
                                    + purchase.isAutoRenewing());
                }
            }
        };
        game.setGameService(gameService);
        game.setCloudHandler(cloudHandler);
        game.setPurchaseManager(pManager);

        initialize(game, config);

        AndroidAdController adControl = new AndroidAdController(this, game);
        //        MobileAds.initialize(this, "ca-app-pub-1115107980762897~2559261023");

        RelativeLayout layout = new RelativeLayout(this);
        layout.setContentDescription("The main layout for the game.");
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.MATCH_PARENT);
        layout.setLayoutParams(params);

        View gameView = initializeForView(game, config);
        gameView.setContentDescription("The game view that displays all game content.");
        gameView.setLayoutParams(params);
        layout.addView(gameView);

        for (AdView adView : adControl.getAdViews()) {
            layout.addView(adView);
        }

        setContentView(layout);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        activityHandler.handleActivitResult(requestCode, resultCode, data);
    }
}