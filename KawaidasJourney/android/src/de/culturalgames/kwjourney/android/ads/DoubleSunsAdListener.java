package de.culturalgames.kwjourney.android.ads;

import com.google.android.gms.ads.reward.RewardItem;

import de.culturalgames.kwjourney.api.EJXRewardVideoAdListener;
import de.culturalgames.kwjourney.util.KJTypes.KJString;

public class DoubleSunsAdListener extends KJAdAndroidListener {

    public static final String ANDROID_DOUBLE_SUNS_AD_ID = "ca-app-pub-1115107980762897/3513250045";

    public DoubleSunsAdListener(EJXRewardVideoAdListener listener, AndroidAdController controller) {
        super(listener, controller);
    }

    @Override
    public String getAdId() {
        if (controller.getGame().isDevMode())
            return TestAdListener.ANDROID_TEST_AD_ID;
        return ANDROID_DOUBLE_SUNS_AD_ID;
    }

    @Override
    public String getAdName() {
        return KJString.AD_double_suns;
    }

    @Override
    public void onRewardedVideoStarted() {
        listener.onRewardedVideoStarted(getAdName(), getAdId(), controller);
    }

    @Override
    public void onRewardedVideoAdOpened() {
        listener.onRewardedVideoAdOpened(getAdName(), getAdId(), controller);
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        listener.onRewardedVideoAdLoaded(getAdName(), getAdId(), controller);
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        listener.onRewardedVideoAdLeftApplication(getAdName(), getAdId(), controller);
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int arg0) {
        listener.onRewardedVideoAdFailedToLoad(getAdName(), getAdId(), arg0, controller);
    }

    @Override
    public void onRewardedVideoAdClosed() {
        listener.onRewardedVideoAdClosed(getAdName(), getAdId(), controller);
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        listener.onRewarded(getAdName(), getAdId(), rewardItem.getType(), rewardItem.getAmount(), controller);
    }

    @Override
    public void onRewardedVideoCompleted() {
        listener.onRewardedVideoCompleted(getAdName(), getAdId(), controller);
    }
}