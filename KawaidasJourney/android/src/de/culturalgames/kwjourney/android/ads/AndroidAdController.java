package de.culturalgames.kwjourney.android.ads;

import java.util.Collection;
import java.util.HashMap;

import android.view.View;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.api.AdMobController;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class AndroidAdController extends AdMobController {

    public static final String ANDROID_TEST_AD_ID = "ca-app-pub-3940256099942544/5224354917";
    public static final String ANDROID_CONTINUE_AD_ID = "ca-app-pub-1115107980762897/4048630186";
    public static final String ANDROID_DOUBLE_SUNS_AD_ID = "ca-app-pub-1115107980762897/3513250045";

    private final AndroidApplication app;
    private final AdRequest.Builder requestBuilder;

    private final HashMap<String, String> namesToAdIds;
    private final HashMap<String, KJAdAndroidListener> rewardListeners;

    private final RewardedVideoAd rewardedAd;
    private final HashMap<String, AdView> adViews;

    public AndroidAdController(AndroidApplication app, KJGame game) {
        super(game);
        this.app = app;

        namesToAdIds = new HashMap<>();
        namesToAdIds.put(KJString.AD_test, ANDROID_TEST_AD_ID);
        namesToAdIds.put(KJString.AD_continue, ANDROID_CONTINUE_AD_ID);
        namesToAdIds.put(KJString.AD_double_suns, ANDROID_DOUBLE_SUNS_AD_ID);

        adViews = new HashMap<>();
        rewardListeners = new HashMap<>();

        requestBuilder = new AdRequest.Builder();
        if (game.isDevMode()) {
            namesToAdIds.put(KJString.AD_continue, ANDROID_TEST_AD_ID);
            namesToAdIds.put(KJString.AD_double_suns, ANDROID_TEST_AD_ID);
            requestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        }

        AdView view = new AdView(app);
        view.setContentDescription("This ad view displays an ad for reward: Test.");
        view.setVisibility(View.INVISIBLE);
        view.setAdUnitId(namesToAdIds.get(KJString.AD_test));
        adViews.put(KJString.AD_test, view);

        view = new AdView(app);
        view.setContentDescription("This ad view displays an ad for reward: Continue.");
        view.setVisibility(View.INVISIBLE);
        view.setAdUnitId(namesToAdIds.get(KJString.AD_continue));
        adViews.put(KJString.AD_continue, view);

        view = new AdView(app);
        view.setContentDescription("This ad view displays an ad for reward: Double suns.");
        view.setVisibility(View.INVISIBLE);
        view.setAdUnitId(namesToAdIds.get(KJString.AD_double_suns));
        adViews.put(KJString.AD_double_suns, view);

        rewardListeners.put(KJString.AD_test,
                        new TestAdListener(ejxListener, this));
        rewardListeners.put(KJString.AD_continue,
                        new ContinueAdListener(ejxListener, this));
        rewardListeners.put(KJString.AD_double_suns,
                        new DoubleSunsAdListener(ejxListener, this));

        rewardedAd = MobileAds.getRewardedVideoAdInstance(app);
    }

    @Override
    public void loadAd(final String adName) {
        rewarded = false;

        if (!rewardListeners.containsKey(adName)) return;
        if (isLoaded(adName) || currentLoadingAd != null &&
                                currentLoadingAd.equals(adName)) return;

        currentLoadingAd = adName;
        currentAd = null;

        app.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (rewardedAd.isLoaded()) {
                    VJXLogger.log("Destroy ad: " + rewardedAd);
                    rewardedAd.destroy(adViews.get(adName).getContext());
                }

                VJXLogger.log("Load ad: " + adName);
                KJAdAndroidListener listener = rewardListeners.get(adName);
                rewardedAd.setRewardedVideoAdListener(listener);

                String adID = listener.getAdId();
                rewardedAd.loadAd(adID, requestBuilder.build());
            }
        });
    }

    @Override
    public void showAd(final String adName) {
        if (!namesToAdIds.containsKey(adName)) return;

        rewarded = false;
        app.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!rewardedAd.isLoaded()) {
                    VJXLogger.log(LogCategory.ERROR, "Ad not loaded: " + adName);
                    return;
                }

                VJXLogger.log("Show ad: " + adName);
                adViews.get(adName).setVisibility(View.VISIBLE);
                rewardedAd.show();
            }
        });
    }

    public Collection<AdView> getAdViews() {
        return adViews.values();
    }
}