package de.culturalgames.kwjourney.android.ads;

import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import de.culturalgames.kwjourney.api.EJXRewardVideoAdListener;

public abstract class KJAdAndroidListener implements RewardedVideoAdListener {

    protected EJXRewardVideoAdListener listener;
    protected AndroidAdController controller;

    public KJAdAndroidListener(EJXRewardVideoAdListener listener, AndroidAdController controller) {
        this.listener = listener;
        this.controller = controller;
    }

    public abstract String getAdId();

    public abstract String getAdName();

}