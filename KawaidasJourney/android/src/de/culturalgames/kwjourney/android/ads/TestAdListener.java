package de.culturalgames.kwjourney.android.ads;

import com.google.android.gms.ads.reward.RewardItem;

import de.culturalgames.kwjourney.api.EJXRewardVideoAdListener;
import de.culturalgames.kwjourney.util.KJTypes.KJString;

public class TestAdListener extends KJAdAndroidListener {

    public static final String ANDROID_TEST_AD_ID = "ca-app-pub-3940256099942544/5224354917";

    public TestAdListener(EJXRewardVideoAdListener listener, AndroidAdController controller) {
        super(listener, controller);
    }

    @Override
    public String getAdId() {
        return ANDROID_TEST_AD_ID;
    }

    @Override
    public String getAdName() {
        return KJString.AD_test;
    }

    @Override
    public void onRewardedVideoStarted() {
        listener.onRewardedVideoStarted(getAdName(), getAdId(), controller);
    }

    @Override
    public void onRewardedVideoAdOpened() {
        listener.onRewardedVideoAdOpened(getAdName(), getAdId(), controller);
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        listener.onRewardedVideoAdLoaded(getAdName(), getAdId(), controller);
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        listener.onRewardedVideoAdLeftApplication(getAdName(), getAdId(), controller);
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int arg0) {
        listener.onRewardedVideoAdFailedToLoad(getAdName(), getAdId(), arg0, controller);
    }

    @Override
    public void onRewardedVideoAdClosed() {
        listener.onRewardedVideoAdClosed(getAdName(), getAdId(), controller);
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        listener.onRewarded(getAdName(), getAdId(), rewardItem.getType(),
                        rewardItem.getAmount(), controller);
    }

    @Override
    public void onRewardedVideoCompleted() {
        listener.onRewardedVideoCompleted(getAdName(), getAdId(), controller);
    }
}