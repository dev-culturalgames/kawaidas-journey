package de.culturalgames.kwjourney.android;

import java.io.IOException;

import com.google.android.gms.games.SnapshotsClient;
import com.google.android.gms.games.SnapshotsClient.DataOrConflict;
import com.google.android.gms.games.SnapshotsClient.SnapshotConflict;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import android.app.Activity;
import android.content.Intent;
import de.culturalgames.kwjourney.KJGame.GameStats;
import de.culturalgames.kwjourney.save.KJCloudSaveHandler;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class AndroidCloudHandler extends KJCloudSaveHandler {

    private static final String ANDROID_CLOUD = "Android cloud - ";

    private static final String KJ_CLOUD_SAVED_GAMES_TITLE = "Kawaida's Journey savegames";
    private static final String ANDROID_CONFLICT_SNAPSHOT = ANDROID_CLOUD + "Conflict while fetching snapshot: ";

    private static final String ANDROID_LOADING_SUCCESSFUL = ANDROID_CLOUD + "Loading cloud save successful.";
    private static final String ANDROID_COMMIT_SUCCESSFUL = ANDROID_CLOUD + "Commit cloud save successful.";

    private static final String ANDROID_NO_DATA_FOR_SNAPSHOT = ANDROID_CLOUD + "No data for snapshot: ";
    private static final String ANDROID_SAVING_DATA = ANDROID_CLOUD + "Saving data...";
    private static final String ANDROID_LOADING_DATA = ANDROID_CLOUD + "Loading data...";
    private static final String ANDROID_RESOLVING_DATA = ANDROID_CLOUD + "Resolving data...";
    private static final String ANDROID_LOAD_CONFLICTING_DATA = ANDROID_CLOUD + "Conflicting data received while loading.";
    private static final String ANDROID_SAVE_CONFLICTING_DATA = ANDROID_CLOUD + "Conflicting data received while saving.";
    private static final String ANDROID_RESOLVE_CONFLICTING_DATA = ANDROID_CLOUD + "Conflicting data received while resolving. ";
    private static final String ANDROID_RESOLVE_LOAD_CONFLICT_USE_LOCAL = ANDROID_CLOUD + "Resolve load conflict using local";
    private static final String ANDROID_RESOLVE_LOAD_CONFLICT_USE_CLOUD = ANDROID_CLOUD + "Resolve load conflict using cloud";

    private static final int MAX_NUMBER_SAVED_GAMES = 1;

    private Activity activity;
    private AndroidGameService androidGameService;

    private OnSuccessListener<SnapshotMetadata> commitListener = new OnSuccessListener<SnapshotMetadata>() {

        @Override
        public void onSuccess(SnapshotMetadata metadata) {
            VJXLogger.log(ANDROID_COMMIT_SUCCESSFUL);
            //            printMetaData(metadata);
            game.getScreen().getMenu().showWaiting(false);
            savegameSaved();
        }
    };

    private OnSuccessListener<DataOrConflict<Snapshot>> deleteListener = new OnSuccessListener<SnapshotsClient.DataOrConflict<Snapshot>>() {

        @Override
        public void onSuccess(DataOrConflict<Snapshot> arg0) {
            if (arg0.isConflict()) {

            } else {
                SnapshotsClient snapshotClient = androidGameService.getSnapshotClient();
                Task<String> getSnapshotTask = snapshotClient
                                .delete(arg0.getData().getMetadata());
                getSnapshotTask.addOnSuccessListener(new OnSuccessListener<String>() {
                    @Override
                    public void onSuccess(String arg0) {
                        VJXLogger.log("delete success " + arg0);
                        game.getScreen().getMenu().showWaiting(false);
                    }
                });
            }
        }
    };

    private OnSuccessListener<DataOrConflict<Snapshot>> resolveListener = new OnSuccessListener<SnapshotsClient.DataOrConflict<Snapshot>>() {
        @Override
        public void onSuccess(DataOrConflict<Snapshot> data) {
            if (data.isConflict()) {
                VJXLogger.log(ANDROID_RESOLVE_CONFLICTING_DATA);
                printData(data);
                game.getScreen().getMenu().showWaiting(false);
                //                writeSnapshot(data.getConflict().getSnapshot());
            } else {
                VJXLogger.log(ANDROID_RESOLVING_DATA);
                //                printData(data);
                writeSnapshot(data.getData());
            }
        }
    };

    private OnSuccessListener<DataOrConflict<Snapshot>> loadListener = new OnSuccessListener<DataOrConflict<Snapshot>>() {
        @Override
        public void onSuccess(final DataOrConflict<Snapshot> data) {
            if (data.isConflict()) {
                VJXLogger.log(LogCategory.ERROR, ANDROID_CONFLICT_SNAPSHOT + save.getName());
                VJXLogger.log(ANDROID_LOAD_CONFLICTING_DATA);
                printData(data);
                resolveConflict(data.getConflict());
            } else {
                VJXLogger.log(ANDROID_LOADING_DATA);
                //                printData(data);
                game.getScreen().getMenu().showWaiting(false);
                loadSnapshotContent(data.getData().getSnapshotContents());
            }
        }
    };

    private OnSuccessListener<DataOrConflict<Snapshot>> saveListener = new OnSuccessListener<DataOrConflict<Snapshot>>() {
        @Override
        public void onSuccess(final DataOrConflict<Snapshot> data) {
            if (data.isConflict()) {
                VJXLogger.log(LogCategory.ERROR, ANDROID_CONFLICT_SNAPSHOT + save.getName());
                VJXLogger.log(ANDROID_SAVE_CONFLICTING_DATA);
                printData(data);
                resolveConflict(data.getConflict());
            } else {
                VJXLogger.log(ANDROID_SAVING_DATA);
                //                printData(data);
                writeSnapshot(data.getData());
            }
        }
    };

    private void resolveConflict(final SnapshotConflict conflict) {
        final String conflictID = conflict.getConflictId();
        final SnapshotMetadataChange metadata = getMetadataChange();
        Runnable useLocal = new Runnable() {

            @Override
            public void run() {
                game.getScreen().getMenu().showWaiting(true);
                VJXLogger.log(ANDROID_RESOLVE_LOAD_CONFLICT_USE_LOCAL);
                Snapshot snapshot = conflict.getConflictingSnapshot();
                String snapshotID = snapshot.getMetadata().getSnapshotId();
                SnapshotsClient snapshotClient = androidGameService
                                .getSnapshotClient();
                Task<DataOrConflict<Snapshot>> resolveTask = snapshotClient
                                .resolveConflict(conflictID, snapshotID,
                                                metadata,
                                                snapshot.getSnapshotContents());
                resolveTask.addOnSuccessListener(resolveListener);
            }
        };

        Runnable useCloud = new Runnable() {

            @Override
            public void run() {
                game.getScreen().getMenu().showWaiting(true);
                VJXLogger.log(ANDROID_RESOLVE_LOAD_CONFLICT_USE_CLOUD);
                Snapshot snapshot = conflict.getSnapshot();
                String snapshotID = snapshot.getMetadata().getSnapshotId();
                SnapshotsClient snapshotClient = androidGameService
                                .getSnapshotClient();
                Task<DataOrConflict<Snapshot>> resolveTask = snapshotClient
                                .resolveConflict(conflictID, snapshotID,
                                                metadata,
                                                snapshot.getSnapshotContents());
                resolveTask.addOnSuccessListener(resolveListener);
            }
        };
        solveConflictingSaveData(useLocal, useCloud);
    }

    public AndroidCloudHandler(Activity activity) {
        super();
        this.activity = activity;
    }

    @Override
    public void setGame(EJXGame game) {
        super.setGame(game);
        androidGameService = (AndroidGameService) gameService;
    }

    @Override
    protected boolean save() {
        getSnapshot(saveListener);
        return true;
    }

    @Override
    public boolean load() {
        getSnapshot(loadListener);
        return true;
    }

    @Override
    public void deleteSave(String saveName) {
        getSnapshot(deleteListener);
    }

    @Override
    public void showSavedGamesUI() {
        game.getScreen().getMenu().showWaiting(true);

        SnapshotsClient snapshotClient = androidGameService.getSnapshotClient();
        Task<Intent> intentTask = snapshotClient.getSelectSnapshotIntent(
                        KJ_CLOUD_SAVED_GAMES_TITLE, true, true,
                        MAX_NUMBER_SAVED_GAMES);

        intentTask.addOnSuccessListener(new OnSuccessListener<Intent>() {
            @Override
            public void onSuccess(Intent intent) {
                activity.startActivityForResult(intent, AndroidActivityHandler.RC_SAVED_GAMES);
            }
        });
    }

    @Override
    public boolean isAvailable() {
        return true;
    }

    private void getSnapshot(
                    OnSuccessListener<DataOrConflict<Snapshot>> onSuccessListener) {
        game.getScreen().getMenu().showWaiting(true);

        SnapshotsClient snapshotClient = androidGameService.getSnapshotClient();
        Task<DataOrConflict<Snapshot>> getSnapshotTask = snapshotClient
                        .open(game.getSave().getName(), true,
                                        SnapshotsClient.RESOLUTION_POLICY_MOST_RECENTLY_MODIFIED);
        getSnapshotTask.addOnSuccessListener(onSuccessListener);
    }

    private void writeSnapshot(Snapshot snapshot) {
        SnapshotMetadataChange metadata = getMetadataChange();
        SnapshotContents contents = snapshot.getSnapshotContents();
        contents.writeBytes(game.getSave().toBytes());
        SnapshotsClient snapshotClient = androidGameService.getSnapshotClient();
        Task<SnapshotMetadata> resolveTask = snapshotClient.commitAndClose(snapshot, metadata);
        resolveTask.addOnSuccessListener(commitListener);
    }

    private void loadSnapshotContent(final SnapshotContents content) {
        loadGamestate(readData(content));
        VJXLogger.log(ANDROID_LOADING_SUCCESSFUL);
    }

    private SnapshotMetadataChange getMetadataChange() {
        SnapshotMetadataChange.Builder mDataBuilder;
        mDataBuilder = new SnapshotMetadataChange.Builder();
        mDataBuilder.setDescription(game.getSave().getName());
        mDataBuilder.setProgressValue(game.getProgress());

        long timePlayed = game.getTimePlayed();
        mDataBuilder.setPlayedTimeMillis(timePlayed);
        game.getSave().putLong(GameStats.TIME_PLAYED.name, timePlayed);
        return mDataBuilder.build();
    }

    private byte[] readData(SnapshotContents snapshotContent) {
        try {
            byte[] data = snapshotContent.readFully();
            return data;
        } catch (IOException e) {
            VJXLogger.log(LogCategory.ERROR, ANDROID_NO_DATA_FOR_SNAPSHOT
                            + game.getSave().getName());
        }
        return null;
    }

    private void printData(DataOrConflict<Snapshot> data) {
        if (data == null) return;
        if (data.isConflict()) {
            SnapshotConflict conflict = data.getConflict();

            VJXLogger.log(ANDROID_CLOUD + "Conflict id:"
                            + conflict.getConflictId() + ", conflict "
                            + conflict);

            VJXLogger.log("Server snapshot:");
            Snapshot conflictSnapshot = conflict.getSnapshot();
            printMetaData(conflictSnapshot.getMetadata());
            printSnapshotContents(conflictSnapshot.getSnapshotContents());

            VJXLogger.log("Modified snapshot:");
            Snapshot conflictingSnapshot = conflict.getConflictingSnapshot();
            printMetaData(conflictSnapshot.getMetadata());
            printSnapshotContents(conflictingSnapshot.getSnapshotContents());

            VJXLogger.log("Resolution data:");
            SnapshotContents resolutionSnapshotContents = conflict
                            .getResolutionSnapshotContents();
            printSnapshotContents(resolutionSnapshotContents);
        } else {
            VJXLogger.log("Snapshot:");
            Snapshot snapshot = data.getData();
            printMetaData(snapshot.getMetadata());
            printSnapshotContents(snapshot.getSnapshotContents());
        }
        VJXLogger.log("----------------------------------------");
    }

    private void printMetaData(SnapshotMetadata metadata) {
        if (metadata == null) return;
        VJXLogger.log(ANDROID_CLOUD + "Metadata snapshot id: "
                        + metadata.getSnapshotId());
        VJXLogger.log(ANDROID_CLOUD + "Metadata device: "
                        + metadata.getDeviceName());
        VJXLogger.log(ANDROID_CLOUD + "Metadata title: " + metadata.getTitle()
        + ", unique name: " + metadata.getUniqueName());
        VJXLogger.log(ANDROID_CLOUD + "Metadata time: "
                        + metadata.getPlayedTime() + ", progress: "
                        + metadata.getProgressValue());
        VJXLogger.log(ANDROID_CLOUD + "Metadata descr: "
                        + metadata.getDescription());
    }

    private void printSnapshotContents(SnapshotContents snapshotContents) {
        if (snapshotContents == null) return;
        byte[] snapshotData = readData(snapshotContents);
        if (snapshotData != null) {
            String jsonData = new String(snapshotData);
            VJXLogger.log(ANDROID_CLOUD + "Snapshot data: " + jsonData);
        }
    }
}