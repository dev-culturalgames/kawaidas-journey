package de.culturalgames.kwjourney.android;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.SnapshotsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import android.app.Activity;
import android.content.Intent;
import de.culturalgames.kwjourney.android.AndroidActivityHandler;
import de.venjinx.ejx.util.EJXGameService;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class AndroidGameService extends EJXGameService {

    private static final String ANDROID_GPS_DISCONNECT_FAILED = "GooglePlayService - Disconnect failed: ";

    private Activity activity;

    // Sign in stuff
    private GoogleSignInClient signInClient;
    private GoogleSignInAccount account;

    // Save stuff
    private SnapshotsClient snapshotClient;

    public AndroidGameService(Activity activity) {
        this.activity = activity;

        GoogleSignInOptions.Builder optionsBuilder;
        optionsBuilder = new GoogleSignInOptions.Builder(
                        GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);

        // Since we are using SavedGames, we need to add the SCOPE_APPFOLDER
        // to access Google Drive.
        GoogleSignInOptions options = optionsBuilder
                        .requestScopes(Drive.SCOPE_APPFOLDER).requestEmail()
                        .requestProfile().build();
        signInClient = GoogleSignIn.getClient(activity, options);
    }

    @Override
    protected void connect() {
        game.getScreen().getMenu().showWaiting(true);
        if (!isAutoConnect()) {
            activity.startActivityForResult(signInClient.getSignInIntent(),
                                            AndroidActivityHandler.RC_SIGN_IN);
            return;
        }

        signInClient.silentSignIn().addOnCompleteListener(activity,
                        new OnCompleteListener<GoogleSignInAccount>() {
            @Override
            public void onComplete(Task<GoogleSignInAccount> task) {
                game.getScreen().getMenu().showWaiting(false);
                if (task.isSuccessful()) onConnected();
                else onDisconnected();
            }
        });
    }

    @Override
    public void onConnected() {
        GoogleSignInAccount account = GoogleSignIn
                        .getLastSignedInAccount(activity);
        if (this.account != account) {
            this.account = account;
            onAccountChanged(account);
        }

        snapshotClient = Games.getSnapshotsClient(activity, account);
        setConnected(true);
    }

    @Override
    protected void disconnect() {
        game.getScreen().getMenu().showWaiting(true);
        signInClient.signOut().addOnCompleteListener(activity,
                        new OnCompleteListener<Void>() {
            @Override
            public void onComplete(Task<Void> task) {
                game.getScreen().getMenu().showWaiting(false);
                if (task.isSuccessful()) onDisconnected();
                else VJXLogger.log(LogCategory.INFO,
                                ANDROID_GPS_DISCONNECT_FAILED
                                + game.getSave().getName());
            }
        });
    }

    @Override
    public void onDisconnected() {
        setConnected(false);
        account = null;
    }

    public SnapshotsClient getSnapshotClient() {
        return snapshotClient;
    }

    @Override
    public void share() {
        Intent myIntent = new Intent(Intent.ACTION_SEND);
        myIntent.setType("text/plain");
        String body = "https://www.kawaidasjourney.de/";
        String sub = "Kawaida's Journey";
        myIntent.putExtra(Intent.EXTRA_SUBJECT, sub);
        myIntent.putExtra(Intent.EXTRA_TEXT, body);
        activity.startActivity(Intent.createChooser(myIntent, "Share Using"));
    }

    private void onAccountChanged(GoogleSignInAccount account) {
        VJXLogger.log("GameService account changed: ");
        VJXLogger.log("    name  : " + account.getDisplayName());
        VJXLogger.log("    family: " + account.getFamilyName());
        VJXLogger.log("    nick  : " + account.getGivenName());
        VJXLogger.log("    mail  : " + account.getEmail());
    }
}