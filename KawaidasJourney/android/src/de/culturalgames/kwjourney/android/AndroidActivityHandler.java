package de.culturalgames.kwjourney.android;

import com.google.android.gms.common.api.Status;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.KJScreen;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class AndroidActivityHandler {

    // request codes
    public static final int RC_SIGN_IN = 9001;
    public static final int RC_SAVED_GAMES = 9009;
    //    private static final int RC_LIST_SAVED_GAMES = 9002;
    //    private static final int RC_SELECT_SNAPSHOT = 9003;
    //    private static final int RC_SAVE_SNAPSHOT = 9004;
    //    private static final int RC_LOAD_SNAPSHOT = 9005;

    private static final String SIGN_IN_STATUS = "googleSignInStatus";

    private static final String ANDROID_GPS_INTENT_NULL = "GooglePlayService - Intent is null: ";
    private static final String ANDROID_GPS_REQUEST_CODE = "GooglePlayService - Request code: ";
    private static final String ANDROID_GPS_RESULT_CODE = "GooglePlayService - Result code: ";
    private static final String ANDROID_GPS_INTENT = "GooglePlayService - Intent: ";
    private static final String ANDROID_GPS_TRY_SOLVE_STATUS = "GooglePlayService - Trying to solve status: ";
    private static final String ANDROID_GPS_RESOLUTION_FAILED = "GooglePlayService - Starting resolution of status failed: ";
    private static final String ANDROID_GPS_NO_RESOLUTION = "GooglePlayService - Status has no resolution: ";
    private static final String ANDROID_GPS_ACTIVITY_RESULT = "GooglePlayService - Activity result: ";
    private static final String ANDROID_GPS_ON_ACTTIVITY_RESULT_SAVED_GAMES = "GooglePlayService - onActivityResult RC_SAVED_GAMES ";

    private Activity activity;
    private EJXGame game;

    public AndroidActivityHandler(Activity activity) {
        this.activity = activity;
    }

    public void setGame(EJXGame game) {
        this.game = game;
    }

    public void handleActivitResult(int requestCode, int resultCode, Intent intent) {
        Bundle extrasBundle;
        if (!checkIntent(intent, requestCode, resultCode)) return;
        extrasBundle = intent.getExtras();
        for (String key : extrasBundle.keySet())
            VJXLogger.log(LogCategory.INFO, "    " + key + ": " + extrasBundle.get(key));

        switch (requestCode) {
            case RC_SIGN_IN:
                if (intent.hasExtra(SIGN_IN_STATUS)) {
                    tryResolve((Status) extrasBundle.get(SIGN_IN_STATUS));
                    return;
                }
                game.getScreen().getMenu().showWaiting(false);

                if (game == null) break;
                if (game.getGameService() == null) break;

                game.getGameService().onConnected();
                break;
            case RC_SAVED_GAMES:
                game.getScreen().getMenu().showWaiting(false);
                VJXLogger.log(ANDROID_GPS_ON_ACTTIVITY_RESULT_SAVED_GAMES);
                break;
            default:
        }
    }

    private void tryResolve(Status status) {
        VJXLogger.log(LogCategory.INFO, ANDROID_GPS_TRY_SOLVE_STATUS + status);
        if (status != null) {
            KJMenu menu = game.getScreen().getMenu(KJMenu.class);
            TextControl texts = TextControl.getInstance();
            if (status.hasResolution()) {
                try {
                    status.startResolutionForResult(activity,
                                    status.getStatusCode());
                } catch (SendIntentException e) {
                    VJXLogger.log(LogCategory.INFO,
                                    ANDROID_GPS_RESOLUTION_FAILED + status);
                    menu.showWaiting(false);
                    menu.showInfo(texts.get(KJTexts.INFO_RESOLUTION_FAILED_TITLE),
                                    texts.get(KJTexts.INFO_RESOLUTION_FAILED_TEXT));
                }
            } else {
                VJXLogger.log(LogCategory.INFO,
                                ANDROID_GPS_NO_RESOLUTION + status);
                menu.showWaiting(false);
                menu.showInfo(texts.get(KJTexts.INFO_NO_INET_TITLE),
                                texts.get(KJTexts.INFO_NO_INET_TEXT));

                if (menu.getCurrentMenuId() == MENU.LOADING.id) {
                    ((KJScreen) game.getScreen()).onMenuLoaded();
                }
            }
        }
    }

    private boolean checkIntent(Intent intent, int requestCode, int resultCode) {
        if (intent == null) {
            VJXLogger.log(LogCategory.ERROR, ANDROID_GPS_ACTIVITY_RESULT
                            + game.getSave().getName());
            VJXLogger.log(LogCategory.ERROR, ANDROID_GPS_INTENT_NULL + intent);
            VJXLogger.log(LogCategory.ERROR, ANDROID_GPS_REQUEST_CODE + requestCode);
            VJXLogger.log(LogCategory.ERROR, ANDROID_GPS_RESULT_CODE + resultCode);
            return false;
        }

        VJXLogger.log(LogCategory.INFO, ANDROID_GPS_ACTIVITY_RESULT + game.getSave().getName());
        VJXLogger.log(LogCategory.INFO, ANDROID_GPS_INTENT + intent);
        VJXLogger.log(LogCategory.INFO, ANDROID_GPS_REQUEST_CODE + requestCode);
        VJXLogger.log(LogCategory.INFO, ANDROID_GPS_RESULT_CODE + resultCode);
        return true;
    }
}