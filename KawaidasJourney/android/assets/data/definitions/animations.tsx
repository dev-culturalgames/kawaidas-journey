<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="animations" tilewidth="2688" tileheight="1792" tilecount="95" columns="0">
 <grid orientation="orthogonal" width="650" height="488"/>
 <properties>
  <property name="type" value="spine"/>
 </properties>
 <tile id="0">
  <properties>
   <property name="sfx:climb_down" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_climbUp.mp3"/>
   <property name="sfx:climb_run_down" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_climb.mp3"/>
   <property name="sfx:climb_run_up" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_climb.mp3"/>
   <property name="sfx:climb_up" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_climbUp.mp3"/>
   <property name="sfx:damage" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_damage.mp3"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_die_1.mp3"/>
   <property name="sfx:jump_down" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_jump_down.mp3"/>
   <property name="sfx:land" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_land.mp3"/>
   <property name="sfx:pickUp" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_pickUp.mp3"/>
   <property name="sfx:plant" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_plant.mp3"/>
   <property name="sfx:rise" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_jump.mp3"/>
   <property name="sfx:run" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_run.mp3"/>
   <property name="sfx:squirt" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_squirt.mp3"/>
   <property name="sfx:throw" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_throw.mp3"/>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/vervetmonkey_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/vervetmonkey_2.mp3"/>
   <property name="sfx:voice2" type="file" value="../../sfx/voices/vervetmonkey_3.mp3"/>
   <property name="sfx:voice3" type="file" value="../../sfx/voices/vervetmonkey_4.mp3"/>
   <property name="sfx:walk" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetmonkey0_walk.mp3"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/kawaida0.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="sfx:trigger" type="file" value="../../sfx/objects/character/trap0/trap0_trigger.mp3"/>
  </properties>
  <image width="340" height="56" source="../../../rawAssets/objects/character/trap0/trap0_icon.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="sfx:block" type="file" value="../../sfx/objects/character/crab0/crab0_block.mp3"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/character/crab0/crab0_die.mp3"/>
   <property name="sfx:walk" type="file" value="../../sfx/objects/character/crab0/crab0_walk.mp3"/>
  </properties>
  <image width="512" height="256" source="../../../rawAssets/objects/character/crab0/crab0_icon.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="sfx:attacking" type="file" value="../../sfx/objects/character/tigerMosquito0/tigerMosquito0_attack.mp3"/>
   <property name="sfx:attacking:vol" type="float" value="0.69999999999999996"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/character/tigerMosquito0/tigerMosquito0_die.mp3"/>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/tigerMosquito0/tigerMosquito0_idle.mp3"/>
   <property name="sfx:idle:count" type="int" value="4"/>
  </properties>
  <image width="256" height="200" source="../../../rawAssets/objects/character/tigerMosquito0/tigerMosquito0_icon.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="sfx:damage" type="file" value="../../sfx/objects/character/bulldozer0/bulldozer0_damage.mp3"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/character/bulldozer0/bulldozer0_die.mp3"/>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/bulldozer0/bulldozer0_idle.mp3"/>
   <property name="sfx:run" type="file" value="../../sfx/objects/character/bulldozer0/bulldozer0_accelerate_1.mp3"/>
   <property name="sfx:run:vol" type="float" value="0.69999999999999996"/>
   <property name="sfx:startAttack" type="file" value="../../sfx/objects/character/bulldozer0/bulldozer0_attack.mp3"/>
   <property name="sfx:startAttack:vol" type="float" value="0.69999999999999996"/>
   <property name="sfx:walk" type="file" value="../../sfx/objects/character/bulldozer0/bulldozer0_walk_2.mp3"/>
   <property name="sfx:walk:vol" type="float" value="0.29999999999999999"/>
  </properties>
  <image width="768" height="640" source="../../../rawAssets/objects/character/bulldozer0/steamGhost0_icon.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="sfx:appear" type="file" value="../../sfx/objects/character/leopard0/leopard_spawn.mp3"/>
   <property name="sfx:appear:vol" type="float" value="0.5"/>
   <property name="sfx:disappear" type="file" value="../../sfx/objects/character/leopard0/leopard_despawn.mp3"/>
   <property name="sfx:disappear:vol" type="float" value="0.5"/>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/leopard_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/leopard_2.mp3"/>
  </properties>
  <image width="384" height="384" source="../../../rawAssets/objects/character/leopard0/leopard0_icon.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/banana0/banana0_one_collected.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/banana0/banana0_one_icon.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/coconut0/coconut0_one_collected.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/coconut0/coconut0_one_icon.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/sun0/sun0_small_collect.mp3"/>
   <property name="sfx:collected:vol" type="float" value="0.69999999999999996"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/sun0/sun0_small_icon.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/totem0/totem0_collected.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/totem0/totem0_icon.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="sfx:grow_HP1" type="file" value="../../sfx/objects/decoration/palmSeedling0/palmSeedling0_grown.mp3"/>
   <property name="sfx:grow_HP1_end" type="file" value="../../sfx/objects/decoration/palmSeedling0/palmSeedling0_grow_end.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/palmSeedling0/palmSeedling0_icon.png"/>
 </tile>
 <tile id="12">
  <properties>
   <property name="sfx:damage" type="file" value="../../sfx/objects/decoration/treeHouse0/treehouse0_damage.mp3"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/decoration/treeHouse0/treehouse0_die.mp3"/>
  </properties>
  <image width="2688" height="1792" source="../../../rawAssets/objects/decoration/treeHouse0/treeHouse0_icon.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="sfx:fly_bonus" type="file" value="../../sfx/objects/character/beamCloud0/beamCloud0_fly.mp3"/>
   <property name="sfx:fly_normal" type="file" value=""/>
  </properties>
  <image width="512" height="256" source="../../../rawAssets/objects/character/beamCloud0/beamCloud0_icon.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/banana0/banana0_bunch_collected.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/banana0/banana0_bunch_icon.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/coconut0/coconut0_bunch_collected.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/coconut0/coconut0_bunch_icon.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/sun0/sun0_big_collected.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/sun0/sun0_big_icon.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="sfx:active" type="file" value="../../sfx/objects/item/karafuu0/karafuu0_active.mp3"/>
   <property name="sfx:active:loop" type="bool" value="true"/>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/karafuu0/karafuu0_collected.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/karafuu0/karafuu0_icon.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="sfx:active" type="file" value="../../sfx/objects/item/solarPanel0/solarPanel0_active.mp3"/>
   <property name="sfx:active:loop" type="bool" value="true"/>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/solarPanel0/solarPanel0_collected.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/solarPanel0/solarPanel0_icon.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="sfx:onScreen" type="file" value="../../sfx/objects/decoration/bumperSign0/bumperSign0_onScreen.mp3"/>
  </properties>
  <image width="128" height="512" source="../../../rawAssets/objects/decoration/bumperSign0.png"/>
 </tile>
 <tile id="20">
  <properties>
   <property name="sfx:grow_HP1" type="file" value="../../sfx/objects/character/fire0/fire_grow.mp3"/>
   <property name="sfx:grow_HP2" type="file" value="../../sfx/objects/character/fire0/fire_grow.mp3"/>
   <property name="sfx:grow_HP3" type="file" value="../../sfx/objects/character/fire0/fire_grow.mp3"/>
   <property name="sfx:grow_HP4" type="file" value="../../sfx/objects/character/fire0/fire_grow.mp3"/>
   <property name="sfx:grow_HP5" type="file" value="../../sfx/objects/character/fire0/fire_grow.mp3"/>
   <property name="sfx:idle_HP1" type="file" value="../../sfx/objects/character/fire0/fire_grow.mp3"/>
   <property name="sfx:idle_HP1:vol" type="float" value="0.5"/>
   <property name="sfx:idle_HP2" type="file" value="../../sfx/objects/character/fire0/fire_grow.mp3"/>
   <property name="sfx:idle_HP2:vol" type="float" value="0.5"/>
   <property name="sfx:idle_HP3" type="file" value="../../sfx/objects/character/fire0/fire_grow.mp3"/>
   <property name="sfx:idle_HP3:vol" type="float" value="0.5"/>
   <property name="sfx:idle_HP4" type="file" value="../../sfx/objects/character/fire0/fire_grow.mp3"/>
   <property name="sfx:idle_HP4:vol" type="float" value="0.5"/>
   <property name="sfx:idle_HP5" type="file" value="../../sfx/objects/character/fire0/fire_grow.mp3"/>
   <property name="sfx:idle_HP5:vol" type="float" value="0.5"/>
  </properties>
  <image width="512" height="384" source="../../../rawAssets/objects/character/fire0/fire0_icon.png"/>
 </tile>
 <tile id="21">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/electricSpark0/electricSpark0_small.mp3"/>
   <property name="sfx:idle:count" type="int" value="5"/>
   <property name="sfx:idle:loop" type="bool" value="false"/>
   <property name="sfx:idle:vol" type="float" value="0.20000000000000001"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/character/electricSpark0/electricSpark0_small.png"/>
 </tile>
 <tile id="22">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/electricSpark0/electricSpark0_big.mp3"/>
   <property name="sfx:idle:count" type="int" value="5"/>
   <property name="sfx:idle:loop" type="bool" value="false"/>
   <property name="sfx:idle:vol" type="float" value="0.20000000000000001"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/character/electricSpark0/electricSpark0_big.png"/>
 </tile>
 <tile id="23">
  <properties>
   <property name="sfx:throw" type="file" value="../../sfx/objects/character/colobus0_evil/colobus0_evil.mp3"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/colobus0_evil.png"/>
 </tile>
 <tile id="24">
  <properties>
   <property name="sfx:damage" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetMonkey0_evil_damage.mp3"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetMonkey0_evil_die.mp3"/>
   <property name="sfx:run" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetMonkey0_evil_run.mp3"/>
   <property name="sfx:throw" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetMonkey0_evil_throw.mp3"/>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/vervetmonkey_evil_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/vervetmonkey_evil_2.mp3"/>
   <property name="sfx:walk" type="file" value="../../sfx/objects/character/vervetMonkey0/vervetMonkey0_walk.mp3"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/drifter0.png"/>
 </tile>
 <tile id="25">
  <properties>
   <property name="sfx:damage" type="file" value="../../sfx/objects/character/baboon0/baboon0_damage.mp3"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/character/baboon0/baboon0_die.mp3"/>
   <property name="sfx:run" type="file" value="../../sfx/objects/character/baboon0/baboon0_run.mp3"/>
   <property name="sfx:throw" type="file" value="../../sfx/objects/character/baboon0/baboon0_throw.mp3"/>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/baboon_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/baboon_2.mp3"/>
   <property name="sfx:walk" type="file" value="../../sfx/objects/character/baboon0/baboon0_walk.mp3"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/baboon0.png"/>
 </tile>
 <tile id="26">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/level/exclamation_spawned.mp3"/>
  </properties>
  <image width="259" height="386" source="../../../rawAssets/objects/item/secret0/secret0_icon.png"/>
 </tile>
 <tile id="27">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/transporter0/transporter0.ogg"/>
   <property name="sfx:idle:vol" type="float" value="0.5"/>
  </properties>
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_blue0.png"/>
 </tile>
 <tile id="28">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/motorbike1/motorbike1.mp3"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/vehicle/motorbike1/bajaji0_blue0.png"/>
 </tile>
 <tile id="29">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/motorbike0/motorbike0.mp3"/>
  </properties>
  <image width="512" height="384" source="../../../rawAssets/objects/vehicle/motorbike0/pikiPiki0.png"/>
 </tile>
 <tile id="30">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/bus0/bus0.mp3"/>
  </properties>
  <image width="1024" height="384" source="../../../rawAssets/objects/vehicle/bus0/omnibus0_red0.png"/>
 </tile>
 <tile id="31">
  <properties>
   <property name="sfx:attack" type="file" value="../../sfx/objects/character/marabu0/marabu0_attack.mp3"/>
   <property name="sfx:damage" type="file" value="../../sfx/objects/character/marabu0/marabu0_damage.mp3"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/character/marabu0/marabu0_die.mp3"/>
   <property name="sfx:flying" type="file" value="../../sfx/objects/character/marabu0/marabu0_flying.mp3"/>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/marabu_talk.mp3"/>
  </properties>
  <image width="1024" height="1024" source="../../../rawAssets/objects/character/marabu0/marabu0.png"/>
 </tile>
 <tile id="32">
  <properties>
   <property name="sfx:attack" type="file" value="../../sfx/objects/character/trashchicken0/trashchicken_attack.mp3"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/character/trashchicken0/trashchicken_die.mp3"/>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/trashchicken0/trashchicken_idle.mp3"/>
  </properties>
  <image width="419" height="418" source="../../../rawAssets/objects/character/chicken0/chicken0_evil0.png"/>
 </tile>
 <tile id="33">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/chicken0/chicken0_brown0_idle.mp3"/>
   <property name="sfx:idle:vol" type="float" value="0.5"/>
  </properties>
  <image width="419" height="418" source="../../../rawAssets/objects/character/chicken0/chicken0_brown0.png"/>
 </tile>
 <tile id="34">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/chicken0/chicken0_white0_idle.mp3"/>
   <property name="sfx:idle:vol" type="float" value="0.5"/>
  </properties>
  <image width="419" height="418" source="../../../rawAssets/objects/character/chicken0/chicken0_white0.png"/>
 </tile>
 <tile id="35">
  <properties>
   <property name="sfx:throw" type="file" value="../../sfx/objects/character/hippo0/hippo0_collected.mp3"/>
  </properties>
  <image width="384" height="320" source="../../../rawAssets/objects/character/hippo0/hippo0_icon.png"/>
 </tile>
 <tile id="36">
  <properties>
   <property name="sfx:attack" type="file" value="../../sfx/objects/character/ant0/ant0_attack.mp3"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/character/ant0/ant0_die.mp3"/>
   <property name="sfx:walk" type="file" value="../../sfx/objects/character/ant0/ant0_walk.mp3"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/character/ant0/siafu0.png"/>
 </tile>
 <tile id="37">
  <properties>
   <property name="sfx:attack" type="file" value="../../sfx/objects/character/crow0/crow0_attack.mp3"/>
   <property name="sfx:die" type="file" value="../../sfx/objects/character/crow0/crow0_die.mp3"/>
   <property name="sfx:fly" type="file" value="../../sfx/objects/character/crow0/crow0_flying.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/character/crow0/crow0_icon.png"/>
 </tile>
 <tile id="38">
  <properties>
   <property name="sfx:fly" type="file" value=""/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/character/plasticBag0/plasticBag0_icon.png"/>
 </tile>
 <tile id="39">
  <properties>
   <property name="sfx:walk_noSkirt" type="file" value="../../sfx/objects/character/ragdoll0/ragdoll0_walk.mp3"/>
   <property name="sfx:walk_skirt" type="file" value="../../sfx/objects/character/ragdoll0/ragdoll0_walk.mp3"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/character/ragdoll0/ragdoll0_icon.png"/>
 </tile>
 <tile id="40">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/colobussi_talk.mp3"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/colobussi0/colobussi0_icon.png"/>
 </tile>
 <tile id="41">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/karim_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/karim_2.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/karim0.png"/>
 </tile>
 <tile id="42">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/lady0_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lady0.png"/>
 </tile>
 <tile id="43">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/lady1_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lady1.png"/>
 </tile>
 <tile id="44">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/lady2_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lady2.png"/>
 </tile>
 <tile id="45">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/Elephant_voice_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/Elephant_voice_2.mp3"/>
   <property name="sfx:walk" type="file" value="../../sfx/objects/character/elephant0/elephant0_run.mp3"/>
  </properties>
  <image width="1344" height="896" source="../../../rawAssets/objects/character/elephant0/elephant0_gray0.png"/>
 </tile>
 <tile id="46">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/hyena_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/hyena_2.mp3"/>
  </properties>
  <image width="320" height="320" source="../../../rawAssets/objects/character/hyena0/hyena0.png"/>
 </tile>
 <tile id="47">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/motorbike1/motorbike1.mp3"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/vehicle/motorbike1/bajaji0_green0.png"/>
 </tile>
 <tile id="48">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/motorbike1/motorbike1.mp3"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/vehicle/motorbike1/bajaji0_red0.png"/>
 </tile>
 <tile id="49">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/transporter0/transporter0.ogg"/>
   <property name="sfx:idle:vol" type="float" value="0.5"/>
  </properties>
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_blue1.png"/>
 </tile>
 <tile id="50">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/transporter0/transporter0.ogg"/>
   <property name="sfx:idle:vol" type="float" value="0.5"/>
  </properties>
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_red0.png"/>
 </tile>
 <tile id="51">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/transporter0/transporter0.ogg"/>
   <property name="sfx:idle:vol" type="float" value="0.5"/>
  </properties>
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_red1.png"/>
 </tile>
 <tile id="52">
  <properties>
   <property name="sfx:idle" type="file" value="../../sfx/objects/character/bus0/bus0.mp3"/>
  </properties>
  <image width="1024" height="384" source="../../../rawAssets/objects/vehicle/bus0/omnibus0_blue0.png"/>
 </tile>
 <tile id="53">
  <properties>
   <property name="sfx:walk" type="file" value="../../sfx/objects/character/elephant0/elephant0_run.mp3"/>
  </properties>
  <image width="1344" height="896" source="../../../rawAssets/objects/character/elephant0/elephant0_gray1.png"/>
 </tile>
 <tile id="54">
  <properties>
   <property name="sfx:voice:0" type="file" value="../../sfx/voices/Elephant_voice_1.mp3"/>
   <property name="sfx:voice:1" type="file" value="../../sfx/voices/Elephant_voice_2.mp3"/>
   <property name="sfx:walk" type="file" value="../../sfx/objects/character/elephant0/elephant0_run.mp3"/>
  </properties>
  <image width="1344" height="896" source="../../../rawAssets/objects/character/elephant0/elephant0_white0.png"/>
 </tile>
 <tile id="55">
  <properties>
   <property name="sfx:hit" type="file" value="../../sfx/objects/item/coconut0/coconut0_hit.mp3"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/coconut0/coconut0_shell_icon.png"/>
 </tile>
 <tile id="94">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/bushbaby_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_lady0.png"/>
 </tile>
 <tile id="95">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/bushbaby_talk.mp3"/>
  </properties>
  <image width="256" height="327" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_market0.png"/>
 </tile>
 <tile id="96">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/bushbaby_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_rapper0.png"/>
 </tile>
 <tile id="97">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/bushbaby_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_scarf0.png"/>
 </tile>
 <tile id="98">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/bushbaby_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_bushbaby0_kanzu0.png"/>
 </tile>
 <tile id="99">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/colobus0_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus0_market0.png"/>
 </tile>
 <tile id="100">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/colobus0_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus0_scarf0.png"/>
 </tile>
 <tile id="101">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/colobus0_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus0_farmer0.png"/>
 </tile>
 <tile id="102">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/colobus0_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus0_rapper0.png"/>
 </tile>
 <tile id="103">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/colobus1_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus1_lady0.png"/>
 </tile>
 <tile id="104">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/colobus1_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus1_lady1.png"/>
 </tile>
 <tile id="105">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/colobus1_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus1_scarf0.png"/>
 </tile>
 <tile id="106">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/colobus1_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus1_kanzu0.png"/>
 </tile>
 <tile id="107">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/colobus1_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus1_suit0.png"/>
 </tile>
 <tile id="108">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/dikdik_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/dikdik_2.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dikdik0_kanga0.png"/>
 </tile>
 <tile id="109">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/dikdik_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/dikdik_2.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dikdik0_lady0.png"/>
 </tile>
 <tile id="110">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/dikdik_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/dikdik_2.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dikdik0_suit0.png"/>
 </tile>
 <tile id="111">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/dikdik_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/dikdik_2.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_dikdik0_kanzu0.png"/>
 </tile>
 <tile id="112">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/dikdik_1.mp3"/>
   <property name="sfx:voice1" type="file" value="../../sfx/voices/dikdik_2.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_dikdik0_massaiFarmer0.png"/>
 </tile>
 <tile id="113">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/dog_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dog0_massaiChief0.png"/>
 </tile>
 <tile id="114">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/dog_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dog0_scarf0.png"/>
 </tile>
 <tile id="115">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/dog_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_dog0_massaiFarmer0.png"/>
 </tile>
 <tile id="116">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/impalla_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_impala0_massaiChief0.png"/>
 </tile>
 <tile id="117">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/impalla_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_impala0_suit0.png"/>
 </tile>
 <tile id="118">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/impalla_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_impala0_kid0.png"/>
 </tile>
 <tile id="119">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/impalla_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_impala0_massaiFarmer0.png"/>
 </tile>
 <tile id="120">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/lizard_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_lizard0_kanga0.png"/>
 </tile>
 <tile id="121">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/lizard_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_lizard0_massai0.png"/>
 </tile>
 <tile id="122">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/lizard_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_lizard0_scarfDriver0.png"/>
 </tile>
 <tile id="123">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/lizard_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_lizard0_driver0.png"/>
 </tile>
 <tile id="124">
  <properties>
   <property name="sfx:voice0" type="file" value="../../sfx/voices/lizard_talk.mp3"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_lizard0_kanzu0.png"/>
 </tile>
 <tile id="125">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/sun0/sun0_small_collect.mp3"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/notes0/note0_one_half.png"/>
 </tile>
 <tile id="126">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/sun0/sun0_small_collect.mp3"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/notes0/note1_two_sixteenth.png"/>
 </tile>
 <tile id="127">
  <properties>
   <property name="sfx:collected" type="file" value="../../sfx/objects/item/sun0/sun0_small_collect.mp3"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/notes0/note2_one_eight.png"/>
 </tile>
 <tile id="128">
  <properties>
   <property name="sfx:grown" type="file" value=""/>
  </properties>
  <image width="483" height="450" source="../../../rawAssets/objects/decoration/palms0/palms0_icon.png"/>
 </tile>
 <tile id="129">
  <properties>
   <property name="sfx:grown" type="file" value=""/>
  </properties>
  <image width="205" height="267" source="../../../rawAssets/objects/decoration/plant10/plant10_icon.png"/>
 </tile>
 <tile id="130">
  <properties>
   <property name="sfx:grown" type="file" value=""/>
  </properties>
  <image width="259" height="259" source="../../../rawAssets/objects/decoration/plant11/plant11_icon.png"/>
 </tile>
 <tile id="132">
  <properties>
   <property name="sfx:grown" type="file" value=""/>
  </properties>
  <image width="258" height="130" source="../../../rawAssets/objects/decoration/plant12/plant12_icon.png"/>
 </tile>
 <tile id="133">
  <properties>
   <property name="sfx:grown" type="file" value=""/>
  </properties>
  <image width="214" height="150" source="../../../rawAssets/objects/decoration/plant13/plant13_icon.png"/>
 </tile>
 <tile id="134">
  <properties>
   <property name="sfx:grown" type="file" value=""/>
  </properties>
  <image width="550" height="227" source="../../../rawAssets/objects/decoration/bush3/bush3_icon.png"/>
 </tile>
</tileset>
