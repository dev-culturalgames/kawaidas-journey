<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="ui_sounds" tilewidth="128" tileheight="128" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="1">
  <properties>
   <property name="sfx:applause" type="file" value="../../sfx/level/win_applause.mp3"/>
   <property name="sfx:back" type="file" value="../../sfx/ui/btn_back_clicked.mp3"/>
   <property name="sfx:click" type="file" value="../../sfx/ui/btn_clicked.mp3"/>
   <property name="sfx:fill" type="file" value="../../sfx/ui/PBar_cash.mp3"/>
   <property name="sfx:lose" type="file" value="../../sfx/level/lose.mp3"/>
   <property name="sfx:popup_close" type="file" value="../../sfx/ui/popup_closed.mp3"/>
   <property name="sfx:popup_open" type="file" value="../../sfx/ui/popup_opened.mp3"/>
   <property name="sfx:sf_spawn" type="file" value="../../sfx/level/starfruit0_spawn.mp3"/>
   <property name="sfx:sf_spawned" type="file" value="../../sfx/level/starfruit0_spawned.mp3"/>
   <property name="sfx:win_vo_0" type="file" value="../../sfx/level/win_0_poa.mp3"/>
   <property name="sfx:win_vo_1" type="file" value="../../sfx/level/win_1_freshi.mp3"/>
   <property name="sfx:win_vo_2" type="file" value="../../sfx/level/win_2_bomba_sana.mp3"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/playSFX.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="music:credits" type="file" value="../../music/nature_iddi.mp3"/>
   <property name="music:endScreen" type="file" value="../../music/inspecta_pamba.mp3"/>
   <property name="music:intro" type="file" value="../../music/tse_mandudu_credits.mp3"/>
   <property name="music:main" type="file" value="../../music/tse_makabwela_unplugged.mp3"/>
   <property name="music:outro0" type="file" value="../../music/tse_agwe.mp3"/>
   <property name="music:outro1" type="file" value="../../music/nature_iddi.mp3"/>
   <property name="music:outro2" type="file" value="../../music/gsolo_muda.mp3"/>
   <property name="music:outro3" type="file" value="../../music/matonya_nifungulie.mp3"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/music.png"/>
 </tile>
</tileset>
