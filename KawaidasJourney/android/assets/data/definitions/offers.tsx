<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="offers" tilewidth="256" tileheight="256" tilecount="26" columns="8">
 <grid orientation="orthogonal" width="1" height="1"/>
 <properties>
  <property name="category" value="shop"/>
  <property name="type" value="offer"/>
 </properties>
 <tile id="12">
  <properties>
   <property name="amount:max" type="int" value="3"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="label" value="+1"/>
   <property name="level">level_1
price:1000
level_2
price:4000
level_3
price:8000</property>
   <property name="offerType" value="upgrade"/>
   <property name="text">You are able to carry one 
more banana. Mhh, yummy!</property>
   <property name="title" value="MORE LIFE"/>
   <property name="titleSW" value="Ndizi moja zaidi"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/solar/offer_banana_slot.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="amount:max" type="int" value="5"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="label" value="+6"/>
   <property name="level">level_1
price:1000
value:6
level_2
price:1250
value:6
level_3
price:1500
value:6
level_4
price:1750
value:6
level_5
price:2500
value:18</property>
   <property name="offerType" value="upgrade"/>
   <property name="text" value="You are able to carry more coconuts. Fight for your right!"/>
   <property name="title" value="MORE COCONUTS"/>
   <property name="titleSW" value="Minazi zaidi"/>
   <property name="value" type="float" value="6"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/solar/offer_coconut_slot.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="amount:max" type="int" value="5"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="label" value="+1s"/>
   <property name="level">level_1
price:1000
value:1f
level_2
price:1250
value:1f
level_3
price:1500
value:1f
level_4
price:1750
value:1f
level_5
price:2750
value:4f</property>
   <property name="offerType" value="upgrade"/>
   <property name="text">You are invincible for a longer time
when collecting a clove!</property>
   <property name="title" value="MORE SPICE TIME"/>
   <property name="titleSW" value="Muda mrefu la karafuu"/>
   <property name="value" type="float" value="1"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/solar/offer_karafuu_duration.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="amount:max" type="int" value="5"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="label" value="+1s"/>
   <property name="level">level_1
price:250
value:1f
level_2
price:325
value:1f
level_3
price:400
value:1f
level_4
price:475
value:1f
level_5
price:1000
value:6f</property>
   <property name="offerType" value="upgrade"/>
   <property name="text">The effect of the solar panel last longer.
Your heart is a magnet!</property>
   <property name="title" value="MORE SOLAR POWER TIME"/>
   <property name="titleSW" value="Muda zaidi ya umeme wa jua"/>
   <property name="value" type="float" value="1"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/solar/offer_solarPanel_duration.png"/>
 </tile>
 <tile id="20">
  <properties>
   <property name="amount:max" type="int" value="5"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="label" value="+r"/>
   <property name="level">level_1
price:500
value:128
level_2
price:625
value:128
level_3
price:750
value:128
level_4
price:875
value:128
level_5
price:1000
value:128</property>
   <property name="offerType" value="upgrade"/>
   <property name="text">The solar panel attracts suns in a wider
range. Your independent power supply!</property>
   <property name="title" value="MORE SOLAR POWER RADIUS"/>
   <property name="titleSW" value="Kubwa zaidi umeme wa jua"/>
   <property name="value" type="float" value="128"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/solar/offer_solarPanel_range.png"/>
 </tile>
 <tile id="21">
  <properties>
   <property name="amount:max" type="int" value="-1"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="offerType" value="item"/>
   <property name="price" type="float" value="100"/>
   <property name="text">Get one ancient artifact
to rise again from death!</property>
   <property name="title" value="ONE TOTEM"/>
   <property name="titleSW" value="Sanamu moja"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/solar/offer_totem.png"/>
 </tile>
 <tile id="22">
  <properties>
   <property name="amount:max" type="int" value="2"/>
   <property name="googleType" value="entitlement"/>
   <property name="isPremium" type="bool" value="true"/>
   <property name="label" value="+1"/>
   <property name="level">level_1
price:0.0f
level_2
price:0.0f
</property>
   <property name="offerType" value="upgrade"/>
   <property name="text">You are able to carry one 
more totem. Rise again!</property>
   <property name="title" value="MORE TOTEMS"/>
   <property name="titleSW" value="Sanamu moja zaidi"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/prime/offer_totem_slot.png"/>
 </tile>
 <tile id="25">
  <properties>
   <property name="amount:max" type="int" value="-1"/>
   <property name="googleType" value="consumable"/>
   <property name="isPremium" type="bool" value="true"/>
   <property name="offerType" value="consumable"/>
   <property name="price" type="float" value="0"/>
   <property name="text" value="Sun is shining!"/>
   <property name="title" value="GET 2.000 SUNS"/>
   <property name="titleSW" value="Pakiti ndogo ya jua"/>
   <property name="value" type="float" value="2000"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/prime/offer_sun_s.png"/>
 </tile>
 <tile id="26">
  <properties>
   <property name="amount:max" type="int" value="-1"/>
   <property name="googleType" value="consumable"/>
   <property name="isPremium" type="bool" value="true"/>
   <property name="offerType" value="consumable"/>
   <property name="price" type="float" value="0"/>
   <property name="text" value="Feel the warmth!"/>
   <property name="title" value="GET 4.000 SUNS"/>
   <property name="titleSW" value="Pakiti ya kati ya jua"/>
   <property name="value" type="float" value="4000"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/prime/offer_sun_m.png"/>
 </tile>
 <tile id="27">
  <properties>
   <property name="amount:max" type="int" value="-1"/>
   <property name="googleType" value="consumable"/>
   <property name="isPremium" type="bool" value="true"/>
   <property name="offerType" value="consumable"/>
   <property name="price" type="float" value="0"/>
   <property name="text" value="It's getting hot in here!"/>
   <property name="title" value="GET 8.000 SUNS"/>
   <property name="titleSW" value="Pakiti kubwa ya jua"/>
   <property name="value" type="float" value="8000"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/prime/offer_sun_l.png"/>
 </tile>
 <tile id="28">
  <properties>
   <property name="amount:max" type="int" value="1"/>
   <property name="googleType" value="entitlement"/>
   <property name="isPremium" type="bool" value="true"/>
   <property name="offerType" value="item"/>
   <property name="price" type="float" value="0"/>
   <property name="text">For every sun you collect, you
get two suns. Double the power!</property>
   <property name="title" value="DOUBLE SUNS"/>
   <property name="titleSW" value="Jua mara mbili"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/prime/offer_sun_double.png"/>
 </tile>
 <tile id="33">
  <properties>
   <property name="amount:max" type="int" value="4"/>
   <property name="googleType" value="entitlement"/>
   <property name="isPremium" type="bool" value="true"/>
   <property name="level">level_1
price:0.0f
level_2
price:0.0f
level_3
price:0.0f
level_4
price:0.0f</property>
   <property name="offerType" value="upgrade"/>
   <property name="price" type="float" value="0"/>
   <property name="text">Unlock the next chapter and all scenarios
of the current chapter. Go for it!</property>
   <property name="title" value="UNLOCK CHAPTER"/>
   <property name="titleSW" value="Kufungua sura mpya"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/prime/offer_unlock_chapter.png"/>
 </tile>
 <tile id="34">
  <properties>
   <property name="amount:max" type="int" value="1"/>
   <property name="googleType" value="entitlement"/>
   <property name="isPremium" type="bool" value="true"/>
   <property name="offerType" value="item"/>
   <property name="price" type="float" value="0"/>
   <property name="text">Unlock all chapters and scenarios, 
reveal all secrets and get all upgrades   
instantly.Show some love for East 
Africa! ASANTE SANA!</property>
   <property name="title" value="SUPPORT PACK"/>
   <property name="titleSW" value="Pakiti ya msaada"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/prime/offer_unlock_all.png"/>
 </tile>
 <tile id="35">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="1"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="2000"/>
   <property name="skin" value="burkaMonkey0"/>
   <property name="text" value="Get an epic burka ninja costume for Kawaida."/>
   <property name="title" value="BURKA KAWAIDA - BLACK NINJA"/>
   <property name="titleSW" value="Buibui - ninja nyeusi"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_burkaMonkey0.png"/>
 </tile>
 <tile id="47">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="0"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="isReward" type="bool" value="true"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="0"/>
   <property name="skin" value="graduateMonkey0"/>
   <property name="text" value="Get an epic graduate costume for Kawaida."/>
   <property name="title" value="GRADUATE KAWAIDA"/>
   <property name="titleSW" value="Hitimu"/>
   <property name="unlockText">To get this epic costume you have
to unlock all encyclopedia items first!</property>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_graduateMonkey0.png"/>
 </tile>
 <tile id="48">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="1"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="2000"/>
   <property name="skin" value="soccerMonkey0_green"/>
   <property name="text" value="Get an epic green soccer skin for Kawaida."/>
   <property name="title" value="GREEN SOCCER KAWAIDA"/>
   <property name="titleSW" value="Mchezaji wa mpira kijani"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_soccerMonkey0_green.png"/>
 </tile>
 <tile id="49">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="0"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="isReward" type="bool" value="true"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="0"/>
   <property name="skin" value="hipHopMonkey0"/>
   <property name="text" value="Get an epic rapper costume for Kawaida."/>
   <property name="title" value="RAPPER KAWAIDA"/>
   <property name="titleSW" value="Mwimbaji wa Bongo Flava"/>
   <property name="unlockText">To unlock this epic costume you have
to finish the BONGO FLAVA MUSIC level
in DAR ES SALAAM!</property>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_hipHopMonkey0.png"/>
 </tile>
 <tile id="50">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="1"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="2000"/>
   <property name="skin" value="holyMonkey0"/>
   <property name="text" value="Get an epic priest costume for Kawaida."/>
   <property name="title" value="HOLY KAWAIDA"/>
   <property name="titleSW" value="Kuhani"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_holyMonkey0.png"/>
 </tile>
 <tile id="51">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="1"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="2000"/>
   <property name="skin" value="kangaMonkey0"/>
   <property name="text" value="Get an epic kanga ninja costume for Kawaida. KANGA or KITENGE are colourful cloths traditionally worn by East African women. "/>
   <property name="title" value="KANGA KAWAIDA - RED NINJA"/>
   <property name="titleSW" value="Kanga - ninja nyekundu"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_kangaMonkey0.png"/>
 </tile>
 <tile id="52">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="1"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="2000"/>
   <property name="skin" value="kanzuMonkey0"/>
   <property name="text" value="Get an epic kanzu costume for Kawaida. A KANZU is a white coloured robe worn by East African men mostly together with the KOFIA hat."/>
   <property name="title" value="KANZU KAWAIDA"/>
   <property name="titleSW" value="Kanzu na kofia"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_kanzuMonkey0.png"/>
 </tile>
 <tile id="53">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="0"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="isReward" type="bool" value="true"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="0"/>
   <property name="skin" value="massaiMonkey0"/>
   <property name="text" value="Get an epic massai costume for Kawaida."/>
   <property name="title" value="MASSAI KAWAIDA"/>
   <property name="titleSW" value="Mimi ni Massai bwana!"/>
   <property name="unlockText">To unlock this epic costume you have
to finish the PIPE REPAIR level
in the SERENGETI.</property>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_massaiMonkey0.png"/>
 </tile>
 <tile id="54">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="1"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="2000"/>
   <property name="skin" value="soccerMonkey0_red"/>
   <property name="text" value="Get an epic red soccer costume for Kawaida."/>
   <property name="title" value="RED SOCCER KAWAIDA"/>
   <property name="titleSW" value="Mchezaji wa mpira nyekundu"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_soccerMonkey0_red.png"/>
 </tile>
 <tile id="55">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="0"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="isReward" type="bool" value="true"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="0"/>
   <property name="skin" value="royalMonkey0"/>
   <property name="text" value="Get an epic royal costume for Kawaida."/>
   <property name="title" value="ROYAL KAWAIDA"/>
   <property name="titleSW" value="Mfalme"/>
   <property name="unlockText">To unlock this epic costume you have
to beat the FINAL BOSS!</property>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_royalMonkey0.png"/>
 </tile>
 <tile id="56">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="0"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="isReward" type="bool" value="true"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="0"/>
   <property name="skin" value="sailorMonkey0"/>
   <property name="text" value="Get an epic sailor costume for Kawaida."/>
   <property name="title" value="SAILOR KAWAIDA"/>
   <property name="titleSW" value="Mvuvi"/>
   <property name="unlockText">To unlock this epic costume you have
to beat the FIRST LEVEL.</property>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_sailorMonkey0.png"/>
 </tile>
 <tile id="57">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="0"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="isReward" type="bool" value="true"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="0"/>
   <property name="skin" value="fireFighterMonkey0"/>
   <property name="text" value="Get an epic fire fighter costume for Kawaida."/>
   <property name="title" value="FIRE FIGHTER KAWAIDA"/>
   <property name="titleSW" value="Mzima moto"/>
   <property name="unlockText">To unlock this epic costume you have
to finish the FIRE FIGHTING level
in ZANZIBAR TOWN!</property>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_fireFighterMonkey0.png"/>
 </tile>
 <tile id="58">
  <properties>
   <property name="activatable" type="bool" value="true"/>
   <property name="amount:max" type="int" value="1"/>
   <property name="isPremium" type="bool" value="false"/>
   <property name="offerType" value="skin"/>
   <property name="price" type="float" value="2000"/>
   <property name="skin" value="rastaMonkey0"/>
   <property name="text" value="Get an epic rastafari costume for Kawaida."/>
   <property name="title" value="RASTA KAWAIDA"/>
   <property name="titleSW" value="Rastafari"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/offers/skins/offer_skin_rastaMonkey0.png"/>
 </tile>
</tileset>
