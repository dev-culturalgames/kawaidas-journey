<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="skin" tilewidth="1344" tileheight="1024" tilecount="112" columns="8">
 <grid orientation="orthogonal" width="1024" height="640"/>
 <tile id="0">
  <properties>
   <property name="skeleton" value="smoke0"/>
   <property name="spawn:defName" value="smokeCloud0_clean0"/>
  </properties>
  <image width="384" height="384" source="../../../rawAssets/objects/character/smoke0/smoke0_clean0.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="logics" value="spawn,attack"/>
   <property name="skeleton" value="smoke0"/>
   <property name="spawn:defName" value="smokeCloud0_dirty0"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="384" height="384" source="../../../rawAssets/objects/character/smoke0/smoke0_dirty0.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="logics" value="spawn,attack"/>
   <property name="skeleton" value="smoke0"/>
   <property name="spawn:defName" value="smokeCloud0_poison0"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="384" height="384" source="../../../rawAssets/objects/character/smoke0/smoke0_poison0.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="skeleton" value="smokeCloud0"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/character/smokeCloud0/smokeCloud0_clean0.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="logics" value="move,cloud,life,attack"/>
   <property name="skeleton" value="smokeCloud0"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/character/smokeCloud0/smokeCloud0_dirty0.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="logics" value="move,cloud,life,attack"/>
   <property name="skeleton" value="smokeCloud0"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/character/smokeCloud0/smokeCloud0_poison0.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="skeleton" value="transporter0"/>
  </properties>
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_blue0.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="skeleton" value="transporter0"/>
  </properties>
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_blue1.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="skeleton" value="transporter0"/>
  </properties>
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_red0.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="skeleton" value="transporter0"/>
  </properties>
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_red1.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="skeleton" value="bush0"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/bush0/bush0_dGreen0.png"/>
 </tile>
 <tile id="12">
  <properties>
   <property name="skeleton" value="bush0"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/bush0/bush0_green0.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="skeleton" value="bush0"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/bush0/bush0_lGreen0.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="attack:ranged" type="bool" value="true"/>
   <property name="logics" value="attack,colobus"/>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="false"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/colobus0_evil.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="logics" value="move,jump,attack,life,character"/>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/drifter0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_drifter" x="192" y="248" width="128" height="104">
    <properties>
     <property name="box2d:category" value="trigger,danger"/>
    </properties>
   </object>
   <object id="4" name="life_hitbox" x="192" y="224" width="128" height="24">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <properties>
   <property name="ammo:amount" type="int" value="0"/>
   <property name="ammo:amount_max" type="int" value="6"/>
   <property name="attack:isEnemy" type="bool" value="false"/>
   <property name="coin:amount" type="int" value="0"/>
   <property name="coin:amount_max" type="int" value="-1"/>
   <property name="idleFillInBaseTime" type="float" value="5"/>
   <property name="idleFillInOffset" type="float" value="1"/>
   <property name="inventory:items" value="coin,potion,tool,ammo"/>
   <property name="life:isEnemy" type="bool" value="false"/>
   <property name="logics" value="move,jump,inventory,attack,context,plant,climb,life,character,extinguish,pickUp"/>
   <property name="onDead">game:delay:1f
game:lose</property>
   <property name="potion:amount" type="int" value="1"/>
   <property name="potion:amount_max" type="int" value="1"/>
   <property name="skeleton" value="monkey0"/>
   <property name="subcategory" value="avatar"/>
   <property name="tool:amount" type="int" value="0"/>
   <property name="tool:amount_max" type="int" value="24"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/kawaida0.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="skeleton" value="palm0"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm0/palm0_bright0.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="skeleton" value="palm2"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm2/palm2.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="skeleton" value="palm0"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm0/palm0_bright1.png"/>
 </tile>
 <tile id="20">
  <properties>
   <property name="skeleton" value="palm0"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm0/palm0_bright2.png"/>
 </tile>
 <tile id="21">
  <properties>
   <property name="skeleton" value="palm0"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm0/palm0_bright3.png"/>
 </tile>
 <tile id="22">
  <properties>
   <property name="skeleton" value="bus0"/>
  </properties>
  <image width="1024" height="384" source="../../../rawAssets/objects/vehicle/bus0/omnibus0_red0.png"/>
 </tile>
 <tile id="23">
  <properties>
   <property name="skeleton" value="bus0"/>
  </properties>
  <image width="1024" height="384" source="../../../rawAssets/objects/vehicle/bus0/omnibus0_blue0.png"/>
 </tile>
 <tile id="24">
  <properties>
   <property name="skeleton" value="leafPlant0"/>
  </properties>
  <image width="384" height="256" source="../../../rawAssets/objects/decoration/leafPlant0/plant0.png"/>
 </tile>
 <tile id="25">
  <properties>
   <property name="skeleton" value="leafPlant0"/>
  </properties>
  <image width="384" height="256" source="../../../rawAssets/objects/decoration/leafPlant0/plant1.png"/>
 </tile>
 <tile id="26">
  <properties>
   <property name="skeleton" value="leafPlant0"/>
  </properties>
  <image width="384" height="256" source="../../../rawAssets/objects/decoration/leafPlant0/plant2.png"/>
 </tile>
 <tile id="27">
  <properties>
   <property name="skeleton" value="leafPlant0"/>
  </properties>
  <image width="384" height="256" source="../../../rawAssets/objects/decoration/leafPlant0/plant3.png"/>
 </tile>
 <tile id="28">
  <properties>
   <property name="skeleton" value="leafPlant0"/>
  </properties>
  <image width="384" height="256" source="../../../rawAssets/objects/decoration/leafPlant0/plant4.png"/>
 </tile>
 <tile id="29">
  <properties>
   <property name="skeleton" value="smallPlant0"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/plant5_icon.png"/>
 </tile>
 <tile id="30">
  <properties>
   <property name="skeleton" value="smallPlant0"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/plant6_icon.png"/>
 </tile>
 <tile id="31">
  <properties>
   <property name="skeleton" value="smallPlant0"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/plant7_icon.png"/>
 </tile>
 <tile id="32">
  <properties>
   <property name="skeleton" value="smallPlant0"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/plant8_icon.png"/>
 </tile>
 <tile id="33">
  <properties>
   <property name="skeleton" value="smallPlant0"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/plant9_icon.png"/>
 </tile>
 <tile id="34">
  <properties>
   <property name="skeleton" value="chicken0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="419" height="418" source="../../../rawAssets/objects/character/chicken0/chicken0_brown0.png"/>
 </tile>
 <tile id="35">
  <properties>
   <property name="attack:isEnemy" type="bool" value="true"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="chicken:pickDelay" type="float" value="1"/>
   <property name="chicken:pickRandom" type="float" value="2"/>
   <property name="life:despawn_delay" type="float" value="3"/>
   <property name="logics" value="move,life,attack,chicken"/>
   <property name="move:speedX" type="float" value="1"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="range" type="float" value="400"/>
   <property name="skeleton" value="chicken0"/>
   <property name="useDefName" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
   <property name="view" type="float" value="700"/>
  </properties>
  <image width="419" height="418" source="../../../rawAssets/objects/character/chicken0/chicken0_evil0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="life_hitbox" x="104.75" y="104.5" width="209.5" height="52.25">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
   <object id="2" name="attack_trigger" x="261.875" y="156.75" width="52.25" height="180">
    <properties>
     <property name="box2d:category" value="trigger"/>
    </properties>
   </object>
   <object id="3" name="life_hitbox" x="104.75" y="156.75" width="157.125" height="104.5">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="36">
  <properties>
   <property name="skeleton" value="chicken0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="419" height="418" source="../../../rawAssets/objects/character/chicken0/chicken0_white0.png"/>
 </tile>
 <tile id="37">
  <properties>
   <property name="skeleton" value="cloth0"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/cloth0/towel0.png"/>
 </tile>
 <tile id="38">
  <properties>
   <property name="skeleton" value="cloth0"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/cloth0/towel1.png"/>
 </tile>
 <tile id="39">
  <properties>
   <property name="skeleton" value="cloth0"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/cloth0/towel2.png"/>
 </tile>
 <tile id="40">
  <properties>
   <property name="skeleton" value="cloth0"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/cloth0/towel3.png"/>
 </tile>
 <tile id="55">
  <properties>
   <property name="skeleton" value="motorbike0"/>
  </properties>
  <image width="512" height="384" source="../../../rawAssets/objects/vehicle/motorbike0/pikiPiki0.png"/>
 </tile>
 <tile id="56">
  <properties>
   <property name="skeleton" value="motorbike1"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/vehicle/motorbike1/bajaji0_blue0.png"/>
 </tile>
 <tile id="57">
  <properties>
   <property name="skeleton" value="motorbike1"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/vehicle/motorbike1/bajaji0_green0.png"/>
 </tile>
 <tile id="58">
  <properties>
   <property name="skeleton" value="motorbike1"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/vehicle/motorbike1/bajaji0_red0.png"/>
 </tile>
 <tile id="161">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/bushbaby0.png"/>
 </tile>
 <tile id="162">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/colobus0.png"/>
 </tile>
 <tile id="163">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/colobus1.png"/>
 </tile>
 <tile id="164">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/dikdik0.png"/>
 </tile>
 <tile id="165">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/dog0.png"/>
 </tile>
 <tile id="166">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_lady0.png"/>
 </tile>
 <tile id="167">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="327" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_market0.png"/>
 </tile>
 <tile id="168">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_rapper0.png"/>
 </tile>
 <tile id="169">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_scarf0.png"/>
 </tile>
 <tile id="170">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus0_market0.png"/>
 </tile>
 <tile id="171">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus0_scarf0.png"/>
 </tile>
 <tile id="172">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus1_lady0.png"/>
 </tile>
 <tile id="173">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus1_lady1.png"/>
 </tile>
 <tile id="174">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus1_scarf0.png"/>
 </tile>
 <tile id="175">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dikdik0_kanga0.png"/>
 </tile>
 <tile id="176">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dikdik0_lady0.png"/>
 </tile>
 <tile id="177">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dikdik0_suit0.png"/>
 </tile>
 <tile id="178">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dog0_massaiChief0.png"/>
 </tile>
 <tile id="179">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dog0_scarf0.png"/>
 </tile>
 <tile id="180">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_impala0_massaiChief0.png"/>
 </tile>
 <tile id="181">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_impala0_suit0.png"/>
 </tile>
 <tile id="182">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_lizard0_kanga0.png"/>
 </tile>
 <tile id="183">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_lizard0_massai0.png"/>
 </tile>
 <tile id="184">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_lizard0_scarfDriver0.png"/>
 </tile>
 <tile id="185">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/impala0.png"/>
 </tile>
 <tile id="186">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/karim0.png"/>
 </tile>
 <tile id="187">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lizard0.png"/>
 </tile>
 <tile id="188">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_bushbaby0_kanzu0.png"/>
 </tile>
 <tile id="189">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus0_farmer0.png"/>
 </tile>
 <tile id="190">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus0_rapper0.png"/>
 </tile>
 <tile id="191">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus1_kanzu0.png"/>
 </tile>
 <tile id="192">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus1_suit0.png"/>
 </tile>
 <tile id="193">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_dikdik0_kanzu0.png"/>
 </tile>
 <tile id="194">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_dikdik0_massaiFarmer0.png"/>
 </tile>
 <tile id="195">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_dog0_massaiFarmer0.png"/>
 </tile>
 <tile id="196">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_impala0_kid0.png"/>
 </tile>
 <tile id="197">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_impala0_massaiFarmer0.png"/>
 </tile>
 <tile id="198">
  <properties>
   <property name="animsReplace" value="walk:walk_noSkirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_lizard0_driver0.png"/>
 </tile>
 <tile id="199">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_lizard0_kanzu0.png"/>
 </tile>
 <tile id="200">
  <properties>
   <property name="logics" value="move,character"/>
   <property name="skeleton" value="elephant0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="1344" height="896" source="../../../rawAssets/objects/character/elephant0/elephant0_gray0.png"/>
 </tile>
 <tile id="201">
  <properties>
   <property name="logics" value="move,character"/>
   <property name="skeleton" value="elephant0"/>
   <property name="useDefName" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="1344" height="896" source="../../../rawAssets/objects/character/elephant0/elephant0_white0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="passable" x="462" y="224" width="504" height="28">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="2" name="passable" x="462" y="224">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
    <polygon points="0,0 -84,42 -84,70 0,28"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="202">
  <properties>
   <property name="skeleton" value="hyena0"/>
  </properties>
  <image width="320" height="320" source="../../../rawAssets/objects/character/hyena0/hyena0.png"/>
 </tile>
 <tile id="203">
  <properties>
   <property name="skeleton" value="marabu0"/>
  </properties>
  <image width="1024" height="1024" source="../../../rawAssets/objects/character/marabu0/marabu0.png"/>
 </tile>
 <tile id="207">
  <properties>
   <property name="logics" value="move,jump,attack,life,character"/>
   <property name="skeleton" value="monkey0"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/baboon0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_baboon" x="192" y="248" width="128" height="104">
    <properties>
     <property name="box2d:category" value="trigger,danger"/>
    </properties>
   </object>
   <object id="2" name="life_hitbox" x="192" y="224" width="128" height="24">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="208">
  <properties>
   <property name="logics" value="move,attack"/>
   <property name="skeleton" value="elephant0"/>
   <property name="useDefName" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="1344" height="896" source="../../../rawAssets/objects/character/elephant0/elephant0_gray1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_passive" x="168" y="-1456" width="882" height="2240">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="210">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lady1.png"/>
 </tile>
 <tile id="211">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lady2.png"/>
 </tile>
 <tile id="213">
  <properties>
   <property name="animsReplace" value="walk:walk_skirt"/>
   <property name="logics" value="move,character,plant"/>
   <property name="skeleton" value="ragdoll0"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lady0.png"/>
 </tile>
 <tile id="214">
  <properties>
   <property name="skeleton" value="bulldozer0"/>
  </properties>
  <image width="768" height="640" source="../../../rawAssets/objects/character/bulldozer0/steamGhost0_icon.png"/>
 </tile>
 <tile id="215">
  <properties>
   <property name="skeleton" value="electricSpark0"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/character/electricSpark0/electricSpark0_big.png"/>
 </tile>
 <tile id="216">
  <properties>
   <property name="skeleton" value="electricSpark0"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/character/electricSpark0/electricSpark0_small.png"/>
 </tile>
 <tile id="217">
  <properties>
   <property name="animsReplace" value="idle:one_half"/>
   <property name="skeleton" value="notes0"/>
   <property name="useAnimation" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/notes0/note0_one_half.png"/>
 </tile>
 <tile id="218">
  <properties>
   <property name="animsReplace" value="idle:two_sixteenth"/>
   <property name="skeleton" value="notes0"/>
   <property name="useAnimation" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/notes0/note1_two_sixteenth.png"/>
 </tile>
 <tile id="219">
  <properties>
   <property name="animsReplace" value="idle:one_eighth"/>
   <property name="skeleton" value="notes0"/>
   <property name="useAnimation" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/notes0/note2_one_eight.png"/>
 </tile>
 <tile id="220">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/burkaMonkey0.png"/>
 </tile>
 <tile id="221">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/graduateMonkey0.png"/>
 </tile>
 <tile id="222">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/soccerMonkey0_green.png"/>
 </tile>
 <tile id="223">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/hipHopMonkey0.png"/>
 </tile>
 <tile id="224">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/holyMonkey0.png"/>
 </tile>
 <tile id="225">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/kangaMonkey0.png"/>
 </tile>
 <tile id="226">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/kanzuMonkey0.png"/>
 </tile>
 <tile id="227">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/massaiMonkey0.png"/>
 </tile>
 <tile id="228">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/soccerMonkey0_red.png"/>
 </tile>
 <tile id="229">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/royalMonkey0.png"/>
 </tile>
 <tile id="230">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/sailorMonkey0.png"/>
 </tile>
 <tile id="231">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/fireFighterMonkey0.png"/>
 </tile>
 <tile id="232">
  <properties>
   <property name="skeleton" value="monkey0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/rastaMonkey0.png"/>
 </tile>
 <tile id="233">
  <properties>
   <property name="ammo:amount" type="int" value="0"/>
   <property name="ammo:amount_max" type="int" value="6"/>
   <property name="attack:isEnemy" type="bool" value="false"/>
   <property name="coin:amount" type="int" value="0"/>
   <property name="coin:amount_max" type="int" value="-1"/>
   <property name="idleFillInBaseTime" type="float" value="5"/>
   <property name="idleFillInOffset" type="float" value="1"/>
   <property name="inventory:items" value="coin,potion,tool,ammo"/>
   <property name="life:isEnemy" type="bool" value="false"/>
   <property name="logics" value="move,jump,inventory,attack,context,life,character"/>
   <property name="onDead">game:delay:1f
game:lose</property>
   <property name="potion:amount" type="int" value="1"/>
   <property name="potion:amount_max" type="int" value="1"/>
   <property name="skeleton" value="monkey0_bike"/>
   <property name="subcategory" value="avatar"/>
   <property name="tool:amount" type="int" value="0"/>
   <property name="tool:amount_max" type="int" value="24"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="1024" height="512" source="../../../rawAssets/objects/character/monkey0_bike/kawaida0_bike.png"/>
 </tile>
</tileset>
