<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="mapDeco" tilewidth="256" tileheight="138" tilecount="43" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <properties>
  <property name="category" value="userInterface"/>
  <property name="definitionSet" type="bool" value="true"/>
 </properties>
 <tile id="2">
  <properties>
   <property name="lvlBtn" value=""/>
   <property name="type" value="lvlPath"/>
  </properties>
  <image width="32" height="32" source="../../../rawAssets/sprites/global/ui/icons/map/map_path_white.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_ant.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_blueHouse.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_bulldozer.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_colobus.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_dala.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="186" height="138" source="../../../rawAssets/sprites/global/ui/icons/map/map_dar.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_door4.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_door5.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_fireTrash.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_how.png"/>
 </tile>
 <tile id="20">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_karafu.png"/>
 </tile>
 <tile id="24">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_note.png"/>
 </tile>
 <tile id="25">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_solar.png"/>
 </tile>
 <tile id="26">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_plant.png"/>
 </tile>
 <tile id="27">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="130" height="120" source="../../../rawAssets/sprites/global/ui/icons/map/map_stoneTown.png"/>
 </tile>
 <tile id="28">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_treeHouse.png"/>
 </tile>
 <tile id="30">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_twinTower.png"/>
 </tile>
 <tile id="31">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_whiteHouse.png"/>
 </tile>
 <tile id="32">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_baboon.png"/>
 </tile>
 <tile id="36">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_fire_ext.png"/>
 </tile>
 <tile id="37">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_fire_grass.png"/>
 </tile>
 <tile id="38">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_hyena.png"/>
 </tile>
 <tile id="41">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_pipe.png"/>
 </tile>
 <tile id="42">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_trash.png"/>
 </tile>
 <tile id="43">
  <properties>
   <property name="dirX" type="float" value="1"/>
   <property name="dirY" type="float" value="1"/>
   <property name="randomSpeed" type="float" value="0.05"/>
   <property name="speedX" type="float" value="0.35"/>
   <property name="speedY" type="float" value="0.35"/>
   <property name="type" value="mapDeco"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/mapDeco/birds0.png"/>
 </tile>
 <tile id="44">
  <properties>
   <property name="type" value="mapDeco"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/mapDeco/boat0.png"/>
 </tile>
 <tile id="45">
  <properties>
   <property name="dirX" type="float" value="-1"/>
   <property name="dirY" type="float" value="0.2"/>
   <property name="randomSpeed" type="float" value="0.1"/>
   <property name="speedX" type="float" value="0.35"/>
   <property name="speedY" type="float" value="0.35"/>
   <property name="type" value="mapDeco"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/mapDeco/cloud5.png"/>
 </tile>
 <tile id="46">
  <properties>
   <property name="animTimeScales" value="idle:0.5f"/>
   <property name="type" value="mapDeco"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/mapDeco/dolphin0.png"/>
 </tile>
 <tile id="47">
  <properties>
   <property name="animTimeScales" value="idle:0.5f"/>
   <property name="type" value="mapDeco"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/mapDeco/ferry0.png"/>
 </tile>
 <tile id="48">
  <properties>
   <property name="type" value="mapDeco"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/mapDeco/impala0.png"/>
 </tile>
 <tile id="49">
  <properties>
   <property name="type" value="mapDeco"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/mapDeco/lion0.png"/>
 </tile>
 <tile id="50">
  <properties>
   <property name="animTimeScales" value="idle:0.5f"/>
   <property name="type" value="mapDeco"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/mapDeco/waves_beach0.png"/>
 </tile>
 <tile id="51">
  <properties>
   <property name="type" value="mapDeco"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/mapDeco/waves_sea0.png"/>
 </tile>
 <tile id="52">
  <properties>
   <property name="type" value="mapDeco"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/mapDeco/whale0.png"/>
 </tile>
 <tile id="53">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_25.png"/>
 </tile>
 <tile id="54">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_26.png"/>
 </tile>
 <tile id="55">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_33.png"/>
 </tile>
 <tile id="56">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_35.png"/>
 </tile>
 <tile id="57">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_36.png"/>
 </tile>
 <tile id="58">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_37.png"/>
 </tile>
 <tile id="59">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_47.png"/>
 </tile>
 <tile id="60">
  <properties>
   <property name="levelPath" type="file" value=""/>
   <property name="nextPoint" value=""/>
   <property name="type" value="lvlBtn"/>
  </properties>
  <image width="96" height="96" source="../../../rawAssets/sprites/global/ui/icons/map/map_48.png"/>
 </tile>
</tileset>
