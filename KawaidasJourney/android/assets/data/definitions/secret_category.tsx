<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="secret_category_defs" tilewidth="320" tileheight="256" tilecount="6" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="name" value="topics"/>
   <property name="position" type="int" value="0"/>
   <property name="titleSW" value="Mada"/>
  </properties>
  <image width="320" height="256" source="../../../rawAssets/sprites/global/ui/icons/ency/ency_topics.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="name" value="characters"/>
   <property name="position" type="int" value="1"/>
   <property name="titleSW" value="Wahusika"/>
  </properties>
  <image width="320" height="256" source="../../../rawAssets/sprites/global/ui/icons/ency/ency_characters.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="name" value="places"/>
   <property name="position" type="int" value="3"/>
   <property name="titleSW" value="Mahali"/>
  </properties>
  <image width="320" height="256" source="../../../rawAssets/sprites/global/ui/icons/ency/ency_places.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="name" value="skills"/>
   <property name="position" type="int" value="2"/>
   <property name="titleSW" value="Ujuzi"/>
  </properties>
  <image width="320" height="256" source="../../../rawAssets/sprites/global/ui/icons/ency/ency_skills.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="name" value="objects"/>
   <property name="position" type="int" value="4"/>
   <property name="titleSW" value="Vito"/>
  </properties>
  <image width="320" height="256" source="../../../rawAssets/sprites/global/ui/icons/ency/ency_objects.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="name" value="swahili"/>
   <property name="position" type="int" value="5"/>
   <property name="titleSW" value="Kiswahili"/>
  </properties>
  <image width="320" height="256" source="../../../rawAssets/sprites/global/ui/icons/ency/ency_kiswahili.png"/>
 </tile>
</tileset>
