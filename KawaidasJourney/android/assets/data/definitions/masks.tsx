<?xml version="1.0" encoding="UTF-8"?>
<tileset name="masks" tilewidth="99" tileheight="114" tilecount="11" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <properties>
  <property name="category" value="utility"/>
  <property name="definitionSet" type="bool" value="true"/>
  <property name="environment" value="global"/>
  <property name="fromSet" type="bool" value="true"/>
  <property name="subcategory" value="mask"/>
 </properties>
 <tile id="2">
  <image width="99" height="109" source="../../../rawAssets/utility/masks/mask0.png"/>
 </tile>
 <tile id="3">
  <image width="79" height="113" source="../../../rawAssets/utility/masks/mask1.png"/>
 </tile>
 <tile id="4">
  <image width="88" height="114" source="../../../rawAssets/utility/masks/mask2.png"/>
 </tile>
 <tile id="5">
  <image width="64" height="64" source="../../../rawAssets/utility/masks/mask3.png"/>
 </tile>
 <tile id="6">
  <image width="64" height="64" source="../../../rawAssets/utility/masks/mask4.png"/>
 </tile>
 <tile id="7">
  <image width="64" height="64" source="../../../rawAssets/utility/masks/mask5.png"/>
 </tile>
 <tile id="8">
  <image width="64" height="64" source="../../../rawAssets/utility/masks/mask6.png"/>
 </tile>
 <tile id="9">
  <image width="64" height="64" source="../../../rawAssets/utility/masks/mask7.png"/>
 </tile>
 <tile id="10">
  <image width="64" height="64" source="../../../rawAssets/utility/masks/mask8.png"/>
 </tile>
 <tile id="11">
  <image width="64" height="64" source="../../../rawAssets/utility/masks/mask9.png"/>
 </tile>
 <tile id="12">
  <image width="64" height="64" source="../../../rawAssets/utility/masks/alphaMask0.png"/>
 </tile>
</tileset>
