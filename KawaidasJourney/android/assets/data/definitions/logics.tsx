<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="logics" tilewidth="128" tileheight="128" tilecount="16" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="delay" type="float" value="1"/>
   <property name="delay_randomize" type="float" value="2"/>
   <property name="maxProgress" type="float" value="1"/>
   <property name="minProgress" type="float" value="0"/>
   <property name="randomize" type="bool" value="false"/>
   <property name="rangeX" type="float" value="256"/>
   <property name="rangeY" type="float" value="256"/>
   <property name="target" value="camTarget"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/ambient.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="height" type="float" value="0.25"/>
   <property name="speed" type="float" value="0.75"/>
   <property name="target" value="camTarget"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/bumper.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="flying" type="bool" value="false"/>
   <property name="gravityScale" type="float" value="15"/>
   <property name="isBullet" type="bool" value="false"/>
   <property name="speedModX" type="float" value="0.5"/>
   <property name="speedModY" type="float" value="0.5"/>
   <property name="speedRandomize" type="float" value="0"/>
   <property name="speedX" type="float" value="1"/>
   <property name="speedY" type="float" value="1"/>
   <property name="still" type="bool" value="false"/>
   <property name="usePhysics" type="bool" value="false"/>
   <property name="waypoints" value=""/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/move.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="height_max" type="float" value="1"/>
   <property name="speed" type="float" value="1"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/jump.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="speedX" type="float" value="1"/>
   <property name="speedY" type="float" value="1"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/climb.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="ammo" type="int" value="0"/>
   <property name="ammo_max" type="int" value="0"/>
   <property name="damage" type="int" value="1"/>
   <property name="directionX" type="float" value="1"/>
   <property name="directionY" type="float" value="0.15"/>
   <property name="isEnemy" type="bool" value="true"/>
   <property name="jump" type="bool" value="false"/>
   <property name="killsInstant" type="bool" value="false"/>
   <property name="melee" type="bool" value="false"/>
   <property name="power" type="float" value="2"/>
   <property name="range" type="float" value="0"/>
   <property name="ranged" type="bool" value="false"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/attack.png"/>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="seeds" type="int" value="0"/>
   <property name="seeds_max" type="int" value="24"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/plant.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="emotional" type="bool" value="false"/>
   <property name="portrait" type="file" value=""/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/character.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="ambient_fertilized" value=""/>
   <property name="canBlock" type="bool" value="false"/>
   <property name="damage_types" value="melee,ranged"/>
   <property name="despawnOnDeath" type="bool" value="true"/>
   <property name="despawn_delay" type="float" value="0"/>
   <property name="grow_random" type="float" value="0"/>
   <property name="grow_time" type="float" value="1"/>
   <property name="growable" type="bool" value="false"/>
   <property name="growing" type="bool" value="false"/>
   <property name="hitpoints" type="int" value="1"/>
   <property name="hitpoints_max" type="int" value="1"/>
   <property name="isEnemy" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/life.png"/>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="range" type="float" value="0"/>
   <property name="slots" type="int" value="0"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/inventory.png"/>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="11">
  <image width="128" height="128" source="../../../rawAssets/logics/repair.png"/>
 </tile>
 <tile id="12">
  <image width="128" height="128" source="../../../rawAssets/logics/bonus.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="allowMove" type="bool" value="true"/>
   <property name="height" type="float" value="1"/>
   <property name="singleUse" type="bool" value="false"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/trampoline.png"/>
 </tile>
 <tile id="14">
  <image width="128" height="128" source="../../../rawAssets/logics/transport.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="activatable" type="bool" value="false"/>
   <property name="amount" type="int" value="1"/>
   <property name="amount_max" type="int" value="1"/>
   <property name="category" value=""/>
   <property name="duration" type="float" value="0"/>
   <property name="moveUI" type="bool" value="false"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/item.png"/>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="angularDampening" type="float" value="0"/>
   <property name="angularVelocity" type="float" value="0"/>
   <property name="bodyType" value="static"/>
   <property name="bullet" type="bool" value="false"/>
   <property name="fixedRotation" type="bool" value="false"/>
   <property name="gravityScale" type="float" value="0"/>
   <property name="linearDampening" type="float" value="0"/>
   <property name="linearVelocityX" type="float" value="0"/>
   <property name="linearVelocityY" type="float" value="0"/>
   <property name="mass" type="float" value="0"/>
   <property name="rotationalInertia" type="float" value="0"/>
   <property name="sleepAllowed" type="bool" value="true"/>
   <property name="transitionAllowed" type="bool" value="false"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/logics/box2d.png"/>
  <objectgroup draworder="index">
   <object id="1" name="up" x="16" y="0" width="96" height="32">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
   <object id="2" name="down" x="16" y="96" width="96" height="32">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
   <object id="3" name="left" x="0" y="16" width="32" height="96">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
   <object id="4" name="right" x="96" y="16" width="32" height="96">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
</tileset>
