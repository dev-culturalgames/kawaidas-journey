<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="tiled" tilewidth="512" tileheight="256" tilecount="60" columns="8">
 <grid orientation="orthogonal" width="256" height="256"/>
 <properties>
  <property name="category" value="tiled"/>
  <property name="environment" value="global_decoration"/>
  <property name="usePhysics" type="bool" value="true"/>
 </properties>
 <tile id="15">
  <properties>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="logics" value="move"/>
   <property name="move:flying" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_dark#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="16" y="48" width="112" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <properties>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="logics" value="move"/>
   <property name="move:flying" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_dark#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="48" width="128" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="17">
  <properties>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="logics" value="move"/>
   <property name="move:flying" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_dark#2.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="48" width="112" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="21">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_dark#0.png"/>
  <objectgroup draworder="index">
   <object id="4" name="ground" x="16" y="32" width="112" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="22">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_dark#1.png"/>
  <objectgroup draworder="index">
   <object id="3" name="ground" x="0" y="32" width="128" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="23">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_dark#2.png"/>
  <objectgroup draworder="index">
   <object id="4" name="ground" x="0" y="32" width="112" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="27">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_dark#0.png"/>
  <objectgroup draworder="index">
   <object id="3" name="ground" x="128" y="80">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 0,48 -128,48"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="28">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_dark#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="80" width="128" height="48">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="29">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_dark#2.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="128">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 128,0 0,-48"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="36">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_branch0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="8" y="79" width="168" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="37">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_branch1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="16" y="64" width="144" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="38">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_crown0#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="96" y="192" width="160" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="39">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_crown0#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="192" width="256" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="2" name="climbable" x="56" y="208" width="136" height="48">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="40">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_crown0#2.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="192" width="160" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="45">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_stump0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="trunk" x="56" y="0" width="136" height="96">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="46">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_top0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="32" y="240" width="192" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="47">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_trunk0.png"/>
  <objectgroup draworder="index">
   <object id="2" name="trunk" x="56" y="0" width="136" height="256">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="48">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_trunk1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="trunk" x="56" y="0" width="136" height="256">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="49">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_blue#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="32" width="256" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="50">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_blue#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="32" width="180" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="51">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="32" width="128" height="96">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="52">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="32" width="128" height="96">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="53">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#2.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="32" width="128" height="96">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="54">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#3.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="0" width="128" height="128">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="55">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#4.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="0" width="128" height="128">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="56">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#5.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="0" width="128" height="128">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="57">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#6.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="0" width="128" height="128">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="58">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#7.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="0" width="128" height="128">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="59">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#8.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="0" width="128" height="128">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="60">
  <properties>
   <property name="onTrigger" value="Kawaida:detach"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_branch2#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="104" y="64" width="144" height="32">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="68">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_curve0#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="118.667" y="2">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
    <polygon points="9.333,-2 137.333,70 137.333,94 -14.667,-2"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="69">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_curve0#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="78">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
    <polygon points="0,-6 256,74 256,98 0,18"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="70">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_curve0#2.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="150">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
    <polygon points="0,0 256,48 256,74 0,26"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="71">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_dia0#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="170.667">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
    <polygon points="0,-26.667 112,85.333 128,85.333 0,-42.667"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="72">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_dia0#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="128" y="0">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
    <polygon points="0,0 128,128 128,144 -16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="73">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_horizontal0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="128" width="256" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="74">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerPole0/powerPole0_bot0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="pole" x="116" y="0" width="24" height="192">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="75">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerPole0/powerPole0_mid0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="pole" x="116" y="0" width="24" height="256">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="76">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerPole0/powerPole0_top0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="pole" x="116" y="80" width="24" height="176">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
   <object id="2" name="ground" x="16" y="64" width="200" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="77">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerPole0/powerPole0_top1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="pole" x="116" y="80" width="24" height="176">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
   <object id="2" name="ground" x="16" y="64" width="200" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="78">
  <image width="255" height="256" source="../../../rawAssets/objects/decoration/tiled/powerPole0/powerPole0_top2.png"/>
  <objectgroup draworder="index">
   <object id="1" name="pole" x="116" y="80" width="24" height="176">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
   <object id="2" name="ground" x="16" y="64" width="200" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="79">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_branch2#0.png"/>
 </tile>
 <tile id="80">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_branch2#2.png"/>
 </tile>
 <tile id="81">
  <properties>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="logics" value="move"/>
   <property name="move:flying" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_light#0.png"/>
  <objectgroup draworder="index">
   <object id="2" name="ground" x="16" y="48" width="112" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="82">
  <properties>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="logics" value="move"/>
   <property name="move:flying" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_light#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="48" width="128" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="83">
  <properties>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="logics" value="move"/>
   <property name="move:flying" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_light#2.png"/>
  <objectgroup draworder="index">
   <object id="3" name="ground" x="0" y="48" width="112" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="84">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_light#0.png"/>
  <objectgroup draworder="index">
   <object id="2" name="ground" x="16" y="32" width="112" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="85">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_light#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="32" width="128" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="86">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_light#2.png"/>
  <objectgroup draworder="index">
   <object id="3" name="ground" x="0" y="32" width="112" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="87">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_light#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="128" y="80">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 0,48 -128,48"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="88">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_light#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="80" width="128" height="48">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="89">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_light#2.png"/>
  <objectgroup draworder="index">
   <object id="4" name="ground" x="0" y="128">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 128,0 0,-48"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="92">
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/tiled/palm7/palm7_crown0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="trunk" x="232" y="80" width="32" height="176">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
   <object id="2" name="ground" x="0" y="64" width="512" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="93">
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/tiled/palm7/palm7_stump0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="trunk" x="232" y="0" width="32" height="96">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="94">
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/tiled/palm7/palm7_trunk0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="trunk" x="232" y="0" width="32" height="256">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="95">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_cyan#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="32" width="256" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="96">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_cyan#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="32" width="180" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="97">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_red#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="32" width="256" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="98">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_red#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="32" width="180" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="99">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_vertical0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="wire_climb" x="116" y="0" width="24" height="256">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
</tileset>
