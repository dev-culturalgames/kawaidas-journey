<?xml version="1.0" encoding="UTF-8"?>
<tileset name="item_defs" tilewidth="259" tileheight="386" tilecount="22" columns="8">
 <grid orientation="orthogonal" width="164" height="165"/>
 <properties>
  <property name="category" value="item"/>
  <property name="logics" value="item"/>
 </properties>
 <tile id="0">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_character"/>
   <property name="item:category" value="potion"/>
   <property name="item:moveUI" type="bool" value="true"/>
   <property name="logics" value="move,item"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/banana0/banana0_icon.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="logics" value="move,item"/>
   <property name="skeleton" value="banana0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/banana0/banana0_one_icon.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="item:amount" type="int" value="5"/>
   <property name="logics" value="move,item"/>
   <property name="skeleton" value="banana0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/banana0/banana0_bunch_icon.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="animation" value="spine"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="environment" value="global_character"/>
   <property name="item:category" value="ammo"/>
   <property name="item:moveUI" type="bool" value="true"/>
   <property name="logics" value="move,item"/>
   <property name="restitution" type="float" value="0.5"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/coconut0/coconut0_icon.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="logics" value="move,item"/>
   <property name="skeleton" value="coconut0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/coconut0/coconut0_one_icon.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="item:amount" type="int" value="6"/>
   <property name="logics" value="move,item"/>
   <property name="skeleton" value="coconut0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/coconut0/coconut0_bunch_icon.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="animation" value="spine"/>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="coinCount" type="bool" value="true"/>
   <property name="environment" value="global_character"/>
   <property name="item:category" value="coin"/>
   <property name="item:moveUI" type="bool" value="true"/>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/sun0/sun0_icon.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="logics" value="move,item"/>
   <property name="move:flying" type="bool" value="true"/>
   <property name="skeleton" value="sun0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/sun0/sun0_small_icon.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="item:amount" type="int" value="10"/>
   <property name="logics" value="move,item"/>
   <property name="move:flying" type="bool" value="true"/>
   <property name="skeleton" value="sun0"/>
   <property name="useDefName" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/sun0/sun0_big_icon.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_character"/>
   <property name="item:category" value="continue"/>
   <property name="logics" value="move,item"/>
   <property name="moveUI" type="bool" value="false"/>
   <property name="use_in_ui" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/totem0/totem0_icon.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_character"/>
   <property name="item:activatable" type="bool" value="true"/>
   <property name="item:category" value="powerUp"/>
   <property name="item:duration" type="float" value="4"/>
   <property name="logics" value="move,item"/>
   <property name="moveUI" type="bool" value="false"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/karafuu0/karafuu0_icon.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_character"/>
   <property name="item:activatable" type="bool" value="true"/>
   <property name="item:category" value="coinMagnet"/>
   <property name="item:duration" type="float" value="6"/>
   <property name="logics" value="move,item"/>
   <property name="moveUI" type="bool" value="false"/>
   <property name="range" type="float" value="128"/>
   <property name="view" type="float" value="512"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/solarPanel0/solarPanel0_icon.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="attack:ranged" type="bool" value="true"/>
   <property name="item:category" value="projectile"/>
   <property name="logics" value="move,attack,item,coco"/>
   <property name="move:flying" type="bool" value="false"/>
   <property name="move:isBullet" type="bool" value="true"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="skeleton" value="coconut0"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/coconut0/coconut0_shell_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_ranged" x="104" y="104" width="48" height="48">
    <properties>
     <property name="box2d:category" value="danger,solid"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="14">
  <properties>
   <property name="use_in_ui" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/banana0/banana0_ui_icon.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="use_in_ui" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/sun0/sun0_ui_icon.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="use_in_ui" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/coconut0/coconut0_ui_icon.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_character"/>
   <property name="range" type="float" value="0"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/climbHere0/climbHere0_icon.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_character"/>
   <property name="item:category" value="secret"/>
   <property name="visible" type="bool" value="true"/>
  </properties>
  <image width="259" height="386" source="../../../rawAssets/objects/item/secret0/secret0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="collect" x="0" y="96.5" width="259" height="289.5">
    <properties>
     <property name="box2d:category" value="trigger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="19">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_character"/>
   <property name="fromSet" type="bool" value="true"/>
  </properties>
  <image width="259" height="259" source="../../../rawAssets/objects/item/starfruit0/starfruit0_icon.png"/>
 </tile>
 <tile id="23">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="item:category" value="tool"/>
   <property name="moveUI" type="bool" value="false"/>
  </properties>
  <image width="128" height="256" source="../../../rawAssets/objects/item/extinguisher_icon.png"/>
 </tile>
 <tile id="24">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="item:category" value="tool"/>
   <property name="moveUI" type="bool" value="false"/>
  </properties>
  <image width="192" height="192" source="../../../rawAssets/objects/item/trashRod_icon.png"/>
 </tile>
 <tile id="25">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_character"/>
   <property name="item:category" value="note"/>
   <property name="logics" value="move,item,note"/>
   <property name="skeletonDef" type="bool" value="true"/>
   <property name="use_in_ui" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/notes0/notes0_icon.png"/>
 </tile>
</tileset>
