<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="collision_tiles256" tilewidth="256" tileheight="256" spacing="4" margin="4" tilecount="80" columns="8">
 <properties>
  <property name="category" value="tiled"/>
  <property name="usePhysics" type="bool" value="true"/>
 </properties>
 <image source="../../../rawAssets/tiles/collision_tiles256.png" width="2084" height="2604"/>
 <tile id="0">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="solid" x="256" y="192">
    <polyline points="0,0 -64,64 0,64 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="4"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="5"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="6"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="7"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="8"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="9"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="10"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="11"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" name="solid" x="0" y="128" width="256" height="128"/>
  </objectgroup>
 </tile>
 <tile id="11">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="12"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="13"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="14"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="15"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="16"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="17"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="18"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="19"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="20"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="21"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="22"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="23"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="24"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="25"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="26"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="27"/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="28"/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="29"/>
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="30"/>
  </properties>
 </tile>
 <tile id="30">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="31"/>
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="32"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="33"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="34"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="35"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="36"/>
  </properties>
 </tile>
 <tile id="36">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="37"/>
  </properties>
 </tile>
 <tile id="37">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="38"/>
  </properties>
 </tile>
 <tile id="38">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="39"/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="40"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="41"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="42"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="43"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="44"/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="45"/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="46"/>
  </properties>
 </tile>
 <tile id="46">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="47"/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="48"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="49"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="50"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" name="solid" x="0" y="0" width="256" height="256"/>
  </objectgroup>
 </tile>
 <tile id="50">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="51"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="52"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="53"/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="54"/>
  </properties>
 </tile>
 <tile id="54">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="55"/>
  </properties>
 </tile>
 <tile id="55">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="56"/>
  </properties>
 </tile>
 <tile id="56">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="57"/>
  </properties>
 </tile>
 <tile id="57">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="58"/>
  </properties>
 </tile>
 <tile id="58">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="59"/>
  </properties>
 </tile>
 <tile id="59">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="60"/>
  </properties>
 </tile>
 <tile id="60">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="61"/>
  </properties>
 </tile>
 <tile id="61">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="62"/>
  </properties>
 </tile>
 <tile id="62">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="63"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="64"/>
  </properties>
 </tile>
 <tile id="64">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="65"/>
  </properties>
 </tile>
 <tile id="65">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="66"/>
  </properties>
 </tile>
 <tile id="66">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="67"/>
  </properties>
 </tile>
 <tile id="67">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="68"/>
  </properties>
 </tile>
 <tile id="68">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="69"/>
  </properties>
 </tile>
 <tile id="69">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="70"/>
  </properties>
 </tile>
 <tile id="70">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="71"/>
  </properties>
 </tile>
 <tile id="71">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="72"/>
  </properties>
 </tile>
 <tile id="72">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="73"/>
  </properties>
 </tile>
 <tile id="73">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="74"/>
  </properties>
 </tile>
 <tile id="74">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="75"/>
  </properties>
 </tile>
</tileset>
