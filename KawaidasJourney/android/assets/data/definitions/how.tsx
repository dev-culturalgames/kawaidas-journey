<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="how" tilewidth="1024" tileheight="1536" tilecount="39" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <properties>
  <property name="category" value="decoration"/>
  <property name="environment" value="global_how"/>
 </properties>
 <tile id="0">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="512" height="256" source="../../../rawAssets/sprites/global/how/how_balkony.png"/>
  <objectgroup draworder="index">
   <object id="2" x="0" y="192">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 512,0 512,-64 0,-64"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="768" height="256" source="../../../rawAssets/sprites/global/how/how_balkony_end.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="192">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 672,0 672,-64 0,-64"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="768" height="256" source="../../../rawAssets/sprites/global/how/how_balkony_start.png"/>
  <objectgroup draworder="index">
   <object id="1" x="96" y="192">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 672,0 672,-64 0,-64"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="1024" height="1024" source="../../../rawAssets/sprites/global/how/how_cabin.png"/>
  <objectgroup draworder="index">
   <object id="1" x="128" y="512">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
    <polygon points="768,-64 768,-96 0,-96 0,-64"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <image width="512" height="1024" source="../../../rawAssets/sprites/global/how/how_decal0_wall_base.png"/>
 </tile>
 <tile id="5">
  <image width="512" height="1024" source="../../../rawAssets/sprites/global/how/how_decal0_wall_base_start.png"/>
 </tile>
 <tile id="6">
  <image width="512" height="768" source="../../../rawAssets/sprites/global/how/how_decal0_wall_f1.png"/>
 </tile>
 <tile id="7">
  <image width="512" height="768" source="../../../rawAssets/sprites/global/how/how_decal0_wall_f1_start.png"/>
 </tile>
 <tile id="8">
  <image width="512" height="512" source="../../../rawAssets/sprites/global/how/how_decal0_wall_f2.png"/>
 </tile>
 <tile id="9">
  <image width="512" height="512" source="../../../rawAssets/sprites/global/how/how_decal0_wall_f2_start.png"/>
 </tile>
 <tile id="10">
  <image width="512" height="1024" source="../../../rawAssets/sprites/global/how/how_decal1_wall_base.png"/>
 </tile>
 <tile id="11">
  <image width="512" height="768" source="../../../rawAssets/sprites/global/how/how_decal1_wall_f1.png"/>
 </tile>
 <tile id="12">
  <image width="512" height="768" source="../../../rawAssets/sprites/global/how/how_decal1_wall_f1_start.png"/>
 </tile>
 <tile id="13">
  <image width="512" height="512" source="../../../rawAssets/sprites/global/how/how_decal1_wall_f2.png"/>
 </tile>
 <tile id="14">
  <image width="512" height="256" source="../../../rawAssets/sprites/global/how/how_fence.png"/>
 </tile>
 <tile id="15">
  <image width="512" height="256" source="../../../rawAssets/sprites/global/how/how_fence_start.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/how/how_path.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="384">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 512,0 512,-192 0,-192"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="17">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/sprites/global/how/how_path_stairs.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="384">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 768,0 768,-192 480,-192"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="18">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/sprites/global/how/how_path_start.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="384">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 768,0 768,-192 0,-192"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="19">
  <image width="256" height="1024" source="../../../rawAssets/sprites/global/how/how_pillar_base.png"/>
 </tile>
 <tile id="20">
  <image width="256" height="1024" source="../../../rawAssets/sprites/global/how/how_pillar_f1.png"/>
 </tile>
 <tile id="21">
  <image width="256" height="768" source="../../../rawAssets/sprites/global/how/how_pillar_f2.png"/>
 </tile>
 <tile id="22">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/how/how_roof.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="384">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 512,0 512,-256 0,-256"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="23">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="1024" height="512" source="../../../rawAssets/sprites/global/how/how_roof_start.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="384">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
    <polygon points="0,0 1024,0 1024,-256 512,-256"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="24">
  <image width="768" height="1536" source="../../../rawAssets/sprites/global/how/how_tower.png"/>
 </tile>
 <tile id="25">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="768" height="256" source="../../../rawAssets/sprites/global/how/how_tower_f1.png"/>
  <objectgroup draworder="index">
   <object id="2" x="96" y="128" width="576" height="32">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="26">
  <properties>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="768" height="256" source="../../../rawAssets/sprites/global/how/how_tower_f2.png"/>
  <objectgroup draworder="index">
   <object id="1" x="96" y="128" width="576" height="32">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="27">
  <image width="768" height="512" source="../../../rawAssets/sprites/global/how/how_tower_side.png"/>
 </tile>
 <tile id="28">
  <image width="512" height="1024" source="../../../rawAssets/sprites/global/how/how_wall_base.png"/>
 </tile>
 <tile id="29">
  <image width="512" height="1024" source="../../../rawAssets/sprites/global/how/how_wall_base_start.png"/>
 </tile>
 <tile id="30">
  <image width="512" height="768" source="../../../rawAssets/sprites/global/how/how_wall_f1.png"/>
 </tile>
 <tile id="31">
  <image width="512" height="768" source="../../../rawAssets/sprites/global/how/how_wall_f1_start.png"/>
 </tile>
 <tile id="32">
  <image width="512" height="512" source="../../../rawAssets/sprites/global/how/how_wall_f2.png"/>
 </tile>
 <tile id="33">
  <image width="512" height="512" source="../../../rawAssets/sprites/global/how/how_wall_f2_start.png"/>
 </tile>
 <tile id="34">
  <image width="512" height="256" source="../../../rawAssets/sprites/global/how/how_wall_floor.png"/>
 </tile>
 <tile id="35">
  <image width="768" height="256" source="../../../rawAssets/sprites/global/how/how_wall_floor_start.png"/>
 </tile>
 <tile id="36">
  <image width="256" height="512" source="../../../rawAssets/sprites/global/how/how_window_base_f1.png"/>
 </tile>
 <tile id="37">
  <image width="256" height="256" source="../../../rawAssets/sprites/global/how/how_window_f1.png"/>
 </tile>
 <tile id="38">
  <image width="256" height="256" source="../../../rawAssets/sprites/global/how/how_window_f2.png"/>
 </tile>
</tileset>
