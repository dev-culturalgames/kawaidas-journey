<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="character" tilewidth="1344" tileheight="1024" tilecount="23" columns="8">
 <grid orientation="orthogonal" width="650" height="640"/>
 <properties>
  <property name="category" value="character"/>
  <property name="usePhysics" type="bool" value="false"/>
 </properties>
 <tile id="0">
  <properties>
   <property name="animation" value="spine"/>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="move,bonusCloud"/>
   <property name="move:flying" type="bool" value="true"/>
   <property name="move:speedX" type="float" value="5"/>
   <property name="move:speedY" type="float" value="5"/>
   <property name="useOrientation" type="bool" value="false"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="512" height="256" source="../../../rawAssets/objects/character/beamCloud0/beamCloud0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="26" y="128" width="464" height="32">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="2" name="return_trigger" x="128" y="128" width="256" height="32">
    <properties>
     <property name="box2d:category" value="trigger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <properties>
   <property name="animation" value="spine"/>
   <property name="animsAdd">idle:idle_ghost,idle_steam
walk:idle_ghost,idle_steam
startAttack:idle_ghost,attack_steam,attack_shovel
run:idle_ghost,run_steam,attack_shovel
damage:idle_steam
die:die_ghost
dead:dead_ghost
attack:attack_heavy,attack_steam
brake:idle_steam,idle_ghost</property>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="earthBound" type="bool" value="true"/>
   <property name="environment" value="global_character"/>
   <property name="life:damageTypes" value="ranged"/>
   <property name="life:despawnOnDeath" type="bool" value="false"/>
   <property name="life:hitpoints" type="int" value="5"/>
   <property name="life:hitpoints_max" type="int" value="5"/>
   <property name="logics" value="move,attack,life,steamGhost"/>
   <property name="move:speedX" type="float" value="4.5"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="steamGhost:chaseTime" type="float" value="5"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="768" height="640" source="../../../rawAssets/objects/character/bulldozer0/bulldozer0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="box2d_body" x="304" y="384" width="160" height="160">
    <properties>
     <property name="box2d:category" value="object"/>
    </properties>
    <ellipse/>
   </object>
   <object id="2" name="life_hitbox" x="84" y="190" width="108" height="350">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
   <object id="5" name="attack_passive" x="372" y="70" width="48" height="120">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
   <object id="6" name="chassis" x="132" y="330" width="492" height="170">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
   <object id="8" name="roof" x="120" y="190" width="300" height="140">
    <properties>
     <property name="box2d:category" value="solid"/>
    </properties>
   </object>
   <object id="9" name="attack_melee" x="456" y="310" width="192" height="193">
    <properties>
     <property name="box2d:category" value="trigger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="environment" value="global_character"/>
   <property name="life:canBlock" type="bool" value="true"/>
   <property name="life:damageTypes" value="ranged"/>
   <property name="logics" value="move,life,attack,crab"/>
   <property name="move:speedX" type="float" value="3"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="512" height="256" source="../../../rawAssets/objects/character/crab0/crab0_icon.png"/>
  <objectgroup draworder="index">
   <object id="13" name="attack_passive" x="96" y="128" width="320" height="96">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
   <object id="9" name="box2d_body" x="224" y="160" width="64" height="64">
    <properties>
     <property name="box2d:category" value="object"/>
    </properties>
    <ellipse/>
   </object>
   <object id="10" name="life_hitbox" x="128" y="48" width="256" height="128">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
   <object id="11" name="attack_passive" x="0" y="32" width="128" height="96">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
   <object id="12" name="attack_passive" x="384" y="32" width="128" height="96">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="move,life,attack,crow"/>
   <property name="move:flying" type="bool" value="true"/>
   <property name="move:speedX" type="float" value="2"/>
   <property name="move:speedY" type="float" value="2"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="range" type="float" value="512"/>
   <property name="usePhysics" type="bool" value="true"/>
   <property name="view" type="float" value="768"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/character/crow0/crow0_icon.png"/>
  <objectgroup draworder="index">
   <object id="2" name="attack_passive" x="64" y="128" width="128" height="32">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
   <object id="3" name="life_hitbox" x="32" y="64" width="192" height="42">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
   <object id="1" name="box2d_body" x="100" y="100" width="64" height="64">
    <properties>
     <property name="box2d:category" value="object"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="trampoline,bonus"/>
   <property name="trampoline:allowMove" type="bool" value="false"/>
   <property name="trampoline:singleUse" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="384" height="320" source="../../../rawAssets/objects/character/hippo0/hippo0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="trampoline_trigger" x="96" y="120" width="240" height="80">
    <properties>
     <property name="box2d:category" value="trigger"/>
    </properties>
   </object>
   <object id="2" name="bonus_trigger" x="144" y="120" width="144" height="40">
    <properties>
     <property name="box2d:category" value="trigger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="8">
  <properties>
   <property name="animation" value="spine"/>
   <property name="emotional" type="bool" value="true"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="character,tutor"/>
  </properties>
  <image width="384" height="384" source="../../../rawAssets/objects/character/leopard0/leopard0_icon.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="environment" value="global_character"/>
   <property name="life:despawn_delay" type="float" value="3"/>
   <property name="logics" value="move,attack,life,mosquito"/>
   <property name="move:flying" type="bool" value="true"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="range" type="float" value="500"/>
   <property name="usePhysics" type="bool" value="true"/>
   <property name="view" type="float" value="750"/>
  </properties>
  <image width="256" height="200" source="../../../rawAssets/objects/character/tigerMosquito0/tigerMosquito0_icon.png"/>
  <objectgroup draworder="index">
   <object id="2" name="life_hitbox" x="64" y="37.5" width="128" height="87.5">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
   <object id="3" name="attack_passive" x="78" y="125" width="100" height="25">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
   <object id="1" name="box2d_body" x="96" y="46" width="62" height="62">
    <properties>
     <property name="box2d:category" value="object"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="10">
  <properties>
   <property name="animation" value="spine"/>
   <property name="box2d:bodyType" value="static"/>
   <property name="environment" value="global_character"/>
   <property name="life:damageTypes" value="ranged"/>
   <property name="life:despawnOnDeath" type="bool" value="false"/>
   <property name="logics" value="life,trap"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="340" height="56" source="../../../rawAssets/objects/character/trap0/trap0_icon.png"/>
  <objectgroup draworder="index">
   <object id="2" name="trap_trigger" x="65" y="0" width="210" height="21">
    <properties>
     <property name="box2d:category" value="trigger"/>
    </properties>
   </object>
   <object id="1" name="life_hitbox" x="0" y="-88" width="340" height="144">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="11">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="environment" value="global_character"/>
   <property name="life:despawnOnDeath" type="bool" value="false"/>
   <property name="logics" value="move,attack,plasticBag,trash,life"/>
   <property name="move:flying" type="bool" value="true"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="plasticBag:rangeX" type="float" value="256"/>
   <property name="plasticBag:rangeY" type="float" value="256"/>
   <property name="useOrientation" type="bool" value="false"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/character/plasticBag0/plasticBag0_icon.png"/>
  <objectgroup draworder="index">
   <object id="2" name="attack_passive" x="0" y="0" width="128" height="128">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
    <ellipse/>
   </object>
   <object id="1" name="box2d_body" x="64" y="64" width="32" height="32">
    <properties>
     <property name="box2d:category" value="object"/>
    </properties>
    <ellipse/>
   </object>
   <object id="3" name="life_hitbox" x="0" y="0" width="128" height="128">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="12">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:jump" type="bool" value="true"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="box2d:sleepAllowed" type="bool" value="false"/>
   <property name="emotional" type="bool" value="true"/>
   <property name="environment" value="global_character"/>
   <property name="life:despawnOnDeath" type="bool" value="false"/>
   <property name="move:gravityScale" type="float" value="16"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="skeletonDef" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/monkey0_icon.png"/>
  <objectgroup draworder="index">
   <object id="6" name="attack_melee" x="208" y="310" width="96" height="24">
    <properties>
     <property name="box2d:category" value="trigger,danger"/>
    </properties>
   </object>
   <object id="7" name="box2d_body" x="192" y="206" width="128" height="128">
    <properties>
     <property name="box2d:category" value="object"/>
    </properties>
    <ellipse/>
   </object>
   <object id="8" name="life_hitbox" x="208" y="230" width="96" height="72">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
   <object id="10" name="climb_start" x="320" y="206" width="16" height="128">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
   <object id="11" name="climb_top" x="288" y="190" width="64" height="16">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
   <object id="14" name="climb_bottom" x="288" y="334" width="64" height="16">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
   <object id="17" name="climb_stop_top" x="288" y="174" width="64" height="16">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
   <object id="18" name="climb_stop_bottom" x="288" y="350" width="64" height="16">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
   <object id="21" name="climb_align_right" x="336" y="206" width="16" height="128">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
   <object id="22" name="climb_align_left" x="288" y="206" width="32" height="128">
    <properties>
     <property name="box2d:category" value="side"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="13">
  <properties>
   <property name="animTimeScales">walk_noSkirt:1.5f
walk_skirt:1.5f</property>
   <property name="animation" value="spine"/>
   <property name="emotional" type="bool" value="true"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="move,jump,character"/>
   <property name="skeletonDef" type="bool" value="true"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/character/ragdoll0/ragdoll0_icon.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="move,attack,life,ant"/>
   <property name="move:speedX" type="float" value="4"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/character/ant0/siafu0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="box2d_body" x="96" y="32" width="64" height="64">
    <properties>
     <property name="box2d:category" value="object"/>
    </properties>
    <ellipse/>
   </object>
   <object id="2" name="attack_melee" x="160" y="48.5" width="38.5" height="38.5">
    <properties>
     <property name="box2d:category" value="trigger"/>
    </properties>
    <ellipse/>
   </object>
   <object id="4" name="life_jumpKill" x="32" y="0" width="160" height="46">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="15">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:isEnemy" type="bool" value="false"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="move,chicken"/>
   <property name="skeletonDef" type="bool" value="true"/>
  </properties>
  <image width="419" height="418" source="../../../rawAssets/objects/character/chicken0/chicken0_icon.png"/>
  <objectgroup draworder="index">
   <object id="5" name="box2d_body" x="104.75" y="156.75" width="157.125" height="156.75">
    <properties>
     <property name="box2d:category" value="object"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="move,attack"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/character/electricSpark0/electricSpark0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_passive" x="64" y="64" width="128" height="128">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="17">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="context:range" type="float" value="300"/>
   <property name="environment" value="global_character"/>
   <property name="fire:smoke" value="smoke0_clean0"/>
   <property name="life:growTime" type="float" value="2"/>
   <property name="life:growing" type="bool" value="true"/>
   <property name="life:hitpoints" type="int" value="1"/>
   <property name="life:hitpoints_max" type="int" value="3"/>
   <property name="logics" value="spawn,attack,life,fire"/>
   <property name="progress" type="bool" value="true"/>
   <property name="spawn:delay" type="float" value="2"/>
   <property name="spawn:delay_randomize" type="float" value="1.5"/>
   <property name="spawn:max" type="int" value="1"/>
   <property name="spawn:rangeX" type="float" value="200"/>
   <property name="spawn:rangeY" type="float" value="0"/>
   <property name="spawn:spawning" type="bool" value="false"/>
   <property name="spawn:waypoints" value="left,right"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="512" height="384" source="../../../rawAssets/objects/character/fire0/fire0_icon.png"/>
  <objectgroup draworder="index">
   <object id="2" name="attack_passive" x="118" y="312">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
    <polygon points="0,0 256,0 128,-240"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="18">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="spawn"/>
   <property name="skeletonDef" type="bool" value="true"/>
   <property name="spawn:align" value="alignTop"/>
   <property name="spawn:delay" type="float" value="1"/>
  </properties>
  <image width="384" height="384" source="../../../rawAssets/objects/character/smoke0/smoke0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_passive" x="168" y="142" width="52" height="240">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="19">
  <properties>
   <property name="animation" value="spine"/>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="environment" value="global_character"/>
   <property name="life:growTime" type="float" value="1"/>
   <property name="life:growing" type="bool" value="true"/>
   <property name="life:hitpoints" type="int" value="1"/>
   <property name="life:hitpoints_max" type="int" value="3"/>
   <property name="lifespan" type="float" value="5"/>
   <property name="logics" value="move,life,cloud"/>
   <property name="move:flying" type="bool" value="true"/>
   <property name="skeletonDef" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/character/smokeCloud0/smokeCloud0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_melee" x="75" y="32" width="64" height="64">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
    <ellipse/>
   </object>
   <object id="2" name="attack_melee" x="116" y="33" width="64" height="64">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="20">
  <properties>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="environment" value="global_character"/>
   <property name="flying" type="bool" value="true"/>
   <property name="logics" value="move,wind"/>
   <property name="move:dist" type="float" value="768"/>
   <property name="move:left" type="bool" value="true"/>
   <property name="move:speed" type="float" value="10"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/character/wind0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="wind" x="0" y="32" width="128" height="64">
    <properties>
     <property name="box2d:category" value="trigger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="22">
  <properties>
   <property name="animation" value="spine"/>
   <property name="emotional" type="bool" value="true"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="character"/>
   <property name="skeletonDef" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/colobussi0/colobussi0_icon.png"/>
 </tile>
 <tile id="23">
  <properties>
   <property name="animation" value="spine"/>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="emotional" type="bool" value="true"/>
   <property name="environment" value="global_character"/>
   <property name="logics" value="move,character"/>
   <property name="skeletonDef" type="bool" value="true"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/hyena0/hyena0_icon.png"/>
 </tile>
 <tile id="24">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="emotional" type="bool" value="true"/>
   <property name="environment" value="global_character"/>
   <property name="life:despawnOnDeath" type="bool" value="false"/>
   <property name="life:hitpoints" type="int" value="4"/>
   <property name="life:hitpoints_max" type="int" value="4"/>
   <property name="logics" value="move,attack,life,character,marabu"/>
   <property name="move:flying" type="bool" value="true"/>
   <property name="move:speedX" type="float" value="4"/>
   <property name="move:speedY" type="float" value="4"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="skeletonDef" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
   <property name="view" type="float" value="1000"/>
  </properties>
  <image width="1024" height="1024" source="../../../rawAssets/objects/character/marabu0/marabu0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="life_hitbox" x="384" y="560" width="224" height="24">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
   <object id="3" name="attack_melee" x="656" y="608" width="80" height="24">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
   <object id="5" name="box2d_body" x="464" y="672" width="84" height="84">
    <properties>
     <property name="box2d:category" value="object"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="25">
  <properties>
   <property name="animation" value="spine"/>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="emotional" type="bool" value="true"/>
   <property name="environment" value="global_character"/>
   <property name="skeletonDef" type="bool" value="true"/>
  </properties>
  <image width="1344" height="896" source="../../../rawAssets/objects/character/elephant0/elephant0_icon.png"/>
 </tile>
 <tile id="26">
  <properties>
   <property name="animation" value="spine"/>
   <property name="attack:jump" type="bool" value="true"/>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="emotional" type="bool" value="true"/>
   <property name="environment" value="global_character"/>
   <property name="life:despawnOnDeath" type="bool" value="false"/>
   <property name="move:gravityScale" type="float" value="16"/>
   <property name="move:usePhysics" type="bool" value="true"/>
   <property name="skeletonDef" type="bool" value="true"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="1024" height="512" source="../../../rawAssets/objects/character/monkey0_bike/monkey0_bike_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_melee" x="464" y="488" width="96" height="24">
    <properties>
     <property name="box2d:category" value="trigger,danger"/>
    </properties>
   </object>
   <object id="2" name="box2d_body" x="448" y="384" width="128" height="128">
    <properties>
     <property name="box2d:category" value="object"/>
    </properties>
    <ellipse/>
   </object>
   <object id="3" name="life_hitbox" x="464" y="408" width="96" height="72">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
</tileset>
