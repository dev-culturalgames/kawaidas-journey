<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="project_defs" tilewidth="256" tileheight="256" tilecount="7" columns="0">
 <grid orientation="orthogonal" width="769" height="1024"/>
 <tile id="28">
  <properties>
   <property name="buttonText" value="G-Solo"/>
   <property name="name" value="gsolo"/>
   <property name="position" type="int" value="2"/>
   <property name="url" value="http://kamakawa.com/index.html"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/icons/projects/proj_gsolo.png"/>
 </tile>
 <tile id="29">
  <properties>
   <property name="buttonText" value="Inspector Haroun"/>
   <property name="name" value="inspectaHaroun"/>
   <property name="position" type="int" value="3"/>
   <property name="url" value="https://www.instagram.com/inspector_haroun_babu/"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/icons/projects/proj_inspectaHaroun.png"/>
 </tile>
 <tile id="31">
  <properties>
   <property name="buttonText" value="Juma Nature"/>
   <property name="name" value="jumaNature"/>
   <property name="position" type="int" value="4"/>
   <property name="url" value="https://www.instagram.com/sir_nature/"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/icons/projects/proj_jumaNature.png"/>
 </tile>
 <tile id="32">
  <properties>
   <property name="buttonText" value="Matonya"/>
   <property name="name" value="matonya"/>
   <property name="position" type="int" value="5"/>
   <property name="url" value="https://www.instagram.com/matonyambili/"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/icons/projects/proj_matonya.png"/>
 </tile>
 <tile id="33">
  <properties>
   <property name="buttonText" value="Sugu"/>
   <property name="name" value="sugu"/>
   <property name="position" type="int" value="6"/>
   <property name="url" value="https://www.facebook.com/sugumr2/"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/icons/projects/proj_sugu.png"/>
 </tile>
 <tile id="35">
  <properties>
   <property name="buttonText">Deutsch-Tansanische
Partnerschaft</property>
   <property name="name" value="dtp"/>
   <property name="position" type="int" value="0"/>
   <property name="url" value="https://dtpev.de/en"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/icons/projects/proj_dtpev.png"/>
 </tile>
 <tile id="36">
  <properties>
   <property name="buttonText" value="TSE Tanzania"/>
   <property name="name" value="tse"/>
   <property name="position" type="int" value="1"/>
   <property name="url" value="https://www.facebook.com/TSEmpowerment/"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/icons/projects/proj_tse.png"/>
 </tile>
</tileset>
