<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="utility_defs" tilewidth="128" tileheight="128" tilecount="14" columns="8">
 <properties>
  <property name="category" value="object"/>
  <property name="environment" value="global_decoration"/>
  <property name="old" type="bool" value="true"/>
  <property name="visible" type="bool" value="false"/>
 </properties>
 <tile id="0">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="logics" value="dialogue"/>
   <property name="onEnd" value=""/>
   <property name="parts" value=""/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/dialogue.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="file" type="file" value=""/>
   <property name="offCommands" value="this:stop_sound"/>
   <property name="onCommands" value="this:play_sound"/>
   <property name="triggerType" value="screen"/>
   <property name="volume" type="float" value="1"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/playSFX.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="onCommands" value="this:stop_sound"/>
   <property name="triggerType" value="screen"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/stopSFX.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="onReach" value=""/>
   <property name="onSet" value=""/>
   <property name="subcategory" value="point"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/point.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="logics" value="teleport"/>
   <property name="loop" type="bool" value="true"/>
   <property name="range" type="float" value="-1"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/teleport.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="range" type="float" value="128"/>
   <property name="triggerType" value="screen"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/execute.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="file" type="file" value=""/>
   <property name="loop" type="bool" value="true"/>
   <property name="offCommands" value="this:stop_sound"/>
   <property name="onCommands" value="this:play_sound"/>
   <property name="triggerType" value="spawn"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/music.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="file" type="file" value=""/>
   <property name="loop" type="bool" value="true"/>
   <property name="offCommands" value="this:stop_amb"/>
   <property name="onCommands" value="this:play_amb"/>
   <property name="triggerType" value="screen"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient.png"/>
 </tile>
 <tile id="12">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="logics" value="hint"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/info.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="logics" value="spawn"/>
   <property name="spawning" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/spawn.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="onCommands" value="this:secret"/>
   <property name="range" type="float" value="-1"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/secret.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="light:color" type="color" value="#ff000000"/>
   <property name="light:radius" type="float" value="0"/>
   <property name="lightSource" type="bool" value="true"/>
   <property name="logics" value="light"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/pointLight.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="environment" value="global_character"/>
   <property name="light:color" type="color" value="#ff000000"/>
   <property name="light:direction" value="0:-1"/>
   <property name="light:soft" type="bool" value="false"/>
   <property name="light:softness" type="float" value="0"/>
   <property name="light:xRay" type="bool" value="false"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/sunLight.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="cam:accelerationX" type="float" value="0.4"/>
   <property name="cam:accelerationY" type="float" value="0.3"/>
   <property name="cam:edgeSnappingX" type="bool" value="true"/>
   <property name="cam:edgeSnappingY" type="bool" value="true"/>
   <property name="cam:maxX" type="float" value="0"/>
   <property name="cam:maxY" type="float" value="0"/>
   <property name="cam:minX" type="float" value="0"/>
   <property name="cam:minY" type="float" value="0"/>
   <property name="cam:modeX" value="forward_focus"/>
   <property name="cam:modeY" value="forward_focus"/>
   <property name="cam:offsetBreak" type="float" value="128"/>
   <property name="cam:offsetX" type="float" value="128"/>
   <property name="cam:offsetXCatchMax" type="float" value="576"/>
   <property name="cam:offsetXCatchMin" type="float" value="576"/>
   <property name="cam:offsetXLock" type="float" value="0"/>
   <property name="cam:offsetY" type="float" value="32"/>
   <property name="cam:offsetYCatchMax" type="float" value="256"/>
   <property name="cam:offsetYCatchMin" type="float" value="256"/>
   <property name="cam:offsetYLock" type="float" value="0"/>
   <property name="cam:platformOffset" type="float" value="128"/>
   <property name="cam:platformSnapping" type="bool" value="true"/>
   <property name="cam:speedXMax" type="float" value="4"/>
   <property name="cam:speedYMax" type="float" value="6"/>
   <property name="cam:zoom" type="float" value="1"/>
   <property name="environment" value="global_character"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/camera.png"/>
 </tile>
</tileset>
