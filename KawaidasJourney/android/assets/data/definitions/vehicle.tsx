<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="vehicle_defs" tilewidth="1088" tileheight="640" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <properties>
  <property name="category" value="object"/>
  <property name="logics" value="vehicle"/>
  <property name="usePhysics" type="bool" value="true"/>
 </properties>
 <tile id="2">
  <properties>
   <property name="animation" value="spine"/>
   <property name="body" value="kinematic"/>
   <property name="environment" value="global_vehicle"/>
   <property name="logics" value="move,jump,vehicle"/>
   <property name="skeletonDef" type="bool" value="true"/>
   <property name="subcategory" value="vehicle"/>
  </properties>
  <image width="1024" height="384" source="../../../rawAssets/objects/vehicle/bus0/bus0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="roof" x="64" y="60" width="768" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="3" name="attack_melee" x="832" y="96" width="32" height="240">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <properties>
   <property name="animation" value="spine"/>
   <property name="body" value="kinematic"/>
   <property name="environment" value="global_vehicle"/>
   <property name="logics" value="move,jump,vehicle"/>
   <property name="skeletonDef" type="bool" value="true"/>
   <property name="subcategory" value="vehicle"/>
  </properties>
  <image width="512" height="384" source="../../../rawAssets/objects/vehicle/motorbike0/motorbike0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="roof" x="16" y="180" width="368" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <properties>
   <property name="animation" value="spine"/>
   <property name="body" value="kinematic"/>
   <property name="environment" value="global_vehicle"/>
   <property name="logics" value="move,jump,vehicle"/>
   <property name="skeletonDef" type="bool" value="true"/>
   <property name="subcategory" value="vehicle"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/vehicle/motorbike1/motorbike1_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="roof" x="96.25" y="76.3125" width="476" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="2" name="ground" x="62" y="289.125" width="596" height="32">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="5">
  <properties>
   <property name="animation" value="spine"/>
   <property name="body" value="kinematic"/>
   <property name="environment" value="global_vehicle"/>
   <property name="logics" value="move,jump,vehicle"/>
   <property name="move:speedX" type="float" value="4"/>
   <property name="skeletonDef" type="bool" value="true"/>
   <property name="subcategory" value="vehicle"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/transporter0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="roof" x="71" y="149" width="782" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="2" name="ground" x="85" y="375" width="884" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="3" name="bumper_trigger" x="816" y="320" width="160" height="160">
    <properties>
     <property name="box2d:category" value="trigger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
</tileset>
