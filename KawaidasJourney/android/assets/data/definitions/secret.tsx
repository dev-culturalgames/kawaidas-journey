<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="secret" tilewidth="512" tileheight="512" tilecount="46" columns="0">
 <grid orientation="orthogonal" width="769" height="1024"/>
 <tile id="1">
  <properties>
   <property name="encyText0" value="Unlike most other types of mosquitos, the tiger or forest mosquito typically flies and feeds in the daytime. Mosquitos are among the most dangerous animals in East Africa because of their disease transmission risk. Don't let them bite you!"/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Aedes_albopictus"/>
   <property name="name" value="mosquito"/>
   <property name="position" type="int" value="1"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="characters"/>
   <property name="subtitle" value="New enemy discovered"/>
   <property name="text0" value="Mosquitos are among the most dangerous animals in East Africa because of their disease transmission risk."/>
   <property name="title" value="Tiger mosquito"/>
   <property name="titleSW" value="Mbu"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/characters/secret_characters_mosq.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="encyText0">There is an average of 7 hours and 45 min of sunlight per day in Tanzania. In comparison there is just an average of 4:45 of sunlight per day in Germany. The sun is a cheap and powerful source of energy! Sun means power! Collect suns to get powerups in the shop!

Find out more about solar power:
</property>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Solar_power"/>
   <property name="name" value="sun"/>
   <property name="position" type="int" value="1"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="objects"/>
   <property name="subtitle" value="New collectable discovered"/>
   <property name="text0" value="Sun means power! Collect suns to get powerups in the shop!"/>
   <property name="title" value="Sun"/>
   <property name="titleSW" value="Jua"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/objects/secret_objects_sun.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="encyText0" value="The Steam Ghost is the embodiment of greed and profit. He doesn't care about the locals and the environment. The Steam Ghost is focused on destroying."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Deforestation"/>
   <property name="name" value="steamGhost"/>
   <property name="position" type="int" value="4"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="characters"/>
   <property name="subtitle" value="New boss discovered"/>
   <property name="text0" value="The Steam Ghost is the embodiment of greed and profit. He doesn't care about the locals and the environment. Throw a coconut at his back to destroy his bulldozer!"/>
   <property name="title" value="Steam Ghost Bulldozer"/>
   <property name="titleSW" value="Majangiri"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/characters/secret_characters_bulldozer.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="encyText0" value="Bananas are a rich source of vitamin B6 and contain vitamin C and dietary fiber. This makes bananas one of the healthiest foods. There are more than 1000 different types of banana plants in the world. In Swahili the sweet banana tree is called MGOMBA. Maximum amount to carry can be upgraded in the ingame shop."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Banana"/>
   <property name="name" value="banana"/>
   <property name="position" type="int" value="2"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="objects"/>
   <property name="subtitle" value="New collectable discovered"/>
   <property name="text0" value="Bananas mean life!! Collect bananas to survive enemy attacks!"/>
   <property name="title" value="Sweet banana"/>
   <property name="titleSW" value="Ndizi"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/objects/secret_objects_banana.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="encyText0" value="The Baobab is the most widespread of the Adansonia species, and is native to the African continent. The long-lived tree is typically found in dry, hot savannahs of sub-Saharan Africa. Baobabs can store up to 120 thousand litres of water in its trunk to endure harsh drought conditions. The Baobab fruits make tasty sweets."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Adansonia"/>
   <property name="name" value="baobab"/>
   <property name="position" type="int" value="0"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="objects"/>
   <property name="subtitle" value="Local plant discovered"/>
   <property name="text0" value="The Baobab tree is native to the African continent. Baobabs can store up to 120 thousand litres of water in its trunk and its fruits make tasty sweets."/>
   <property name="title" value="Baobab tree"/>
   <property name="titleSW" value="Mbuyu"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/objects/secret_objects_baobab.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="encyText0" value="Coconuts are known for their great versatility as evidenced by many traditional uses. Ranging from food to cosmetics. They are a staple food for many people living in the tropics/ subtropics. In the traditional fishing villages of Zanzibar a rope called KAMBA is made out of dried coconut hair. A full-sized coconut weighs about 1.44 kg, has a hard shell and makes a dangerous weapon in the hand of little heroes.  Maximum amount to carry can be upgraded in the ingame shop."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Coconut"/>
   <property name="name" value="coconut"/>
   <property name="position" type="int" value="3"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="objects"/>
   <property name="subtitle" value="New collectable discovered"/>
   <property name="text0" value="A full-sized coconut weighs about 1.44 kg, has a hard shell and makes a dangerous weapon in the hand of little heroes."/>
   <property name="title" value="Coconuts"/>
   <property name="titleSW" value="Minazi"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/objects/secret_objects_cocos.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="encyText0" value="The coconut crab is a species of terrestrial hermit crab, also known as the robber crab or palm thief. It is the largest land-living arthropod in the world. Its colour ranges from orange-red to purplish blue. The coconut crab has a thick amour and dangerous claws. In East Africa its population is on the verge of extinction."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Coconut_crab"/>
   <property name="name" value="crab"/>
   <property name="position" type="int" value="2"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="characters"/>
   <property name="subtitle" value="New enemy discovered"/>
   <property name="text0" value="The coconut crab has a thick amour and dangerous claws. Throw a coconut to defeat it!"/>
   <property name="title" value="Coconut crab"/>
   <property name="titleSW" value="Kaa"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/characters/secret_characters_crab.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="encyText0">This magic Hippo is your friend. If you tickle the nose of this nervous character, it catapults you into a bonus area, before it goes back to sleep. This hippo is dressed in tinga-tinga-art style, thats where its magic comes from.

Find out more about the real hippopotamus:</property>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Hippopotamus"/>
   <property name="name" value="hippo"/>
   <property name="position" type="int" value="3"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="characters"/>
   <property name="subtitle" value="New character discovered"/>
   <property name="text0" value="This magic Hippo is your friend. If you tickle the nose of this nervous character, it catapults you into a bonus area, before it goes back to sleep."/>
   <property name="title" value="Magic hippo"/>
   <property name="titleSW" value="Kiboko ajabu"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/characters/secret_characters_hippo.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="encyText0" value="The TUFAHAMIANE women group oppose the steadily ongoing cutting down of the local forest by planting palm trees and other fast growing trees for a sustainable future. The project is carried out in Kizimkazi Mkunguni, in the south west of the Zanzibar island."/>
   <property name="encyUrl" value="https://dtpev.de/storage/app/media/palmtree_flyer_2.pdf"/>
   <property name="name" value="plantPalmtree"/>
   <property name="position" type="int" value="0"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="topics"/>
   <property name="subtitle" value="Local project discovered"/>
   <property name="text0" value="The TUFAHAMIANE women group oppose the steadily ongoing cutting down of the local forest by planting fast growing trees for a sustainable future."/>
   <property name="title" value="PLANT YOUR PALMTREE"/>
   <property name="titleSW" value="Kupanda mtende yako"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/topics/secret_topics_plant_palmtree.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="encyText0" value="Extensive wood cutting has dramatically depleted the forests. In Tanzania the forests declined by 20% in the last two decades. Soil erosion, less rain, loss of biodiversity and CO2 compensation are the consequences. But reforestation is easy. Get active and plant a tree yourself! You make the difference!"/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Deforestation"/>
   <property name="name" value="deforestation"/>
   <property name="position" type="int" value="1"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="topics"/>
   <property name="subtitle" value="New topic discovered"/>
   <property name="text0" value="Soil erosion, less rain, loss of biodiversity and CO2 compensation are the consequences of extensive wood cutting. Reforestation is the key. Get active and plant a tree yourself!"/>
   <property name="title" value="Deforestation"/>
   <property name="titleSW" value="Usambazaji wa miti"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/topics/secret_topics_deforestation.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="encyText0">This is an ancient artifact of mystery! It incorporates the spirits of the dead or in Swahili so-called SHETANI. Use it to rise again from death and continue your journey! Maximum amount to carry can be upgraded in the ingame shop.

Find out more about East African mythology:</property>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Shetani"/>
   <property name="name" value="totem"/>
   <property name="position" type="int" value="4"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="objects"/>
   <property name="subtitle" value="New collectable discovered"/>
   <property name="text0" value="This is an ancient artifact of mystery! Use it to rise again from death and continue your journey!"/>
   <property name="title" value="Totem"/>
   <property name="titleSW" value="Sanamu"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/objects/secret_objects_totem.png"/>
 </tile>
 <tile id="12">
  <properties>
   <property name="encyText0" value="This magical character is the last Zanzibar Leopard - he is a true legend. In addition he is your friend and guides you on your journey. The Zanzibar leopard is smaller than his mainland conspecifics. Hunted because of the danger it poses for livestock and it's associations with witchcraft, the Zanzibar leopard has almost died out."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Zanzibar_leopard"/>
   <property name="name" value="leopard"/>
   <property name="position" type="int" value="0"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="characters"/>
   <property name="subtitle" value="New character discovered"/>
   <property name="text0" value="This magical character is the last Zanzibar Leopard - he is a true legend. In addition he is your friend and guides you on your journey."/>
   <property name="title" value="Zanzibar leopard"/>
   <property name="titleSW" value="Chui zanzibari"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/characters/secret_characters_leo.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="encyText0">MAMBO VIPI is the Tanzanian way to say hello to your friends! One of the most common answers is POA. POA means everything is cool.

Learn Swahili!</property>
   <property name="encyUrl" value="http://africanlanguages.com/swahili/index.php"/>
   <property name="name" value="mamboVipi"/>
   <property name="position" type="int" value="1"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="swahili"/>
   <property name="subtitle" value="New Swahili expression discovered"/>
   <property name="text0" value="MAMBO VIPI is the Tanzanian way to say hello to your friends! One of the most common answers is POA. POA means everything is cool."/>
   <property name="title" value="Hello"/>
   <property name="titleSW" value="Mambo vipi"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/kiswhahili/secret_kiswahili_mambo_vipi.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="encyText0">SHIKAMO literally means I hold your feet. It's the respectful greeting for the elders. The formal reply is: MARAHABA. Meaning something like I am delighted. BIBI is a kind expression of grandmother.

Learn Swahili!</property>
   <property name="encyUrl" value="http://africanlanguages.com/swahili/index.php"/>
   <property name="name" value="shikamo"/>
   <property name="position" type="int" value="2"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="swahili"/>
   <property name="subtitle" value="New Swahili expression discovered"/>
   <property name="text0" value="SHIKAMO literally means I hold your feet. It's the respectful greeting for the elders. The formal reply is: MARAHABA. Meaning something like I am delighted."/>
   <property name="title" value="Respectful greeting"/>
   <property name="titleSW" value="Shikamo"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/kiswhahili/secret_kiswahili_shikamo.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="encyText0">ASANTE means thank you. ASANTE SANA means thank you very much. The reply to ASANTE is KARIBU meaning you are welcome.

Learn Swahili:</property>
   <property name="encyUrl" value="http://africanlanguages.com/swahili/index.php"/>
   <property name="name" value="asante"/>
   <property name="position" type="int" value="3"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="swahili"/>
   <property name="subtitle" value="New Swahili expression discovered"/>
   <property name="text0" value="ASANTE means thank you. ASANTE SANA means thank you very much."/>
   <property name="title" value="Thank you"/>
   <property name="titleSW" value="Asante"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/kiswhahili/secret_kiswahili_asante.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="encyText0" value="Dorylus, also known as driver ants or safari ants, is a large genus of army ants found primarily in central and east Africa. They are tiny, but very dangerous! They have powerful shearing jaws and likely attack in groups! That's why the Swahili people call them ASKARI QUA MIGU - foot soldiers!"/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Dorylus"/>
   <property name="name" value="siafu"/>
   <property name="position" type="int" value="5"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="characters"/>
   <property name="subtitle" value="New enemy discovered"/>
   <property name="text0" value="They are tiny, but they have powerful jaws and likely attack in groups! Be careful, if you defeat one, others will run to you."/>
   <property name="title" value="Driver ants"/>
   <property name="titleSW" value="Siafu"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/characters/secret_characters_siafu.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="encyText0" value="The Jozani forest is the only national park on the island of Zanzibar. Even if it is one of the smallest national parks in Tanzania, it shows a great biodiversity of insects, animals and plants, like mangrove trees. It is the only location where you can find the Zanzibar Red Colobus monkey."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Jozani_Chwaka_Bay_National_Park"/>
   <property name="name" value="jozani"/>
   <property name="position" type="int" value="0"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="places"/>
   <property name="subtitle" value="New location discovered"/>
   <property name="text0" value="The Jozani forest shows a great biodiversity of insects, animals and plants. It is the only location where you can find the Zanzibar Red Colobus monkey."/>
   <property name="title" value="Jozani forest"/>
   <property name="titleSW" value="Msitu ya Jozani"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/places/secret_places_jozani.png"/>
 </tile>
 <tile id="20">
  <properties>
   <property name="encyText0" value="The Zanzibar Red Colobus monkey is endemic to Zanzibar Island living in the Jozani National Park. The Colobus monkeys are very social animals living in groups consist up to 50 individuals. They can often be observed playing and grooming. Unfortunately the population trend is decreasing and it is now classified as an endangered species."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Zanzibar_red_colobus"/>
   <property name="name" value="colobus"/>
   <property name="position" type="int" value="6"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="characters"/>
   <property name="subtitle" value="New character discovered"/>
   <property name="text0" value="The Zanzibar Red Colobus monkeys are living in the Jozani National Park. Normally they are friendly and playful. But now they are enraged because their home forest has been destroyed. Be aware of their coconut shots!"/>
   <property name="title" value="Zanzibar Red Colobus"/>
   <property name="titleSW" value="Kima punju"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/characters/secret_characters_colobus.png"/>
 </tile>
 <tile id="21">
  <properties>
   <property name="encyText0" value="Cloves are the aromatic flower buds of a tree in the family Myrtaceae, Syzygium aromaticum. In former times Zanzibar was one of the world's leading exporter of cloves. Cloves are commonly used as a spice, but they are great painkillers, too, especially used in dental medicine. Grants invincibility for a few seconds when collected! Duration can be upgraded in the ingame shop."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Clove"/>
   <property name="name" value="karafuu"/>
   <property name="position" type="int" value="8"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="objects"/>
   <property name="subtitle" value="New collectable discovered"/>
   <property name="text0" value="Cloves are painkillers. Grants invincibility for a few seconds when collected!"/>
   <property name="title" value="Clove"/>
   <property name="titleSW" value="Karafuu"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/objects/secret_objects_karafuu.png"/>
 </tile>
 <tile id="22">
  <properties>
   <property name="encyText0" value="Plastic has become a vital part of our lives of convenience. But nearly every piece of plastic ever made still exists today. More than five trillion pieces of plastic are already in the oceans, and by 2050 there will be more plastic in the sea than fish, by weight, according to the Ellen MacArthur Foundation. Plastic trash is not biodegradable. Microplastic is found even in the air and in soil. Avoid pastic trash!"/>
   <property name="encyUrl" value="http://edition.cnn.com/interactive/2016/12/world/midway-plastic-island/"/>
   <property name="name" value="plasticTrash"/>
   <property name="position" type="int" value="2"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="topics"/>
   <property name="subtitle" value="New topic discovered"/>
   <property name="text0" value="Plastic trash is not biodegradable. Microplastic is found already everywhere: in the ocean, in the air, even in soil. Avoid pastic trash!"/>
   <property name="title" value="Plastic trash"/>
   <property name="titleSW" value="Taka plastiki"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/topics/secret_topics_trash.png"/>
 </tile>
 <tile id="23">
  <properties>
   <property name="encyText0">A solar panel collects the energy of the sun to generate electricity. For a short period of time all nearby suns will be collected automatically. Attracting range and duration can be upgraded in the ingame shop. My heart is a magnet!

Find out more about the advantages of photovoltaic energy systems:</property>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Solar_panel"/>
   <property name="name" value="solarPanel"/>
   <property name="position" type="int" value="5"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="objects"/>
   <property name="subtitle" value="New collectable discovered"/>
   <property name="text0" value="A solar panel collects the energy of the sun to generate electricity. For a short period of time all nearby suns will be collected automatically. My heart is a magnet!"/>
   <property name="title" value="Solar panel"/>
   <property name="titleSW" value="Jopo la umeme wa jua"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/objects/secret_objects_panel.png"/>
 </tile>
 <tile id="24">
  <properties>
   <property name="encyText0" value="DALA DALA cars are mini buses for public transport. They developed as illegal taxis. Nowadays most of them are being run by private contractors. DALA DALAs are the main means of transport in East African urban areas."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Dala_dala"/>
   <property name="name" value="dalaDala"/>
   <property name="position" type="int" value="7"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="objects"/>
   <property name="subtitle" value="New vehicle discovered"/>
   <property name="text0" value="DALA DALA  cars are mini buses for public transport. Jump on its roof and take a ride!"/>
   <property name="title" value="Mini van"/>
   <property name="titleSW" value="Dala dala"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/objects/secret_objects_dala.png"/>
 </tile>
 <tile id="25">
  <properties>
   <property name="encyText0">KAWAIDA literally means usually/ naturally. Often the saying KAMA KAWAIDA is used to express that everything is ok and is going on well on a daily basis.

Learn Swahili!</property>
   <property name="encyUrl" value="http://africanlanguages.com/swahili/index.php"/>
   <property name="name" value="kawaida"/>
   <property name="position" type="int" value="0"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="swahili"/>
   <property name="subtitle" value="New Swahili expression discovered"/>
   <property name="text0" value="KAWAIDA literally means usually/ naturally. Often the saying KAMA KAWAIDA is used to express that everything is ok and is going on well."/>
   <property name="title" value="Usually"/>
   <property name="titleSW" value="Kawaida"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/kiswhahili/secret_kiswahili_kawaida.png"/>
 </tile>
 <tile id="26">
  <properties>
   <property name="encyText0">KARIBU means you are welcome! If somebody knocks on your door shouting out HODI, the Swahili way of asking to come in, the response is KARIBU to welcome your guest. It is also used in response to ASANTE (thank you). In an other context KARIBU means to be close to something.

Learn Swahili!</property>
   <property name="encyUrl" value="http://africanlanguages.com/swahili/index.php"/>
   <property name="name" value="karibu"/>
   <property name="position" type="int" value="4"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="swahili"/>
   <property name="subtitle" value="New Swahili expression discovered"/>
   <property name="text0" value="KARIBU means you are welcome!"/>
   <property name="title" value="Welcome"/>
   <property name="titleSW" value="Karibu"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/kiswhahili/secret_kiswahili_karibu.png"/>
 </tile>
 <tile id="27">
  <properties>
   <property name="encyText0">HABARI YAKO stands for how are you. HABARI literally means news. So HABARI YAKO means what are the news from you. It is used to greet people. In comparison to MAMBO VIPI it is more polite and official.

Learn Swahili!</property>
   <property name="encyUrl" value="http://africanlanguages.com/swahili/index.php"/>
   <property name="name" value="habari"/>
   <property name="position" type="int" value="5"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="swahili"/>
   <property name="subtitle" value="New Swahili expression discovered"/>
   <property name="text0" value="HABARI YAKO stands for how are you. In comparison to MAMBO VIPI it is more polite and official."/>
   <property name="title" value="How are you"/>
   <property name="titleSW" value="Habari yako"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/kiswhahili/secret_kiswahili_habari.png"/>
 </tile>
 <tile id="28">
  <properties>
   <property name="encyText0" value="The Indian crow, or house crow, is an aggressive species, which occurs in urban and industrial areas. It feeds insects, eggs, nestlings, fruits and refuse around human habitations. The Indian crow was introduced to East Africa around Zanzibar about 1897."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/House_crow"/>
   <property name="name" value="crow"/>
   <property name="position" type="int" value="7"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="characters"/>
   <property name="subtitle" value="New ememy discovered"/>
   <property name="text0" value="Flying crows attack from above. They have kind of a feisty attitude. Be aware of their sharp beak!"/>
   <property name="title" value="Indian crow"/>
   <property name="titleSW" value="Kunguru"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/characters/secret_characters_crow.png"/>
 </tile>
 <tile id="29">
  <properties>
   <property name="encyText0">MJINI means town in Swahili. The diminutive KIJIJINI means village.

Learn Swahili!</property>
   <property name="encyUrl" value="http://africanlanguages.com/swahili/index.php"/>
   <property name="name" value="mjini"/>
   <property name="position" type="int" value="6"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="swahili"/>
   <property name="subtitle" value="New Swahili expression discovered"/>
   <property name="text0" value="MJINI means town in Swahili. The diminutive KIJIJINI means village."/>
   <property name="title" value="Town"/>
   <property name="titleSW" value="Mjini"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/kiswhahili/secret_kiswahili_mjini.png"/>
 </tile>
 <tile id="30">
  <properties>
   <property name="encyText0" value="The Darajani is the main market in Stone Town, Zanzibar. It is located in Darajani Road, in the surroundings of the Anglican Cathedral of Christ. The main structure of the market was built in 1904 for Sultan Ali bin Hamud. The Darajani market is known for its wide variety of typical Zanzibar spices."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Darajani_Market"/>
   <property name="name" value="darajani"/>
   <property name="position" type="int" value="1"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="places"/>
   <property name="subtitle" value="New location discovered"/>
   <property name="text0" value="The Darajani is the main market in Stone Town. It is known for its wide variety of typical Zanzibar spices."/>
   <property name="title" value="Darajani market"/>
   <property name="titleSW" value="Soko ya Darajani"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/places/secret_places_darajani.png"/>
 </tile>
 <tile id="31">
  <properties>
   <property name="encyText0" value="Stone Town is located on the western coast of Zanzibar. It is the former capital of the Zanzibar Sultanate, and flourishing centre of the spice trade. Its architecture, mostly dating back to the 19th century, reflects the diverse influences underlying the Swahili culture, giving a unique mixture of Arab, Persian, Indian and European elements."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Stone_Town"/>
   <property name="name" value="stoneTown"/>
   <property name="position" type="int" value="2"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="places"/>
   <property name="subtitle" value="New location discovered"/>
   <property name="text0" value="Stone Town is located on the western coast of Zanzibar. It is the former capital of the Zanzibar Sultanate, and flourishing centre of the spice trade."/>
   <property name="title" value="Stone Town"/>
   <property name="titleSW" value="Mji Mkongwe"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/places/secret_places_stone_town.png"/>
 </tile>
 <tile id="32">
  <properties>
   <property name="encyText0" value="Most electricity in Tanzania is generated using gas; hydropower is also a significant source of power. Only 24% of urban areas and 7% of rural areas have access to electricity. Power cuts are common due to inferior infrastructure. Decentral power supplies like solar home systems are promising alternative solutions."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Energy_in_Tanzania"/>
   <property name="name" value="electricSpark"/>
   <property name="position" type="int" value="3"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="topics"/>
   <property name="subtitle" value="New topic discovered"/>
   <property name="text0" value="Electric sparks are dangerous! Brzzzzz... brzzzz..."/>
   <property name="title" value="Electricity"/>
   <property name="titleSW" value="Umeme"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/topics/secret_topics_electric.png"/>
 </tile>
 <tile id="33">
  <properties>
   <property name="encyText0" value="Unfortunately in Tanzania are just a few public fire departments. Unable to cover all urban and rural areas. Thus fire protection is a key issue, not only in Tanzania! Don`t leave open fires such as barbecues and candles unattended! In addition fire alarms and sprinklers are crucial life-saving measures."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Fire_protection"/>
   <property name="name" value="fireProtection"/>
   <property name="position" type="int" value="5"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="topics"/>
   <property name="subtitle" value="New topic discovered"/>
   <property name="text0" value="Fire protection is a key issue, not only in Tanzania! Don`t leave open fires unattended and install fire alarms and sprinklers if possible."/>
   <property name="title" value="Fire protection"/>
   <property name="titleSW" value="Kuzima moto"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/topics/secret_topics_fire_extinguisher.png"/>
 </tile>
 <tile id="34">
  <properties>
   <property name="encyText0" value="The architecture in Stone Town is highly influenced by Arab, Persian and Indian merchants, who came to Zanzibar along the east coast of Africa. Until today this influence shapes the townscape. Especially visible are the curved (Indian) and flat (Arabic) doors in Stone Town. Nevertheless most of the doors are a mixture of these traditional styles."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Swahili_architecture"/>
   <property name="name" value="doors"/>
   <property name="position" type="int" value="4"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="topics"/>
   <property name="subtitle" value="New topic discovered"/>
   <property name="text0" value="The architecture in Stone Town is highly influenced by Arab, Persian and Indian merchants. Especially visible are the doors representing a mixture of these traditional styles."/>
   <property name="title" value="Traditional doors"/>
   <property name="titleSW" value="Milango ya kienyeji"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/topics/secret_topics_doors.png"/>
 </tile>
 <tile id="35">
  <properties>
   <property name="encyText0">SAMAHANI means Excuse me.

Learn Swahili!</property>
   <property name="encyUrl" value="http://africanlanguages.com/swahili/index.php"/>
   <property name="name" value="samahani"/>
   <property name="position" type="int" value="7"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="swahili"/>
   <property name="subtitle" value="New Swahili expression discovered"/>
   <property name="text0" value="SAMAHANI means Excuse me."/>
   <property name="title" value="Excuse me"/>
   <property name="titleSW" value="Samahani"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/kiswhahili/secret_kiswahili_samahani.png"/>
 </tile>
 <tile id="36">
  <properties>
   <property name="encyText0" value="Dar es Salaam, from Arabic: the house of peace, is the former capital as well as the largest and most populous city in Tanzania (4-5 million residents). Dar Es Salaam is the central of business, art and education in Tanzania. Located on the Swahili coast, the city is one of the fastest growing cities in the world."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Dar_es_Salaam"/>
   <property name="name" value="dar"/>
   <property name="position" type="int" value="3"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="300"/>
   <property name="secretCategory" value="places"/>
   <property name="subtitle" value="New location discovered"/>
   <property name="text0" value="Dar es Salaam, from Arabic: the house of peace, is the biggest city in Tanzania. It is the central of business, art and education."/>
   <property name="title" value="Dar Es Salaam"/>
   <property name="titleSW" value="Bongo"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/places/secret_places_dar.png"/>
 </tile>
 <tile id="37">
  <properties>
   <property name="encyText0" value="The traffic jam had become a huge problem in Dar Es Salaam due to inferior infrastructure, broken roads and rapid increase of private cars. Since 2016 a bus rapid transit system was installed in Dar Es Salaam to reduce the traffic jam."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Dar_es_Salaam_bus_rapid_transit"/>
   <property name="name" value="traffic"/>
   <property name="position" type="int" value="6"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="300"/>
   <property name="secretCategory" value="topics"/>
   <property name="subtitle" value="New topic discovered"/>
   <property name="text0" value="The traffic jam had become a huge problem in Dar Es Salaam. Since 2016 a bus rapid transit system was installed to reduce the traffic jam."/>
   <property name="title" value="Traffic jam"/>
   <property name="titleSW" value="Foleni"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/topics/secret_topics_traffic.png"/>
 </tile>
 <tile id="38">
  <properties>
   <property name="encyText0" value="These chickens have become aggressive because they ate to much poisonous trash! Take care about your waste and dispose your toxic trash correctly!"/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Waste"/>
   <property name="name" value="trashChicken"/>
   <property name="position" type="int" value="8"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="300"/>
   <property name="secretCategory" value="characters"/>
   <property name="subtitle" value="New ememy discovered"/>
   <property name="text0" value="These chickens have become aggressive because they ate to much poisonous trash! Don't get too close to them."/>
   <property name="title" value="Trash chicken"/>
   <property name="titleSW" value="Kuku wa taka"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/characters/secret_characters_chicken.png"/>
 </tile>
 <tile id="39">
  <properties>
   <property name="encyText0" value="Bongo flava is the nickname for Tanzanian rap music. It is very popular all over East Africa and part of the vibrant Swahili cultural identity. The genre developed in the 1990s, mainly as a derivative of American hip hop, with additional influences from reggae, RnB, afrobeat, dancehall and traditional Tanzanian styles such as taarab and dansi, a combination that forms a unique style of music. Lyrics are usually in Swahili."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Bongo_Flava"/>
   <property name="name" value="bongoFlava"/>
   <property name="position" type="int" value="7"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="300"/>
   <property name="secretCategory" value="topics"/>
   <property name="subtitle" value="New topic discovered"/>
   <property name="text0" value="Bongo flava is the nickname for Tanzanian rap music. It is very popular all over East Africa and part of the vibrant Swahili cultural identity."/>
   <property name="title" value="Bongo Flavour"/>
   <property name="titleSW" value="Bongo Flava"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/topics/secret_topics_bongo_flava.png"/>
 </tile>
 <tile id="42">
  <properties>
   <property name="encyText0" value="The Serengeti National Park is located in the north of Tanzania, close to the border with Kenya. It is the home of the BIG FIVE savanna animals such as elephant, lion, Cape buffalo, rhinoceros and leopard, as well as many more like hippos, hyenas and vervet monkeys. It is famous for its annual migration of over 1.5 million white-bearded wildebeest. Its ecosystem is threatened by the ongoing extension of human civilization and has been listed as a World Heritage."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Serengeti_National_Park"/>
   <property name="name" value="serengeti"/>
   <property name="position" type="int" value="4"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="400"/>
   <property name="secretCategory" value="places"/>
   <property name="subtitle" value="New location discovered"/>
   <property name="text0" value="The Serengeti is located in the north of Tanzania, close to the border with Kenya. It is the home of the BIG FIVE savanna animals such as elephant, lion, Cape buffalo, rhinoceros and leopard, as well as many more like hippos, hyenas and vervet monkeys."/>
   <property name="title" value="Serengeti National Park"/>
   <property name="titleSW" value="Hifadhi ya Serengeti"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/places/secret_places_serengeti.png"/>
 </tile>
 <tile id="43">
  <properties>
   <property name="encyText0" value="Poaching is defined as the illegal hunting of wild animals. In East Africa actually elephants are threatened by poaching. Rhinos are almost extinct. The main problem is not the hunting from traditional tribes, but the mass slaughter for illegal ivory trade."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Poaching"/>
   <property name="name" value="poaching"/>
   <property name="position" type="int" value="8"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="400"/>
   <property name="secretCategory" value="topics"/>
   <property name="subtitle" value="New topic discovered"/>
   <property name="text0" value="In East Africa actually elephants are threatened by poaching. Rhinos are almost extinct. The main problem is the mass slaughter for illegal ivory trade."/>
   <property name="title" value="Poaching"/>
   <property name="titleSW" value="Ujangili"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/topics/secret_topics_poaching.png"/>
 </tile>
 <tile id="44">
  <properties>
   <property name="encyText0">You can plant seedlings on fertile ground. Press the button near to a dead plant with a skull to reforest the environment.

Learn more about the Kawaida's Journey game project:</property>
   <property name="encyUrl" value="http://www.kawaidasjourney.de"/>
   <property name="name" value="planting"/>
   <property name="position" type="int" value="0"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="skills"/>
   <property name="subtitle" value="New ability discovered"/>
   <property name="text0" value="You can plant seedlings on fertile ground. Press the button near to a dead plant with a skull to reforest the environment."/>
   <property name="title" value="Planting seeds"/>
   <property name="titleSW" value="Kupanda mbegu"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/skills/secret_skills_plant.png"/>
 </tile>
 <tile id="45">
  <properties>
   <property name="encyText0">Use the garbage tong to pick up trash! Press the button near to the trash.

Learn more about the Kawaida's Journey game project:</property>
   <property name="encyUrl" value="http://www.kawaidasjourney.de"/>
   <property name="name" value="picking"/>
   <property name="position" type="int" value="1"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="skills"/>
   <property name="subtitle" value="New ability discovered"/>
   <property name="text0" value="Use the garbage tong to pick up trash! Press the button near to the trash."/>
   <property name="title" value="Picking up trash"/>
   <property name="titleSW" value="Chukua taka"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/skills/secret_skills_pick.png"/>
 </tile>
 <tile id="46">
  <properties>
   <property name="encyText0">You can enter traditional doors with spikes. Press the button near to the door.

Learn more about the Kawaida's Journey game project:</property>
   <property name="encyUrl" value="http://www.kawaidasjourney.de"/>
   <property name="name" value="teleport"/>
   <property name="position" type="int" value="2"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="skills"/>
   <property name="subtitle" value="New ability discovered"/>
   <property name="text0" value="You can enter traditional doors with spikes. Press the button near to the door."/>
   <property name="title" value="Enter doors"/>
   <property name="titleSW" value="Ingiza mlango"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/skills/secret_skills_teleport.png"/>
 </tile>
 <tile id="47">
  <properties>
   <property name="encyText0">With the fire extinguisher you can put out fires! Press the button near to a fire.

Learn more about the Kawaida's Journey game project:</property>
   <property name="encyUrl" value="http://www.kawaidasjourney.de"/>
   <property name="name" value="extinguish"/>
   <property name="position" type="int" value="3"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="200"/>
   <property name="secretCategory" value="skills"/>
   <property name="subtitle" value="New ability discovered"/>
   <property name="text0" value="With the fire extinguisher you can put out fires! Press the button near to a fire."/>
   <property name="title" value="Extinguish fire"/>
   <property name="titleSW" value="Kuzima moto"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/skills/secret_skills_extinguish.png"/>
 </tile>
 <tile id="48">
  <properties>
   <property name="encyText0">Collect music notes to turn on the music!

Learn more about the Kawaida's Journey game project:</property>
   <property name="encyUrl" value="http://www.kawaidasjourney.de"/>
   <property name="name" value="notes"/>
   <property name="position" type="int" value="4"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="300"/>
   <property name="secretCategory" value="skills"/>
   <property name="subtitle" value="New ability discovered"/>
   <property name="text0" value="Collect music notes to turn on the music!"/>
   <property name="title" value="Collect notes"/>
   <property name="titleSW" value="Kukusanya noti"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/skills/secret_skills_notes.png"/>
 </tile>
 <tile id="49">
  <properties>
   <property name="encyText0">Collect a spare part to repair the water pipeline. Press the button near to a broken part to fix it.

Learn more about the Kawaida's Journey game project:</property>
   <property name="encyUrl" value="http://www.kawaidasjourney.de"/>
   <property name="name" value="repair"/>
   <property name="position" type="int" value="5"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="400"/>
   <property name="secretCategory" value="skills"/>
   <property name="subtitle" value="New ability discovered"/>
   <property name="text0" value="Collect a spare part to repair the water pipeline. Press the button near to a broken part to fix it."/>
   <property name="title" value="Repair parts"/>
   <property name="titleSW" value="Kutengeneza"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/skills/secret_skills_repair.png"/>
 </tile>
 <tile id="50">
  <properties>
   <property name="encyText0" value="Traps are used to catch wild animals. One Problem is, that protected animals were randomly caught and deadly wounded by traps. Be careful, it hurts when they snap shut."/>
   <property name="encyUrl" value="https://en.wikipedia.org/wiki/Poaching"/>
   <property name="name" value="trap"/>
   <property name="position" type="int" value="6"/>
   <property name="reward" value="sun"/>
   <property name="reward:amount" type="int" value="100"/>
   <property name="secretCategory" value="objects"/>
   <property name="subtitle" value="New enemy discovered"/>
   <property name="text0" value="Traps are used to catch wild animals. Be careful, it hurts when they snap shut."/>
   <property name="title" value="Trap"/>
   <property name="titleSW" value="Mtego"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/sprites/global/ui/icons/secrets/objects/secret_objects_trap.png"/>
 </tile>
</tileset>
