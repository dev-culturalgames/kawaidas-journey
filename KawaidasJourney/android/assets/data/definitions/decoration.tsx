<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="decoration" tilewidth="2688" tileheight="1792" tilecount="147" columns="8">
 <grid orientation="orthogonal" width="1665" height="1792"/>
 <properties>
  <property name="category" value="decoration"/>
 </properties>
 <tile id="77">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="384" height="128" source="../../../rawAssets/objects/decoration/bank0.png"/>
 </tile>
 <tile id="78">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/banner0.png"/>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="79">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/baobab0/baobab0_icon.png"/>
 </tile>
 <tile id="80">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="1024" height="1024" source="../../../rawAssets/objects/decoration/baobab1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="trunk" x="384" y="144" width="224" height="720">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
   <object id="2" name="ground" x="128" y="128" width="752" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="81">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/bucket0.png"/>
 </tile>
 <tile id="82">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/bucket1.png"/>
 </tile>
 <tile id="86">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
   <property name="life:growable" type="bool" value="true"/>
   <property name="life:hitpoints" type="int" value="1"/>
   <property name="life:hitpoints_max" type="int" value="2"/>
   <property name="logics" value="life,spawn"/>
   <property name="spawn:defName" value="skull0"/>
   <property name="spawn:order" type="int" value="-1"/>
   <property name="spawn:randomize" type="float" value="1"/>
   <property name="spawn:scale" type="float" value="1.5"/>
   <property name="spawning" type="bool" value="true"/>
  </properties>
  <image width="550" height="227" source="../../../rawAssets/objects/decoration/bush3/bush3_icon.png"/>
 </tile>
 <tile id="87">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="640" height="256" source="../../../rawAssets/objects/decoration/bushes/bush4-znz-hr.png"/>
 </tile>
 <tile id="88">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="896" height="384" source="../../../rawAssets/objects/decoration/bushes/bush5-znz-hr.png"/>
 </tile>
 <tile id="89">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="640" height="256" source="../../../rawAssets/objects/decoration/bushes/bush6-znz-bg.png"/>
 </tile>
 <tile id="90">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="768" height="256" source="../../../rawAssets/objects/decoration/bushes/bush7-znz-bg.png"/>
 </tile>
 <tile id="91">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/decoration/clothesLine0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="27" y="128" width="709" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="92">
  <properties>
   <property name="environment" value="horizon_decoration"/>
   <property name="logics" value="move"/>
   <property name="move:speedRandomize" type="float" value="0.5"/>
   <property name="move:speedX" type="float" value="0.5"/>
   <property name="onRespawn" value="this:set_wp:clouds_wp"/>
  </properties>
  <image width="1024" height="512" source="../../../rawAssets/objects/decoration/clouds/cloud0.png"/>
 </tile>
 <tile id="93">
  <properties>
   <property name="environment" value="horizon_decoration"/>
   <property name="logics" value="move"/>
   <property name="move:speedRandomize" type="float" value="0.5"/>
   <property name="move:speedX" type="float" value="0.5"/>
   <property name="onRespawn" value="this:set_wp:clouds_wp"/>
  </properties>
  <image width="1024" height="512" source="../../../rawAssets/objects/decoration/clouds/cloud1.png"/>
 </tile>
 <tile id="98">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="teleport"/>
   <property name="range" type="float" value="-1"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doors/door0.png"/>
 </tile>
 <tile id="99">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="teleport"/>
   <property name="range" type="float" value="-1"/>
  </properties>
  <image width="256" height="272" source="../../../rawAssets/objects/decoration/doors/door1.png"/>
 </tile>
 <tile id="100">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco0_icon.png"/>
 </tile>
 <tile id="101">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco1_icon.png"/>
 </tile>
 <tile id="102">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco2_icon.png"/>
 </tile>
 <tile id="103">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco3_icon.png"/>
 </tile>
 <tile id="104">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="512" height="128" source="../../../rawAssets/objects/decoration/fence0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="40" y="24" width="420" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="105">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="384" height="128" source="../../../rawAssets/objects/decoration/fence1.png"/>
 </tile>
 <tile id="107">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="1024" height="512" source="../../../rawAssets/objects/decoration/hut1.png"/>
  <objectgroup draworder="index">
   <object id="18" name="ground" x="840" y="336" width="48" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="20" name="ground" x="605" y="336" width="96" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="21" name="ground" x="128" y="328" width="352" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="22" name="ground" x="263" y="72" width="496" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="108">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
   <property name="skeletonDef" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/cloth0/cloth0_icon.png"/>
 </tile>
 <tile id="112">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/melon0.png"/>
 </tile>
 <tile id="118">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/palm1/palm1_icon.png"/>
 </tile>
 <tile id="119">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="642" height="386" source="../../../rawAssets/objects/decoration/bigPlant0/bigPlant0_icon.png"/>
 </tile>
 <tile id="120">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
   <property name="life:growable" type="bool" value="true"/>
   <property name="life:hitpoints" type="int" value="1"/>
   <property name="life:hitpoints_max" type="int" value="2"/>
   <property name="logics" value="life,spawn"/>
   <property name="range" type="float" value="500"/>
   <property name="spawn:behind" type="bool" value="true"/>
   <property name="spawn:defName" value="skull0"/>
   <property name="spawn:delay_randomize" type="float" value="1"/>
   <property name="spawn:scale" type="float" value="1.5"/>
   <property name="spawn:spawning" type="bool" value="true"/>
  </properties>
  <image width="483" height="450" source="../../../rawAssets/objects/decoration/palms0/palms0_icon.png"/>
 </tile>
 <tile id="121">
  <properties>
   <property name="animation" value="spine"/>
   <property name="box2d:bodyType" value="static"/>
   <property name="environment" value="global_decoration"/>
   <property name="life:growTime" type="float" value="0"/>
   <property name="life:growing" type="bool" value="true"/>
   <property name="life:hitpoints" type="int" value="1"/>
   <property name="life:hitpoints_max" type="int" value="2"/>
   <property name="logics" value="life"/>
   <property name="range" type="float" value="500"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/palmSeedling0/palmSeedling0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="passable" type="passable" x="16" y="16.3125" width="220" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="122">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="foreground_decoration"/>
   <property name="skeletonDef" type="bool" value="true"/>
  </properties>
  <image width="387" height="260" source="../../../rawAssets/objects/decoration/leafPlant0/leafPlant0_icon.png"/>
 </tile>
 <tile id="127">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
   <property name="skeletonDef" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/smallPlant0_icon.png"/>
 </tile>
 <tile id="132">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
   <property name="life:growable" type="bool" value="true"/>
   <property name="life:hitpoints" type="int" value="1"/>
   <property name="life:hitpoints_max" type="int" value="2"/>
   <property name="logics" value="life,spawn"/>
   <property name="range" type="float" value="500"/>
   <property name="spawn:behind" type="bool" value="true"/>
   <property name="spawn:defName" value="skull0"/>
   <property name="spawn:delay_randomize" type="float" value="1"/>
   <property name="spawn:scale" type="float" value="1.5"/>
   <property name="spawn:spawning" type="bool" value="true"/>
  </properties>
  <image width="205" height="267" source="../../../rawAssets/objects/decoration/plant10/plant10_icon.png"/>
 </tile>
 <tile id="133">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
   <property name="life:growable" type="bool" value="true"/>
   <property name="life:hitpoints" type="int" value="1"/>
   <property name="life:hitpoints_max" type="int" value="2"/>
   <property name="logics" value="life,spawn"/>
   <property name="range" type="float" value="500"/>
   <property name="spawn:behind" type="bool" value="true"/>
   <property name="spawn:defName" value="skull0"/>
   <property name="spawn:delay_randomize" type="float" value="1"/>
   <property name="spawn:scale" type="float" value="1.5"/>
   <property name="spawn:spawning" type="bool" value="true"/>
  </properties>
  <image width="259" height="259" source="../../../rawAssets/objects/decoration/plant11/plant11_icon.png"/>
 </tile>
 <tile id="134">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
   <property name="life:growable" type="bool" value="true"/>
   <property name="life:hitpoints" type="int" value="1"/>
   <property name="life:hitpoints_max" type="int" value="2"/>
   <property name="logics" value="life,spawn"/>
   <property name="range" type="float" value="500"/>
   <property name="spawn:behind" type="bool" value="true"/>
   <property name="spawn:defName" value="skull0"/>
   <property name="spawn:delay_randomize" type="float" value="1"/>
   <property name="spawn:scale" type="float" value="1.5"/>
   <property name="spawn:spawning" type="bool" value="true"/>
  </properties>
  <image width="258" height="130" source="../../../rawAssets/objects/decoration/plant12/plant12_icon.png"/>
 </tile>
 <tile id="135">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
   <property name="life:growable" type="bool" value="true"/>
   <property name="life:hitpoints" type="int" value="1"/>
   <property name="life:hitpoints_max" type="int" value="2"/>
   <property name="logics" value="life,spawn"/>
   <property name="range" type="float" value="500"/>
   <property name="spawn:behind" type="bool" value="true"/>
   <property name="spawn:defName" value="skull0"/>
   <property name="spawn:delay_randomize" type="float" value="1"/>
   <property name="spawn:scale" type="float" value="1.5"/>
   <property name="spawn:spawning" type="bool" value="true"/>
  </properties>
  <image width="214" height="150" source="../../../rawAssets/objects/decoration/plant13/plant13_icon.png"/>
 </tile>
 <tile id="139">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="horizon_decoration"/>
   <property name="logics" value="move"/>
   <property name="move:speedRandomize" type="float" value="0.5"/>
   <property name="move:speedX" type="float" value="0.5"/>
   <property name="onRespawn" value="this:set_wp:clouds_wp"/>
  </properties>
  <image width="320" height="256" source="../../../rawAssets/objects/decoration/ship0/ship0_icon.png"/>
 </tile>
 <tile id="140">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/skull0/skull0_icon.png"/>
 </tile>
 <tile id="141">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/table0.png"/>
 </tile>
 <tile id="142">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/tree0/tree0_icon.png"/>
 </tile>
 <tile id="143">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="256" height="640" source="../../../rawAssets/objects/decoration/trees/tree1.png"/>
 </tile>
 <tile id="144">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="256" height="640" source="../../../rawAssets/objects/decoration/trees/tree2.png"/>
 </tile>
 <tile id="145">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="256" height="640" source="../../../rawAssets/objects/decoration/trees/tree3.png"/>
 </tile>
 <tile id="146">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="256" height="640" source="../../../rawAssets/objects/decoration/trees/tree4.png"/>
 </tile>
 <tile id="147">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="256" height="640" source="../../../rawAssets/objects/decoration/trees/tree5.png"/>
 </tile>
 <tile id="148">
  <properties>
   <property name="animation" value="spine"/>
   <property name="box2d:bodyType" value="kinematic"/>
   <property name="fromSet" type="bool" value="false"/>
   <property name="life:damage_types" value="melee"/>
   <property name="life:despawnOnDeath" type="bool" value="false"/>
   <property name="life:hitpoints" type="int" value="4"/>
   <property name="life:hitpoints_max" type="int" value="4"/>
   <property name="life:isEnemy" type="bool" value="false"/>
   <property name="logics" value="life"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="2688" height="1792" source="../../../rawAssets/objects/decoration/treeHouse0/treeHouse0_icon.png"/>
  <objectgroup draworder="index">
   <object id="1" name="trunk" x="657" y="1016" width="348" height="744">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
   <object id="5" name="tree_house" x="561" y="1000" width="662" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="7" name="life_hitbox" x="801.333" y="1568" width="64" height="160">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
   <object id="30" name="crown1" x="336" y="280">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
    <polygon points="0,70 252,-42 252,448 0,448"/>
   </object>
   <object id="33" name="crown2a" x="588" y="224">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
    <polygon points="0,14 0,504 252,336 252,14"/>
   </object>
   <object id="29" name="crown4" x="1344" y="350" width="168" height="378">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
   <object id="31" name="crown3" x="1092" y="224">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
    <polygon points="0,14 252,126 252,504 0,504"/>
   </object>
   <object id="34" name="crown2b" x="840" y="224">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
    <polygon points="0,14 252,14 252,504 0,336"/>
   </object>
   <object id="22" name="tree_top2" x="588" y="224" width="504" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="23" name="tree_top5" x="1344" y="336" width="168" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="24" name="tree_top3" x="1092" y="280">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
    <polygon points="0,-56 252,56 252,70 0,-42"/>
   </object>
   <object id="27" name="crown0" x="168" y="350" width="168" height="378">
    <properties>
     <property name="box2d:category" value="climbable"/>
    </properties>
   </object>
   <object id="25" name="tree_top0" x="168" y="336" width="168" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
   <object id="26" name="tree_top1" x="588" y="280">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
    <polygon points="0,-56 -252,56 -252,70 0,-42"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="149">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="768" height="640" source="../../../rawAssets/objects/decoration/trees/trees0.png"/>
 </tile>
 <tile id="150">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window0.png"/>
 </tile>
 <tile id="152">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window2.png"/>
 </tile>
 <tile id="154">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/windows/window4.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="50" y="64" width="420" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="157">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/antenna0.png"/>
 </tile>
 <tile id="158">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/antenna1.png"/>
 </tile>
 <tile id="159">
  <properties>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="bumper"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="128" height="512" source="../../../rawAssets/objects/decoration/bumperSign0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="bump_hitbox" x="40" y="0" width="32" height="512">
    <properties>
     <property name="box2d:category" value="hitbox"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="160">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco4_icon.png"/>
 </tile>
 <tile id="161">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco5_icon.png"/>
 </tile>
 <tile id="162">
  <properties>
   <property name="crash" type="bool" value="true"/>
   <property name="crash_time" type="float" value="2"/>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/crash.png"/>
 </tile>
 <tile id="163">
  <properties>
   <property name="box2d:bodyType" value="dynamic"/>
   <property name="environment" value="global_decoration"/>
   <property name="fall" type="bool" value="true"/>
   <property name="range" type="float" value="200"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/fall.png"/>
 </tile>
 <tile id="166">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="teleport"/>
   <property name="range" type="float" value="-1"/>
  </properties>
  <image width="256" height="272" source="../../../rawAssets/objects/decoration/doors/door4.png"/>
 </tile>
 <tile id="167">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="teleport"/>
   <property name="range" type="float" value="-1"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doors/door5.png"/>
 </tile>
 <tile id="168">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window5.png"/>
  <objectgroup draworder="index">
   <object id="2" name="ground" x="64" y="32" width="128" height="32">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="169">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window6.png"/>
 </tile>
 <tile id="170">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window7.png"/>
 </tile>
 <tile id="171">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window8.png"/>
 </tile>
 <tile id="172">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/apple0.png"/>
 </tile>
 <tile id="173">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="trash"/>
  </properties>
  <image width="64" height="128" source="../../../rawAssets/objects/decoration/trash/bottle0.png"/>
 </tile>
 <tile id="174">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="trash"/>
  </properties>
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/trash/box0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="20" y="30" width="326" height="200">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="175">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="64" height="64" source="../../../rawAssets/objects/decoration/trash/orange0.png"/>
 </tile>
 <tile id="178">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="trash"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/trash/paper2.png"/>
 </tile>
 <tile id="179">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="trash"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/trashBag0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="30" y="30" width="200" height="200">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="180">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="horizon_decoration"/>
   <property name="skeletonDef" type="bool" value="true"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm0/palm0_icon.png"/>
 </tile>
 <tile id="187">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="light:color" type="color" value="#bf00ffff"/>
   <property name="light:radius" type="float" value="512"/>
   <property name="lightSource" type="bool" value="true"/>
   <property name="logics" value="light"/>
   <property name="subcategory" value="light"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_cyan0.png"/>
 </tile>
 <tile id="189">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="light:color" type="color" value="#bfff00ff"/>
   <property name="light:radius" type="float" value="512"/>
   <property name="lightSource" type="bool" value="true"/>
   <property name="logics" value="light"/>
   <property name="subcategory" value="light"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_pink0.png"/>
 </tile>
 <tile id="190">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="light:color" type="color" value="#bfffaa00"/>
   <property name="light:radius" type="float" value="512"/>
   <property name="lightSource" type="bool" value="true"/>
   <property name="logics" value="light"/>
   <property name="subcategory" value="light"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_red0.png"/>
 </tile>
 <tile id="191">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="light:color" type="color" value="#bfffff00"/>
   <property name="light:radius" type="float" value="512"/>
   <property name="lightSource" type="bool" value="true"/>
   <property name="logics" value="light"/>
   <property name="subcategory" value="light"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_yellow0.png"/>
 </tile>
 <tile id="192">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/bush0/bush0_icon.png"/>
 </tile>
 <tile id="193">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window9.png"/>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="194">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/windows/window9_light.png"/>
 </tile>
 <tile id="195">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window10.png"/>
 </tile>
 <tile id="196">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/windows/window10_light.png"/>
 </tile>
 <tile id="198">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window12.png"/>
 </tile>
 <tile id="199">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window13.png"/>
 </tile>
 <tile id="200">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window14.png"/>
 </tile>
 <tile id="202">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="1792" height="512" source="../../../rawAssets/objects/decoration/kilimanjaro0.png"/>
 </tile>
 <tile id="203">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/bushes/bush8-ser-w.png"/>
 </tile>
 <tile id="204">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="768" height="256" source="../../../rawAssets/objects/decoration/bushes/bush9-ser-hr.png"/>
 </tile>
 <tile id="205">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/bushes/bush10-ser-fg.png"/>
 </tile>
 <tile id="206">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/decoration/bushes/bush11-ser-fg.png"/>
 </tile>
 <tile id="207">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/decoration/acacia/acacia0-w.png"/>
 </tile>
 <tile id="208">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="1024" height="512" source="../../../rawAssets/objects/decoration/acacia/acacia1-fg.png"/>
 </tile>
 <tile id="209">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/acacia/acacia2-hr.png"/>
 </tile>
 <tile id="210">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/acacia/acacia3-hr.png"/>
 </tile>
 <tile id="211">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass_combo0-ser.png"/>
 </tile>
 <tile id="212">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass_combo1-ser.png"/>
 </tile>
 <tile id="213">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass0-ser.png"/>
 </tile>
 <tile id="214">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass1-ser.png"/>
 </tile>
 <tile id="215">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass2-ser.png"/>
 </tile>
 <tile id="216">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass3-ser.png"/>
 </tile>
 <tile id="217">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass4-ser.png"/>
 </tile>
 <tile id="218">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass5-ser.png"/>
 </tile>
 <tile id="223">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_blue0#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="64" y="100" width="192" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="224">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_blue0#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="100" width="256" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="225">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_red0#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="64" y="100" width="192" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="226">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_red0#1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="0" y="100" width="256" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="227">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_yellow0#0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="ground" x="64" y="100" width="192" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="228">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_yellow0#1.png"/>
  <objectgroup draworder="index">
   <object id="2" name="ground" x="0" y="100" width="256" height="16">
    <properties>
     <property name="box2d:category" value="passable"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="234">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="384" source="../../../rawAssets/objects/decoration/shop/shop0#0.png"/>
 </tile>
 <tile id="235">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="384" source="../../../rawAssets/objects/decoration/shop/shop0#1.png"/>
 </tile>
 <tile id="236">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/shop/sign0_chili0.png"/>
 </tile>
 <tile id="237">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/shop/sign0_fruits0.png"/>
 </tile>
 <tile id="238">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="64" height="64" source="../../../rawAssets/objects/decoration/shop/spices0.png"/>
 </tile>
 <tile id="239">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="64" height="64" source="../../../rawAssets/objects/decoration/shop/spices1.png"/>
 </tile>
 <tile id="240">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="64" height="64" source="../../../rawAssets/objects/decoration/shop/spices2.png"/>
 </tile>
 <tile id="241">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0.png"/>
 </tile>
 <tile id="245">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="subcategory" value="light"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_cyan0_glow.png"/>
 </tile>
 <tile id="246">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="subcategory" value="light"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_pink0_glow.png"/>
 </tile>
 <tile id="247">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="subcategory" value="light"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_red0_glow.png"/>
 </tile>
 <tile id="248">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="subcategory" value="light"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_yellow0_glow.png"/>
 </tile>
 <tile id="249">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="1280" height="512" source="../../../rawAssets/objects/decoration/bushes/bush12-ser-fg.png"/>
 </tile>
 <tile id="250">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="1280" height="512" source="../../../rawAssets/objects/decoration/bushes/bush13-ser-fg.png"/>
 </tile>
 <tile id="251">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_bananas0.png"/>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="255">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/sign1/sign1_icon.png"/>
 </tile>
 <tile id="256">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="1280" height="512" source="../../../rawAssets/objects/decoration/acacia/acacia4-fg.png"/>
 </tile>
 <tile id="257">
  <properties>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="attack,trash"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/bottle0_broken0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_melee" x="12" y="0" width="64" height="64">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="258">
  <properties>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="attack,trash"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/bottle0_broken1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_melee" x="16" y="16" width="64" height="64">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="259">
  <properties>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="attack,trash"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/trash/fishBone0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_melee" x="64" y="32" width="128" height="64">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="260">
  <properties>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="attack,trash"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/shard0.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_melee" x="32" y="32" width="64" height="64">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="261">
  <properties>
   <property name="attack:melee" type="bool" value="true"/>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="attack,trash"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/shard1.png"/>
  <objectgroup draworder="index">
   <object id="1" name="attack_melee" x="32" y="32" width="64" height="64">
    <properties>
     <property name="box2d:category" value="danger"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="262">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="pipe"/>
   <property name="part" type="int" value="0"/>
   <property name="pipe:pickable" type="bool" value="false"/>
   <property name="range" type="float" value="-1"/>
   <property name="view" type="float" value="256"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe0.png"/>
 </tile>
 <tile id="263">
  <properties>
   <property name="cbspawn:count" type="int" value="4"/>
   <property name="cbspawn:name" value="tigerMosquito0"/>
   <property name="cbspawn:range" type="float" value="512"/>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="broken,spawn,endSpawn"/>
   <property name="loop" type="bool" value="true"/>
   <property name="part" type="int" value="0"/>
   <property name="range" type="float" value="-1"/>
   <property name="spawn:defName" value="tigerMosquito0"/>
   <property name="spawn:max" type="int" value="1"/>
   <property name="spawn:spawning" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe0_broken0.png"/>
 </tile>
 <tile id="264">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="pipe"/>
   <property name="part" type="int" value="1"/>
   <property name="pipe:pickable" type="bool" value="false"/>
   <property name="range" type="float" value="-1"/>
   <property name="view" type="float" value="256"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe1.png"/>
 </tile>
 <tile id="265">
  <properties>
   <property name="cbspawn:count" type="int" value="4"/>
   <property name="cbspawn:name" value="tigerMosquito0"/>
   <property name="cbspawn:range" type="float" value="512"/>
   <property name="delay" type="float" value="2"/>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="broken,spawn,endSpawn"/>
   <property name="loop" type="bool" value="true"/>
   <property name="part" type="int" value="1"/>
   <property name="range" type="float" value="-1"/>
   <property name="spawn:defName" value="tigerMosquito0"/>
   <property name="spawn:max" type="int" value="1"/>
   <property name="spawn:spawning" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe1_broken0.png"/>
 </tile>
 <tile id="266">
  <properties>
   <property name="cbspawn:count" type="int" value="4"/>
   <property name="cbspawn:name" value="tigerMosquito0"/>
   <property name="cbspawn:range" type="float" value="512"/>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="broken,spawn,endSpawn"/>
   <property name="loop" type="bool" value="true"/>
   <property name="part" type="int" value="0"/>
   <property name="range" type="float" value="-1"/>
   <property name="spawn:defName" value="tigerMosquito0"/>
   <property name="spawn:max" type="int" value="1"/>
   <property name="spawn:spawning" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe0_broken1.png"/>
 </tile>
 <tile id="267">
  <properties>
   <property name="cbspawn:count" type="int" value="4"/>
   <property name="cbspawn:name" value="tigerMosquito0"/>
   <property name="cbspawn:range" type="float" value="512"/>
   <property name="delay" type="float" value="2"/>
   <property name="environment" value="global_decoration"/>
   <property name="logics" value="broken,spawn,endSpawn"/>
   <property name="loop" type="bool" value="true"/>
   <property name="part" type="int" value="1"/>
   <property name="range" type="float" value="-1"/>
   <property name="spawn:defName" value="tigerMosquito0"/>
   <property name="spawn:max" type="int" value="1"/>
   <property name="spawn:spawning" type="bool" value="true"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe1_broken1.png"/>
 </tile>
 <tile id="268">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window7_big.png"/>
 </tile>
 <tile id="269">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/windows/window12_light.png"/>
 </tile>
 <tile id="270">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_apples0.png"/>
 </tile>
 <tile id="271">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_cocos0.png"/>
 </tile>
 <tile id="272">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_lemons0.png"/>
 </tile>
 <tile id="273">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_melons0.png"/>
 </tile>
 <tile id="274">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="192" height="160" source="../../../rawAssets/objects/decoration/shop/woodenBox0_oranges0.png"/>
 </tile>
 <tile id="275">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_spices0.png"/>
 </tile>
 <tile id="276">
  <properties>
   <property name="environment" value="global_decoration"/>
   <property name="usePhysics" type="bool" value="true"/>
  </properties>
  <image width="768" height="640" source="../../../rawAssets/objects/decoration/how0.png"/>
 </tile>
 <tile id="277">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="768" height="640" source="../../../rawAssets/objects/decoration/how0_hr0.png"/>
 </tile>
 <tile id="278">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/twinTowers0.png"/>
 </tile>
 <tile id="279">
  <properties>
   <property name="environment" value="horizon_decoration"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/twinTowers0_hr0.png"/>
 </tile>
 <tile id="280">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/trash/trashPile0.png"/>
 </tile>
 <tile id="281">
  <properties>
   <property name="environment" value="global_decoration"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/trash/trashPile1.png"/>
 </tile>
 <tile id="282">
  <properties>
   <property name="animation" value="spine"/>
   <property name="environment" value="global_decoration"/>
   <property name="skeletonDef" type="bool" value="true"/>
  </properties>
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm2/palm2_icon.png"/>
 </tile>
</tileset>
