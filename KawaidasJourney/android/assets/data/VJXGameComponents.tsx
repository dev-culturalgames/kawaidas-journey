<?xml version="1.0" encoding="UTF-8"?>
<tileset name="VJXGameComponents" tilewidth="128" tileheight="128" tilecount="1" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="zoom" type="float" value="1"/>
  </properties>
  <image width="128" height="128" source="../../rawAssets/game/components/camera.png"/>
 </tile>
</tileset>
