<?xml version="1.0" encoding="UTF-8"?>
<tileset name="deco_hr_KB" tilewidth="768" tileheight="640" tilecount="14" columns="3">
 <grid orientation="orthogonal" width="768" height="565"/>
 <tile id="9">
  <image width="640" height="256" source="../../../rawAssets/objects/decoration/bushes/bush6-znz-bg.png"/>
 </tile>
 <tile id="10">
  <image width="768" height="256" source="../../../rawAssets/objects/decoration/bushes/bush7-znz-bg.png"/>
 </tile>
 <tile id="11">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/baobab0/baobab0_icon.png"/>
 </tile>
 <tile id="12">
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm0/palm0_bright3.png"/>
 </tile>
 <tile id="13">
  <image width="320" height="256" source="../../../rawAssets/objects/decoration/ship0/ship0_icon.png"/>
 </tile>
 <tile id="14">
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm0/palm0_bright0.png"/>
 </tile>
 <tile id="15">
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm0/palm0_bright1.png"/>
 </tile>
 <tile id="16">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/plant5_icon.png"/>
 </tile>
 <tile id="17">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/plant7_icon.png"/>
 </tile>
 <tile id="18">
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/acacia/acacia2-hr.png"/>
 </tile>
 <tile id="19">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/acacia/acacia3-hr.png"/>
 </tile>
 <tile id="20">
  <image width="768" height="256" source="../../../rawAssets/objects/decoration/bushes/bush9-ser-hr.png"/>
 </tile>
 <tile id="21">
  <image width="768" height="640" source="../../../rawAssets/objects/decoration/how0_hr0.png"/>
 </tile>
 <tile id="22">
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/twinTowers0_hr0.png"/>
 </tile>
</tileset>
