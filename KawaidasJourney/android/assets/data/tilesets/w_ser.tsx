<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="w_ser" tilewidth="256" tileheight="256" spacing="4" margin="4" tilecount="64" columns="8">
 <image source="../../../rawAssets/tiles/w_ser.png" width="2084" height="2084"/>
 <tile id="0">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="preFlip" type="bool" value="true"/>
   <property name="tileCase" type="int" value="66"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="68"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="box2d:category" value="passable"/>
   <property name="preFlip" type="bool" value="true"/>
   <property name="tileCase" type="int" value="66"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="box2d:category" value="passable"/>
   <property name="tileCase" type="int" value="68"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="box2d:category" value="passable"/>
   <property name="tileCase" type="int" value="73"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="box2d:category" value="passable"/>
   <property name="tileCase" type="int" value="68"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="11"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="11"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="11"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="box2d:category" value="solid"/>
   <property name="tileCase" type="int" value="11"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="box2d:category" value="passable"/>
   <property name="tileCase" type="int" value="7"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="box2d:category" value="passable"/>
   <property name="tileCase" type="int" value="68"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="box2d:category" value="passable"/>
   <property name="tileCase" type="int" value="7"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="box2d:category" value="passable"/>
   <property name="tileCase" type="int" value="68"/>
  </properties>
 </tile>
</tileset>
