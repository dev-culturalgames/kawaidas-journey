<?xml version="1.0" encoding="UTF-8"?>
<tileset name="deco_bg_KB" tilewidth="1792" tileheight="640" tilecount="12" columns="3">
 <grid orientation="orthogonal" width="1024" height="640"/>
 <tile id="0">
  <image width="640" height="256" source="../../../rawAssets/objects/decoration/bushes/bush4-znz-hr.png"/>
 </tile>
 <tile id="1">
  <image width="896" height="384" source="../../../rawAssets/objects/decoration/bushes/bush5-znz-hr.png"/>
 </tile>
 <tile id="2">
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm0/palm0_bright2.png"/>
 </tile>
 <tile id="3">
  <image width="768" height="640" source="../../../rawAssets/objects/decoration/trees/trees0.png"/>
 </tile>
 <tile id="4">
  <image width="256" height="640" source="../../../rawAssets/objects/decoration/trees/tree1.png"/>
 </tile>
 <tile id="5">
  <image width="256" height="640" source="../../../rawAssets/objects/decoration/trees/tree2.png"/>
 </tile>
 <tile id="6">
  <image width="256" height="640" source="../../../rawAssets/objects/decoration/trees/tree3.png"/>
 </tile>
 <tile id="7">
  <image width="256" height="640" source="../../../rawAssets/objects/decoration/trees/tree4.png"/>
 </tile>
 <tile id="8">
  <image width="256" height="640" source="../../../rawAssets/objects/decoration/trees/tree5.png"/>
 </tile>
 <tile id="9">
  <image width="1024" height="512" source="../../../rawAssets/objects/decoration/clouds/cloud0.png"/>
 </tile>
 <tile id="10">
  <image width="1024" height="512" source="../../../rawAssets/objects/decoration/clouds/cloud1.png"/>
 </tile>
 <tile id="11">
  <image width="1792" height="512" source="../../../rawAssets/objects/decoration/kilimanjaro0.png"/>
 </tile>
</tileset>
