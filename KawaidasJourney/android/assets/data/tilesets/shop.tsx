<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="shop" tilewidth="1920" tileheight="352" tilecount="7" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <properties>
  <property name="item" value=""/>
  <property name="type" value="item"/>
 </properties>
 <tile id="0">
  <properties>
   <property name="type" value="object"/>
  </properties>
  <image width="1920" height="352" source="../../../rawAssets/sprites/global/ui/icons/shop/shop_table.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="type" value="object"/>
  </properties>
  <image width="101" height="191" source="../../../rawAssets/sprites/global/ui/icons/shop/shop_giraffe.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="type" value="object"/>
  </properties>
  <image width="273" height="295" source="../../../rawAssets/sprites/global/ui/icons/shop/shop_plant.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="type" value="object"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/icons/shop/shop_tile.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="type" value="object"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/sprites/global/ui/icons/shop/shop_web.png"/>
 </tile>
 <tile id="6">
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lady0.png"/>
 </tile>
 <tile id="7">
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dikdik0_suit0.png"/>
 </tile>
</tileset>
