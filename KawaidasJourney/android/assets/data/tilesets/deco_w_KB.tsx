<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="deco_w_KB" tilewidth="768" tileheight="640" tilecount="100" columns="0">
 <grid orientation="orthogonal" width="576" height="649"/>
 <tile id="0">
  <image width="384" height="128" source="../../../rawAssets/objects/decoration/bank0.png"/>
 </tile>
 <tile id="1">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/banner0.png"/>
 </tile>
 <tile id="2">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/bucket0.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="view" type="float" value="128"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/bucket1.png"/>
 </tile>
 <tile id="4">
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/bush0/bush0_dGreen0.png"/>
 </tile>
 <tile id="5">
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/bush0/bush0_green0.png"/>
 </tile>
 <tile id="6">
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/bush0/bush0_lGreen0.png"/>
 </tile>
 <tile id="8">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doors/door0.png"/>
 </tile>
 <tile id="9">
  <image width="256" height="272" source="../../../rawAssets/objects/decoration/doors/door1.png"/>
 </tile>
 <tile id="10">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco0_icon.png"/>
 </tile>
 <tile id="11">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco1_icon.png"/>
 </tile>
 <tile id="12">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco2_icon.png"/>
 </tile>
 <tile id="13">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco3_icon.png"/>
 </tile>
 <tile id="15">
  <image width="384" height="128" source="../../../rawAssets/objects/decoration/fence1.png"/>
 </tile>
 <tile id="17">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/cloth0/towel0.png"/>
 </tile>
 <tile id="18">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/cloth0/towel1.png"/>
 </tile>
 <tile id="19">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/cloth0/towel2.png"/>
 </tile>
 <tile id="20">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/cloth0/towel3.png"/>
 </tile>
 <tile id="21">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/melon0.png"/>
 </tile>
 <tile id="23">
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/palm2/palm2.png"/>
 </tile>
 <tile id="25">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/palm1/palm1_icon.png"/>
 </tile>
 <tile id="26">
  <image width="642" height="386" source="../../../rawAssets/objects/decoration/bigPlant0/bigPlant0_icon.png"/>
 </tile>
 <tile id="28">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/plant6_icon.png"/>
 </tile>
 <tile id="30">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/plant8_icon.png"/>
 </tile>
 <tile id="31">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/smallPlant0/plant9_icon.png"/>
 </tile>
 <tile id="32">
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/table0.png"/>
 </tile>
 <tile id="33">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/tree0/tree0_icon.png"/>
 </tile>
 <tile id="34">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window0.png"/>
 </tile>
 <tile id="36">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window2.png"/>
 </tile>
 <tile id="39">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_dark#0.png"/>
 </tile>
 <tile id="40">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_dark#1.png"/>
 </tile>
 <tile id="41">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_dark#2.png"/>
 </tile>
 <tile id="42">
  <properties>
   <property name="category" value="tiled"/>
  </properties>
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_light#0.png"/>
 </tile>
 <tile id="43">
  <properties>
   <property name="category" value="tiled"/>
  </properties>
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_light#1.png"/>
 </tile>
 <tile id="44">
  <properties>
   <property name="category" value="tiled"/>
  </properties>
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stone0/stone0_light#2.png"/>
 </tile>
 <tile id="45">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/antenna0.png"/>
 </tile>
 <tile id="46">
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/antenna1.png"/>
 </tile>
 <tile id="47">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco4_icon.png"/>
 </tile>
 <tile id="48">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doorDeco/doorDeco5_icon.png"/>
 </tile>
 <tile id="49">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/apple0.png"/>
 </tile>
 <tile id="50">
  <image width="64" height="128" source="../../../rawAssets/objects/decoration/trash/bottle0.png"/>
 </tile>
 <tile id="51">
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/trash/box0.png"/>
 </tile>
 <tile id="52">
  <image width="64" height="64" source="../../../rawAssets/objects/decoration/trash/orange0.png"/>
 </tile>
 <tile id="54">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/trashBag0.png"/>
 </tile>
 <tile id="57">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/windows/window9_light.png"/>
 </tile>
 <tile id="58">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window9.png"/>
 </tile>
 <tile id="59">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/windows/window10_light.png"/>
 </tile>
 <tile id="60">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window10.png"/>
 </tile>
 <tile id="62">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window12.png"/>
 </tile>
 <tile id="63">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window13.png"/>
 </tile>
 <tile id="64">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window14.png"/>
 </tile>
 <tile id="66">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_cyan0.png"/>
 </tile>
 <tile id="67">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_pink0.png"/>
 </tile>
 <tile id="68">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_red0.png"/>
 </tile>
 <tile id="69">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_yellow0.png"/>
 </tile>
 <tile id="70">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window5.png"/>
 </tile>
 <tile id="71">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window6.png"/>
 </tile>
 <tile id="72">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window7.png"/>
 </tile>
 <tile id="73">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window8.png"/>
 </tile>
 <tile id="76">
  <properties>
   <property name="port:speed" type="float" value="1"/>
  </properties>
  <image width="256" height="272" source="../../../rawAssets/objects/decoration/doors/door4.png"/>
 </tile>
 <tile id="77">
  <properties>
   <property name="port:speed" type="float" value="1"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/doors/door5.png"/>
 </tile>
 <tile id="78">
  <image width="768" height="512" source="../../../rawAssets/objects/decoration/acacia/acacia0-w.png"/>
 </tile>
 <tile id="79">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/bushes/bush8-ser-w.png"/>
 </tile>
 <tile id="80">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass_combo0-ser.png"/>
 </tile>
 <tile id="81">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass_combo1-ser.png"/>
 </tile>
 <tile id="82">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass0-ser.png"/>
 </tile>
 <tile id="83">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass1-ser.png"/>
 </tile>
 <tile id="84">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass2-ser.png"/>
 </tile>
 <tile id="85">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass3-ser.png"/>
 </tile>
 <tile id="86">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass4-ser.png"/>
 </tile>
 <tile id="87">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/grass/grass5-ser.png"/>
 </tile>
 <tile id="96">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/shop/sign0_chili0.png"/>
 </tile>
 <tile id="97">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/shop/sign0_fruits0.png"/>
 </tile>
 <tile id="98">
  <image width="64" height="64" source="../../../rawAssets/objects/decoration/shop/spices0.png"/>
 </tile>
 <tile id="99">
  <image width="64" height="64" source="../../../rawAssets/objects/decoration/shop/spices1.png"/>
 </tile>
 <tile id="100">
  <image width="64" height="64" source="../../../rawAssets/objects/decoration/shop/spices2.png"/>
 </tile>
 <tile id="101">
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0.png"/>
 </tile>
 <tile id="114">
  <properties>
   <property name="bumper:height" type="float" value="0.25"/>
   <property name="bumper:speed" type="float" value="0.5"/>
  </properties>
  <image width="128" height="512" source="../../../rawAssets/objects/decoration/bumperSign0.png"/>
 </tile>
 <tile id="115">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_cyan0_glow.png"/>
 </tile>
 <tile id="116">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_pink0_glow.png"/>
 </tile>
 <tile id="117">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_red0_glow.png"/>
 </tile>
 <tile id="118">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/light/lamp0/lamp0_yellow0_glow.png"/>
 </tile>
 <tile id="123">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/bottle0_broken0.png"/>
 </tile>
 <tile id="124">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/bottle0_broken1.png"/>
 </tile>
 <tile id="125">
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/trash/fishBone0.png"/>
 </tile>
 <tile id="126">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/shard0.png"/>
 </tile>
 <tile id="127">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/trash/shard1.png"/>
 </tile>
 <tile id="128">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/sign1/sign1_icon.png"/>
 </tile>
 <tile id="129">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/windows/window7_big.png"/>
 </tile>
 <tile id="130">
  <image width="256" height="128" source="../../../rawAssets/objects/decoration/trash/paper2.png"/>
 </tile>
 <tile id="131">
  <image width="512" height="512" source="../../../rawAssets/objects/decoration/windows/window12_light.png"/>
 </tile>
 <tile id="132">
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_apples0.png"/>
 </tile>
 <tile id="133">
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_bananas0.png"/>
 </tile>
 <tile id="134">
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_melons0.png"/>
 </tile>
 <tile id="135">
  <image width="192" height="160" source="../../../rawAssets/objects/decoration/shop/woodenBox0_oranges0.png"/>
 </tile>
 <tile id="136">
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_cocos0.png"/>
 </tile>
 <tile id="137">
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_spices0.png"/>
 </tile>
 <tile id="138">
  <image width="192" height="128" source="../../../rawAssets/objects/decoration/shop/woodenBox0_lemons0.png"/>
 </tile>
 <tile id="139">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/trash/trashPile0.png"/>
 </tile>
 <tile id="140">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/trash/trashPile1.png"/>
 </tile>
</tileset>
