<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="characters_KB" tilewidth="1344" tileheight="1024" tilecount="74" columns="6">
 <grid orientation="orthogonal" width="650" height="640"/>
 <tile id="3">
  <properties>
   <property name="attack:directionX" type="float" value="1"/>
   <property name="attack:directionY" type="float" value="0.029999999999999999"/>
   <property name="attack:power" type="float" value="1"/>
   <property name="climb:speedX" type="float" value="2"/>
   <property name="climb:speedY" type="float" value="2"/>
   <property name="jump:height_max" type="float" value="2.1499999999999999"/>
   <property name="move:speedX" type="float" value="3.25"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/kawaida0.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="move:speedX" type="float" value="0.75"/>
   <property name="move:speedY" type="float" value="0.75"/>
   <property name="range" type="float" value="512"/>
   <property name="view" type="float" value="768"/>
  </properties>
  <image width="256" height="200" source="../../../rawAssets/objects/character/tigerMosquito0/tigerMosquito0_icon.png"/>
 </tile>
 <tile id="5">
  <image width="512" height="256" source="../../../rawAssets/objects/character/crab0/crab0.png"/>
 </tile>
 <tile id="6">
  <image width="256" height="256" source="../../../rawAssets/objects/character/crow0/crow0_icon.png"/>
 </tile>
 <tile id="7">
  <image width="340" height="56" source="../../../rawAssets/objects/character/trap0/trap0_icon.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
   <property name="move:speedY" type="float" value="2"/>
   <property name="plasticBag:rangeX" type="float" value="256"/>
   <property name="plasticBag:rangeY" type="float" value="256"/>
   <property name="plasticBag:waitTime" type="float" value="0"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/character/plasticBag0/plasticBag0_icon.png"/>
 </tile>
 <tile id="9">
  <image width="384" height="384" source="../../../rawAssets/objects/character/leopard0/leopard0_icon.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="trampoline:height" type="float" value="8"/>
  </properties>
  <image width="384" height="320" source="../../../rawAssets/objects/character/hippo0/hippo0_icon.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lady0.png"/>
 </tile>
 <tile id="12">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lady1.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/lady2.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="life:hitpoints" type="int" value="5"/>
   <property name="life:hitpoints_max" type="int" value="5"/>
   <property name="move:speedX" type="float" value="3"/>
  </properties>
  <image width="768" height="640" source="../../../rawAssets/objects/character/bulldozer0/steamGhost0_icon.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="context:range" type="float" value="320"/>
  </properties>
  <image width="384" height="288" source="../../../rawAssets/objects/character/fire0/fire0.png"/>
 </tile>
 <tile id="16">
  <image width="384" height="384" source="../../../rawAssets/objects/character/smoke0/smoke0_clean0.png"/>
 </tile>
 <tile id="17">
  <image width="256" height="128" source="../../../rawAssets/objects/character/smokeCloud0/smokeCloud0_clean0.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_lizard0_driver0.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="box2d:bodyType" value="dynamic"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/character/electricSpark0/electricSpark0_small.png"/>
 </tile>
 <tile id="20">
  <properties>
   <property name="move:flying" type="bool" value="true"/>
   <property name="move:speedX" type="float" value="2"/>
   <property name="move:speedY" type="float" value="2"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/character/electricSpark0/electricSpark0_big.png"/>
 </tile>
 <tile id="21">
  <properties>
   <property name="move:speedX" type="float" value="2.5"/>
   <property name="view" type="float" value="768"/>
  </properties>
  <image width="256" height="128" source="../../../rawAssets/objects/character/ant0/siafu0.png"/>
 </tile>
 <tile id="22">
  <properties>
   <property name="attack:power" type="float" value="4"/>
   <property name="view" type="float" value="1024"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/colobus0_evil.png"/>
 </tile>
 <tile id="23">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_lady0.png"/>
 </tile>
 <tile id="24">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_scarf0.png"/>
 </tile>
 <tile id="25">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus1_lady0.png"/>
 </tile>
 <tile id="26">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus1_lady1.png"/>
 </tile>
 <tile id="27">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dikdik0_kanga0.png"/>
 </tile>
 <tile id="28">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dikdik0_lady0.png"/>
 </tile>
 <tile id="29">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_lizard0_kanga0.png"/>
 </tile>
 <tile id="32">
  <image width="256" height="256" source="../../../rawAssets/objects/character/colobussi0/colobussi0.png"/>
 </tile>
 <tile id="33">
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_blue0.png"/>
 </tile>
 <tile id="34">
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_blue1.png"/>
 </tile>
 <tile id="35">
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_red0.png"/>
 </tile>
 <tile id="36">
  <image width="1088" height="640" source="../../../rawAssets/objects/vehicle/transporter0/dalaDala0_red1.png"/>
 </tile>
 <tile id="37">
  <properties>
   <property name="move:speedX" type="float" value="2.5"/>
  </properties>
  <image width="1024" height="384" source="../../../rawAssets/objects/vehicle/bus0/omnibus0_red0.png"/>
 </tile>
 <tile id="38">
  <properties>
   <property name="move:speedX" type="float" value="2.5"/>
  </properties>
  <image width="1024" height="384" source="../../../rawAssets/objects/vehicle/bus0/omnibus0_blue0.png"/>
 </tile>
 <tile id="39">
  <image width="419" height="418" source="../../../rawAssets/objects/character/chicken0/chicken0_brown0.png"/>
 </tile>
 <tile id="41">
  <image width="419" height="418" source="../../../rawAssets/objects/character/chicken0/chicken0_white0.png"/>
 </tile>
 <tile id="42">
  <properties>
   <property name="move:speedX" type="float" value="2.5"/>
   <property name="range" type="float" value="512"/>
   <property name="view" type="float" value="640"/>
  </properties>
  <image width="419" height="418" source="../../../rawAssets/objects/character/chicken0/chicken0_evil0.png"/>
 </tile>
 <tile id="43">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/karim0.png"/>
 </tile>
 <tile id="44">
  <properties>
   <property name="life:hitpoints" type="int" value="4"/>
   <property name="life:hitpoints_max" type="int" value="4"/>
   <property name="move:speedX" type="float" value="3.5"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/drifter0.png"/>
 </tile>
 <tile id="45">
  <properties>
   <property name="box2d:bodyType" value="kinematic"/>
  </properties>
  <image width="512" height="384" source="../../../rawAssets/objects/vehicle/motorbike0/pikiPiki0.png"/>
 </tile>
 <tile id="46">
  <image width="384" height="384" source="../../../rawAssets/objects/character/smoke0/smoke0_poison0.png"/>
 </tile>
 <tile id="47">
  <properties>
   <property name="box2d:bodyType" value="kinematic"/>
  </properties>
  <image width="768" height="512" source="../../../rawAssets/objects/vehicle/motorbike1/bajaji0_blue0.png"/>
 </tile>
 <tile id="48">
  <image width="1344" height="896" source="../../../rawAssets/objects/character/elephant0/elephant0_gray0.png"/>
 </tile>
 <tile id="49">
  <image width="1344" height="896" source="../../../rawAssets/objects/character/elephant0/elephant0_white0.png"/>
 </tile>
 <tile id="50">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_dikdik0_massaiFarmer0.png"/>
 </tile>
 <tile id="51">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_dikdik0_kanzu0.png"/>
 </tile>
 <tile id="52">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_lizard0_kanzu0.png"/>
 </tile>
 <tile id="53">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="327" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_market0.png"/>
 </tile>
 <tile id="54">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_bushbaby0_rapper0.png"/>
 </tile>
 <tile id="55">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus0_market0.png"/>
 </tile>
 <tile id="56">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus0_scarf0.png"/>
 </tile>
 <tile id="57">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_colobus1_scarf0.png"/>
 </tile>
 <tile id="58">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dikdik0_suit0.png"/>
 </tile>
 <tile id="59">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dog0_massaiChief0.png"/>
 </tile>
 <tile id="60">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_dog0_scarf0.png"/>
 </tile>
 <tile id="61">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_impala0_massaiChief0.png"/>
 </tile>
 <tile id="62">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_impala0_suit0.png"/>
 </tile>
 <tile id="63">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_lizard0_massai0.png"/>
 </tile>
 <tile id="64">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/fe_lizard0_scarfDriver0.png"/>
 </tile>
 <tile id="65">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_bushbaby0_kanzu0.png"/>
 </tile>
 <tile id="66">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus0_farmer0.png"/>
 </tile>
 <tile id="67">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus0_rapper0.png"/>
 </tile>
 <tile id="68">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus1_kanzu0.png"/>
 </tile>
 <tile id="69">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_colobus1_suit0.png"/>
 </tile>
 <tile id="70">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_dog0_massaiFarmer0.png"/>
 </tile>
 <tile id="71">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_impala0_kid0.png"/>
 </tile>
 <tile id="72">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="256" height="320" source="../../../rawAssets/objects/character/ragdoll0/ma_impala0_massaiFarmer0.png"/>
 </tile>
 <tile id="73">
  <properties>
   <property name="move:speedX" type="float" value="2"/>
  </properties>
  <image width="320" height="320" source="../../../rawAssets/objects/character/hyena0/hyena0.png"/>
 </tile>
 <tile id="74">
  <properties>
   <property name="life:hitpoints" type="int" value="7"/>
   <property name="life:hitpoints_max" type="int" value="7"/>
   <property name="move:speedX" type="float" value="4"/>
   <property name="move:speedY" type="float" value="4"/>
   <property name="view" type="float" value="1000"/>
  </properties>
  <image width="1024" height="1024" source="../../../rawAssets/objects/character/marabu0/marabu0.png"/>
 </tile>
 <tile id="75">
  <properties>
   <property name="climb:speedY" type="float" value="2"/>
   <property name="life:hitpoints" type="int" value="5"/>
   <property name="life:hitpoints_max" type="int" value="5"/>
   <property name="move:speedX" type="float" value="4"/>
  </properties>
  <image width="512" height="512" source="../../../rawAssets/objects/character/monkey0/baboon0.png"/>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="76">
  <image width="1344" height="896" source="../../../rawAssets/objects/character/elephant0/elephant0_gray1.png"/>
 </tile>
 <tile id="77">
  <image width="768" height="512" source="../../../rawAssets/objects/vehicle/motorbike1/bajaji0_green0.png"/>
 </tile>
 <tile id="78">
  <image width="768" height="512" source="../../../rawAssets/objects/vehicle/motorbike1/bajaji0_red0.png"/>
 </tile>
 <tile id="79">
  <image width="1024" height="512" source="../../../rawAssets/objects/character/monkey0_bike/kawaida0_bike.png"/>
 </tile>
</tileset>
