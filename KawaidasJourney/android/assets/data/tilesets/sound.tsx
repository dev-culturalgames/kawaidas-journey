<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="sound" tilewidth="128" tileheight="128" tilecount="11" columns="0">
 <properties>
  <property name="category" value="object"/>
  <property name="environment" value="global_character"/>
  <property name="logics" value="ambient"/>
  <property name="utility" type="bool" value="true"/>
  <property name="visible" type="bool" value="false"/>
 </properties>
 <tile id="13">
  <properties>
   <property name="ambient:atmo" type="file" value="../../sfx/ambient/amb_beach/beach_ambient0.mp3"/>
   <property name="ambient:delay" type="float" value="6"/>
   <property name="ambient:delay_randomize" type="float" value="2"/>
   <property name="ambient:rangeX" type="float" value="-1"/>
   <property name="ambient:rangeY" type="float" value="-1"/>
   <property name="sfx:birds2" type="file" value="../../sfx/ambient/amb_global/birds2.mp3"/>
   <property name="sfx:birds2:prop" type="float" value="0.20000000000000001"/>
   <property name="sfx:wave1" type="file" value="../../sfx/ambient/amb_global/wave0.mp3"/>
   <property name="sfx:wave1:prop" type="float" value="0.40000000000000002"/>
   <property name="sfx:wave2" type="file" value="../../sfx/ambient/amb_global/wave3.mp3"/>
   <property name="sfx:wave2:prop" type="float" value="0.40000000000000002"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_beach.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="ambient:delay" type="float" value="5"/>
   <property name="ambient:delay_randomize" type="float" value="1"/>
   <property name="ambient:rangeX" type="float" value="-1"/>
   <property name="ambient:rangeY" type="float" value="-1"/>
   <property name="sfx:car_driveBy" type="file" value="../../sfx/ambient/amb_dar/car_driveBy.mp3"/>
   <property name="sfx:car_driveBy:prop" type="float" value="0.14999999999999999"/>
   <property name="sfx:car_honk" type="file" value="../../sfx/ambient/amb_global/car_honk.mp3"/>
   <property name="sfx:car_honk:prop" type="float" value="0.25"/>
   <property name="sfx:car_honk_concert" type="file" value="../../sfx/ambient/amb_dar/car_honk_concert.mp3"/>
   <property name="sfx:car_honk_concert:prop" type="float" value="0.050000000000000003"/>
   <property name="sfx:driveBy" type="file" value="../../sfx/ambient/amb_dar/driveBy.mp3"/>
   <property name="sfx:driveBy:prop" type="float" value="0.25"/>
   <property name="sfx:kids_laughter" type="file" value="../../sfx/ambient/amb_global/kids_laughter.mp3"/>
   <property name="sfx:kids_laughter:prop" type="float" value="0.050000000000000003"/>
   <property name="sfx:kids_laughter:vol" type="float" value="0.69999999999999996"/>
   <property name="sfx:laughter" type="file" value="../../sfx/ambient/amb_dar/laughter.mp3"/>
   <property name="sfx:laughter:prop" type="float" value="0.20000000000000001"/>
   <property name="sfx:police_siren" type="file" value="../../sfx/ambient/amb_dar/police_siren.mp3"/>
   <property name="sfx:police_siren:prop" type="float" value="0.050000000000000003"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_darDay.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="ambient:delay" type="float" value="6"/>
   <property name="ambient:delay_randomize" type="float" value="2"/>
   <property name="ambient:rangeX" type="float" value="-1"/>
   <property name="ambient:rangeY" type="float" value="-1"/>
   <property name="sfx:car_driveBy" type="file" value="../../sfx/ambient/amb_dar/car_driveBy.mp3"/>
   <property name="sfx:car_driveBy:prop" type="float" value="0.20000000000000001"/>
   <property name="sfx:car_honk" type="file" value="../../sfx/ambient/amb_global/car_honk.mp3"/>
   <property name="sfx:car_honk:prop" type="float" value="0.20000000000000001"/>
   <property name="sfx:driveBy" type="file" value="../../sfx/ambient/amb_dar/driveBy.mp3"/>
   <property name="sfx:driveBy:prop" type="float" value="0.20000000000000001"/>
   <property name="sfx:laughter" type="file" value="../../sfx/ambient/amb_dar/laughter.mp3"/>
   <property name="sfx:laughter:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:police_siren" type="file" value="../../sfx/ambient/amb_dar/police_siren.mp3"/>
   <property name="sfx:police_siren:prop" type="float" value="0.29999999999999999"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_darNight.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="ambient:atmo" type="file" value="../../sfx/ambient/amb_forest/forest_ambient0.mp3"/>
   <property name="ambient:delay" type="float" value="3"/>
   <property name="ambient:delay_randomize" type="float" value="1"/>
   <property name="ambient:rangeX" type="float" value="-1"/>
   <property name="ambient:rangeY" type="float" value="-1"/>
   <property name="sfx:birdShout0" type="file" value="../../sfx/ambient/amb_forest/birdShout0.mp3"/>
   <property name="sfx:birdShout0:prop" type="float" value="0.11"/>
   <property name="sfx:birdShout3" type="file" value="../../sfx/ambient/amb_forest/birdShout3.mp3"/>
   <property name="sfx:birdShout3:prop" type="float" value="0.11"/>
   <property name="sfx:birdShout4" type="file" value="../../sfx/ambient/amb_forest/birdShout4.mp3"/>
   <property name="sfx:birdShout4:prop" type="float" value="0.11"/>
   <property name="sfx:birdShout5" type="file" value="../../sfx/ambient/amb_forest/birdShout5.mp3"/>
   <property name="sfx:birdShout5:prop" type="float" value="0.11"/>
   <property name="sfx:birds0" type="file" value="../../sfx/ambient/amb_global/birds0.mp3"/>
   <property name="sfx:birds0:prop" type="float" value="0.11"/>
   <property name="sfx:birds1" type="file" value="../../sfx/ambient/amb_global/birds1.mp3"/>
   <property name="sfx:birds1:prop" type="float" value="0.11"/>
   <property name="sfx:birds2" type="file" value="../../sfx/ambient/amb_global/birds2.mp3"/>
   <property name="sfx:birds2:prop" type="float" value="0.11"/>
   <property name="sfx:jungle_bird0" type="file" value="../../sfx/ambient/amb_forest/jungle_bird0.mp3"/>
   <property name="sfx:jungle_bird0:prop" type="float" value="0.11"/>
   <property name="sfx:monkey0" type="file" value="../../sfx/ambient/amb_forest/monkey0.mp3"/>
   <property name="sfx:monkey0:prop" type="float" value="0.11"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_forest.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="ambient:atmo" type="file" value="../../sfx/ambient/amb_forest/forest_ambient1.mp3"/>
   <property name="ambient:maxProgress" type="float" value="0.40000000000000002"/>
   <property name="ambient:minProgress" type="float" value="0"/>
   <property name="ambient:rangeX" type="float" value="-1"/>
   <property name="ambient:rangeY" type="float" value="-1"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_forest_evo0.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="ambient:atmo" type="file" value="../../sfx/ambient/amb_forest/forest_ambient2.mp3"/>
   <property name="ambient:delay" type="float" value="5"/>
   <property name="ambient:delay_randomize" type="float" value="2"/>
   <property name="ambient:maxProgress" type="float" value="0.59999999999999998"/>
   <property name="ambient:minProgress" type="float" value="0.40000000000000002"/>
   <property name="ambient:rangeX" type="float" value="-1"/>
   <property name="ambient:rangeY" type="float" value="-1"/>
   <property name="sfx:birdShout3" type="file" value="../../sfx/ambient/amb_forest/birdShout3.mp3"/>
   <property name="sfx:birdShout3:prop" type="float" value="0.29999999999999999"/>
   <property name="sfx:birds2" type="file" value="../../sfx/ambient/amb_global/birds2.mp3"/>
   <property name="sfx:birds2:prop" type="float" value="0.5"/>
   <property name="sfx:jungle_bird1" type="file" value="../../sfx/ambient/amb_forest/jungle_bird1.mp3"/>
   <property name="sfx:jungle_bird1:prop" type="float" value="0.20000000000000001"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_forest_evo1.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="ambient:atmo" type="file" value="../../sfx/ambient/amb_forest/forest_ambient0.mp3"/>
   <property name="ambient:delay" type="float" value="3"/>
   <property name="ambient:delay_randomize" type="float" value="1"/>
   <property name="ambient:maxProgress" type="float" value="1"/>
   <property name="ambient:minProgress" type="float" value="0.59999999999999998"/>
   <property name="ambient:rangeX" type="float" value="-1"/>
   <property name="ambient:rangeY" type="float" value="-1"/>
   <property name="sfx:birdShout0" type="file" value="../../sfx/ambient/amb_forest/birdShout0.mp3"/>
   <property name="sfx:birdShout0:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:birdShout1" type="file" value="../../sfx/ambient/amb_forest/birdShout1.mp3"/>
   <property name="sfx:birdShout1:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:birdShout2" type="file" value="../../sfx/ambient/amb_forest/birdShout2.mp3"/>
   <property name="sfx:birdShout2:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:birdShout3" type="file" value="../../sfx/ambient/amb_forest/birdShout3.mp3"/>
   <property name="sfx:birdShout3:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:birdShout4" type="file" value="../../sfx/ambient/amb_forest/birdShout4.mp3"/>
   <property name="sfx:birdShout4:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:birdShout5" type="file" value="../../sfx/ambient/amb_forest/birdShout5.mp3"/>
   <property name="sfx:birdShout5:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:birds0" type="file" value="../../sfx/ambient/amb_global/birds0.mp3"/>
   <property name="sfx:birds0:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:birds1" type="file" value="../../sfx/ambient/amb_global/birds1.mp3"/>
   <property name="sfx:birds1:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:birds2" type="file" value="../../sfx/ambient/amb_global/birds2.mp3"/>
   <property name="sfx:birds2:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:jungle_bird0" type="file" value="../../sfx/ambient/amb_forest/jungle_bird0.mp3"/>
   <property name="sfx:jungle_bird0:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:jungle_bird1" type="file" value="../../sfx/ambient/amb_forest/jungle_bird1.mp3"/>
   <property name="sfx:jungle_bird1:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:lemur" type="file" value="../../sfx/ambient/amb_forest/lemur.mp3"/>
   <property name="sfx:lemur:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:monkey0" type="file" value="../../sfx/ambient/amb_forest/monkey0.mp3"/>
   <property name="sfx:monkey0:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:monkey1" type="file" value="../../sfx/ambient/amb_forest/monkey1.mp3"/>
   <property name="sfx:monkey1:prop" type="float" value="0.059999999999999998"/>
   <property name="sfx:snakeHiss" type="file" value="../../sfx/ambient/amb_forest/snakeHiss.mp3"/>
   <property name="sfx:snakeHiss:prop" type="float" value="0.059999999999999998"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_forest_evo2.png"/>
 </tile>
 <tile id="20">
  <properties>
   <property name="ambient:atmo" type="file" value="../../sfx/ambient/amb_forest/forest_ambient0.mp3"/>
   <property name="ambient:delay" type="float" value="3"/>
   <property name="ambient:delay_randomize" type="float" value="1"/>
   <property name="ambient:rangeX" type="float" value="0"/>
   <property name="ambient:rangeY" type="float" value="0"/>
   <property name="sfx:birdShout0" type="file" value="../../sfx/ambient/amb_forest/birdShout0.mp3"/>
   <property name="sfx:birdShout0:prop" type="float" value="0.11"/>
   <property name="sfx:birdShout3" type="file" value="../../sfx/ambient/amb_forest/birdShout3.mp3"/>
   <property name="sfx:birdShout3:prop" type="float" value="0.11"/>
   <property name="sfx:birdShout4" type="file" value="../../sfx/ambient/amb_forest/birdShout4.mp3"/>
   <property name="sfx:birdShout4:prop" type="float" value="0.11"/>
   <property name="sfx:birdShout5" type="file" value="../../sfx/ambient/amb_forest/birdShout5.mp3"/>
   <property name="sfx:birdShout5:prop" type="float" value="0.11"/>
   <property name="sfx:birds0" type="file" value="../../sfx/ambient/amb_global/birds0.mp3"/>
   <property name="sfx:birds0:prop" type="float" value="0.11"/>
   <property name="sfx:birds1" type="file" value="../../sfx/ambient/amb_global/birds1.mp3"/>
   <property name="sfx:birds1:prop" type="float" value="0.11"/>
   <property name="sfx:birds2" type="file" value="../../sfx/ambient/amb_global/birds2.mp3"/>
   <property name="sfx:birds2:prop" type="float" value="0.11"/>
   <property name="sfx:jungle_bird0" type="file" value="../../sfx/ambient/amb_forest/jungle_bird0.mp3"/>
   <property name="sfx:jungle_bird0:prop" type="float" value="0.11"/>
   <property name="sfx:monkey0" type="file" value="../../sfx/ambient/amb_forest/monkey0.mp3"/>
   <property name="sfx:monkey0:prop" type="float" value="0.11"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_forest_plant.png"/>
 </tile>
 <tile id="21">
  <properties>
   <property name="ambient:atmo" type="file" value="../../sfx/ambient/amb_savannah/savannah_ambient0.mp3"/>
   <property name="ambient:delay" type="float" value="15"/>
   <property name="ambient:delay_randomize" type="float" value="5"/>
   <property name="ambient:rangeX" type="float" value="-1"/>
   <property name="ambient:rangeY" type="float" value="-1"/>
   <property name="sfx:elephant_hoot0" type="file" value="../../sfx/ambient/amb_savannah/elephant_hoot0.mp3"/>
   <property name="sfx:elephant_hoot0:prop" type="float" value="0.5"/>
   <property name="sfx:lion_growl0" type="file" value="../../sfx/ambient/amb_savannah/lion_growl0.mp3"/>
   <property name="sfx:lion_growl0:prop" type="float" value="0.5"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_savanna.png"/>
 </tile>
 <tile id="22">
  <properties>
   <property name="ambient:delay" type="float" value="5"/>
   <property name="ambient:delay_randomize" type="float" value="2"/>
   <property name="ambient:rangeX" type="float" value="-1"/>
   <property name="ambient:rangeY" type="float" value="-1"/>
   <property name="sfx:car_honk" type="file" value="../../sfx/ambient/amb_global/car_honk.mp3"/>
   <property name="sfx:car_honk:prop" type="float" value="0.20000000000000001"/>
   <property name="sfx:kids_laughter" type="file" value="../../sfx/ambient/amb_global/kids_laughter.mp3"/>
   <property name="sfx:kids_laughter:prop" type="float" value="0.20000000000000001"/>
   <property name="sfx:kids_laughter:vol" type="float" value="0.69999999999999996"/>
   <property name="sfx:laughter" type="file" value="../../sfx/ambient/amb_dar/laughter.mp3"/>
   <property name="sfx:laughter:prop" type="float" value="0.20000000000000001"/>
   <property name="sfx:stoneTown0" type="file" value="../../sfx/ambient/amb_stoneTown/stoneTown0.mp3"/>
   <property name="sfx:stoneTown0:prop" type="float" value="0.20000000000000001"/>
   <property name="sfx:wave0" type="file" value="../../sfx/ambient/amb_global/wave0.mp3"/>
   <property name="sfx:wave0:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:wave3" type="file" value="../../sfx/ambient/amb_global/wave3.mp3"/>
   <property name="sfx:wave3:prop" type="float" value="0.10000000000000001"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_stoneTown.png"/>
 </tile>
 <tile id="23">
  <properties>
   <property name="ambient:delay" type="float" value="5"/>
   <property name="ambient:delay_randomize" type="float" value="2.5"/>
   <property name="ambient:rangeX" type="float" value="-1"/>
   <property name="ambient:rangeY" type="float" value="-1"/>
   <property name="sfx:birds0" type="file" value="../../sfx/ambient/amb_global/birds0.mp3"/>
   <property name="sfx:birds0:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:birds1" type="file" value="../../sfx/ambient/amb_global/birds1.mp3"/>
   <property name="sfx:birds1:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:birds2" type="file" value="../../sfx/ambient/amb_global/birds2.mp3"/>
   <property name="sfx:birds2:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:cow0" type="file" value="../../sfx/ambient/amb_village/cow0.mp3"/>
   <property name="sfx:cow0:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:cow1" type="file" value="../../sfx/ambient/amb_village/cow1.mp3"/>
   <property name="sfx:cow1:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:goat0" type="file" value="../../sfx/ambient/amb_village/goat0.mp3"/>
   <property name="sfx:goat0:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:goat1" type="file" value="../../sfx/ambient/amb_village/goat1.mp3"/>
   <property name="sfx:goat1:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:rooster0" type="file" value="../../sfx/ambient/amb_village/rooster0.mp3"/>
   <property name="sfx:rooster0:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:village0" type="file" value="../../sfx/ambient/amb_village/village0.mp3"/>
   <property name="sfx:village0:prop" type="float" value="0.10000000000000001"/>
   <property name="sfx:village1" type="file" value="../../sfx/ambient/amb_village/village1.mp3"/>
   <property name="sfx:village1:prop" type="float" value="0.10000000000000001"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/ambient/amb_village.png"/>
 </tile>
</tileset>
