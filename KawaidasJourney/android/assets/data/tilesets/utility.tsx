<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.5" name="utility" tilewidth="128" tileheight="128" tilecount="11" columns="8">
 <grid orientation="orthogonal" width="256" height="256"/>
 <tile id="0">
  <properties>
   <property name="delay" type="float" value="0"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/dialogue.png"/>
 </tile>
 <tile id="1">
  <image width="128" height="128" source="../../../rawAssets/utility/playSFX.png"/>
 </tile>
 <tile id="2">
  <image width="128" height="128" source="../../../rawAssets/utility/stopSFX.png"/>
 </tile>
 <tile id="3">
  <image width="128" height="128" source="../../../rawAssets/utility/point.png"/>
 </tile>
 <tile id="4">
  <image width="128" height="128" source="../../../rawAssets/utility/teleport.png"/>
 </tile>
 <tile id="7">
  <image width="128" height="128" source="../../../rawAssets/utility/execute.png"/>
 </tile>
 <tile id="9">
  <image width="128" height="128" source="../../../rawAssets/utility/music.png"/>
 </tile>
 <tile id="10">
  <image width="128" height="128" source="../../../rawAssets/utility/ambient.png"/>
 </tile>
 <tile id="12">
  <image width="128" height="128" source="../../../rawAssets/utility/info.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="spawn:check_wp" type="bool" value="true"/>
   <property name="spawn:delay" type="float" value="0"/>
   <property name="spawn:delay_randomize" type="float" value="0"/>
   <property name="spawn:loop" type="bool" value="true"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/spawn.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="cam:accelerationX" type="float" value="0.5"/>
   <property name="cam:accelerationY" type="float" value="2"/>
   <property name="cam:edgeSnappingX" type="bool" value="true"/>
   <property name="cam:edgeSnappingY" type="bool" value="true"/>
   <property name="cam:maxX" type="float" value="0"/>
   <property name="cam:maxY" type="float" value="0"/>
   <property name="cam:minX" type="float" value="0"/>
   <property name="cam:minY" type="float" value="0"/>
   <property name="cam:modeX" value="forward_focus"/>
   <property name="cam:modeY" value="forward_focus"/>
   <property name="cam:offsetBreak" type="float" value="256"/>
   <property name="cam:offsetX" type="float" value="128"/>
   <property name="cam:offsetXCatchMax" type="float" value="576"/>
   <property name="cam:offsetXCatchMin" type="float" value="576"/>
   <property name="cam:offsetXLock" type="float" value="0"/>
   <property name="cam:offsetY" type="float" value="64"/>
   <property name="cam:offsetYCatchMax" type="float" value="256"/>
   <property name="cam:offsetYCatchMin" type="float" value="320"/>
   <property name="cam:offsetYLock" type="float" value="0"/>
   <property name="cam:platformOffset" type="float" value="64"/>
   <property name="cam:platformSnapping" type="bool" value="true"/>
   <property name="cam:speedXMax" type="float" value="4"/>
   <property name="cam:speedYMax" type="float" value="4"/>
   <property name="cam:zoom" type="float" value="1.8"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/utility/camera.png"/>
 </tile>
</tileset>
