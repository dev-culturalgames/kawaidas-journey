<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="platforms_KB" tilewidth="2688" tileheight="1792" tilecount="71" columns="6">
 <grid orientation="orthogonal" width="512" height="512"/>
 <tile id="0">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_dark#0.png"/>
 </tile>
 <tile id="1">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_dark#1.png"/>
 </tile>
 <tile id="2">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_dark#2.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="category" value="tiled"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_light#0.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="category" value="tiled"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_light#1.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="category" value="tiled"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform1/platform1_light#2.png"/>
 </tile>
 <tile id="6">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_dark#0.png"/>
 </tile>
 <tile id="7">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_dark#1.png"/>
 </tile>
 <tile id="8">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_dark#2.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="move:speedX" type="float" value="3"/>
   <property name="move:speedY" type="float" value="3"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_light#0.png"/>
 </tile>
 <tile id="10">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_light#1.png"/>
 </tile>
 <tile id="11">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/platform0/platform0_light#2.png"/>
 </tile>
 <tile id="12">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_stump0.png"/>
 </tile>
 <tile id="13">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_trunk0.png"/>
 </tile>
 <tile id="14">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_trunk1.png"/>
 </tile>
 <tile id="15">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_crown0#0.png"/>
 </tile>
 <tile id="16">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_crown0#1.png"/>
 </tile>
 <tile id="17">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_crown0#2.png"/>
 </tile>
 <tile id="18">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_branch0.png"/>
 </tile>
 <tile id="19">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_branch1.png"/>
 </tile>
 <tile id="20">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_top0.png"/>
 </tile>
 <tile id="21">
  <properties>
   <property name="move:speedX" type="float" value="15"/>
   <property name="move:speedY" type="float" value="15"/>
  </properties>
  <image width="512" height="256" source="../../../rawAssets/objects/character/beamCloud0/beamCloud0_icon.png"/>
 </tile>
 <tile id="30">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#0.png"/>
 </tile>
 <tile id="31">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#1.png"/>
 </tile>
 <tile id="32">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#2.png"/>
 </tile>
 <tile id="33">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#3.png"/>
 </tile>
 <tile id="34">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#4.png"/>
 </tile>
 <tile id="35">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#5.png"/>
 </tile>
 <tile id="36">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#6.png"/>
 </tile>
 <tile id="37">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#7.png"/>
 </tile>
 <tile id="38">
  <image width="128" height="256" source="../../../rawAssets/objects/decoration/tiled/stoneWall0/stoneWall0_light#8.png"/>
 </tile>
 <tile id="45">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_blue#0.png"/>
 </tile>
 <tile id="46">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_blue#1.png"/>
 </tile>
 <tile id="47">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_cyan#0.png"/>
 </tile>
 <tile id="48">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_cyan#1.png"/>
 </tile>
 <tile id="49">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_red#0.png"/>
 </tile>
 <tile id="50">
  <image width="256" height="64" source="../../../rawAssets/objects/decoration/tiled/ledge0/ledge0_red#1.png"/>
 </tile>
 <tile id="51">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_branch2#0.png"/>
 </tile>
 <tile id="52">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_branch2#1.png"/>
 </tile>
 <tile id="53">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/tree6/tree6_branch2#2.png"/>
 </tile>
 <tile id="54">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerPole0/powerPole0_bot0.png"/>
 </tile>
 <tile id="55">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerPole0/powerPole0_mid0.png"/>
 </tile>
 <tile id="56">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerPole0/powerPole0_top0.png"/>
 </tile>
 <tile id="57">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerPole0/powerPole0_top1.png"/>
 </tile>
 <tile id="58">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_horizontal0.png"/>
 </tile>
 <tile id="59">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_dia0#1.png"/>
 </tile>
 <tile id="60">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_dia0#0.png"/>
 </tile>
 <tile id="66">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_curve0#0.png"/>
 </tile>
 <tile id="67">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_curve0#1.png"/>
 </tile>
 <tile id="68">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_curve0#2.png"/>
 </tile>
 <tile id="69">
  <image width="255" height="256" source="../../../rawAssets/objects/decoration/tiled/powerPole0/powerPole0_top2.png"/>
 </tile>
 <tile id="70">
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/tiled/palm7/palm7_stump0.png"/>
 </tile>
 <tile id="71">
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/tiled/palm7/palm7_trunk0.png"/>
 </tile>
 <tile id="72">
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/tiled/palm7/palm7_crown0.png"/>
 </tile>
 <tile id="75">
  <image width="1024" height="1024" source="../../../rawAssets/objects/decoration/baobab1.png"/>
 </tile>
 <tile id="76">
  <image width="768" height="512" source="../../../rawAssets/objects/decoration/clothesLine0.png"/>
 </tile>
 <tile id="77">
  <image width="512" height="128" source="../../../rawAssets/objects/decoration/fence0.png"/>
 </tile>
 <tile id="78">
  <image width="1024" height="512" source="../../../rawAssets/objects/decoration/hut1.png"/>
 </tile>
 <tile id="80">
  <image width="256" height="384" source="../../../rawAssets/objects/decoration/shop/shop0#0.png"/>
 </tile>
 <tile id="81">
  <image width="256" height="384" source="../../../rawAssets/objects/decoration/shop/shop0#1.png"/>
 </tile>
 <tile id="82">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_blue0#0.png"/>
 </tile>
 <tile id="83">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_blue0#1.png"/>
 </tile>
 <tile id="84">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_red0#0.png"/>
 </tile>
 <tile id="85">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_red0#1.png"/>
 </tile>
 <tile id="86">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_yellow0#0.png"/>
 </tile>
 <tile id="87">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/shop/blanket0_yellow0#1.png"/>
 </tile>
 <tile id="88">
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/windows/window4.png"/>
 </tile>
 <tile id="90">
  <image width="768" height="640" source="../../../rawAssets/objects/decoration/how0.png"/>
 </tile>
 <tile id="91">
  <image width="512" height="640" source="../../../rawAssets/objects/decoration/twinTowers0.png"/>
 </tile>
 <tile id="92">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/tiled/powerCord0/powerCord0_vertical0.png"/>
 </tile>
 <tile id="93">
  <image width="2688" height="1792" source="../../../rawAssets/objects/decoration/treeHouse0/treeHouse0_icon.png"/>
 </tile>
</tileset>
