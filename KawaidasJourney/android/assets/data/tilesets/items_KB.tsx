<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="items_KB" tilewidth="550" tileheight="512" tilecount="31" columns="10">
 <grid orientation="orthogonal" width="552" height="450"/>
 <tile id="0">
  <properties>
   <property name="move:speedX" type="float" value="15"/>
   <property name="move:speedY" type="float" value="15"/>
   <property name="range" type="float" value="96"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/sun0/sun0_small_icon.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="move:speedX" type="float" value="15"/>
   <property name="move:speedY" type="float" value="15"/>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/sun0/sun0_big_icon.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/banana0/banana0_one_icon.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/banana0/banana0_bunch_icon.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/coconut0/coconut0_one_icon.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/coconut0/coconut0_bunch_icon.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="item:duration" type="float" value="2"/>
   <property name="range" type="float" value="160"/>
   <property name="view" type="float" value="384"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/solarPanel0/solarPanel0_icon.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="item:duration" type="float" value="4"/>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/karafuu0/karafuu0_icon.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/item/totem0/totem0_icon.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="range" type="float" value="700"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/palmSeedling0/palmSeedling0_icon.png"/>
 </tile>
 <tile id="10">
  <image width="128" height="128" source="../../../rawAssets/objects/item/climbHere0/climbHere0_icon.png"/>
 </tile>
 <tile id="11">
  <image width="550" height="227" source="../../../rawAssets/objects/decoration/bush3/bush3_icon.png"/>
 </tile>
 <tile id="12">
  <image width="483" height="450" source="../../../rawAssets/objects/decoration/palms0/palms0_icon.png"/>
 </tile>
 <tile id="13">
  <image width="205" height="267" source="../../../rawAssets/objects/decoration/plant10/plant10_icon.png"/>
 </tile>
 <tile id="14">
  <image width="259" height="259" source="../../../rawAssets/objects/decoration/plant11/plant11_icon.png"/>
 </tile>
 <tile id="15">
  <image width="258" height="130" source="../../../rawAssets/objects/decoration/plant12/plant12_icon.png"/>
 </tile>
 <tile id="16">
  <image width="214" height="150" source="../../../rawAssets/objects/decoration/plant13/plant13_icon.png"/>
 </tile>
 <tile id="17">
  <image width="128" height="128" source="../../../rawAssets/objects/decoration/skull0/skull0_icon.png"/>
 </tile>
 <tile id="18">
  <image width="128" height="512" source="../../../rawAssets/objects/decoration/bumperSign0.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/notes0/note0_one_half.png"/>
 </tile>
 <tile id="20">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/notes0/note1_two_sixteenth.png"/>
 </tile>
 <tile id="21">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="128" height="128" source="../../../rawAssets/objects/item/notes0/note2_one_eight.png"/>
 </tile>
 <tile id="22">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="128" height="256" source="../../../rawAssets/objects/item/extinguisher_icon.png"/>
 </tile>
 <tile id="23">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="192" height="192" source="../../../rawAssets/objects/item/trashRod_icon.png"/>
 </tile>
 <tile id="29">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe0.png"/>
 </tile>
 <tile id="30">
  <properties>
   <property name="cbspawn:count" type="int" value="3"/>
   <property name="cbspawn:range" type="float" value="1024"/>
   <property name="spawn:defName" value="tigerMosquito0"/>
   <property name="spawn:delay" type="float" value="6"/>
   <property name="spawn:loop" type="bool" value="true"/>
   <property name="spawn:max" type="int" value="1"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe0_broken0.png"/>
 </tile>
 <tile id="31">
  <properties>
   <property name="cbspawn:count" type="int" value="3"/>
   <property name="cbspawn:range" type="float" value="1024"/>
   <property name="spawn:defName" value="tigerMosquito0"/>
   <property name="spawn:delay" type="float" value="6"/>
   <property name="spawn:loop" type="bool" value="true"/>
   <property name="spawn:max" type="int" value="1"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe0_broken1.png"/>
 </tile>
 <tile id="32">
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe1.png"/>
 </tile>
 <tile id="33">
  <properties>
   <property name="cbspawn:count" type="int" value="3"/>
   <property name="cbspawn:range" type="float" value="1024"/>
   <property name="spawn:defName" value="tigerMosquito0"/>
   <property name="spawn:delay" type="float" value="6"/>
   <property name="spawn:loop" type="bool" value="true"/>
   <property name="spawn:max" type="int" value="1"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe1_broken0.png"/>
 </tile>
 <tile id="34">
  <properties>
   <property name="cbspawn:count" type="int" value="3"/>
   <property name="cbspawn:range" type="float" value="1024"/>
   <property name="spawn:defName" value="tigerMosquito0"/>
   <property name="spawn:delay" type="float" value="6"/>
   <property name="spawn:loop" type="bool" value="true"/>
   <property name="spawn:max" type="int" value="1"/>
  </properties>
  <image width="256" height="256" source="../../../rawAssets/objects/decoration/pipes/pipe1_broken1.png"/>
 </tile>
 <tile id="35">
  <properties>
   <property name="range" type="float" value="160"/>
  </properties>
  <image width="259" height="386" source="../../../rawAssets/objects/item/secret0/secret0_icon.png"/>
 </tile>
</tileset>
