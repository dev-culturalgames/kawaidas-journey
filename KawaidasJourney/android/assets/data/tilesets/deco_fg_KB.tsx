<?xml version="1.0" encoding="UTF-8"?>
<tileset name="deco_fg_KB" tilewidth="1280" tileheight="512" tilecount="11" columns="0">
 <grid orientation="orthogonal" width="363" height="256"/>
 <tile id="0">
  <image width="384" height="256" source="../../../rawAssets/objects/decoration/leafPlant0/plant0.png"/>
 </tile>
 <tile id="1">
  <image width="384" height="256" source="../../../rawAssets/objects/decoration/leafPlant0/plant1.png"/>
 </tile>
 <tile id="2">
  <image width="384" height="256" source="../../../rawAssets/objects/decoration/leafPlant0/plant2.png"/>
 </tile>
 <tile id="3">
  <image width="384" height="256" source="../../../rawAssets/objects/decoration/leafPlant0/plant3.png"/>
 </tile>
 <tile id="4">
  <image width="384" height="256" source="../../../rawAssets/objects/decoration/leafPlant0/plant4.png"/>
 </tile>
 <tile id="5">
  <image width="1024" height="512" source="../../../rawAssets/objects/decoration/acacia/acacia1-fg.png"/>
 </tile>
 <tile id="7">
  <image width="512" height="256" source="../../../rawAssets/objects/decoration/bushes/bush10-ser-fg.png"/>
 </tile>
 <tile id="8">
  <image width="768" height="512" source="../../../rawAssets/objects/decoration/bushes/bush11-ser-fg.png"/>
 </tile>
 <tile id="9">
  <image width="1152" height="512" source="../../../rawAssets/objects/decoration/acacia/acacia4-fg.png"/>
 </tile>
 <tile id="10">
  <image width="1280" height="512" source="../../../rawAssets/objects/decoration/bushes/bush12-ser-fg.png"/>
 </tile>
 <tile id="11">
  <image width="1280" height="512" source="../../../rawAssets/objects/decoration/bushes/bush13-ser-fg.png"/>
 </tile>
</tileset>
