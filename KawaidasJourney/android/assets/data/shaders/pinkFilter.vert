//incoming Position attribute from our SpriteBatch
attribute vec2 Position;

//the transformation matrix of our SpriteBatch
uniform mat4 u_projTrans;

void main() {
    gl_Position = u_projTrans * vec4(Position, 0.0, 1.0);
}
