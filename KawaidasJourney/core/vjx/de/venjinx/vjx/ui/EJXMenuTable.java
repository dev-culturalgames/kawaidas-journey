package de.venjinx.vjx.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.AudioControl;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;

public abstract class EJXMenuTable extends Table {

    public static final String EJXMenuTable = "EJXMenuTable";

    protected AudioControl audioControl;
    protected MenuStage menuControl;

    public EJXMenuTable(final MenuStage mc, String name) {
        super(mc.getSkin());
        setFillParent(true);
        setName(name);

        audioControl = mc.getGame().sfx;
        menuControl = mc;
    }

    public abstract void updateLanguage(TextControl texts);

    public abstract void updateLayout(Skin skin);

    public abstract void entered(EJXGame g, MenuStage mc);

    public abstract void exited(EJXGame g, MenuStage mc);

    public abstract void enterCallback();

    public abstract Table getContent();

    public abstract void lvlLoaded();
}