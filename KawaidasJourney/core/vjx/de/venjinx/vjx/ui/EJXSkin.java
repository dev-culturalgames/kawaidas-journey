package de.venjinx.vjx.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXFont.FONT_DEF;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXGraphix;
import de.venjinx.vjx.ui.widgets.TextButton;

public abstract class EJXSkin extends Skin {

    public static final String style_default = "default";

    public static final String style_ = "style_";
    public static final String style_transparent_256 = "style_btn_transparent_256";

    public static final Color uiBlank = VJXGraphix.createColor(0, 0, 0, 0);
    public static final Color uiTrans50 = VJXGraphix.createColor(0, 0, 0, .5f);
    public static final Color uiTrans80 = VJXGraphix.createColor(0, 0, 0, .8f);
    public static final Color uiGreen0 = VJXGraphix.createColor(44, 199, 72, 1);
    public static final Color uiRed0 = VJXGraphix.createColor(255, 0, 0, 1);
    public static final Color uiYellow0 = VJXGraphix.createColor(255, 255, 0, 1);
    public static final Color uiLila0 = VJXGraphix.createColor(72, 0, 255, 1);
    public static final Color uiBlue0 = VJXGraphix.createColor(102, 201, 255, 1);

    public EJXSkin(EJXFont font) {
        for (FONT_DEF def : EJXFont.FONT_DEF.values()) {
            add(def.name, font.getLabelStyle(def.name));
            add(def.name, font.getFont(def.name));
        }
    }

    public abstract void init();

    protected void addButtonStyle(String name) {
        String path = name;
        Drawable d0 = getDrawable(path);
        Drawable d1 = has(path + "_down", TextureRegion.class)
                        ? getDrawable(path + "_down") : d0;
                        Drawable d2 = has(path + "_active", TextureRegion.class)
                                        ? getDrawable(path + "_active") : d0;
                                        addButtonStyle(name, d0, d1, d2);
    }

    protected void addButtonStyle(String name, Drawable d0, Drawable d1) {
        addButtonStyle(name, d0, d1, d0);
    }

    protected void addButtonStyle(String name, Drawable d0, Drawable d1, Drawable d2) {
        add(style_ + name, new ButtonStyle(d0, d1, d2));
    }

    public static TextButton createTButton(String bgDrawableName, Skin skin) {
        return createTButton(bgDrawableName, VJXString.STR_EMPTY, skin, null);
    }

    public static TextButton createTButton(String bgDrawableName, Skin skin,
                    ClickListener listener) {
        return createTButton(bgDrawableName, VJXString.STR_EMPTY, skin, listener);
    }

    public static TextButton createTButton(String bgDrawableName, String text,
                    Skin skin, ClickListener listener) {
        TextButton button;
        button = new TextButton(bgDrawableName, text, skin);
        if (listener != null) button.addListener(listener);

        return button;
    }

    public Texture createBackgroundTexture(int width, int height, float r, float g,
                    float b, float a) {
        Pixmap pm = new Pixmap(width, height, Format.RGBA8888);
        pm.setColor(r, g, b, a);
        pm.fill();
        Texture texture = new Texture(pm);
        pm.dispose();
        return texture;
    }

    public Texture createButtonTexture(int w, int h, Color shadow, Color border,
                    Color main1, Color main2, int offset) {
        Pixmap pm = new Pixmap(w, h, Format.RGBA8888);
        VJXGraphix.drawButWindow(pm, border, 0, 10, w - 10, h - 10, 10, offset);
        VJXGraphix.drawGradButWindow(pm, main1, main2, 5, 15, w - 20, h - 20,
                        10, offset);
        Texture tex = new Texture(pm);
        pm.dispose();
        return tex;
    }
}