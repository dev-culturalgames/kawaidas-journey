package de.venjinx.vjx.ui.debug;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import de.venjinx.ejx.EJXCamera;
import de.venjinx.vjx.ui.widgets.ValueLabel;

public class CameraWindow extends Window {

    private static final String title = "Camera ";

    private EJXCamera camera;

    private ValueLabel camTargetLabel;
    private ValueLabel camZoomNameLabel;
    private ValueLabel camSpeedXLabel;
    private ValueLabel camSpeedYLabel;
    private ValueLabel camFollowXLabel;
    private ValueLabel camFollowYLabel;
    private ValueLabel camCatchXLabel;
    private ValueLabel camCatchYLabel;
    private ValueLabel camStoppingXLabel;
    private ValueLabel camStoppingYLabel;
    private ValueLabel camAlignYLabel;
    private ValueLabel camAlignYUpLabel;
    private ValueLabel camAlignYDownLabel;

    public CameraWindow(Skin skin) {
        super(title, skin);
        setName("window_camera");

        float prefW0 = 100f;

        camTargetLabel = new ValueLabel("Target: ", prefW0, skin);
        camZoomNameLabel = new ValueLabel("Zoom: ", prefW0, skin);

        camSpeedXLabel = new ValueLabel("SpeedX: ", prefW0, skin);
        camSpeedYLabel = new ValueLabel("SpeedY: ", prefW0, skin);
        camFollowXLabel = new ValueLabel("FollowX: ", prefW0, skin);
        camFollowYLabel = new ValueLabel("FollowY: ", prefW0, skin);
        camCatchXLabel = new ValueLabel("CatchX: ", prefW0, skin);
        camCatchYLabel = new ValueLabel("CatchY: ", prefW0, skin);
        camStoppingXLabel = new ValueLabel("StopX: ", prefW0, skin);
        camStoppingYLabel = new ValueLabel("StopY: ", prefW0, skin);
        camAlignYLabel = new ValueLabel("AlignY: ", prefW0, skin);
        camAlignYUpLabel = new ValueLabel("AlignUp: ", prefW0, skin);
        camAlignYDownLabel = new ValueLabel("AlignDown: ", prefW0, skin);

        Table valuesTable = new Table();
        valuesTable.add(camTargetLabel).expandX().fill();
        valuesTable.add(camZoomNameLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(camSpeedXLabel).expandX().fill();
        valuesTable.add(camFollowXLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(camSpeedYLabel).expandX().fill();
        valuesTable.add(camFollowYLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(camCatchXLabel).expandX().fill();
        valuesTable.add(camStoppingXLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(camCatchYLabel).expandX().fill();
        valuesTable.add(camStoppingYLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(camAlignYLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(camAlignYUpLabel).expandX().fill();
        valuesTable.add(camAlignYDownLabel).expandX().fill();

        Table mainTable = new Table();
        mainTable.add(valuesTable).expand().fill();

        add(mainTable).expand().fill();
        pack();
    }

    @Override
    public void act(float delta) {
        if (camera == null) return;
        getTitleLabel().setText(title + camera.position);
        if (camera.getCamTarget() != null)
            camTargetLabel.setValue(camera.getCamTarget().getName());
        camZoomNameLabel.setValue(Float.toString(camera.zoom));

        camFollowXLabel.setValue(camera.followX());
        camSpeedXLabel.setValue(Float.toString(camera.getSpeedX()));
        camFollowYLabel.setValue(camera.followY());
        camSpeedYLabel.setValue(Float.toString(camera.getSpeedY()));

        camCatchXLabel.setValue(camera.catchingX());
        camStoppingXLabel.setValue(camera.stoppingX());
        camCatchYLabel.setValue(camera.catchingY());
        camStoppingYLabel.setValue(camera.stoppingY());

        camAlignYLabel.setValue(camera.aligningY());
        camAlignYUpLabel.setValue(camera.aligningYUp());
        camAlignYDownLabel.setValue(camera.aligningYDown());
    }

    public void setCamera(EJXCamera camera) {
        this.camera = camera;
    }
}