package de.venjinx.vjx.ui.debug;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.stages.DebugStage.DebugStats;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.vjx.ui.widgets.ValueLabel;

public class PerformanceWindow extends Window {

    private EJXGame game;

    private ValueLabel updateGameLabel;
    private ValueLabel renderGameLabel;

    private ValueLabel updateObjectsLabel;
    private ValueLabel renderObjectsLabel;

    private ValueLabel updateB2DLabel;
    private ValueLabel processLifeCycleLabel;

    private ValueLabel updateLevelLabel;
    private ValueLabel renderLevelLabel;

    private ValueLabel updateHUDLabel;
    private ValueLabel renderHUDLabel;

    private ValueLabel objectsOnScreenLabel;

    public PerformanceWindow(EJXGame game, Skin skin) {
        super("Performance", skin);
        setName("window_performance");

        this.game = game;

        float prefW = 100f;
        updateGameLabel = new ValueLabel("Screen update: ", prefW, skin);
        renderGameLabel = new ValueLabel("Screen render: ", prefW, skin);

        updateLevelLabel = new ValueLabel("Level update: ", prefW, skin);
        renderLevelLabel = new ValueLabel("Level render: ", prefW, skin);

        updateObjectsLabel = new ValueLabel("Objects update: ", prefW, skin);
        renderObjectsLabel = new ValueLabel("Objects render: ", prefW, skin);

        updateB2DLabel = new ValueLabel("Physics update: ", prefW, skin);
        processLifeCycleLabel = new ValueLabel("Lifecycle update: ", prefW, skin);

        updateHUDLabel = new ValueLabel("HUD update: ", prefW, skin);
        renderHUDLabel = new ValueLabel("HUD render: ", prefW, skin);

        objectsOnScreenLabel = new ValueLabel("Objects onScreen: ", prefW, skin);

        Table valuesTable = new Table();
        valuesTable.add(updateGameLabel).expandX().fill();
        valuesTable.add(renderGameLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(updateLevelLabel).expandX().fill();
        valuesTable.add(renderLevelLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(updateObjectsLabel).expandX().fill();
        valuesTable.add(renderObjectsLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(updateB2DLabel).expandX().fill();
        valuesTable.add(processLifeCycleLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(updateHUDLabel).expandX().fill();
        valuesTable.add(renderHUDLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(objectsOnScreenLabel).expandX().fill();

        Table mainTable = new Table();
        mainTable.add(valuesTable).expand().fill();

        add(mainTable).expand().fill();
        pack();
    }

    private long nanoToMilli = 1000;

    @Override
    public void act(float delta) {
        long updateScreen = DebugStats.screenUpdateTime / nanoToMilli;
        long renderScreen = DebugStats.screenRenderTime / nanoToMilli;

        long updateLevel = DebugStats.levelUpdateTime / nanoToMilli;
        long renderLevel = DebugStats.levelRenderTime / nanoToMilli;

        long updateObjects = DebugStats.objectsUpdateTime / nanoToMilli;
        long renderObjects = DebugStats.objectsRenderTime / nanoToMilli;

        long updateHUD = DebugStats.hudUpdateTime / nanoToMilli;
        long renderHUD = DebugStats.hudRenderTime / nanoToMilli;

        long b2d = DebugStats.b2dStepTime / nanoToMilli;
        long processLC = DebugStats.processLifecycleTime / nanoToMilli;

        updateGameLabel.setValue(updateScreen / 1000f + "ms");
        renderGameLabel.setValue(renderScreen / 1000f + "ms");

        updateLevelLabel.setValue(updateLevel / 1000f + "ms");
        renderLevelLabel.setValue(renderLevel / 1000f + "ms");

        updateObjectsLabel.setValue(updateObjects / 1000f + "ms");
        renderObjectsLabel.setValue(renderObjects / 1000f + "ms");

        updateHUDLabel.setValue(updateHUD / 1000f + "ms");
        renderHUDLabel.setValue(renderHUD / 1000f + "ms");

        updateB2DLabel.setValue(b2d / 1000f + "ms");
        processLifeCycleLabel.setValue(processLC / 1000f + "ms");

        LevelStage level = game.getScreen().getLevel();
        if (level != null)
            objectsOnScreenLabel.setValue(Integer.toString(
                            level.getOnScreenObjects()));
    }
}