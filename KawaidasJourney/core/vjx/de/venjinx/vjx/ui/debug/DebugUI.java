package de.venjinx.vjx.ui.debug;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.stages.DebugStage;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.vjx.ui.widgets.ValueLabel;

public class DebugUI extends Window {

    private DebugStage dbgStage;
    private Preferences settings;

    // debug windows
    private CameraWindow cameraWindow;
    private PerformanceWindow performanceWindow;
    private GraphicsWindow graphicsWindow;
    private MemoryWindow memoryWindow;
    private ScenegraphWindow scenegraphWindow;
    private EntityWindow entityWindow;
    private LevelStatsWindow levelStatsWindow;

    private Window customWindow;

    private Table drawStuffButtons;

    // game info
    private ValueLabel fpsLabel;
    private ValueLabel frameLabel;
    private ValueLabel deltaLabel;

    private CheckBox cameraCheckbox;
    private CheckBox graphicsCheckbox;

    private CheckBox performanceCheckbox;
    private CheckBox memoryCheckbox;

    private CheckBox scenegraphCheckbox;
    private CheckBox entityCheckbox;
    private CheckBox levelStatsCheckbox;

    private CheckBox customCheckbox;

    private CheckBox drawMouse;
    private CheckBox dbgScene;
    private CheckBox dbgBox2D;
    private CheckBox dbgGrid;

    private TextButton btnShowSaves;

    public DebugUI(final EJXGame game, DebugStage debugStage, Skin skin) {
        super("Debug", skin);
        setResizable(true);
        setWidth(512);

        dbgStage = debugStage;
        settings = debugStage.getSettings();

        cameraWindow = new CameraWindow(skin);
        performanceWindow = new PerformanceWindow(game, skin);
        graphicsWindow = new GraphicsWindow(debugStage.getGLProfiler(), skin);
        memoryWindow = new MemoryWindow(game, skin);
        scenegraphWindow = new ScenegraphWindow(game, skin);
        entityWindow = new EntityWindow(skin);
        levelStatsWindow = new LevelStatsWindow(game, skin);

        fpsLabel = new ValueLabel("FPS: ", 75f, skin);
        deltaLabel = new ValueLabel("Delta: ", 75f, skin);
        frameLabel = new ValueLabel("Frame: ", 75f, skin);

        Table t = new Table();
        t.add(fpsLabel).expandX().fill();
        t.add(deltaLabel).expandX().fill();
        t.add(frameLabel).expandX().fill();

        add(t).expandX().fill();
        row();

        drawMouse = new CheckBox(" Draw mouse", skin);
        drawMouse.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dbgStage.setDrawMouse(drawMouse.isChecked());
            }
        });
        dbgGrid = new CheckBox(" Draw grid", skin);
        dbgScene = new CheckBox(" Draw bounds", skin);
        dbgBox2D = new CheckBox(" Draw collision", skin);
        ChangeListener changeListener = new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dbgStage.setDebug(dbgScene.isChecked(), dbgBox2D.isChecked(),
                                dbgGrid.isChecked());
            }
        };

        dbgGrid.addListener(changeListener);
        dbgScene.addListener(changeListener);
        dbgBox2D.addListener(changeListener);

        btnShowSaves = new TextButton("Show saves", skin);
        btnShowSaves.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                game.getCloudHandler().showSavedGamesUI();
            }
        });

        float w = getWidth() / 4;
        t = new Table();
        t.add(drawMouse).prefWidth(w).expandX().fill().left();
        t.add(dbgGrid).prefWidth(w).expandX().fill().left();
        t.add(dbgScene).prefWidth(w).expandX().fill().left();
        t.add(dbgBox2D).prefWidth(w).expandX().fill().left();
        t.row();
        t.add(btnShowSaves);

        add(t).expandX().fill();
        row();

        add(createBtns(skin)).expandX().fill();
        row();

        pack();
        setWidth(512);
        setPosition(25, 650);
        performanceWindow.setPosition(getX() + getWidth(),
                        getY() + getHeight() - performanceWindow.getHeight());
        cameraWindow.setPosition(getX() + getWidth(),
                        performanceWindow.getY() - cameraWindow.getHeight());
        scenegraphWindow.setPosition(getX(), getY() - scenegraphWindow.getHeight() - 25);
        memoryWindow.setPosition(
                        scenegraphWindow.getX() + scenegraphWindow.getWidth() + 100,
                        getY() - memoryWindow.getHeight());
        entityWindow.setPosition(cameraWindow.getX() + cameraWindow.getWidth(),
                        performanceWindow.getY() - entityWindow.getHeight());

        //        setDebug(true, true);
    }

    @Override
    public void act(float delta) {
        float deltaFormat = (int) (Gdx.graphics.getDeltaTime() * 1000f) / 1000f;
        fpsLabel.setValue(Float.toString(Gdx.graphics.getFramesPerSecond()));
        deltaLabel.setValue(deltaFormat + "s");
        frameLabel.setValue(Long.toString(Gdx.graphics.getFrameId()));
    }

    public void updateGraphics() {
        graphicsWindow.updateStats();
    }

    public void show(boolean show) {
        if (show) {
            dbgStage.addActor(this);
            dbgStage.setDebug(dbgScene.isChecked(), dbgBox2D.isChecked(),
                            dbgGrid.isChecked());
            load();
        } else {
            remove();
            dbgStage.unfocusAll();
            dbgStage.setDebug(false, false, false);
            save();
        }
    }

    public void setCustomDebug(Window window) {
        customWindow = window;
    }

    public Window getCustomDebug() {
        return customWindow;
    }

    public void updateEntityProps() {
        entityWindow.updateEntityProps();
    }

    public void setSelected(EJXEntity selectedEntity, boolean scroll) {
        scenegraphWindow.setSelected(selectedEntity, scroll);
    }

    public void setLevel(LevelStage stage) {
        scenegraphWindow.setLevel(stage);
        levelStatsWindow.setLevel(stage);
        cameraWindow.setCamera((EJXCamera) stage.getCamera());
    }

    public void updateStats(boolean updateTime) {
        levelStatsWindow.updateStats(updateTime);
    }

    public void resetStats() {
        levelStatsWindow.resetStats();
    }

    private Table createBtns(Skin skin) {

        drawStuffButtons = new Table();
        Table t = new Table();

        //        connectGPSBtn = new TextButton("Connect", skin);
        //        connectGPSBtn.addListener(new ClickListener() {
        //            @Override
        //            public void clicked(InputEvent event, float x, float y) {
        //                super.clicked(event, x, y);
        //
        //                if (!game.getGPSHandler().isConnected()) {
        //                    game.getGPSHandler().connect(VJXString.KJ_SAVE_GAME_NAME);
        //                } else {
        //                    game.getGPSHandler().disconnect(VJXString.KJ_SAVE_GAME_NAME);
        //                }
        //            }
        //        });
        //
        //        showSavesBtn = new TextButton("Show saves", skin);
        //        showSavesBtn.addListener(new ClickListener() {
        //            @Override
        //            public void clicked(InputEvent event, float x, float y) {
        //                super.clicked(event, x, y);
        //                if (game.getGPSHandler().isConnected()) {
        //                    game.getGPSHandler().showSavedGamesUI();
        //                }
        //            }
        //        });

        cameraCheckbox = new CheckBox(" Camera", skin);
        cameraCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!cameraCheckbox.isChecked()) {
                    cameraWindow.remove();
                } else {
                    dbgStage.addActor(cameraWindow);
                }
            }
        });

        graphicsCheckbox = new CheckBox(" Graphics", skin);
        graphicsCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!graphicsCheckbox.isChecked()) {
                    graphicsWindow.remove();
                } else {
                    dbgStage.addActor(graphicsWindow);
                }
            }
        });

        performanceCheckbox = new CheckBox(" Performance", skin);
        performanceCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!performanceCheckbox.isChecked()) {
                    performanceWindow.remove();
                } else {
                    dbgStage.addActor(performanceWindow);
                }
            }
        });

        memoryCheckbox = new CheckBox(" Memory", skin);
        memoryCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!memoryCheckbox.isChecked()) {
                    memoryWindow.remove();
                } else {
                    dbgStage.addActor(memoryWindow);
                }
            }
        });

        scenegraphCheckbox = new CheckBox(" Scenegraph", skin);
        scenegraphCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!scenegraphCheckbox.isChecked()) {
                    scenegraphWindow.remove();
                } else {
                    dbgStage.addActor(scenegraphWindow);
                }
            }
        });

        entityCheckbox = new CheckBox(" Entity", skin);
        entityCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!entityCheckbox.isChecked()) {
                    entityWindow.remove();
                } else {
                    dbgStage.addActor(entityWindow);
                }
            }
        });

        levelStatsCheckbox = new CheckBox(" Level stats", skin);
        levelStatsCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!levelStatsCheckbox.isChecked()) {
                    levelStatsWindow.remove();
                } else {
                    dbgStage.addActor(levelStatsWindow);
                }
            }
        });

        customCheckbox = new CheckBox(" Custom debug", skin);
        customCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (customWindow == null) return;
                if (!customCheckbox.isChecked()) {
                    customWindow.remove();
                } else {
                    dbgStage.addActor(customWindow);
                }
            }
        });

        //        t.add(connectGPSBtn).prefWidth(w).pad(0, 10, 0, 10);
        //        t.add(showSavesBtn).prefWidth(w).pad(0, 10, 0, 10);
        //        t.row();
        t.add(cameraCheckbox).expandX().left();
        t.add(graphicsCheckbox).expandX().left();
        t.row();
        t.add(performanceCheckbox).expandX().left();
        t.add(memoryCheckbox).expandX().left();
        t.row();
        t.add(scenegraphCheckbox).expandX().left();
        t.add(entityCheckbox).expandX().left();
        t.row();
        t.add(levelStatsCheckbox).expandX().left();
        t.add(customCheckbox).expandX().left();

        drawStuffButtons.add(t).expandX().fill();

        return drawStuffButtons;
    }

    private void saveWindow(Window w) {
        settings.putFloat(w.getName() + "_x", w.getX());
        settings.putFloat(w.getName() + "_y", w.getY());

        if (w.isResizable()) {
            settings.putFloat(w.getName() + "_w", w.getWidth());
            settings.putFloat(w.getName() + "_h", w.getHeight());
        }
    }

    public void save() {
        settings.putFloat("window_debug_x", getX());
        settings.putFloat("window_debug_y", getY());

        settings.putBoolean("drawMouse", drawMouse.isChecked());
        settings.putBoolean("dbgScene", dbgScene.isChecked());
        settings.putBoolean("dbgBox2D", dbgBox2D.isChecked());
        settings.putBoolean("dbgGrid", dbgGrid.isChecked());

        settings.putBoolean("window_camera", cameraCheckbox.isChecked());
        saveWindow(cameraWindow);

        settings.putBoolean("window_graphics", graphicsCheckbox.isChecked());
        settings.putBoolean("graphics_profiling_enabled", graphicsWindow.profilingEnabled());
        saveWindow(graphicsWindow);

        settings.putBoolean("window_performance", performanceCheckbox.isChecked());
        saveWindow(performanceWindow);

        settings.putBoolean("window_memory", memoryCheckbox.isChecked());
        saveWindow(memoryWindow);

        settings.putBoolean("window_scenegraph", scenegraphCheckbox.isChecked());
        saveWindow(scenegraphWindow);

        settings.putBoolean("window_entity", entityCheckbox.isChecked());
        saveWindow(entityWindow);

        settings.putBoolean("window_levelStats", levelStatsCheckbox.isChecked());
        saveWindow(levelStatsWindow);

        if (customWindow != null) {
            settings.putBoolean("window_custom", customCheckbox.isChecked());
            saveWindow(customWindow);
        }
    }

    private void loadWindow(Window w) {
        if (w.isResizable()) {
            w.setSize(settings.getFloat(w.getName() + "_w", w.getWidth()),
                            settings.getFloat(w.getName() + "_h", w.getHeight()));
        }
        w.setPosition(settings.getFloat(w.getName() + "_x", w.getX()),
                        settings.getFloat(w.getName() + "_y", w.getY()));
    }

    private void load() {
        setPosition(settings.getFloat("window_debug_x", getX()),
                        settings.getFloat("window_debug_y", getY()));

        drawMouse.setChecked(settings.getBoolean("drawMouse", false));
        dbgScene.setChecked(settings.getBoolean("dbgScene", false));
        dbgBox2D.setChecked(settings.getBoolean("dbgBox2D", false));
        dbgGrid.setChecked(settings.getBoolean("dbgGrid", false));

        cameraCheckbox.setChecked(settings.getBoolean("window_camera", false));
        loadWindow(cameraWindow);

        graphicsCheckbox.setChecked(settings.getBoolean("window_graphics", false));
        loadWindow(graphicsWindow);
        graphicsWindow.setProfilingEnabled(
                        settings.getBoolean("graphics_profiling_enabled", false));

        performanceCheckbox.setChecked(settings.getBoolean("window_performance", false));
        loadWindow(performanceWindow);

        memoryCheckbox.setChecked(settings.getBoolean("window_memory", false));
        loadWindow(memoryWindow);

        scenegraphCheckbox.setChecked(settings.getBoolean("window_scenegraph", false));
        loadWindow(scenegraphWindow);

        entityCheckbox.setChecked(settings.getBoolean("window_entity", false));
        loadWindow(entityWindow);

        levelStatsCheckbox.setChecked(settings.getBoolean("window_levelStats", false));
        loadWindow(levelStatsWindow);

        if (customWindow != null) {
            customCheckbox.setChecked(settings.getBoolean("window_custom", false));
            loadWindow(customWindow);
        }
    }
}