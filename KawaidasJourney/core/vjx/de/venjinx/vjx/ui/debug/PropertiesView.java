package de.venjinx.vjx.ui.debug;

import java.util.HashMap;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.EntityProperty;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;
import de.venjinx.ejx.util.EJXTypes.SpatialProperty;
import de.venjinx.ejx.util.EJXTypes.VJXProperties;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class PropertiesView extends Table {

    public static final String LABEL_Definition = "Source";
    public static final String LABEL_Entity = "Entity";
    public static final String LABEL_Property = "Property";
    public static final String LABEL_Source = "Source";
    public static final String LABEL_Spatial = "Spatial";
    public static final String LABEL_Value = "Value";

    private float fontScale = 1f;
    //    private Vector2 tmpVec;

    private EJXEntity entity;

    private HashMap<EJXPropertyDefinition, Actor> propActors;

    private Table scrollContent;

    public PropertiesView(Skin s) {
        super(s);

        propActors = new HashMap<>();
        //        tmpVec = new Vector2();

        scrollContent = new Table();

        Table t = new Table();
        Label propNameLabel, propValueLabel;
        propNameLabel = new Label(LABEL_Property, s);
        propNameLabel.setFontScale(fontScale);
        t.add(propNameLabel).width(128).fill();

        propValueLabel = new Label(LABEL_Value, s);
        propValueLabel.setFontScale(fontScale);
        t.add(propValueLabel).expandX().fill();

        scrollContent.add(t).expandX().fill();
        scrollContent.row();

        createProperties(LABEL_Source, SourceProperty.values(), s);
        createProperties(LABEL_Definition, DefinitionProperty.values(), s);
        createProperties(LABEL_Spatial, SpatialProperty.values(), s);
        createProperties(LABEL_Entity, EntityProperty.values(), s);

        ScrollPane scroll = new ScrollPane(scrollContent);
        add(scroll).expandX().fill();
        pack();
    }

    public void setEntity(EJXEntity entity) {
        this.entity = entity;
        updateProperties();
        updateEditableProps();
    }

    public void updateProperties() {
        if (entity == null) return;

        //        VJXProperties props = entity.getDef().getVJXProperties();
        //        for (SourceProperty prop : SourceProperty.values()) {
        //            setFromMapProps(prop, props);
        //        }
        //        for (DefinitionProperty prop : DefinitionProperty.values()) {
        //            setFromMapProps(prop, props);
        //        }
        //        for (SpatialProperty prop : SpatialProperty.values()) {
        //            setFromMapProps(prop, props);
        //        }
        //
        //        tmpVec.set(entity.getWorldPosition());
        //        ((Label) propActors.get(EntityProperty.x)).setText(Float.toString(tmpVec.x));
        //        ((Label) propActors.get(EntityProperty.y)).setText(Float.toString(tmpVec.y));
        //
        //        tmpVec.set(entity.getVelocity());
        //        ((Label) propActors.get(EntityProperty.velocityX)).setText(Float.toString(tmpVec.x));
        //        ((Label) propActors.get(EntityProperty.velocityY)).setText(Float.toString(tmpVec.y));
        //
        //        ((Label) propActors.get(EntityProperty.lifecycle)).setText(entity.getLifecycle().name);
        //        ((Label) propActors.get(EntityProperty.activity)).setText(entity.getActivity().name);

        //        VJXEntity target = entity.getTargetEntity();
        //        if (target != null)
        //            ((Label) propActors.get(EntityProperty.target)).setText(target.getName());
        //        else
        //            ((Label) propActors.get(EntityProperty.target)).setText(VJXString.STR_EMPTY);
    }

    public void updateEditableProps() {
        // custom editable properties
        //        ((TextField) propActors.get(CustomProperty.range))
        //        .setText(Float.toString(entity.getRange()));
        //
        //        ((TextField) propActors.get(CustomProperty.view))
        //        .setText(Float.toString(entity.getView()));

        //        LifeLogic l = (LifeLogic) entity.getLogic(VJXLogicType.LIFE);
        //        if (l != null) {
        //            ((TextField) propActors.get(CustomProperty.hitpoints))
        //            .setText(Integer.toString(l.getHP()));
        //
        //            ((TextField) propActors.get(CustomProperty.hitpoints_max))
        //            .setText(Integer.toString(l.getMaxHP()));
        //        }

        //        if (entity instanceof WorldObjectImpl) {
        //            WorldObjectImpl wo = (WorldObjectImpl) entity;
        //            ((TextField) propActors.get(CustomProperty.move_speed))
        //            .setText(Float.toString(wo.getMoveSpeed()));
        //        }

        //        if (entity instanceof VJXCharacter) {
        //            VJXCharacter c = (VJXCharacter) entity;
        //            ((TextField) propActors.get(CustomProperty.climb_speed))
        //            .setText(Float.toString(c.getClimbSpeed()));
        //
        //            ((TextField) propActors.get(CustomProperty.jump_height))
        //            .setText(Float.toString(c.getJumpHeight()));
        //
        //            ((TextField) propActors.get(CustomProperty.jump_speed))
        //            .setText(Float.toString(c.getJumpSpeed()));
        //
        //            ((TextField) propActors.get(CustomProperty.power))
        //            .setText(Float.toString(c.getPower()));
        //        }
    }

    public void applyEditableProperties() {
        //        float f;
        //        int i;

        //        f = Float.parseFloat(((TextField) propActors.get(CustomProperty.range))
        //                        .getText());
        //        entity.setRange(f);
        //
        //        f = Float.parseFloat(((TextField) propActors.get(CustomProperty.view))
        //                        .getText());
        //        entity.setView(f);

        //        LifeLogic l = (LifeLogic) entity.getLogic(VJXLogicType.LIFE);
        //        if (l != null) {
        //            i = Integer.parseInt(((TextField) propActors
        //                            .get(LifeProperty.hitpoints)).getText());
        //            l.setHitPoints(i);
        //
        //            i = Integer.parseInt(((TextField) propActors
        //                            .get(LifeProperty.hitpointsMax)).getText());
        //            l.setMaxHP(i);
        //        }

        //        if (entity instanceof WorldObjectImpl) {
        //            WorldObjectImpl wo = (WorldObjectImpl) entity;
        //            f = Float.parseFloat(((TextField) propActors
        //                            .get(CustomProperty.move_speed)).getText());
        //            wo.setMoveSpeed(f);
        //        }
        //
        //        if (entity instanceof VJXCharacter) {
        //            VJXCharacter c = (VJXCharacter) entity;
        //            f = Float.parseFloat(((TextField) propActors
        //                            .get(CustomProperty.climb_speed)).getText());
        //            c.setClimbSpeed(f);
        //
        //            f = Float.parseFloat(((TextField) propActors
        //                            .get(CustomProperty.jump_height)).getText());
        //            c.setJumpHeight(f);
        //
        //            f = Float.parseFloat(((TextField) propActors
        //                            .get(CustomProperty.jump_speed)).getText());
        //            c.setJumpSpeed(f);
        //
        //            f = Float.parseFloat(
        //                            ((TextField) propActors.get(CustomProperty.power))
        //                            .getText());
        //            c.setPower(f);
        //        }
    }

    private void createProperties(String name,
                    EJXPropertyDefinition[] properties, Skin s) {
        CheckBox checkBox;

        final Table t = new Table();
        checkBox = new CheckBox(name, s);
        scrollContent.add(checkBox).expandX().left();
        scrollContent.row();

        for (EJXPropertyDefinition prop : properties) {
            newProperty(prop, VJXString.STR_EMPTY, t);
        }
        insertEmptyLine(s, t);

        final Cell<Table> cell = scrollContent.add(t).expandX().fill();
        checkBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (((CheckBox) actor).isChecked()) {
                    cell.setActor(t);
                    scrollContent.pack();
                } else {
                    t.remove();
                    scrollContent.pack();
                }
            }
        });
        t.remove();

        scrollContent.row();
    }

    private void newProperty(EJXPropertyDefinition property, String dfltValue,
                    Table t) {
        Label propNameLabel = new Label(property.getName(), getSkin());
        propNameLabel.setFontScale(fontScale);
        t.add(propNameLabel).width(128).fill();

        Actor a;
        CheckBox check;
        Label propValueLabel;
        switch (property.getType()) {
            case boolProp:
                check = new CheckBox(VJXString.STR_EMPTY, getSkin());
                a = check;
                break;

            default:
                propValueLabel = new Label(dfltValue, getSkin());
                propValueLabel.setFontScale(fontScale);
                a = propValueLabel;
                break;
        }

        t.add(a).expandX().fill();
        t.row();

        propActors.put(property, a);
    }

    //    private void createEditProperties(String name,
    //                    EJXPropertyDefinition[] properties, Skin s) {
    //        CheckBox checkBox;
    //
    //        final Table t = new Table();
    //        checkBox = new CheckBox(name, s);
    //        scrollContent.add(checkBox).expandX().left();
    //        scrollContent.row();
    //
    //        for (EJXPropertyDefinition prop : properties) {
    //            newEditProperty(prop, VJXString.STR_EMPTY, t);
    //        }
    //        insertEmptyLine(s, t);
    //
    //        final Cell<Table> cell = scrollContent.add(t).expandX().fill();
    //        checkBox.addListener(new ChangeListener() {
    //            @Override
    //            public void changed(ChangeEvent event, Actor actor) {
    //                if (((CheckBox) actor).isChecked()) {
    //                    cell.setActor(t);
    //                    scrollContent.pack();
    //                } else {
    //                    t.remove();
    //                    scrollContent.pack();
    //                }
    //            }
    //        });
    //        t.remove();
    //
    //        scrollContent.row();
    //
    //        TextButton apply = new TextButton("Apply", s);
    //        apply.addListener(new ClickListener() {
    //            @Override
    //            public void clicked(InputEvent event, float x, float y) {
    //                applyEditableProperties();
    //                getStage().unfocusAll();
    //
    //            }
    //        });
    //        scrollContent.add(apply).expandX().left();
    //    }

    //    private void newEditProperty(EJXPropertyDefinition property,
    //                    String dfltValue, Table t) {
    //        Label propNameLabel = new Label(property.getName(), getSkin());
    //        propNameLabel.setFontScale(fontScale);
    //        t.add(propNameLabel).width(128).fill();
    //
    //        Actor a;
    //        CheckBox check;
    //        TextField valueField;
    //        switch (property.getType()) {
    //            case boolProp:
    //                check = new CheckBox(VJXString.STR_EMPTY, getSkin());
    //                a = check;
    //                break;
    //
    //            default:
    //                valueField = new TextField(dfltValue, getSkin());
    //                a = valueField;
    //                break;
    //        }
    //
    //        t.add(a).expandX().fill();
    //        t.row();
    //
    //        propActors.put(property, a);
    //    }

    private void insertEmptyLine(Skin s, Table t) {
        insertNamedLine(s, null, t);
    }

    private void insertNamedLine(Skin s, String name, Table t) {
        Label propNameLabel, propValueLabel;
        propNameLabel = new Label(name != null ? name : VJXString.STR_EMPTY, s);
        propNameLabel.setFontScale(fontScale);
        t.add(propNameLabel).width(128).fill();

        propValueLabel = new Label(VJXString.STR_EMPTY, s);
        propValueLabel.setFontScale(fontScale);
        t.add(propValueLabel).expandX().fill();
        t.row();
    }

    private void setFromMapProps(EJXPropertyDefinition prop,
                    VJXProperties props) {
        boolean boolValue;
        String strValue;
        switch (prop.getType()) {
            case boolProp:
                boolValue = props.get(prop, Boolean.class);
                ((CheckBox) propActors.get(prop)).setChecked(boolValue);
                break;
            case intProp:
                strValue = Integer.toString(props.get(prop, Integer.class));
                ((Label) propActors.get(prop)).setText(strValue);
                break;
            case floatProp:
                strValue = Float.toString(props.get(prop, Float.class));
                ((Label) propActors.get(prop)).setText(strValue);
                break;
            default:
                if (props.get(prop) == null) break;
                strValue = props.get(prop).toString();
                ((Label) propActors.get(prop)).setText(strValue);
                break;
        }
    }

}
