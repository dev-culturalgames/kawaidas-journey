package de.venjinx.vjx.ui.debug;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.ui.Tree.Node;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Selection;
import com.badlogic.gdx.utils.SnapshotArray;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.stages.LevelStage;

public class ScenegraphWindow extends Window {

    private class ScenegraphNode extends Node<ScenegraphNode, Actor, Actor> {

        public ScenegraphNode(Actor a) {
            super(a);
        }
    }

    private LevelStage level;

    private Tree<ScenegraphNode, Actor> treeView;
    private ScrollPane scrollArea;

    public ScenegraphWindow(final EJXGame game, Skin skin) {
        super("Scenegraph", skin);
        setName("window_scenegraph");
        setResizable(true);
        setResizeBorder(5);

        treeView = new Tree<>(skin);
        scrollArea = new ScrollPane(treeView);

        scrollArea.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Selection<ScenegraphNode> selected = treeView.getSelection();
                if (selected.size() == 0) return;
                ScenegraphNode n = selected.first();
                String s = ((Label) n.getActor()).getText().toString();

                EJXEntity w = level.getEntityByName(s);

                setSelected(w, false);
            }
        });
        scrollArea.validate();

        add(scrollArea).expand().fill();
        pack();
    }

    public void setLevel(LevelStage level) {
        this.level = level;
        treeView.clearChildren();
        Label l = new Label(level.getName() + " (" + level.getEntities().size() + ")", getSkin());
        ScenegraphNode root = new ScenegraphNode(l);

        treeView.add(root);

        SnapshotArray<Actor> children = level.getScene().getChildren();
        for (Actor a : children)
            addNode(a, root);

        pack();
    }

    public void setSelected(EJXEntity selectedEntity, boolean scroll) {
        if (selectedEntity == null) return;

        ScenegraphNode n = treeView.findNode(selectedEntity.getActor());
        if (n == null) return;
        n.expandTo();
        treeView.getSelection().clear();
        treeView.getSelection().choose(n);

        scrollArea.validate();
        if (scroll) scrollArea.scrollTo(n.getActor().getX(),
                                        n.getActor().getY(), 0, 50, false, true);
    }

    private void addNode(Actor a, ScenegraphNode parent) {
        Label l = new Label(a.getName(), getSkin());
        if (a instanceof Group) l.setText(a.getName() + " ("
                + ((Group) a).getChildren().size + ")");
        ScenegraphNode n = new ScenegraphNode(l);
        n.setValue(a);
        parent.add(n);
        if (a instanceof Group) {
            SnapshotArray<Actor> children = ((Group) a).getChildren();
            for (Actor child : children)
                addNode(child, n);
        }
    }

}