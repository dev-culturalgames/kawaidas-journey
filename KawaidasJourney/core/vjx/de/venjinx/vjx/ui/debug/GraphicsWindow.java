package de.venjinx.vjx.ui.debug;

import com.badlogic.gdx.graphics.profiling.GLProfiler;
import com.badlogic.gdx.math.FloatCounter;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import de.venjinx.vjx.ui.widgets.ValueLabel;

public class GraphicsWindow extends Window {

    private GLProfiler profiler;

    private CheckBox enableCheckbox;
    private ValueLabel callsLabel;
    private ValueLabel drawCallsLabel;
    private ValueLabel shaderSwitchesLabel;
    private ValueLabel textureBindingsLabel;

    private Label vertexLabel;
    private ValueLabel vertexCountLabel;
    private ValueLabel vertexTotalLabel;
    private ValueLabel vertexAverageLabel;
    private ValueLabel vertexMinLabel;
    private ValueLabel vertexMaxLabel;
    private ValueLabel vertexValueLabel;

    public GraphicsWindow(final GLProfiler profiler, Skin skin) {
        super("Graphics", skin);
        setName("window_graphics");

        this.profiler = profiler;
        enableCheckbox = new CheckBox(" Enable", skin);
        enableCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (enableCheckbox.isChecked()) profiler.enable();
                else profiler.disable();
            }
        });

        float prefW = 75f;
        callsLabel = new ValueLabel("Calls: ", prefW, skin);
        drawCallsLabel = new ValueLabel("Draw calls: ", prefW, skin);
        shaderSwitchesLabel = new ValueLabel("Shader switches: ", prefW, skin);
        textureBindingsLabel = new ValueLabel("Texture bindings: ", prefW, skin);

        vertexLabel = new Label("Vertices: ", skin);
        vertexCountLabel = new ValueLabel("Count: ", prefW, skin);
        vertexTotalLabel = new ValueLabel("Total: ", prefW, skin);
        vertexAverageLabel = new ValueLabel("Average: ", prefW, skin);
        vertexMinLabel = new ValueLabel("Min: ", prefW, skin);
        vertexMaxLabel = new ValueLabel("Max: ", prefW, skin);
        vertexValueLabel = new ValueLabel("Value: ", prefW, skin);

        Table valuesTable = new Table();
        valuesTable.add(enableCheckbox);
        valuesTable.row();
        valuesTable.add(callsLabel).expandX().fill();
        valuesTable.add(drawCallsLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(shaderSwitchesLabel).expandX().fill();
        valuesTable.add(textureBindingsLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(vertexLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(vertexCountLabel).expandX().fill();
        valuesTable.add(vertexTotalLabel).expandX().fill();
        valuesTable.add(vertexAverageLabel).expandX().fill();
        valuesTable.row();
        valuesTable.add(vertexMinLabel).expandX().fill();
        valuesTable.add(vertexMaxLabel).expandX().fill();
        valuesTable.add(vertexValueLabel).expandX().fill();

        Table mainTable = new Table();
        mainTable.add(valuesTable).expand().fill();

        add(mainTable).expand().fill();
        pack();

        //        setDebug(true, true);
    }

    public void updateStats() {
        callsLabel.setValue(Integer.toString(profiler.getCalls()));
        drawCallsLabel.setValue(Integer.toString(profiler.getDrawCalls()));
        shaderSwitchesLabel.setValue(Integer.toString(profiler.getShaderSwitches()));
        textureBindingsLabel.setValue(Integer.toString(profiler.getTextureBindings()));

        FloatCounter counter = profiler.getVertexCount();
        vertexCountLabel.setValue(Integer.toString(counter.count));
        vertexTotalLabel.setValue(Float.toString(counter.total));
        vertexAverageLabel.setValue(Integer.toString((int) counter.average));
        vertexMinLabel.setValue(Float.toString(counter.min));
        vertexMaxLabel.setValue(Float.toString(counter.max));
        vertexValueLabel.setValue(Float.toString(counter.value));
        profiler.reset();
    }

    public void setProfilingEnabled(boolean enabled) {
        enableCheckbox.setChecked(enabled);
    }

    public boolean profilingEnabled() {
        return enableCheckbox.isChecked();
    }

}