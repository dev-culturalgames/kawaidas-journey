package de.venjinx.vjx.ui.debug;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.stages.MenuStage;

public class DebugUIControl extends MenuStage {

    public DebugUIControl(EJXGame game, String name) {
        super(game, name);
    }

    @Override
    protected void userInit() {
    }

    @Override
    protected void preAct(float delta) {
    }

    @Override
    protected void postAct(float delta) {
    }

    @Override
    protected void preDraw() {
    }

    @Override
    protected void postDraw() {
    }
}