package de.venjinx.vjx.ui.debug;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.AnimationType;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class EntityWindow extends Window {
    private Table entityTable;
    private Cell<Actor> entityPreviewCell;
    private Table entityPreview;
    private PropertiesView entityPropsView;
    private Image entityImage;
    private SpineActor spineActor;
    private TextureRegionDrawable frame;

    public EntityWindow(Skin skin) {
        super("Entity properties", skin);
        setName("window_entity");
        setResizable(true);
        setResizeBorder(5);

        entityTable = new Table(skin);

        getTitleLabel().setText("Entity properties: " + VJXString.STR_NO_NAME);

        entityPreview = new Table(skin);
        entityPreviewCell = entityPreview.add((Actor) entityImage).size(128);

        entityPropsView = new PropertiesView(skin);

        spineActor = new SpineActor("selected_actor");
        spineActor.setSize(128, 128);

        entityTable.add(entityPreview).expandX();
        entityTable.row();
        entityTable.add(entityPropsView).expandX().fill().padTop(10).top();

        add(entityTable).expand().fillX().top();

        //        setDebug(true, true);
        pack();
    }

    public void setSelected(EJXEntity selectedEntity, boolean scroll) {
        entityPreviewCell.setActor(null);
        if (selectedEntity == null) return;

        //        Node n = treeView.findNode(selectedEntity.getActor());
        //        if (n == null) return;
        //        n.expandTo();
        //        treeView.getSelection().clear();
        //        treeView.getSelection().choose(n);

        getTitleLabel().setText("Entity properties: " + selectedEntity.getName());
        entityPropsView.setEntity(selectedEntity);
        if (selectedEntity.getDef().getAnimationType() == AnimationType.SPINE) {
            spineActor.setEntity((AnimatedEntity) selectedEntity);
            entityPreviewCell.setActor(spineActor);
        } else if (selectedEntity.getFrame() != null) {
            frame.setRegion(selectedEntity.getFrame());
            entityPreviewCell.setActor(entityImage);
        }

        //        scrollArea.validate();
        //        if (scroll) scrollArea.scrollTo(n.getActor().getX(),
        //                        n.getActor().getY(), 0, 50, false, true);
    }

    public void updateEntityProps() {
        entityPropsView.updateProperties();
    }

}