package de.venjinx.vjx.ui.debug;

import java.util.Collection;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EntityDef;
import de.venjinx.ejx.entity.logics.EJXLogics;
import de.venjinx.ejx.entity.logics.EJXLogics.EJXLogicType;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.util.EJXTypes.Category;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.widgets.ValueLabel;
import de.venjinx.vjx.ui.widgets.ValueTable;

public class LevelStatsWindow extends Window {

    public static class LevelStats {
        public static int hits = 0;
        public static int deaths = 0;
        public static int tries = 0;
        public static int continues = 0;
        public static long time = 0;
    }

    private LevelStage level;

    private ValueLabel hitsLabel;
    private ValueLabel deathsLabel;
    private ValueLabel triesLabel;
    private ValueLabel continuesLabel;
    private ValueLabel playTimeLabel;

    private ValueTable valueTable;

    private HashMap<Integer, EntityDef> definitions;
    private TreeSet<Integer> objectIDs;
    private HashMap<Integer, Integer> objectCounts;

    private SelectBox<Category> categorySelectBox;
    private SelectBox<EJXLogicType> logicsSelectBox;
    //    private CheckBox enemySelectBox;
    private TextField filterTextField;

    public LevelStatsWindow(EJXGame game, Skin skin) {
        super("Level statistics", skin);
        setName("window_levelStats");
        setResizable(true);

        definitions = game.factory.getDefs();

        objectIDs = new TreeSet<>();
        objectCounts = new HashMap<>();

        createPlayStats();

        createOptions();

        valueTable = new ValueTable("Objects",
                        new String[] { "DefID", "DefName", "Category", "Count" },
                        skin);
        add(valueTable).expand().fill();
        //        setDebug(true, true);
    }

    public void updateStats(boolean updateTime) {
        hitsLabel.setValue(Integer.toString(LevelStats.hits));
        deathsLabel.setValue(Integer.toString(LevelStats.deaths));
        triesLabel.setValue(Integer.toString(LevelStats.tries));
        continuesLabel.setValue(Integer.toString(LevelStats.continues));

        if (updateTime) {
            LevelStats.time = System.currentTimeMillis() - LevelStats.time;
            long seconds = LevelStats.time / 1000;
            long minutes = seconds / 60;
            seconds = seconds % 60;
            playTimeLabel.setValue(minutes + VJXString.SEP_DDOT + seconds);
        }
    }

    public void resetStats() {
        LevelStats.hits = 0;
        LevelStats.deaths = 0;
        LevelStats.tries = 0;
        LevelStats.continues = 0;
        LevelStats.time = System.currentTimeMillis();
        updateStats(false);
    }

    private void filterObjects() {
        valueTable.clearTable();
        objectIDs.clear();
        objectCounts.clear();

        int id;
        int count = 0;
        int total = 0;
        Category cat = categorySelectBox.getSelected();
        EJXLogicType logic = logicsSelectBox.getSelected();

        String filter = filterTextField.getText();
        String[] filters;
        if (!filter.isEmpty() && !filter.equals("<filter>")) {
            filters = filter.split(VJXString.SEP_LIST);
        } else filters = null;

        TreeMap<Long, EJXEntity> entities = level.getEntities();
        for (EJXEntity entity : entities.values()) {
            count = 0;
            id = entity.getDef().getDefID();
            if (cat != Category.ALL && cat != entity.getDef().getCategory())
                continue;

            if (logic != VJXLogicType.TEMPLATE && !entity.hasLogic(logic))
                continue;

            if (filters != null) {
                boolean contains = false;
                for (String s : filters)
                    contains |= entity.getDefName().contains(s);
                if (!contains) continue;
            }

            objectIDs.add(id);
            if (objectCounts.containsKey(id)) count = objectCounts.get(id);
            objectCounts.put(id, count + 1);
            total++;
        }

        EntityDef def;
        for (int defID : objectIDs) {
            def = definitions.get(defID);
            valueTable.addRow(Integer.toString(defID), def.getDefName(),
                            def.getCategory().catName,
                            Integer.toString(objectCounts.get(defID)));
        }
        valueTable.addRow("", "", "Total", Integer.toString(total));
    }

    public void setLevel(LevelStage level) {
        this.level = level;
        getTitleLabel().setText("Level statistics: " + level.getLevelName());
        filterObjects();
    }

    private void createPlayStats() {
        Table playStatsTable = new Table();

        hitsLabel = new ValueLabel("Hits: ", 50, getSkin());
        deathsLabel = new ValueLabel("Deaths: ", 50, getSkin());
        triesLabel = new ValueLabel("Tries: ", 50, getSkin());
        continuesLabel = new ValueLabel("Continues: ", 50, getSkin());
        playTimeLabel = new ValueLabel("Time: ", 50, getSkin());

        playStatsTable.add(hitsLabel);
        playStatsTable.add(deathsLabel);
        playStatsTable.add(triesLabel);
        playStatsTable.add(continuesLabel);
        playStatsTable.row();
        playStatsTable.add(playTimeLabel);

        add(playStatsTable).expandX().left();
        row();
    }

    private void createOptions() {
        Table optionsTable = new Table();
        TextButton updateBtn = new TextButton("Update", getSkin());
        updateBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                filterObjects();
            }
        });

        categorySelectBox = new SelectBox<>(getSkin());
        categorySelectBox.setItems(Category.values());

        logicsSelectBox = new SelectBox<>(getSkin());

        Collection<EJXLogicType> logicKeys = EJXLogics.getLogicTypes();
        EJXLogicType[] logics = new EJXLogicType[logicKeys.size()];
        logicKeys.toArray(logics);
        logicsSelectBox.setItems(logics);
        logicsSelectBox.setSelected(VJXLogicType.TEMPLATE);

        //        enemySelectBox = new CheckBox(" Enemy", getSkin());
        filterTextField = new TextField("<filter>", getSkin());

        optionsTable.add(updateBtn).pad(0, 5, 0, 5);
        optionsTable.add(categorySelectBox).pad(0, 5, 0, 5);
        optionsTable.add(logicsSelectBox).pad(0, 5, 0, 5);
        //        optionsTable.add(enemySelectBox).pad(0, 5, 0, 5);
        optionsTable.add(filterTextField).pad(0, 5, 0, 5).expandX().fillX();
        add(optionsTable).expandX().fillX();
        row();
    }
}