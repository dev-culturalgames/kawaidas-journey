package de.venjinx.vjx.ui.debug;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import de.venjinx.ejx.EJXGame;

public class MemoryWindow extends Window {

    //    private VJXGame game;

    public MemoryWindow(EJXGame game, Skin skin) {
        super("Memory", skin);
        setName("window_memory");
        setResizable(true);

        //        this.game = game;
    }
}