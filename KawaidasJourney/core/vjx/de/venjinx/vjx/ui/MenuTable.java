package de.venjinx.vjx.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;

public abstract class MenuTable extends EJXMenuTable {

    public MenuTable(MenuStage mc, String name) {
        super(mc, name);
    }

    @Override
    public Table getContent() {
        return null;
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
    }

    @Override
    public void enterCallback() {
    }

    @Override
    public void lvlLoaded() {
    }

    @Override
    public void updateLanguage(TextControl texts) {
    }

    @Override
    public void updateLayout(Skin skin) {
    }
}