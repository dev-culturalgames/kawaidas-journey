package de.venjinx.vjx.ui;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ColorAction;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.EJXPlayer;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public abstract class IngameHUD extends MenuTable {

    public enum Transition {
        VIDEO_SEQUENCE, BLACKSCREEN
    }

    protected LevelStage level;
    protected EJXPlayer player;
    protected EJXEntity camFocusEntity;

    protected Table mainTable;
    protected Table top;

    protected List<String> hiddenElements;
    protected Color tmpColor = new Color();

    public IngameHUD(MenuStage mc, String name) {
        super(mc, name);
        setFillParent(true);
        setTouchable(Touchable.enabled);

        level = mc.getScreen().getLevel();
        player = mc.getGame().getPlayer();
        hiddenElements = new ArrayList<>();

        mainTable = new Table();
        mainTable.setName("ingameHUD");
        mainTable.setFillParent(true);
        addActor(mainTable);

        setSize(mc.getWidth(), mc.getHeight());
    }

    public abstract void moveUIItem(String name, float x, float y);

    public abstract void update(boolean decrMovingItems);

    public void toggleHUD(boolean on) {
        toggleHUD(on, .25f, 0f);
    }

    private void toggleHUD(boolean on, float duration, float delay) {
        menuControl.toggleActor(mainTable, on, duration, delay);
    }

    public void hideUIElements(String elementsStrList, boolean on) {
        String[] split = elementsStrList.split(VJXString.SEP_LIST);
        for (String s : split)
            hideUIElement(s, on);
    }

    public void hideUIElement(String elemName, boolean on) {
        enableUIElement(elemName, on);
        toggleUIElement(elemName, on);
    }

    public void enableAllElements() {
        hiddenElements.clear();
    }

    public void enableUIElement(String elemName, boolean enable) {
        if (!enable) {
            if (!hiddenElements.contains(elemName))
                hiddenElements.add(elemName);
        } else if (hiddenElements.contains(elemName))
            hiddenElements.remove(elemName);
    }

    public List<String> getHiddenElements() {
        return hiddenElements;
    }

    public void toggleUIElement(String name, boolean on) {
        if (name == null) return;
        Actor uiElement = findActor(name.split(VJXString.SEP_USCORE)[0]);
        if (uiElement != null)
            toggleUIElement(uiElement, on, uiElement.getTouchable());
    }

    public void toggleUIElement(String name, boolean on, Touchable touchable) {
        if (name == null) return;
        toggleUIElement(findActor(name), on, touchable);
    }

    public void toggleUIElement(Actor uiActor, boolean on, Touchable touchable) {
        if (uiActor != null) {
            if (on) tmpColor.set(1, 1, 1, 1);
            else tmpColor.set(0, 0, 0, 0);
            float duration = .25f;
            ColorAction ca = Actions.color(tmpColor, duration);
            uiActor.addAction(ca);
            uiActor.setTouchable(touchable);
        }
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
    }

    @Override
    public Table getContent() {
        return mainTable;
    }

    @Override
    public void updateLanguage(TextControl textControl) {
    }

    @Override
    public void updateLayout(Skin skin) {
    }
}