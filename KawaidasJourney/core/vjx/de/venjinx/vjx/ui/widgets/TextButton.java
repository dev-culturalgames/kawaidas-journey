package de.venjinx.vjx.ui.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.venjinx.ejx.util.EJXFont;
import de.venjinx.vjx.ui.EJXSkin;

public class TextButton extends Button {

    private static final String stackName = "TextButton_stack";
    private static final String imageName = "TextButton_bgImage";
    private static final String tableName = "TextButton_bgTable";
    private static final String mainName = "TextButton_mainTable";

    protected Stack stack;
    protected Image bgImage;
    protected Table mainTable;
    protected EJXLabel title;
    private Vector2 textOffset;

    public TextButton(String name, String text, Skin skin) {
        super();
        init(skin, name, text);
        centerText();
    }

    private void init(Skin skin, String name, String text) {
        setName("ui_" + name);
        setSkin(skin);

        textOffset = new Vector2();

        stack = new Stack();
        stack.setName(stackName);

        bgImage = new Image();
        bgImage.setName(imageName);
        Table bgTable = new Table();
        bgTable.setName(tableName);
        bgTable.add(bgImage);
        stack.add(bgTable);

        mainTable = new Table();
        mainTable.setName(mainName);
        stack.add(mainTable);

        add(stack);

        title = new EJXLabel(skin, text, EJXFont.TITLE_48);
        addActor(title);

        if (skin.has(EJXSkin.style_ + name, ButtonStyle.class)) {
            setStyle(skin.get(EJXSkin.style_ + name, ButtonStyle.class));
        } else if (skin.has(name, TextureRegion.class)) {
            setStyle(new ButtonStyle());
            setBGDrawable(skin.getDrawable(name));
            pack();
        } else if (skin.has(EJXSkin.style_default, ButtonStyle.class)) {
            setStyle(skin.get(ButtonStyle.class));
        } else {
            setStyle(new ButtonStyle());
        }
        getStyle().pressedOffsetY = -10;
    }

    public void setBGDrawable(String drawableName) {
        setBGDrawable(getSkin().getDrawable(drawableName));
    }

    public void setBGDrawable(Drawable bgDrawable) {
        bgImage.setDrawable(bgDrawable);
    }

    public void setText(String text) {
        title.setText(text);
    }

    public void setFont(String name) {
        title.setStyle(getSkin().get(name, LabelStyle.class));
        title.pack();
        setTextOffset(textOffset);
    }

    public void centerText() {
        setTextOffset(getWidth() / 2f - title.getWidth() / 2f,
                        getHeight() / 2f - title.getHeight() / 2f);
    }

    public void setTextOffsetX(float offX) {
        setTextOffset(offX, textOffset.y);
    }

    public void setTextOffsetY(float offY) {
        setTextOffset(textOffset.x, offY);
    }

    public void setTextOffset(Vector2 offset) {
        setTextOffset(offset.x, offset.y);
    }

    public void setTextOffset(float offX, float offY) {
        textOffset.set(offX, offY);
        title.setPosition(offX, offY);
    }

    public String getText() {
        return title.getText().toString();
    }

    public void setEnabled(boolean enabled) {
        if (enabled) {
            setTouchable(Touchable.enabled);
            bgImage.setColor(Color.WHITE);
            title.setColor(Color.WHITE);
        } else {
            setTouchable(Touchable.disabled);
            bgImage.setColor(Color.GRAY);
            title.setColor(Color.GRAY);
        }
    }

    @Override
    public void layout() {
        super.layout();
        setTextOffset(textOffset);
    }
}