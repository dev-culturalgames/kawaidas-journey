package de.venjinx.vjx.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;

import de.venjinx.ejx.util.EJXFont;

public class EJXLabel extends Label {

    public EJXLabel(Skin skin) {
        this(skin, null, EJXFont.TITLE_64, Align.center);
    }

    public EJXLabel(Skin skin, String text) {
        this(skin, text, EJXFont.TITLE_64, Align.center);
    }

    public EJXLabel(Skin skin, String text, String style) {
        this(skin, text, style, Align.center);
    }

    public EJXLabel(Skin skin, String text, String style, int align) {
        super(text, skin, style);
        setAlignment(align);
        setTouchable(Touchable.disabled);
        pack();
    }
}