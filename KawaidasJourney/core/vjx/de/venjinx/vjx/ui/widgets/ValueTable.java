package de.venjinx.vjx.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.SplitPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.util.EJXTypes.VJXString;

public class ValueTable extends Table {

    private Label titleLabel;

    private Table listTable;
    private SplitPane lastColumn;

    private ScrollPane scrollPane;
    private Table scrollContent;

    private Array<String> columnTitles;
    private Array<Table> columnTables;
    private Array<Array<Label>> rows;

    private Table indexTable;


    public ValueTable(String title, String[] columnNames, Skin skin) {
        super(skin);

        titleLabel = new Label(title, skin);
        add(titleLabel).expandX().left();
        row();

        columnTables = new Array<>();
        rows = new Array<>();
        columnTitles = new Array<>();
        columnTitles.addAll(columnNames);

        scrollContent = new Table();
        scrollContent.setName("scrollContent");

        scrollPane = new ScrollPane(scrollContent);
        add(scrollPane).expand().fill();

        init();
    }

    private void init() {
        indexTable = new Table();
        indexTable.setWidth(50);
        Label entryNrLabel = new Label("Index", getSkin());
        indexTable.add(entryNrLabel);

        lastColumn = new SplitPane(indexTable, null, false, getSkin());

        listTable = new Table();
        listTable.add(lastColumn).expandX().fillX();

        for (String s : columnTitles) {
            addColumn(s);
        }
        scrollContent.add(listTable).expand().fillX().top();
    }

    private void addColumn(String title) {
        Table entryTable = new Table();
        Label entryNrLabel = new Label(title, getSkin());
        entryTable.add(entryNrLabel);
        SplitPane newColumn = new SplitPane(entryTable, null, false, getSkin());
        lastColumn.setSecondWidget(newColumn);
        lastColumn = newColumn;

        columnTables.add(entryTable);
    }

    public void addRow(String... values) {
        Label entry = new Label(Integer.toString(rows.size), getSkin());
        indexTable.row();
        indexTable.add(entry);

        Array<Label> row = new Array<>();
        for (int i = 0; i < columnTables.size; i++) {
            if (i < values.length) {
                entry = new Label(values[i], getSkin());
            } else entry = new Label(VJXString.STR_EMPTY, getSkin());

            columnTables.get(i).row();
            columnTables.get(i).add(entry);
            row.add(entry);
        }

        rows.add(row);
    }

    public void clearTable() {
        scrollContent.clear();
        columnTables.clear();
        rows.clear();
        init();
    }
}