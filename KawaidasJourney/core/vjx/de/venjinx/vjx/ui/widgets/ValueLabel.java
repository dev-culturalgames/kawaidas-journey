package de.venjinx.vjx.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.venjinx.ejx.util.EJXTypes.VJXString;

public class ValueLabel extends Table {

    private Label valueNameLabel;
    private Label valueLabel;

    public ValueLabel(String name, float nameW, Skin skin) {
        super();

        valueNameLabel = new Label(name, skin);
        valueNameLabel.setEllipsis(true);
        valueLabel = new Label(VJXString.STR_EMPTY, skin);
        valueLabel.setEllipsis(true);

        add(valueNameLabel).minWidth(nameW).left();
        add(valueLabel).minWidth(nameW).expandX().left();
        pack();
    }

    public void setValueName(String name) {
        valueNameLabel.setText(name);
    }

    public void setValue(String value) {
        valueLabel.setText(value);
    }
}