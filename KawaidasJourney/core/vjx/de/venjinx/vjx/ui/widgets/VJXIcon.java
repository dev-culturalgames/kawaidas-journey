package de.venjinx.vjx.ui.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.venjinx.ejx.util.EJXFont;

public class VJXIcon extends Table {

    private Image icon;
    private EJXLabel textLabel;
    private Vector2 textOffset;

    public VJXIcon(String text, Drawable iconDrawable, Skin skin) {
        super();
        setTouchable(Touchable.disabled);
        textOffset = new Vector2();

        icon = new Image(iconDrawable);
        add(icon);
        setSize(icon.getWidth(), icon.getHeight());

        textLabel = new EJXLabel(skin, text, EJXFont.TITLE_36);
        addActor(textLabel);

        setTextOffset(getWidth() / 2f - textLabel.getWidth() / 2f,
                        -textLabel.getHeight() / 2f);
    }

    public void showText(boolean show) {
        textLabel.setVisible(show);
    }

    public void showIcon(boolean show) {
        icon.setVisible(show);
    }

    public void setIconDrawable(Drawable iconDrawable) {
        float w = getWidth(), h = getHeight();
        icon.setDrawable(iconDrawable);
        setSize(w, h);
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        //        super.setSize(width / 2f, height / 2f);
        //        float sclX = width / icon.getWidth();
        //        float sclY = width / icon.getHeight();
        //        icon.setSize(width / 3f, height / 3f);
        //        icon.setSize(width, height);
        //        icon.setScale(sclX, sclY);
    }

    public void setText(String text) {
        textLabel.setText(text);
        textLabel.pack();
        setTextOffset(getWidth() / 2f - textLabel.getWidth() / 2f,
                        -textLabel.getHeight() / 2f);
    }

    public void setFont(LabelStyle style) {
        textLabel.setStyle(style);
        textLabel.pack();
        setTextOffset(textOffset);
    }

    public void centerText() {
        setTextOffset(getWidth() / 2f - textLabel.getWidth() / 2f,
                        getHeight() / 2f - textLabel.getHeight() / 2f);
    }

    public void setTextOffset(Vector2 offset) {
        setTextOffset(offset.x, offset.y);
    }

    public void setTextOffset(float offX, float offY) {
        textOffset.set(offX, offY);
        textLabel.setPosition(offX, offY);
    }

    public EJXLabel getLabel() {
        return textLabel;
    }

    public void setEnabled(boolean enabled) {
        if (enabled) {
            icon.setColor(Color.WHITE);
            textLabel.setColor(Color.WHITE);
        } else {
            icon.setColor(Color.GRAY);
            textLabel.setColor(Color.GRAY);
        }
    }
}