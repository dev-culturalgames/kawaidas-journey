package de.venjinx.vjx.ui;

public interface LoadingUI {
    public void startLoading(boolean showIcon);

    public void startUnloading(boolean showIcon);

    public void stopLoading();

    public void updateLoading(float delta);
}