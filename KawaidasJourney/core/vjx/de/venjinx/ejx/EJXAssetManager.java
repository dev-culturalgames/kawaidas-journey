/*******************************************************************************
 * Copyright 2020 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.venjinx.ejx;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonJson;

import de.venjinx.ejx.util.EJXTypes;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

/**
 * Provides additional functions to conveniently access loaded assets and
 * create various asset types:</br>
 * {@link Texture}, {@link TextureRegion}, {@link Drawable},
 * {@link SkeletonData}, {@link TextureAtlas}
 *
 * @author Torge Rothe (X-Ray-Jin, xrayjin@gmx.de )
 * @see AssetManager
 *
 */
public class EJXAssetManager extends AssetManager {
    private HashMap<String, AssetDescriptor<? extends Object>> descriptions;
    // private HashMap<String, Pixmap> masks;

    /**
     * Creates a new {@link EJXAssetManager} instance.
     */
    public EJXAssetManager() {
        descriptions = new HashMap<>();
        // masks = new HashMap<>();
    }

    //    public void loadMask(String path, String name) {
    //        if (masks.containsKey(name)) return;
    //
    //        AtlasRegion tr = getRegion("global_utility", path + "" + name);
    //        if (!tr.getTexture().getTextureData().isPrepared()) {
    //            tr.getTexture().getTextureData().prepare();
    //        }
    //        Pixmap maskAtlas = tr.getTexture().getTextureData().consumePixmap();
    //        Pixmap mask = new Pixmap(tr.originalWidth, tr.originalHeight, maskAtlas.getFormat());
    //
    //        for (int x = 0; x < tr.getRegionWidth(); x++) {
    //            for (int y = 0; y < tr.getRegionHeight(); y++) {
    //                int colorInt = maskAtlas.getPixel(tr.getRegionX() + x,
    //                                tr.getRegionY() + y);
    //                mask.drawPixel(x + (int) tr.offsetX,
    //                                tr.originalHeight - tr.getRegionHeight() + y
    //                                - (int) tr.offsetY,
    //                                colorInt);
    //            }
    //        }
    //        masks.put(name, mask);
    //    }
    //
    //    public void addMask(String name, Pixmap mask) {
    //        masks.put(name, mask);
    //    }
    //
    //    public void removeMask(String name) {
    //        masks.remove(name);
    //    }
    //
    //    public Pixmap getMask(String name) {
    //        return masks.get(name);
    //    }

    /**
     * Adds a resource to the loading queue and maps it to the specified name.
     *
     * @param resourceName
     *                 the name of the asset that is used to reference it
     * @param resource
     *                 the resource to load
     */
    public synchronized void addResource(String resourceName,
                    AssetDescriptor<? extends Object> resource) {
        if (descriptions.containsKey(resourceName)) {
            VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                            getLoadString() + " asset '" + resourceName
                            + "' has already been added.", 1);
            return;
        }

        boolean mapped = false;
        AssetDescriptor<? extends Object> res;
        for (String resName : descriptions.keySet()) {
            res = descriptions.get(resName);
            if (res.fileName.equals(resource.fileName)) {
                resource = res;
                mapped = true;
                VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                                getLoadString() + " asset '" + resourceName
                                + "' mapped to '" + resName + "'." , 1);
                break;
            }
        }
        descriptions.put(resourceName, resource);
        load(resource);

        if (!mapped)
            VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                            getLoadString() + " queued " + resource.type.getSimpleName()
                            + " '" + resourceName + "'. Source: " + resource.fileName, 1);
    }

    /**
     * Unloads a the resource with the specified name.
     *
     * @param resourceName
     *             the name of the resource to unload
     */
    public synchronized void unloadResource(String resourceName) {
        if (descriptions.containsKey(resourceName)) {
            AssetDescriptor<? extends Object> resource = descriptions.get(resourceName);
            VJXLogger.logIncr(LogCategory.LOAD | LogCategory.DETAIL,
                            getLoadString() + " unloading asset '"
                                            + resourceName + "'. "
                                            + "Source: " + resource.fileName);
            unloadDependencies(resource.fileName);
            if (isLoaded(resourceName))
                unload(descriptions.get(resourceName).fileName);

            descriptions.remove(resourceName);
            VJXLogger.logDecr(LogCategory.LOAD | LogCategory.DETAIL,
                            getLoadString() + " asset '" + resourceName
                            + "' unloaded.");
        } else VJXLogger.log(LogCategory.WARNING,
                        getLoadString() + " trying to unload asset '" + resourceName
                        + "' failed. Asset not registered or already unloaded.", 1);
    }

    /**
     * Returns the {@link Texture} object with the specified name or null if it
     * is not loaded.
     *
     * @param resourceName
     *             the name of the texture resource
     * @return the texture or null
     */
    public synchronized Texture getTexture(String resourceName) {
        return get(resourceName, Texture.class);
    }

    /**
     * Returns the {@link TextureAtlas} object with the specified name or null
     * if it is not loaded.
     *
     * @param resourceName
     *             the name of the texture resource
     * @return the texture or null
     */
    public synchronized TextureAtlas getTextureAtlas(String resourceName) {
        return get(resourceName, TextureAtlas.class);
    }

    /**
     * Returns a new {@link Drawable} object with the specified name or null if
     * it is not loaded.
     *
     * @param resourceName
     *                     the name of the texture resource
     * @return the texture or null
     */
    public synchronized Drawable getDrawable(String resourceName) {
        if (isLoaded(resourceName, Drawable.class))
            return get(resourceName, Drawable.class);
        if (isLoaded(resourceName, TextureRegionDrawable.class))
            return get(resourceName, TextureRegionDrawable.class);
        if (isLoaded(resourceName, SpriteDrawable.class))
            return get(resourceName, SpriteDrawable.class);
        if (isLoaded(resourceName, Texture.class))
            return new TextureRegionDrawable(getTexture(resourceName));
        if (isLoaded(resourceName, TextureRegion.class))
            return new TextureRegionDrawable(get(resourceName, TextureRegion.class));
        if (isLoaded(resourceName, AtlasRegion.class)) {
            AtlasRegion ar = get(resourceName, AtlasRegion.class);
            return new TextureRegionDrawable(ar);
        }

        TextureRegion tr;
        Array<TextureAtlas> out = new Array<>();
        getAll(TextureAtlas.class, out);
        for (TextureAtlas textureAtlas : out) {
            tr = textureAtlas.findRegion(resourceName);
            if (tr != null)
                return new TextureRegionDrawable(tr);
        }
        return null;
    }

    /**
     * Returns the {@link AtlasRegion} object with the specified name on the
     * specified {@link TextureAtlas}.
     *
     * @param atlasName
     *                   the name of the texture atlas resource
     * @param regionName
     *                   the name of the region
     * @return the {@link AtlasRegion} or null
     */
    public synchronized AtlasRegion getRegion(String atlasName, String regionName) {
        if (!descriptions.containsKey(atlasName)) return null;
        TextureAtlas ta = get(descriptions.get(atlasName).fileName, TextureAtlas.class);

        return ta.findRegion(regionName);
    }

    @Deprecated
    public synchronized TextureRegion getRegion(TiledMapTile tile) {
        if (tile.getTextureRegion() == null)
            return getRegion(tile.getProperties());
        else return tile.getTextureRegion();
    }

    @Deprecated
    public synchronized AtlasRegion getRegion(MapProperties properties) {
        String environment = properties.get(SourceProperty.environment.name,
                        VJXString.STR_EMPTY, String.class);
        String defName = properties.get(DefinitionProperty.defName.name,
                        VJXString.STR_EMPTY, String.class);
        String name = properties.get(SourceProperty.tileName.name, defName,
                        String.class);
        String src = properties.get(SourceProperty.source.name,
                        VJXString.STR_EMPTY, String.class);

        src += name;
        if (src.contains("tiles")) { return null; }
        if (src.contains("utility")) {
            name = src;
        } else {
            String cat = properties.get(DefinitionProperty.category.name,
                                        VJXString.STR_EMPTY, String.class);
            name = environment.split(VJXString.SEP_USCORE)[1];
            if (cat.equals("item"))
                name = cat;
            name = src.split(name + VJXString.SEP_SLASH)[1];
        }
        AtlasRegion tr = getRegion(environment, name);
        if (tr == null && !name.contains("collision")) {
            VJXLogger.log(defName + " get stupid texture " + name + " env "
                            + environment, 1);
        }
        return tr;
    }

    /**
     * Returns a new {@link SkeletonData} object for
     * <a href="http://de.esotericsoftware.com/">Spine</a> animations based on
     * the provided data.
     *
     * @param defName
     *                    the definition name of the animation
     * @param source
     *                    the path to the animation file
     * @param environment
     *                    the name of the atlas containing the animation images
     * @return a new {@link SkeletonData} object
     */
    public synchronized SkeletonData loadSkeletonData(String defName,
                    String source, String environment) {
        TextureAtlas ta;
        if (!environment.isEmpty())
            ta = getTextureAtlas(environment);
        else ta = getTextureAtlas(defName + "_anims"); // basically only the tree house

        return new SkeletonJson(ta).readSkeletonData(
                        Gdx.files.internal(source + defName + ".json"));
    }

    @Override
    public synchronized <T> T get(String name, Class<T> type) {
        if (super.isLoaded(name, type)) return super.get(name, type);
        if (!descriptions.containsKey(name)) return null;
        return get(descriptions.get(name).fileName, type);
    }

    @Override
    public synchronized boolean isLoaded(String name) {
        if (!descriptions.containsKey(name)) return super.isLoaded(name);
        return super.isLoaded(descriptions.get(name).fileName);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public synchronized boolean isLoaded(String name, Class type) {
        if (!descriptions.containsKey(name)) return super.isLoaded(name, type);
        return super.isLoaded(descriptions.get(name).fileName, type);
    }

    @Override
    public void finishLoading() {
        long time = System.currentTimeMillis();
        VJXLogger.logIncr(LogCategory.LOAD, getLoadString()
                        + " finish loading...");
        super.finishLoading();
        VJXLogger.logDecr(LogCategory.LOAD, getLoadString()
                        + " finished loading assets. Took "
                        + (System.currentTimeMillis() - time)
                        + "ms.");
    }

    /**
     * Unloads all dependencies of a resource.
     *
     * @param resourceName
     *                     the name of the parent asset
     */
    private synchronized void unloadDependencies(String resourceName) {
        Array<String> dependencies = getDependencies(resourceName);
        if (dependencies != null) {
            VJXLogger.logIncr(LogCategory.LOAD | LogCategory.DETAIL, "dependencies:");
            for (String s : dependencies) {
                VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL, s);

                unloadDependencies(s);
                if (isLoaded(s)) {
                    unload(s);
                }
            }
            VJXLogger.logDecr(LogCategory.LOAD | LogCategory.DETAIL, "dependencies unloaded.");
        }
    }

    /**
     *
     * @return a string representation of the current load of this asset manager.
     */
    private String getLoadString() {
        return "Assetmanager (" +getLoadedAssets() + "/" + getQueuedAssets() + ")";
    }
}