package de.venjinx.ejx.animation;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationState.AnimationStateListener;
import com.esotericsoftware.spine.AnimationState.TrackEntry;
import com.esotericsoftware.spine.Event;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonRenderer;

import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.util.VJXGraphix;

public class SpineActor extends Actor implements AnimationStateListener {

    private SkeletonRenderer spineRenderer;
    private AnimationState animationState;
    private Skeleton skeleton;
    private String curAnimName;

    private float sSclX = 1f, sSclY = 1f;
    private boolean flipX, flipY;

    private SpineActorListener listener;

    private boolean premAlpha = true;
    private Color drawColor = new Color();

    private String[] animScales;

    public SpineActor(String name) {
        this(name, null);
    }

    public SpineActor(String name, SpineActorListener listener) {
        setName(name);
        this.listener = listener;
        spineRenderer = VJXGraphix.spineRenderer;
    }

    public void setSkin(String skinName) {
        skeleton.setSkin(skinName);
    }

    public void setAnimation(String name, boolean loop) {
        setAnimation(name, loop, false);
    }

    public void setAnimation(String name, boolean loop, boolean randomStart) {
        curAnimName = name;
        animationState.setAnimation(0, name, loop);

        if (animScales != null) {
            String[] animSet;
            for (String animation : animScales) {
                animSet = animation.split(":");
                animationState.setTimeScale(Float.parseFloat(animSet[1]));
            }
        }
        if (randomStart)
            animationState.update(MathUtils.random(getAnimDuration(name)));
    }

    public boolean isAtAnim(String name) {
        return curAnimName.equals(name);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (animationState != null) {
            animationState.update(delta);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        boolean currentPremAlpha = spineRenderer.getPremultipliedAlpha();
        Color c = batch.getColor();
        drawColor.set(c);
        drawColor.mul(getColor());
        drawColor.a *= parentAlpha;
        batch.setColor(drawColor);
        spineRenderer.setPremultipliedAlpha(premAlpha);
        if (skeleton != null) {
            skeleton.getRootBone().setScaleX(sSclX);
            skeleton.getRootBone().setScaleY(sSclY);
            skeleton.findBone("origin").setScaleX(flipX ? -1 : 1);
            skeleton.findBone("origin").setScaleY(flipY ? -1 : 1);
            skeleton.setPosition(getX(), getY());
            skeleton.setColor(drawColor);

            skeleton.updateWorldTransform();

            animationState.apply(skeleton);
            spineRenderer.draw(batch, skeleton);
        }
        spineRenderer.setPremultipliedAlpha(currentPremAlpha);
        batch.setColor(c);
        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    }

    public void usePremAlpha(boolean use) {
        premAlpha = use;
    }

    @Override
    public void setScaleX(float scaleX) {
        //        super.setScaleX(scaleX);
        sSclX = scaleX;
    }

    @Override
    public void setScaleY(float scaleY) {
        //        super.setScaleY(scaleY);
        sSclY = scaleY;
    }

    @Override
    public void setScale(float scaleXY) {
        setScale(scaleXY, scaleXY);
    }

    @Override
    public void setScale(float scaleX, float scaleY) {
        //        super.setScale(scaleX, scaleY);
        sSclX = scaleX;
        sSclY = scaleY;
    }

    @Override
    public void setSize(float w, float h) {
        super.setSize(w, h);

        //        updateSkeletonScale(w, h);
    }

    @Override
    public void setRotation(float degrees) {
        setRotation("root", degrees);
    }

    public void setRotation(String bone, float degrees) {
        super.setRotation(degrees);
        skeleton.findBone(bone).setRotation(degrees);
    }

    public void flipX(boolean flipX) {
        this.flipX = flipX;
        if (flipX)
            setPosition(getX(), getY());
    }

    public void flipY(boolean flipY) {
        this.flipY = flipY;
        if (flipY)
            setPosition(getX(), getY());
    }

    public void setEntity(AnimatedEntity entity) {
        if (entity.getSkeleton() == null) return;
        setAnimationState(entity.getAnimState());
        setSkeleton(entity.getSkeleton(), null);
        setScale(entity.getScaleX(), entity.getScaleY());
        setRotation(entity.getRotation());
        setSize(entity.getWidth(), entity.getHeight());
        //        updateSkeletonScale(entity.getWidth(), entity.getHeight());
    }

    public void setSkeleton(Skeleton skeleton) {
        setSkeleton(skeleton, null);
    }

    public void setSkeleton(Skeleton skeleton, MapProperties props) {
        if (this.skeleton == skeleton) return;

        this.skeleton = skeleton;
        updateSkeletonScale(getWidth(), getHeight());
        if (props != null) {
            String animTimesString = props.get("animTimeScales", "", String.class);
            if (!animTimesString.isEmpty()) {
                animTimesString = animTimesString.replace("\r", "");
                animScales = animTimesString.split("\n");
            }
        }
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public void updateSkeletonScale(float w, float h) {
        if (skeleton == null) return;
        sSclX = getWidth() / w * skeleton.getRootBone().getScaleX();
        sSclY = getHeight() / h * skeleton.getRootBone().getScaleY();
    }

    public void setAnimationState(AnimationState animation) {
        animation.addListener(this);
        animationState = animation;
    }

    public AnimationState getState() {
        return animationState;
    }

    public float getAnimDuration(String name) {
        SkeletonData skData = skeleton.getData();
        float duration = 0f;
        if (skData.findAnimation(name) != null)
            duration = skData.findAnimation(name).getDuration();
        return duration;
    }

    public void setAnimListener(SpineActorListener listener) {
        this.listener = listener;
    }

    @Override
    public void complete(TrackEntry entry) {
        if (listener != null)
            listener.complete(this, entry.getAnimation().getName());
    }

    @Override
    public void end(TrackEntry entry) {
        if (listener != null)
            listener.end(this, entry.getAnimation().getName());
    }

    @Override
    public void event(TrackEntry entry, Event event) {
        if (listener != null)
            listener.event(this, entry.getAnimation().getName(), event);
    }

    @Override
    public void start(TrackEntry entry) {
        if (listener != null)
            listener.start(this, entry.getAnimation().getName());
    }

    @Override
    public void interrupt(TrackEntry entry) {
        if (listener != null)
            listener.interrupt(this, entry.getAnimation().getName());
    }

    @Override
    public void dispose(TrackEntry entry) {
        if (listener != null)
            listener.dispose(this, entry.getAnimation().getName());
    }

    public interface SpineActorListener {

        public void complete(SpineActor actor, String anim);

        public void dispose(SpineActor actor, String anim);

        public void interrupt(SpineActor actor, String anim);

        public void start(SpineActor actor, String anim);

        public void event(SpineActor actor, String anim, Event event);

        public void end(SpineActor actor, String anim);

    }
}