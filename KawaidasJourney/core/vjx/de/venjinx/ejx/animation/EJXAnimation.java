package de.venjinx.ejx.animation;

import java.util.HashMap;

import com.badlogic.gdx.maps.MapProperties;

import de.venjinx.ejx.util.EJXTypes.AnimationType;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class EJXAnimation {

    public static final String VJXAnimation = "VJXAnimation";
    public static final String ANIM_FRAME = "frame";
    public static final String ANIM_FBF = "fbf";
    public static final String ANIM_SPINE = "spine";

    public static final String ANIM_TYPE = "animation:type";
    public static final String ANIM_NAME = "animation:name";
    public static final String ANIM_ANIMATIONS = "animations";
    public static final String ANIM_TIMESCALES = "animation:timeScales";

    private AnimationType type = AnimationType.FRAME;
    private String name = VJXAnimation;

    private HashMap<String, String[]> animDefs = new HashMap<>();
    private HashMap<String, Float> animTimeScales = new HashMap<>();

    private MapProperties properties = new MapProperties();

    public EJXAnimation(MapProperties properties) {
        this(VJXAnimation, properties);
    }

    public EJXAnimation(String name, MapProperties properties) {
        this.properties.putAll(properties);
        this.name = properties.get(ANIM_NAME, name, String.class);
        String types = properties.get(ANIM_TYPE, ANIM_FRAME, String.class);
        switch (types) {
            case ANIM_FBF:
                type = AnimationType.FBF;
                break;
            case ANIM_SPINE:
                type = AnimationType.SPINE;
                break;
            default:
                type = AnimationType.FRAME;
        }
        String animsString = properties.get(ANIM_ANIMATIONS,
                        VJXString.STR_EMPTY, String.class);
        if (!animsString.isEmpty()) {
            animsString = animsString.replace(VJXString.SEP_NEW_LINE_R,
                            VJXString.STR_EMPTY);
            String[] animsArray = animsString.split(VJXString.SEP_NEW_LINE_N);
            String[] animSet;
            for (String animation : animsArray) {
                animSet = animation.split(VJXString.SEP_DDOT);
                animDefs.put(animSet[0], animSet[1].split(VJXString.SEP_LIST));
            }
        }

        String animTimesString = properties.get(ANIM_TIMESCALES,
                        VJXString.STR_EMPTY, String.class);
        if (!animTimesString.isEmpty()) {
            animTimesString = animTimesString.replace(VJXString.SEP_NEW_LINE_R,
                            VJXString.STR_EMPTY);
            String[] animsArray = animTimesString
                            .split(VJXString.SEP_NEW_LINE_N);
            String[] animSet;
            for (String animation : animsArray) {
                animSet = animation.split(VJXString.SEP_DDOT);
                animTimeScales.put(animSet[0], Float.parseFloat(animSet[1]));
            }
        }
    }

    public AnimationType getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}