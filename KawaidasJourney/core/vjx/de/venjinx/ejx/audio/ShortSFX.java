package de.venjinx.ejx.audio;

import java.util.HashSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import de.venjinx.ejx.util.EJXTypes;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class ShortSFX extends VJXSound {

    // Actual sound and dependencies for the audio controller
    private Sound sound;

    private HashSet<Long> ids;

    public ShortSFX(String name, FileHandle file) {
        super(name, file);
        ids = new HashSet<>();
    }

    @Override
    public long load() {
        if (sound != null) return 0;

        sound = Gdx.audio.newSound(file);
        size = file.length();

        if (size > EJXTypes.ONE_MB) VJXLogger.log(LogCategory.WARNING,
                        "Sound " + getName()
                        + " size exceeds limit of 1mb. Size "
                        + EJXTypes.byteToMB(size, 2) + "mb");

        VJXLogger.log(LogCategory.SFX, "    Loaded sound '" + getName()
        + "'. Size: " + EJXTypes.byteToMB(size, 2) + "mb");
        return size;
    }

    @Override
    public long unload() {
        long freedSize = 0;
        if (sound != null && !inUse()) {
            sound.dispose();
            freedSize = size;

            sound = null;
            size = 0;
            ids.clear();
            VJXLogger.log(LogCategory.SFX, "    Unloaded sound '" + getName()
                            + "'. Size: " + EJXTypes.byteToMB(freedSize, 2));
        }

        return freedSize;
    }

    @Override
    public long play(float volume) {
        return play(volume, 1);
    }

    public long play(float vol, float pitch) {
        if (isLooping()) return loop(vol, pitch);
        if (sound == null) {
            VJXLogger.log(LogCategory.ERROR, getName() + ": Sound is null.");
            return -1;
        }
        long id = sound.play(vol, pitch, 0);
        ids.add(id);
        return id;
    }

    @Override
    public long loop(float volume) {
        return loop(volume, 1);
    }

    public long loop(float vol, float pitch) {
        if (sound == null) return -1;
        long id = sound.loop(vol, pitch, 0);
        ids.add(id);
        return id;
    }

    @Override
    public void pause() {
        if (sound == null) return;
        sound.pause();
    }

    @Override
    public void resume() {
        if (sound == null) return;
        sound.resume();
    }

    @Override
    public void stop() {
        if (sound == null) return;
        sound.stop();
        ids.clear();
    }

    public void stop(long id) {
        if (sound == null) return;
        sound.stop(id);
        ids.remove(id);
    }

    public Sound getSound() {
        return sound;
    }
}