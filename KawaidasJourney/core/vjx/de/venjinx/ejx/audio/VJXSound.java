package de.venjinx.ejx.audio;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;

public abstract class VJXSound implements Disposable {

    private int dependencies = 0;

    // Source data
    private String name;
    protected FileHandle file;
    protected long size;

    // Sound settings
    protected float volume = 1f;
    protected float pitch = 1f;
    protected boolean loop = false;

    private float propability;

    private int count;

    public VJXSound(String name, FileHandle file) {
        this.name = name;
        this.file = file;
    }

    public abstract long load();

    public abstract long unload();

    public abstract long play(float vol);

    public abstract long loop(float vol);

    public abstract void pause();

    public abstract void resume();

    public abstract void stop();

    public String getName() {
        return name;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public float getVolume() {
        return volume;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public boolean isLooping() {
        return loop;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public float getPitch() {
        return pitch;
    }

    public String getFilePath() {
        return file.path();
    }

    public long getSize() {
        return size;
    }

    public void addDependency() {
        dependencies++;
    }

    public void removeDependency() {
        if (dependencies > 0) dependencies--;
    }

    public boolean inUse() {
        return dependencies > 0;
    }

    @Override
    public void dispose() {
        unload();
    }

    public void setPropability(float propability) {
        this.propability = propability;
    }

    public float getPropability() {
        return propability;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}