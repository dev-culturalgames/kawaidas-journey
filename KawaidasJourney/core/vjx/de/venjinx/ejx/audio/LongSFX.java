package de.venjinx.ejx.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;

public class LongSFX extends VJXSound {

    // Actual sound and dependencies for the audio controller
    private Music longSound;

    public LongSFX(String name, FileHandle file) {
        super(name, file);
    }

    @Override
    public long load() {
        if (longSound != null) return size;

        longSound = Gdx.audio.newMusic(file);
        size = file.length();

        return size;
    }

    @Override
    public long unload() {
        long freedSize = 0;
        if (longSound != null && !inUse()) {
            longSound.dispose();
            freedSize = size;

            longSound = null;
            size = 0;
        }

        return freedSize;
    }

    @Override
    public long play(float volume) {
        if (longSound.isPlaying()) longSound.stop();

        longSound.setVolume(volume);
        longSound.setLooping(isLooping());
        longSound.play();
        return -1;
    }

    @Override
    public long loop(float volume) {
        longSound.setVolume(volume);
        longSound.setLooping(true);

        longSound.play();
        return -1;
    }

    @Override
    public void pause() {
        if (longSound == null) return;
        longSound.pause();
    }

    @Override
    public void resume() {
        if (longSound == null) return;
        longSound.play();
    }

    @Override
    public void stop() {
        if (longSound == null) return;
        longSound.stop();
    }

    public Music getSound() {
        return longSound;
    }
}