package de.venjinx.ejx.actions.game;

import com.badlogic.gdx.math.Vector2;

import de.venjinx.ejx.actions.GameAction;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EJXGroup;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SpawnEntityAction extends GameAction {

    private static final String SPAWN_ACTION = "spawn_action";
    private static final String EXEC_MSG_DEFNAME = " (defName: ";
    private static final String EXEC_MSG_POSITION = ", position: ";
    private static final String EXEC_MSG_LAYER = ", layer: ";
    private static final String EXEC_MSG_PARENT = ", parent: ";
    private static final String EXEC_MSG_BEHIND_PARENT = ", behind parent: ";

    private String defName = VJXString.STR_EMPTY;
    private Vector2 pos;
    private EJXGroup layerNode;
    private EJXEntity parent;
    private boolean behindParent = false;

    public SpawnEntityAction() {
        name = SPAWN_ACTION;
        pos = new Vector2();
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_DEFNAME + defName;
        execMsg += EXEC_MSG_POSITION + pos;
        execMsg += EXEC_MSG_LAYER + layerNode.getName();
        execMsg += EXEC_MSG_PARENT + parent.getName();
        execMsg += EXEC_MSG_BEHIND_PARENT + behindParent;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        EJXEntity e = game.factory.createEntity(defName);
        pos.sub(e.getOriginX() * e.getScaleX(), e.getOriginY() * e.getScaleY());
        e.setBirthPlace(pos);
        game.getScreen().getLevel().addEntity(e, parent, layerNode, behindParent);
        return true;
    }

    public void setDefName(String defName) {
        this.defName = defName;
    }

    public void setBirthX(float birthX) {
        pos.x = birthX;
    }

    public void setBirthY(float birthY) {
        pos.y = birthY;
    }

    public void setBirthXY(float birthX, float birthY) {
        setBirthX(birthX);
        setBirthY(birthY);
    }

    public void setBirthPosition(Vector2 birthPosition) {
        setBirthXY(birthPosition.x, birthPosition.y);
    }

    public void setParent(EJXEntity parent) {
        this.parent = parent;
    }

    public void setLayer(EJXGroup layerNode) {
        this.layerNode = layerNode;
    }

    public void setBehindParent(boolean behindParent) {
        this.behindParent = behindParent;
    }

    public void set(String defName, Vector2 pos, EJXEntity parent,
                    EJXGroup layerNode, boolean behindParent) {
        setDefName(defName);
        setBirthPosition(pos);
        setParent(parent);
        setLayer(layerNode);
        setBehindParent(behindParent);
    }
}