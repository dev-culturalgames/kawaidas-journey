package de.venjinx.ejx.actions.game;

import de.venjinx.ejx.actions.GameAction;

public class UpdateUIAction extends GameAction {

    private static final String UPDATE_UI_ACTION = "update_ui_action";

    private boolean decrMovingItems;

    public UpdateUIAction() {
        name = UPDATE_UI_ACTION;
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        menu.updateHUD(decrMovingItems);
        return true;
    }

    public void decrMovingItems(boolean decrMovingItems) {
        this.decrMovingItems = decrMovingItems;
    }
}