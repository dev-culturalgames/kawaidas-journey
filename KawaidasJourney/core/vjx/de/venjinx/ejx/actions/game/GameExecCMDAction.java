package de.venjinx.ejx.actions.game;

import de.venjinx.ejx.actions.GameAction;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class GameExecCMDAction extends GameAction {

    private static final String GAME_EXEC_CMD_ACTION = "game_exec_cmd_action";
    private static final String EXEC_MSG_CMD = " (cmd: ";

    Command command;
    GameControl listener;

    public GameExecCMDAction() {
        name = GAME_EXEC_CMD_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_CMD + command.toString();
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        listener.execSingleCmd(command);
        return true;
    }

    public void setListener(GameControl listener) {
        this.listener = listener;
        setGame(listener.getGame());
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}