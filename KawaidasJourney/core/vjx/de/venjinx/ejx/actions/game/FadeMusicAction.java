package de.venjinx.ejx.actions.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.math.Interpolation;

import de.venjinx.ejx.actions.GameAction;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;

public class FadeMusicAction extends GameAction {

    private static final String FADE_MUSIC_ACTION = "fade_music_action";
    private static final String EXEC_MSG_FADE = "(fadeIn: ";
    private static final String EXEC_MSG_DURATION = ", duration: ";

    private Interpolation fade = Interpolation.fade;
    private Music music;
    private boolean fadeIn;
    private float timer;
    private float duration;
    private float volume;

    public FadeMusicAction() {
        name = FADE_MUSIC_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_FADE + fadeIn;
        execMsg = EXEC_MSG_DURATION + duration;
        execMsg += VJXString.SEP_BRACKET_CLOSE;

        volume = fadeIn ? 0f : 1f;
        music.setVolume(volume);
        music.play();
        music.setVolume(volume);
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        timer += delta;

        float progress = Math.min(1f, timer / duration);
        if (fadeIn) {
            volume = fade.apply(progress);
        } else volume = 1f - fade.apply(progress);

        VJXLogger.log(volume * game.sfx.getMusicVolume()
                        * game.sfx.getMasterVolume() + " "
                        + Gdx.graphics.getDeltaTime());
        music.setVolume(volume * game.sfx.getMusicVolume()
                        * game.sfx.getMasterVolume());

        return fadeIn ? volume == 1f : volume == 0f;
    }

    @Override
    public void reset() {
        super.reset();
        timer = 0;
        volume = fadeIn ? 0f : 1f;
        music = null;
    }

    public void setMusic(Music music) {
        this.music = music;
    }

    public void setFadeIn(boolean fadeIn) {
        this.fadeIn = fadeIn;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }
}