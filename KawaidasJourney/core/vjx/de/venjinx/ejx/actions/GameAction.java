package de.venjinx.ejx.actions;

import com.badlogic.gdx.scenes.scene2d.Action;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public abstract class GameAction extends Action {

    private static final String GAME_ACTION = "game_action";
    private static final String ACTION = "Action: Game execute ";
    protected String execMsg = VJXString.STR_EMPTY;

    protected boolean initialized = false;
    protected String name = GAME_ACTION;

    protected EJXGame game;
    protected MenuStage menu;
    protected LevelStage level;

    @Override
    public boolean act(float delta) {
        if (!initialized) {
            init();
            initialized = true;
            VJXLogger.log(LogCategory.EXEC, ACTION + name + execMsg, 1);
        }
        return true;
    }

    protected void init() {
    }

    @Override
    public void reset() {
        super.reset();
        initialized = false;
    }

    public void setGame(EJXGame game) {
        this.game = game;
        menu = game.getScreen().getMenu();
        level = game.getScreen().getLevel();
    }
}