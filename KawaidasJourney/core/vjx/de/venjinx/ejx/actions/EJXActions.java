package de.venjinx.ejx.actions;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.actions.game.FadeMusicAction;
import de.venjinx.ejx.actions.game.SpawnEntityAction;
import de.venjinx.ejx.actions.game.UpdateUIAction;
import de.venjinx.ejx.actions.object.AppearAtWPAction;
import de.venjinx.ejx.actions.object.AttachToAction;
import de.venjinx.ejx.actions.object.BindAction;
import de.venjinx.ejx.actions.object.BlinkAction;
import de.venjinx.ejx.actions.object.DetachAction;
import de.venjinx.ejx.actions.object.EntityExecuteCMDAction;
import de.venjinx.ejx.actions.object.FollowPathAction;
import de.venjinx.ejx.actions.object.OrientationAction;
import de.venjinx.ejx.actions.object.SetActivity;
import de.venjinx.ejx.actions.object.SetAnimation;
import de.venjinx.ejx.actions.object.SetGhost;
import de.venjinx.ejx.actions.object.SetGravityScale;
import de.venjinx.ejx.actions.object.SetLifecycle;
import de.venjinx.ejx.actions.object.SetPosition;
import de.venjinx.ejx.actions.object.SetTarget;
import de.venjinx.ejx.actions.object.SetVisibleAction;
import de.venjinx.ejx.actions.object.SetWaypoint;
import de.venjinx.ejx.actions.object.StartMove;
import de.venjinx.ejx.actions.object.ToggleRunAction;
import de.venjinx.ejx.actions.object.TranslateAction;
import de.venjinx.ejx.actions.object.UpdateBodyAction;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EJXGroup;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.Orientation;

public class EJXActions extends Actions {

    /*
     * Object actions
     */

    public static FollowPathAction followPathAction(String waypoints,
                    boolean disappear, boolean loop, float delay) {
        FollowPathAction followPathAction = action(FollowPathAction.class);
        followPathAction.setWaypoint(waypoints);
        followPathAction.setDisappear(disappear);
        followPathAction.setLoop(loop);
        followPathAction.setDelay(delay);
        return followPathAction;
    }

    public static AppearAtWPAction appearAtWP(Waypoint wp, boolean disappear,
                    float duration) {
        AppearAtWPAction appearAtWPAction = action(AppearAtWPAction.class);
        appearAtWPAction.setWaypoint(wp);
        appearAtWPAction.setDisappear(disappear);
        return appearAtWPAction;
    }

    public static UpdateBodyAction updateBody(EJXEntity entity) {
        UpdateBodyAction updBodyAction = action(UpdateBodyAction.class);
        updBodyAction.setEntity(entity);
        return updBodyAction;
    }

    public static SetLifecycle setLifecycle(Lifecycle lifecycle,
                    EJXEntity entity) {
        SetLifecycle setStateAction = action(SetLifecycle.class);
        setStateAction.setLifecycle(lifecycle);
        setStateAction.setEntity(entity);
        return setStateAction;
    }

    public static SetActivity setActivity(Activity activity, EJXEntity entity) {
        SetActivity setStateAction = action(SetActivity.class);
        setStateAction.setActivity(activity);
        setStateAction.setEntity(entity);
        return setStateAction;
    }

    public static SetGravityScale setGravityScale(float gravScale,
                    EJXEntity entity) {
        SetGravityScale setGravityScale = action(SetGravityScale.class);
        setGravityScale.setGravity(gravScale);
        setGravityScale.setEntity(entity);
        return setGravityScale;
    }

    public static EntityExecuteCMDAction entityExecute(Command command,
                    EJXEntity entity) {
        EntityExecuteCMDAction execCommandAction = new EntityExecuteCMDAction();
        execCommandAction.setCommand(command);
        execCommandAction.setEntity(entity);
        return execCommandAction;
    }

    public static SetGhost setGhost(boolean isGhost, EJXEntity entity) {
        SetGhost ghostAction = action(SetGhost.class);
        ghostAction.setGhost(isGhost);
        ghostAction.setEntity(entity);
        return ghostAction;
    }

    public static SetTarget setTarget(EJXEntity entity, EJXEntity target) {
        SetTarget setTargetAction = action(SetTarget.class);
        setTargetAction.setEntity(entity);
        setTargetAction.setTargetEntity(target);
        return setTargetAction;
    }

    public static SetWaypoint setWPAction(Waypoint waypoint, EJXEntity entity) {
        SetWaypoint setWPAction = action(SetWaypoint.class);
        setWPAction.setEntity(entity);
        setWPAction.setWaypoint(waypoint);
        return setWPAction;
    }

    public static StartMove startMove(EJXEntity entity, boolean stop,
                    boolean updateActivity) {
        StartMove startMoveAction = action(StartMove.class);
        startMoveAction.setEntity(entity);
        startMoveAction.setStop(stop);
        startMoveAction.setUpdateActivity(updateActivity);
        return startMoveAction;
    }

    public static SetPosition setPosition(Vector2 position, EJXEntity entity) {
        SetPosition setPositionAction = action(SetPosition.class);
        setPositionAction.setPosition(position);
        setPositionAction.setEntity(entity);
        return setPositionAction;
    }

    public static SetPosition setPosition(float x, float y, EJXEntity entity) {
        SetPosition setPositionAction = action(SetPosition.class);
        setPositionAction.setPosition(x, y);
        setPositionAction.setEntity(entity);
        return setPositionAction;
    }

    public static TranslateAction translate(Vector2 position, float duration,
                    EJXEntity entity) {
        TranslateAction translateAction = action(TranslateAction.class);
        translateAction.setEntity(entity);
        translateAction.set(position, duration);
        return translateAction;
    }

    /*
     * Object animation actions
     */
    public static SetAnimation setAnimation(String animationName, boolean loop,
                    EJXEntity entity) {
        SetAnimation setAnimationAction = action(SetAnimation.class);
        setAnimationAction.setAnimName(animationName);
        setAnimationAction.setLoop(loop);
        setAnimationAction.setEntity(entity);
        return setAnimationAction;
    }

    //    public static AddAnimationAction addAnimation(String animationName,
    //                    int track, boolean loop, float delay, VJXEntity entity) {
    //        AddAnimationAction addAnimationAction = action(AddAnimationAction.class);
    //        addAnimationAction.setAnimName(animationName);
    //        addAnimationAction.setTrack(track);
    //        addAnimationAction.setLoop(loop);
    //        addAnimationAction.setAnimationDelay(delay);
    //        addAnimationAction.setEntity(entity);
    //        return addAnimationAction;
    //    }

    public static SequenceAction bounce(float min, float max, float speed) {
        return bounce(1f, 1f, min, max, speed);
    }

    public static SequenceAction bounce(float targetX, float targetY, float min,
                    float max, float speed) {
        SequenceAction bounceAction = Actions.sequence();
        bounceAction.addAction(Actions.scaleTo(min, min, speed));
        bounceAction.addAction(Actions.scaleTo(max, max, speed));
        bounceAction.addAction(Actions.scaleTo(targetX, targetY, speed));
        return bounceAction;
    }

    public static SequenceAction bounceIn(float max, float speed) {
        return bounceIn(0f, max, speed);
    }

    public static SequenceAction bounceIn(float min, float max, float speed) {
        SequenceAction bounceAction = Actions.sequence();
        if (min > 0f) bounceAction.addAction(Actions.scaleTo(min, min));
        bounceAction.addAction(Actions.scaleTo(max, max, speed));
        bounceAction.addAction(Actions.scaleTo(1f, 1f, speed / 2f));
        return bounceAction;
    }

    public static SequenceAction bounceOut(float min, float max, float speed) {
        SequenceAction bounceAction = Actions.sequence();
        bounceAction.addAction(Actions.scaleTo(max, max, speed / 2f));
        bounceAction.addAction(Actions.scaleTo(min, min, speed));
        return bounceAction;
    }

    public static BlinkAction blink(float duration, EJXEntity entity) {
        return blink(duration, .25f, entity);
    }

    public static BlinkAction blink(float duration, float interval,
                    EJXEntity entity) {
        BlinkAction blinkAction = action(BlinkAction.class);
        blinkAction.setDuration(duration);
        blinkAction.setInterval(interval);
        blinkAction.setEntity(entity);
        return blinkAction;
    }

    public static Action attachTo(EJXEntity entity, EJXEntity attachTo) {
        AttachToAction attachToAction = action(AttachToAction.class);
        attachToAction.setEntity(entity);
        attachToAction.attachTo(attachTo);
        return attachToAction;
    }

    public static Action detach(EJXEntity entity) {
        DetachAction detachAction = action(DetachAction.class);
        detachAction.setEntity(entity);
        return detachAction;
    }

    public static Action bindAction(EJXEntity entity, boolean on, EJXEntity target) {
        BindAction ba = action(BindAction.class);
        ba.setEntity(entity);
        ba.setActivate(on);
        ba.setTargetEntity(target);
        return ba;
    }

    public static Action setOrientation(EJXEntity entity, Orientation orientation) {
        OrientationAction orientationAction = action(OrientationAction.class);
        orientationAction.setEntity(entity);
        orientationAction.setOrientation(orientation);
        return orientationAction;
    }

    public static Action setToggleRun(EJXEntity entity, boolean enable) {
        ToggleRunAction toggleRun = action(ToggleRunAction.class);
        toggleRun.setEntity(entity);
        toggleRun.setEnable(enable);
        return toggleRun;
    }

    public static Action visible(boolean visible, EJXEntity entity) {
        SetVisibleAction visibleAction = action(SetVisibleAction.class);
        visibleAction.setEntity(entity);
        visibleAction.setVisible(visible);
        return visibleAction;
    }

    /*
     * game actions
     */

    public static FadeMusicAction fadeMusic(Music music, boolean fadeIn,
                    float duration, EJXGame game) {
        FadeMusicAction fadeMusicAction = action(FadeMusicAction.class);
        fadeMusicAction.setMusic(music);
        fadeMusicAction.setFadeIn(fadeIn);
        fadeMusicAction.setDuration(duration);
        fadeMusicAction.setGame(game);
        return fadeMusicAction;
    }

    public static SpawnEntityAction spawnEntity(String defName,
                    Vector2 position, EJXEntity parent, EJXGroup layerNode,
                    boolean pre, EJXGame game) {
        SpawnEntityAction spawnEntityAction = action(SpawnEntityAction.class);
        spawnEntityAction.setGame(game);
        if (parent == null) {
            spawnEntityAction.set(defName, position, null, layerNode, pre);
        } else {
            if (layerNode == null) layerNode = parent.getLayer();
        }
        spawnEntityAction.set(defName, position, parent, layerNode, pre);
        return spawnEntityAction;
    }

    public static Action updateUI(EJXGame game) {
        return updateUI(false, game);
    }

    public static Action updateUI(boolean decrMovingItems, EJXGame game) {
        UpdateUIAction updateUIAction = action(UpdateUIAction.class);
        updateUIAction.setGame(game);
        updateUIAction.decrMovingItems(decrMovingItems);
        return updateUIAction;
    }
}