package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class BindAction extends ObjectAction {

    private static final String BIND_ACTION = "bind_action";
    private static final String EXEC_MSG_TARGET = " (target: ";
    private static final String EXEC_MSG_BIND = ", bind: ";

    private Box2DLogic phyicsLogic;
    private boolean activate;
    private EJXEntity targetEntity;

    public BindAction() {
        name = BIND_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_TARGET + targetEntity.getName();
        execMsg += EXEC_MSG_BIND + activate;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        if (targetEntity != null && targetEntity.hasLogic(VJXLogicType.MOVE)
                        && targetEntity.getPhysics() != null) {
            phyicsLogic = targetEntity.getPhysics();
        }
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (phyicsLogic == null) return true;
        if (activate) {
            phyicsLogic.attachTo(executor);
            targetEntity.getPhysics().setGhost(true);
            targetEntity.setLifecycle(Lifecycle.STILL);
            targetEntity.setActivity(Activity.DISABLED);
        } else {
            phyicsLogic.detach();
            targetEntity.getPhysics().setGhost(false);
            targetEntity.setActivity(Activity.IDLE);
        }
        return true;
    }

    public void setActivate(boolean activate) {
        this.activate = activate;
    }

    public void setTargetEntity(EJXEntity target) {
        targetEntity = target;
    }
}