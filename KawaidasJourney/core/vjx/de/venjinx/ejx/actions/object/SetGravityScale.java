package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SetGravityScale extends ObjectAction {

    private static final String SET_GRAVITYSCALE_ACTION = "set_gravityScale_action";
    private static final String EXEC_MSG_GRAVITYSCALE = " (gravityScale: ";

    private float gravity = B2DWorld.DFLT_GRAVITY_SCALE;

    public SetGravityScale() {
        name = SET_GRAVITYSCALE_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_GRAVITYSCALE + gravity;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (executor == null) return true;
        executor.setGravityScale(gravity);
        return true;
    }

    public void setGravity(float gravity) {
        this.gravity = gravity;
    }
}