package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SetActivity extends ObjectAction {

    private static final String SET_ACTIVITY_ACTION = "set_activity_action";
    private static final String EXEC_MSG_ACTIVITY = " (activity: ";

    private Activity activity = Activity.IDLE;

    public SetActivity() {
        name = SET_ACTIVITY_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_ACTIVITY + activity;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (executor == null) return true;
        executor.setActivity(activity);
        return true;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}