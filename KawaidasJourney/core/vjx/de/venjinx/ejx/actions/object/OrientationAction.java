package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class OrientationAction extends ObjectAction {

    private static final String SET_ORIENTATION_ACTION = "set_orientation_action";
    private static final String EXEC_MSG_ORIENTATION = " (orientation: ";

    private boolean right;
    private Orientation orientation;

    public OrientationAction() {
        name = SET_ORIENTATION_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_ORIENTATION + right;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        executor.setOrientation(orientation);
        return true;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }
}