package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SetVisibleAction extends ObjectAction {
    private static final String SET_VISIBLE_ACTION = "set_visible_action";
    private static final String EXEC_MSG_VISIBLE = " (visible: ";

    private boolean visible;

    public SetVisibleAction() {
        name = SET_VISIBLE_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_VISIBLE + visible;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        executor.setVisible(visible);
        return true;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}