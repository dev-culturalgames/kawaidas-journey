package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.logics.Box2DLogic;

public class DetachAction extends ObjectAction {
    private static final String DETACH_ACTION = "detach_action";

    private Box2DLogic physicsLogic;

    public DetachAction() {
        name = DETACH_ACTION;
    }

    @Override
    protected void init() {
        if (executor != null && executor.getPhysics() != null)
            physicsLogic = executor.getPhysics();
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (physicsLogic == null) return true;
        physicsLogic.detach();
        return true;
    }
}