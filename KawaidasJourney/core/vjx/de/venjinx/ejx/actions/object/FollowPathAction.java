package de.venjinx.ejx.actions.object;

import com.badlogic.gdx.scenes.scene2d.Actor;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class FollowPathAction extends ObjectAction {

    private static final String FOLLOW_PATH_ACTION = "move_to_wp_action";
    private static final String EXEC_MSG_WAYPOINT = " (waypoint: ";
    private static final String EXEC_MSG_DISAPPEAR = ", disappear: ";
    private static final String EXEC_MSG_LOOP = ", loop: ";
    private static final String EXEC_MSG_DELAY = ", delay: ";

    private String waypoints;
    private boolean disappear;
    private boolean loop;
    private float delay;

    private Actor followActor;
    private AppearAtWPAction[] wpActions;
    private AppearAtWPAction currentAction;

    private int currentActionID = 0;
    private float timer = 0;

    public FollowPathAction() {
        name = FOLLOW_PATH_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_WAYPOINT + waypoints;
        execMsg += EXEC_MSG_DISAPPEAR + disappear;
        execMsg += EXEC_MSG_LOOP + loop;
        execMsg += EXEC_MSG_DELAY + delay;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();

        if (followActor == null) followActor = actor;

        Waypoint wp;
        String[] wpSplit = waypoints.split(VJXString.SEP_LIST);
        wpActions = new AppearAtWPAction[wpSplit.length];
        AppearAtWPAction wpAction;
        for (int i = 0; i < wpSplit.length; i++) {
            wp = Waypoints.getWaypoint(wpSplit[i]);
            wpAction = EJXActions.appearAtWP(wp, disappear, delay);
            wpActions[i] = wpAction;
        }
        currentAction = wpActions[0];
        followActor.addAction(currentAction);
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        timer += delta;
        if (currentAction.isFinished() && timer >= delay) {
            currentAction.setFinished(false);
            currentActionID++;
            if (currentActionID == wpActions.length) {
                reset();
                return !loop;
            }

            timer -= delay;
            currentAction = wpActions[currentActionID];
            followActor.addAction(currentAction);
            return false;
        }
        return false;
    }

    @Override
    public void reset() {
        super.reset();
        currentActionID = 0;
        timer = 0;
    }

    public void setWaypoint(String wpName) {
        waypoints = wpName;
    }

    public void setDelay(float delay) {
        this.delay = delay;
    }

    public void setDisappear(boolean disappear) {
        this.disappear = disappear;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }
}