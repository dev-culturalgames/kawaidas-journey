package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class EntityExecuteCMDAction extends ObjectAction {
    private static final String ENTITY_EXEC_CMD_ACTION = "entity_exec_cmd_action";
    private static final String EXEC_MSG_CMD = " (cmd: ";

    Command command;

    public EntityExecuteCMDAction() {
        name = ENTITY_EXEC_CMD_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_CMD + command.toString();
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (executor == null) return true;
        executor.execSingleCmd(command);
        return true;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}