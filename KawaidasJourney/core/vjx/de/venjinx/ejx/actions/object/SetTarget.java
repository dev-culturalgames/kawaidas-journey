package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SetTarget extends ObjectAction {

    private static final String SET_TARGET_ACTION = "set_target_action";
    private static final String EXEC_MSG_TARGET = " (target: ";

    private EJXEntity target;

    public SetTarget() {
        name = SET_TARGET_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_TARGET + target;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (executor == null) return true;
        executor.setTargetEntity(target);
        return true;
    }

    public void setTargetEntity(EJXEntity target) {
        this.target = target;
    }
}