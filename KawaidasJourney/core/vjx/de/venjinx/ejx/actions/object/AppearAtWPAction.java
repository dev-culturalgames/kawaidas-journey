package de.venjinx.ejx.actions.object;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.actions.VisibleAction;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class AppearAtWPAction extends ObjectAction {

    private static final String MOVE_TO_WP_ACTION = "move_to_wp_action";
    private static final String EXEC_MSG_WAYPOINT = " (waypoint: ";
    private static final String EXEC_MSG_DISAPPEAR = ", disappear: ";
    private static final String EXEC_MSG_DURATION = ", duration: ";

    private Waypoint wp;
    private boolean disappear;
    private float duration;
    private boolean finished;

    private SequenceAction sequence;

    public AppearAtWPAction() {
        name = MOVE_TO_WP_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_WAYPOINT + wp;
        execMsg += EXEC_MSG_DISAPPEAR + disappear;
        execMsg += EXEC_MSG_DURATION + duration;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();

        sequence = Actions.sequence();

        MoveToAction ma = Actions.moveTo(wp.getCX() - actor.getWidth() / 2f,
                        wp.getCY() - actor.getHeight() / 2f);
        ma.setActor(actor);
        VisibleAction vaT = Actions.visible(true);
        vaT.setActor(actor);

        sequence.addAction(ma);
        sequence.addAction(vaT);

        if (actor instanceof SpineActor) {
            SpineActor sa = (SpineActor) actor;
            duration = sa.getAnimDuration("idle");
            sa.setAnimation("idle", true);
            sequence.addAction(Actions.delay(duration));
        }
        if (disappear) {
            VisibleAction vaF = Actions.visible(false);
            vaF.setActor(actor);
            sequence.addAction(vaF);
        }
        sequence.setActor(actor);
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);

        if (sequence.act(delta)) finished = true;
        return finished;
    }

    public void setWaypoint(Waypoint wp) {
        this.wp = wp;
    }

    public void setDisappear(boolean disappear) {
        this.disappear = disappear;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isFinished() {
        return finished;
    }
}