package de.venjinx.ejx.actions.object;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class TranslateAction extends ObjectAction {

    private static final String TRANSLATE_ACTION = "translate_action";
    private static final String EXEC_MSG_X = " (x: ";
    private static final String EXEC_MSG_Y = " (y: ";
    private static final String EXEC_MSG_DURATION = " (duration: ";

    private boolean finished = false;
    private MoveToAction moveAction = null;

    public TranslateAction() {
        name = TRANSLATE_ACTION;
        moveAction = Actions.moveTo(0, 0, 0);
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_X + moveAction.getX();
        execMsg = EXEC_MSG_Y + moveAction.getY();
        execMsg = EXEC_MSG_DURATION + moveAction.getDuration();
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        setActor(executor.getActor());
        moveAction.setTarget(actor);
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (executor == null) return true;
        finished = moveAction.act(delta);
        executor.setPosition(actor.getX(), actor.getY(), true, false);
        return finished;
    }

    @Override
    public void reset() {
        super.reset();
        finished = false;
        moveAction.reset();
    }

    public void set(Vector2 targetPos, float duration) {
        moveAction.setPosition(targetPos.x, targetPos.y);
        moveAction.setDuration(duration);
    }
}