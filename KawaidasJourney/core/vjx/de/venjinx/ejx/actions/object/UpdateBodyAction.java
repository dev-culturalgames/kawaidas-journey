package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;

public class UpdateBodyAction extends ObjectAction {

    private static final String UPDATE_BODY_ACTION = "update_body_action";

    public UpdateBodyAction() {
        name = UPDATE_BODY_ACTION;
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        executor.updateBody();
        return true;
    }
}