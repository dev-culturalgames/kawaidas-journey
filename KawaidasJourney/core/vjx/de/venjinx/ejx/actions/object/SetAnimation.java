package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SetAnimation extends ObjectAction {

    private static final String IDLE = "idle";
    private static final String SET_ANIMATION_ACTION = "set_animation_action";
    private static final String EXEC_MSG_ANIM = " (anim: ";
    private static final String EXEC_MSG_LOOP = ", loop: ";

    private AnimatedEntity animObject;
    protected String animName = IDLE;
    protected boolean loop = false;

    public SetAnimation() {
        name = SET_ANIMATION_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_ANIM + animName;
        execMsg += EXEC_MSG_LOOP + loop;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        if (executor != null && executor instanceof AnimatedEntity)
            animObject = (AnimatedEntity) executor;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (animObject != null) animObject.setAnimation(animName, loop);
        return true;
    }

    public void setAnimName(String animName) {
        this.animName = animName;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }
}