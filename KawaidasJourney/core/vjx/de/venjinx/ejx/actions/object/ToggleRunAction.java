package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class ToggleRunAction extends ObjectAction {

    private static final String TOGGLE_RUN_ACTION = "toggle_run_action";
    private static final String EXEC_MSG_ENABLE = " (enable: ";

    private MoveLogic moveLogic;
    private boolean enable;

    public ToggleRunAction() {
        name = TOGGLE_RUN_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_ENABLE + enable;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        if (executor != null && executor.hasLogic(VJXLogicType.MOVE))
            moveLogic = (MoveLogic) executor.getLogic(VJXLogicType.MOVE);
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (moveLogic == null) return true;
        moveLogic.setSpeedMod(1f, 1f);
        return true;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}