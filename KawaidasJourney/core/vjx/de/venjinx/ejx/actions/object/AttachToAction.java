package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class AttachToAction extends ObjectAction {

    private static final String ATTACH_TO_ACTION = "attachTo_action";
    private static final String EXEC_MSG_TARGET = " (target: ";

    private Box2DLogic physicsLogic;
    private EJXEntity attachTo;

    public AttachToAction() {
        name = ATTACH_TO_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_TARGET + attachTo;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        if (executor != null && executor.getPhysics() != null)
            physicsLogic = executor.getPhysics();
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (physicsLogic == null) return true;
        physicsLogic.attachTo(attachTo);
        return true;
    }

    public void attachTo(EJXEntity attachTo) {
        this.attachTo = attachTo;
    }
}