package de.venjinx.ejx.actions.object;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class BlinkAction extends ObjectAction {

    private static final String BLINK_ACTION = "blink_action";
    private static final String EXEC_MSG_DURATION = " (duration: ";
    private static final String EXEC_MSG_INTERVAL = ", interval: ";
    private static final String EXEC_MSG_COUNT = ", count: ";

    private float duration = 1f;
    private float interval = .5f;
    private int count = 0;

    public BlinkAction() {
        name = BLINK_ACTION;
    }

    @Override
    protected void init() {
        count = (int) (duration / interval / 2f);

        execMsg = EXEC_MSG_DURATION + duration;
        execMsg += EXEC_MSG_INTERVAL + interval;
        execMsg += EXEC_MSG_COUNT + count;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (actor == null) return true;

        if (duration <= 0 || count == 0) return true;

        SequenceAction sa = Actions.sequence();
        for (int i = 0; i < count; i++) {
            sa.addAction(blink());
        }
        actor.addAction(sa);

        return true;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public void setInterval(float interval) {
        this.interval = interval;
    }

    private SequenceAction blink() {
        return Actions.sequence(Actions.alpha(.5f, interval),
                        Actions.alpha(1f, interval));
    }
}