package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class StartMove extends ObjectAction {

    private static final String START_MOVE_ACTION = "start_move_action";
    private static final String EXEC_MSG_MOVE = " (move: ";
    private static final String EXEC_MSG_UPDATE_ACTIVITY = ", updateActivity: ";

    private boolean stop;
    private boolean updateActivity;
    private MoveLogic moveLogic;

    public StartMove() {
        name = START_MOVE_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_MOVE + stop;
        execMsg += EXEC_MSG_UPDATE_ACTIVITY + updateActivity;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        if (executor != null && executor.hasLogic(VJXLogicType.MOVE))
            moveLogic = (MoveLogic) executor.getLogic(VJXLogicType.MOVE);
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (moveLogic == null) return true;
        if (!stop) {
            moveLogic.startMove();
        } else moveLogic.stop(updateActivity);
        return true;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public void setUpdateActivity(boolean updateActivity) {
        this.updateActivity = updateActivity;
    }
}