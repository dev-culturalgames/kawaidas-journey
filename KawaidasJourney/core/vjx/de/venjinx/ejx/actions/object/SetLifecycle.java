package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SetLifecycle extends ObjectAction {

    private static final String SET_LIFECYCLE_ACTION = "set_lifecycle_action";
    private static final String EXEC_MSG_LIFECYCLE = " (lifecycle: ";

    private Lifecycle lifecycle = Lifecycle.CREATED;

    public SetLifecycle() {
        name = SET_LIFECYCLE_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_LIFECYCLE + lifecycle;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (executor == null) return true;
        executor.setLifecycle(lifecycle);
        return true;
    }

    public void setLifecycle(Lifecycle lifecycle) {
        this.lifecycle = lifecycle;
    }
}