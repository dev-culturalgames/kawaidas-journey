package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SetGhost extends ObjectAction {

    private static final String SET_GHOST_ACTION = "set_ghost_action";
    private static final String EXEC_MSG_GHOST = " (ghost: ";

    private Box2DLogic phyicsLogic;
    private boolean ghost = false;

    public SetGhost() {
        name = SET_GHOST_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_GHOST + ghost;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        if (executor != null && executor.getPhysics() != null) {
            phyicsLogic = executor.getPhysics();
        }
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (phyicsLogic == null) return true;
        phyicsLogic.setGhost(ghost);
        return true;
    }

    public void setGhost(boolean ghost) {
        this.ghost = ghost;
    }
}