package de.venjinx.ejx.actions.object;

import com.badlogic.gdx.math.Vector2;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SetPosition extends ObjectAction {

    private static final String SET_POSITION_ACTION = "set_position_action";
    private static final String EXEC_MSG_POSITION = " (position: ";

    private Vector2 position = new Vector2();

    public SetPosition() {
        name = SET_POSITION_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_POSITION + position;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (executor == null) return true;
        executor.setPosition(position, true, true);
        return true;
    }

    public void setPosition(Vector2 position) {
        setPosition(position.x, position.y);
    }

    public void setPosition(float x, float y) {
        position.set(x, y);
    }
}