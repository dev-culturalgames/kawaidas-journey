package de.venjinx.ejx.actions.object;

import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SetWaypoint extends ObjectAction {

    private static final String SET_WAYPOINT_ACTION = "set_waypoint_action";
    private static final String EXEC_MSG_WAYPOINT = " (waypoint: ";

    private Waypoint waypoint;
    private MoveLogic moveLogic;

    public SetWaypoint() {
        name = SET_WAYPOINT_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_WAYPOINT + waypoint;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        if (executor != null && executor.hasLogic(VJXLogicType.MOVE))
            moveLogic = (MoveLogic) executor.getLogic(VJXLogicType.MOVE);
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (moveLogic == null) return true;
        if (waypoint != null) moveLogic.setWaypoint(waypoint);
        else moveLogic.setWaypoint(moveLogic.nextWaypoint());
        return true;
    }

    public void setWaypoint(Waypoint waypoint) {
        this.waypoint = waypoint;
    }
}