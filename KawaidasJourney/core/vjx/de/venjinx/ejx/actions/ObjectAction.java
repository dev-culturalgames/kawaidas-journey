package de.venjinx.ejx.actions;

import com.badlogic.gdx.scenes.scene2d.Action;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public abstract class ObjectAction extends Action {

    private static final String OBJECT_ACTION = "object_action";
    private static final String ACTION = "Action: ";
    private static final String EXECUTE = " execute ";
    protected String execMsg = VJXString.STR_EMPTY;

    private boolean initialized = false;
    protected String name = OBJECT_ACTION;

    protected EJXEntity executor;

    @Override
    public boolean act(float delta) {
        if (!initialized) {
            init();
            VJXLogger.log(LogCategory.EXEC,
                            ACTION + executor + EXECUTE + name + execMsg, 1);
        }
        return true;
    }

    protected void init() {
        initialized = true;
    }

    @Override
    public void reset() {
        super.reset();
        initialized = false;
    }

    public void setEntity(EJXEntity entity) {
        executor = entity;
    }
}