package de.venjinx.ejx.stages;

import java.util.HashMap;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.stages.DebugStage.DebugStats;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public abstract class EJXStage extends Stage {

    private String name = "VJX-stage";

    protected EJXGame game;

    protected HashMap<String, AssetDescriptor<? extends Object>> resources;

    private boolean active = false;
    private boolean runBackground = false;
    private boolean initialized = false;
    protected long stageFrameNr = 0;

    protected boolean dataPrepared = false;

    public EJXStage(EJXGame game) {
        super(game.getDisplayView(), game.batch);
        this.game = game;
        resources = new HashMap<>();
        getRoot().setName(name + "_root");
    }

    @Override
    public void act(float delta) {
        if (!active && getRoot().getActions().size == 0) return;

        VJXLogger.logIncr(LogCategory.STAGE | LogCategory.FLOW,
                        "Stage '" + name + "' acting at frame #"
                                        + stageFrameNr + ". Delta time: " + delta);

        VJXLogger.logIncr(LogCategory.STAGE | LogCategory.FLOW
                        | LogCategory.DETAIL,
                        "Stage '" + name + "' started pre-act.");
        preAct(delta);

        VJXLogger.logDecr(LogCategory.STAGE | LogCategory.FLOW
                        | LogCategory.DETAIL,
                        "Stage '" + name + "' finished pre-act.");
        if (this instanceof LevelStage) {
            DebugStats.objectsUpdateTime = 0;
            DebugStats.levelUpdateTime = System.nanoTime();
        }
        super.act(delta);
        if (this instanceof LevelStage)
            DebugStats.levelUpdateTime = System.nanoTime()
            - DebugStats.levelUpdateTime;

        VJXLogger.logIncr(LogCategory.STAGE | LogCategory.FLOW
                        | LogCategory.DETAIL,
                        "Stage '" + name + "' started post-act.");
        postAct(delta);

        if (active || getRoot().getActions().size > 0) stageFrameNr++;

        VJXLogger.logDecr(LogCategory.STAGE | LogCategory.FLOW
                        | LogCategory.DETAIL,
                        "Stage '" + name + "' finished post-act.");

        VJXLogger.logDecr(LogCategory.STAGE | LogCategory.FLOW,
                        "Stage '" + name + "' acted at frame #" + stageFrameNr);
    }

    @Override
    public void draw() {
        if (!runBackground) {
            VJXLogger.logIncr(LogCategory.DRAW, "Stage '" + name
                            + "' drawing frame #" + stageFrameNr);

            preDraw();
            super.draw();
            postDraw();

            VJXLogger.logDecr(LogCategory.DRAW, "Stage '" + name + "' frame #"
                            + stageFrameNr + " drawn.");
        }
    }

    public void init() {
        long time = System.currentTimeMillis();
        VJXLogger.logIncr(LogCategory.FLOW | LogCategory.LOAD | LogCategory.INIT,
                        "Stage '" + name + "' started initialization.");
        userInit();
        initialized = true;

        VJXLogger.logDecr(
                        LogCategory.FLOW | LogCategory.LOAD | LogCategory.INIT,
                        "Stage '" + name + "' finished initialization. Took "
                                        + (System.currentTimeMillis() - time) + "ms.");
    }

    public void reset() {
        dataPrepared = false;
        initialized = false;
        active = false;
        stageFrameNr = 0;
    }

    protected abstract void userInit();

    protected abstract void preAct(float delta);

    protected abstract void postAct(float delta);

    protected abstract void preDraw();

    protected abstract void postDraw();

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean setZoom(float zoom) {
        if (getCamera() instanceof OrthographicCamera) {
            ((OrthographicCamera) getCamera()).zoom = zoom;
            game.getScreen().getRenderer().setZoom(zoom);
            return true;
        }
        return false;
    }

    public float getZoom() {
        if (getCamera() instanceof OrthographicCamera) { return ((OrthographicCamera) getCamera()).zoom; }
        return 1;
    }

    public EJXGame getGame() {
        return game;
    }

    public HashMap<String, AssetDescriptor<? extends Object>> getResources() {
        return resources;
    }

    public void setActive(boolean active) {
        if (active != this.active) {
            this.active = active;
        }
    }

    public void setActive(final boolean active, float delay) {
        if (delay > 0f)
            addAction(Actions.sequence(Actions.delay(delay),
                            Actions.run(new Runnable() {
                                @Override
                                public void run() {
                                    setActive(active);
                                }
                            })));
        else setActive(active);
    }

    public void setRunBackground(boolean background) {
        float alpha = background ? 0f : 1f;
        getRoot().getColor().a = alpha;
        runBackground = background;
    }

    public void setRunBackground(final boolean background, float duration) {
        if (runBackground == background) return;
        float alpha = background ? 0f : 1f;
        if (duration > 0f) {
            SequenceAction sa = Actions
                            .sequence(Actions.alpha(alpha, duration));

            if (background) sa.addAction(Actions.run(new Runnable() {
                @Override
                public void run() {
                    runBackground = background;
                }
            }));
            else runBackground = background;

            addAction(sa);
        } else {
            setRunBackground(background);
        }
    }

    private Runnable setRunBG = new Runnable() {

        @Override
        public void run() {
            runBackground = true;
        }
    };

    public void setRunBackground(final boolean background, float duration,
                    float delay) {
        if (runBackground == background) return;
        float alpha = background ? 0 : 1;
        SequenceAction sa = Actions.sequence();
        if (delay > 0) sa.addAction(Actions.delay(delay));

        if (duration > 0) {
            sa.addAction(Actions.alpha(alpha, duration));

            if (background) sa.addAction(Actions.run(setRunBG));
            else runBackground = background;

            addAction(sa);
        } else {
            setRunBackground(background);
        }
    }

    public boolean isActive() {
        return active;
    }

    public boolean runBackground() {
        return runBackground;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public boolean dataIsPrepared() {
        return dataPrepared;
    }

    public long getFrameNr() {
        return stageFrameNr;
    }

    @Override
    public void dispose() {
        VJXLogger.logIncr(
                        LogCategory.STAGE | LogCategory.FLOW | LogCategory.INIT
                        | LogCategory.LOAD,
                        "Stage '" + name + "' disposing.");
        super.dispose();

        for (String s : resources.keySet())
            game.assets.unloadResource(s);

        reset();
        dataPrepared = false;

        VJXLogger.logDecr(
                        LogCategory.STAGE | LogCategory.FLOW | LogCategory.INIT
                        | LogCategory.LOAD,
                        "Stage '" + name + "' disposed.");
    }

    @Override
    public String toString() {
        String s = this.getClass().getSimpleName() + " - " + getName();
        s += " - prepared: " + initialized;

        return s;
    }
}