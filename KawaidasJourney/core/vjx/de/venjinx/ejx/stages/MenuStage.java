package de.venjinx.ejx.stages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapImageLayer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ObjectMap;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.EJXGameScreen;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.controllers.AudioControl;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.tiled.TiledMenu;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.vjx.ui.EJXMenuTable;
import de.venjinx.vjx.ui.IngameHUD;
import de.venjinx.vjx.ui.IngameHUD.Transition;
import de.venjinx.vjx.ui.MenuTable;

public abstract class MenuStage extends EJXStage {
    public static final String SFX_BACK = "sfx:back";

    public static final String img_loading_bg_lvl = "loading_bg_lvl";
    public static final String img_unloading_bg_lvl = "unloading_bg_lvl";
    public static final float PAD_X = 20, PAD_Y = 20;

    protected Skin skin;
    protected IngameHUD hud;
    protected float fadeUI = .2f;

    protected ObjectMap<Integer, MenuTable> menus;
    protected ObjectMap<Integer, EJXMenuTable> popups;
    private List<MenuTable> lastMenus;
    public HashMap<String, TiledMap> maps;

    protected MenuTable currentMenu;
    protected EJXMenuTable currentPopup;
    protected Image popupBG;

    public final ClickListener uiBackListener = initUIBackListener();
    public final ClickListener backToMapListener = initBackToMapListener();

    private Color tmpColor = new Color();
    private HashMap<Transition, Actor> overlays = new HashMap<>();

    public MenuStage(EJXGame game, String name) {
        super(game);
        setName(name);
        menus = new ObjectMap<>();
        popups = new ObjectMap<>();
        lastMenus = new ArrayList<>();
        maps = new HashMap<>();
        createOverlays();
    }

    public void setUserInterface(HashMap<String, TiledMap> maps, Skin skin) {
        this.maps.putAll(maps);

        // TODO optimize font loading takes a lot of time
        this.skin = skin;

        TiledMapImageLayer imgLayer;
        TextureRegion tr;

        TiledMap m;
        for (String mName : maps.keySet()) {
            m = maps.get(mName);
            imgLayer = (TiledMapImageLayer) m.getLayers().get("background");
            if (imgLayer == null) continue;
            tr = imgLayer.getTextureRegion();
            skin.add(mName, new TextureRegionDrawable(tr), Drawable.class);
        }

        dataPrepared = true;
    }

    public EJXGameScreen getScreen() {
        return getGame().getScreen();
    }

    public void showMessageToUser(String title, String msg) {
        VJXLogger.log(title + VJXString.SEP_DDOT_SPACE + msg);
    }

    public void showWaiting(boolean show) {
    }

    public void enterCallback() {
        if (currentPopup != null) currentPopup.enterCallback();
        else currentMenu.enterCallback();
    }

    public void initMenus() {
        for (MenuTable entry : menus.values()) {
            if (entry instanceof TiledMenu) ((TiledMenu) entry).load();
        }
    }

    public void addMenu(int id, MenuTable menu) {
        menus.put(id, menu);
    }

    public MenuTable getMenu(int menuIndex) {
        return menus.get(menuIndex);
    }

    public MenuTable getMenu(String name) {
        return menus.get(getMenuId(name));
    }

    public void setMenu(int menuIndex) {
        showUI(menus.get(menuIndex), 0, 0);
    }

    public int getMenuId(String name) {
        MenuTable menu;
        for (int id : menus.keys()) {
            menu = getMenu(id);
            if (menu.getName().equals(name)) return id;
        }
        return -1;
    }

    public int getCurrentMenuId() {
        EJXMenuTable menu;
        for (int id : menus.keys()) {
            menu = menus.get(id);
            if (currentMenu == menu) return id;
        }
        return -1;
    }

    public MenuTable getCurrentMenu() {
        return currentMenu;
    }

    public TiledMenu getCurrentTiled() {
        if (!(currentMenu instanceof TiledMenu)) return null;
        return (TiledMenu) currentMenu;
    }

    public void switchMenu(int menuIndex) {
        //        switchMenu(menuIndex, fadeUI, fadeUI, false);
        switchMenu(menuIndex, fadeUI, fadeUI);
    }

    public void switchMenu(int menuIndex, boolean saveHistory) {
        MenuTable menu = menus.get(menuIndex);
        switchToUI(menu, fadeUI, fadeUI, saveHistory);
    }

    public void switchMenu(int menuIndex, float fadeOutDuration,
                    float fadeInDuration) {
        //        switchMenu(menuIndex, fadeOutDuration, fadeInDuration, false);
        MenuTable menu = menus.get(menuIndex);
        //        switchMenu(menu, fadeOutDuration, fadeInDuration, false);
        switchToUI(menu, fadeOutDuration, fadeInDuration, true);
    }

    //    public void switchMenu(int menuIndex, float fadeOutDuration,
    //                    float fadeInDuration, boolean clearHistory) {
    //        MenuTable menu = menus.get(menuIndex);
    //        switchMenu(menu, fadeOutDuration, fadeInDuration, clearHistory);
    //    }

    //    public void switchMenu(MenuTable menu) {
    //        //        if (clearHistory) lastMenus.clear();
    //        switchToUI(menu, fadeUI, fadeUI, true);
    //        //        switchMenu(menu, fadeUI, fadeUI, false);
    //    }

    //    public void switchMenu(MenuTable menu, boolean clearHistory) {
    //        switchMenu(menu, fadeUI, fadeUI, clearHistory);
    //    }

    //    public void switchMenu(MenuTable menu, float fadeOutDuration,
    //                    float fadeInDuration, boolean clearHistory) {
    //        if (clearHistory) lastMenus.clear();
    //        switchToUI(menu, fadeOutDuration, fadeInDuration, true);
    //    }

    public void switchMenu(int menuIndex, float fadeOutDuration,
                    float fadeInDuration, Transition transition) {
        switchMenu(menuIndex, fadeOutDuration, fadeInDuration, true, transition);
    }

    public void switchMenu(int menuIndex, float fadeOutDuration,
                    float fadeInDuration, boolean saveHistory,
                    Transition transition) {
        MenuTable menu;
        if (menuIndex < 0 || menuIndex > menus.size) menu = getMenu(0);
        else menu = getMenu(menuIndex);

        //        if (clearHistory) lastMenus.clear();

        switchToUI(menu, fadeOutDuration, fadeInDuration, saveHistory, transition);
    }

    public void clearHistory() {
        lastMenus.clear();
    }

    protected void switchToUI(MenuTable ui) {
        switchToUI(ui, fadeUI, fadeUI, true);
    }

    protected void switchToUI(MenuTable ui, boolean saveHistory) {
        switchToUI(ui, fadeUI, fadeUI, saveHistory);
    }

    protected void switchToUI(MenuTable ui, float fadeOutDuration,
                    float fadeInDuration, boolean saveHistory) {
        if (ui != currentMenu) {
            if (currentMenu != null) {
                VJXLogger.log(LogCategory.UI,
                                "User interface switch from menu '"
                                                + currentMenu.getName() + "' to '"
                                                + (ui == null ? ui
                                                                : ui.getName())
                                                + "'.",
                                                2);
                hideUI(currentMenu, fadeOutDuration);
                if (saveHistory) {
                    lastMenus.add(currentMenu);
                }

            }

            showUI(ui, fadeInDuration, fadeInDuration);
        }
    }

    protected void switchToUI(MenuTable ui, float fadeOutDuration,
                    float fadeInDuration, boolean saveHistory,
                    Transition transition) {

        if (ui != currentMenu) {
            toggleOverlay(Transition.BLACKSCREEN, true, 0f, 0f);

            if (currentMenu != null) {
                VJXLogger.log(LogCategory.UI,
                                "User interface switch from menu '"
                                                + currentMenu.getName()
                                                + "' to '" + ui.getName()
                                                + "'.", 2);
                hideUI(currentMenu, fadeOutDuration);
                if (saveHistory) {
                    VJXLogger.log("save " + currentMenu.getName());
                    lastMenus.add(currentMenu);
                }

            }

            showUI(ui, 0f, 0f);
            toggleOverlay(Transition.BLACKSCREEN, false, fadeInDuration,
                            fadeOutDuration);
        }
    }

    public void restoreUI() {
        if (currentPopup != null) {
            hidePopup();
        } else {
            if (lastMenus.size() > 0) {
                MenuTable menu = lastMenus.remove(lastMenus.size() - 1);
                switchToUI(menu, false);
            }
        }
    }

    public EJXMenuTable getPopup(int dialogueIndex) {
        if (dialogueIndex < 0 || dialogueIndex > popups.size)
            return popups.get(0);
        else return popups.get(dialogueIndex);
    }

    public int getCurrentPopupId() {
        EJXMenuTable popup;
        for (int id : popups.keys()) {
            popup = popups.get(id);
            if (currentPopup == popup) return id;
        }
        return -1;
    }

    public void showPopup(int dialogueIndex) {
        showPopup(popups.get(dialogueIndex), true, null);
    }

    public void showPopup(int dialogueIndex, boolean bounce) {
        showPopup(popups.get(dialogueIndex), bounce, null);
    }

    protected void showPopup(EJXMenuTable popup, boolean bounceIn, String hideType) {
        if (currentMenu != null) currentMenu.setTouchable(Touchable.disabled);

        removePopup(null);

        currentPopup = popup;
        addActor(popupBG);
        addActor(currentPopup);
        currentPopup.setTouchable(Touchable.enabled);
        if (hideType == null) {
            popupBG.setPosition(0, 0);
            popupBG.setFillParent(true);
        } else if (hideType.equals("left")) {
            popupBG.setFillParent(false);
            popupBG.setPosition(0, 0);
            popupBG.setSize(getWidth() / 2, getHeight());
        } else {
            popupBG.setFillParent(false);
            popupBG.setPosition(getWidth() / 2, 0);
            popupBG.setSize(getWidth() / 2, getHeight());
        }

        if (bounceIn) {
            currentPopup.addAction(EJXActions.bounceIn(.7f, 1.1f, .2f));
        }

        game.sfx.pauseEntitySounds();
        currentPopup.updateLayout(getSkin());
        currentPopup.entered(game, this);
    }

    public void hidePopup() {
        hidePopup(null, true);
    }

    public void hidePopup(boolean bounceOut) {
        hidePopup(null, bounceOut);
    }

    public void hidePopup(Runnable onClosed) {
        hidePopup(onClosed, true);
    }

    public void hidePopup(final Runnable onClosed, boolean bounceOut) {
        if (currentMenu != null) currentMenu.setTouchable(Touchable.enabled);
        if (currentPopup == null) return;

        game.sfx.resumeEntitySounds();
        currentPopup.setTouchable(Touchable.disabled);

        if (!bounceOut) {
            removePopup(onClosed);
        } else {
            SequenceAction sa = Actions.sequence();
            sa.addAction(EJXActions.bounceOut(.7f, 1.1f, .2f));
            sa.addAction(Actions.run(new Runnable() {
                @Override
                public void run() {
                    removePopup(onClosed);
                }
            }));
            currentPopup.addAction(sa);
        }
    }

    public void updateHUD() {
        updateHUD(false);
    }

    public void updateHUD(boolean decrMovingItems) {
        hud.update(decrMovingItems);
    }

    public IngameHUD getUI() {
        return hud;
    }

    @SuppressWarnings("unchecked")
    public <T> T getUI(Class<T> clazz) {
        return (T) hud;
    }

    public void lvlLoaded() {
        for (EJXMenuTable t : menus.values())
            t.lvlLoaded();
        for (EJXMenuTable t : popups.values())
            t.lvlLoaded();
    }

    public void updateLanguage() {
        for (EJXMenuTable menu : menus.values()) {
            menu.updateLanguage(TextControl.getInstance());
        }
        for (EJXMenuTable popup : popups.values()) {
            popup.updateLanguage(TextControl.getInstance());
        }
    }

    public void toggleOverlay(Transition transition, boolean on) {
        toggleOverlay(transition, on, .25f, 0f);
    }

    public void toggleOverlay(Transition transition, boolean on, float duration,
                    float delay) {
        if (on) {
            Actor overlay = overlays.get(transition);
            addActor(overlay);
            toggleActor(overlay, true, duration, delay);
        } else {
            Actor overlay = overlays.get(transition);
            if (!overlay.hasParent()) return;

            toggleActor(overlay, false, duration, delay);
            SequenceAction sa = Actions.sequence();
            sa.addAction(Actions.delay(duration + delay));
            sa.addAction(Actions.removeActor());
            overlay.addAction(sa);
        }
    }

    public void toggleActor(Actor actor, boolean on, float duration,
                    float delay) {
        SequenceAction sa = Actions.sequence();
        if (on) {
            tmpColor.set(1, 1, 1, 1);
            actor.setTouchable(Touchable.enabled);
            if (duration == 0 && delay == 0) {
                actor.clearActions();
                actor.setColor(tmpColor);
            } else {
                sa.addAction(Actions.delay(delay));
                sa.addAction(Actions.color(tmpColor, duration));
                actor.addAction(sa);
            }
        } else {
            tmpColor.set(0, 0, 0, 0);
            actor.setTouchable(Touchable.disabled);
            if (duration == 0 && delay == 0) {
                actor.clearActions();
                actor.setColor(tmpColor);
            } else {
                sa.addAction(Actions.delay(delay));
                sa.addAction(Actions.color(tmpColor, duration));
                actor.addAction(sa);
            }
        }
    }

    public void playMusic(String name) {
        game.sfx.playMusic(name);
    }

    public void playMusic(FileHandle file) {
        game.sfx.playMusic(file);
    }

    public void pauseMusic() {
        game.sfx.pauseMusic();
    }

    public void resumeMusic() {
        game.sfx.resumeMusic();
    }

    public void stopMusic() {
        game.sfx.stopMusic();
    }

    public Skin getSkin() {
        return skin;
    }

    private void showUI(MenuTable ui, float duration, float delay) {
        if (ui == null) {
            currentMenu = ui;
            getScreen().setMapToRender(null);
            return;
        }

        if (ui instanceof TiledMenu) {
            TiledMenu tm = (TiledMenu) ui;
            getScreen().setMapToRender(tm.getMap());
        }

        if (currentMenu != null && currentMenu != ui)
            getRoot().addActorBefore(currentMenu, ui);
        else addActor(ui);

        currentMenu = ui;

        currentMenu.clearActions();
        currentMenu.addAction(Actions.sequence(Actions.alpha(0), Actions.delay(delay),
                        Actions.fadeIn(duration)));
        currentMenu.updateLayout(getSkin());
        currentMenu.entered(game, this);
    }

    private void hideUI(final EJXMenuTable ui, float duration) {
        ui.clearActions();
        ui.addAction(Actions.sequence(Actions.alpha(1),
                        Actions.fadeOut(duration), Actions.removeActor()));
        ui.exited(game, this);
    }

    private void removePopup(Runnable onClosed) {
        popupBG.remove();
        if (currentPopup == null) return;

        currentPopup.remove();
        currentPopup.exited(game, this);
        currentPopup = null;

        if (onClosed != null) onClosed.run();
    }

    private void createOverlays() {
        Pixmap blackOverlay = new Pixmap(1280, 720, Format.RGBA8888);
        Texture t;
        Image overlay;

        blackOverlay.setColor(Color.BLACK);
        blackOverlay.fillRectangle(0, 0, (int) getWidth(), 90);
        blackOverlay.fillRectangle(0, (int) getHeight() - 90, (int) getWidth(), 90);
        t = new Texture(blackOverlay);

        overlay = new Image(t);
        overlay.setName(Transition.VIDEO_SEQUENCE.name());
        overlay.setTouchable(Touchable.disabled);
        overlays.put(Transition.VIDEO_SEQUENCE, overlay);

        blackOverlay.setColor(Color.BLACK);
        blackOverlay.fillRectangle(0, 0, (int) getWidth(), (int) getHeight());
        t = new Texture(blackOverlay);

        overlay = new Image(t);
        overlay.setName(Transition.BLACKSCREEN.name());
        overlay.setTouchable(Touchable.disabled);
        overlays.put(Transition.BLACKSCREEN, overlay);
    }

    private ClickListener initUIBackListener() {
        final AudioControl ac = game.sfx;
        return new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                ac.playDynSFX(SFX_BACK);
                restoreUI();
            }
        };
    }

    private ClickListener initBackToMapListener() {
        final AudioControl ac = game.sfx;
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ac.stopAllSounds();
                ac.playDynSFX(SFX_BACK);
                getScreen().unloadLevel(false);
            }
        };
    }
}