package de.venjinx.ejx.stages;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.TreeMap;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.Transform;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.SnapshotArray;
import com.badlogic.gdx.utils.viewport.FitViewport;

import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.EJXPlayer;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.CollisionObject;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EJXGroup;
import de.venjinx.ejx.entity.EntityDef;
import de.venjinx.ejx.entity.PlayerEntity;
import de.venjinx.ejx.entity.TiledEntity;
import de.venjinx.ejx.entity.logics.EJXLogics.EJXLogicType;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.scenegraph.EJXScenegraph;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.scenegraph.WorldLayer;
import de.venjinx.ejx.scenegraph.WorldLayer.LayerCell;
import de.venjinx.ejx.stages.DebugStage.DebugStats;
import de.venjinx.ejx.util.EJXTypes.Category;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.SpatialProperty;
import de.venjinx.ejx.util.EJXTypes.Tiles;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public abstract class LevelStage extends EJXStage {

    // general
    private static final String DEPTH_ENABLED = "depth:enabled";
    private static final String PARALLAX_VALUE = "parallax:value";
    private static final String LIGHT_ENABLED = "lights:enabled";
    private static final String WIN_VALUE = "condition:win:value";

    public static abstract class B2DWorld {

        // physics world unit to game world unit conversion multipliers
        public static final float WORLD_TO_B2D = .01f;
        public static final float B2D_TO_WORLD = 100f;

        // physics
        public static final Vector2 GRAVITY = new Vector2(0f, 1f);
        public static final float WORLD_UNIT = 256f;
        public static final float WORLD_STEP = 1f / 60f;
        public static final int VEL_ITERATIONS = 1;
        public static final int POS_ITERATIONS = 1;
        public static final Vector2 WORLD_GRAVITY = new Vector2(0f, 0);
        public static final float DFLT_GRAVITY_SCALE = 4f;
        public static final float BASE_FRICTION = 1f;
        public static final int LIGHT_RAY_COUNT = 32;

        private static final Vector2 tmpVec0 = new Vector2();

        public static final Vector2 getFixtureWorldCenter(Fixture fixture) {
            return getFixtureCenter(fixture, false);
        }

        public static final Vector2 getFixtureLocalCenter(Fixture fixture) {
            return getFixtureCenter(fixture, true);
        }

        public static final Vector2 getFixtureCenter(Fixture fixture, boolean local) {
            if (fixture.getShape() instanceof CircleShape) {
                tmpVec0.setZero();
                Shape s = fixture.getShape();
                tmpVec0.set(((CircleShape) s).getPosition());
                if (!local) fixture.getBody().getTransform().mul(tmpVec0);
                tmpVec0.scl(B2D_TO_WORLD);
                return tmpVec0;
            }

            if (fixture.getShape() instanceof PolygonShape) {
                float w = getFixtureWidth(fixture);
                float h = getFixtureHeight(fixture);
                getFixturePosition(fixture, local);
                return tmpVec0.add(w / 2f, h / 2f);
            }

            return tmpVec0.setZero();
        }

        public static final Vector2 getFixtureWorldPosition(Fixture fixture) {
            return getFixturePosition(fixture, false);
        }

        public static final Vector2 getFixtureLocalPosition(Fixture fixture) {
            return getFixturePosition(fixture, true);
        }

        public static final Vector2 getFixturePosition(Fixture fixture, boolean local) {
            tmpVec0.setZero();
            Shape s = fixture.getShape();

            if (s instanceof CircleShape) {
                float radius = fixture.getShape().getRadius() * B2D_TO_WORLD;
                getFixtureCenter(fixture, local);
                return tmpVec0.sub(radius, radius);
            }

            if (s instanceof PolygonShape) {
                Transform t = fixture.getBody().getTransform();
                PolygonShape ps = (PolygonShape) s;
                int count = ps.getVertexCount();
                if (count == 0) return tmpVec0;
                if (count == 1) {
                    ps.getVertex(0, tmpVec0);
                    if (!local) t.mul(tmpVec0);
                    tmpVec0.scl(B2D_TO_WORLD);
                    return tmpVec0;
                }

                ps.getVertex(0, tmpVec0);
                float minX = tmpVec0.x, minY = tmpVec0.y;
                for (int i = 1; i < count; i++) {
                    ps.getVertex(i, tmpVec0);
                    minX = Math.min(minX, tmpVec0.x);
                    minY = Math.min(minY, tmpVec0.y);
                }
                tmpVec0.set(minX, minY);
                if (!local) t.mul(tmpVec0);
                tmpVec0.scl(B2D_TO_WORLD);

                return tmpVec0;
            }

            return tmpVec0;
        }

        public static final float getFixtureWidth(Fixture fixture) {
            tmpVec0.setZero();

            Body b = fixture.getBody();
            if (b == null) return 0;

            Shape s = fixture.getShape();

            if (s instanceof CircleShape) return s.getRadius() * B2D_TO_WORLD;

            if (s instanceof PolygonShape) {
                PolygonShape ps = (PolygonShape) s;
                int count = ps.getVertexCount();
                if (count < 2) return 0;

                ps.getVertex(0, tmpVec0);
                float minX = tmpVec0.x, maxX = tmpVec0.x;
                for (int i = 1; i < count; i++) {
                    ps.getVertex(i, tmpVec0);
                    minX = Math.min(minX, tmpVec0.x);
                    maxX = Math.max(maxX, tmpVec0.x);
                }

                return (maxX - minX) * B2D_TO_WORLD;
            }

            return 0;
        }

        public static final float getFixtureHeight(Fixture fixture) {
            tmpVec0.setZero();

            Body b = fixture.getBody();
            if (b == null) return 0;

            Shape s = fixture.getShape();

            if (s instanceof CircleShape) { return s.getRadius(); }

            if (s instanceof PolygonShape) {
                PolygonShape ps = (PolygonShape) s;
                int count = ps.getVertexCount();
                if (count < 2) return 0;

                ps.getVertex(0, tmpVec0);
                float minY = tmpVec0.y, maxY = tmpVec0.y;
                for (int i = 1; i < count; i++) {
                    ps.getVertex(i, tmpVec0);
                    minY = Math.min(minY, tmpVec0.y);
                    maxY = Math.max(maxY, tmpVec0.y);
                }

                return (maxY - minY) * B2D_TO_WORLD;
            }

            return 0;
        }

        public static final Vector2 getFixtureBounds(Fixture fixture) {
            tmpVec0.set(getFixtureWidth(fixture), getFixtureHeight(fixture));
            return tmpVec0;
        }
    }

    private Vector2 tmpVec = new Vector2();
    private Vector2 viewSize = new Vector2();

    private EJXCamera cam;

    // game world world
    private float worldWidth;
    private float worldHeight;

    private OrthographicCamera b2dCam;
    protected World b2dWorld;

    protected TiledMap map;
    protected EJXScenegraph scene;

    // level settings
    private String levelName;
    private int mapWidth;
    private int mapHeight;
    private int tileWidth;
    private int tileHeight;

    private float preloadBoundMult = 1.5f;
    private Rectangle preLoadBound;
    private Rectangle screenBound;
    private Rectangle lvlBound;

    protected TreeMap<Long, EJXEntity> entities;
    private ArrayDeque<Long> removeIDs;
    private int onScreenObjects;

    private boolean depthEnabled;
    private boolean lightsEnabled;

    protected EJXEntity camEntity;
    protected PlayerEntity playerEntity;

    private float currentProgress;
    protected float maxProgress;
    protected float winProgress;
    private boolean finished;

    public LevelStage(EJXGame game) {
        super(game);
        setName("VJX-level-stage");

        viewSize.set(1280, 720);

        cam = new EJXCamera();
        setViewport(new FitViewport(viewSize.x, viewSize.y, cam));

        b2dCam = new OrthographicCamera();
        b2dCam.setToOrtho(false,
                        getViewport().getWorldWidth() * B2DWorld.WORLD_TO_B2D,
                        getViewport().getWorldHeight() * B2DWorld.WORLD_TO_B2D);
        b2dCam.zoom = cam.zoom;

        lvlBound = new Rectangle();

        screenBound = new Rectangle(0, 0,
                        viewSize.x * cam.zoom, viewSize.y * cam.zoom);
        screenBound.setCenter(cam.position.x, cam.position.y);
        cam.setScreenBound(screenBound);

        preLoadBound = new Rectangle(
                        viewSize.x * cam.zoom * preloadBoundMult, 0,
                        viewSize.x * cam.zoom * preloadBoundMult,
                        viewSize.y * cam.zoom * preloadBoundMult);

        scene = new EJXScenegraph();
        entities = new TreeMap<>();
        removeIDs = new ArrayDeque<>();
    }

    protected abstract void initResources();

    public abstract void restart();

    public abstract void start();

    protected abstract void progressComplete();

    public abstract void win();

    public abstract boolean isProgressFinished();

    protected abstract void checkSpawn(EJXEntity entity, boolean spawned);

    @Override
    public void userInit() {
        game.factory.loadAnims();

        scene.setupScenegraph();
        scene.setupLevelNodes(map, depthEnabled);
        addActor(scene);

        createPlayerAvatar();

        game.getScreen().getRenderer().initLight(b2dWorld, lightsEnabled);

        restart();
    }

    @Override
    public void preAct(float delta) {
        game.gameControl.act(delta);
    }

    @Override
    public void postAct(float delta) {
        DebugStats.b2dStepTime = System.nanoTime();
        if (dataPrepared) b2dWorld.step(delta, B2DWorld.VEL_ITERATIONS,
                        B2DWorld.POS_ITERATIONS);
        DebugStats.b2dStepTime = System.nanoTime() - DebugStats.b2dStepTime;

        DebugStats.processLifecycleTime = System.nanoTime();
        processLifecycle(delta);
        DebugStats.processLifecycleTime = System.nanoTime()
                        - DebugStats.processLifecycleTime;
        updateView(delta);
    }

    @Override
    public void preDraw() {
        if (!isInitialized()) return;

        //        updateView(Gdx.graphics.getDeltaTime());
        //        game.getScreen().getDebugStage().getGLProfiler().reset();
        game.getScreen().getRenderer().renderScenegraph(b2dCam);
        //        game.getScreen().getDebugStage().getDebugUI().updateGraphics();
    }

    @Override
    public void postDraw() {
        //        game.getScreen().getDebugStage().getDebugUI().updateGraphics();
    }

    @Override
    public boolean setZoom(float zoom) {
        boolean zoomed = super.setZoom(zoom);

        if (zoomed) {
            screenBound.setSize(cam.viewportWidth * zoom,
                            cam.viewportHeight * zoom);
            preLoadBound.setSize(cam.viewportWidth * zoom * preloadBoundMult,
                            cam.viewportHeight * zoom * preloadBoundMult);
            b2dCam.zoom = zoom;
        }

        return zoomed;
    }

    public void setCamFocusObject(EJXEntity camTargetEntity, boolean usePlayerY) {
        camEntity = camTargetEntity;
        cam.setPlayerEntity(playerEntity);
        cam.setUsePlayerY(usePlayerY);
        cam.setCamTarget(camTargetEntity);
    }

    protected void loadProperties(MapProperties props) {
        tileWidth = props.get("tileWidth", 0, Integer.class);
        tileHeight = props.get("tileHeight", 0, Integer.class);

        mapWidth = props.get("width", 0, Integer.class);
        mapHeight = props.get("height", 0, Integer.class);

        worldWidth = mapWidth * tileWidth;
        worldHeight = mapHeight * tileHeight;

        lvlBound.setPosition(0, 0);
        lvlBound.setSize(worldWidth, worldHeight);

        depthEnabled = props.get(DEPTH_ENABLED, true, Boolean.class);

        if (depthEnabled) game.getScreen().getRenderer().setParallax(
                        props.get(PARALLAX_VALUE, .1f, Float.class));
        else game.getScreen().getRenderer().setParallax(0);

        lightsEnabled = props.get(LIGHT_ENABLED, false, Boolean.class);

        winProgress = props.get(WIN_VALUE, 1f, Float.class);
    }

    public void setLvlData(TiledMap map) {
        this.map = map;
        resources.clear();
        initResources();

        long loadingTime = System.currentTimeMillis();
        // load map properties
        MapProperties props = map.getProperties();

        setName(props.get("name", String.class));

        VJXLogger.logIncr(
                        LogCategory.LEVEL | LogCategory.FLOW | LogCategory.LOAD
                        | LogCategory.INIT,
                        "Level '" + getName() + "' preparing data.");

        // create physics world
        b2dWorld = new World(B2DWorld.WORLD_GRAVITY, true);
        b2dWorld.setContactListener(game.collisionControl);
        game.getScreen().getDebugStage().setWorld(b2dWorld);

        loadProperties(map.getProperties());

        Waypoint wp = new Waypoint("level_start");
        wp.setCenter(200, 0);
        Waypoints.addPoint(wp);

        wp = new Waypoint("level_end");
        wp.setCenter(worldWidth + 1000, 0);
        Waypoints.addPoint(wp);

        wp = new Waypoint("clouds_wp");
        wp.setCenter(-1500, 0);
        Waypoints.addPoint(wp);

        Collection<EntityDef> defs = game.factory.getDefs().values();
        for (EntityDef def : defs) {
            VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                            "Level add dependencies for " + def.getDefName()
                            + ": " + def.getSource());
            def.getDependencies(resources);
        }

        resources.put("global_character", new AssetDescriptor<>(
                        "spriteSheets/global/global_character.atlas",
                        TextureAtlas.class));

        resources.put("global_decoration", new AssetDescriptor<>(
                        "spriteSheets/global/global_decoration.atlas",
                        TextureAtlas.class));

        resources.put("horizon_decoration", new AssetDescriptor<>(
                        "spriteSheets/horizon/horizon_decoration.atlas",
                        TextureAtlas.class));
        resources.put("foreground_decoration", new AssetDescriptor<>(
                        "spriteSheets/foreground/foreground_decoration.atlas",
                        TextureAtlas.class));
        //        resources.put("global_item",
        //                        new AssetDescriptor<>(
        //                                        "spriteSheets/global/global_item.atlas",
        //                                        TextureAtlas.class));
        resources.put("global_vehicle", new AssetDescriptor<>(
                        "spriteSheets/global/global_vehicle.atlas",
                        TextureAtlas.class));
        resources.put("global_portraits", new AssetDescriptor<>(
                        "spriteSheets/global/global_portraits.atlas",
                        TextureAtlas.class));

        dataPrepared = true;
        loadingTime = System.currentTimeMillis() - loadingTime;

        VJXLogger.logDecr(
                        LogCategory.LEVEL | LogCategory.FLOW | LogCategory.LOAD
                        | LogCategory.INIT,
                        "Level '" + getName() + "' data prepared. Took: "
                                        + loadingTime + "ms.");
    }

    public void incrLvlProgress() {
        currentProgress++;
        checkProgress();
    }

    public void decrLvlProgress() {
        currentProgress--;
        checkProgress();
    }

    public void setProgress(float percent) {
        currentProgress = maxProgress * percent;
        checkProgress();
    }

    public void setProgress(int absolute) {
        currentProgress = absolute;
        checkProgress();
    }

    public float getLvlProgress() {
        return currentProgress / maxProgress;
    }

    public boolean isProgrGreater(float value) {
        return value > getLvlProgress();
    }

    public boolean isProgrSmaller(float value) {
        return value < getLvlProgress();
    }

    public void setFinished() {
        setProgress(winProgress);
    }

    public void checkProgress() {
        if (isProgressFinished() && !finished) {
            finished = true;
            progressComplete();
        }
    }

    public void resetProgress() {
        finished = false;
        currentProgress = 0;
        maxProgress = 0;
    }

    public void addEntity(EJXEntity entity) {
        if (entity.getLayer() != null) addEntity(entity, entity.getLayer());
        else addEntity(entity, getScene().findGroup("world"));
    }

    public void addEntity(EJXEntity entity, EJXGroup layer) {
        addEntity(entity, null, layer, false);
    }

    public void addEntity(EJXEntity entity, EJXEntity parent) {
        addEntity(entity, parent, parent.getLayer(), false);
    }

    public void addEntity(EJXEntity entity, EJXEntity parent, boolean pre) {
        addEntity(entity, parent, parent.getLayer(), pre);
    }

    public void addEntity(EJXEntity entity, EJXEntity parent, EJXGroup node,
                    boolean behindParent) {
        entity.setLayer(node);
        entity.setParent(parent);
        entity.setBehindParent(behindParent);

        if (entities.containsKey(entity.getID())) {
            EJXEntity e = entities.get(entity.getID());
            if (e != entity) e.setLifecycle(Lifecycle.DESPAWN);
            else addEntityActor(entity);
        } else {
            if (entity.getLifecycle() != Lifecycle.CREATED)
                addEntityActor(entity);
        }

        entities.put(entity.getID(), entity);
    }

    public void removeEntity(EJXEntity entity) {
        entities.remove(entity.getID());
    }

    public EJXEntity getEntityByID(long id) {
        if (id < 0) return null;
        EJXEntity result = entities.get(id);

        if (result != null && !result.isLifecycle(Lifecycle.DESPAWN))
            return result;

        return null;
    }

    public EJXEntity getEntityByName(String name) {
        return getEntityByName(name, false);
    }

    public EJXEntity getEntityByName(String name, boolean contains) {
        if (name == null || name.isEmpty()) return null;
        for (EJXEntity e : entities.values()) {
            if (e.isLifecycle(Lifecycle.DESPAWN)) continue;
            if (contains) {
                if (e.getName().contains(name)) return e;
            } else
                if (e.getName().equals(name)) return e;
        }
        if (name.equals(EJXCamera.cam_target)) return camEntity;
        return null;
    }

    public Array<EJXEntity> getEntitiesByName(String name,
                    Array<EJXEntity> targetArray) {
        for (EJXEntity e : entities.values()) {
            if (e.isLifecycle(Lifecycle.DESPAWN)) continue;
            if (e.getName().equals(name)) targetArray.add(e);
        }
        return targetArray;
    }

    public Array<EJXEntity> getEntitiesByContainDefName(String defName,
                    Array<EJXEntity> targetArray) {
        for (EJXEntity e : entities.values()) {
            if (e.isLifecycle(Lifecycle.DESPAWN)) continue;
            if (e.getDef().getDefName().contains(defName))
                targetArray.add(e);
        }
        return targetArray;
    }

    public Array<EJXEntity> getEntitiesByDefName(String defName,
                    Array<EJXEntity> targetArray) {
        for (EJXEntity e : entities.values()) {
            if (e.isLifecycle(Lifecycle.DESPAWN)) continue;
            if (e.getDef().getDefName().equals(defName)) targetArray.add(e);
        }
        return targetArray;
    }

    public Array<EJXEntity> getEntitiesByCategory(Category category,
                    Array<EJXEntity> targetArray) {
        for (EJXEntity e : entities.values()) {
            if (e.isLifecycle(Lifecycle.DESPAWN)) continue;
            if (e.getDef().getCategory() == category) targetArray.add(e);
        }
        return targetArray;
    }

    public Array<EJXEntity> getEntitiesByLogic(EJXLogicType logic,
                    Array<EJXEntity> targetArray) {
        for (EJXEntity e : entities.values()) {
            if (e.isLifecycle(Lifecycle.DESPAWN)) continue;
            if (e.hasLogic(logic))
                targetArray.add(e);
        }
        return targetArray;
    }

    public int getEntityCountByLogic(VJXLogicType logic) {
        int count = 0;
        for (EJXEntity e : entities.values()) {
            if (e.isLifecycle(Lifecycle.DESPAWN))
                continue;
            if (e.hasLogic(logic))
                count++;
        }
        return count;
    }

    public TreeMap<Long, EJXEntity> getEntities() {
        return entities;
    }

    public PlayerEntity getPlayerEntity() {
        return playerEntity;
    }

    public EJXScenegraph getScene() {
        return scene;
    }

    public TiledMap getMap() {
        return map;
    }

    /**
     * Returns the width of a tile on this map in pixels.
     *
     * @return The width of a tile.
     */
    public int getTileWidth() {
        return tileWidth;
    }

    /**
     * Returns the height of a tile on this map in pixels.
     *
     * @return The height of a tile.
     */
    public int getTileHeight() {
        return tileHeight;
    }

    /**
     * Returns the width of the map as count of tiles in x direction.
     *
     * @return The count of tiles in x direction.
     */
    public int getMapWidth() {
        return mapWidth;
    }

    /**
     * Returns the height of the map as count of tiles in y direction.
     *
     * @return The count of tiles in y direction.
     */
    public int getMapHeight() {
        return mapHeight;
    }

    /**
     * Returns the width of the map in pixels in x direction.
     *
     * @return The width of the map in pixels.
     */
    public float getWorldWidth() {
        return worldWidth;
    }

    /**
     * Returns the height of the map in pixels in y direction.
     *
     * @return The height of the map in pixels.
     */
    public float getWorldHeight() {
        return worldHeight;
    }

    public OrthographicCamera getB2DCam() {
        return b2dCam;
    }

    public World getB2DWorld() {
        return b2dWorld;
    }

    public void setLevelName(String name) {
        levelName = name;
    }

    public String getLevelName() {
        return levelName != null ? levelName : getName();
    }

    protected void clearWorld(boolean destroyScene) {
        VJXLogger.logIncr(LogCategory.LEVEL | LogCategory.FLOW | LogCategory.INIT,
                        "Level '" + getName() + "' clearing world.");
        Collection<EJXEntity> es = entities.values();
        for (EJXEntity e : es) {
            if (e instanceof CollisionObject)
                despawnCollisionObject((CollisionObject) e);
            if (e.isSpawned()) {
                e.despawn(false, false);
                //                game.sfx.unprepare(e);
            }
        }
        entities.clear();
        VJXLogger.logDecr(LogCategory.LEVEL | LogCategory.FLOW | LogCategory.INIT,
                        "Level '" + getName() + "' world cleared.");
        scene.clear(destroyScene);
    }

    protected void createObjects() {
        EntityDef def;
        EJXEntity e;
        EJXPlayer p = game.getPlayer();
        Vector2 pos = new Vector2();
        MapProperties objProps;

        Array<MapLayer> objLayers = map.getLayers().getByType(MapLayer.class);

        String worldLayerName;
        WorldLayer worldNode;
        EJXGroup layerNode;
        boolean layerUsePhysics = false;

        boolean drawOrderY = false;
        for (MapLayer layer : objLayers) {
            if (layer.getName().contains("depth")) continue;

            worldLayerName = layer.getProperties().get("world_layer", "skybox", String.class);
            drawOrderY = layer.getProperties().get("drawOrderY", false, Boolean.class);

            layerUsePhysics = layer.getName().contains("world")
                            || layer.getName().equals("path")
                            || layer.getName().equals("collision");
            layerUsePhysics &= layer.getProperties().get("usePhysics",
                            layerUsePhysics, Boolean.class);

            worldNode = scene.getWorldLayer(worldLayerName);
            layerNode = worldNode.findActor(layer.getName());
            layerNode.setDrawOrderY(drawOrderY);

            for (MapObject mObj : layer.getObjects()) {
                if (mObj.getName().contains("camera")) {
                    cam.configure(mObj.getProperties());
                    cam.setMoveArea(lvlBound);
                    setZoom(cam.zoom);
                    continue;
                }
                def = null;
                objProps = mObj.getProperties();

                if (objProps.get("gid", Integer.class) == null)
                    continue;

                int id = objProps.get("gid", Integer.class);
                id = id & ~0xE0000000;

                boolean objUsePhysics = objProps.get(
                                DefinitionProperty.usePhysics.name, false,
                                Boolean.class);

                boolean visible = objProps.get("visible", true, Boolean.class);
                visible &= mObj.isVisible();

                boolean flipY = objProps.get("flipY", false, Boolean.class);

                TiledMapTile tile = map.getTileSets().getTile(id);
                def = game.factory.getEntityDef(
                                objProps.get("defID", -1, Integer.class));

                pos.set(objProps.get("x", Float.class), objProps.get("y", Float.class));
                switch (def.getCategory()) {
                    case TILED:
                        e = getEntityByName(mObj.getName().replace("_origin",
                                        VJXString.STR_EMPTY));
                        if (e == null)
                            e = game.factory.createTiledObject(layerNode, def, mObj,
                                            tile);
                        else ((TiledEntity) e).addObject(mObj, game.assets.getRegion(tile));
                        break;
                    default:
                        e = game.factory.createEntity(layerNode, p, def, mObj);
                        if (e == null) continue;
                        ((AnimatedEntity) e).setFlipY(flipY);
                }
                e.getDef().setValue(DefinitionProperty.usePhysics.name,
                                layerUsePhysics && objUsePhysics);
                e.getDef().getCustomProperties().put("visible", visible);
                e.setVisible(visible);
                addEntity(e, layerNode);
            }
        }
    }

    protected void createCollision() {
        Array<TiledMapTileLayer> layers = map.getLayers()
                        .getByType(TiledMapTileLayer.class);
        boolean usePhysics;
        for (TiledMapTileLayer layer : layers) {
            Cell c;
            usePhysics = layer.getName().contains("world")
                            || layer.getName().equals("path")
                            || layer.getName().equals("collision");
            usePhysics &= layer.getProperties().get("usePhysics", usePhysics,
                            Boolean.class);
            boolean[][] checked = new boolean[mapWidth][mapHeight];
            for (int x = 0; x < mapWidth; x++)
                for (int y = 0; y < mapHeight; y++) {
                    if (checked[x][y] || (c = layer.getCell(x, y)) == null) {
                        checked[x][y] = true;
                        continue;
                    }
                    MapProperties props = c.getTile().getProperties();

                    if (props.get("tileCase", 0, Integer.class) == 0) continue;

                    props.put(SpatialProperty.x.name, (float) x * tileWidth);
                    props.put(SpatialProperty.y.name, (float) y * tileHeight);

                    EntityDef def = game.factory.getEntityDef("collision");

                    //                    VJXLogger.log("createTerrain: " + def.getTileID());
                    //                    VJXLogger.printMapProperties(def.getProperties());
                    //                    VJXLogger.log("-------------------------------------");
                    CollisionObject te = (CollisionObject) game.factory
                                    .createEntity(def, props);
                    //                    VJXLogger.log("create tiled " + te);
                    //                    VJXLogger.log(te + " check: " + x + ", " + y);
                    //                    VJXLogger.printMapProperties(te.getDef().getProperties());
                    //                    VJXLogger.log("-------------------------------------");
                    //                    VJXLogger.log("-------------------------------------");
                    te.setBirthPlace(x * tileWidth, y * tileHeight);
                    //                    te.setPosition(x * getTileWidth(), y * getTileHeight());
                    te.setTileSize(tileWidth, tileHeight);

                    Vector2 origin = new Vector2(x, y);
                    te.setCells(checkCells2(x, y, checked, origin,
                                    new Array<LayerCell>(), layer));

                    if (!usePhysics)
                        te.getDef().setValue(DefinitionProperty.usePhysics.name,
                                        false);

                    EJXGroup node = scene.findActor(layer.getName());
                    addEntity(te, node);
                }
        }
    }

    private Array<LayerCell> checkCells2(int x, int y, boolean[][] checked,
                    Vector2 origin, Array<LayerCell> cells,
                    TiledMapTileLayer layer) {
        if (checked[x][y]) return cells;
        checked[x][y] = true;

        LayerCell c = (LayerCell) layer.getCell(x, y);
        if (c == null) return cells;

        c.setLocalXY(x - (int) origin.x, y - (int) origin.y);
        cells.add(c);
        layer.setCell(x, y, null);

        int tileCase = c.getTile().getProperties().get("tileCase", 0,
                        Integer.class);
        boolean preFlip = c.getTile().getProperties().get("preFlip", false,
                        Boolean.class);
        if (tileCase == 0) return cells;
        int cellRotation = c.getRotation();
        int[] neighborIDs = Tiles.tileNeighbors[tileCase];

        int nID0, nx, ny;
        for (int i = 0; i < neighborIDs.length; i++) {
            nID0 = neighborIDs[i];

            // rotate neighbors
            if (cellRotation != 0)
                nID0 = (4 - (nID0 - cellRotation)) % 4;

            if (preFlip && nID0 % 2 == 1 && !c.getFlipHorizontally())
                nID0 = (nID0 + 2) % 4;

            // flip neighbors
            if (nID0 % 2 == 1 && c.getFlipHorizontally()
                            || nID0 % 2 == 0 && c.getFlipVertically())
                nID0 = (nID0 + 2) % 4;

            nx = Tiles.neighborDir[nID0][0];
            ny = Tiles.neighborDir[nID0][1];

            nx += x;
            ny += y;

            if (nx >= 0 && nx < checked.length && ny >= 0
                            && ny < checked[0].length) {
                LayerCell nCell = (LayerCell) layer.getCell(nx, ny);
                if (nCell == null) continue;

                tileCase = nCell.getTile().getProperties().get("tileCase", 0,
                                Integer.class);
                if (tileCase == 0) continue;

                int[] neighborIDs2 = Tiles.tileNeighbors[tileCase];
                for (int nID1 : neighborIDs2) {

                    // rotate neighbor
                    if (nCell.getRotation() != 0)
                        nID1 = (4 - (nID1 - nCell.getRotation())) % 4;

                    boolean preFlip2 = nCell.getTile().getProperties()
                                    .get("preFlip", false, Boolean.class);
                    if (preFlip2 && nID1 % 2 == 1
                                    && !nCell.getFlipHorizontally())
                        nID1 = (nID1 + 2) % 4;

                    // flip neighbor
                    if (nID1 % 2 == 1 && nCell.getFlipHorizontally()
                                    || nID1 % 2 == 0 && nCell.getFlipVertically())
                        nID1 = (nID1 + 2) % 4;

                    if (nID0 != nID1 && (nID1 + nID0) % 2 == 0)
                        checkCells2(nx, ny, checked, origin, cells, layer);
                }
            }

        }

        return cells;
    }

    private void despawnCollisionObject(CollisionObject tEntity) {
        if (tEntity.getActor().getParent() == null) return;
        MapLayer layer;
        String layerName = tEntity.getActor().getParent().getName();
        layerName = layerName.split("_node")[0].replace("objects", "tiles");
        layer = map.getLayers().get(layerName);
        if (!(layer instanceof TiledMapTileLayer)) return;

        for (LayerCell cell : tEntity.getCells())
            ((TiledMapTileLayer) layer).setCell(cell.getOriginX(),
                            cell.getOriginY(), cell);
        tEntity.getCells().clear();
    }

    private void createPlayerAvatar() {
        EntityDef def;
        EJXPlayer p = game.getPlayer();
        if (map.getProperties().get("bike", false, Boolean.class))
            def = game.factory.getEntityDef("kawaida0_bike");
        else
            def = game.factory.getEntityDef("kawaida0");
        playerEntity = (PlayerEntity) game.factory.createEntity(def, null);
        p.setEntity(playerEntity);
    }

    private void updateView(float delta) {
        if (isActive()) {
            tmpVec.set(cam.updateFocus(delta));
        }
        tmpVec.set(cam.position.x, cam.position.y);

        tmpVec.scl(B2DWorld.WORLD_TO_B2D);
        game.getScreen().getRenderer().setLightFocus(tmpVec);

        preLoadBound.setCenter(tmpVec.x, tmpVec.y);

        b2dCam.position.set(tmpVec, 0);
        b2dCam.update();

        game.getScreen().getRenderer().updateSceneParallax();
    }

    private void processLifecycle(float delta) {
        screenBound.setCenter(cam.position.x, cam.position.y);
        preLoadBound.setCenter(cam.position.x, cam.position.y);
        onScreenObjects = 0;
        for (EJXEntity e : entities.values()) {

            switch (e.getLifecycle()) {
                case CREATED:
                    spawnEntity(e);
                    break;
                case DESPAWN:
                    if (e.getActor().getActions().size == 0) {
                        despawnEntity(e, false);
                        continue;
                    }
                    break;
                default:
            }

            isOnScreen(e);
            //            preLoad(e);

            if (e.getWorldCenter().x < -1000)
                e.setPosition(worldWidth + 1000, e.getY());

            if (e.getWorldCenter().y < -1000) e.setLifecycle(Lifecycle.DESPAWN);
            if (e.getLifespan() - e.getTimeAlive() <= 0)
                e.setLifecycle(Lifecycle.DESPAWN);
            else e.updatePosFromBody();
        }

        while (!removeIDs.isEmpty()) {
            EJXEntity e = entities.remove(removeIDs.removeLast());
            e.clearListeners();
        }

        //        game.sfx.load();
    }

    public Rectangle getScreenbound() {
        return screenBound;
    }

    private boolean isOnScreen(EJXEntity entity) {
        boolean isOnScreen = false;
        if (entity.getBound().getBoundingRectangle().overlaps(screenBound))
            isOnScreen = true;

        if (isOnScreen ^ entity.isOnScreen()) {
            entity.onScreenChanged(isOnScreen);

            //            if (!isOnScreen) entity.getActor().remove();
            //            else addEntityActor(entity);
        }
        if (isOnScreen) onScreenObjects++;
        return isOnScreen;
    }

    //    private void preLoad(VJXEntity entity) {
    //        if (entity.getBound().getBoundingRectangle().overlaps(preLoadBound)
    //                        || entity.hasLogic(VJXLogicType.AMBIENT)) {
    //            if (!entity.isMarked()) {
    //                entity.markForPreload();
    //                game.sfx.prepare(entity);
    //            }
    //        } else if (entity.isMarked()) {
    //            entity.unmark();
    //            game.sfx.unprepare(entity);
    //        }
    //    }

    public int getOnScreenObjects() {
        return onScreenObjects;
    }

    public void spawnEntity(EJXEntity entity) {
        spawnEntity(entity, true);
    }

    private void spawnEntity(EJXEntity entity, boolean checkSpawn) {
        if (entity == null) return;

        entity.setEntityListener(game.gameControl);

        if (entity.getLayer() == null)
            entity.setLayer(getScene().findGroup("world"));

        entity.spawn(b2dWorld);

        addEntityActor(entity);
        checkObjectName(entity);
        game.sfx.loadEntitySounds(entity.getDefName());

        if (checkSpawn) checkSpawn(entity, true);
    }

    public void spawnGroup(EJXEntity parent, String defName, int count,
                    float dist, float spawnAngle, float spawnDelay,
                    Waypoint[] waypoints, boolean left) {
        //        VJXLogger.log("spawnGroup:of " + count);
        if (count <= 0) return;
        EntityDef def = game.factory.getEntityDef(defName);
        float step = 0;
        if (count > 1) step = spawnAngle / (count - 1);
        //        VJXLogger.log("step:" + step);
        //        VJXLogger.log("dist:" + dist);
        boolean useWaypoint = waypoints != null && waypoints.length == count;
        //        pos.add(128, 0);
        Vector2 pos = parent.getWorldCenter();
        //        VJXLogger.log("spawner.getRotation():" + spawner.getRotation());
        //        VJXLogger.log("spawner.isFlipX():" + spawner.isFlipX());
        //        VJXLogger.log("spawner.isFlipY():" + spawner.isFlipY());
        for (int i = 0; i < count; i++) {
            EJXEntity e = game.factory.createEntity(def, null);
            if (useWaypoint) {
                Waypoint wp = waypoints[i];
                e.setBirthPlace(wp.getCX(), wp.getCY());
            } else {
                if (left) {
                    tmpVec.set(dist, 0);
                    float angle = tmpVec.angle() + step * i;
                    //                    VJXLogger.log("angle:" + angle);
                    tmpVec.setAngle(angle);
                } else {
                    tmpVec.set(-dist, 0);
                    float angle = tmpVec.angle() - step * i;
                    //                    VJXLogger.log("angle:" + angle);
                    tmpVec.setAngle(angle);
                }
                tmpVec.add(pos);
                tmpVec.sub(e.getScaledWidth() / 2f, e.getScaledHeight() / 2f);
                e.setBirthPlace(tmpVec);
                e.disable(spawnDelay);
                //                e.setRotation(spawner.getRotation());
                //                e.setFlipX(spawner.isFlipX());
                //                e.setFlipY(spawner.isFlipY());
                //                if (e instanceof WorldObjectImpl)
                //                    ((WorldObjectImpl) e).isBlockedTimer = 10f;
            }
            addEntity(e, parent, parent.getLayer(), false);
            spawnEntity(e, false);
        }
    }

    protected void spawnAllNow() {
        for (EJXEntity entity : entities.values())
            if (entity.isLifecycle(Lifecycle.CREATED)) spawnEntity(entity);

        if (camEntity == null)
            setCamFocusObject(game.getPlayer().getEntity(), true);
    }

    protected void despawnEntity(EJXEntity entity, boolean now) {
        if (entity instanceof CollisionObject)
            despawnCollisionObject((CollisionObject) entity);

        checkSpawn(entity, false);

        //        game.sfx.unprepare(entity);
        entity.despawn();

        if (now) {
            entities.remove(entity.getID());
            entity.clearListeners();
        } else removeIDs.add(entity.getID());
    }

    private void checkObjectName(EJXEntity e) {
        if (!e.getName().isEmpty()) {
            switch (e.getName()) {
                case EJXCamera.cam_target:
                    setCamFocusObject(e, true);
                    break;
            }
        }
    }

    private void addEntityActor(EJXEntity entity) {
        EJXEntity parent = entity.getParent();
        //        VJXLogger.log("addEntityActor:" + entity + " parent " + parent);
        boolean zNeg = entity.isBehindParent();
        EJXGroup node = entity.getLayer();

        entity.getActor().remove();

        SnapshotArray<Actor> actors = node.getChildren();
        if (node.isDrawOrderY()) {
            for (Actor a : actors)
                if (a.getY() >= entity.getBirthPlace().y) continue;
                else {
                    node.addActorBefore(a, entity.getActor());
                    return;
                }
            node.addActor(entity.getActor());
        }

        if (parent != null) {
            if (zNeg) node.addActorBefore(parent.getActor(), entity.getActor());
            else node.addActorAfter(parent.getActor(), entity.getActor());
        } else if (zNeg) node.addActorAt(0, entity.getActor());
        else node.addActor(entity.getActor());
    }

    @Override
    public void dispose() {
        VJXLogger.logIncr(LogCategory.LEVEL | LogCategory.FLOW
                        | LogCategory.INIT | LogCategory.LOAD,
                        "Level '" + getName() + "' disposing.");
        super.dispose();

        clearWorld(true);
        entities.clear();
        removeIDs.clear();

        if (map != null)
            map.dispose();

        if (b2dWorld != null) b2dWorld.dispose();

        game.factory.clearAnims();
        game.sfx.unloadEntitySounds();

        game.assets.unloadResource(MenuStage.img_loading_bg_lvl);

        VJXLogger.logDecr(LogCategory.LEVEL | LogCategory.FLOW
                        | LogCategory.INIT | LogCategory.LOAD,
                        "Level '" + getName() + "' disposed.");
    }
}