package de.venjinx.ejx.stages;

import java.util.ArrayDeque;
import java.util.HashMap;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.ejx.util.VJXMapLoader;
import de.venjinx.vjx.ui.EJXMenuTable;
import de.venjinx.vjx.ui.LoadingUI;

public class LoadingStage extends EJXStage {

    private ArrayDeque<EJXStage> stagesToLoad;
    private ArrayDeque<EJXStage> stagesToUnload;
    private ArrayDeque<EJXStage> loadedStages;

    public VJXMapLoader mapLoader;

    private long loadingTime = 0;

    private boolean loading = false;
    private boolean unloading = false;

    private Table loadingUITable;
    private LoadingUI loadingUI;

    private Runnable onFinished;

    public LoadingStage(EJXGame game) {
        super(game);
        setName("vjx_loading_stage");

        stagesToLoad = new ArrayDeque<>();
        stagesToUnload = new ArrayDeque<>();
        loadedStages = new ArrayDeque<>();

        mapLoader = new VJXMapLoader(game.sfx);

        getRoot().getColor().a = 1;
    }

    @Override
    protected void userInit() {
    }

    public void loadRessource(String name,
                    AssetDescriptor<? extends Object> ressource) {
        loadRessource(name, ressource, null);
    }

    private void loadRessource(String name,
                    AssetDescriptor<? extends Object> ressource,
                    Runnable onFinished) {
        this.onFinished = onFinished;
        initLoad();
        game.assets.addResource(name, ressource);
    }

    public void unloadRessource(String name) {
        game.assets.unloadResource(name);
    }

    public void loadGlobalDefs() {
        mapLoader.loadGlobalDefs();
    }

    public void loadMap(final String path, final boolean internal) {
        if (loadingUI != null) loadingUI.startLoading(true);

        addAction(Actions.sequence(Actions.delay(.5f),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                final TiledMap m = mapLoader.load(path,
                                                !internal);
                                game.getScreen().getRenderer().setMap(m);

                                game.factory.setDefs(mapLoader.getDefs(),
                                                LevelUtil.getAnims());
                                game.getScreen().getLevel().setLvlData(m);
                            }
                        })));
    }

    public void loadInterface(final Skin skin) {
        TiledMap tz = mapLoader.load("data/ui/map_tz.tmx", false);
        TiledMap st = mapLoader.load("data/ui/map_st.tmx", false);
        TiledMap dar = mapLoader.load("data/ui/map_dar.tmx", false);
        TiledMap shop = mapLoader.load("data/ui/map_shop.tmx", false);
        TiledMap shopPrime = mapLoader.load("data/ui/map_shop_prime.tmx", false);
        TiledMap shopSkins = mapLoader.load("data/ui/map_shop_skins.tmx", false);

        HashMap<String, TiledMap> menus = new HashMap<>();
        menus.put("map_tz", tz);
        menus.put("map_st", st);
        menus.put("map_dar", dar);
        menus.put("shop", shop);
        menus.put("shop_prime", shopPrime);
        menus.put("shop_skins", shopSkins);
        game.getScreen().getMenu().setUserInterface(menus, skin);
    }

    public void loadStage(EJXStage stage) {
        game.getScreen().getLevel().getViewport().apply(true);

        initLoad();
        VJXLogger.logIncr(LogCategory.LOAD, "Loader " + getProgress() + "% ("
                        + game.assets.getLoadedAssets() + "/"
                        + (game.assets.getLoadedAssets()
                                        + game.assets.getQueuedAssets())
                        + ") start loading " + stage.getName());

        for (String s : stage.getResources().keySet()) {
            game.assets.addResource(s, stage.getResources().get(s));
        }

        stagesToLoad.add(stage);
        VJXLogger.log(LogCategory.LOAD, "Added " + game.assets.getQueuedAssets()
        + " assets to loading queue.");
        VJXLogger.log(LogCategory.LOAD, "Loader start loading...");
    }

    public void unloadStage(EJXStage stage) {
        VJXLogger.logIncr(LogCategory.LOAD, "Loader " + getProgress() + "% ("
                        + game.assets.getLoadedAssets() + "/"
                        + (game.assets.getLoadedAssets()
                                        + game.assets.getQueuedAssets())
                        + ") start unloading " + stage.getName());
        if (!unloading) {
            loadingTime = System.currentTimeMillis();
        }

        unloading = true;
        stage.dispose();
        VJXLogger.logDecr(LogCategory.LOAD, "Loader " + getProgress() + "% ("
                        + game.assets.getLoadedAssets() + "/"
                        + (game.assets.getLoadedAssets()
                                        + game.assets.getQueuedAssets())
                        + ") finished unloading " + stage.getName());
    }

    public boolean isLoading() {
        return loading || unloading;
    }

    public boolean hasNewData() {
        return !loadedStages.isEmpty() && !loading;
    }

    public float getProgress() {
        int progress = (int) (game.assets.getProgress() * 10000);

        return progress / 100f;
    }

    public void setBackground(Drawable background) {
        if (loadingUITable == null) {
            loadingUITable = new Table();
            loadingUITable.setFillParent(true);
            addActor(loadingUITable);
        }
        loadingUITable.setBackground(background);
    }

    public void setLoadingUI(EJXMenuTable loadingUI) {
        if (loadingUITable != null) {
            loadingUITable.remove();
            loadingUI.setBackground(loadingUITable.getBackground());
        }
        loadingUITable = loadingUI;
        if (loadingUITable instanceof LoadingUI)
            this.loadingUI = (LoadingUI) loadingUI;
    }

    @Override
    protected void preAct(float delta) {
        if (loading || unloading) {
            game.assets.update(100);

            if (loadingUI != null)
                loadingUI.updateLoading(delta);
            VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                            "Loading " + getProgress() + "% ("
                                            + game.assets.getLoadedAssets() + "/"
                                            + (game.assets.getLoadedAssets()
                                                            + game.assets.getQueuedAssets())
                                            + ") done. "
                                            + game.assets.getQueuedAssets()
                                            + " assets left.");
        }

        if (game.assets.getProgress() == 1 && loading || unloading) {
            VJXLogger.logDecr(LogCategory.LOAD, "Loader " + getProgress()
            + "% (" + game.assets.getLoadedAssets() + "/"
            + (game.assets.getLoadedAssets()
                            + game.assets.getQueuedAssets())
            + ") finished loading. Took "
            + (System.currentTimeMillis() - loadingTime)
            + "ms.");

            for (EJXStage s : stagesToLoad) {
                if (loadingUI != null) loadingUI.updateLoading(delta);
                s.init();
            }

            stagesToLoad.clear();
            stagesToUnload.clear();

            loading = unloading = false;

            if (onFinished != null) {
                onFinished.run();
                onFinished = null;
            }
            loadingUI.stopLoading();
        }
    }

    @Override
    protected void postAct(float delta) {
    }

    @Override
    protected void preDraw() {
    }

    @Override
    protected void postDraw() {
    }

    private void initLoad() {
        if (!loading) {
            loadingTime = System.currentTimeMillis();
        }
        loading = true;
    }
}