package de.venjinx.ejx.stages;

import java.util.Iterator;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.profiling.GLProfiler;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;

import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.InputControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.save.GameSettings;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.debug.DebugUI;

public class DebugStage extends EJXStage {

    public static class DebugStats {
        public static long screenUpdateTime = 0;
        public static long screenRenderTime = 0;

        public static long levelUpdateTime = 0;
        public static long levelRenderTime = 0;

        public static long objectsUpdateTime = 0;
        public static long objectsRenderTime = 0;

        public static long hudUpdateTime = 0;
        public static long hudRenderTime = 0;

        public static long b2dStepTime = 0;
        public static long processLifecycleTime = 0;
    }

    private Preferences settings;

    private InputControl input;
    private LevelStage level;
    private World world;

    private Batch batch;
    private ShapeRenderer shapeRenderer;
    private Box2DDebugRenderer dbgRenderer;
    private GLProfiler glProfiler;

    public static BitmapFont dbgFont;

    private boolean drawMouse = false;
    private boolean drawGrid = false;
    private boolean debugB2D = false;

    private boolean showDebugView = false;
    private boolean showUI = true;
    private boolean pauseLevel = false;
    private Vector2 mouseScr = new Vector2();
    private Vector2 mouseUI = new Vector2();
    private Vector2 mouseWorld = new Vector2();
    private Vector2 tmpVec = new Vector2();
    private float mouseX, mouseY;

    private EJXEntity selectedEntity = null;
    private Actor selectedActor = null;

    private Skin skin;
    private DebugUI ui;

    private OrthographicCamera cam;

    private FitViewport view;

    public DebugStage(EJXGame game) {
        super(game);
        String name = GameSettings.subDir + "debug";
        settings = Gdx.app.getPreferences(name + VJXString.FILE_pref);

        cam = new OrthographicCamera();
        cam.position.set(720, 405, 0);
        view = new FitViewport(1440, 810, cam);
        view.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        view.apply();

        setViewport(view);
        setName("VJX_debug_stage");

        batch = game.batch;
        dbgFont = new BitmapFont();
        dbgFont.getData().setScale(1f);
        dbgFont.setColor(Color.WHITE);
        shapeRenderer = game.shapeRenderer;
        dbgRenderer = new Box2DDebugRenderer();
        String path = "data/ui/skins/skin_libgdx/";
        TextureAtlas ta = new TextureAtlas(
                        Gdx.files.internal(path + "skin_libgdx.atlas"),
                        Gdx.files.internal("images/"));
        skin = new Skin(Gdx.files.internal(path + "skin_libgdx.json"), ta);
        glProfiler = new GLProfiler(Gdx.graphics);

        ui = new DebugUI(game, this, skin);
    }

    public Skin getSkin() {
        return skin;
    }

    public GLProfiler getGLProfiler() {
        return glProfiler;
    }

    @Override
    protected void userInit() {
    }

    @Override
    public void preAct(float delta) {
    }

    @Override
    public void postAct(float delta) {
        if (level != null && level.isInitialized()) {
            if (level.isActive())
                level.setActive(!pauseLevel);

            if (input.isKeyPressed(Keys.RIGHT) || input.isKeyDown(Keys.SHIFT_LEFT)
                            && input.isKeyDown(Keys.RIGHT))
                level.setActive(true);

            ui.updateEntityProps();
        }
    }

    @Override
    public void preDraw() {
        if (!showDebugView || level == null) return;

        if (drawGrid) drawGrid();

        if (debugB2D && level.isInitialized()) {
            dbgRenderer.render(world, level.getB2DCam().combined);

            batch.setProjectionMatrix(level.getCamera().combined);
            batch.begin();
            game.getPlayer().getEntity().drawBatchDebug(batch, dbgFont);
            if (selectedActor != null) {
                EJXEntity e = level.getEntityByName(selectedActor.getName());
                if (e != null)
                    e.drawBatchDebug(batch, dbgFont);
            }
            batch.end();
        }
    }

    @Override
    public void postDraw() {
        if (!showDebugView) return;
        if (drawMouse) {
            // mouse position
            if (mouseUI.x < game.getDisplayView().getWorldWidth() / 2f)
                mouseX = mouseUI.x + 10;
            else mouseX = mouseUI.x - 200;

            if (mouseUI.y < game.getDisplayView().getWorldHeight() / 2f)
                mouseY = mouseUI.y;
            else mouseY = mouseUI.y - 100;

            batch.setProjectionMatrix(game.getDisplayCamera().combined);
            batch.begin();
            dbgFont.draw(batch, "screen: " + mouseScr, mouseX, mouseY + 75);
            dbgFont.draw(batch, "ui    : " + mouseUI, mouseX, mouseY + 50);
            dbgFont.draw(batch, "world : " + mouseWorld, mouseX, mouseY + 25);
            batch.end();
        }
    }

    @Override
    public boolean keyDown(int keyCode) {
        boolean handled = super.keyDown(keyCode);
        if (handled) return true;

        if (keyCode == Keys.F) {
            toggleDebugView();
            return true;
        }

        if (keyCode == Keys.U) {
            showUI = !showUI;
            game.getScreen().getMenu().getUI().toggleHUD(showUI);
            return true;
        }

        if (keyCode == Keys.Y) {
            level.setProgress(level.getLvlProgress() + .1f);
            return true;
        }

        if (keyCode == Keys.O) {
            level.win();
            return true;
        }

        if (keyCode == Keys.P) {
            pauseLevel = !pauseLevel;
            level.setActive(!pauseLevel);
            return true;
        }

        if (!input.isKeyDown(Keys.SHIFT_LEFT) && keyCode == Keys.T) {
            System.out.println("---------------------------------------------");
            return true;
        }

        if (input.isKeyDown(Keys.SHIFT_LEFT) && keyCode == Keys.T) {
            System.out.println("Level world root: ");
            System.out.println(level.getRoot());
            System.out.println("---------------------------------------------");
            System.out.println(game.getScreen().getMenu().getRoot());
            System.out.println("---------------------------------------------");
            return true;
        }

        if (ui.getCustomDebug() instanceof InputProcessor) {
            if (((InputProcessor) ui.getCustomDebug()).keyDown(keyCode))
                return true;
        }

        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        boolean handled = super.touchDown(screenX, screenY, pointer, button);
        if (!handled) {
            Vector2 worldPos = new Vector2(screenX, screenY);
            if (!showDebugView) return false;
            if (level == null)
                return super.touchDown(screenX, screenY, pointer, button);

            level.getViewport().unproject(worldPos);

            selectedActor = level.getScene().hit(worldPos.x, worldPos.y, false);

            if (selectedActor != null) {
                selectedEntity = level
                                .getEntityByName(selectedActor.getName());

                if (selectedEntity != null) ui.setSelected(selectedEntity, true);
            }

            unfocusAll();
        }
        return handled;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (!drawMouse) return super.mouseMoved(screenX, screenY);

        boolean handled = super.mouseMoved(screenX, screenY);
        mouseScr.set(screenX, Gdx.graphics.getHeight() - screenY);

        tmpVec.set(screenX, screenY);

        game.getDisplayView().unproject(tmpVec);
        mouseUI.set(tmpVec.x, tmpVec.y);

        if (level != null) {
            mouseWorld.set(screenX, screenY);
            level.getViewport().unproject(mouseWorld);
        }

        return handled;
    }

    public void setDrawMouse(boolean drawMouse) {
        this.drawMouse = drawMouse;
    }

    public void showDebug(boolean show) {
        showDebugView = show;
        setRunBackground(!showDebugView, 0f);
        ui.show(showDebugView);
    }

    public void toggleDebugView() {
        showDebugView = !showDebugView;
        setRunBackground(!showDebugView, 0f);
        ui.show(showDebugView);
    }

    public boolean debugViewActive() {
        return showDebugView;
    }

    public void setDebug(boolean dbgLGDX, boolean dbgB2D, boolean drawGrid) {
        game.setDebug(dbgLGDX);
        debugB2D = dbgB2D;
        this.drawGrid = drawGrid;
    }

    public void setDebugB2D(boolean dbgB2D) {
        debugB2D = dbgB2D;
    }

    public void setLevel(LevelStage level) {
        this.level = level;
        world = level.getB2DWorld();
        ui.setLevel(level);
    }

    public void setWorld(World b2dWorld) {
        world = level.getB2DWorld();
    }

    public void setInputControl(InputControl input) {
        this.input = input;
    }

    public DebugUI getDebugUI() {
        return ui;
    }

    private void drawGrid() {
        if (!level.isActive()) return;
        shapeRenderer.begin();
        shapeRenderer.setColor(Color.WHITE);

        EJXCamera cam = (EJXCamera) level.getCamera();
        int tileW = level.getTileWidth();
        int tileH = level.getTileHeight();
        float camX = (int) cam.position.x;
        float camY = (int) cam.position.y;
        float screenW = level.getViewport().getScreenWidth() / 2 * cam.zoom;
        float screenH = level.getViewport().getScreenHeight() / 2 * cam.zoom;

        int screenXMin = (int) (camX - screenW) / tileW;
        screenXMin *= tileW;

        int screenXMax = (int) (camX + screenW) / tileW;
        screenXMax++;
        screenXMax *= tileW;

        int screenYMin = (int) (camY - screenH) / tileH;
        screenYMin *= tileH;

        int screenYMax = (int) (camY + screenH) / tileH;
        screenYMax++;
        screenYMax *= tileH;

        for (int x = screenXMin; x <= screenXMax; x += tileW) {
            shapeRenderer.line(x, screenYMin, x, screenYMax);
        }

        for (int y = screenYMin; y <= screenYMax; y += tileH) {
            shapeRenderer.line(screenXMin, y, screenXMax, y);
        }

        shapeRenderer.end();
    }

    public Preferences getSettings() {
        return settings;
    }

    private void save() {
        if (Gdx.app.getType() == ApplicationType.Desktop) {
            ui.save();
            settings.putBoolean("showDebug", showDebugView);
            settings.flush();
        }
    }

    public void load() {
        showDebugView = settings.getBoolean("showDebug", false);
        showDebug(showDebugView);
    }

    @Override
    public void dispose() {
        save();
    }

    public static void printMapObject(MapObject mo) {
        String key;
        Object value;

        System.out.println(mo.getName());

        MapProperties props = mo.getProperties();
        Iterator<String> iter = props.getKeys();
        while (iter.hasNext()) {
            key = iter.next();
            value = props.get(key);
            System.out.println(key + ": " + value + "[" + value.getClass() + "]");
        }
        System.out.println();
    }

    public static void printMapTile(TiledMapTile t) {
        String key;
        Object value;

        System.out.println("MapTile " + t.getId());
        System.out.println("Offset: " + t.getOffsetX() + ", " + t.getOffsetY());

        MapProperties props = t.getProperties();
        Iterator<String> iter = props.getKeys();
        while (iter.hasNext()) {
            key = iter.next();
            value = props.get(key);
            System.out.println(
                            key + ": " + value + "[" + value.getClass() + "]");
        }
        System.out.println();
    }

    public static void printAtlasRegionInfo(AtlasRegion ar) {
        System.out.println("AtlasRegion " + ar.name + " - " + ar.index);
        System.out.println("Position: " + ar.getRegionX() + ", " + ar.getRegionY());
        System.out.println("Offset  : " + ar.offsetX + ", " + ar.offsetY);
        System.out.println("Size    : " + ar.getRegionWidth() + ", " + ar.getRegionHeight());
        System.out.println("OrigSize: " + ar.originalWidth + ", " + ar.originalHeight);
        System.out.println("Rotate  : " + ar.rotate);
    }
}