package de.venjinx.ejx.entity.component;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;

import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EntityCallbacks;
import de.venjinx.ejx.entity.logics.EJXLogics.EJXLogicType;
import de.venjinx.ejx.tiled.entity.EJXAbstractObject;
import de.venjinx.ejx.tiled.object.EJXMapObject;
import de.venjinx.ejx.util.EJXTypes;
import de.venjinx.ejx.util.EJXTypes.EJXProperty;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.VJXProperties;
import de.venjinx.ejx.util.VJXData;

public abstract class EJXComponent implements EntityCallbacks {

    protected Vector2 tmpVec;
    protected EJXAbstractObject owner;
    protected EJXLogicType logicType;
    protected EJXPropertyDefinition[] userProperties;
    protected VJXProperties vjxProps;
    protected MapProperties mapProperties;
    protected GameControl gameControl;

    public EJXComponent(EJXLogicType logicType) {
        this.logicType = logicType;

        tmpVec = new Vector2();
        vjxProps = EJXTypes.newProps();
        userProperties = new EJXPropertyDefinition[0];
    }

    public EJXComponent(EJXLogicType logicType, EJXPropertyDefinition[] properties) {
        this.logicType = logicType;
        userProperties = properties;
        tmpVec = new Vector2();
        vjxProps = EJXTypes.newProps();
    }

    public abstract void init(EJXMapObject mapObject);

    public void update(float deltaT) {
    }

    public void drawBatchDebug(Batch batch, BitmapFont font) {
    }

    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
    }

    public EJXPropertyDefinition getPropertyDefinition(String propertyName) {
        for (EJXPropertyDefinition property : userProperties) {
            if (property.getName().equals(propertyName))
                return property;
        }
        return null;
    }

    public EJXProperty<?> getProperty(String propertyName) {
        EJXPropertyDefinition prop = getPropertyDefinition(propertyName);
        Object property = vjxProps.get(prop);
        if (property instanceof EJXProperty<?>)
            return (EJXProperty<?>) property;
        return null;
    }

    public <T> T getProperty(EJXPropertyDefinition key, Class<T> clazz) {
        return vjxProps.get(key, clazz);
    }

    public void setProperty(EJXPropertyDefinition property, Object value) {
        vjxProps.get(property);
    }

    public void setOwner(EJXAbstractObject owner) {
        this.owner = owner;
        gameControl = owner.getGameControl();
    }

    public EJXAbstractObject getOwner() {
        return owner;
    }

    public EJXLogicType getType() {
        return logicType;
    }

    @Override
    public void onRespawn() {
    }

    @Override
    public void onScreenChanged(boolean onScreen) {
    }

    @Override
    public void targetInView(boolean inView) {
    }

    @Override
    public void targetInRange(boolean inRange) {
    }

    @Override
    public void targetChanged(EJXEntity targetEntity) {
    }

    @Override
    public void onTrigger(boolean triggered, VJXData thisData,
                    EJXEntity other, VJXData otherData) {
    }

    @Override
    public void onActivated(EJXEntity activator, boolean activated) {
    }

    @Override
    public void onDamage(int oldHP, int newHP) {
    }

    @Override
    public void onDie() {
    }

    @Override
    public void onDead() {
    }

    @Override
    public void onDespawn() {
    }

    @Override
    public void onEnd() {
    }

    @Override
    public void onStart() {
    }

    @Override
    public void positionReached() {
    }

    @Override
    public String toString() {
        return logicType.getName();
    }

    private static HashMap<EJXLogicType, Class<? extends EJXComponent>> components = new HashMap<>();

    public static void registerLogic(EJXLogicType type,
                    Class<? extends EJXComponent> clazz) {
        components.put(type, clazz);
    }

    public static EJXComponent newComponent(EJXLogicType name,
                    EJXAbstractObject owner) {
        if (!components.containsKey(name)) return null;
        try {
            EJXComponent component = components.get(name).newInstance();
            component.setOwner(owner);
            return component;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}