package de.venjinx.ejx.entity.component;

import java.util.HashMap;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.cmds.CommandParser;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.tiled.object.EJXMapObject;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class CommandComponent extends EJXComponent {

    private Actor actor;
    private HashMap<VJXCallback, Command[]> callBackCommands;

    public CommandComponent() {
        super(VJXLogicType.COMMAND);
        actor = new Actor();
        callBackCommands = new HashMap<>();
    }

    @Override
    public void init(EJXMapObject mapObject) {
        actor.setName(mapObject.getName() + VJXString.SEP_USCORE + getType());
        loadCallBacks(mapObject.getProperties());
    }

    public Actor getActor() {
        return actor;
    }

    public void setCallBackCommands(VJXCallback type, Command... cmds) {
        callBackCommands.put(type, cmds);
    }

    public Command[] getCallbackCommands(VJXCallback callback) {
        return callBackCommands.get(callback);
    }

    public void removeCallback(VJXCallback callback) {
        callBackCommands.remove(callback);
    }

    protected void loadCallBacks(MapProperties properties) {
        String triggerType;
        VJXTrigger trigger;

        for (VJXCallback callback : VJXCallback.values()) {
            Command[] cmds = CommandParser.getCommands(callback, properties,
                            gameControl, this);
            if (cmds != null && cmds.length > 0) {
                if (callback == VJXCallback.onCommands) {
                    triggerType = properties.get(VJXString.TRIGGER_TYPE,
                                    VJXString.TRIGGER_RANGE, String.class);
                    trigger = VJXTrigger.get(triggerType);
                    callback = VJXTrigger.getCallback(trigger, true);
                } else if (callback == VJXCallback.offCommands) {
                    triggerType = properties.get(VJXString.TRIGGER_TYPE,
                                    VJXString.TRIGGER_RANGE, String.class);
                    trigger = VJXTrigger.get(triggerType);
                    callback = VJXTrigger.getCallback(trigger, true);
                }
                setCallBackCommands(callback, cmds);
            }
        }
    }

    protected float execute(Command command, SequenceAction sequence) {
        //        VJXLogger.log(this + " exec single " + command);
        if (command == null) return 0f;
        return command.execute(sequence);
    }

    public float execSingleCmd(Command cmd) {
        return execSingleCmd(cmd, 0f);
    }

    public float execSingleCmd(Command cmd, float delay) {
        if (cmd.getExecutor() != null && cmd.getComponent() != this)
            return cmd.getExecutor().execSingleCmd(cmd, delay);

        SequenceAction sa = Actions.sequence();
        float duration = execSingleCmd(cmd, sa, delay);

        actor.addAction(sa);
        return duration;
    }

    public float execSingleCmd(Command cmd, SequenceAction sequence) {
        return execSingleCmd(cmd, sequence, 0f);
    }

    public float execSingleCmd(Command cmd, SequenceAction sequence,
                    float delay) {
        if (cmd == null) return 0f;

        VJXLogger.logIncr(LogCategory.EXEC | LogCategory.DETAIL,
                        owner.getName() + " exec single (delay " + delay + "):" + cmd);
        if (cmd.getExecutor() != null && cmd.getComponent() != this) {
            VJXLogger.logDecr(LogCategory.EXEC | LogCategory.DETAIL,
                            owner.getName() + " delegate exec " + cmd + " to "
                                            + cmd.getExecutor());
            return cmd.getExecutor().execSingleCmd(cmd, sequence, delay);
        }

        float duration = 0;
        if (delay > 0) {
            sequence.addAction(Actions.delay(delay));
            duration += delay;
        }

        duration += execute(cmd, sequence);

        VJXLogger.logDecr(LogCategory.EXEC | LogCategory.DETAIL, owner.getName()
                        + " finished exec " + cmd + ". Runs " + duration + "s");
        return duration;
    }

    public SequenceAction execMultiCmds(Command[] commands) {
        return execMultiCmds(commands, 0f);
    }

    public SequenceAction execMultiCmds(Command[] commands, float delay) {
        SequenceAction sa = Actions.sequence();
        execMultiCmds(commands, sa, delay);
        actor.addAction(sa);
        return sa;
    }

    public SequenceAction execMultiCmds(Command[] commands,
                    SequenceAction sequence) {
        return execMultiCmds(commands, sequence, 0f);
    }

    public SequenceAction execMultiCmds(Command[] commands,
                    SequenceAction sequence, float delay) {
        if (commands == null || commands.length == 0) return sequence;

        VJXLogger.logIncr(LogCategory.EXEC | LogCategory.DETAIL,
                        owner.getName() + " exec multi commands(delay " + delay + ")");

        float duration = 0f;
        if (delay > 0) {
            sequence.addAction(Actions.delay(delay));
            duration += delay;
        }

        float execTime = 0;
        for (Command cmd : commands) {
            execTime = execSingleCmd(cmd, sequence, 0);
            duration += execTime;
        }

        VJXLogger.logDecr(LogCategory.EXEC | LogCategory.DETAIL, owner.getName()
                        + " finished multi commands. Runs " + duration + "s");

        return sequence;
    }

    public SequenceAction exec(VJXCallback type) {
        return exec(type, true);
    }

    public SequenceAction exec(VJXCallback type, boolean selfExec) {
        //        VJXLogger.log("exec:" + this + ":" + type);
        SequenceAction sa = Actions.sequence();
        exec(type, sa);
        if (selfExec) actor.addAction(sa);
        else if (gameControl != null) gameControl.addActions(sa);
        return sa;
    }

    public SequenceAction exec(VJXCallback type, SequenceAction sequence) {
        VJXLogger.logIncr(LogCategory.EXEC,
                        owner.getName() + " execute commands '" + type + "'.");
        Command[] actions = callBackCommands.get(type);

        if (actions == null || actions.length == 0) {
            VJXLogger.logDecr(LogCategory.EXEC,
                            owner.getName() + " no commands for '" + type + "'.");
            return sequence;

        }

        execMultiCmds(actions, sequence);
        VJXLogger.logDecr(LogCategory.EXEC,
                        owner.getName() + " finished commands '" + type + "'.");
        return sequence;
    }
}