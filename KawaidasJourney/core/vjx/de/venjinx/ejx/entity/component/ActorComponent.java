package de.venjinx.ejx.entity.component;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.tiled.object.EJXMapObject;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class ActorComponent extends EJXComponent {

    private Actor actor;

    public ActorComponent() {
        super(VJXLogicType.ACTOR);
        actor = new Actor();
        actor.setTouchable(Touchable.disabled);
    }

    @Override
    public void init(EJXMapObject mapObject) {
        actor.setName(mapObject.getName() + VJXString.SEP_USCORE + getType());
        actor.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                CommandComponent cmdComp = (CommandComponent) owner
                                .getComponent(VJXLogicType.COMMAND);
                cmdComp.exec(VJXCallback.onClick, true);
            }
        });
        actor.setTouchable(Touchable.enabled);

        setWidth(mapObject.getWidth());
        setHeight(mapObject.getHeight());
        setX(mapObject.getX());
        setY(mapObject.getY());
    }

    public Actor getActor() {
        return actor;
    }

    public void setWidth(float width) {
        actor.setWidth(width);
    }

    public void setHeight(float height) {
        actor.setHeight(height);
    }

    public void setX(float x) {
        actor.setX(x);
    }

    public void setY(float y) {
        actor.setY(y);
    }
}