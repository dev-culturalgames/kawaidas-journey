package de.venjinx.ejx.entity;

import de.venjinx.ejx.EJXPlayer;

public class PlayerEntity extends EntityImpl {

    protected EJXPlayer player;

    public PlayerEntity(EJXPlayer p, EntityDef def) {
        super(def);
        player = p;
    }

    @Override
    public boolean hasCollPrio() {
        return true;
    }
}