package de.venjinx.ejx.entity;

import java.util.LinkedList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.controllers.CollisionControl;
import de.venjinx.ejx.entity.logics.Box2DLogic.Box2DProperty;
import de.venjinx.ejx.scenegraph.WorldLayer.LayerCell;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.Tiles;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXData;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class CollisionObject extends EntityImpl {

    private long fixID;
    private int tileWidth;
    private int tileHeight;
    private Array<LayerCell> cells;
    private LinkedList<FixtureDef> fixtureDefs;

    private Matrix3 flipTrans = new Matrix3();
    private Matrix3 rotationTrans = new Matrix3();
    private Matrix3 localOffTrans = new Matrix3();

    public CollisionObject(EntityDef def, int tileSize) {
        super(def);
        tileWidth = tileSize;
        tileHeight = tileSize;
        cells = new Array<>();
        fixtureDefs = new LinkedList<>();
        //        VJXLogger.log("CollisionObject:" + this);
    }

    public void setCells(Array<LayerCell> newCells) {
        cells.clear();
        Vector2 minV = new Vector2(), maxV = new Vector2();

        for (LayerCell cell : newCells) {
            cells.add(cell);
            minV.set(Math.min(minV.x, cell.getLocalX()),
                            Math.min(minV.y, cell.getLocalY()));
            maxV.set(Math.max(maxV.x, cell.getLocalX() + 1),
                            Math.max(maxV.y, cell.getLocalY() + 1));
        }


        int offX = -(int) minV.x;
        int offY = -(int) minV.y;

        minV.scl(tileWidth, tileHeight);
        maxV.scl(tileWidth, tileHeight);
        setSize(maxV.x - minV.x, maxV.y - minV.y);

        if (getBirthPlace().x + minV.x < getBirthPlace().x)
            setBirthPlace(getBirthPlace().x + minV.x, getBirthPlace().y);
        if (getBirthPlace().y + minV.y < getBirthPlace().y)
            setBirthPlace(getBirthPlace().x, getBirthPlace().y + minV.y);
        create(offX, offY);

        //        pointList = VJXGraphix.quickHull(pointList);

        //        Array<Vector2> all = new Array<>();

        //        for (Vector2 v : pointList)
        //            all.add(v);
        //
        //        pointList.clear();
        //        all = VJXGraphix.convex(all);
        //        for (Vector2 v : all)
        //            pointList.add(v);
        //
        //        VJXLogger.log("add points " + pointList.size());
        //        createPolys(pointList, "solid", mask);
        //        VJXLogger.log(" -------------- ");
    }

    private void create(int xOff, int yOff) {
        String sCat;
        TiledMapTile t;
        MapProperties mp;
        int tileCase = 0;

        for (LayerCell cell : cells) {
            cell.setLocalX(cell.getLocalX() + xOff);
            cell.setLocalY(cell.getLocalY() + yOff);

            t = cell.getTile();
            mp = t.getProperties();

            tileCase = mp.get("tileCase", 0, Integer.class);

            if (tileCase == 0) continue;

            int rotation = convertRotation(cell.getRotation());
            boolean preFlipped = mp.get("preFlip", false, Boolean.class);

            Vector2[] points = Tiles.getTileVertices(tileCase);
            convertPolyPoints(points, cell.getLocalX(), cell.getLocalY(),
                            rotation, cell.getFlipHorizontally() ^ preFlipped,
                            cell.getFlipVertically());

            sCat = mp.get(Box2DProperty.category.name, B2DBit.NONE.name,
                            String.class);
            //            VJXLogger.log(this + " cType " + sCat);
            //            String[] catSplit = sCat.split(VJXString.SEP_LIST);
            //            for (String cat : catSplit)
            if (points.length == 4) createPolys(points, sCat);
            else addVertices(points, cell, sCat);

            // create additional colliders
            sCat = mp.get("collision", VJXString.STR_EMPTY, String.class);
            if (!sCat.isEmpty()) {
                VJXLogger.log(LogCategory.ERROR, "removed functionality");
                //                catSplit = sCat.split(VJXString.SEP_LIST);
                //                for (String cat : catSplit) {
                //                    String[] catSplit2 = cat.split(VJXString.SEP_ATTR);
                //                    points = Tiles.getTileVertices(
                //                                    Integer.parseInt(catSplit2[0]));
                //                    convertPolyPoints(points, cell.getLocalX(),
                //                                    cell.getLocalY(), rotation,
                //                                    cell.getFlipHorizontally() || preFlipped,
                //                                    cell.getFlipVertically());
                //                    //                    pointList.clear();
                //                    //                    for (Vector2 v : points)
                //                    //                        pointList.add(v);
                //                    //
                //                    //                    //                    pointList = VJXGraphix.quickHull(pointList);
                //                    //                    createPolys(pointList, cat, mask);
                //                    if (points.length == 4)
                //                        createPolys(points, cat);
                //                    else addVertices(points, cell, cat);
                //
                //                }
            }
        }
    }

    public Array<LayerCell> getCells() {
        return cells;
    }

    public void addVertices(Vector2[] vertices, LayerCell cell,
                    String physCat) {
        // convert rotation to degree
        int rotation = convertRotation(cell.getRotation());
        addVertices(vertices, cell.getLocalX(), cell.getLocalY(),
                        cell.getFlipHorizontally(), cell.getFlipVertically(),
                        rotation, physCat);
    }

    public void addVertices(Vector2[] verts, int x, int y, boolean flipX,
                    boolean flipY, int rotation, String physCat) {

        //        createPolys(verts, x, y, flipX, flipY, rotation, physCat, b2dMask);
        createChain(verts, x, y, flipX, flipY, rotation, physCat);
        //        createEdges(verts, x, y, flipX, flipY, rotation, physCat, b2dMask);
    }

    private void createChain(Vector2[] verts, int x, int y, boolean flipX,
                    boolean flipY, int rotation, String physCat) {
        if (verts == null || verts.length == 0) return;
        //        VJXLogger.log(this + " createChain: physCat " + physCat + ", b2dMask "
        //                        + b2dMask);

        ChainShape cShape = new ChainShape();
        FixtureDef fixDef;
        String[] pCats = physCat.split(VJXString.SEP_LIST);
        short tmpCat = 0;
        for (String pCat : pCats)
            tmpCat |= B2DBit.get(pCat).id;
        //        VJXLogger.log(this + " createChain: physCat " + tmpCat + ", b2dMask "
        //                        + b2dMask);

        cShape.createLoop(verts);

        fixDef = new FixtureDef();
        fixDef.shape = cShape;
        fixDef.filter.categoryBits = tmpCat;
        fixDef.filter.maskBits = LevelUtil.solvePhysicsBits(tmpCat);

        fixDef.restitution = 0f;
        fixDef.friction = 0f;
        fixDef.isSensor = physCat.contains("climbable") ? true : false;

        fixtureDefs.add(fixDef);
    }

    private void createPolys(Vector2[] verts, String physCat) {
        if (verts == null || verts.length == 0) return;

        PolygonShape pShape = new PolygonShape();
        FixtureDef fixDef;
        String[] pCats = physCat.split(VJXString.SEP_LIST);
        short tmpCat = 0;
        for (String pCat : pCats)
            tmpCat |= B2DBit.get(pCat).id;
        //        Vector2[] ar = new Vector2[verts.length];
        //        VJXLogger.log(LoggerCategory.ALL, "---------------------------------");
        pShape.set(verts);
        //        VJXLogger.log(this + " createPolys: physCat " + tmpCat + ", b2dMask "
        //                        + b2dMask);

        //        EdgeShape e = new EdgeShape();

        //        VJXLogger.log(LoggerCategory.ALL, "createPolys:" + tileWidth + ", " + tileHeight);
        //        pShape.setAsBox(tileWidth / 100f, tileHeight / 100f);

        fixDef = new FixtureDef();
        fixDef.shape = pShape;
        fixDef.filter.categoryBits = tmpCat;
        fixDef.filter.maskBits = LevelUtil.solvePhysicsBits(tmpCat);

        fixDef.restitution = 0f;
        fixDef.friction = 0f;
        fixDef.isSensor = physCat.contains("climbable") ? true : false;

        fixtureDefs.add(fixDef);
    }

    public void setTileSize(int w, int h) {
        tileWidth = w;
        tileHeight = h;
    }

    public int getTileWidth() {
        return tileWidth;
    }

    public int getTileHeight() {
        return tileHeight;
    }

    @Override
    public void userAct(float deltaT) {
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
        Color c = batch.getColor();
        drawColor.set(c);
        drawColor.a *= actor.getColor().a * parentAlpha;

        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        if (listener.getGame().getScreen().getRenderer().useFBO()) batch.begin();
        for (LayerCell cell : cells)
            if (cell.isVisible())
                batch.draw(cell.getTile().getTextureRegion(),
                                getWorldPosition().x + cell.getLocalX() * tileWidth,
                                getWorldPosition().y + cell.getLocalY() * tileHeight,
                                tileWidth / 2f, tileHeight / 2f,
                                tileWidth, tileHeight, cell.getFlipHorizontally() ? -1 : 1,
                                                cell.getFlipVertically() ? -1 : 1,
                                                                cell.getRotation() * 90);
        if (listener.getGame().getScreen().getRenderer().useFBO()) batch.end();
        batch.setColor(c);
    }

    private Color color = new Color(Color.rgba8888(MathUtils.random(),
                    MathUtils.random(), MathUtils.random(), 1f));

    private float collisionWidth;
    private float collisionHeight;

    @Override
    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        tmpVec.set(getWorldCenter());

        // body center
        shapeRenderer.setColor(Color.WHITE);
        if (body != null)
            shapeRenderer.circle(body.getPosition().x * B2DWorld.B2D_TO_WORLD,
                            body.getPosition().y * B2DWorld.B2D_TO_WORLD, 2.5f);

        // draw entity size
        shapeRenderer.setColor(Color.GREEN);
        shapeRenderer.rect(actor.getX(), actor.getY(), 0, 0, getScaledWidth(),
                        getScaledHeight(), 1, 1, 0);

        shapeRenderer.setColor(color);
        int tileCase, rotation, nID1;
        int[] neighborIDs;
        boolean draw, preFlip;
        for (LayerCell cell : cells) {
            tileCase = cell.getCase();
            neighborIDs = Tiles.tileNeighbors[tileCase];
            preFlip = cell.getTile().getProperties().get("preFlip", false, Boolean.class);
            for (int i = 0; i < 4; i++) {
                draw = true;
                for (int j = 0; j < neighborIDs.length; j++) {
                    rotation = cell.getRotation();
                    nID1 = neighborIDs[j];

                    // rotate neighbor
                    if (rotation != 0)
                        nID1 = (4 - (nID1 - rotation)) % 4;

                    if (preFlip && nID1 % 2 == 1
                                    && !cell.getFlipHorizontally())
                        nID1 = (nID1 + 2) % 4;

                    // flip neighbor
                    if (nID1 % 2 == 1 && cell.getFlipHorizontally()
                                    || nID1 % 2 == 0 && cell.getFlipVertically())
                        nID1 = (nID1 + 2) % 4;

                    if (i == nID1)
                        draw = false;
                }

                if (draw)
                    shapeRenderer.line(
                                    Tiles.getVertex(Tiles.tileEdges[i][0])
                                    .scl(tileWidth, tileHeight)
                                    .add(getX(), getY())
                                    .add(cell.getLocalX() * tileWidth, cell
                                                    .getLocalY() * tileHeight),
                                    Tiles.getVertex(Tiles.tileEdges[i][1]).scl(
                                                    tileWidth, tileHeight)
                                    .add(getX(), getY())
                                    .add(cell.getLocalX()
                                                    * tileWidth,
                                                    cell.getLocalY() * tileHeight));
            }
        }
    }

    @Override
    protected void createBodyFixtures() {
        fixID = 0;

        Array<Fixture> fixtures = body.getFixtureList();
        for (Fixture f : fixtures)
            body.destroyFixture(f);

        Fixture fix;
        String name;
        for (FixtureDef f : fixtureDefs) {
            fix = body.createFixture(f);
            name = getDefName();
            if (CollisionControl.isCategory(f.filter.categoryBits, B2DBit.CLIMBABLE))
                name += "_climbable";
            if (CollisionControl.isCategory(f.filter.categoryBits, B2DBit.SOLID))
                name += "_solid";
            if (CollisionControl.isCategory(f.filter.categoryBits, B2DBit.PASSABLE))
                name += "_passable";
            if (CollisionControl.isCategory(f.filter.categoryBits, B2DBit.DANGER))
                name += "_danger";
            fix.setUserData(new VJXData(name + VJXString.SEP_NR + fixID, this,
                            collisionWidth, collisionHeight));
            fixID++;
        }
    }

    private int convertRotation(int rotation) {
        switch (rotation) {
            case Cell.ROTATE_90:
                rotation = 90;
                break;
            case Cell.ROTATE_180:
                rotation = 180;
                break;
            case Cell.ROTATE_270:
                rotation = 270;
                break;
            default:
                rotation = 0;
                break;
        }
        return rotation;
    }

    private void convertPolyPoints(Vector2[] verts, int x, int y, int rotation,
                    boolean flipX, boolean flipY) {
        if (verts == null || verts.length == 0) return;

        float xOff = x * tileWidth;
        float yOff = y * tileHeight;

        float xMin = Float.MAX_VALUE;
        float xMax = -Float.MAX_VALUE;

        float yMin = Float.MAX_VALUE;
        float yMax = -Float.MAX_VALUE;

        flipTrans.idt();
        rotationTrans.idt();
        localOffTrans.idt();

        rotationTrans.rotate(rotation);
        localOffTrans.translate(tileWidth / 2f, tileHeight / 2f);

        if (flipX) flipTrans.scale(-1, 1);
        if (flipY) flipTrans.scale(1, -1);

        for (int i = 0; i < verts.length; i++) {
            verts[i].scl(tileWidth, tileHeight);
            verts[i].mul(localOffTrans.inv());
            verts[i].mul(flipTrans);
            verts[i].mul(rotationTrans);
            verts[i].mul(localOffTrans.inv());
            verts[i].add(xOff, yOff).scl(.01f);
            xMin = Math.min(xMin, verts[i].x);
            xMax = Math.max(xMax, verts[i].x);
            //            VJXLogger.log(this + ":verts[i].y:" + verts[i].y);
            yMin = Math.min(yMin, verts[i].y);
            yMax = Math.max(yMax, verts[i].y);
        }
        //        VJXLogger.log(this + ":yMin:" + yMin);
        //        VJXLogger.log(this + ":yMax:" + yMax);
        collisionWidth = Math.abs(xMin - xMax);
        collisionHeight = Math.abs(yMin - yMax);
        //        VJXLogger.log(this + ":collisionHeight:" + collisionHeight);
    }

}
