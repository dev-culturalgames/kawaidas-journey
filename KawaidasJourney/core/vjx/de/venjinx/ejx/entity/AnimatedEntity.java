package de.venjinx.ejx.entity;

import java.util.HashMap;
import java.util.HashSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationState.AnimationStateListener;
import com.esotericsoftware.spine.AnimationState.TrackEntry;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.EventData;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonRenderer;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.audio.VJXSound;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.cmds.Commands;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.AnimationType;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXGraphix;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public abstract class AnimatedEntity extends EJXEntity
implements AnimationStateListener {

    public static final String ANIM_EVENT_PLANTED = "planted";
    public static final String ANIM_EVENT_THROW = "throw";
    public static final String ANIM_EVENT_PICKUP = "pickUp";
    public static final String ANIM_EVENT_REPAIR = "repaired";
    public static final String ANIM_EVENT_PLAY_SFX = "play_sfx";
    public static final String ANIM_EVENT_LOOP_SFX = "loop_sfx";
    public static final String ANIM_EVENT_STOP_SFX = "stop_sfx";

    private SkeletonRenderer spineRenderer;
    //    private static SpriteBatch sBatch = new SpriteBatch();

    private HashMap<String, String[]> animDefs = new HashMap<>();
    private HashMap<String, String> replaceAnims = new HashMap<>();
    private HashMap<String, Float> animTimeScales = new HashMap<>();
    private HashMap<Animation, Long> animSounds = new HashMap<>();

    protected AnimationState state;
    private Skeleton skeleton;
    private SkeletonData skData;
    private int maxTracks = 1;
    protected boolean fillIn = false;

    private String skin;
    private float skScaleX = 1f;
    private float skScaleY = 1f;

    protected Affine2 drawTransform;
    protected Matrix4 scaleTransform = new Matrix4();
    protected Matrix4 tmpTransform = new Matrix4();

    private Bone rootBone;
    private Bone originBone;
    private Bone bodyBone;
    private boolean updateAnim;

    private FrameBuffer fbo;
    private TextureRegion fboRegion;
    private boolean useDefName;

    protected Color drawColor = new Color();

    private Vector2 tmpVec2 = new Vector2();

    private float randomTimeBase;
    private float randomTimeOffset;
    private float randomTime;
    private float randomTimer;

    private static final String animBackflip = "backflip";
    private static final String animScratch = "scratch";
    private static final String animTurnHead = "turn_head";

    private String[] randomAnims = new String[] {
                    animBackflip, animScratch, animTurnHead
    };

    public AnimatedEntity(EntityDef def) {
        super(def);
        drawTransform = new Affine2();
        spineRenderer = VJXGraphix.spineRenderer;
    }

    @Override
    protected void loadProperties(MapProperties properties) {
        super.loadProperties(properties);
        updateAnim = properties.get("updateAnim", true, boolean.class);
        useDefName = properties.get("useDefName", false, boolean.class);
        randomTimeBase = properties.get("idleFillInBaseTime", -1f, float.class);
        randomTimeOffset = properties.get("idleFillInOffset", 0f, float.class);
        randomTime = randomTimeBase + MathUtils.random(randomTimeOffset * 2f)
        - randomTimeOffset;
    }

    @Override
    public boolean update(float deltaT) {
        if (super.update(deltaT)) {
            if (updateAnim && getDef().getAnimationType() == AnimationType.SPINE)
                if (updateAnim) state.update(deltaT);

            randomTimer += deltaT;
            if (isActivity(Activity.IDLE) && randomTimeBase != -1) {
                if (!fillIn) {
                    if (randomTimer > randomTime) {
                        int random = MathUtils.random(randomAnims.length - 1);
                        setAnimation(randomAnims[random], false, 0);
                        randomTime = getAnimDuration(randomAnims[random]);
                        randomTimer = 0f;
                        fillIn = true;
                    }
                } else {
                    if (randomTimer > randomTime) {
                        fillIn = false;
                        randomTime = randomTimeBase
                                        + MathUtils.random(randomTimeOffset * 2f)
                                        - randomTimeOffset;
                        randomTimer = 0f;
                    }
                }
            } else randomTimer = 0f;

            return true;
        }
        return false;
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
        Color c = batch.getColor();
        drawColor.set(c);
        c.mul(actor.getColor());
        c.a *= parentAlpha;

        getLocalPosition(tmpVec);

        if (listener.getGame().getScreen().getRenderer().useFBO()) {
            fbo.begin();
            Gdx.gl20.glClearColor(0, 0, 0, 0);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            batch.begin();
        }
        batch.setColor(c);
        if (frame != null
                        && getDef().getAnimationType() == AnimationType.FRAME) {
            VJXGraphix.updateDrawTransform(this, drawTransform);

            batch.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);
            float w = frame.getRegionWidth() * 2 * getScaleX();
            float h = frame.getRegionHeight() * 2 * getScaleY();
            batch.draw(frame, w, h, drawTransform);
            batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        } else {
            rootBone.setScale(actor.getScaleX() * skScaleX,
                            actor.getScaleY() * skScaleY);

            rootBone.setRotation(getActor().getRotation());

            originBone.setScale(1f, 1f);
            if (useOrientation && orientation == Orientation.LEFT)
                originBone.setScaleX(-Math.abs(originBone.getScaleX()));

            if (isFlipY())
                originBone.setScaleY(-Math.abs(originBone.getScaleY()));
            skeleton.setColor(c);
            skeleton.setPosition(tmpVec.x, tmpVec.y);
            skeleton.updateWorldTransform();
            state.apply(skeleton);
            spineRenderer.draw(batch, skeleton);
        }

        if (listener.getGame().getScreen().getRenderer().useFBO()) {
            updateFBORegion();
            batch.end();
            fbo.end();
        }
        batch.setColor(drawColor);
    }

    @Override
    public void drawFrameDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(Color.PINK);

        float x, y, w, h;
        boolean project = false;

        if (fboRegion != null) {
            x = fboRegion.getRegionX();
            y = fbo.getHeight() - fboRegion.getRegionY();
            w = fboRegion.getRegionWidth();
            h = fboRegion.getRegionHeight();

            if (project) {
                tmpVec.set(x, y);
                tmpVec2.set(x + w, y + h);

                actor.getStage().getViewport().unproject(tmpVec);
                actor.getStage().getViewport().unproject(tmpVec2);

                tmpVec2.sub(tmpVec);

                actor.getParent().stageToLocalCoordinates(tmpVec);
            } else {
                y = fboRegion.getRegionY() - fboRegion.getRegionHeight();
                tmpVec.set(x, y);

                Viewport view = actor.getStage().getViewport();
                tmpVec2.set(view.getCamera().position.x,
                                view.getCamera().position.y);

                float zoom = ((OrthographicCamera) view.getCamera()).zoom;
                tmpVec2.sub(view.getWorldWidth() / 2 * zoom,
                                view.getWorldHeight() / 2 * zoom);

                tmpVec.add(tmpVec2);
                tmpVec2.set(w, h);
            }
            shapeRenderer.rect(tmpVec.x, tmpVec.y, tmpVec2.x, tmpVec2.y);
        }

        if (frame == null) return;
        w = frame.getRegionWidth() * 2;
        h = frame.getRegionHeight() * 2;
        float rotation = actor.getRotation();
        int flipX = isFlipX() ? -1 : 1;
        int flipY = isFlipY() ? -1 : 1;

        drawTransform.getTranslation(tmpVec);

        shapeRenderer.setColor(Color.GRAY);
        shapeRenderer.rect(tmpVec.x, tmpVec.y, 0, 0, w, h,
                        flipX * actor.getScaleX(), flipY * actor.getScaleY(),
                        rotation);
    }

    private void updateFBORegion() {
        getLocalPosition(tmpVec);

        actor.getParent().localToStageCoordinates(tmpVec);
        tmpVec2.set(tmpVec.x + getScaledWidth(), tmpVec.y + getScaledHeight());

        actor.getStage().stageToScreenCoordinates(tmpVec);
        actor.getStage().stageToScreenCoordinates(tmpVec2);
        tmpVec2.sub(tmpVec);

        tmpVec.y = fbo.getHeight() - tmpVec.y + fboRegion.getRegionHeight();
        fboRegion.setRegion((int) tmpVec.x, (int) tmpVec.y, (int) tmpVec2.x,
                        (int) tmpVec2.y);
    }

    public void drawToBatch(Batch batch) {
        if (this instanceof TiledEntity) return;
        if (this instanceof CollisionObject) return;

        batch.setColor(drawColor);

        getLocalPosition(tmpVec);
        actor.getParent().localToStageCoordinates(tmpVec);
        actor.getStage().stageToScreenCoordinates(tmpVec);
        tmpVec.y = fbo.getHeight() - tmpVec.y;
        batch.draw(fboRegion, tmpVec.x, tmpVec.y);
    }

    public void setFBO(FrameBuffer fbo) {
        //        this.fbo = fbo;
        int w = (int) getScaledWidth();
        int h = (int) getScaledHeight();
        this.fbo = new FrameBuffer(Format.RGBA8888, fbo.getWidth(),
                        fbo.getHeight(), false);
        fboRegion = new TextureRegion(this.fbo.getColorBufferTexture(), 0, 0,
                        w, h);
        fboRegion.flip(false, true);
    }

    public void setupSpine() {
        if (getDef().getAnimationType() != AnimationType.SPINE) return;

        skeleton = new Skeleton(getDef().getSkeleton());
        state = new AnimationState(getDef().getAnimationStateData());
        state.addListener(this);

        skData = skeleton.getData();

        rootBone = skeleton.getRootBone();
        originBone = getBone("origin");
        bodyBone = getBone("body");

        if (originBone == null) originBone = rootBone;
        if (bodyBone == null) bodyBone = originBone;

        skScaleX = rootBone.getScaleX();
        skScaleY = rootBone.getScaleY();

        if (skScaleX != 1 || skScaleY != 1) {
            VJXLogger.log(LogCategory.ERROR, this + " bad scale");
        }

        setupSpineAnimations();

        if (getDef().getValue(DefinitionProperty.skin, Boolean.class)) {
            skin = getDef().getValue(SourceProperty.tileName, String.class);
        }
        else
            if (getDef().getValue(DefinitionProperty.skeletonDef, Boolean.class))
                skin = getDefName();
            else skin = "default";

        setSkin(skin);

        setAnimation("idle_HP1", true);
        setAnimation("idle", true);
        state.update(MathUtils.random(getAnimDuration("idle")));
    }

    public void setSkin(String skin) {
        if (skeleton == null) return;
        if (skeleton.getData().findSkin(skin) != null) skeleton.setSkin(skin);
        else VJXLogger.log(LogCategory.WARNING,
                        getDefName() + ": Skin '" + skin + "' not found.");
    }

    public String getSkin() {
        return skeleton.getSkin().getName();
    }

    public void appear() {
        SequenceAction sa = Actions.sequence();
        appear(0, sa);
        actor.addAction(sa);
    }

    public void appear(float duration) {
        SequenceAction sa = Actions.sequence();
        appear(duration, sa);
        actor.addAction(sa);
    }

    public void appear(SequenceAction sequence) {
        appear(0, sequence);
    }

    public void appear(float duration, SequenceAction sequence) {
        setActivity(Activity.EXECUTE);

        sequence.addAction(Actions.visible(true));

        Command appearCmd = Commands.getAnimCommand(this, "appear", false);
        float aDur = execSingleCmd(appearCmd, sequence);
        if (aDur > 0) {
            Command delayCmd = Commands.getDelayCommand(aDur);
            execSingleCmd(delayCmd, sequence);
        }
        Command actCmd = Commands.getActivityCommand(this, Activity.IDLE);
        execSingleCmd(actCmd, sequence);
        Command animCmd = Commands.getAnimCommand(this, "idle", true);
        execSingleCmd(animCmd, sequence);
    }

    public void disappear() {
        SequenceAction sa = Actions.sequence();
        disappear(0, sa);
        actor.addAction(sa);
    }

    public void disappear(float duration) {
        SequenceAction sa = Actions.sequence();
        disappear(duration, sa);
        actor.addAction(sa);
    }

    public void disappear(SequenceAction sequence) {
        disappear(0, sequence);
    }

    public void disappear(float duration, SequenceAction sequence) {
        setActivity(Activity.EXECUTE);

        Command appearCmd = Commands.getAnimCommand(this, "disappear", false);
        float aDur = execSingleCmd(appearCmd, sequence);

        if (aDur > 0) {
            Command delayCmd = Commands.getDelayCommand(aDur);
            execSingleCmd(delayCmd, sequence);
        } else if (duration > 0) {
            Command alphaCmd = Commands.getAlphaCommand(0f, duration);
            execSingleCmd(alphaCmd, sequence);
            Command delayCmd = Commands.getDelayCommand(duration);
            execSingleCmd(delayCmd, sequence);
        }

        sequence.addAction(Actions.visible(false));
    }

    public void blink() {
        SequenceAction sa = Actions.sequence();
        blink(sa);
        actor.addAction(sa);
    }

    public void blink(float duration) {
        SequenceAction sa = Actions.sequence();
        blink(duration, .5f, sa);
        actor.addAction(sa);
    }

    public void blink(float duration, float timer) {
        SequenceAction sa = Actions.sequence();
        blink(duration, timer, sa);
        actor.addAction(sa);
    }

    public void blink(SequenceAction sequence) {
        blink(1f, .5f, sequence);
    }

    public void blink(float duration, SequenceAction sequence) {
        blink(duration, .5f, sequence);
    }

    public void blink(float duration, float timer, SequenceAction sequence) {
        sequence.addAction(EJXActions.blink(duration, timer, this));
    }

    //    private void drawFBO(Batch batch, Matrix4 projection) {
    //        //        batch.end();
    //        //        drawSkeleton(batch);
    //        //        drawToBatch(sBatch);
    //
    //        sBatch.setProjectionMatrix(projection);
    //        drawSkeleton2(batch);
    //        //        drawToBatch(batch);
    //
    //        //        batch.begin();
    //    }

    public boolean hasAnim(String name) {
        return getDef().getSkeleton().getData().findAnimation(name) != null;
    }

    public float getAnimDuration(String name) {
        if (definition.getAnimationType() == AnimationType.SPINE) {
            float duration = 0;
            if (skData.findAnimation(name) != null)
                duration = skData.findAnimation(name).getDuration();

            String[] addAnims = animDefs.get(name);
            if (addAnims != null && addAnims.length > 0)
                for (String animation : addAnims)
                    if (skData.findAnimation(animation) != null)
                        duration = Math.max(duration, skData
                                        .findAnimation(animation).getDuration());
            return duration;
        }

        return 0f;
    }

    private Animation getAnimation(String name) {
        return skData.findAnimation(name);
    }

    public Animation getCurrentAnimation() {
        if (state.getCurrent(0) == null) return null;
        return state.getCurrent(0).getAnimation();
    }

    public boolean isCurrentLooping() {
        if (state.getCurrent(0) == null) return false;
        return state.getCurrent(0).getLoop();
    }

    public SkeletonData getSkeletonData() {
        return skData;
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public AnimationState getAnimState() {
        return state;
    }

    public Vector2 getSpineOrigin(Vector2 targetVec) {
        targetVec.set(getWCX(), getWCY());
        if (definition.getAnimationType() == AnimationType.SPINE)
            if (originBone != null)
                targetVec.set(originBone.getX(), originBone.getY());
            else targetVec.set(getScaledWidth() / 2f, getScaledHeight() / 2);
        return targetVec;
    }

    public Bone getBone(String name) {
        Array<Bone> bones = skeleton.getBones();

        for (Bone b : bones)
            if (b.getData().getName().equals(name)) return b;
        return null;
    }

    public Bone getRootBone() {
        return rootBone;
    }

    public Bone getOriginBone() {
        return originBone;
    }

    public Bone getBodyBone() {
        return bodyBone;
    }

    public void enqueueAnimation(String name, boolean loop, int track, float delay) {
        if (definition.getAnimationType() == AnimationType.SPINE) {
            //            setSpineAnimation(name, loop, track, delay);
            return;
        }
    }

    private HashSet<Integer> additionalTracks = new HashSet<>();

    public int addAnimation(String name, boolean loop) {
        Animation anim = getAnimation(name);
        if (anim != null) return addAnimation(anim, loop);
        else return -1;
    }

    public int addAnimation(Animation name, boolean loop) {
        int track = maxTracks;
        while (additionalTracks.contains(track))
            track++;

        setSpineAnimation(name, loop, track, 0f, false);
        additionalTracks.add(track);
        return track;
    }

    public void setAnimation(String name, boolean loop) {
        setSpineAnimation(name, loop, 0, 0f, false);
    }

    public void setAnimation(String name, boolean loop, int track) {
        setSpineAnimation(name, loop, track, 0f, false);
    }

    public void setAnimation(String name, boolean loop, int track, boolean enqueue) {
        setSpineAnimation(name, loop, track, 0f, enqueue);
    }

    public void stopAnim(String name, int trackID) {
        if (trackID < 0 || definition.getAnimationType() != AnimationType.SPINE)
            return;

        state.clearTrack(trackID);
        additionalTracks.remove(trackID);
        skeleton.setSlotsToSetupPose();
    }

    private void setSpineAnimation(String name, boolean loop, int track,
                    float delay, boolean enqueue) {
        if (definition.getAnimationType() != AnimationType.SPINE)
            return;

        Animation anim = findAnim(name);
        if (setSpineAnimation(anim, loop, track++, delay, enqueue))
            addAnims(name, loop, track);

    }

    private boolean setSpineAnimation(Animation anim, boolean loop, int track,
                    float delay, boolean enqueue) {
        if (anim == null) return false;
        if (enqueue) {
            state.addAnimation(track, anim, loop, delay);
            state.apply(skeleton);
            return true;
        }

        TrackEntry current = state.getCurrent(track);
        if (current != null && current.getAnimation() == anim) {
            return false;
        }

        if (fillIn && anim.getName().equals(Activity.IDLE.name)) return false;
        else fillIn = false;

        skeleton.setToSetupPose();

        for (int i = 0; i < maxTracks; i++)
            state.clearTrack(i);

        state.setAnimation(track++, anim, loop);

        if (animTimeScales.containsKey(anim.getName()))
            state.setTimeScale(animTimeScales.get(anim.getName()));
        else state.setTimeScale(1f);

        if (delay > 0) state.update(delay);
        state.apply(skeleton);

        return true;
    }

    private void addAnims(String name, boolean loop, int track) {
        String[] addAnims = animDefs.get(name);
        if (addAnims != null && addAnims.length > 0) {
            for (String animation : addAnims) {
                state.setAnimation(track++, findAnim(animation), loop);
            }
        }
    }

    private Animation findAnim(String name) {
        // find base animation
        Animation anim = getAnimation(name);
        if (anim == null)
            // look for replacement animation
            if (replaceAnims.containsKey(name)) {
                anim = getAnimation(replaceAnims.get(name));
            } else {
                // find animation based on attributes
                String attr = definition.getAttributes();
                if (!attr.isEmpty())
                    anim = getAnimation(name + "_" + attr);
            }

        return anim;
    }

    private void setupSpineAnimations() {
        String[] animsArray;
        String[] animSet;
        String animsString = definition.getCustomProperties()
                        .get("animsReplace", "", String.class);

        if (!animsString.isEmpty()) {
            animsString = animsString.replace("\r", "");
            animsArray = animsString.split("\n");
            for (String animation : animsArray) {
                animSet = animation.split(":");
                replaceAnims.put(animSet[0], animSet[1]);
            }
        }

        animsString = definition.getCustomProperties().get("animsAdd", "",
                        String.class);
        if (!animsString.isEmpty()) {
            animsString = animsString.replace("\r", "");
            animsArray = animsString.split("\n");
            String[] addAnims;
            for (String animation : animsArray) {
                animSet = animation.split(":");
                addAnims = animSet[1].split(VJXString.SEP_LIST);
                animDefs.put(animSet[0], addAnims);
                maxTracks = Math.max(maxTracks, addAnims.length) + 1;
            }
        }

        String animTimesString = definition.getCustomProperties()
                        .get("animTimeScales", "", String.class);
        if (!animTimesString.isEmpty()) {
            animTimesString = animTimesString.replace("\r", "");
            animsArray = animTimesString.split("\n");
            for (String animation : animsArray) {
                animSet = animation.split(":");
                animTimeScales.put(animSet[0], Float.parseFloat(animSet[1]));
            }
        }

        //        Animation anim = skeleton.getData().findAnimation("idle");
        //        Animation anim2 = skeleton.getData().findAnimation("backflip");
        //        if (anim == null) return;
        //        if (anim2 == null) return;
        //        state.getData().setMix("idle", "backflip", getAnimDuration("backflip"));
        //        //        //        state.getData().setMix("idle", "run", 1f);
        //        //
        //        anim2 = skeleton.getData().findAnimation("backflip");
        //        if (anim2 == null) return;
        //        state.getData().setMix("idle", "scratch", getAnimDuration("scratch"));
    }

    private void playAnimSound(Animation anim, String name) {
        if (animSounds.containsKey(anim)) {
            VJXSound s = getListener().getAudioControl().getSound(name);
            if (s == null) return;

            // clear old sound id if it is not looping and start new
            if (!s.isLooping()) {
                stopAnimSound(anim, name);
            }
            Long id = animSounds.get(anim);
            if (id == null || id == -1) animSounds.put(anim, playSfx(name));
        } else {
            animSounds.put(anim, playSfx(name));
        }
    }

    private void stopAnimSound(Animation anim, String name) {
        VJXSound s = getListener().getAudioControl().getSound(name);
        if (s == null) return;
        if (s.isLooping()) stopSfx(name, animSounds.remove(anim));
        else animSounds.remove(anim);
    }

    private void animEnded(Animation anim) {
        Array<EJXLogic> logics = getLogics();
        for (int i = 0; i < logics.size; i++) {
            logics.get(i).animEnded(anim);
        }
    }

    private void executeAnimEvent(String name, String string) {
        switch (name) {
            case ANIM_EVENT_PLAY_SFX:
                playSfx(getDefName() + "_" + name);
                break;
            case ANIM_EVENT_LOOP_SFX:
                break;
            case ANIM_EVENT_STOP_SFX:
                break;
        }

        Array<EJXLogic> logics = getLogics();
        for (int i = 0; i < logics.size; i++) {
            logics.get(i).executeAnimEvent(name, string);
        }
    }

    @Override
    public void start(TrackEntry entry) {
        int trackID = entry.getTrackIndex();
        if (state == null || state.getCurrent(trackID) == null)
            return;
        Animation anim = entry.getAnimation();
        VJXLogger.log(LogCategory.ANIM,
                        this + " Spine animation '" + anim.getName()
                        + "' on track " + trackID
                        + " started. Duration: "
                        + anim.getDuration() + "s");

        String name = getSoundName(anim);
        playAnimSound(anim, name);
    }

    @Override
    public void interrupt(TrackEntry entry) {
        int trackID = entry.getTrackIndex();
        if (state == null || state.getCurrent(trackID) == null) return;
        Animation anim = entry.getAnimation();
        VJXLogger.log(LogCategory.ANIM, this + " Spine animation '"
                        + anim.getName() + "' on track " + trackID
                        + " interrupted. Duration: " + anim.getDuration()
                        + "s");
        String name = getSoundName(anim);
        stopAnimSound(anim, name);
    }

    @Override
    public void complete(TrackEntry entry) {
        int trackID = entry.getTrackIndex();
        if (state == null || state.getCurrent(trackID) == null)
            return;
        Animation anim = entry.getAnimation();
        VJXLogger.log(LogCategory.ANIM,
                        this + " Spine animation '" + anim.getName()
                        + "' on track " + trackID
                        + " completed. Duration: "
                        + anim.getDuration() + "s");

        String name = getSoundName(anim);
        if (state.getCurrent(trackID).getLoop()) {
            playAnimSound(anim, name);
        }
        else {
            stopAnimSound(anim, name);
            playSfx(name + "_end");
            animEnded(anim);
        }
    }


    @Override
    public void end(TrackEntry entry) {
        int trackID = entry.getTrackIndex();
        if (state == null || state.getCurrent(trackID) == null)
            return;

        Animation anim = entry.getAnimation();
        VJXLogger.log(LogCategory.ANIM,
                        this + " Spine animation '" + anim.getName()
                        + "' on track " + trackID
                        + " ended. Duration: "
                        + anim.getDuration() + "s");

        String name = getSoundName(anim);
        stopAnimSound(anim, name);
        playSfx(name + "_end");
        animEnded(anim);
    }

    @Override
    public void dispose(TrackEntry entry) {

    }

    private String getSoundName(Animation anim) {
        return getSoundName() + VJXString.FILL_sfx + anim.getName();
    }

    @Override
    public String getSoundName() {
        if (useDefName || skData == null)
            return getDefName();
        return super.getSoundName();
    }

    @Override
    public void event(TrackEntry entry, com.esotericsoftware.spine.Event arg1) {
        int trackID = entry.getTrackIndex();

        EventData data = arg1.getData();
        String eventName = data.getName();

        String s = " Spine animation event on track " + trackID;
        s += " at " + arg1.getTime() + "s: " + eventName;
        VJXLogger.log(LogCategory.ANIM, this + s + " - args:" + arg1.getString());

        executeAnimEvent(eventName, arg1.getString());
    }
}