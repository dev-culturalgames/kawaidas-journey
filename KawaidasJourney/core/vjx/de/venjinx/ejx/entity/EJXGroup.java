package de.venjinx.ejx.entity;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.SnapshotArray;

public class EJXGroup extends Group {

    private boolean drawOrderY = false;
    private int depth = 0;

    public EJXGroup(String name) {
        super();
        setName(name);
    }

    public void drawToBatch(Batch batch) {
        SnapshotArray<Actor> children = getChildren();
        Actor[] actors = children.begin();
        Object dataObject;
        for (int i = 0, n = children.size; i < n; i++) {
            Actor child = actors[i];
            if (!child.isVisible()) continue;

            dataObject = child.getUserObject();
            if (dataObject instanceof AnimatedEntity) {
                AnimatedEntity e = (AnimatedEntity) dataObject;
                // render the offscreen texture with "premultiplied alpha" blending
                //                batch.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);
                e.drawToBatch(batch);
                //                if (e.getFrame() != null) {
                //                    //                    batch.draw(e.getFrame(), 0, 0);
                //                    //                    batch.setColor(drawColor);
                //                    e.getLocalPosition(tmpVec);
                //                    batch.draw(e.getFrame(), tmpVec.x, tmpVec.y);
                //                }
                //                e.dra
            }
        }
        children.end();
    }

    public void setDrawOrderY(boolean drawOrderY) {
        this.drawOrderY = drawOrderY;
    }

    public boolean isDrawOrderY() {
        return drawOrderY;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getDepth() {
        return depth;
    }
}