package de.venjinx.ejx.entity.logics;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.esotericsoftware.spine.Animation;

import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EntityCallbacks;
import de.venjinx.ejx.entity.EntityDef;
import de.venjinx.ejx.entity.logics.EJXLogics.EJXLogicType;
import de.venjinx.ejx.util.EJXTypes;
import de.venjinx.ejx.util.EJXTypes.EJXProperty;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.VJXProperties;
import de.venjinx.ejx.util.VJXData;

public abstract class EJXLogic implements EntityCallbacks {

    protected Vector2 tmpVec;
    protected EJXEntity owner;
    protected EntityDef definition;
    protected EJXLogicType logicType;
    protected EJXPropertyDefinition[] userProperties;
    protected VJXProperties vjxProps;
    protected MapProperties mapProperties;
    protected GameControl listener;
    protected boolean unstucking;

    public EJXLogic(EJXLogicType logicType) {
        this.logicType = logicType;

        tmpVec = new Vector2();
        vjxProps = EJXTypes.newProps();
        userProperties = new EJXPropertyDefinition[0];
    }

    public EJXLogic(EJXLogicType logicType, EJXPropertyDefinition[] properties) {
        this.logicType = logicType;
        userProperties = properties;
        tmpVec = new Vector2();
        vjxProps = EJXTypes.newProps();
    }

    public abstract void init(MapProperties properties);

    public EJXPropertyDefinition getPropertyDefinition(String propertyName) {
        for (EJXPropertyDefinition property : userProperties) {
            if (property.getName().equals(propertyName))
                return property;
        }
        return null;
    }

    public EJXProperty<?> getProperty(String propertyName) {
        EJXPropertyDefinition prop = getPropertyDefinition(propertyName);
        Object property = vjxProps.get(prop);
        if (property instanceof EJXProperty<?>)
            return (EJXProperty<?>) property;
        return null;
    }

    public <T> T getProperty(EJXPropertyDefinition key, Class<T> clazz) {
        return vjxProps.get(key, clazz);
    }

    public void setProperty(EJXPropertyDefinition property, Object value) {
        vjxProps.get(property);
    }

    public void setOwner(EJXEntity owner) {
        this.owner = owner;
        definition = owner.getDef();
        mapProperties = definition.getCustomProperties();
        listener = owner.getListener();
    }

    public EJXEntity getOwner() {
        return owner;
    }

    public EJXLogicType getType() {
        return logicType;
    }

    public SequenceAction interrupt(float delayResume) {
        SequenceAction sa = Actions.sequence();
        return sa;
    }

    public void fixturesCreated() {
    }

    public void update(float deltaT) {
    }

    public void drawBatchDebug(Batch batch, BitmapFont font) {
    }

    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
    }

    public void childSpawned(EJXEntity child) {
    }

    public void childDespawned(EJXEntity child) {
    }

    public void waypointReached() {
    }

    public void executeAnimEvent(String name, String string) {
    }

    public void animEnded(Animation animation) {
    }

    public boolean unstuck() {
        return false;
    }

    public boolean unstucking() {
        return unstucking;
    }

    public void stopUnstucking() {
        unstucking = false;
    }

    @Override
    public void onRespawn() {
    }

    @Override
    public void onScreenChanged(boolean onScreen) {
    }

    @Override
    public void targetInView(boolean inView) {
    }

    @Override
    public void targetInRange(boolean inRange) {
    }

    @Override
    public void targetChanged(EJXEntity targetEntity) {
    }

    @Override
    public void onTrigger(boolean triggered, VJXData thisData,
                    EJXEntity other, VJXData otherData) {
    }

    @Override
    public void onActivated(EJXEntity activator, boolean activated) {
    }

    @Override
    public void onDamage(int oldHP, int newHP) {
    }

    @Override
    public void onDie() {
    }

    @Override
    public void onDead() {
    }

    @Override
    public void onDespawn() {
    }

    @Override
    public void onEnd() {
    }

    @Override
    public void onStart() {
    }

    @Override
    public void positionReached() {
    }

    @Override
    public String toString() {
        return logicType.getName();
    }
}