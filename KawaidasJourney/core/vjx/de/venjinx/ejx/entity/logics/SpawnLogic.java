package de.venjinx.ejx.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.cmds.Commands;
import de.venjinx.ejx.cmds.SpawnCommand;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class SpawnLogic extends EJXLogic {

    public static final String spawn_align = "spawn:align";
    public static final String spawn_behind = "spawn:behind";
    public static final String spawn_checkWP = "spawn:check_wp";
    public static final String spawn_defName = "spawn:defName";
    public static final String spawn_delay = "spawn:delay";
    public static final String spawn_delayRandomize = "spawn:delay_randomize";
    public static final String spawn_layer = "spawn:layer";
    public static final String spawn_loop = "spawn:loop";
    public static final String spawn_max = "spawn:max";
    public static final String spawn_rangeX = "spawn:rangeX";
    public static final String spawn_rangeY = "spawn:rangeY";
    public static final String spawn_scale = "spawn:scale";
    public static final String spawn_spawning = "spawn:spawning";
    public static final String spawn_waypoints = "spawn:waypoints";

    public enum SpawnProperty implements EJXPropertyDefinition {
        align(SpawnLogic.spawn_align, PropertyType.stringProp, VJXString.ALIGN_CENTER),
        behind(SpawnLogic.spawn_behind, PropertyType.boolProp, false),
        checkWP(SpawnLogic.spawn_checkWP, PropertyType.boolProp, false),
        defName(SpawnLogic.spawn_defName, PropertyType.stringProp, VJXString.STR_EMPTY),
        delay(SpawnLogic.spawn_delay, PropertyType.floatProp, 0f),
        delayRandomize(SpawnLogic.spawn_delayRandomize, PropertyType.floatProp, 0f),
        layer(SpawnLogic.spawn_layer, PropertyType.stringProp, VJXString.STR_EMPTY),
        loop(SpawnLogic.spawn_loop, PropertyType.boolProp, false),
        max(SpawnLogic.spawn_max, PropertyType.intProp, 1),
        rangeX(SpawnLogic.spawn_rangeX, PropertyType.floatProp, 0f),
        rangeY(SpawnLogic.spawn_rangeY, PropertyType.floatProp, 0f),
        scale(SpawnLogic.spawn_scale, PropertyType.floatProp, 1f),
        spawning(SpawnLogic.spawn_spawning, PropertyType.boolProp, true),
        waypoints(SpawnLogic.spawn_waypoints, PropertyType.stringProp, VJXString.STR_EMPTY)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private SpawnProperty(final String name, final PropertyType type, Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final SpawnProperty get(String name) {
            for (SpawnProperty property : SpawnProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private int currentChildCount = 0;
    private float timePassed = 0f;
    private int currentWPIndex = 0;
    private Waypoint currentWP;

    private String defName;
    private String layerName;
    private int maxSpawn;
    private float rangeX;
    private float rangeY;

    private boolean checkWP;
    private boolean spawning;
    private boolean behindParent;
    private boolean loop;

    private float scale;
    private float delay;
    private float delayRandomize;

    private float currentDelay;
    private Array<Waypoint> waypoints;
    private String align;
    private SpawnCommand spawnCmd;

    public SpawnLogic() {
        super(VJXLogicType.SPAWN);
        waypoints = new Array<>();
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(SpawnProperty.values(), vjxProps,
                        properties);

        spawnCmd = Commands.getSpawnCommand(listener.getGame(),
                        VJXString.STR_EMPTY, owner, 0, 0);
        Waypoints.loadWaypoints(SpawnProperty.waypoints.name, owner, waypoints);

        if (waypoints.size == 0) {
            currentWP = new Waypoint(owner.getName() + "_origin");
            currentWP.setCenter(owner.getWorldCenter());
        } else currentWP = waypoints.get(0);

        spawning = vjxProps.get(SpawnProperty.spawning, Boolean.class);

        defName = vjxProps.get(SpawnProperty.defName, String.class);
        if (defName == null || defName.isEmpty()) defName = owner.getDefName();

        layerName = vjxProps.get(SpawnProperty.layer, String.class);
        if (layerName == null || layerName.isEmpty())
            layerName = owner.getLayer().getName();
        if (layerName == null || layerName.isEmpty())
            layerName = "world";

        maxSpawn = vjxProps.get(SpawnProperty.max, Integer.class);
        rangeX = vjxProps.get(SpawnProperty.rangeX, Float.class);
        rangeY = vjxProps.get(SpawnProperty.rangeY, Float.class);
        checkWP = vjxProps.get(SpawnProperty.checkWP, boolean.class);
        behindParent = vjxProps.get(SpawnProperty.behind, boolean.class);
        loop = vjxProps.get(SpawnProperty.loop, boolean.class);
        scale = vjxProps.get(SpawnProperty.scale, Float.class);
        delay = vjxProps.get(SpawnProperty.delay, Float.class);
        delayRandomize = vjxProps.get(SpawnProperty.delayRandomize, Float.class);
        align = vjxProps.get(SpawnProperty.align, String.class);

        currentDelay = MathUtils.random(delayRandomize);
    }

    @Override
    public void update(float deltaT) {
        super.update(deltaT);

        if (loop) setSpawning(currentChildCount < maxSpawn);

        if (!isSpawning() || !owner.isActivity(Activity.IDLE))
            return;

        timePassed += deltaT;

        if (timePassed >= delay + currentDelay) {
            if (currentChildCount < maxSpawn) {
                spawnCmd.setDefName(defName);
                spawnCmd.setLayerNode(listener.getGroup(layerName));

                spawnCmd.setPosition(currentWP.getCenter(tmpVec));

                spawnCmd.setExecutor(owner);

                if (layerName.equals(owner.getLayer().getName()))
                    spawnCmd.setSpawnBehindParent(behindParent);

                listener.execSingleCmd(spawnCmd);

                currentChildCount++;
                setSpawning(currentChildCount < maxSpawn);
            }

            timePassed -= delay + currentDelay;
            currentDelay = delayRandomize * (float) Math.random();
        }
    }

    public void setSpawning(boolean spawning) {
        this.spawning = spawning;
    }

    public boolean isSpawning() {
        return spawning;
    }

    private void updateSpawnPos(EJXEntity entity) {
        currentWP.getCenter(tmpVec);
        currentWP.add(entity);

        tmpVec.add(MathUtils.random(rangeX) * 2 - rangeX,
                        MathUtils.random(rangeY) * 2 - rangeY);
        EJXLogics.setAlign(align, tmpVec, owner);

        tmpVec.sub(entity.getScaledWidth() / 2f, entity.getScaledHeight() / 2f);
        entity.setPosition(tmpVec.x, tmpVec.y, true, true);

        if (checkWP) updateWaypoint();
    }

    private void updateWaypoint() {
        if (waypoints.size <= 1) return;

        currentWPIndex = (currentWPIndex + 1) % waypoints.size;
        currentWP = waypoints.get(currentWPIndex);

        if (checkWP && currentWP.hasEntity()) {
            Waypoint tmp = currentWP;
            for (int i = 0; i < waypoints.size; i++) {
                tmp = waypoints.get(i);
                if (tmp == currentWP) continue;
                if (!tmp.hasEntity()) currentWP = tmp;
            }
        }
    }

    @Override
    public void childSpawned(EJXEntity child) {
        if (!child.getDefName().equals(defName))
            return;
        child.getScale(tmpVec);
        child.setScale(scale * tmpVec.x, scale * tmpVec.y);
        updateSpawnPos(child);
    }

    @Override
    public void childDespawned(EJXEntity child) {
        currentChildCount--;
        child.removeSpawnPoint();

        setSpawning(currentChildCount < maxSpawn);
    }
}