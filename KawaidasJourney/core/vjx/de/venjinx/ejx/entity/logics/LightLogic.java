package de.venjinx.ejx.entity.logics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;

import box2dLight.PointLight;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class LightLogic extends EJXLogic {

    // global light properties
    public static final String LIGHT_amb_color = "lights:amb:color";

    public static final String LIGHT_blur = "lights:blur";
    public static final String LIGHT_shadows = "lights:shadows";

    // focus light properties
    public static final String LIGHT_focus_color = "lights:focus:color";
    public static final String LIGHT_focus_radius = "lights:focus:radius";
    public static final String LIGHT_focus_soft = "lights:focus:soft";
    public static final String LIGHT_focus_softness = "lights:focus:softness";
    public static final String LIGHT_focus_xRay = "lights:focus:xRay";

    // light properties
    public static final String LIGHT_align = "light:align";
    public static final String LIGHT_color = "light:color";
    public static final String LIGHT_radius = "light:radius";
    public static final String LIGHT_soft = "light:soft";
    public static final String LIGHT_softness = "light:softness";
    public static final String LIGHT_xRay = "light:xRay";

    private PointLight light;
    private Vector2 lightLocalPos;
    private float radius;
    private Float softness;

    public LightLogic() {
        super(VJXLogicType.LIGHT);
    }

    @Override
    public void init(MapProperties properties) {
        radius = properties.get(LIGHT_radius,
                        owner.getScaledWidth() * 5f, Float.class)
                        * B2DWorld.WORLD_TO_B2D;
        softness = properties.get(LIGHT_softness, 0f, Float.class);
        //        boolean xRay = properties.get(VJXString.LIGHT_XRAY, true, Boolean.class);
        //        boolean soft = properties.get(VJXString.LIGHT_SOFT, false, Boolean.class);
        //        Color color = properties.get(VJXString.LIGHT_COLOR, new Color(1, 1, 1, .5f),
        //                        Color.class);

        final String align = properties.get(LIGHT_align, VJXString.ALIGN_CENTER,
                        String.class);


        lightLocalPos = new Vector2();
        EJXLogics.setAlign(align, lightLocalPos, owner.getScaledWidth(),
                        owner.getScaledHeight(), owner.getScaleX(),
                        owner.getScaleY());
        lightLocalPos.scl(B2DWorld.WORLD_TO_B2D);

        //        light = new PointLight(listener.getLightRenderer(), B2DWorld.RAY_COUNT,
        //                        color, radius, 0f, 0f);
        //
        //        light.setXray(xRay);
        //        light.setSoft(soft);
        //        light.setSoftnessLength(softness);
        //        tmpVec.set(owner.getWorldPosition()).scl(B2DWorld.WORLD_TO_B2D);
        //        light.setPosition(tmpVec.add(lightLocalPos));
    }

    @Override
    public void update(float delta) {
        tmpVec.set(owner.getWorldPosition())
        .scl(B2DWorld.WORLD_TO_B2D);
        light.setPosition(tmpVec.add(lightLocalPos));
    }

    @Override
    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(Color.BLUE);
        shapeRenderer.circle(
                        light.getPosition().x * B2DWorld.B2D_TO_WORLD,
                        light.getPosition().y * B2DWorld.B2D_TO_WORLD,
                        radius * B2DWorld.B2D_TO_WORLD);
        shapeRenderer.setColor(Color.CYAN);
        shapeRenderer.circle(
                        light.getPosition().x * B2DWorld.B2D_TO_WORLD,
                        light.getPosition().y * B2DWorld.B2D_TO_WORLD,
                        softness * B2DWorld.B2D_TO_WORLD);
    }
}