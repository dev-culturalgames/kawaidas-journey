package de.venjinx.ejx.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.entity.EJXEntity;

public class RangeTracker {

    public interface RangeTrackerListener {
        public Vector2 getCurrentPosition(Vector2 storeVector);

        public void targetInRange(EJXEntity target);

        public void targetOutRange(EJXEntity target);
    }

    private static String prop_rangeOld = "range";
    private static String prop_range = ":range";
    private static String prop_rangeX = ":rangeX";
    private static String prop_rangeY = ":rangeY";

    private Circle triggerCircle;
    private Rectangle triggerBox;

    private RangeTrackerListener trackingListener;
    private Array<EJXEntity> trackedObjects;
    private Array<Long> inRangeIds;

    public RangeTracker(String name, RangeTrackerListener listener, MapProperties properties) {
        trackingListener = listener;
        float range = properties.get(prop_rangeOld, 0f, Float.class);
        triggerCircle = new Circle(0, 0,
                        properties.get(name + prop_range, range, Float.class));
        triggerBox = new Rectangle(0, 0,
                        properties.get(name + prop_rangeX, 0f, Float.class) * 2f,
                        properties.get(name + prop_rangeY, 0f, Float.class) * 2f);
        trackedObjects = new Array<>();
        inRangeIds = new Array<>();
    }

    public void update(EJXEntity object) {
        triggerCircle.setPosition(object.getWCX(), object.getWCY());
        triggerBox.setCenter(object.getWCX(), object.getWCY());
        float x, y;
        for (EJXEntity entity : trackedObjects) {
            x = entity.getWCX();
            y = entity.getWCY();
            if (!inRange(entity.getID()) && (triggerCircle.contains(x, y)
                            || triggerBox.contains(x, y))) {
                inRangeIds.add(entity.getID());
                trackingListener.targetInRange(entity);
            }
            if (inRange(entity.getID()) && !triggerBox.contains(x, y)
                            && !triggerCircle.contains(x, y)) {
                inRangeIds.removeValue(entity.getID(), false);
                trackingListener.targetOutRange(entity);
            }
        }
    }

    public void track(EJXEntity object) {
        if (!trackedObjects.contains(object, true)) trackedObjects.add(object);
    }

    public void untrack(EJXEntity object) {
        trackedObjects.removeValue(object, true);
    }

    public void untrackAll() {
        trackedObjects.clear();
    }

    public float getX() {
        return triggerBox.x;
    }

    public float getY() {
        return triggerBox.y;
    }

    public float getCX() {
        return triggerCircle.x;
    }

    public float getCY() {
        return triggerCircle.y;
    }

    public boolean inRange(Vector2 targetPosition) {
        return inRange(targetPosition.x, targetPosition.y);
    }

    public boolean inRange(float x, float y) {
        return triggerCircle.contains(x, y) || triggerBox.contains(x, y);
    }

    public float getRange() {
        return triggerCircle.radius;
    }

    public void setRange(float range) {
        triggerCircle.setRadius(range);
    }

    public float getRangeX() {
        return triggerBox.getWidth() / 2;
    }

    public void setRangeX(float rangeX) {
        triggerBox.setWidth(rangeX);
    }

    public float getRangeY() {
        return triggerBox.getHeight() / 2;
    }

    public void setRangeY(float rangeY) {
        triggerBox.setHeight(rangeY);
    }

    private boolean inRange(long id) {
        return inRangeIds.contains(id, false);
    }
}