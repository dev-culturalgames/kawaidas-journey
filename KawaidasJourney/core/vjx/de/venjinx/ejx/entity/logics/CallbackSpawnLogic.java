package de.venjinx.ejx.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;

public class CallbackSpawnLogic extends EJXLogic {

    private int spawnCount;
    private float spawnRange;
    private String spawnName;
    private Waypoint[] waypoints;
    private EJXEntity target;
    private float spawnAngle;
    private float spawnDelay;

    public CallbackSpawnLogic() {
        super(VJXLogicType.END_SPAWN);
    }

    @Override
    public void init(MapProperties properties) {
        target = owner.getTargetEntity();
        spawnDelay = properties.get("cbspawn:delay", 0f, float.class);
        spawnCount = properties.get("cbspawn:count", 0, int.class);
        spawnName = properties.get("cbspawn:name", null, String.class);
        spawnRange = properties.get("cbspawn:range", 256f, float.class);
        spawnAngle = properties.get("cbspawn:angle", 90f, float.class);
        String wps = properties.get("cbspawn:waypoints", String.class);
        if (wps != null) waypoints = Waypoints.getWaypoints(wps);
    }

    @Override
    public void onEnd() {
        listener.getLevel().spawnGroup(owner, spawnName, spawnCount, spawnRange,
                        spawnAngle, spawnDelay, waypoints,
                        target.getWCX() < owner.getWCX());
    }

    public int getCount() {
        return spawnCount;
    }

    public String getSpawnName() {
        return spawnName;
    }
}