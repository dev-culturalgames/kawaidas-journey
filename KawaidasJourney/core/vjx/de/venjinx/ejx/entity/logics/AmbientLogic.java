package de.venjinx.ejx.entity.logics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.audio.VJXSound;
import de.venjinx.ejx.controllers.AudioControl;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class AmbientLogic extends EJXLogic {

    public static final String amb_delay = "ambient:delay";
    public static final String amb_delayRandomize = "ambient:delay_randomize";
    public static final String amb_maxProgress = "ambient:maxProgress";
    public static final String amb_minProgress = "ambient:minProgress";
    public static final String amb_random = "ambient:randomize";
    public static final String amb_rangeX = "ambient:rangeX";
    public static final String amb_rangeY = "ambient:rangeY";
    public static final String amb_target = "ambient:target";

    public enum AmbientProperty implements EJXPropertyDefinition {
        delay(AmbientLogic.amb_delay, PropertyType.floatProp, 1f),
        delayRandomize(AmbientLogic.amb_delayRandomize, PropertyType.floatProp, 2f),
        maxProgress(AmbientLogic.amb_maxProgress, PropertyType.floatProp, 1f),
        minProgress(AmbientLogic.amb_minProgress, PropertyType.floatProp, 0f),
        random(AmbientLogic.amb_random, PropertyType.boolProp, false),
        rangeX(AmbientLogic.amb_rangeX, PropertyType.floatProp, 256f),
        rangeY(AmbientLogic.amb_rangeY, PropertyType.floatProp, 256f),
        target(AmbientLogic.amb_target, PropertyType.stringProp, EJXCamera.cam_target),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private AmbientProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final AmbientProperty get(String name) {
            for (AmbientProperty property : AmbientProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private float rangeX;
    private float rangeY;

    private Rectangle area;

    private boolean active;
    private float delay;
    private float delayRandomize;
    private float minProgress;
    private float maxProgress;

    private float timer = 0;
    private float nextTime;

    private Array<VJXSound> slist;
    private AudioControl ac;
    private boolean wasInArea;

    public AmbientLogic() {
        super(VJXLogicType.AMBIENT);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(AmbientProperty.values(), vjxProps, properties);

        active = true;
        rangeX = vjxProps.get(AmbientProperty.rangeX, float.class);
        rangeY = vjxProps.get(AmbientProperty.rangeY, float.class);
        delay = vjxProps.get(AmbientProperty.delay, float.class);
        delayRandomize = vjxProps.get(AmbientProperty.delayRandomize, float.class);
        minProgress = vjxProps.get(AmbientProperty.minProgress, float.class);
        maxProgress = vjxProps.get(AmbientProperty.maxProgress, float.class);

        area = new Rectangle(0, 0, rangeX * 2, rangeY * 2);
        setAreaSize(rangeX, rangeY);

        ac = listener.getAudioControl();
        ac.addDynamicSounds(owner.getDefName(), properties);
        slist = ac.getSounds(owner.getDefName());
    }

    @Override
    public void update(float deltaT) {
        if (!active)
            return;
        float progress = listener.getLevel().getLvlProgress();

        if (progress < minProgress || maxProgress != 1
                        && progress >= maxProgress) { return; }
        if (owner.getTargetEntity() == null) return;

        boolean inArea = area.contains(owner.getTargetEntity().getWorldCenter());
        if (inArea ^ wasInArea && deltaT > 0)
            inAreaChanged(inArea);

        wasInArea = inArea && deltaT > 0;
        if (!inArea)
            return;

        timer += deltaT;
        if (timer >= nextTime && slist.size > 0) {
            timer = 0;
            nextTime = delay + MathUtils.random(delayRandomize);

            float probability = MathUtils.random(100) / 100f;
            float addProb = 0f;
            for (VJXSound sound : slist) {
                if (sound.getName().contains(VJXString.PRE_ambient)) continue;
                addProb += sound.getPropability();
                if (probability <= addProb) {
                    ac.playDynSFX(sound.getName());
                    break;
                }
            }
        }
    }

    public void setAreaCenter(Vector2 center) {
        setAreaCenter(center.x, center.y);
    }

    public void setAreaCenter(float centerX, float centerY) {
        area.setCenter(centerX, centerY);
    }

    public void setAreaRangeX(float rangeX) {
        area.setWidth(rangeX * 2);
    }

    public void setAreaRangeY(float rangeY) {
        area.setHeight(rangeY * 2);
    }

    public void setAreaSize(float range) {
        setAreaSize(range, range);
    }

    public void setAreaSize(float rangeX, float rangeY) {
        area.getCenter(tmpVec);
        if (rangeX == -1) {
            rangeX = listener.getLevel().getWorldWidth() / 2f;
            tmpVec.x = rangeX;
        }

        if (rangeY == -1) {
            rangeY = listener.getLevel().getWorldHeight() / 2f;
            tmpVec.y = rangeY;
        }

        setAreaRangeX(rangeX);
        setAreaRangeY(rangeY);

        area.setCenter(tmpVec);
    }

    public void resetArea() {
        setAreaRangeX(rangeX);
        setAreaRangeY(rangeY);
        setAreaCenter(owner.getWorldCenter());
    }

    public void setMinMaxProgress(float min, float max) {
        minProgress = min;
        maxProgress = max;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    private void inAreaChanged(boolean inArea) {
        if (inArea) {
            ac.playDynSFX(owner.getDefName() + ":ambient:atmo", true);
        } else {
            ac.stopSound(owner.getDefName() + ":ambient:atmo");
        }
    }

    @Override
    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        super.drawShapeDebug(shapeRenderer);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(area.x, area.y, area.width, area.height);
    }
}