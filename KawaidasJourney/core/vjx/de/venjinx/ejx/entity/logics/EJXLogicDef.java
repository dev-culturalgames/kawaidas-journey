package de.venjinx.ejx.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

public class EJXLogicDef {
    public static final String VJXLogic = "EJXLogic";
    public static final String logic = "logic";

    public static final String LOGIC_NAME = "logic:name";

    private MapProperties properties = new MapProperties();
    private String name = VJXLogic;

    public EJXLogicDef(MapProperties properties) {
        this(properties.get(LOGIC_NAME, VJXLogic, String.class), properties);
    }

    public EJXLogicDef(String name, MapProperties properties) {
        this.properties.putAll(properties);
        this.name = name;
        properties.put(LOGIC_NAME, name);
    }

    public MapProperties getProperties() {
        return properties;
    }

    public String getName() {
        return name;
    }

}