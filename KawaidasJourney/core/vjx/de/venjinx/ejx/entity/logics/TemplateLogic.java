package de.venjinx.ejx.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class TemplateLogic extends EJXLogic {

    // Declare variable names for loading from Tiled properties
    //      Identifier                         Tiled
    // <logicName>_<varName> <-> "<logicNamy>:<varName>"
    public static final String template_strVar = "template:strVar";
    public static final String template_intVar = "template:intVar";
    public static final String template_floatVar = "template:floatVar";
    public static final String template_boolVar = "template:boolVar";

    public enum TemplateProperty implements EJXPropertyDefinition {
        strValue(template_strVar, PropertyType.stringProp, VJXString.STR_EMPTY),
        intValue(template_intVar, PropertyType.intProp, 0),
        floatValue(template_floatVar, PropertyType.floatProp, 0f),
        boolValue(template_boolVar, PropertyType.boolProp, false);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private TemplateProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final TemplateProperty get(String name) {
            for (TemplateProperty property : TemplateProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private String strValue;
    private int intValue;
    private float floatValue;
    private boolean boolValue;

    public TemplateLogic() {
        super(VJXLogicType.TEMPLATE);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(TemplateProperty.values(), vjxProps,
                        properties);

        strValue = vjxProps.get(TemplateProperty.strValue, String.class);
        intValue = vjxProps.get(TemplateProperty.intValue, Integer.class);
        floatValue = vjxProps.get(TemplateProperty.floatValue, Float.class);
        boolValue = vjxProps.get(TemplateProperty.boolValue, boolean.class);
    }

    @Override
    public String toString() {
        String string = VJXLogicType.TEMPLATE.getName() + ":\n";
        string += "    strValue: " + strValue;
        string += "    intValue: " + intValue;
        string += "    floatValue: " + floatValue;
        string += "    boolValue: " + boolValue;
        return string;
    }
}