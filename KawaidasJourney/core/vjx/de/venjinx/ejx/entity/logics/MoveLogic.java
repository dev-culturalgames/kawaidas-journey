package de.venjinx.ejx.entity.logics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.PlayerEntity;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.stages.DebugStage.DebugStats;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.EJXFloat;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXDirection;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class MoveLogic extends EJXLogic implements CollisionListener {
    public static final String move_attachTo = "move:attachTo";
    public static final String move_collider = "move:collider";
    public static final String move_flying = "move:flying";
    public static final String move_gravityScale = "move:gravityScale";
    public static final String move_isBullet = "move:isBullet";
    public static final String move_still = "move:still";
    public static final String move_speedModX = "move:speedModX";
    public static final String move_speedModY = "move:speedModY";
    public static final String move_speedRandomize = "move:speedRandomize";
    public static final String move_speedX = "move:speedX";
    public static final String move_speedY = "move:speedY";
    public static final String move_usePhysics = "move:usePhysics";
    public static final String move_waypoints = "move:waypoints";

    public enum MoveProperty implements EJXPropertyDefinition {
        attachTo(MoveLogic.move_attachTo, PropertyType.stringProp,  VJXString.STR_EMPTY),
        collider(MoveLogic.move_collider, PropertyType.colliderDef, null),
        flying(MoveLogic.move_flying, PropertyType.boolProp, false),
        gravityScale(MoveLogic.move_gravityScale, PropertyType.floatProp, 15f),
        isBullet(MoveLogic.move_isBullet, PropertyType.boolProp, false),
        speedRandomize(MoveLogic.move_speedRandomize, PropertyType.floatProp, 0f),

        speedModX(MoveLogic.move_speedModX, PropertyType.ejxFloat, EJXTypes.newFloat(.5f)),
        speedModY(MoveLogic.move_speedModY, PropertyType.ejxFloat, EJXTypes.newFloat(.5f)),
        speedX(MoveLogic.move_speedX, PropertyType.ejxFloat, EJXTypes.newFloat(1f)),
        speedY(MoveLogic.move_speedY, PropertyType.ejxFloat, EJXTypes.newFloat(1f)),

        still(MoveLogic.move_still, PropertyType.boolProp, false),
        usePhysics(MoveLogic.move_usePhysics, PropertyType.boolProp, false),
        waypoints(MoveLogic.move_waypoints, PropertyType.stringProp, VJXString.STR_EMPTY),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private MoveProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final MoveProperty get(String name) {
            for (MoveProperty property : MoveProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private Vector2 lastSavePosition;
    private Vector2 oldPosition;
    private Vector2[] oldPositions;
    private int currentOldPosIndex = 0;
    private int nextOldPosIndex = 0;

    private Rectangle targetPos;
    private Vector2 moveDir;
    private Vector2 velocity;

    private float deltaVelX;
    private float deltaVelY;

    private boolean flying;
    protected float gravityScale;

    private float speedRandomize;

    private EJXFloat speedX;
    private EJXFloat speedY;
    private EJXFloat speedModX;
    private EJXFloat speedModY;

    private Array<Waypoint> waypoints;

    private float distanceTravelled;
    private float isBlockedTimer = .25f;
    private float isBlockedTime;

    private boolean usePhyiscs;
    private boolean ignoreGravity;
    private boolean isBullet;
    private boolean still;
    private boolean destinationReached;

    private int currentWPIndex = -1;
    Waypoint waypoint;

    private Box2DLogic physicsLogic;
    private Vector2 rayDir;
    private Vector2 tmpVec2;

    private Runnable resumeRun = new Runnable() {
        @Override
        public void run() {
            setIgnoreGravity(false);
        }
    };

    public MoveLogic() {
        super(VJXLogicType.MOVE, MoveProperty.values());
        tmpVec2 = new Vector2();
        rayDir = new Vector2();
        oldPosition = new Vector2();
        oldPositions = new Vector2[32];
        velocity = new Vector2();
        moveDir = new Vector2();
        waypoints = new Array<>();
        targetPos = new Rectangle(0, 0, 1, 1);
        lastSavePosition = new Vector2();
    }

    @Override
    public boolean unstuck() {
        if (owner instanceof PlayerEntity
                        || owner.isActivity(Activity.DISABLED)
                        || owner.isActivity(Activity.EXECUTE)
                        || owner.isActivity(Activity.RISE)
                        || owner.isActivity(Activity.FALL))
            return true;

        unstucking = true;
        if (waypoints.size > 1) {
            setWaypoint(nextWaypoint());
            unstucking = false;
            return true;
        } else {
            returnHome();
            unstucking = false;
            return true;
        }
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(MoveProperty.values(), vjxProps, properties);

        speedX = getProperty(MoveProperty.speedX, EJXFloat.class);
        speedY = getProperty(MoveProperty.speedY, EJXFloat.class);
        speedModX = getProperty(MoveProperty.speedModX, EJXFloat.class);
        speedModY = getProperty(MoveProperty.speedModY, EJXFloat.class);

        if (speedX.value > 0) speedX.value += MathUtils.random(speedRandomize);
        if (speedY.value > 0)
            speedX.value += MathUtils.random(speedRandomize);

        gravityScale = vjxProps.get(MoveProperty.gravityScale, Float.class);
        speedRandomize = vjxProps.get(MoveProperty.speedRandomize, Float.class);
        usePhyiscs = vjxProps.get(MoveProperty.usePhysics, Boolean.class);
        isBullet = vjxProps.get(MoveProperty.isBullet, Boolean.class);
        still = vjxProps.get(MoveProperty.still, Boolean.class);
        flying = vjxProps.get(MoveProperty.flying, Boolean.class);

        Waypoints.loadWaypoints(owner,
                        vjxProps.get(MoveProperty.waypoints, String.class),
                        waypoints);

        if (owner.getPhysics() != null) {
            physicsLogic = owner.getPhysics();
            physicsLogic.setIgnorePassable(flying);
        }

        if (owner.getBody() != null)
            owner.getBody().setSleepingAllowed(false);

        String attachTo = vjxProps.get(MoveProperty.attachTo, String.class);
        if (!attachTo.isEmpty()) {
            EJXEntity e = listener.getEntityByName(attachTo);
            if (e != null) {
                if (owner.getPhysics() != null)
                    owner.getPhysics().attachTo(e);
                else e.getPhysics().attach(owner);
            }
        }

        owner.getLocalCenter(tmpVec);
        setDestination(tmpVec);
        oldPosition.set(tmpVec);
        for (int i = 0; i < oldPositions.length; i++) {
            oldPositions[i] = new Vector2(tmpVec);
        }
    }

    @Override
    public void update(float deltaT) {
        if (still) {
            updateActivity(tmpVec.setZero());
            return;
        }
        if (isGrounded() && owner.isAlive())
            setLastSavePosition();
        switch (owner.getLifecycle()) {
            case DEAD:
                flying = false;
                physicsLogic.setIgnorePassable(false);
                stop();
                checkStep(deltaT);
                break;
            case STILL:
                setDestination(owner.getWorldCenter());
                switch (owner.getActivity()) {
                    case DISABLED:
                    case EXECUTE:
                        break;
                    case IDLE:
                        setIgnoreGravity(false);
                        checkForWaypoint();
                        checkStep(deltaT);
                        break;
                    case FALL:
                    case RISE:
                        break;
                    case FLY:
                        checkForWaypoint();
                        checkStep(deltaT);
                        break;
                    default:
                }
                break;
            case MOVE:
                distanceTravelled = owner.getWorldCenter().dst(oldPosition);
                checkBlocked(deltaT);

                oldPosition.set(owner.getWorldCenter());
                if (oldPositions[currentOldPosIndex]
                                .dst(owner.getLocalCenter(tmpVec)) > 25) {
                    nextOldPosIndex = (currentOldPosIndex + 1) % oldPositions.length;

                    owner.getLocalCenter(oldPositions[nextOldPosIndex]);
                    currentOldPosIndex++;
                    currentOldPosIndex %= oldPositions.length;
                }

                switch (owner.getActivity()) {
                    case DISABLED:
                        setVelocity(0, 0);
                        break;
                    case IDLE:
                        checkStep(deltaT);
                        break;
                    case FALL:
                    case RISE:
                        checkStep(deltaT);
                        break;
                    case FLY:
                    case EXECUTE:
                        checkStep(deltaT);
                        break;
                    case RUN:
                    case WALK:
                        checkStep(deltaT);
                        break;
                    default:
                }
                break;
            default:
        }
    }

    public boolean isStill() {
        return still;
    }

    public void resetGravity() {
        gravityScale = vjxProps.get(MoveProperty.gravityScale, Float.class);
    }

    public void setGravityScale(float scale) {
        gravityScale = scale;
    }

    public float getGravityScale() {
        return gravityScale;
    }

    public void setSpeedX(float speedX) {
        this.speedX.value = speedX;
    }

    public void setSpeedY(float speedY) {
        this.speedY.value = speedY;
    }

    public void setSpeed(Vector2 speed) {
        setSpeed(speed.x, speed.y);
    }

    public void setSpeed(float speed) {
        setSpeed(speed, speed);
    }

    public void setSpeed(float speedX, float speedY) {
        this.speedX.value = speedX;
        this.speedY.value = speedY;
    }

    public float getSpeedX() {
        return speedX.value;
    }

    public float getSpeedY() {
        return speedY.value;
    }

    public void setSpeedModX(float speedModX) {
        this.speedModX.value = speedModX;
    }

    public void setSpeedModY(float speedModY) {
        this.speedModY.value = speedModY;
    }

    public void setSpeedMod(Vector2 speedMod) {
        setSpeedMod(speedMod.x, speedMod.y);
    }

    public void setSpeedMod(float speedModX, float speedModY) {
        setSpeedModX(speedModX);
        setSpeedModY(speedModY);
    }

    public float getSpeedModX() {
        return speedModX.value;
    }

    public float getSpeedModY() {
        return speedModY.value;
    }

    public void setVelocity(Vector2 velocity) {
        setVelocity(velocity.x, velocity.y);
    }

    public void setVelocity(float velX, float velY) {
        velocity.set(velX, velY);
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public boolean applyGravity() {
        return !flying && !isGrounded() && !ignoreGravity;
    }

    public void setIgnoreGravity(boolean ignore) {
        ignoreGravity = ignore;
    }

    public void setDestination(Vector2 destination) {
        setDestination(destination.x, destination.y);
    }

    public void setDestination(float destX, float destY) {
        setDestination(destX, destY, flying || !isGrounded());
    }

    protected void setDestination(float destX, float destY, boolean yAllowed) {
        if (!yAllowed) destY = owner.getWCY();
        tmpVec.set(destX, destY);
        if (owner.getActor().getParent() != null)
            owner.getActor().getParent().stageToLocalCoordinates(tmpVec);

        targetPos.setCenter(tmpVec);

        destinationReached = targetPos.contains(owner.getLCX(), owner.getLCY());

        if (tmpVec.x < owner.getWCX()) owner.setOrientation(Orientation.LEFT);
        else if (tmpVec.x > owner.getWCX()) owner.setOrientation(Orientation.RIGHT);

        if (!destinationReached) updateVelocity(yAllowed);
    }

    public float getDestinationX() {
        return targetPos.x + targetPos.width / 2f;
    }

    public float getDestinationY() {
        return targetPos.y + targetPos.height / 2f;
    }

    public Vector2 getDestination(Vector2 destination) {
        return targetPos.getCenter(destination);
    }

    public void startMove() {
        if (!owner.isAlive()) return;
        if (destinationReached) return;
        owner.setLifecycle(Lifecycle.MOVE);
        updateVelocity();
    }

    public void moveDirection(Vector2 direction, boolean yAllowed) {
        if (!owner.isAlive()) return;
        owner.setLifecycle(Lifecycle.MOVE);
        tmpVec.set(owner.getWorldCenter());
        setDestination(direction.x * 100 + tmpVec.x,
                        direction.y * 100 + tmpVec.y, yAllowed);
        updateVelocity(yAllowed);
    }

    public void returnHome() {
        tmpVec.set(owner.birthPosition).add(owner.getOriginX(),
                        owner.getOriginY());

        setDestination(tmpVec);
        startMove();
    }

    private void updateVelocity() {
        updateVelocity(false);
    }

    private void updateVelocity(boolean yAllowed) {
        targetPos.getCenter(moveDir);
        moveDir.sub(owner.getLocalCenter(tmpVec));
        moveDir.nor();
        if (flying) {
            velocity.x = moveDir.x * speedModX.value * speedX.value;
            velocity.y = moveDir.y * speedModY.value * speedY.value;
            velocity.scl(B2DWorld.WORLD_UNIT);
            return;
        }

        if (isGrounded() && !yAllowed) {
            moveDir.x = Math.signum(moveDir.x);
            moveDir.y = 0;
            velocity.y = 0;
        } else velocity.y = ignoreGravity
                        ? moveDir.y * speedModY.value * speedY.value
                                        * B2DWorld.WORLD_UNIT
                                        : velocity.y;

        velocity.x = moveDir.x * speedModX.value * speedX.value
                        * B2DWorld.WORLD_UNIT;
    }

    public void addWaypoint(Waypoint wp) {
        waypoints.add(wp);
    }

    public void removeCurrentWaypoint() {
        waypoints.removeValue(waypoints.get(currentWPIndex), true);
    }

    public int getWaypointCount() {
        return waypoints.size;
    }

    public void setWaypoint(Waypoint waypoint) {
        //        VJXLogger.log(owner + " setWaypoint " + waypoint);
        //        VJXLogger.log(owner + " setWaypoint", 1);
        VJXLogger.log(LogCategory.EXEC, owner + " set waypoint " + waypoint, 1);

        if (waypoint == null) {
            this.waypoint = null;
            setDestination(owner.getWorldCenter());
            return;
        }

        this.waypoint = waypoint;

        if (waypoint.reachedBy(owner, !flying && isGrounded())) {
            setDestination(owner.getWorldCenter());
            posReached();
            return;
        }

        waypoint.reset();

        if (waypoint.executeOnce() && waypoints.contains(waypoint, true))
            waypoints.removeIndex(currentWPIndex);

        waypoint.getCenter(tmpVec);

        SequenceAction sa = owner.execMultiCmds(waypoint.getOnSetCommands(owner));

        if (sa.getActions().size > 0) owner.setActivity(Activity.EXECUTE);

        if (waypoint.updateActivity()) owner.setActivity(Activity.IDLE);

        setDestination(tmpVec);
        if (!owner.isActivity(Activity.EXECUTE)
                        && !owner.isActivity(Activity.DISABLED))
            startMove();
    }

    public Waypoint nextWaypoint() {
        return nextWaypoint(false);
    }

    public Waypoint nextWaypoint(boolean peek) {
        //        VJXLogger.log(owner + " next waypoint");
        //        VJXLogger.log(owner + " next waypoint", 1);
        if (waypoints.size == 0) {
            currentWPIndex = -1;
            return null;
        }

        if (!peek) {
            currentWPIndex++;
            currentWPIndex = currentWPIndex % waypoints.size;
            Waypoint wp = waypoints.get(currentWPIndex);
            wp.reset();
            return wp;
        } else return waypoints.get((currentWPIndex + 1) % waypoints.size);
    }

    public void insertWPBeforeCurrent(Waypoint wp) {
        wp.reset();
        if (currentWPIndex == -1) waypoints.insert(0, wp);
        waypoints.insert(currentWPIndex % (waypoints.size + 1), wp);
    }

    public void insertWPAfterCurrent(Waypoint wp) {
        wp.reset();
        if (currentWPIndex == -1) waypoints.insert(1, wp);
        waypoints.insert((currentWPIndex + 1) % (waypoints.size + 1), wp);
    }

    public Waypoint getWaypoint() {
        return waypoint;
    }

    protected void checkForWaypoint() {
        //        VJXLogger.log(owner + " checkForWaypoint " + waypoint);
        //        VJXLogger.log(owner + " checkForWaypoint", 1);
        if (waypoint == null) {
            //            VJXLogger.log(owner + " find new wp cause wp null "
            //                          + waypoints.size + " current " + currentWPIndex);
            if (waypoints.size > 0) if (currentWPIndex == -1
                            || waypoints.get(currentWPIndex).isExecuted())
                setWaypoint(nextWaypoint());
            else setWaypoint(waypoints.get(currentWPIndex));
        } else if (!waypoint.isExecuted()) {
            //            VJXLogger.log(owner + " reset current " + waypoint);
            if (!owner.isActivity(Activity.DISABLED)) setWaypoint(waypoint);
        } else {
            //            VJXLogger.log(owner + " check current " + waypoint);
            if (!waypoint.reachedBy(owner, !flying && isGrounded())) return;
            //            VJXLogger.log(owner + " find new wp");
            if (waypoints.size > 0) setWaypoint(nextWaypoint());
        }
    }

    @Override
    public void waypointReached() {
        if (!owner.isAlive()) return;

        //        VJXLogger.log(owner + " " + owner.getWorldCenter()
        //        + " reached waypoint " + waypoint);

        Command[] cmds = waypoint.getOnReachCmds(owner);
        VJXLogger.logIncr(LogCategory.EXEC, owner + " execute waypointReached");
        if (cmds != null && cmds.length > 0) {
            owner.setActivity(Activity.EXECUTE);
            owner.execMultiCmds(cmds);
        }
        waypoint.setExecuted();

        EJXLogic logic;
        Array<EJXLogic> logics = owner.getLogics();
        for (int i = 0; i < logics.size; i++) {
            logic = logics.get(i);
            if (logic != this)
                logic.waypointReached();
        }

        waypoint = null;
        VJXLogger.logDecr(LogCategory.EXEC,
                        owner + " finished waypointReached");
    }

    public float getDeltaVelX() {
        return deltaVelX;
    }

    public float getDeltaVelY() {
        return deltaVelY;
    }

    private void checkStep(float deltaT) {
        if (waypoint != null && owner.isAlive()) updateVelocity();
        long currentTime = System.nanoTime();
        applyPhysics(deltaT);

        deltaVelX = velocity.x * deltaT;
        deltaVelY = velocity.y * deltaT;

        // stop x movement when blocked
        if (!flying && owner.isLifecycle(Lifecycle.MOVE)
                        && !canMoveX(moveDir.x)) {
            unstuck();
            velocity.x = 0;
            deltaVelX = 0;
        }

        if (physicsLogic != null) {
            float tmp = deltaVelY;
            deltaVelY = physicsLogic.checkFloor(deltaVelY);
            if (deltaVelY != tmp) velocity.y = Math.signum(deltaVelY) * 1;
        }

        targetPos.getCenter(tmpVec);
        tmpVec.sub(owner.getWorldCenter());

        boolean equalSign = Math.signum(deltaVelX) == Math.signum(tmpVec.x);
        boolean reachedX = false;
        if (equalSign) {
            reachedX = Math.abs(deltaVelX) >= Math.abs(tmpVec.x)
                            || targetPos.contains(owner.getWCX() + deltaVelX,
                                            targetPos.y);
            if (reachedX) {
                if (!isBullet || flying) {
                    deltaVelX = tmpVec.x;
                    stopH();
                }
            }
        }

        boolean reachedY = !flying && isGrounded();
        if (flying) {
            equalSign = Math.signum(deltaVelY) == Math.signum(tmpVec.y);
            if (equalSign) {
                reachedY = Math.abs(deltaVelY) >= Math.abs(tmpVec.y) || targetPos
                                .contains(targetPos.x, owner.getWCY() + deltaVelY);
                if (reachedY) {
                    if (!isBullet) {
                        deltaVelY = tmpVec.y;
                        stopV();
                    }
                }
            }
        }

        //        VJXLogger.log(owner + " vel: " + velocity + " deltaXY: " + deltaVelX
        //                        + ", " + deltaVelY + " delta: " + deltaT);
        owner.getLocalCenter(tmpVec).add(deltaVelX, deltaVelY);
        owner.getActor().getParent().localToStageCoordinates(tmpVec);
        owner.setCenter(tmpVec);

        if (physicsLogic != null)
            physicsLogic.updateChildren(deltaVelX, deltaVelY);

        if (!destinationReached)
            if (reachedX)
                if (reachedY) {
                    posReached();
                }

        updateActivity(velocity);
        DebugStats.objectsUpdateTime += System.nanoTime() - currentTime;
    }

    private void applyPhysics(float deltaT) {
        if (!usePhyiscs) return;
        float dirX = Math.signum(velocity.x);
        if (applyGravity()) {
            velocity.x += B2DWorld.GRAVITY.x * gravityScale
                            * B2DWorld.WORLD_UNIT * deltaT;
            velocity.y += -B2DWorld.GRAVITY.y
                            * gravityScale * B2DWorld.WORLD_UNIT
                            * deltaT;
        } else {
            if (isGrounded() && velocity.y < 0) velocity.y = 0;
        }

        if (owner.isActivity(Activity.RISE) || owner.isActivity(Activity.FALL)
                        || flying)
            return;

        //        float dirX = Math.signum(velocity.x);
        //        VJXDirection dir = slopeDown ? VJXDirection.LEFT :
        if (owner.isActivity(Activity.EXECUTE) || !isGrounded()) return;
        float slope = physicsLogic.getSlope(dirX);
        //        VJXLogger.log(owner + " runs on ramp " + " sign " + dirX + " slope "
        //                        + slope + " grounded " + isGrounded());
        if (slope != 0) {
            //            VJXLogger.log(owner + " runs on ramp " + " sign " + dirX + " angle "
            //                            + slope + " grounded " + isGrounded());
            if (velocity.x != 0) {
                matchSlopeSpeed(slope, dirX);
            }
        } else {
            slope = physicsLogic.getSlope(dirX, VJXDirection.LEFT);
            //            VJXLogger.log(owner + " check left slope " + slope);
            if (slope < 0) {
                matchSlopeSpeed(slope, dirX);
            } else if (slope > 0) velocity.y = 0;
        }
    }

    private void matchSlopeSpeed(float slope, float dirX) {
        tmpVec.set(1, 0);
        tmpVec.setAngle(slope);
        moveDir.x = dirX * tmpVec.x;
        moveDir.y = tmpVec.y;
        velocity.x = moveDir.x * speedModX.value * speedX.value * B2DWorld.WORLD_UNIT;
        velocity.y = moveDir.y * speedModX.value * speedX.value * B2DWorld.WORLD_UNIT;

        if (!isGrounded() && slope >= 0) {
            velocity.y = 0;
        }
    }

    private void updateActivity(Vector2 velocity) {
        if (owner.isActivity(Activity.EXECUTE)
                        || owner.isActivity(Activity.DISABLED))
            return;

        if (!velocity.isZero()) {
            owner.setLifecycle(Lifecycle.MOVE);
            if (flying) {
                owner.setActivity(Activity.FLY);
            } else {
                if (applyGravity()) {
                    float distance = owner.getPhysics().getDistance(
                                    owner.getWorldCenter(), tmpVec.set(0, -1),
                                    128);
                    if (distance > 128 || distance == -1) {
                        if (velocity.y < 0f) {
                            owner.setActivity(Activity.FALL);
                            physicsLogic.detach();
                        } else if (velocity.y > 0f) {
                            owner.setActivity(Activity.RISE);
                        }
                    }
                } else {
                    if (isGrounded() && velocity.x != 0) {
                        if (speedModX.value > .5f)
                            owner.setActivity(Activity.RUN);
                        else owner.setActivity(Activity.WALK);
                    }
                }
            }
        } else {
            owner.setLifecycle(Lifecycle.STILL);
            if (destinationReached) {
                isBlockedTime = 0f;
            }

            if (isGrounded()) owner.setActivity(Activity.IDLE);
        }

        if (owner instanceof AnimatedEntity) {
            AnimatedEntity aObj = (AnimatedEntity) owner;
            aObj.setAnimation(owner.getActivity().name, true);
        }
    }

    public void move(float deltaT) {
        checkStep(deltaT);
    }

    private boolean checkBlocked(float deltaT) {
        boolean blocked = false;
        //        VJXLogger.log(owner + " distance travelled " + distanceTravelled);
        if (distanceTravelled < 1) {
            isBlockedTime += deltaT;
            if (isBlockedTime >= isBlockedTimer) {
                if (!unstucking) {
                    EJXLogic logic;
                    Array<EJXLogic> logics = owner.getLogics();
                    for (int i = 0; i < logics.size; i++) {
                        logic = logics.get(i);
                        if (logic == this) continue;
                        if (!logic.unstucking())
                            if (logic.unstuck()) {
                                isBlockedTime = 0;
                                unstucking = false;
                                return true;
                            }
                        unstucking = logic.unstucking();
                        if (unstucking) break;
                    }
                    if (!unstucking) unstuck();
                }
            }
        } else {
            isBlockedTime = 0;
        }
        return blocked;
    }

    private void posReached() {
        //        VJXLogger.log(owner + " reached pos " + waypoint);
        //        VJXLogger.log(owner + " " + owner.getWorldCenter() + " reached pos "
        //                        + velocity);
        destinationReached = true;
        owner.positionReached();
        if (waypoint == null) {
            stop();
            return;
        }
        if (waypoint.reachedBy(owner, !flying && isGrounded())) {
            Waypoint wp = nextWaypoint(true);
            boolean stop = wp == null || wp == waypoint;

            if (stop) {
                stop(stop);
                waypointReached();
                return;
            }

            if (waypoint.hasOnReachCMDs()) {
                stop(true);
                waypointReached();
                return;
            }

            stop();
            waypointReached();
            setWaypoint(nextWaypoint());
        } else setWaypoint(waypoint);
    }

    public void stop() {
        stop(false);
    }

    public void stop(boolean updateActivity) {
        if (!applyGravity() || !usePhyiscs) stop(true, true);
        else stop(true, false);

        if (updateActivity) updateActivity(velocity);
    }

    private void stop(boolean horizontal, boolean vertical) {
        if (horizontal) stopH();
        if (vertical) stopV();
    }

    private void stopH() {
        velocity.x = 0;
        moveDir.x = 0;
    }

    private void stopV() {
        velocity.y = 0;
        moveDir.y = 0;
    }

    public boolean isFlying() {
        return flying;
    }

    public boolean isMoving() {
        return owner.isLifecycle(Lifecycle.MOVE);
    }

    public boolean isGrounded() {
        if (physicsLogic != null)
            return !usePhyiscs || physicsLogic.isGrounded();
        else return !flying;
    }

    private boolean checkMoveDir(VJXDirection dir) {
        if (physicsLogic == null) return true;

        Fixture body = physicsLogic.getBodyFixture();
        float rayLength = 128;
        float step = 0;
        float scanAngle = 135f;
        float angle = velocity.angle() - scanAngle / 2f;
        int precision = 8;

        if (precision >= 1) step = scanAngle / precision;

        Vector2 shapePos = ((CircleShape) body.getShape()).getPosition();
        shapePos.scl(B2DWorld.B2D_TO_WORLD).add(owner.getWorldPosition());

        rayDir.set(1, 0);

        boolean canMove = true;
        int i = 0;
        while (i < 9) {
            rayDir.setAngle(angle);
            tmpVec2.set(rayDir);
            canMove &= owner.getPhysics().canMove(shapePos, tmpVec2, rayLength);
            angle += step;
            i++;
        }
        return canMove;
    }

    private boolean canMoveX(float dirX) {
        if (dirX == 0) return false;

        VJXDirection vDir = dirX > 0 ? VJXDirection.RIGHT : VJXDirection.LEFT;
        if (physicsLogic != null) if (!physicsLogic.touchesB2DBit(B2DBit.SOLID,
                        vDir)) { return true; }

        if (checkMoveDir(vDir)) return true;

        return false;
    }

    public void setLastSavePosition() {
        lastSavePosition.set(owner.getWorldPosition());
    }

    public Vector2 getLastSavePosition() {
        return lastSavePosition;
    }

    @Override
    public SequenceAction interrupt(float delayResume) {
        stop();
        setIgnoreGravity(true);

        SequenceAction sa = super.interrupt(delayResume);
        if (delayResume > 0) sa.addAction(Actions.delay(delayResume));
        sa.addAction(Actions.run(resumeRun));
        return sa;
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (thisFix == physicsLogic.getBodyFixture() || flying || !isGrounded()
                        || physicsLogic.isGhost(otherFix)
                        || owner.isActivity(Activity.RISE))
            return false;

        short otherCat = otherFix.getFilterData().categoryBits;
        if (B2DBit.GROUND.isCategory(otherCat)) {
            if (velocity.x != 0) {
                float dirX = Math.signum(velocity.x);
                float slope = physicsLogic.getSlope(dirX, VJXDirection.RIGHT);
                if (slope != 0) {
                    //                    VJXLogger.log(thisFix.getUserData() + " begin "
                    //                                    + otherFix.getUserData());
                    //                    matchSlopeSpeed(slope, dirX);
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (thisFix == physicsLogic.getBodyFixture() || flying
                        || physicsLogic.isGhost(otherFix)
                        || owner.isActivity(Activity.RISE))
            return false;

        short otherCat = otherFix.getFilterData().categoryBits;
        if (B2DBit.GROUND.isCategory(otherCat)) {
            if (velocity.x != 0) {
                float dirX = Math.signum(velocity.x);
                float slope = physicsLogic.getSlope(dirX, VJXDirection.LEFT);
                if (slope != 0) {
                    matchSlopeSpeed(slope, dirX);
                }

                if (!owner.isActivity(Activity.WALK)
                                && !owner.isActivity(Activity.RUN))
                    return false;
                slope = physicsLogic.getSlope(dirX, VJXDirection.RIGHT);
                //                VJXLogger.log(thisFix.getUserData() + " end "
                //                                + otherFix.getUserData());
                if (slope == 0) {
                    owner.setCenter(owner.getWCX(), oldPosition.y);
                    //                    owner.setPosition(owner.getX(), oldPosition.y);
                    matchSlopeSpeed(slope, dirX);
                    velocity.y = 0;
                }
            }
        }
        return false;
    }

    @Override
    public void drawBatchDebug(Batch batch, BitmapFont font) {
        tmpVec.set(owner.getWorldPosition());
        font.draw(batch, "velocity: " + velocity.x + "(" + deltaVelX + "), "
                        + velocity.y + "(" + deltaVelY + ")", tmpVec.x + 5,
                        tmpVec.y + 50);
    }

    @Override
    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        // draw old positions
        shapeRenderer.setColor(Color.TEAL);
        int count = oldPositions.length;
        for (int i = 0; i < count; i++) {
            tmpVec.set(oldPositions[i]);
            shapeRenderer.circle(tmpVec.x, tmpVec.y, 6);

            if (i == currentOldPosIndex) {
                shapeRenderer.line(tmpVec.x, tmpVec.y, owner.getLCX(),
                                owner.getLCY());
            } else {
                shapeRenderer.line(tmpVec, oldPositions[(i + 1) % count]);
            }
        }

        if (waypoint != null) {
            // draw waypoint destination
            shapeRenderer.setColor(Color.TEAL);
            shapeRenderer.line(owner.getLocalCenter(tmpVec),
                            waypoint.getCenter(tmpVec2));

            // draw waypoint position
            shapeRenderer.rect(waypoint.getX(), waypoint.getY(), 0, 0,
                            waypoint.getCenterRect().width,
                            waypoint.getCenterRect().height, 1, 1, 0);
        }

        // draw target destination dir
        shapeRenderer.setColor(Color.YELLOW);
        shapeRenderer.line(owner.getLocalCenter(tmpVec),
                        targetPos.getCenter(tmpVec2));

        // draw target position
        shapeRenderer.rect(targetPos.x, targetPos.y, 0, 0, targetPos.width,
                        targetPos.height, 1, 1, 0);

        // draw move direction
        shapeRenderer.setColor(Color.ORANGE);
        tmpVec.set(moveDir).scl(50);
        shapeRenderer.line(owner.getLocalCenter(tmpVec2), tmpVec.add(tmpVec2));

        // draw move velocity
        shapeRenderer.setColor(Color.GREEN);
        tmpVec.set(velocity).scl(1f / B2DWorld.WORLD_UNIT);
        shapeRenderer.line(owner.getLocalCenter(tmpVec2), tmpVec.add(tmpVec2));

        if (isGrounded()) {
            shapeRenderer.set(ShapeType.Filled);

            owner.getLocalCenter(tmpVec);
            shapeRenderer.circle(tmpVec.x, tmpVec.y, 5);
            shapeRenderer.set(ShapeType.Line);
        }
    }
}