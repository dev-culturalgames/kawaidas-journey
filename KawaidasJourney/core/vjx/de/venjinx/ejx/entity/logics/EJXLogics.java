package de.venjinx.ejx.entity.logics;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;

import com.badlogic.gdx.math.Vector2;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class EJXLogics {

    public static class LogicComparator implements Comparator<EJXLogicType> {

        @Override
        public int compare(EJXLogicType o1, EJXLogicType o2) {
            return o1.getName().compareTo(o2.getName());
        }

    }

    public interface EJXLogicType {
        public String getName();
    }

    public enum VJXLogicType implements EJXLogicType {
        TEMPLATE("template"),
        ACTOR("actor"), AMBIENT("ambient"), BOX2D("box2d"),
        COMMAND("command"), END_SPAWN("endSpawn"), HINT("hint"),
        LIGHT("light"), MOVE("move"), SPAWN("spawn"),
        ;

        public final String name;

        private VJXLogicType(String name) {
            this.name = name;
        }

        public final EJXLogicType get(String name) {
            for (VJXLogicType logic : VJXLogicType.values())
                if (logic.name.equals(name))
                    return logic;
            VJXLogger.log(LogCategory.WARNING, "Logic '" + name + "' not found!");
            return null;
        }

        @Override
        public String getName() {
            return name;
        }
    }

    private static HashMap<EJXLogicType, Class<? extends EJXLogic>> logics = new HashMap<>();

    public static void registerLogic(EJXLogicType type, Class<? extends EJXLogic> clazz) {
        logics.put(type, clazz);
    }

    public static EJXLogicType getType(String name) {
        for (EJXLogicType type : logics.keySet()) {
            if (type.getName().equals(name)) return type;
        }
        return null;
    }

    public static EJXLogic newLogic(EJXLogicType name, EJXEntity owner) {
        if (!logics.containsKey(name)) return null;
        try {
            EJXLogic logic = logics.get(name).newInstance();
            logic.setOwner(owner);
            return logic;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Collection<EJXLogicType> getLogicTypes() {
        return logics.keySet();
    }

    public static void setAlign(String align, Vector2 pos, EJXEntity entity) {
        String[] tmp = align.split(VJXString.SEP_LIST);
        final String align0 = tmp[0];
        switch (align0) {
            case VJXString.ALIGN_BOTTOM:
                pos.y -= entity.getScaledHeight() / 2f * entity.getScaleY();
                break;
            case VJXString.ALIGN_RIGHT:
                pos.x += entity.getScaledWidth() / 2f * entity.getScaleX();
                break;
            case VJXString.ALIGN_TOP:
                pos.y += entity.getScaledHeight() / 2f * entity.getScaleY();
                break;
            case VJXString.ALIGN_LEFT:
                pos.x -= entity.getScaledWidth() / 2f * entity.getScaleX();
                break;
            default:
                break;
        }

        final String align1;
        if (tmp.length > 1) {
            align1 = tmp[1];
        } else align1 = VJXString.STR_EMPTY;

        switch (align1) {
            case VJXString.ALIGN_BOTTOM:
                pos.y -= entity.getScaledHeight() / 2f * entity.getScaleY();
                break;
            case VJXString.ALIGN_RIGHT:
                pos.x += entity.getScaledWidth() / 2f * entity.getScaleX();
                break;
            case VJXString.ALIGN_TOP:
                pos.y += entity.getScaledHeight() / 2f * entity.getScaleY();
                break;
            case VJXString.ALIGN_LEFT:
                pos.x -= entity.getScaledWidth() / 2f * entity.getScaleX();
                break;
            default:
                break;
        }
    }

    public static void setAlign(String align, Vector2 pos, float w, float h,
                    float sclX, float sclY) {
        String[] tmp = align.split(VJXString.SEP_LIST);

        final String align0 = tmp[0];
        switch (align0) {
            case VJXString.ALIGN_CENTER:
                pos.x = w / 2f * sclX;
                pos.y = h / 2f * sclY;
                //                break;
                //            case VJXString.ALIGN_BOTTOM:
                //                pos.y = -(h / 2f * sclY);
                break;
            case VJXString.ALIGN_RIGHT:
                pos.x = w * sclX;
                break;
            case VJXString.ALIGN_TOP:
                pos.y = h * sclY;
                break;
                //            case VJXString.ALIGN_LEFT:
                //                pos.x = -(w / 2f * sclX);
                //                break;
            default:
                break;
        }

        final String align1;
        if (tmp.length > 1) {
            align1 = tmp[1];
        } else align1 = VJXString.STR_EMPTY;

        switch (align1) {
            case VJXString.ALIGN_CENTER:
                pos.x = w / 2f * sclX;
                pos.y = h / 2f * sclY;
                break;
                //            case VJXString.ALIGN_BOTTOM:
                //                pos.y = -(h / 2f * sclY);
                //                break;
            case VJXString.ALIGN_RIGHT:
                pos.x = w * sclX;
                break;
            case VJXString.ALIGN_TOP:
                pos.y = h * sclY;
                break;
                //            case VJXString.ALIGN_LEFT:
                //                pos.x = -(w / 2f * sclX);
                //                break;
            default:
                break;
        }
    }
}