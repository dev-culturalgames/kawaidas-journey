package de.venjinx.ejx.entity.logics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.CollisionDef;
import de.venjinx.ejx.entity.CollisionDef.CollShape;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EJXGroup;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXDirection;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXData;

public class Box2DLogic extends EJXLogic implements CollisionListener {
    public static final String box2d_category = "box2d:category";
    public static final String box2d_mask = "box2d:mask";
    public static final String box2d_friction = "box2d:friction";
    public static final String box2d_restitution = "box2d:restitution";
    public static final String box2d_isSensor = "box2d:isSensor";

    public static final String box2d_angularDampening = "box2d:angularDampening";
    public static final String box2d_angularVelocity = "box2d:angularVelocity";
    public static final String box2d_bodyType = "box2d:bodyType";
    public static final String box2d_bullet = "box2d:bullet";
    public static final String box2d_collider = "box2d:collider";
    public static final String box2d_fixedRotation = "box2d:fixedRotation";
    public static final String box2d_gravityScale = "box2d:gravityScale";
    public static final String box2d_linearDampening = "box2d:linearDampening";
    public static final String box2d_linearVelocityX = "box2d:linearVelocityX";
    public static final String box2d_linearVelocityY = "box2d:linearVelocityY";
    public static final String box2d_mass = "box2d:mass";
    public static final String box2d_rotationalInertia = "box2d:rotationalInertia";
    public static final String box2d_sleepAllowed = "box2d:sleepAllowed";
    public static final String box2d_transitionAllowed = "box2d:transitionAllowed";

    public enum Box2DProperty implements EJXPropertyDefinition {
        // Fixture properties
        category(Box2DLogic.box2d_category, PropertyType.stringProp, B2DBit.NONE.name),
        mask(Box2DLogic.box2d_mask, PropertyType.stringProp, B2DBit.NONE.name),
        friction(Box2DLogic.box2d_friction, PropertyType.floatProp, 0f),
        restitution(Box2DLogic.box2d_restitution, PropertyType.floatProp, 0f),
        isSensor(Box2DLogic.box2d_isSensor, PropertyType.boolProp, false),

        // logic properties
        angularDampening(Box2DLogic.box2d_angularDampening, PropertyType.floatProp, 0f),
        angularVelocity(Box2DLogic.box2d_angularVelocity, PropertyType.floatProp, 0f),
        bodyType(Box2DLogic.box2d_bodyType, PropertyType.physicsType, BodyType.StaticBody),
        bullet(Box2DLogic.box2d_bullet, PropertyType.boolProp, false),
        collider(Box2DLogic.box2d_collider, PropertyType.colliderDef, null),
        fixedRotation(Box2DLogic.box2d_fixedRotation, PropertyType.boolProp, false),
        gravityScale(Box2DLogic.box2d_gravityScale, PropertyType.floatProp, 0f),
        linearDampening(Box2DLogic.box2d_linearDampening, PropertyType.floatProp, 0f),
        linearVelocityX(Box2DLogic.box2d_linearVelocityX, PropertyType.floatProp, 0f),
        linearVelocityY(Box2DLogic.box2d_linearVelocityY, PropertyType.floatProp, 0f),
        mass(Box2DLogic.box2d_mass, PropertyType.floatProp, 0f),
        rotationalInertia(Box2DLogic.box2d_rotationalInertia, PropertyType.floatProp, 0f),
        sleepAllowed(Box2DLogic.box2d_sleepAllowed, PropertyType.boolProp, true),
        transitionAllowed(Box2DLogic.box2d_transitionAllowed, PropertyType.boolProp, false)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private Box2DProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final Box2DProperty get(String name) {
            for (Box2DProperty property : Box2DProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private RayCastCallback checkFloorCB = new RayCastCallback() {

        private float minFraction = Float.MAX_VALUE;

        @Override
        public float reportRayFixture(Fixture fixture, Vector2 point,
                        Vector2 normal, float fraction) {
            if (isGhost(fixture))
                return 1;

            short fixtureCat = fixture.getFilterData().categoryBits;
            if ((fixtureCat & targetBit) > 0) {
                if (targetFixture == null) {
                    if (hitFixture == null || fraction < minFraction) {
                        hitFixture = fixture;
                        minFraction = fraction;

                        rayHitPosition.set(point);
                        rayHitNormal.set(normal);
                        rayHitNormalAngle = normal.angle();
                        //                        return 0;
                    }
                } else {
                    if (targetFixture == fixture) {
                        hitFixture = fixture;

                        rayHitPosition.set(point);
                        rayHitNormal.set(normal);
                        rayHitNormalAngle = normal.angle();
                        return 0;
                    }
                }
                //                    VJXLogger.log(owner + " hit fixture "
                //                                    + fixture.getUserData() + " fraction "
                //                                    + fraction + " hit position " + rayHit
                //                                    + " normal " + rayHitNormal + " angle "
                //                                    + rayHitNormalAngle);
            }
            return 1;
        }
    };

    private RayCastCallback canPassCB = new RayCastCallback() {

        @Override
        public float reportRayFixture(Fixture fixture, Vector2 point,
                        Vector2 normal, float fraction) {

            if (fixture == targetFixture
                            && (!owner.isActivity(Activity.EXECUTE) || !owner
                                            .isActivity(Activity.FALL))) {
                //                VJXLogger.log(owner + " target " + targetFixture.getUserData()
                //                              + " hit " + fixture.getUserData() + " fraction "
                //                              + fraction);
                rayHitPosition.set(point).scl(B2DWorld.B2D_TO_WORLD);
                rayHitNormal.set(normal);

                normal.x = Math.abs(normal.x);
                normal.y = Math.abs(normal.y);
                rayHitNormalAngle = Math.round(normal.angle());
                //                VJXLogger.log(owner + " legal hit " + fixture.getUserData()
                //                + " normal " + rayHitNormal + " angle "
                //                + rayHitNormalAngle + " fraction " + fraction);
                return 0;
            }
            return 1;
        }
    };

    private Body body;
    private HashMap<B2DBit, int[]> sideCollider;
    private HashMap<Fixture, Array<Fixture>> sideContacts = new HashMap<>();
    private Array<Fixture> ghostFixtures = new Array<>();
    private Array<Fixture> contactFixtures = new Array<>();
    private Fixture bodyFixture;
    private Fixture sensorDown;
    private Fixture sensorRight;
    private Fixture sensorUp;
    private Fixture sensorLeft;
    private VJXData bodyFixtureData;

    private BodyType bodyType;
    private float angularDamping;
    private float angularVelocity;
    private boolean bullet;
    private boolean fixedRotation;
    private float gravityScale;
    private float linearDamping;
    private float linearVelocityX;
    private float linearVelocityY;
    private float mass;
    private float rotatationalInertia;
    private boolean sleepAllowed;
    private boolean transitionAllowed;
    private boolean isGhost;

    private Matrix3 localOffTrans;
    private Matrix3 rotationTrans;

    // hit data
    private short targetBit;
    private Fixture targetFixture;
    private Vector2 rayStart;
    private Vector2 rayEnd;
    private Fixture hitFixture;
    private Vector2 rayHitPosition;
    private Vector2 rayHitNormal;
    private float rayHitNormalAngle = 0;

    private Vector2 tmpVec2 = new Vector2();
    private Vector2 rayDir = new Vector2();
    private Vector2[] rayStarts;
    private Vector2[] rayEnds;
    private int rayPrecision = 32;

    private EJXEntity attachedTo;
    private HashSet<EJXEntity> attachedChildren;
    private Vector2 attachedOffset;

    private boolean ignorePassable;

    public Box2DLogic() {
        super(VJXLogicType.BOX2D);
        localOffTrans = new Matrix3();
        rotationTrans = new Matrix3();
        rayHitNormal = new Vector2();
        rayStart = new Vector2();
        rayEnd = new Vector2();
        rayHitPosition = new Vector2();
        attachedChildren = new HashSet<>();
        attachedOffset = new Vector2();

        rayStarts = new Vector2[rayPrecision];
        rayEnds = new Vector2[rayPrecision];
        for (int i = 0; i < rayPrecision; i++) {
            rayStarts[i] = new Vector2();
            rayEnds[i] = new Vector2();
        }
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(Box2DProperty.values(), vjxProps, properties);
        //        VJXLogger.printMapProperties(properties);
        angularDamping = vjxProps.get(Box2DProperty.angularDampening, Float.class);
        angularVelocity = vjxProps.get(Box2DProperty.angularVelocity, Float.class);
        bodyType = vjxProps.get(Box2DProperty.bodyType, BodyType.class);
        bullet = vjxProps.get(Box2DProperty.bullet, boolean.class);
        fixedRotation = vjxProps.get(Box2DProperty.fixedRotation, boolean.class);
        gravityScale = vjxProps.get(Box2DProperty.gravityScale, Float.class);
        linearDamping = vjxProps.get(Box2DProperty.linearDampening, Float.class);
        linearVelocityX = vjxProps.get(Box2DProperty.linearVelocityX, Float.class);
        linearVelocityY = vjxProps.get(Box2DProperty.linearVelocityY, Float.class);
        mass = vjxProps.get(Box2DProperty.mass, Float.class);
        rotatationalInertia = vjxProps.get(Box2DProperty.rotationalInertia, Float.class);
        sleepAllowed = vjxProps.get(Box2DProperty.sleepAllowed, boolean.class);
        transitionAllowed = vjxProps.get(Box2DProperty.transitionAllowed, boolean.class);

        sideCollider = new HashMap<>();
        addSideCollider(B2DBit.SOLID);
        addSideCollider(B2DBit.PASSABLE);
    }

    @Override
    public void update(float deltaT) {
    }

    @Override
    public void onDie() {
        if (body == null) return;
        Gdx.app.postRunnable(new Runnable() {

            @Override
            public void run() {
                Array<Fixture> fixs = body.getFixtureList();
                Fixture f;
                for (int i = 0; i < fixs.size; i++) {
                    f = fixs.get(i);
                    VJXData data = (VJXData) f.getUserData();
                    if (!data.getName().contains("box2d")) {
                        body.destroyFixture(f);
                        i--;
                    }
                }
            }
        });
    }

    @Override
    public void onDespawn() {
        detach();
    }

    public Body createBody(World world) {
        if (world == null) return null;
        body = world.createBody(new BodyDef());

        body.setAngularDamping(angularDamping);
        body.setAngularVelocity(angularVelocity);
        body.setBullet(bullet);
        body.setFixedRotation(fixedRotation);
        body.setGravityScale(gravityScale);
        body.setLinearDamping(linearDamping);
        body.setLinearVelocity(linearVelocityX, linearVelocityY);
        body.setSleepingAllowed(sleepAllowed);
        body.setType(bodyType);

        MassData mData = new MassData();
        mData.I = rotatationalInertia;
        mData.mass = mass;
        body.setMassData(mData);

        body.setUserData(new VJXData(owner.getDefName(), owner));
        body.setTransform(owner.getBirthPlace().x * B2DWorld.WORLD_TO_B2D,
                        owner.getBirthPlace().y * B2DWorld.WORLD_TO_B2D, 0);
        return body;
    }

    public void destroyBody() {
        if (body != null) {
            body.getWorld().destroyBody(body);
            body = null;
        }
    }

    public Body getBody() {
        return body;
    }

    public void setBodyPosition(float x, float y) {
        body.setTransform(x * B2DWorld.WORLD_TO_B2D, y * B2DWorld.WORLD_TO_B2D,
                        body.getAngle());
    }

    public float getFeetY() {
        return getBodyHeight() / 2f;
    }

    public Fixture getClosesHit(Vector2 start, Vector2 dir, float length) {
        World world = listener.getLevel().getB2DWorld();
        clearHitData();
        initRay(start, dir, length);
        targetBit = B2DBit.ALL.id;
        world.rayCast(checkFloorCB, rayStart, rayEnd);

        return hitFixture;
    }

    public float getDistance(Vector2 start, Vector2 dir, float length) {
        return getDistance(start, dir, length, B2DBit.GROUND);
    }

    public float getDistance(Vector2 start, VJXDirection dir, float length) {
        return getDistance(start, dir.direction, length, B2DBit.GROUND);
    }

    public float getDistance(Vector2 start, Vector2 dir, float length, B2DBit targetBit) {
        return getDistance(start, dir, length, targetBit.id);
    }

    public float getDistance(Vector2 start, VJXDirection dir, float length, B2DBit targetBit) {
        return getDistance(start, dir.direction, length, targetBit.id);
    }

    public float getDistance(Vector2 start, VJXDirection dir, float length, short targetMask) {
        return getDistance(start, dir.direction, length, targetMask);
    }

    public float getDistance(Vector2 start, Vector2 dir, float length, short targetMask) {
        World world = listener.getLevel().getB2DWorld();
        clearHitData();
        initRay(start, dir, length);
        targetBit = targetMask;
        world.rayCast(checkFloorCB, rayStart, rayEnd);
        if (rayHitPosition.isZero()) return -1;

        rayHitPosition.scl(B2DWorld.B2D_TO_WORLD);
        tmpVec.set(rayHitPosition).sub(start);
        //        rayHitPosition.sub(start);
        return tmpVec.len();
    }



    public float getAngle(Vector2 start, Vector2 dir, float length,
                    Fixture fixture) {
        World world = listener.getLevel().getB2DWorld();
        clearHitData();
        initRay(start, dir, length);
        targetFixture = fixture;
        targetBit = B2DBit.GROUND.id;
        world.rayCast(checkFloorCB, rayStart, rayEnd);

        return rayHitNormalAngle;
    }

    private boolean canPass(VJXDirection dir, Fixture targetFix) {
        float rayLength = 128f;
        float step = 0;
        float scanAngle = 135f;
        float angle = dir.direction.angle() - scanAngle / 2f;
        int precision = rayStarts.length - 1;

        if (precision >= 1) step = scanAngle / precision;

        Vector2 shapePos = ((CircleShape) bodyFixture.getShape()).getPosition();
        shapePos.scl(B2DWorld.B2D_TO_WORLD).add(owner.getWorldPosition());

        boolean canPass = true;
        int i = 0;
        rayDir.set(1, 0);
        while (i < rayStarts.length) {
            rayStarts[i].set(shapePos);
            rayDir.setAngle(angle);
            canPass &= canPass(rayStarts[i], rayDir, rayLength, targetFix);

            if (rayHitPosition.isZero()) {
                rayEnds[i].set(rayStarts[i].add(rayDir));
            } else rayEnds[i].set(rayHitPosition);
            angle += step;
            i++;
        }
        return canPass;
    }

    private boolean canPass(Vector2 start, Vector2 dir, float length,
                    Fixture targetFix) {
        World world = listener.getLevel().getB2DWorld();
        clearHitData();
        initRay(start, dir, length);
        targetFixture = targetFix;
        world.rayCast(canPassCB, rayStart, rayEnd);
        boolean canPass = rayHitNormal.y < 0 || rayHitNormalAngle < 0
                        || rayHitNormalAngle >= 0 && rayHitNormalAngle < 45;
                        return canPass;
    }

    public boolean canMove(Vector2 start, Vector2 dir, float length) {
        World world = listener.getLevel().getB2DWorld();
        clearHitData();
        initRay(start, dir, length);
        targetBit = B2DBit.GROUND.id;
        world.rayCast(checkFloorCB, rayStart, rayEnd);
        rayHitNormal.x = Math.abs(rayHitNormal.x);
        rayHitNormal.y = Math.abs(rayHitNormal.y);
        return hitFixture == null || rayHitNormal.angle() >= 45;
    }

    public float checkFloor(float deltaVelY) {
        if (deltaVelY > -32) return deltaVelY;
        if (bodyFixture != null) {
            Vector2 shapePos = ((CircleShape) bodyFixture.getShape())
                            .getPosition();
            shapePos.y -= B2DWorld.getFixtureHeight(bodyFixture);
            shapePos.scl(B2DWorld.B2D_TO_WORLD).add(owner.getWorldPosition());

            short targetMask = B2DBit.GROUND.id;
            targetMask |= B2DBit.HITBOX.id;
            float groundDistance = getDistance(shapePos,
                            VJXDirection.DOWN.direction, 1000f, targetMask);
            if (groundDistance == -1) return deltaVelY;
            if (groundDistance <= Math.abs(deltaVelY)) {
                deltaVelY = -groundDistance;
            }
        }
        return deltaVelY;
    }

    public float getSlope(float directionX) {
        return getSlope(directionX, VJXDirection.DOWN);
    }

    public float getSlope(float directionX, VJXDirection dir) {
        if (bodyFixture == null) return 0f;
        CircleShape bodyShape = (CircleShape) bodyFixture.getShape();
        Vector2 shapePos = bodyShape.getPosition();
        shapePos.scl(B2DWorld.B2D_TO_WORLD).add(owner.getWorldPosition());
        if (dir == VJXDirection.RIGHT)
            shapePos.x += directionX * bodyShape.getRadius() * B2DWorld.B2D_TO_WORLD;
        if (dir == VJXDirection.LEFT) shapePos.x -= directionX
                        * bodyShape.getRadius() * B2DWorld.B2D_TO_WORLD;
        float slope = getAngle(shapePos, tmpVec2.set(0, -1), 256, null);

        if (slope == -1) return 0f;
        slope -= 90;
        slope *= directionX;

        return slope;
    }

    public void setGravityScale(float gs) {
        gravityScale = gs;
        body.setGravityScale(gs);
        //        body.setAwake(true);
    }

    public void setGhost(boolean isGhost) {
        this.isGhost = isGhost;

        if (isGhost) {
            clearSideCollider(B2DBit.PASSABLE);
            contactFixtures.clear();
            ghostFixtures.clear();
        }
    }

    public void setGhost(Fixture otherFix, boolean isGhost) {
        if (isGhost) ghostFixtures.add(otherFix);
        else ghostFixtures.removeValue(otherFix, true);
    }

    public boolean isGhost() {
        return isGhost;
    }

    public boolean isGhost(Fixture otherFix) {
        return ghostFixtures.contains(otherFix, true);
    }

    public void setPhysics(B2DBit bit, String fixtureName) {
        if (body == null) return;
        Array<Fixture> fixs = body.getFixtureList();
        Filter filter;
        VJXData data;
        for (Fixture f : fixs) {
            data = (VJXData) f.getUserData();
            if (data.getName().contains(fixtureName)) {
                filter = f.getFilterData();
                filter.maskBits |= bit.id;
                f.setFilterData(filter);
            }
        }
    }

    public void removePhysics(B2DBit bit) {
        if (body == null) return;
        Array<Fixture> fixs = body.getFixtureList();
        Filter filter;
        for (Fixture f : fixs) {
            filter = f.getFilterData();
            filter.maskBits &= ~bit.id;
            f.setFilterData(filter);
        }
    }

    public void setIgnorePassable(boolean ignore) {
        ignorePassable = ignore;
        if (ignore) {
            clearSideCollider(B2DBit.PASSABLE);
            for (Fixture f : contactFixtures) {
                short otherCat = f.getFilterData().categoryBits;
                if (B2DBit.PASSABLE.isCategory(otherCat)) {
                    ghostFixtures.add(f);
                }
            }
        }
    }

    public boolean ignorePassable() {
        return ignorePassable;
    }

    public void flipBodyX() {
        flipBody(true, false);
    }

    public void flipBodyY() {
        flipBody(false, true);
    }

    public void flipBody(boolean flipX, boolean flipY) {
        if (body == null) return;

        for (Fixture f : body.getFixtureList()) {
            if (!sideContacts.containsKey(f) && !((VJXData) f.getUserData())
                            .getName()
                            .contains("body")) {
                if (flipX) flipShapeX(f.getShape());
                if (flipY) flipShapeY(f.getShape());
            }
        }
    }

    public void flipShapeX(Shape shape) {
        flipShapeX(shape, owner.getScaledWidth(), owner.getOriginX());
    }

    public void flipShapeX(Shape shape, float width, float pivotX) {
        Vector2[] verts;
        Vector2 v1 = new Vector2();
        Vector2 v2 = new Vector2();
        float xOff = pivotX * B2DWorld.WORLD_TO_B2D;
        if (shape instanceof PolygonShape) {
            PolygonShape ps = (PolygonShape) shape;
            verts = new Vector2[ps.getVertexCount()];

            for (int i = 0; i < ps.getVertexCount(); i++) {
                v1 = new Vector2();
                ps.getVertex(i, v1);

                v1.x -= xOff;
                v1.x = -v1.x;
                v1.x += xOff;

                verts[i] = v1;
            }
            ps.set(verts);
        } else if (shape instanceof CircleShape) {
            CircleShape cs = (CircleShape) shape;

            v1.set(cs.getPosition());

            v1.x -= xOff;
            v1.x = -v1.x;
            v1.x += xOff;

            cs.setPosition(v1);
        } else if (shape instanceof EdgeShape) {
            EdgeShape es = (EdgeShape) shape;
            es.getVertex1(v1);
            es.getVertex2(v2);

            v1.x -= xOff;
            v1.x = -v1.x;
            v1.x += xOff;

            v2.x -= xOff;
            v2.x = -v2.x;
            v2.x += xOff;

            es.set(v1, v2);
        }
    }

    public void flipShapeY(Shape shape) {
        flipShapeY(shape, owner.getScaledHeight(), owner.getOriginY());
    }

    public void flipShapeY(Shape shape, float width, float pivotY) {
        Vector2[] verts;
        Vector2 v1 = new Vector2();
        Vector2 v2 = new Vector2();
        float yOff = pivotY * B2DWorld.WORLD_TO_B2D;
        if (shape instanceof PolygonShape) {
            PolygonShape ps = (PolygonShape) shape;
            verts = new Vector2[ps.getVertexCount()];

            for (int i = 0; i < ps.getVertexCount(); i++) {
                v1 = new Vector2();
                ps.getVertex(i, v1);

                v1.y -= yOff;
                v1.y = -v1.y;
                v1.y += yOff;

                verts[i] = v1;
            }
            ps.set(verts);
        } else if (shape instanceof CircleShape) {
            CircleShape cs = (CircleShape) shape;

            v1.set(cs.getPosition());

            v1.y -= yOff;
            v1.y = -v1.y;
            v1.y += yOff;

            cs.setPosition(v1);
        } else if (shape instanceof EdgeShape) {
            EdgeShape es = (EdgeShape) shape;
            es.getVertex1(v1);
            es.getVertex2(v2);

            v1.y -= yOff;
            v1.y = -v1.y;
            v1.y += yOff;

            v2.y -= yOff;
            v2.y = -v2.y;
            v2.y += yOff;

            es.set(v1, v2);
        }
    }

    public void rotateBody(float rotation) {
        if (body == null) return;
        body.setTransform(body.getPosition(),
                        rotation * MathUtils.degreesToRadians);
        owner.getActor().setRotation(rotation);
    }

    public void rotateShape(Shape shape, float rotation) {
        rotateShape(shape, rotation, owner.getOriginX(), owner.getOriginY());
    }

    public void rotateShape(Shape shape, float rotation, float pivotX, float pivotY) {
        Vector2[] verts;
        Vector2 v1;

        localOffTrans.idt();
        localOffTrans.translate(pivotX * B2DWorld.WORLD_TO_B2D,
                        pivotY * B2DWorld.WORLD_TO_B2D);
        rotationTrans.idt();
        rotationTrans.rotate(rotation);

        //TODO fix polygon rotation
        if (shape instanceof PolygonShape) {
            PolygonShape ps = (PolygonShape) shape;
            verts = new Vector2[ps.getVertexCount()];
            for (int i = 0; i < verts.length; i++) {
                v1 = new Vector2();
                ps.getVertex(i, v1);
                v1.setAngle(EJXUtil.modAngle(rotation, v1.angle()));
                verts[i] = v1;
            }
            ps.set(verts);
        }
    }

    public void attachTo(EJXEntity target) {
        attachTo(target, null);
    }

    public void attachTo(EJXEntity target, Fixture fix) {
        if (target == null) return;
        Box2DLogic boxLogic = target.getPhysics();
        if (boxLogic == null) return;

        if (attachedTo != null) {
            if (attachedTo == target) { return; }

            if (attachedTo.getVelocity().y <= target.getVelocity().y) {
                detach();

                //                VJXLogger.log(owner + " attachTo " + target);
                //                VJXLogger.log(owner + " attach ", 1);
                attachedTo = target;
            }
        } else {
            //            VJXLogger.log(owner + " attachTo " + target);
            //            VJXLogger.log(owner + " attach ", 1);
            attachedTo = target;
        }
        attachedOffset.set(owner.getWorldCenter()).sub(target.getWorldCenter());
        boxLogic.attach(owner);
    }

    public void attach(EJXEntity wObj) {
        if (!attachedChildren.contains(wObj)) {
            //            VJXLogger.log(owner + " attach " + wObj);
            //            VJXLogger.log(owner + " attach ", 1);
            //            VJXLogger.log(owner + " attach ", 2);
            //            VJXLogger.log(owner + " attach ", 3);
            attachedChildren.add(wObj);
        }
    }

    public void detach() {
        if (attachedTo == null) return;
        //        VJXLogger.log(owner + " detach from " + attachedTo);
        //        VJXLogger.log(owner + " detach ", 1);
        //        VJXLogger.log(owner + " detach ", 2);
        //        VJXLogger.log(owner + " detach ", 3);
        //        VJXLogger.log(owner + " detach ", 4);
        Box2DLogic boxLogic = attachedTo.getPhysics();
        if (boxLogic == null) return;
        boxLogic.removeAttached(owner);
        attachedTo = null;
        attachedOffset.setZero();
    }

    private void removeAttached(EJXEntity wObj) {
        attachedChildren.remove(wObj);
    }

    public boolean isAttached() {
        return attachedTo != null;
    }

    public boolean isAttachedTo(EJXEntity entity) {
        return attachedTo != null && attachedTo == entity;
    }

    private void checkAttach(EJXEntity other, short otherCat) {
        if (attachedTo != null) {
            if (allowTransition(other, otherCat)) {
                attachTo(other);
            }
        } else attachTo(other);
    }

    private boolean allowTransition(EJXEntity other, short otherCat) {
        if (attachedTo == null || attachedTo == other) return true;
        if (transitionAllowed) return true;
        if (B2DBit.SOLID.isCategory(otherCat))
            return !B2DBit.DANGER.isCategory(otherCat);

        float dirX = owner.getOrientation() == Orientation.LEFT ? -1 : 1;
        if (getSlope(dirX, VJXDirection.RIGHT) <= 0)
            return true;

        EJXGroup node = (EJXGroup) other.getActor().getParent();
        EJXGroup node2 = (EJXGroup) attachedTo.getActor().getParent();
        if (node == node2 && attachedTo != other) return true;

        if (other.getPhysics() == null) return false;
        Box2DLogic otherPhysics = other.getPhysics();
        return otherPhysics.transitionAllowed;
    }

    public void updateChildren(float velX, float velY) {
        for (EJXEntity child : attachedChildren) {
            tmpVec.set(child.getWorldPosition()).add(velX, velY);
            child.setPosition(tmpVec.x, tmpVec.y, true, true);

            //            if (child.usesPhysics())
            //                child.getBody().setAwake(owner.isLifecycle(Lifecycle.MOVE));
        }
    }

    public HashSet<EJXEntity> getAttachedChildren() {
        return attachedChildren;
    }

    public EJXEntity getAttachedParent() {
        return attachedTo;
    }

    public void setAttachedOffset(Vector2 newOffset) {
        attachedOffset.set(newOffset);
    }

    public Vector2 getAttachedOffset() {
        return attachedOffset;
    }

    public boolean headCollision(B2DBit category) {
        boolean headCollision = touchesB2DBit(category, VJXDirection.UP);
        return headCollision && !owner.getPhysics().isGhost();
    }

    public boolean isGrounded() {
        return isGrounded(B2DBit.GROUND);
    }

    public boolean isGrounded(B2DBit category) {
        boolean isGrounded = touchesB2DBit(category, VJXDirection.DOWN);
        if (isGrounded && owner.isActivity(Activity.RISE)) {
            return false;
        }
        return isGrounded && !owner.getPhysics().isGhost();
    }

    public void clearGrounded() {
        clearGrounded(false);
    }

    public void clearGrounded(boolean clearPassable) {
        clearSideCollider(B2DBit.SOLID);
        if (clearPassable) {
            contactFixtures.clear();
            clearSideCollider(B2DBit.PASSABLE);
        }
    }

    public Fixture getBodyFixture() {
        return bodyFixture;
    }

    public boolean touchesB2DBit(B2DBit bit) {
        boolean touches = false;
        touches |= touchesB2DBit(bit, VJXDirection.DOWN);
        touches |= touchesB2DBit(bit, VJXDirection.RIGHT);
        touches |= touchesB2DBit(bit, VJXDirection.UP);
        touches |= touchesB2DBit(bit, VJXDirection.LEFT);
        return touches;
    }

    public boolean touchesB2DBit(B2DBit bit, VJXDirection dir) {
        return getSideCollider(bit, dir) > 0;
    }

    public float getBodyHeight() {
        if (bodyFixture == null) return 0f;
        return bodyFixtureData.getHeight() * B2DWorld.B2D_TO_WORLD;
    }

    private void addSideCollider(B2DBit bit) {
        if (!sideCollider.containsKey(bit)) {
            int[] array = { 0, 0, 0, 0 };
            sideCollider.put(bit, array);
        }
    }

    public int getSideCollider(B2DBit bit, VJXDirection dir) {
        if (sideCollider.containsKey(bit)) {
            return sideCollider.get(bit)[dir.id];
        }

        int i = -1;
        if (bit == B2DBit.GROUND) {
            i = getSideCollider(B2DBit.SOLID, dir);
            i += getSideCollider(B2DBit.PASSABLE, dir);
        }
        return i;
    }

    private void checkDirColl(short otherBit, boolean begin,
                    int collId) {
        if (collId == -1) return;
        updateSideCollider(begin, collId, otherBit);
    }

    private void updateSideCollider(boolean on, int collId, short otherCat) {
        for (Map.Entry<B2DBit, int[]> entry : sideCollider.entrySet())
            if (entry.getKey().isCategory(otherCat)) {
                int[] array = entry.getValue();
                if (on) array[collId]++;
                else if (array[collId] > 0) array[collId]--;
            }
    }

    private void clearSideColliders() {
        for (B2DBit bit : sideCollider.keySet())
            clearSideCollider(bit);

        for (Array<Fixture> contacts : sideContacts.values()) {
            contacts.clear();
        }

        ghostFixtures.clear();
    }

    private void clearSideCollider(B2DBit bit) {
        if (bit == B2DBit.GROUND) {
            clearSideCollider(B2DBit.SOLID);
            clearSideCollider(B2DBit.PASSABLE);
        } else if (sideCollider.containsKey(bit)) {
            int[] coll = sideCollider.get(bit);
            for (int i = 0; i < coll.length; i++)
                coll[i] = 0;
        }
    }

    public void createBodyFixtures() {
        createBodyFixtures(definition.getValue(DefinitionProperty.collider,
                        CollisionDef.class), 0f, 0f);
    }

    public void createBodyFixtures(CollisionDef collDef, float x, float y) {
        createBodyFixtures(collDef, x, y, owner.getScaledWidth(), owner.getScaledHeight());
    }

    public void createBodyFixtures(CollisionDef collDef, float x, float y,
                    float width, float height) {
        if (collDef == null) return;
        if (!collDef.isEmpty())
            collDef.createColliders(owner, x, y, width, height);
    }

    public Fixture getFixture(VJXData fixtureData) {
        if (fixtureData == null) return null;
        return getFixture(fixtureData.getName());
    }

    public Fixture getFixture(String name) {
        if (body == null) return null;
        Array<Fixture> fixs = body.getFixtureList();
        VJXData data;
        for (Fixture f : fixs) {
            data = (VJXData) f.getUserData();
            if (data.getName().contains(name)) return f;
        }
        return null;
    }

    public void clearFixtures() {
        if (body == null)
            return;
        Array<Fixture> fixs = body.getFixtureList();
        while (fixs.size != 0) {
            body.destroyFixture(fixs.get(0));
        }
        clearSideColliders();
        ghostFixtures.clear();
        sideContacts.clear();
    }

    private void clearHitData() {
        targetFixture = null;
        rayHitNormal.setZero();
        rayHitPosition.setZero();
        rayHitNormalAngle = -1;
        hitFixture = null;
    }

    private void initRay(Vector2 start, Vector2 dir, float length) {
        //        VJXLogger.log(owner + " init ray start " + start + " dir" + dir
        //                        + " length " + length + " end" + rayEnd);
        rayStart.set(start).scl(B2DWorld.WORLD_TO_B2D);
        length *= B2DWorld.WORLD_TO_B2D;
        rayEnd.set(rayStart).add(dir.x * length, dir.y * length);
        //        VJXLogger.log(owner + " init ray " + rayStart + " " + rayEnd);
    }

    @Override
    public void fixturesCreated() {
        CollisionDef cDef = vjxProps.get(Box2DProperty.collider,
                        CollisionDef.class);
        if (cDef == null) return;
        float x = 0, y = 0, w = 0, h = 0;
        CollisionDef objectDef = owner.getDef().getValue(
                        DefinitionProperty.collider, CollisionDef.class);

        for (CollShape s : objectDef.getShapes()) {
            if (s.name.contains("box2d_body")) {
                x = s.x * owner.getScaledWidth();
                y = s.y * owner.getScaledHeight();
                w = s.w * owner.getScaledWidth();
                h = s.h * owner.getScaledHeight();
                cDef.createColliders(owner, x, y, w, h);

                bodyFixture = getFixture("box2d_body");
                bodyFixtureData = (VJXData) bodyFixture.getUserData();

                sensorDown = getFixture("box2d_down");
                sensorRight = getFixture("box2d_right");
                sensorUp = getFixture("box2d_up");
                sensorLeft = getFixture("box2d_left");

                sideContacts.put(sensorDown, new Array<Fixture>());
                sideContacts.put(sensorRight, new Array<Fixture>());
                sideContacts.put(sensorUp, new Array<Fixture>());
                sideContacts.put(sensorLeft, new Array<Fixture>());
            }
        }
    }

    private boolean checkBeginBody(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        boolean accept = false;
        short otherCat = otherFix.getFilterData().categoryBits;
        if (B2DBit.SOLID.isCategory(otherCat)) {
            accept = true;
        }

        if (B2DBit.PASSABLE.isCategory(otherCat)) {
            if (!ignorePassable) {
                if (!isGhost(otherFix)) {
                    if (!allowTransition(other, otherCat)) {
                        setGhost(otherFix, true);
                    } else {
                        if (canPass(VJXDirection.DOWN, otherFix)) {
                            setGhost(otherFix, true);
                        } else {
                            checkDirColl(B2DBit.PASSABLE.id, true, 0);
                            contactFixtures.add(otherFix);
                            accept = true;
                        }
                    }
                } else accept = false;
            } else {
                setGhost(otherFix, true);
                accept = false;
            }
        }
        return accept;
    }

    private boolean checkEndBody(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        boolean accept = false;
        short otherCat = otherFix.getFilterData().categoryBits;
        if (B2DBit.SOLID.isCategory(otherCat)) {
            accept = true;
        }

        if (B2DBit.PASSABLE.isCategory(otherCat)) {
            setGhost(otherFix, false);
            if (!isGhost(otherFix) && contactFixtures.contains(otherFix, true)) {
                checkDirColl(B2DBit.PASSABLE.id, false, 0);
                contactFixtures.removeValue(otherFix, true);
                accept = true;
            }
        }

        return accept;
    }

    private boolean checkBeginSide(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        boolean accept = false;
        VJXData fData0 = (VJXData) thisFix.getUserData();
        int collID = fData0.getCollID();
        short otherCat = otherFix.getFilterData().categoryBits;

        if (B2DBit.SOLID.isCategory(otherCat)
                        && !B2DBit.DANGER.isCategory(otherCat)) {
            checkDirColl(otherCat, true, collID);
            accept = true;
        }

        if (B2DBit.GROUND.isCategory(otherCat)) {
            if (owner == listener.getPlayerEntity()
                            && !other.isActivity(Activity.DISABLED)) {
                if (collID != VJXDirection.DOWN.id) { return accept; }
                checkAttach(other, otherCat);

                float yPos = B2DWorld.getFixtureWorldCenter(otherFix).y;
                yPos += B2DWorld.getFixtureHeight(otherFix) / 2f;
                listener.snapCamToPlatform(yPos);
            }
        }

        return accept;
    }

    private boolean checkEndSide(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        VJXData fData0 = (VJXData) thisFix.getUserData();
        int collID = fData0.getCollID();
        short otherCat = otherFix.getFilterData().categoryBits;

        if (B2DBit.SOLID.isCategory(otherCat)) {
            checkDirColl(otherCat, false, collID);
            return true;
        }

        return false;
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (isGhost()) return false;

        //        VJXLogger.log(thisFix.getUserData() + " begin "
        //                        + otherFix.getUserData());
        short thisCat = thisFix.getFilterData().categoryBits;

        // this object is terrain
        if (B2DBit.GROUND.isCategory(thisCat)) return true;
        if (bodyFixture == thisFix) {
            return checkBeginBody(thisFix, otherFix, contact, other);
        }

        if (sideContacts.containsKey(thisFix)) {
            return checkBeginSide(thisFix, otherFix, contact, other);
        }
        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (isGhost()) return false;

        //        VJXLogger.log(thisFix.getUserData() + " end " + otherFix.getUserData());
        short thisCat = thisFix.getFilterData().categoryBits;

        // this object is terrain
        if (B2DBit.GROUND.isCategory(thisCat)) return true;
        if (bodyFixture == thisFix) {
            return checkEndBody(thisFix, otherFix, contact, other);
        }

        if (sideContacts.containsKey(thisFix)) {
            return checkEndSide(thisFix, otherFix, contact, other);
        }
        return false;
    }

    @Override
    public void drawBatchDebug(Batch batch, BitmapFont font) {
        if (!owner.isOnScreen()) return;
        if (bodyFixture == null) return;

        tmpVec.set(B2DWorld.getFixtureWorldCenter(bodyFixture));
        font.draw(batch, contactFixtures.size + ", " + ghostFixtures.size,
                        tmpVec.x - 12, tmpVec.y + 15);

        int c = getSideCollider(B2DBit.SOLID, VJXDirection.DOWN);
        tmpVec.set(B2DWorld.getFixtureWorldCenter(sensorDown));
        font.draw(batch, "" + c, tmpVec.x - 4, tmpVec.y + 6);

        c = getSideCollider(B2DBit.SOLID, VJXDirection.RIGHT);
        tmpVec.set(B2DWorld.getFixtureWorldCenter(sensorRight));
        font.draw(batch, "" + c, tmpVec.x - 4, tmpVec.y + 6);

        c = getSideCollider(B2DBit.SOLID, VJXDirection.UP);
        tmpVec.set(B2DWorld.getFixtureWorldCenter(sensorUp));
        font.draw(batch, "" + c, tmpVec.x - 4, tmpVec.y + 6);

        c = getSideCollider(B2DBit.SOLID, VJXDirection.LEFT);
        tmpVec.set(B2DWorld.getFixtureWorldCenter(sensorLeft));
        font.draw(batch, "" + c, tmpVec.x - 4, tmpVec.y + 6);
    }

    @Override
    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(Color.BLUE);
        for (int i = 0; i < rayStarts.length; i++) {
            tmpVec.set(rayStarts[i]);
            tmpVec2.set(rayEnds[i]);
            shapeRenderer.line(tmpVec.x, tmpVec.y, tmpVec2.x, tmpVec2.y);
        }
    }
}