package de.venjinx.ejx.entity;

import java.util.HashMap;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;

import de.venjinx.ejx.tiled.tile.EJXAbstractTile;
import de.venjinx.ejx.util.EJXTypes.AnimationType;
import de.venjinx.ejx.util.EJXTypes.Category;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;

public class EntityDef {

    private EJXAbstractTile tile;
    private MapProperties properties;

    private Category category;
    private BodyType b2d_bodyType;
    private AnimationType animation;

    private AnimationStateData stateData;
    private Skeleton skeleton;

    public EntityDef(EJXAbstractTile tile, int defId) {
        this(tile.getProperties());
        this.tile = tile;

        properties.put("defName", tile.getName());
        properties.put("defId", defId);
        properties.put("type", tile.getType());
    }

    public EntityDef(MapProperties properties) {
        this.properties = new MapProperties();

        if (properties != null)
            setFromProps(properties);
    }

    public EntityDef(EntityDef def) {
        properties = new MapProperties();

        if (def != null)
            copy(def);

    }

    public void copy(EntityDef def) {
        setFromProps(def.properties);
        stateData = def.getAnimationStateData();
        skeleton = def.getSkeleton();
    }

    public Category getCategory() {
        return category;
    }

    public BodyType getBodyType() {
        return b2d_bodyType;
    }

    public AnimationType getAnimationType() {
        return animation;
    }

    public void setSkeletonData(SkeletonData data) {
        stateData = new AnimationStateData(data);
        skeleton = new Skeleton(data);
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public AnimationStateData getAnimationStateData() {
        return stateData;
    }

    public void setValue(String name, Object value) {
        properties.put(name, value);
    }

    public int getDefID() {
        return properties.get(DefinitionProperty.defID.name, -1, Integer.class);
    }

    public String getDefName() {
        return properties.get(DefinitionProperty.defName.name, String.class);
    }

    public String getSkeletonName() {
        return properties.get(DefinitionProperty.skeleton.name, String.class);
    }

    public String getAttributes() {
        return properties.get(DefinitionProperty.attributes.name, String.class);
    }

    public boolean isSkin() {
        return properties.get(DefinitionProperty.skin.name, false, Boolean.class);
    }

    public String getSource() {
        return properties.get(SourceProperty.source.name, String.class);
    }

    public String getTileName() {
        return properties.get(SourceProperty.tileName.name, String.class);
    }

    public void getDependencies(
                    HashMap<String, AssetDescriptor<? extends Object>> defs) {
        String name = getDefName();
        // entity animations
        AnimationType animation = getAnimationType();
        String path = getSource();
        if (animation != AnimationType.FRAME) {
            name = getValue(DefinitionProperty.skeleton, name, String.class);

            if (!getValue(SourceProperty.fromSet, Boolean.class))
                defs.put(name + "_anims", new AssetDescriptor<>(
                                path + name + ".atlas", TextureAtlas.class));
        }
    }

    public void setFromProps(MapProperties eProps) {
        setFromProps(eProps, true);
    }

    public void setFromProps(MapProperties eProps, boolean clear) {
        if (clear) properties.clear();
        properties.putAll(eProps);
        setCategory(eProps.get(DefinitionProperty.category.name, "object", String.class));
        setBodyType(eProps.get(DefinitionProperty.body.name, "static", String.class));
        setAnimationType(eProps.get(DefinitionProperty.animation.name, "frame", String.class));
    }

    public void addProps(MapProperties mp) {
        properties.putAll(mp);
    }

    public MapProperties getCustomProperties() {
        return properties;
    }

    private void setCategory(String categoryString) {
        setCategory(Category.get(categoryString));
    }

    private void setCategory(Category category) {
        this.category = category;
    }

    private void setBodyType(String bodyType) {
        switch (bodyType) {
            case "kinematic":
                b2d_bodyType = BodyType.KinematicBody;
                break;
            case "dynamic":
                b2d_bodyType = BodyType.DynamicBody;
                break;
            default:
                b2d_bodyType = BodyType.StaticBody;
                break;
        }
    }

    private void setAnimationType(String type) {
        switch (type) {
            case "spine":
                animation = AnimationType.SPINE;
                break;
            case "fbf":
                animation = AnimationType.FBF;
                break;
            default:
                animation = AnimationType.FRAME;
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T getValue(EJXPropertyDefinition property, Class<T> clazz) {
        return getValue(property, (T) property.getDefault(), clazz);
    }

    public <T> T getValue(EJXPropertyDefinition property, T dflt, Class<T> clazz) {
        T value = properties.get(property.getName(), clazz);
        if (value == null || value.equals(property.getDefault())) return dflt;

        return value;
    }
}