package de.venjinx.ejx.entity;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.VJXDirection;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXData;

public class CollisionDef {

    public enum CollShapeType {
        RECT, CIRC, POLY
    }

    private String name;
    private List<CollShape> shapes;

    public CollisionDef() {
        this(VJXString.collider);
    }

    public CollisionDef(String name) {
        this(name, null);
    }

    public CollisionDef(String name, CollisionDef baseDef) {
        this.name = name;
        shapes = new ArrayList<>();
        if (baseDef != null)
            shapes.addAll(baseDef.getShapes());
    }

    public String getName() {
        return name;
    }

    public boolean isEmpty() {
        return shapes.size() == 0;
    }

    public void add(CollShape shape) {
        shapes.add(shape);
    }

    public int getShapeCount() {
        return shapes.size();
    }

    public List<CollShape> getShapes() {
        return shapes;
    }

    public void createColliders(EJXEntity entity, float x, float y, float w,
                    float h) {
        String defName = entity.getName();
        Fixture f;
        VJXData d;
        for (CollShape shape : shapes) {
            f = loadFixture(entity.getBody(), shape, defName, w, h, 1, 1, x, y);
            d = (VJXData) f.getUserData();
            d.setEntity(entity);

            if (entity.getOrientation() == Orientation.LEFT)
                entity.getPhysics().flipShapeX(f.getShape());
        }

    }

    private Fixture loadFixture(Body body, CollShape shapeDef,
                    String defName, float width, float height, float sX,
                    float sY, float xOffset, float yOffset) {
        FixtureDef fixDef = new FixtureDef();
        Shape shape = shapeDef.parseCollShape(width, height, sX, sY, xOffset,
                        yOffset);
        fixDef.shape = shape;
        fixDef.filter.categoryBits = shapeDef.categoryBits;
        fixDef.filter.maskBits = shapeDef.maskBits;
        fixDef.isSensor = shapeDef.isSensor;
        fixDef.friction = shapeDef.friction;
        fixDef.restitution = shapeDef.restitution;

        Fixture f = body.createFixture(fixDef);
        VJXDirection dir = VJXDirection.get(shapeDef.name);
        int id = -1;
        if (dir != null) id = dir.id;

        String sName = defName + VJXString.SEP_USCORE + name + VJXString.SEP_USCORE
                        + shapeDef.name;
        VJXData d = new VJXData(sName, null, shapeDef.collisionWidth, shapeDef.collisionHeight, id);
        f.setUserData(d);
        return f;
    }

    public abstract class CollShape {

        public String name;
        public CollShapeType type;
        public float x;
        public float y;

        public float w;
        public float h;

        public boolean isSensor = false;

        public short categoryBits = 0;
        public short maskBits = 0;

        public float friction = 0f;
        public float restitution = 0f;

        public float collisionWidth = 0f;
        public float collisionHeight = 0f;

        public CollShape(String name, CollShapeType type, float width, float height) {
            this.name = name;
            this.type = type;

            w = width;
            h = height;
        }

        public void convert(float w, float h) {

        }

        public abstract Shape parseCollShape(float w, float h, float sX,
                        float sY, float xOffset, float yOffset);

    }

    public class RectShape extends CollShape {

        public RectShape(String name, float w, float h) {
            super(name, CollShapeType.RECT, w, h);
        }

        @Override
        public void convert(float w, float h) {
            x = x / w;
            y = (h - y - this.h) / h;

            this.w /= w;
            this.h /= h;
        }

        @Override
        public Shape parseCollShape(float w, float h, float sX, float sY,
                        float xOffset, float yOffset) {
            PolygonShape rs = new PolygonShape();
            float width = this.w * w * B2DWorld.WORLD_TO_B2D * sX / 2f;
            float height = this.h * h * B2DWorld.WORLD_TO_B2D * sY / 2f;
            float cx = (xOffset + x * w) * B2DWorld.WORLD_TO_B2D * sX;
            float cy = (yOffset + y * h) * B2DWorld.WORLD_TO_B2D * sY;
            collisionWidth = width * 2f;
            collisionHeight = height * 2f;

            cx += width;
            cy += height;
            rs.setAsBox(width, height, new Vector2(cx, cy), 0);
            return rs;
        }
    }

    public class CircShape extends CollShape {

        public float radius;

        public CircShape(String name, float w, float h) {
            super(name, CollShapeType.CIRC, w, h);

            radius = Math.max(w, h) / 2f;
        }

        @Override
        public void convert(float w, float h) {
            x = x / w;
            y = (h - y - this.h) / h;

            this.w /= w;
            this.h /= h;

            radius /= Math.max(w, h);
        }

        @Override
        public Shape parseCollShape(float w, float h, float sX, float sY,
                        float xOffset, float yOffset) {
            CircleShape cs = new CircleShape();
            float x = (xOffset + this.x * w) * B2DWorld.WORLD_TO_B2D * sX;
            float y = (yOffset + this.y * h + this.h * h)
                            * B2DWorld.WORLD_TO_B2D * sY;
            float r = radius * Math.max(w, h) * B2DWorld.WORLD_TO_B2D
                            * Math.max(sX, sY);
            collisionWidth = r * 2f;
            collisionHeight = r * 2f;
            cs.setPosition(new Vector2(x + r, y - r));
            cs.setRadius(r);
            return cs;
        }
    }

    public class PolyShape extends CollShape {

        private Vector2[] points;

        public PolyShape(String name, int pointCount, float w, float h) {
            super(name, CollShapeType.CIRC, w, h);
            points = new Vector2[pointCount];
        }

        public void addPoint(int ind, float x, float y) {
            points[ind] = new Vector2(x, y);
        }

        public Vector2[] getPoints() {
            return points;
        }

        @Override
        public void convert(float w, float h) {
            x = x / w;
            y = (h - y - this.h) / h;
            for (Vector2 v : points) {
                v.x = v.x / w;
                v.y = (this.h - v.y) / h;
            }

            this.w /= w;
            this.h /= h;
        }

        @Override
        public Shape parseCollShape(float w, float h, float sX, float sY,
                        float xOffset, float yOffset) {
            PolygonShape ps = new PolygonShape();
            Vector2[] vertices = new Vector2[points.length];
            float x = (xOffset + this.x * w) * B2DWorld.WORLD_TO_B2D * sX;
            float y = (yOffset + this.y * h) * B2DWorld.WORLD_TO_B2D * sY;
            float xMin = Float.MAX_VALUE;
            float xMax = -Float.MAX_VALUE;
            float yMin = Float.MAX_VALUE;
            float yMax = -Float.MAX_VALUE;
            for (int i = 0; i < points.length; i++) {
                Vector2 p = points[i];
                float px = x + p.x * w * B2DWorld.WORLD_TO_B2D * sX;
                float py = y + p.y * h * B2DWorld.WORLD_TO_B2D * sY;
                vertices[i] = new Vector2(px, py);
                xMin = Math.min(xMin, px);
                xMax = Math.max(xMax, px);
                yMin = Math.min(yMin, py);
                yMax = Math.max(yMax, py);
            }
            collisionWidth = Math.abs(xMin - xMax);
            collisionHeight = Math.abs(yMin - yMax);
            ps.set(vertices);
            return ps;
        }
    }
}
