package de.venjinx.ejx.entity;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;

import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;

public class EJXActor extends Actor {

    private EJXEntity entity;

    public EJXActor(EJXEntity entity) {
        setUserObject(entity);
        this.entity = entity;
    }

    @Override
    public void act(float deltaT) {
        if (getActions().size > 0) {
            super.act(deltaT);
        }
        entity.doStuff(deltaT);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        entity.render(batch, parentAlpha);
    }

    @Override
    public void setDebug(boolean enable) {
        super.setDebug(enable);

        if (enable) setVisible(enable);
        else {
            setVisible(entity.getDef().getValue(DefinitionProperty.visible,
                            Boolean.class));
        }
    }

    @Override
    public void drawDebug(ShapeRenderer shapeRenderer) {
        if (entity.isOnScreen()) {
            entity.drawShapeDebug(shapeRenderer);
        }
    }

    @Override
    public void clearActions() {
        //        VJXLogger.log(getName() + " clear actions", 1);
        super.clearActions();
    }

    @Override
    public void addAction(Action a) {
        //        VJXLogger.log(getName() + " add action " + a, 1);
        super.addAction(a);
    }

    @Override
    public void removeAction(Action action) {
        //        VJXLogger.log(getName() + " remove action " + action, 1);
        super.removeAction(action);
    }
}