package de.venjinx.ejx.entity;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;
import de.venjinx.ejx.util.EJXTypes.SpatialProperty;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXData;
import de.venjinx.ejx.util.VJXGraphix;

public class TiledEntity extends EntityImpl {

    private static final String _origin = "_origin";

    private Array<MapObject> tiles;
    private Array<TextureRegion> textures;

    private Box2DLogic physics;

    public TiledEntity(EntityDef def) {
        super(def);
        tiles = new Array<>();
        textures = new Array<>();
    }

    public void addObject(MapObject object, TextureRegion texture) {
        if (object.getName().contains(_origin)) {
            getDef().addProps(object.getProperties());
        }
        tiles.add(object);
        textures.add(texture);
        float x = object.getProperties().get(SpatialProperty.x.name, Float.class);
        float y = object.getProperties().get(SpatialProperty.y.name, Float.class);

        float w = object.getProperties().get(SpatialProperty.width.name, Float.class);
        float h = object.getProperties().get(SpatialProperty.height.name, Float.class);

        int mx = (int) x + (int) w;
        int my = (int) y + (int) h;

        float sizeX = getScaledWidth();
        float sizeY = getScaledHeight();

        if (x < birthPosition.x) {
            sizeX = sizeX + (birthPosition.x - x);
            setBirthPlace(x, birthPosition.y);
        } else if (mx > birthPosition.x + getScaledWidth())
            sizeX = sizeX + (mx - (birthPosition.x + sizeX));

        if (y < birthPosition.y) {
            sizeY = sizeY + (birthPosition.y - y);
            setBirthPlace(birthPosition.x, y);
        } else if (my > birthPosition.y + getScaledHeight())
            sizeY = sizeY + (my - (birthPosition.y + sizeY));

        setSize(sizeX, sizeY);
    }

    @Override
    public void spawn(World world) {

        String defName, attr;
        float x, y, lx, ly, rotation = 0;
        MapProperties props;

        if (tiles.size == 1) {
            props = tiles.get(0).getProperties();
            rotation = props.get(SpatialProperty.rotation.name, Float.class);
        }

        for (MapObject mo : tiles) {
            props = mo.getProperties();
            if (mo.getName().contains(_origin)) {
                rotation = props.get(SpatialProperty.rotation.name, Float.class);
            }

            defName = props.get(DefinitionProperty.defName.name,
                            String.class);
            attr = props.get(DefinitionProperty.attributes.name,
                            String.class);
            if (!attr.isEmpty())
                defName = defName.replace(VJXString.SEP_USCORE + attr, VJXString.STR_EMPTY);

            x = props.get(SpatialProperty.x.name, Float.class);
            y = props.get(SpatialProperty.y.name, Float.class);

            lx = x - birthPosition.x;
            ly = y - birthPosition.y;
            props.put(SpatialProperty.localX.name, lx);
            props.put(SpatialProperty.localY.name, ly);
        }
        getDef().setValue(SpatialProperty.rotation.name, rotation);
        super.spawn(world);
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
        float rw, rh, sclX, sclY;

        if (listener.getGame().getScreen().getRenderer().useFBO()) batch.begin();
        MapObject obj;
        TextureRegion tr;
        //        float x = getWidth() - getScaledWidth();
        //        float y = getHeight() - getScaledHeight();
        //        VJXLogger.log(this + " x " + getScaledWidth() + ", y "
        //                        + getScaledHeight());
        getLocalPosition(tmpVec);
        //        tmpVec.x += x / 2f;
        //        tmpVec.y += y / 2f;
        for (int i = 0; i < tiles.size; i++) {
            obj = tiles.get(i);
            tr = textures.get(i);
            VJXGraphix.updateDrawTransform(tmpVec,
                            obj.getProperties(), tr, drawTransform, getScaleX(),
                            getScaleY());
            sclX = obj.getProperties().get(SpatialProperty.scaleX.name, Float.class);
            sclY = obj.getProperties().get(SpatialProperty.scaleY.name, Float.class);
            rw = tr.getRegionWidth() * 2 * sclX * getScaleX();
            rh = tr.getRegionHeight() * 2 * sclY * getScaleY();
            batch.draw(tr, rw, rh, drawTransform);
        }
        if (listener.getGame().getScreen().getRenderer().useFBO()) batch.end();
    }

    @Override
    public void acceptBeginColl(short bit, VJXData data, EJXEntity other,
                    short otherBit, VJXData otherData) {
        other.visitBeginColl(otherBit, otherData, bit, this, data);
    }

    @Override
    public void acceptEndColl(short bit, VJXData data, EJXEntity other,
                    short otherBit, VJXData otherData) {
        other.visitEndColl(otherBit, otherData, bit, this, data);
    }

    @Override
    public void userAct(float deltaT) {
    }

    @Override
    protected void createBodyFixtures() {
        if (!usesPhysics()) return;

        physics = getPhysics();

        float lx, ly, w, h;
        CollisionDef collDef;
        for (MapObject mo : tiles) {
            MapProperties props = mo.getProperties();
            lx = props.get(SpatialProperty.localX.name, 0f, Float.class);
            ly = props.get(SpatialProperty.localY.name, 0f, Float.class);
            w = props.get(SpatialProperty.width.name, 0f, Float.class);
            h = props.get(SpatialProperty.height.name, 0f, Float.class);

            collDef = props.get(DefinitionProperty.collider.name, CollisionDef.class);
            physics.createBodyFixtures(collDef, lx, ly, w, h);
            updateFixtures(props);
        }
    }

    private void updateFixtures(MapProperties properties) {
        String defName, name, attr;
        float w, h, lx, ly, rotation;
        boolean flipX, flipY;
        defName = properties.get(DefinitionProperty.defName.name, String.class);
        attr = properties.get(DefinitionProperty.attributes.name, String.class);
        if (!attr.isEmpty())
            defName = defName.replace(VJXString.SEP_USCORE + attr, VJXString.STR_EMPTY);

        name = properties.get(SourceProperty.tileName.name, String.class);

        lx = properties.get(SpatialProperty.localX.name, 0f, Float.class);
        ly = properties.get(SpatialProperty.localY.name, 0f, Float.class);

        w = properties.get(SpatialProperty.width.name, 0f, Float.class);
        h = properties.get(SpatialProperty.height.name, 0f, Float.class);

        rotation = properties.get(SpatialProperty.rotation.name, Float.class);

        flipX = properties.get(SpatialProperty.flipX.name, false, Boolean.class);
        flipY = properties.get(SpatialProperty.flipY.name, false, Boolean.class);

        for (Fixture f : body.getFixtureList()) {
            if (!f.getUserData().toString().contains(name)
                            && !f.getUserData().toString().contains(defName))
                continue;

            if (flipX) physics.flipShapeX(f.getShape(), w, lx + w / 2);
            if (flipY) physics.flipShapeY(f.getShape(), h, ly + h / 2);
        }

        physics.rotateBody(rotation);
    }

    @Override
    public void drawFrameDebug(ShapeRenderer shapeRenderer) {

    }
}