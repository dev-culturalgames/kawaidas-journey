package de.venjinx.ejx.entity;

import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.VJXData;

public class EntityImpl extends AnimatedEntity {

    public EntityImpl(EntityDef def) {
        super(def);
    }

    @Override
    public void userAct(float deltaT) {

    }

    @Override
    protected void activityChanged(Activity oldA, Activity newA) {

    }

    @Override
    public void acceptBeginColl(short bit, VJXData data, EJXEntity other,
                    short otherBit, VJXData otherData) {
        other.visitBeginColl(otherBit, otherData, bit, this, data);
    }

    @Override
    public void acceptEndColl(short bit, VJXData data, EJXEntity other,
                    short otherBit, VJXData otherData) {
        other.visitEndColl(otherBit, otherData, bit, this, data);
    }

    @Override
    public void onDamage(int oldHP, int newHP) {
    }
}