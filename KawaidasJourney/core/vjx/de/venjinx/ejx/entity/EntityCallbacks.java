package de.venjinx.ejx.entity;

import de.venjinx.ejx.util.VJXData;

public interface EntityCallbacks {

    public enum VJXCallback {
        onClick("onClick"),
        onScreen("onScreen"), offScreen("offScreen"),
        onRespawn("onRespawn"), onDespawn("onDespawn"),
        onTrigger("onTrigger"), offTrigger("offTrigger"),
        onActivate("onActivate"), onDeactivate("onDeactivate"),
        targetInView("targetInView"), targetOutView("targetOutView"),
        targetInRange("targetInRange"), targetOutRange("targetOutRange"),
        onDamage("onDamage"), onDie("onDie"), onDead("onDead"),
        onStart("onStart"), onEnd("onEnd"),
        onDialogueStart("onDialogueStart"), onDialogueEnd("onDialogueEnd"),
        onHintStart("onHintStart"), onHintEnd("onHintEnd"),
        onSet("onSet"), onReach("onReach"),
        onCommands("onCommands"), offCommands("offCommands")
        ;

        public final String name;

        private VJXCallback(final String name) {
            this.name = name;
        }

        public static final VJXCallback get(String name) {
            for (VJXCallback callback : VJXCallback.values())
                if (callback.name.equals(name)) return callback;
            return null;
        }
    }

    public enum VJXTrigger {
        screen("screen"),
        spawn("spawn"),
        view("view"), range("range"),
        trigger("trigger")
        ;

        public final String name;

        private VJXTrigger(final String name) {
            this.name = name;
        }

        public static final VJXTrigger get(String name) {
            for (VJXTrigger triggerType : VJXTrigger.values())
                if (triggerType.name.equals(name)) return triggerType;
            return null;
        }

        public static final VJXCallback getCallback(VJXTrigger type, boolean on) {
            switch (type) {
                case screen:
                    if (on) return VJXCallback.onScreen;
                    else return VJXCallback.offScreen;
                case spawn:
                    if (on) return VJXCallback.onRespawn;
                    else return VJXCallback.onDespawn;
                case trigger:
                    if (on) return VJXCallback.onTrigger;
                    else return VJXCallback.offTrigger;
                case view:
                    if (on) return VJXCallback.targetInView;
                    else return VJXCallback.targetOutView;
                case range:
                    if (on) return VJXCallback.targetInRange;
                    else return VJXCallback.targetOutRange;
                default:
            }
            return null;
        }
    }

    /**
     * Object callbacks
     */
    public void onRespawn();

    public void onScreenChanged(boolean onScreen);

    public void targetInView(boolean inView);

    public void targetInRange(boolean inRange);

    public void targetChanged(EJXEntity targetEntity);

    public void onTrigger(boolean triggered, VJXData thisData,
                    EJXEntity other, VJXData otherData);

    public void onActivated(EJXEntity activator, boolean activated);

    public void onDamage(int oldHP, int newHP);

    public void onDie();

    public void onDead();

    public void onDespawn();

    public void onEnd();

    public void onStart();

    public void positionReached();
}