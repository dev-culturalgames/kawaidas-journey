package de.venjinx.ejx.entity;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonJson;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.EJXPlayer;
import de.venjinx.ejx.animation.EJXAnimation;
import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.AnimationType;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;
import de.venjinx.ejx.util.EJXTypes.SpatialProperty;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class EntityFactory {

    private static int currentID = 0;
    private static ArrayDeque<Integer> freeIDs = new ArrayDeque<>();

    private EJXAssetManager am;
    private EJXGame game;

    private HashMap<Integer, EntityDef> entityDefs;
    private HashMap<String, EJXAnimation> entityAnims;

    private HashMap<String, FileHandle> animFiles = new HashMap<>();
    private HashMap<String, SkeletonJson> skeletonJsons = new HashMap<>();
    private static HashMap<String, SkeletonData> skeletonData = new HashMap<>();

    private static HashMap<String, Skeleton> skeletons = new HashMap<>();

    public EntityFactory(EJXGame game) {
        this.game = game;
        am = game.assets;
        entityDefs = new HashMap<>();
        entityAnims = new HashMap<>();
    }

    public void setDefs(HashMap<Integer, EntityDef> defs,
                    HashMap<String, EJXAnimation> anims) {
        entityDefs.clear();
        entityDefs.putAll(defs);

        entityAnims.clear();
        entityAnims.putAll(anims);
    }

    public HashMap<Integer, EntityDef> getDefs() {
        return entityDefs;
    }

    public EntityDef getEntityDef(int id) {
        return entityDefs.get(id);
    }

    public EntityDef getEntityDef(String name) {
        Collection<EntityDef> defs = entityDefs.values();
        for (EntityDef def : defs) {
            if (def.getDefName().equals(name)) return def;
        }
        return null;
    }

    public boolean hasEntityDef(String name) {
        Collection<EntityDef> defs = entityDefs.values();
        for (EntityDef def : defs) {
            if (def.getDefName().equals(name)) return true;
        }
        return false;
    }

    public EJXEntity createEntity(String defName) {
        return createEntity(getEntityDef(defName), null);
    }

    public EJXEntity createEntity(String defName, MapProperties properties) {
        return createEntity(getEntityDef(defName), properties);
    }

    public EJXEntity createEntity(EntityDef def) {
        return createEntity(def, def.getCustomProperties());
    }

    public EJXEntity createEntity(EntityDef def, MapProperties properties) {
        //        VJXLogger.log("createEntity:" + def);
        if (def == null) return null;
        if (properties == null) properties = def.getCustomProperties();

        EJXEntity e;
        switch (def.getCategory()) {
            case CHARACTER:
            case DECORATION:
            case OBJECT:
            case ITEM:
                switch (properties.get(VJXString.subcategory,
                                VJXString.STR_EMPTY, String.class)) {
                                    case "avatar":
                                        e = new PlayerEntity(game.getPlayer(), def);
                                        break;
                                    default:
                                        e = new EntityImpl(def);
                }
                break;
            case COLLISION:
                e = new CollisionObject(def,
                                game.getScreen().getLevel().getTileWidth());

                return e;
            default:
                return null;
        }

        if (properties != null) {
            updateProps(e, properties);
        }
        return e;
    }

    public EJXEntity createEntity(EJXGroup layerNode, EJXPlayer p,
                    EntityDef def, MapObject object) {
        EJXEntity e;
        MapProperties objProps = object.getProperties();
        String objectName = object.getName();

        Vector2 pos = new Vector2(
                        objProps.get(SpatialProperty.x.name, Float.class),
                        objProps.get(SpatialProperty.y.name, Float.class));
        if (objectName.equals("Kawaida")) {
            e = p.getEntity();
            updateProps(e, objProps);
        } else {
            e = createEntity(def, objProps);
            if (e == null) return null;
            e.setTargetEntity(p.getEntity());
        }
        e.setBirthPlace(pos);
        return e;
    }

    public EJXEntity createTiledObject(EJXGroup layerNode, EntityDef def,
                    MapObject mObj, TiledMapTile tile) {
        EJXEntity e;
        MapProperties objProps = mObj.getProperties();
        String objectName = mObj.getName();
        Vector2 pos = new Vector2(
                        objProps.get(SpatialProperty.x.name, Float.class),
                        objProps.get(SpatialProperty.y.name, Float.class));
        e = new TiledEntity(def);

        e.setName(objectName.replace("_origin", VJXString.STR_EMPTY));
        e.setBirthPlace(pos);
        ((TiledEntity) e).addObject(mObj, am.getRegion(tile));

        e.getDef().setFromProps(objProps);
        String logics = objProps.get("logics", VJXString.STR_EMPTY, String.class);
        LevelUtil.loadLogicProps(objProps, logics);
        return e;
    }

    public SpineActor createSpineActor(String name, float width, float height,
                    String sheet, FileHandle file) {
        return createSpineActor(name, width, height, sheet, file, null);
    }

    public SpineActor createSpineActor(String name, float width,
                    float height, String sheet, FileHandle file,
                    MapProperties props) {
        TextureAtlas ta = am.getTextureAtlas(sheet);
        if (ta != null) {
            SpineActor spineActor = new SpineActor(name);

            SkeletonData data = new SkeletonJson(ta).readSkeletonData(file);
            Skeleton skeleton = new Skeleton(data);
            AnimationStateData stateData = new AnimationStateData(skeleton.getData());
            spineActor.setAnimationState(new AnimationState(stateData));
            spineActor.setSkeleton(skeleton, props);
            spineActor.setSize(width, height);
            return spineActor;
        }
        return null;
    }

    public void createWaypoint(String name, Vector2 pos,
                    MapProperties properties) {
        float w2 = properties.get(SpatialProperty.width.name, 0f, Float.class)
                        / 2;
        float h2 = properties.get(SpatialProperty.height.name, 0f, Float.class)
                        / 2;
        Waypoint wp = new Waypoint(name);
        wp.setCenter(pos.x + w2, pos.y + h2);
        boolean execOnce = properties.get("execute_once", false, boolean.class);
        wp.setExecuteOnce(execOnce);
        Waypoints.addPoint(wp);
    }

    public void clearAnims() {
        animFiles.clear();
        skeletonJsons.clear();
        skeletonData.clear();
    }

    public void loadAnims() {
        Collection<EntityDef> defs = entityDefs.values();
        for (EntityDef def : defs) {
            loadAnimation(def);
        }

        loadPortraits();
    }

    private void loadAnimation(EntityDef def) {
        String skName = def.getSkeletonName();
        if (skName == null || skName.isEmpty()) skName = def.getDefName();

        if (def.getAnimationType() == AnimationType.SPINE) {
            String environment = def.getValue(SourceProperty.environment,
                            VJXString.STR_EMPTY, String.class);

            TextureAtlas ta = am.getTextureAtlas(skName + "_anims");

            if (!environment.isEmpty()) ta = am.getTextureAtlas(environment);
            else environment = skName + "_anims";

            skName = def.getSource() + skName;
            skName = skName.replaceAll("rawAssets/", "");

            if (!animFiles.containsKey(skName)) {
                animFiles.put(skName, Gdx.files.internal(skName + ".json"));
            }

            if (!skeletonJsons.containsKey(environment)) {
                skeletonJsons.put(environment, new SkeletonJson(ta));
            }

            if (!skeletonData.containsKey(skName))
                skeletonData.put(skName, skeletonJsons.get(environment)
                                .readSkeletonData(animFiles.get(skName)));

            def.setSkeletonData(skeletonData.get(skName));
        }
    }

    private void loadPortraits() {
        TextureAtlas ta = am.getTextureAtlas("global_portraits");
        SkeletonData skData = new SkeletonJson(ta)
                        .readSkeletonData(Gdx.files.internal(
                                        "objects/character/portraits0/portraits0.json"));
        skeletonData.put("portraits", skData);
        skeletons.put("portraits", new Skeleton(skData));
    }

    private void updateProps(EJXEntity e, MapProperties properties) {

        String defName = e.getDefName();
        e.getDef().setFromProps(properties);
        properties.put(DefinitionProperty.defName.name, defName);

        properties = e.getDef().getCustomProperties();
        int ow = properties.get(SourceProperty.srcWidth.name, 0, Integer.class);
        int oh = properties.get(SourceProperty.srcHeight.name, 0, Integer.class);

        boolean flipX = properties.get(SpatialProperty.flipX.name, false, Boolean.class);
        boolean flipY = properties.get(SpatialProperty.flipY.name, false, Boolean.class);
        e.setOrientation(Orientation.RIGHT, false);
        if (flipX) {
            e.setOrientation(Orientation.LEFT);
        }

        e.setFlipX(flipX);
        e.setFlipY(flipY);
        Vector2 size = new Vector2(ow, oh);
        Vector2 pos = new Vector2(properties.get(SpatialProperty.x.name, 0f, Float.class),
                        properties.get(SpatialProperty.y.name, 0f, Float.class));

        e.setSize(size);
        e.setBirthPlace(pos);

        float sclX = properties.get(SpatialProperty.scaleX.name, 1f, Float.class);
        float sclY = properties.get(SpatialProperty.scaleY.name, 1f, Float.class);
        e.setScale(sclX, sclY);
        e.setRotation(properties.get(SpatialProperty.rotation.name, 0f, Float.class));

        String objectName = properties.get(VJXString.name, "#mObj", String.class);
        if (objectName.contains("#mObj") || objectName.contains("#wID"))
            objectName = defName + "#wID" + e.getID();

        e.setName(objectName);

        String logics = properties.get("logics", VJXString.STR_EMPTY,  String.class);
        LevelUtil.loadLogicProps(properties, logics);

        if (properties.get(VJXString.subcategory, VJXString.STR_EMPTY,
                        String.class).equals("point"))
            createWaypoint(e.getName(), e.getBirthPlace(), properties);
    }

    public static int newID() {
        int id = -1;
        if (!freeIDs.isEmpty()) id = freeIDs.removeFirst();
        else id = currentID++;

        return id;
    }

    public static int freeID(int id) {
        freeIDs.add(id);

        return id;
    }

    public static void loadAnimations(EJXEntity e, EJXAssetManager am) {
        String skName = e.getDef().getSkeletonName();
        if (skName == VJXString.STR_EMPTY) skName = e.getDefName();

        if (e.getDef().getAnimationType() == AnimationType.SPINE) {
            ((AnimatedEntity) e).setupSpine();
        } else if (e.getDef().getAnimationType() == AnimationType.FRAME) {
            VJXLogger.log(LogCategory.WARNING, "ILLEGAL CALL: getRegion: " + e.getDefName());
            e.setFrame(am.getRegion(e.getDef().getCustomProperties()));
        }

        if (e.getDef().isSkin()) {
            skName = e.getDef().getTileName();
        }
    }

    public static Skeleton getSkeleton(String name) {
        return skeletons.get(name);
    }

    public static AnimationState newAnimState(String name) {
        AnimationStateData stateData = new AnimationStateData(skeletonData.get(name));
        return new AnimationState(stateData);

    }
}