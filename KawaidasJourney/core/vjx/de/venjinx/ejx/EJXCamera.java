package de.venjinx.ejx;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.PlayerEntity;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXProperties;
import de.venjinx.ejx.util.LevelUtil;

public class EJXCamera extends OrthographicCamera {

    public enum CamMode {
        NONE("none"), POSITION_LOCK("position_lock"), FORWARD_FOCUS("forward_focus");

        String name;

        private CamMode(String name) {
            this.name = name;
        }

        public static final CamMode get(String name) {
            for (CamMode mode : CamMode.values())
                if (mode.name.equals(name)) return mode;
            return null;
        }
    }

    public static final String cam_accelerationX = "cam:accelerationX";
    public static final String cam_accelerationY = "cam:accelerationY";
    public static final String cam_decelerationX = "cam:decelerationX";
    public static final String cam_decelerationY = "cam:decelerationY";
    public static final String cam_edgeSnappingX = "cam:edgeSnappingX";
    public static final String cam_edgeSnappingY = "cam:edgeSnappingY";
    public static final String cam_maxX = "cam:maxX";
    public static final String cam_minX = "cam:minX";
    public static final String cam_maxY = "cam:maxY";
    public static final String cam_minY = "cam:minY";
    public static final String cam_modeX = "cam:modeX";
    public static final String cam_modeY = "cam:modeY";
    public static final String cam_offsetBreak = "cam:offsetBreak";
    public static final String cam_offsetX = "cam:offsetX";
    public static final String cam_offsetXCatchMax = "cam:offsetXCatchMax";
    public static final String cam_offsetXCatchMin = "cam:offsetXCatchMin";
    public static final String cam_offsetX_lock = "cam:offsetXLock";
    public static final String cam_offsetY = "cam:offsetY";
    public static final String cam_offsetYCatchMax = "cam:offsetYCatchMax";
    public static final String cam_offsetYCatchMin = "cam:offsetYCatchMin";
    public static final String cam_offsetY_lock = "cam:offsetYLock";
    public static final String cam_platformOffset = "cam:platformOffset";
    public static final String cam_platformSnapping = "cam:platformSnapping";
    public static final String cam_speedXMax = "cam:speedXMax";
    public static final String cam_speedYMax = "cam:speedYMax";
    public static final String cam_target = "camTarget";
    public static final String cam_zoom = "cam:zoom";

    public enum CamProperty implements EJXPropertyDefinition {
        accelerationX(EJXCamera.cam_accelerationX, PropertyType.floatProp, 1f),
        accelerationY(EJXCamera.cam_accelerationY, PropertyType.floatProp, 1f),
        decelerationX(EJXCamera.cam_accelerationX, PropertyType.floatProp, 1f),
        decelerationY(EJXCamera.cam_accelerationY, PropertyType.floatProp, 1f),
        edgeSnappingX(EJXCamera.cam_edgeSnappingX, PropertyType.boolProp, true),
        edgeSnappingY(EJXCamera.cam_edgeSnappingY, PropertyType.boolProp, true),
        maxX(EJXCamera.cam_maxX, PropertyType.floatProp, Float.POSITIVE_INFINITY),
        minX(EJXCamera.cam_minX, PropertyType.floatProp, Float.NEGATIVE_INFINITY),
        maxY(EJXCamera.cam_maxY, PropertyType.floatProp, Float.POSITIVE_INFINITY),
        minY(EJXCamera.cam_minY, PropertyType.floatProp, Float.NEGATIVE_INFINITY),
        modeX(EJXCamera.cam_modeX, PropertyType.camModeType, CamMode.POSITION_LOCK),
        modeY(EJXCamera.cam_modeY, PropertyType.camModeType, CamMode.POSITION_LOCK),
        offsetBreak(EJXCamera.cam_offsetBreak, PropertyType.floatProp, 0f),
        offsetX(EJXCamera.cam_offsetX, PropertyType.floatProp, 0f),
        offsetXCatchMax(EJXCamera.cam_offsetXCatchMax, PropertyType.floatProp, 0f),
        offsetXCatchMin(EJXCamera.cam_offsetXCatchMin, PropertyType.floatProp, 0f),
        offsetXLock(EJXCamera.cam_offsetX_lock, PropertyType.floatProp, 0f),
        offsetY(EJXCamera.cam_offsetY, PropertyType.floatProp, 0f),
        offsetYCatchMax(EJXCamera.cam_offsetYCatchMax, PropertyType.floatProp, 0f),
        offsetYCatchMin(EJXCamera.cam_offsetYCatchMin, PropertyType.floatProp, 0f),
        offsetYLock(EJXCamera.cam_offsetY_lock, PropertyType.floatProp, 0f),
        platformOffset(EJXCamera.cam_platformOffset, PropertyType.floatProp, 0f),
        platformSnapping(EJXCamera.cam_platformSnapping, PropertyType.boolProp, true),
        speedXMax(EJXCamera.cam_speedXMax, PropertyType.floatProp, 60f),
        speedYMax(EJXCamera.cam_speedYMax, PropertyType.floatProp, 60f),
        zoom(EJXCamera.cam_zoom, PropertyType.floatProp, 1f)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private CamProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final CamProperty get(String name) {
            for (CamProperty property : CamProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private VJXProperties vjxProps;
    private Rectangle screenBound;
    private PlayerEntity playerEntity;
    private EJXEntity camEntity;
    private MoveLogic mLogic;

    private CamMode modeX;
    private CamMode modeY;
    private boolean usePlayerY;
    private boolean stoppingX;
    private boolean stoppingY;
    private boolean edgeSnappingX;
    private float minX;
    private float maxX;
    private boolean edgeSnappingY;
    private float minY;
    private float maxY;

    private float xOffLock;
    private float yOffLock;

    // forward focus horizontal properties
    private float xOff;
    private float xOffCatchMin;
    private float xOffCatchMax;
    private float xMin;
    private float xMax;
    private float xMinCatch;
    private float xMaxCatch;

    private boolean catchingX;
    private boolean followLeft;
    private boolean followRight;

    // forward focus vertical properties
    private float yOff;
    private float yOffCatchMin;
    private float yOffCatchMax;
    private float yMin;
    private float yMax;
    private float yMinCatch;
    private float yMaxCatch;

    private boolean catchingY;
    private boolean followUp;
    private boolean followDown;
    private boolean allowYOnce;

    // platform snapping properties
    private boolean platformSnapping;
    private float snapOffsetY;
    private float snapTargetY;
    private boolean aligningY;
    private boolean alignUp;
    private boolean alignDown;

    // camera movement properties
    private float accelerationX;
    private float speedX;
    private float speedXMax;
    private float signX;

    private float accelerationY;
    private float speedY;
    private float speedYMax;
    private float signY;

    private float breakOffset;

    private Vector2 tmpVec;

    public EJXCamera() {
        this(CamMode.POSITION_LOCK, CamMode.POSITION_LOCK);
    }

    public EJXCamera(CamMode modeX, CamMode modeY) {
        tmpVec = new Vector2();
        vjxProps = EJXTypes.newProps();

        this.modeX = modeX;
        this.modeY = modeY;

        followLeft = false;
        followRight = true;
        followDown = false;
        followUp = false;
    }

    public void configure(MapProperties properties) {
        LevelUtil.convertProperties(CamProperty.values(), vjxProps, properties);

        modeX = vjxProps.get(CamProperty.modeX, CamMode.class);
        modeY = vjxProps.get(CamProperty.modeY, CamMode.class);

        float minX = vjxProps.get(CamProperty.minX, Float.class);
        float minY = vjxProps.get(CamProperty.minY, Float.class);

        float maxX = vjxProps.get(CamProperty.maxX, Float.class);
        float maxY = vjxProps.get(CamProperty.maxY, Float.class);

        this.minX = minX == 0 ? Float.NEGATIVE_INFINITY : minX;
        this.maxX = maxX == 0 ? Float.POSITIVE_INFINITY : maxX;
        this.minY = minY == 0 ? Float.NEGATIVE_INFINITY : minY;
        this.maxY = maxY == 0 ? Float.POSITIVE_INFINITY : maxY;

        edgeSnappingX = vjxProps.get(CamProperty.edgeSnappingX, Boolean.class);
        edgeSnappingY = vjxProps.get(CamProperty.edgeSnappingY, false, Boolean.class);

        platformSnapping = vjxProps.get(CamProperty.platformSnapping, false, Boolean.class);
        snapOffsetY = vjxProps.get(CamProperty.platformOffset, Float.class);

        xOffLock = vjxProps.get(CamProperty.offsetXLock, Float.class);
        xOff = vjxProps.get(CamProperty.offsetX, Float.class);
        xOffCatchMin = vjxProps.get(CamProperty.offsetXCatchMin, Float.class);
        xOffCatchMax = vjxProps.get(CamProperty.offsetXCatchMax, Float.class);

        yOffLock = vjxProps.get(CamProperty.offsetYLock, Float.class);
        yOff = vjxProps.get(CamProperty.offsetY, Float.class);
        yOffCatchMin = vjxProps.get(CamProperty.offsetYCatchMin, Float.class);
        yOffCatchMax = vjxProps.get(CamProperty.offsetYCatchMax, Float.class);

        accelerationX = vjxProps.get(CamProperty.accelerationX, Float.class);
        accelerationX *= B2DWorld.WORLD_UNIT;
        speedXMax = vjxProps.get(CamProperty.speedXMax, Float.class);
        speedXMax *= B2DWorld.WORLD_UNIT;

        accelerationY = vjxProps.get(CamProperty.accelerationY, Float.class);
        accelerationY *= B2DWorld.WORLD_UNIT;
        speedYMax = vjxProps.get(CamProperty.speedYMax, Float.class);
        speedYMax *= B2DWorld.WORLD_UNIT;

        breakOffset = vjxProps.get(CamProperty.offsetBreak, Float.class);

        zoom = vjxProps.get(CamProperty.zoom, Float.class);
    }

    public void reset() {
        catchingX = modeX == CamMode.FORWARD_FOCUS ? true : false;
        catchingY = false;
        aligningY = false;
        followLeft = false;
        followRight = modeX == CamMode.FORWARD_FOCUS ? true : false;
        followDown = false;
        followUp = false;
        alignDown = false;
        alignUp = false;

        stoppingY = false;
        speedX = 0f;
        speedY = 0f;

        tmpVec.set(camEntity.getWCX() + xOff, camEntity.getWCY());
        if (usePlayerY) tmpVec.y = playerEntity.getWCY() + yOff;
        screenBound.setCenter(tmpVec);
        position.set(tmpVec, 0);

        xMin = tmpVec.x - xOff;
        xMax = tmpVec.x + xOff;
        xMinCatch = tmpVec.x - xOffCatchMin;
        xMaxCatch = tmpVec.x + xOffCatchMax;

        yMin = tmpVec.y - yOff;
        yMax = tmpVec.y + yOff;
        yMinCatch = tmpVec.y - yOffCatchMin;
        yMaxCatch = tmpVec.y + yOffCatchMax;
    }

    public Vector2 updateFocus(float delta) {
        EJXEntity target;
        if (usePlayerY) target = playerEntity;
        else target = camEntity;

        if (target.getListener() == null) { return screenBound.getCenter(tmpVec); }

        switch (modeX) {
            case POSITION_LOCK:
                screenBound.x = camEntity.getWCX() - screenBound.getWidth() / 2f;
                screenBound.x += xOffLock;
                break;
            case FORWARD_FOCUS:
                if (mLogic == null) break;

                screenBound.getCenter(tmpVec);

                xMin = tmpVec.x - xOff;
                xMax = tmpVec.x + xOff;
                xMinCatch = tmpVec.x - xOffCatchMin;
                xMaxCatch = tmpVec.x + xOffCatchMax;

                checkFollowX();

                catchUpX(delta);

                screenBound.setCenter(position.x, position.y);
                break;
            default:
        }

        switch (modeY) {
            case POSITION_LOCK:
                screenBound.y = target.getWCY() - screenBound.getHeight() / 2f;
                screenBound.y += yOffLock;
                break;
            case FORWARD_FOCUS:
                if (mLogic == null) break;

                screenBound.getCenter(tmpVec);

                yMin = tmpVec.y - yOff;
                yMax = tmpVec.y + yOff;
                yMinCatch = tmpVec.y - yOffCatchMin;
                yMaxCatch = tmpVec.y + yOffCatchMax;

                checkFollowY();

                catchUpY(delta);

                screenBound.setCenter(position.x, position.y);
                break;
            default:
        }

        alignToPlatform(delta);
        checkEdgeSnapping();
        update();
        return tmpVec;
    }

    public void setModeX(CamMode mode) {
        modeX = mode;
    }

    public void setModeY(CamMode mode) {
        modeY = mode;
    }

    public void setMode(CamMode modeX, CamMode modeY) {
        this.modeX = modeX;
        this.modeY = modeY;
    }

    public void setLockOffsetX(float xOff) {
        xOffLock = xOff;
    }

    public void setLockOffsetY(float yOff) {
        yOffLock = yOff;
    }

    public void setLockOffset(float xOff, float yOff) {
        xOffLock = xOff;
        yOffLock = yOff;
    }

    public void setUsePlayerY(boolean usePlayerY) {
        this.usePlayerY = usePlayerY;
        if (!usePlayerY)
            platformSnapping = platformSnapping && playerEntity == camEntity;
        else platformSnapping = vjxProps.get(CamProperty.platformSnapping, false,
                        Boolean.class);
    }

    public void setPlayerEntity(PlayerEntity playerEntity) {
        this.playerEntity = playerEntity;
    }

    public void setCamTarget(EJXEntity camEntity) {
        this.camEntity = camEntity;

        if (camEntity != null) {
            if (camEntity.hasLogic(VJXLogicType.MOVE))
                mLogic = (MoveLogic) camEntity.getLogic(VJXLogicType.MOVE);
        }
    }

    public void setScreenBound(Rectangle screenBound) {
        this.screenBound = screenBound;
    }

    public void setMoveArea(Rectangle cameraArea) {
        setMoveArea(cameraArea.x, cameraArea.x + cameraArea.width, cameraArea.y,
                        cameraArea.y + cameraArea.height);
    }

    public void setMoveArea(float minX, float maxX, float minY, float maxY) {
        if (minX != Float.POSITIVE_INFINITY)
            this.minX = Math.max(minX, this.minX);
        if (minY != Float.POSITIVE_INFINITY)
            this.minY = Math.max(minY, this.minY);
        if (maxX != Float.NEGATIVE_INFINITY)
            this.maxX = Math.min(maxX, this.maxX);
        if (maxY != Float.NEGATIVE_INFINITY)
            this.maxY = Math.min(maxY, this.maxY);
    }

    public void catchYDown(boolean activate) {
        aligningY = false;
        alignUp = false;
        alignDown = false;

        catchingY = activate;
        followUp = false;
        followDown = activate;

        stoppingY = false;
        speedY = 0f;
    }

    public void catchYUp(boolean activate) {
        aligningY = false;
        alignUp = false;
        alignDown = false;

        if (activate) allowYOnce = true;
        else {
            catchingY = activate;
            followUp = activate;
        }
        followDown = false;

        stoppingY = false;
        speedY = 0f;
    }

    public String followX() {
        return followLeft ? "followLeft" : followRight ? "followRight" : "none";
    }

    public String followY() {
        return followUp ? "followUp" : followDown ? "followDown" : "none";
    }

    public String catchingX() {
        return catchingX ? "true" : "false";
    }

    public String catchingY() {
        return catchingY ? "true" : "false";
    }

    public String stoppingX() {
        return stoppingX ? "true" : "false";
    }

    public String stoppingY() {
        return stoppingY ? "true" : "false";
    }

    public float getSpeedX() {
        return speedX;
    }

    public float getSpeedY() {
        return speedY;
    }

    public EJXEntity getCamTarget() {
        return camEntity;
    }

    public String aligningY() {
        return aligningY ? "true" : "false";
    }

    public String aligningYUp() {
        return alignUp ? "true" : "false";
    }

    public String aligningYDown() {
        return alignDown ? "true" : "false";
    }

    public void snapToPlatform(float platformY) {
        if (!platformSnapping || aligningY || !usePlayerY) return;

        snapTargetY = platformY + snapOffsetY;
        if (position.y < snapTargetY) {
            aligningY = true;
            alignUp = true;
            alignDown = false;

            stoppingY = false;
            speedY = 0f;
        } else {
            if (position.y > snapTargetY) {
                aligningY = true;
                alignUp = false;
                alignDown = true;

                stoppingY = false;
                speedY = 0f;
            }
        }
        allowYOnce = false;
        catchingY = false;
        followUp = false;
        followDown = false;
    }

    private void alignToPlatform(float delta) {
        if (!aligningY) return;
        if (alignDown) {
            signY = -1f;
            if (snapTargetY < position.y - speedY * delta) {
                position.y -= speedY * delta;
            } else {
                position.y = snapTargetY;
                stopAlignY();
                return;
            }
        }

        if (alignUp) {
            signY = 1f;
            if (snapTargetY > position.y + speedY * delta) {
                position.y += speedY * delta;
            } else {
                position.y = snapTargetY;
                stopAlignY();
                return;
            }
        }

        screenBound.setCenter(position.x, position.y);

        float distance = Math.abs(snapTargetY - position.y);
        if (distance < 1) {
            position.y = snapTargetY;
            stopAlignY();
            return;
        }

        if (distance > 0) {
            stoppingY = distance < snapOffsetY / 2f;

            if (!stoppingY) {
                if (speedY + accelerationY / 2f < speedYMax)
                    speedY += accelerationY / 2f;
                else speedY = speedYMax;
            } else {
                float deceleration = calcDecaleration(speedY, distance, delta);
                speedY -= deceleration;
                aligningY = Math.abs(speedY) > 0 ? true : false;
            }
        }
    }

    private void checkFollowX() {
        if (catchingX) return;
        if (camEntity.getWCX() < xMinCatch) {
            followLeft = true;
            followRight = false;

            catchingX = true;
            stoppingX = false;
            speedX = 0;
            return;
        }

        if (camEntity.getWCX() > xMaxCatch) {
            followLeft = false;
            followRight = true;

            catchingX = true;
            stoppingX = false;
            speedX = 0;
        }
    }

    private void checkFollowY() {
        if (catchingY || aligningY) return;

        EJXEntity target;
        if (usePlayerY) target = playerEntity;
        else target = camEntity;

        if (target.getWCY() < yMinCatch) {
            followDown = true;
            followUp = false;
            alignDown = false;
            alignUp = false;

            catchingY = true;
            stoppingY = false;
            speedY = 0;
            return;
        }

        if (!allowYOnce && (target.isActivity(Activity.RISE)
                        || target.isActivity(Activity.FALL)
                        || target.isActivity(Activity.EXECUTE))) {
            return;
        }

        if (target.getWCY() > yMaxCatch) {
            allowYOnce = false;

            followDown = false;
            followUp = true;
            alignDown = false;
            alignUp = false;

            catchingY = true;
            stoppingY = false;
            speedY = 0;
        }
    }

    private void catchUpX(float delta) {
        float velX = mLogic.getVelocity().x;
        if (camEntity.getPhysics() != null && camEntity.getPhysics().isAttached()) {
            EJXEntity attached = camEntity.getPhysics().getAttachedParent();
            if (attached.hasLogic(VJXLogicType.MOVE))
                velX += ((MoveLogic) attached.getLogic(VJXLogicType.MOVE))
                .getVelocity().x;
        }
        if (!catchingX) {
            if (followLeft && camEntity.getWCX() < xMax && screenBound.x > minX)
                position.x = camEntity.getWCX() - xOff;

            if (followRight && camEntity.getWCX() > xMin
                            && screenBound.x + screenBound.width < maxX)
                position.x = camEntity.getWCX() + xOff;
        } else {
            float targetX = position.x;
            float incr = (velX + speedX) * delta;
            if (followLeft) {
                signX = -1f;
                targetX = camEntity.getWCX() - xOff;
                if (targetX < position.x + incr) {
                    position.x += incr;
                } else {
                    position.x = targetX;
                    stopCatchX();
                    return;
                }
            } else if (followRight) {
                signX = 1f;
                targetX = camEntity.getWCX() + xOff;
                if (targetX > position.x + incr) {
                    position.x += incr;
                } else {
                    position.x = targetX;
                    stopCatchX();
                    return;
                }
            }

            float distance = Math.abs(targetX - position.x);

            if (distance < 1) {
                position.x = targetX;
                stopCatchX();
                return;
            }

            stoppingX = distance < breakOffset;

            if (!stoppingX) {
                if (Math.abs(speedX) < speedXMax)
                    speedX += signX * accelerationX;
                else speedX = signX * speedXMax;
            } else {
                if (stoppingX && velX == 0) {
                    float deceleration = calcDecaleration(speedX, distance, delta);
                    speedX -= signX * deceleration;
                }
            }
        }
    }

    private void catchUpY(float delta) {
        float velY = mLogic.getVelocity().y;
        EJXEntity target;
        if (usePlayerY) {
            target = playerEntity;
            velY = ((MoveLogic) target.getLogic(VJXLogicType.MOVE)).getVelocity().y;
        } else target = camEntity;

        if (target.getPhysics() != null && target.getPhysics().isAttached()) {
            EJXEntity attached = target.getPhysics().getAttachedParent();
            if (attached.hasLogic(VJXLogicType.MOVE))
                velY += ((MoveLogic) attached.getLogic(VJXLogicType.MOVE))
                .getVelocity().y;
        }

        if (!catchingY) {
            if (followDown && target.getWCY() < yMax && screenBound.y > 0) {
                position.y = target.getWCY() - yOff;
            }

            if (followUp && target.getWCY() > minY
                            && screenBound.y + screenBound.height < maxY) {
                position.y = target.getWCY() + yOff;
            }
        } else {
            float targetY = position.y;
            float incr = (velY + speedY) * delta;
            if (followDown) {
                signY = -1;
                targetY = target.getWCY() - yOff;
                if (targetY < position.y + incr) {
                    position.y += incr;
                } else {
                    position.y = targetY;
                    stopCatchY();
                    return;
                }
            } else if (followUp) {
                signY = 1;
                targetY = target.getWCY() + yOff;
                if (targetY > position.y + incr) {
                    position.y += incr;
                } else {
                    position.y = targetY;
                    stopCatchY();
                    return;
                }
            }

            float distance = Math.abs(targetY - position.y);
            if (distance < 1) {
                position.y = targetY;
                stopCatchY();
                return;
            }
            stoppingY = distance < breakOffset;

            if (!stoppingY) {
                if (Math.abs(speedY) < speedYMax)
                    speedY += signY * accelerationY;
                else speedY = signY * speedYMax;
            } else {
                if (stoppingY && velY == 0) {
                    float deceleration = calcDecaleration(speedY, distance, delta);
                    speedY -= signY * deceleration;
                }
            }
        }
    }

    private float calcDecaleration(float startVel, float distance, float delta) {
        return (float) Math.pow(startVel * delta, 2) / (2 * distance);
    }

    private void stopCatchX() {
        catchingX = false;
        speedX = 0;
        screenBound.setCenter(position.x, position.y);
    }

    private void stopCatchY() {
        catchingY = false;
        speedY = 0;
        screenBound.setCenter(position.x, position.y);
    }

    private void stopAlignY() {
        aligningY = false;
        alignUp = false;
        alignDown = false;

        catchingY = false;
        followUp = false;
        followDown = false;

        speedY = 0;
        screenBound.setCenter(position.x, position.y);
    }

    private void checkEdgeSnapping() {
        if (edgeSnappingX) {
            //            if (!cameraMoveArea.contains(screenBound.x, 0)) {
            //                screenBound.x = cameraMoveArea.x;
            //            }

            //            if (!cameraMoveArea.contains(screenBound.x + screenBound.width, 0)) {
            //                screenBound.x = cameraMoveArea.x + cameraMoveArea.width - screenBound.width;
            //            }
            if (screenBound.x < minX) {
                screenBound.x = minX;
            }

            if (screenBound.x + screenBound.width > maxX) {
                screenBound.x = maxX - screenBound.width;
            }
        }

        if (edgeSnappingY) {
            //            if (!cameraMoveArea.contains(0, screenBound.y)) {
            //                screenBound.y = cameraMoveArea.y;
            //                aligningY = alignDown = false;
            //            }
            //
            //            if (!cameraMoveArea.contains(0, screenBound.y + screenBound.height)) {
            //                screenBound.y = cameraMoveArea.y + cameraMoveArea.height - screenBound.height;
            //                aligningY = alignUp = false;
            //            }
            if (screenBound.y < minY) {
                screenBound.y = minY;
                aligningY = alignDown = false;
            }

            if (screenBound.y + screenBound.height > maxY) {
                screenBound.y = maxY - screenBound.height;
                aligningY = alignUp = false;
            }
        }

        position.set(screenBound.getCenter(tmpVec), 0);
    }

    public void drawBatchDebug(Batch batch, BitmapFont font) {
        font.draw(batch, "xMinCatch " + xMinCatch + " " + catchingX, xMinCatch - 4, tmpVec.y + 260);
        font.draw(batch, "xMin " + xMin + " " + followRight, xMin - 4, tmpVec.y + 260);
        font.draw(batch, "xMaxCatch " + xMaxCatch, xMaxCatch - 4, tmpVec.y + 260);
        font.draw(batch, "xMax " + xMax + " " + followLeft, xMax - 4, tmpVec.y + 260);

        font.draw(batch, "yMinCatch " + yMinCatch + " " + catchingY, tmpVec.x + 260, yMinCatch);
        font.draw(batch, "yMin " + yMin + " " + followUp, tmpVec.x + 260, yMin);
        font.draw(batch, "yMaxCatch " + yMaxCatch, tmpVec.x + 260, yMaxCatch);
        font.draw(batch, "yMax " + yMax + " " + followDown, tmpVec.x + 260, yMax);

        font.draw(batch, "yTarget " + snapTargetY + " " + aligningY,
                        tmpVec.x + 260, snapTargetY);
    }

    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.line(xMinCatch, tmpVec.y - 256, xMinCatch, tmpVec.y + 256);
        shapeRenderer.line(xMin, tmpVec.y - 256, xMin, tmpVec.y + 256);

        shapeRenderer.line(xMax, tmpVec.y - 256, xMax, tmpVec.y + 256);
        shapeRenderer.line(xMaxCatch, tmpVec.y - 256, xMaxCatch, tmpVec.y + 256);

        shapeRenderer.line(tmpVec.x - 256, yMinCatch, tmpVec.x + 256, yMinCatch);
        shapeRenderer.line(tmpVec.x - 256, yMin, tmpVec.x + 256, yMin);

        shapeRenderer.line(tmpVec.x - 256, yMax, tmpVec.x + 256, yMax);
        shapeRenderer.line(tmpVec.x - 256, yMaxCatch, tmpVec.x + 256, yMaxCatch);
        shapeRenderer.line(tmpVec.x - 256, snapTargetY, tmpVec.x + 256,
                        snapTargetY);
    }
}