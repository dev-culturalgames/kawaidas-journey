package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class WPCommand extends Command {

    private static final String VALID_COMMAND = " next_wp|set_wp:<string_waypointName>";
    public static final String NO_MOVE_LOGIC = "Object does not have a MoveLogic.";

    private boolean setNext = false;
    private MoveLogic moveLogic;
    private String waypointName = VJXString.STR_EMPTY;

    @Override
    public float execute(SequenceAction sequence) {
        moveLogic = (MoveLogic) executor.getLogic(VJXLogicType.MOVE);
        if (moveLogic == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, NO_MOVE_LOGIC);
            return 0f;
        }
        Waypoint wp;
        if (!setNext) wp = Waypoints.getWaypoint(waypointName);
        else wp = moveLogic.nextWaypoint();

        sequence.addAction(EJXActions.setWPAction(wp, executor));
        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        setNext = false;
        if (split.length < 3) {
            if (split[1].equals(CMD.NEXT_WP.name)) {
                setNext = true;

                if (split.length > 2) {
                    VJXLogger.log(LogCategory.WARNING,
                                    EJXError.WARNING_PARAMS_COUNT_0 + cmdString);
                }
                return 0;
            }
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1
                            + VALID_COMMAND);
            return -1;
        }
        waypointName = split[2];

        if (split.length > 3) {
            VJXLogger.log(LogCategory.WARNING,
                            EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
        }
        return 0;
    }

    @Override
    public void setExecutor(EJXEntity executor) {
        super.setExecutor(executor);
        if (!executor.hasLogic(VJXLogicType.MOVE))
            VJXLogger.log(LogCategory.WARNING, NO_MOVE_LOGIC);
        else moveLogic = (MoveLogic) executor.getLogic(VJXLogicType.MOVE);
    }

    public void setWaypointName(String waypointName) {
        this.waypointName = waypointName;
    }

    public void setNextWP(boolean setNext) {
        this.setNext = setNext;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + setNext
                        + VJXString.SEP_LIST_SPACE + waypointName;
    }
}