package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class AnimCommand extends Command {

    private static final String ERROR_ANIM_NOT_FOUND = " Target obejct does not have the specified animation.";
    private static final String NO_ANIMATED_OBJECT = "Target object is not an instance of AnimatedObject.";
    private static final String VALID_COMMAND = " set_anim:<string_animation>(:<bool_loop>)";

    private AnimatedEntity animatedObject;
    private String animName = Activity.IDLE.name;
    private boolean loop = false;

    @Override
    public float execute(SequenceAction sequence) {
        sequence.addAction(EJXActions.setAnimation(animName, loop, animatedObject));
        return animatedObject.getAnimDuration(animName);
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        if (!(executor instanceof AnimatedEntity)) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, NO_ANIMATED_OBJECT);
            return -1;
        }
        animatedObject = (AnimatedEntity) executor;

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1_2
                            + VALID_COMMAND);
            return -1;
        }

        if (execType != ExecutorType.THIS
                        && !animatedObject.hasAnim(split[2])) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + ERROR_ANIM_NOT_FOUND);
            return -1;
        }
        animName = split[2];

        if (split.length > 3) {
            if (!split[3].matches(EJXRegEx.matchBool)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                + EJXError.ERROR_INVALID_PARAM_BOOL);
                return -1;
            }
            loop = Boolean.parseBoolean(split[3]);

            if (split.length > 4) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_2 + cmdString);
            }
        }
        return 0;
    }

    @Override
    public void setExecutor(EJXEntity executor) {
        super.setExecutor(executor);

        if (!(executor instanceof AnimatedEntity)) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, NO_ANIMATED_OBJECT);
            return;
        }
        animatedObject = (AnimatedEntity) executor;
    }

    public void setAnimName(String animName) {
        this.animName = animName;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + animName
                        + VJXString.SEP_LIST_SPACE + loop;
    }
}
