package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class ActivityCommand extends Command {

    private static final String ERROR_ACTIVITY_NOT_FOUND = " Activity not found.";
    private static final String VALID_COMMAND = " set_activity:<string_activity>";

    private Activity activity = Activity.IDLE;

    @Override
    public float execute(SequenceAction sequence) {
        sequence.addAction(EJXActions.setActivity(activity, executor));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1
                            + VALID_COMMAND);
            return -1;
        }

        activity = Activity.get(split[2]);

        if (activity == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + ERROR_ACTIVITY_NOT_FOUND);
            return -1;
        }

        if (split.length > 3) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
        }
        return 0;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + activity;
    }
}