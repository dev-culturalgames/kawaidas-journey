package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class DespawnCommand extends Command {

    private static final String VALID_COMMAND = " despawn|despawnChildren";

    private boolean children;

    @Override
    public float execute(SequenceAction sequence) {
        if (children)
            executor.despawnChildren();
        else sequence.addAction(EJXActions.setLifecycle(Lifecycle.DESPAWN, executor));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);

        children = false;
        if (split[1].equals(CMD.DESPAWN_CHILDREN.name))
            children = true;

        if (split.length > 2) {
            VJXLogger.log(LogCategory.WARNING,
                            EJXError.WARNING_PARAMS_COUNT_0 + cmdString);
        }
        return 0;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND;
    }
}