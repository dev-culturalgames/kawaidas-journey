package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class AlphaCommand extends Command {

    private static final String VALID_COMMAND = " set_alpha:<float_alpha>(:<float_duration>)";

    private float targetAlpha = 1f;
    private float duration = 0f;

    @Override
    public float execute(SequenceAction sequence) {
        sequence.addAction(Actions.alpha(targetAlpha, duration));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1_2
                            + VALID_COMMAND);
            return -1;
        }

        if (!split[2].matches(EJXRegEx.matchFloat)) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + EJXError.ERROR_INVALID_PARAM_FLOAT);
            return -1;
        }
        targetAlpha = Float.parseFloat(split[2]);

        if (split.length > 3) {
            if (!split[3].matches(EJXRegEx.matchFloat)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                + EJXError.ERROR_INVALID_PARAM_FLOAT);
                return -1;
            }
            duration = Float.parseFloat(split[3]);

            if (split.length > 4) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_2 + cmdString);
            }
        }
        return 0;
    }

    public void setAlpha(float alpha) {
        targetAlpha = alpha;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + targetAlpha
                        + VJXString.SEP_LIST_SPACE + duration;
    }
}