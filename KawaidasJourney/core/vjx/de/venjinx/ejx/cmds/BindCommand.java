package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class BindCommand extends Command {

    private static final String VALID_COMMAND = " bind|unbind:<string_objectName>";

    private boolean bind = true;
    private EJXEntity target;

    @Override
    public float execute(SequenceAction sequence) {
        if (target == null) {
            VJXLogger.log(LogCategory.WARNING,
                            EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, TARGET_NOT_SET);
            return 0f;
        }
        sequence.addAction(EJXActions.bindAction(executor, bind, target));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1
                            + VALID_COMMAND);
            return -1;
        }
        bind = split[1].equals(CMD.BIND.name);

        target = gameControl.getEntityByName(split[2]);
        if (target == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + TARGET_NOT_FOUND);
            return -1;
        }

        if (split.length > 3) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
        }
        return 0;
    }

    public void setTarget(EJXEntity target) {
        this.target = target;
    }

    public void setBind(boolean bind) {
        this.bind = bind;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + bind
                        + VJXString.SEP_LIST_SPACE + target;
    }
}