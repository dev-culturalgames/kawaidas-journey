package de.venjinx.ejx.cmds;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.entity.EJXGroup;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class SpawnCommand extends Command {

    private static final String VALID_COMMAND = "spawn:<string_defName>(:<string_position>)(:<string_layerName>)(:<bool_behindParent>)";

    private String defName;
    private String layerName;
    private Vector2 position = new Vector2();;
    private EJXGroup layerNode;
    private boolean behindParent = false;

    @Override
    public float execute(SequenceAction sequence) {
        sequence.addAction(EJXActions.spawnEntity(defName, position,
                        executor, layerNode, behindParent, gameControl.getGame()));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1_4
                            + VALID_COMMAND);
            return -1;
        }

        // check parameter 1 definition name
        if (!gameControl.getGame().factory.hasEntityDef(split[2])) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + EJXError.ERROR_INVALID_PARAM_DEF_NOT_FOUND);
            return -1;
        }
        defName = split[2];

        // default
        position.setZero();
        layerNode = gameControl.getWorldLayer();
        if (executor != null) {
            position.set(executor.getWorldCenter());
            layerNode = executor.getLayer();
            behindParent = false;
        }

        // check parameter 2 position
        if (split.length > 3) {
            if (!split[3].isEmpty()) {
                Vector2 pos = gameControl.parsePoint(split[3], position);
                if (pos == null) {
                    VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                    VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                    + EJXError.ERROR_INVALID_PARAM_POSITION);
                    return -1;
                }
            }

            // check parameter 3 layer name
            if (split.length > 4) {
                if (!split[4].isEmpty()) {
                    if (gameControl.getLevel().getMap().getLayers().get(split[4]) == null) {
                        VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                        VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS3
                                        + EJXError.ERROR_INVALID_PARAM_LAYER_NOT_FOUND);
                        return -1;
                    }
                    layerNode = gameControl.getGroup(layerName);
                }

                // check parameter 4 spawn behind or in front of parent
                if (split.length > 5) {
                    if (!split[5].matches(EJXRegEx.matchBool)) {
                        VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                        VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                        + EJXError.ERROR_INVALID_PARAM_BOOL);
                        return -1;
                    }
                    behindParent = Boolean.parseBoolean(split[5]);

                    if (split.length > 6) {
                        VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_4 + cmdString);
                    }
                }
            }
        }
        return 0;
    }

    public void setPosition(Vector2 position) {
        setPosition(position.x, position.y);
    }

    public void setPosition(float x, float y) {
        position.set(x, y);
    }


    public void setDefName(String defName) {
        this.defName = defName;
    }


    public void setLayerNode(EJXGroup layerNode) {
        this.layerNode = layerNode;
    }

    public void setSpawnBehindParent(boolean behind) {
        behindParent = behind;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + defName
                        + VJXString.SEP_LIST_SPACE + position
                        + VJXString.SEP_LIST_SPACE + layerNode.getName()
                        + VJXString.SEP_LIST_SPACE + behindParent;
    }
}