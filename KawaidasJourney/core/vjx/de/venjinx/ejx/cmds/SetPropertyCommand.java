package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics;
import de.venjinx.ejx.entity.logics.EJXLogics.EJXLogicType;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXProperty;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class SetPropertyCommand extends Command {

    private static final String VALID_COMMAND = " restore|set:<string_variable>(:<mixed_value>)";
    private static final String LOGIC_NOT_REGISTERED = " LogicType not registered: ";
    private static final String LOGIC_NOT_FOUND = " Object does not have the specified logic: ";
    private static final String LOGIC_PROPERTY_NOT_FOUND = " Logic does not have the specified property: ";

    private Object value;
    private EJXLogicType logicType;
    private String propertyName;
    private boolean restore;

    @Override
    public float execute(SequenceAction sequence) {
        EJXLogic logic = executor.getLogic(logicType);
        if (logic == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + LOGIC_NOT_FOUND + logicType.getName());
            return -1;
        }

        EJXProperty<?> property = logic.getProperty(propertyName);
        if (property == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + LOGIC_PROPERTY_NOT_FOUND + propertyName);
            return -1;
        }

        if (restore) property.restore();
        else property.set(value);
        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_2_3
                            + VALID_COMMAND);
            return -1;
        }

        if (!split[2].matches(EJXRegEx.matchCmdVariable)) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + EJXError.ERROR_INVALID_PARAM_VARIABLE);
            return -1;
        }

        String logicName = split[2].split(VJXString.SEP_DOT)[0];

        logicType = EJXLogics.getType(logicName);
        if (logicType == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + LOGIC_NOT_REGISTERED + split[2]);
            return -1;
        }
        propertyName = split[2].replace(".", VJXString.SEP_DDOT);

        restore = split[1].equals(CMD.RESTORE.name);

        if (!restore) {
            if (split.length < 4) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_3
                                + VALID_COMMAND);
                return -1;
            }

            if (!split[3].matches(EJXRegEx.matchFloat)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                + EJXError.ERROR_INVALID_PARAM_FLOAT);
                return -1;
            }
            value = Float.parseFloat(split[3]);

            if (split.length > 4) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_2 + cmdString);
            }
        } else {
            if (split.length > 3) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
            }
        }

        return 0;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + propertyName
                        + VJXString.SEP_LIST_SPACE + value;
    }
}