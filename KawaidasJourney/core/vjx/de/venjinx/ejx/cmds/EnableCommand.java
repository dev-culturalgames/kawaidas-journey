package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class EnableCommand extends Command {

    private static final String VALID_COMMAND = " enable|disable";

    private boolean enable = true;

    @Override
    public float execute(SequenceAction sequence) {
        if (enable)
            sequence.addAction(EJXActions.setActivity(Activity.IDLE, executor));
        else
            sequence.addAction(EJXActions.setActivity(Activity.DISABLED, executor));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        enable = split[1].equals(CMD.ENABLE.name);

        if (split.length > 2) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_0 + cmdString);
        }
        return 0;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + enable;
    }
}