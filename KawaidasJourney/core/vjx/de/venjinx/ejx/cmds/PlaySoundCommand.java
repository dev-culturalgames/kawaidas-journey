package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class PlaySoundCommand extends Command {

    private static final String VALID_COMMAND0 = " play_sfx:<string_sfxName>(:<boolean_loop>)(:<float_volume>)(:<float_pitch>)";
    private static final String VALID_COMMAND1 = " stop_sfx";

    private enum SoundCMDType {
        PLAY, STOP
    }

    private SoundCMDType type;
    private String sfxName = VJXString.STR_EMPTY;
    private boolean loop = false;
    private float volume = -1f;
    private float pitch = -1f;

    @Override
    public float execute(SequenceAction sequence) {
        switch (type) {
            case PLAY:
                if (execType == ExecutorType.GAME)
                    gameControl.playSfx(VJXString.PRE_sfx + sfxName, loop, volume,
                                    pitch);
                else executor.playSfx(executor.getDefName() + VJXString.FILL_sfx
                                + sfxName, loop);
                break;
                //            case STOP:
                //                if (execType == ExecutorType.GAME)
                //                    listener.stopSfx(VJXString.PRE_sfx + sfxName);
                //                else executor.stopSfx(executor.getDefName() + VJXString.FILL_sfx
                //                                + sfxName);
                //                break;
            default:
        }
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);

        type = SoundCMDType.PLAY;
        if (split[1].equals(VALID_COMMAND1)) type = SoundCMDType.STOP;

        if (type == SoundCMDType.PLAY && split.length < 3) {
            if (split.length < 3) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1_4
                                + VALID_COMMAND0);
                return -1;
            }
        }
        sfxName = split[2];

        // defaults
        loop = false;
        volume = -1f;
        pitch = -1f;

        // check parameter loop
        if (split.length > 3) {
            if (!split[3].isEmpty()) {
                if (!split[3].matches(EJXRegEx.matchBool)) {
                    VJXLogger.log(LogCategory.ERROR,
                                    ERROR_PARSING_COMMAND + cmdString);
                    VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                    + EJXError.ERROR_INVALID_PARAM_BOOL);
                    return -1;
                }
                loop = Boolean.parseBoolean(split[3]);
            }

            // check parameter volume
            if (split.length > 4) {
                if (!split[4].isEmpty()) {
                    if (!split[4].matches(EJXRegEx.matchFloat)) {
                        VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                        VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                        + EJXError.ERROR_INVALID_PARAM_FLOAT);
                        return -1;
                    }
                    volume = Float.parseFloat(split[4]);
                }

                // check parameter pitch
                if (split.length > 5) {
                    if (!split[5].isEmpty()) {
                        if (!split[5].matches(EJXRegEx.matchFloat)) {
                            VJXLogger.log(LogCategory.ERROR,
                                            ERROR_PARSING_COMMAND + cmdString);
                            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS3
                                            + EJXError.ERROR_INVALID_PARAM_FLOAT);
                            return -1;
                        }
                        pitch = Float.parseFloat(split[5]);
                    }

                    if (split.length > 6) {
                        VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_4 + cmdString);
                    }
                }
            }
        }
        return 0;
    }

    public void setSFXName(String sfxName) {
        this.sfxName = sfxName;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public void setVolume(float volume) {
        VJXLogger.log("vol " + volume, 1);
        this.volume = volume;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    @Override
    public String toString() {
        if (type == SoundCMDType.PLAY) { return executor + VALID_COMMAND0
                        + VJXString.SEP_DDOT_SPACE + sfxName
                        + VJXString.SEP_LIST_SPACE + loop
                        + VJXString.SEP_LIST_SPACE + volume
                        + VJXString.SEP_LIST_SPACE + pitch;
        }
        return executor + VALID_COMMAND1 + VJXString.SEP_DDOT_SPACE + sfxName;
    }
}