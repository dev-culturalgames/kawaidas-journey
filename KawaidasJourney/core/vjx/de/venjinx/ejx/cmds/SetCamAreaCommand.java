package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class SetCamAreaCommand extends Command {

    private static final String MIN_X = "minX";
    private static final String MAX_X = "maxX";
    private static final String MIN_Y = "minY";
    private static final String MAX_Y = "maxY";
    private static final String AREA = "area";

    private static final String VALID_COMMAND0 = " set_cam_area_minX:<float_minX>";
    private static final String VALID_COMMAND1 = " set_cam_area_maxX:<float_maxX>";
    private static final String VALID_COMMAND2 = " set_cam_area_minY:<float_minY>";
    private static final String VALID_COMMAND3 = " set_cam_area_maxY:<float_maxY>";
    private static final String VALID_COMMAND = " set_cam_area:<float_minX>:<float_minY>:<float_maxX>:<float_maxY>";

    private String variant = AREA;

    private float minX;
    private float maxX;
    private float minY;
    private float maxY;

    @Override
    public float execute(SequenceAction sequence) {
        EJXCamera cam = (EJXCamera) gameControl.getLevel().getCamera();
        cam.setMoveArea(minX, maxX, minY, maxY);
        switch (variant) {
            case MIN_X:
                cam.setMoveArea(minX,
                                Float.POSITIVE_INFINITY,
                                Float.NEGATIVE_INFINITY,
                                Float.NEGATIVE_INFINITY);
                break;
            case MAX_X:
                cam.setMoveArea(Float.POSITIVE_INFINITY,
                                maxX,
                                Float.POSITIVE_INFINITY,
                                Float.NEGATIVE_INFINITY);
                break;
            case MIN_Y:
                cam.setMoveArea(Float.POSITIVE_INFINITY,
                                Float.NEGATIVE_INFINITY,
                                minY,
                                Float.NEGATIVE_INFINITY);
                break;
            case MAX_Y:
                cam.setMoveArea(Float.POSITIVE_INFINITY,
                                Float.POSITIVE_INFINITY,
                                Float.NEGATIVE_INFINITY,
                                maxY);
                break;
            default:
                cam.setMoveArea(minX, maxX, minY, maxY);
                break;
        }
        return 0;
    }

    @Override
    public void reset() {
        super.reset();
        minX = Float.POSITIVE_INFINITY;
        minY = Float.POSITIVE_INFINITY;
        maxX = Float.NEGATIVE_INFINITY;
        maxY = Float.NEGATIVE_INFINITY;
    }

    @Override
    protected int parse(String cmdString) {
        reset();
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        String[] cmdSplit = split[1].split(VJXString.SEP_USCORE);
        if (cmdSplit.length > 3) {
            if (split.length < 3) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1
                                + VALID_COMMAND);
                return -1;
            }

            if (!split[2].matches(EJXRegEx.matchFloat)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + EJXError.ERROR_INVALID_PARAM_FLOAT);
                return -1;
            }
            switch (cmdSplit[3]) {
                case MIN_X:
                    variant = MIN_X;
                    minX = Float.parseFloat(split[2]);
                    break;
                case MAX_X:
                    variant = MAX_X;
                    maxX = Float.parseFloat(split[2]);
                    break;
                case MIN_Y:
                    variant = MIN_Y;
                    minY = Float.parseFloat(split[2]);
                    break;
                case MAX_Y:
                    variant = MAX_Y;
                    maxY = Float.parseFloat(split[2]);
                    break;
                default:
                    break;
            }

            if (split.length > 3) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_4 + cmdString);
            }
        } else {
            variant = AREA;
            if (split.length < 6) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_4
                                + VALID_COMMAND);
                return -1;
            }

            if (split[2].matches(EJXRegEx.matchSkip))
                minX = Float.POSITIVE_INFINITY;
            else {
                if (!split[2].matches(EJXRegEx.matchFloat)) {
                    VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                    VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                    + EJXError.ERROR_INVALID_PARAM_FLOAT);
                    return -1;
                }
                minX = Float.parseFloat(split[2]);
            }

            if (split[2].matches(EJXRegEx.matchSkip))
                minX = Float.POSITIVE_INFINITY;
            else {
                if (!split[3].matches(EJXRegEx.matchFloat)) {
                    VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                    VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                    + EJXError.ERROR_INVALID_PARAM_FLOAT);
                    return -1;
                }
                maxX = Float.parseFloat(split[3]);
            }

            if (split[4].matches(EJXRegEx.matchSkip))
                minY = Float.POSITIVE_INFINITY;
            else {
                if (!split[4].matches(EJXRegEx.matchFloat)) {
                    VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                    VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS3
                                    + EJXError.ERROR_INVALID_PARAM_FLOAT);
                    return -1;
                }
                minY = Float.parseFloat(split[4]);
            }

            if (split[5].matches(EJXRegEx.matchSkip))
                maxY = Float.NEGATIVE_INFINITY;
            else {
                if (!split[5].matches(EJXRegEx.matchFloat)) {
                    VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                    VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS4
                                    + EJXError.ERROR_INVALID_PARAM_FLOAT);
                    return -1;
                }
                maxY = Float.parseFloat(split[5]);
            }

            if (split.length > 6) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_4 + cmdString);
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        switch (variant) {
            case MIN_X:
                return executor + VALID_COMMAND0 + VJXString.SEP_DDOT_SPACE + minX;
            case MAX_X:
                return executor + VALID_COMMAND1 + VJXString.SEP_DDOT_SPACE + maxX;
            case MIN_Y:
                return executor + VALID_COMMAND2 + VJXString.SEP_DDOT_SPACE + minY;
            case MAX_Y:
                return executor + VALID_COMMAND3 + VJXString.SEP_DDOT_SPACE + maxY;
            default:
                return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + minX
                                + VJXString.SEP_LIST_SPACE + minY
                                + VJXString.SEP_LIST_SPACE + maxX
                                + VJXString.SEP_LIST_SPACE + maxY;
        }
    }
}