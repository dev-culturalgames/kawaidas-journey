package de.venjinx.ejx.cmds;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.EJXGameScreen;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.tiled.EJXTiledMap;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.ejx.util.loader.EJXLoader;
import de.venjinx.ejx.util.loader.FinalTask;

public class LoadLevelCommand extends Command {

    private static final String ERROR_FILE_NOT_FOUND = " File not found: ";
    private static final String VALID_COMMAND = " un-/loadLevel(:<string_levelName>)";

    private String levelName;
    private FileHandle levelFile;
    private boolean unload;

    @Override
    public float execute(SequenceAction sequence) {
        final EJXGame game = gameControl.getGame();
        final EJXGameScreen screen = game.getScreen();
        EJXLoader loader = game.loader;

        screen.getMenu().setRunBackground(true);
        screen.getLoader().setRunBackground(false);
        if (unload) {
            EJXTiledMap map = screen.getLevelStage().getMap();
            game.tFactory.unloadMap(map);
            game.assets.finishLoading();

            gameControl.getMenu().restoreUI();
            screen.getLevelStage().setRunBackground(true);
            screen.getMenu().setRunBackground(false);
            screen.getLoader().setRunBackground(true);
        } else {
            gameControl.getMenu().switchMenu(-1, 0, 0);
            game.tFactory.newMapTask(levelFile, true);
            loader.addTask(new FinalTask(new Runnable() {

                @Override
                public void run() {
                    screen.getLevelStage().load(levelName);

                    screen.getLevelStage().setRunBackground(false);
                    screen.getMenu().setRunBackground(false);
                    screen.getLoader().setRunBackground(true);
                }
            }));
        }
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        unload = split[1].equals(CMD.UNLOAD_LEVEL.name);
        if (!unload && split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1
                            + VALID_COMMAND);
            return -1;
        }

        if (unload) return 0;

        levelName = split[2];
        levelFile = new FileHandle(
                        "data/level/" + levelName + VJXString.FILE_TMX);
        if (levelFile == null || !levelFile.exists()) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + ERROR_FILE_NOT_FOUND + levelFile.path());
            return -1;
        }

        if (split.length > 3) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
        }
        return 0;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + levelName;
    }
}