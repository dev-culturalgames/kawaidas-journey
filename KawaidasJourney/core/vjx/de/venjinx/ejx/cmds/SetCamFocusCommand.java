package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class SetCamFocusCommand extends Command {

    private static final String VALID_COMMAND = " set_cam_focus:<string_objectName>(:<boolean_usePlayerY>)";

    private EJXEntity target;
    private boolean useY;

    @Override
    public float execute(SequenceAction sequence) {
        if (target == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, TARGET_NOT_SET);
            return 0f;
        }
        gameControl.getLevel().setCamFocusObject(target, useY);
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_2
                            + VALID_COMMAND);
            return -1;
        }

        target = gameControl.getEntityByName(split[2]);
        if (target == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + TARGET_NOT_FOUND);
            return -1;
        }

        useY = false;
        if (split.length > 3) {
            if (!split[3].matches(EJXRegEx.matchBool)) {
                VJXLogger.log(LogCategory.ERROR,
                                ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                + EJXError.ERROR_INVALID_PARAM_BOOL);
                return -1;
            }
            useY = Boolean.parseBoolean(split[3]);

            if (split.length > 4) {
                VJXLogger.log(LogCategory.WARNING,
                                EJXError.WARNING_PARAMS_COUNT_2 + cmdString);
            }
        }
        return 0;
    }

    public void setTarget(EJXEntity target) {
        this.target = target;
    }

    public void setUseY(boolean useY) {
        this.useY = useY;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + target
                        + VJXString.SEP_LIST_SPACE + useY;
    }
}