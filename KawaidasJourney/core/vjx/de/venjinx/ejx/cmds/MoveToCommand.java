package de.venjinx.ejx.cmds;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class MoveToCommand extends Command {

    private static final String VALID_COMMAND = " moveTo:<string_position>";
    public static final String NO_MOVE_LOGIC = "Object does not have a MoveLogic.";

    private MoveLogic moveLogic;
    private Vector2 position = new Vector2();

    @Override
    public float execute(SequenceAction sequence) {
        if (moveLogic == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, NO_MOVE_LOGIC);
            return 0f;
        }
        moveLogic.setDestination(position);
        moveLogic.startMove();

        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1
                            + VALID_COMMAND);
            return -1;
        }

        position.setZero();
        Vector2 pos = gameControl.parsePoint(split[2], position);
        if (pos == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + EJXError.ERROR_INVALID_PARAM_POSITION);
            return -1;
        }

        if (split.length > 3) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
        }
        return 0;
    }

    @Override
    public void setExecutor(EJXEntity executor) {
        super.setExecutor(executor);
        if (!executor.hasLogic(VJXLogicType.MOVE))
            VJXLogger.log(LogCategory.WARNING, NO_MOVE_LOGIC);
        else moveLogic = (MoveLogic) executor.getLogic(VJXLogicType.MOVE);
    }

    public void setPosition(float x, float y) {
        position.set(x, y);
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + position;
    }
}