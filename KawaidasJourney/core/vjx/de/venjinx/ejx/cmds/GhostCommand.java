package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class GhostCommand extends Command {

    private static final String VALID_COMMAND = " set_ghost(:<bool_enabled>)";
    private static final String NO_BOX2D_LOGIC = "Object does not have a Box2DLogic.";

    private Box2DLogic physicsLogic;
    private boolean enable = true;

    @Override
    public float execute(SequenceAction sequence) {
        if (physicsLogic == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, NO_BOX2D_LOGIC);
            return 0f;
        }
        physicsLogic.setGhost(enable);
        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        enable = true;
        if (split.length >= 3) {
            if (!split[2].matches(EJXRegEx.matchBool)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + EJXError.ERROR_INVALID_PARAM_BOOL);
                return -1;
            }
            enable = Boolean.parseBoolean(split[2]);

            if (split.length > 3) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
            }
        }
        return 0;
    }

    @Override
    public void setExecutor(EJXEntity executor) {
        super.setExecutor(executor);
        if (executor.getPhysics() == null)
            VJXLogger.log(LogCategory.WARNING, NO_BOX2D_LOGIC);
        else physicsLogic = executor.getPhysics();
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + enable;
    }
}