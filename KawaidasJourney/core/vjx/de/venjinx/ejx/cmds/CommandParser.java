package de.venjinx.ejx.cmds;

import java.util.HashMap;

import com.badlogic.gdx.maps.MapProperties;

import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EntityCallbacks.VJXCallback;
import de.venjinx.ejx.entity.component.CommandComponent;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class CommandParser {

    private static HashMap<CMD, Class<? extends Command>> commands = new HashMap<>();

    public static void registerCommand(CMD command, Class<? extends Command> clazz) {
        if (!commands.containsKey(command)) commands.put(command, clazz);
        else VJXLogger.log(LogCategory.INFO,
                        "Command '" + command + "' already registered.");
    }

    public static Command parseCmd(String cmdString, EJXEntity parsingEntity,
                    GameControl listener) {
        if (cmdString == null) {
            VJXLogger.log(LogCategory.ERROR, Command.ERROR_STRING_NULL);
            return null;
        }

        if (!cmdString.matches(EJXRegEx.matchCommand)) {
            VJXLogger.log(LogCategory.ERROR,
                            Command.ERROR_PARSING_COMMAND + "" + cmdString);
            VJXLogger.log(LogCategory.ERROR, Command.ERROR_INVALID_FORMAT);
            return null;
        }

        String[] split = cmdString.split(VJXString.SEP_DDOT);

        EJXEntity executor = checkExecutor(split[0], parsingEntity, listener);
        if (executor == null) {
            VJXLogger.log(LogCategory.ERROR, Command.ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, Command.ERROR_INVALID_EXECUTOR
                            + Command.VALID_EXECUTOR_VALUES);
            return null;
        }

        CMD commandType = CMD.get(split[1]);
        if (!commands.containsKey(commandType)) {
            VJXLogger.log(LogCategory.ERROR, Command.ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, Command.ERROR_INVALID_COMMAND);
            return null;
        }

        Command command = Commands.command(commands.get(commandType));
        command.setTargetString(split[0]);
        command.setExecutor(executor);
        command.setController(listener);
        if (command.parse(cmdString) == -1) return null;
        return command;
    }

    //    public static final Command[] getCommands(VJXCallback type,
    //                    MapProperties properties, GameControl listener,
    //                    EJXEntity parsingEntity) {
    //        if (!properties.containsKey(type.name)) return null;
    //
    //        String cmdsStr = properties.get(type.name, String.class);
    //        if (cmdsStr == null) return null;
    //
    //        cmdsStr = cmdsStr.replace("\r", "");
    //
    //        return parseCommands(cmdsStr, parsingEntity, listener);
    //    }
    //
    //    private static Command[] parseCommands(String cmdString,
    //                    EJXEntity parsingEntity, GameControl listener) {
    //        String[] split = cmdString.split(VJXString.SEP_NEW_LINE_N);
    //        Command[] cmds = new Command[split.length];
    //        for (int i = 0; i < cmds.length; i++)
    //            cmds[i] = parseCmd(split[i], parsingEntity, listener);
    //        return cmds;
    //    }

    private static EJXEntity checkExecutor(String target,
                    EJXEntity parsingEntity, GameControl listener) {
        if (target.equals(VJXString.ACTOR_PLAYER))
            return listener.getPlayerEntity();
        if (target.equals(VJXString.ACTOR_TARGET) && parsingEntity != null)
            return parsingEntity.getTargetEntity();
        if (target.equals(VJXString.ACTOR_THIS)) return parsingEntity;
        if (target.equals(VJXString.ACTOR_GAME)) return parsingEntity;
        return listener.getEntityByName(target);
    }

    public static final Command[] getCommands(VJXCallback type,
                    MapProperties properties, GameControl listener,
                    CommandComponent parsingComponent) {
        if (!properties.containsKey(type.name)) return null;

        String cmdsStr = properties.get(type.name, String.class);
        if (cmdsStr == null) return null;

        cmdsStr = cmdsStr.replace("\r", "");

        return parseCommands(cmdsStr, parsingComponent, listener);
    }

    private static Command[] parseCommands(String cmdString,
                    CommandComponent parsingComponent, GameControl listener) {
        String[] split = cmdString.split(VJXString.SEP_NEW_LINE_N);
        Command[] cmds = new Command[split.length];
        for (int i = 0; i < cmds.length; i++)
            cmds[i] = parseCommand(split[i], listener, parsingComponent);
        return cmds;
    }

    private static Command parseCommand(String cmdString, GameControl listener,
                    CommandComponent parsingComponent) {
        if (cmdString.isEmpty()) return null;
        Command command = null;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        String actor = split[0];
        CommandComponent executor = checkExecutor(actor, parsingComponent, listener);
        command = parseCmd(cmdString, executor, listener);

        if (command != null) {
            command.setTargetString(actor);
            return command;
        }
        VJXLogger.log(LogCategory.ERROR,
                        "Parser " + parsingComponent + ", executor: " + cmdString);
        return null;
    }

    private static Command parseCmd(String cmdString,
                    CommandComponent parsingComponent, GameControl gameControl) {
        if (cmdString == null) {
            VJXLogger.log(LogCategory.ERROR, Command.ERROR_STRING_NULL);
            return null;
        }

        if (!cmdString.matches(EJXRegEx.matchCommand)) {
            VJXLogger.log(LogCategory.ERROR,
                            Command.ERROR_PARSING_COMMAND + "" + cmdString);
            VJXLogger.log(LogCategory.ERROR, Command.ERROR_INVALID_FORMAT);
            return null;
        }

        String[] split = cmdString.split(VJXString.SEP_DDOT);

        CommandComponent cmdComponent = checkExecutor(split[0], parsingComponent, gameControl);
        if (cmdComponent == null) {
            VJXLogger.log(LogCategory.ERROR,
                            Command.ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, Command.ERROR_INVALID_EXECUTOR
                            + Command.VALID_EXECUTOR_VALUES);
            return null;
        }

        CMD commandType = CMD.get(split[1]);
        if (!commands.containsKey(commandType)) {
            VJXLogger.log(LogCategory.ERROR,
                            Command.ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, Command.ERROR_INVALID_COMMAND);
            return null;
        }

        Command command = Commands.command(commands.get(commandType));
        command.setTargetString(split[0]);
        command.setCommandComponent(cmdComponent);
        command.setController(gameControl);
        if (command.parse(cmdString) == -1) return null;
        return command;
    }

    private static CommandComponent checkExecutor(String target,
                    CommandComponent parsingEntity, GameControl listener) {
        if (target.equals(VJXString.ACTOR_THIS)) return parsingEntity;
        if (target.equals(VJXString.ACTOR_GAME)) return parsingEntity;
        //                return listener.getEntityByName(target);
        return parsingEntity;
    }
}