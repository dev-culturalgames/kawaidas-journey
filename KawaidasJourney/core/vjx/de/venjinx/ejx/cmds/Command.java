package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Pool.Poolable;

import de.venjinx.ejx.cmds.Commands.EJXCmd;
import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.component.CommandComponent;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public abstract class Command implements Poolable {

    protected static final String ERROR_STRING_NULL = "Error parsing command: null";
    protected static final String ERROR_PARSING_COMMAND = "Error parsing command: ";
    protected static final String ERROR_INVALID_COMMAND = "Invalid command type. Command not registered.";
    protected static final String ERROR_INVALID_FORMAT = "Command format is <executor>:<cmdName>(:<param0>:...:<paramN>)";
    protected static final String ERROR_INVALID_EXECUTOR = "Invalid executor type or name.";
    protected static final String VALID_EXECUTOR_VALUES = " Valid values 'this', 'target', 'game', <string_objectName>.";

    protected static final String ERROR_PARAMS_NULL = "Error parsing command: Parameters is null.";

    protected static final String TARGET_NOT_SET = "Target object not set";
    protected static final String TARGET_NOT_FOUND = " Target object not found.";

    public enum ExecutorType {
        THIS("this"), TARGET("target"), NAME("name"),
        GAME("game");

        private String name;

        private ExecutorType(String name) {
            this.name = name;
        }

        public static final ExecutorType get(String name) {
            if (name == null || name.isEmpty()) return THIS;
            for (ExecutorType type : ExecutorType.values())
                if (type.name.equals(name)) return type;
            return NAME;
        }

        public String getName() {
            return name;
        }
    }

    protected ExecutorType execType = ExecutorType.THIS;
    protected GameControl gameControl;
    protected EJXEntity executor;
    protected CommandComponent commandComponent;

    protected String cmdString;
    protected String targetString;

    public void setTargetString(String targetString) {
        this.targetString = targetString;
        setType(ExecutorType.get(targetString));
    }

    public void updateTarget(EJXEntity executor) {
        if (executor == null || this.executor == executor) return;
        switch (execType) {
            case GAME:
            case THIS:
                setExecutor(targetString == null ? this.executor : executor);
                break;
            case TARGET:
                setExecutor(executor.getTargetEntity());
                break;
            case NAME:
                setExecutor(executor.getListener()
                                .getEntityByName(targetString));
                break;
            default:
        }
        if (this.executor == null) setExecutor(executor);
    }

    private void setType(ExecutorType type) {
        execType = type;
    }

    public ExecutorType getType() {
        return execType;
    }

    public void setExecutor(EJXEntity target) {
        executor = target;
        //        if (executor != null) gameControl = executor.getListener();
    }

    public EJXEntity getExecutor() {
        return executor;
    }

    public void setCommandComponent(CommandComponent commandComponent) {
        this.commandComponent = commandComponent;
    }

    public CommandComponent getComponent() {
        return commandComponent;
    }

    public void setController(GameControl listener) {
        gameControl = listener;
    }

    @Override
    public void reset() {
        executor = null;
        commandComponent = null;
        execType = ExecutorType.THIS;
        targetString = execType.name;
    }

    protected EJXEntity findTarget(String target) {
        if (target.equals(VJXString.ACTOR_PLAYER))
            return gameControl.getPlayerEntity();
        if (target.equals(VJXString.ACTOR_TARGET) && executor != null)
            return executor.getTargetEntity();
        if (target.equals(VJXString.ACTOR_THIS)) return executor;
        if (target.equals(VJXString.ACTOR_GAME)) return executor;
        return gameControl.getEntityByName(target);
    }

    public abstract float execute(SequenceAction sequence);

    /**
     * Parses the user defined parameters for a valid {@link Command}.
     * A valid command string looks like this:</br>
     * &lt;executor&gt;:&lt;cmdName&gt;(:&lt;param0&gt;: ... :&lt;paramN&gt;)</br>
     * <p>
     * &lt;executor&gt; is the name of an {@link EJXEntity} object. White spaces are allowed.</br>
     * &lt;cmdName&gt; is the string representation of an {@link EJXCmd} type. White spaces are not allowed.</br>
     * &lt;param0&gt;: ... :&lt;paramN&gt; are the user defined parameters for a command. White spaces are allowed.
     * <p>
     * Implementations of this method only need to handle the parameters as the
     * executing object and the command type are being parsed by the {@link CommandParser}.
     *
     * @param cmdString The command string to parse.
     * @return 0 if command has been parsed correctly or -1 if something
     * went wrong while parsing.
     * @see EJXCmd
     */
    protected abstract int parse(String cmdString);
}