package de.venjinx.ejx.cmds;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class SetPosCommand extends Command {

    private static final String VALID_COMMAND = " set_pos|add_pos:<string_position>";

    private boolean set = true;
    private Vector2 position = new Vector2();

    @Override
    public float execute(SequenceAction sequence) {
        if (!set) position.add(executor.getWorldPosition());
        sequence.addAction(EJXActions.setPosition(position, executor));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1
                            + VALID_COMMAND);
            return -1;
        }

        if (split[1].equals(CMD.ADD_POS.name)) set = false;
        else if (split[1].equals(CMD.SET_POS.name)) set = true;

        position = EJXUtil.parsePosition(split[2], position, gameControl.getLevel());
        if (position == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + EJXError.ERROR_INVALID_PARAM_POSITION);
            return -1;
        }

        if (split.length > 3) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
        }
        return 0;
    }

    public void setPosition(float x, float y) {
        position.set(x, y);
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + set
                        + VJXString.SEP_LIST_SPACE + position;
    }
}
