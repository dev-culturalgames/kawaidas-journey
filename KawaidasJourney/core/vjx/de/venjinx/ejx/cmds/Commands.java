package de.venjinx.ejx.cmds;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EJXGroup;
import de.venjinx.ejx.entity.EntityCallbacks.VJXCallback;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class Commands {

    public interface CmdInterface {
    }

    public enum EJXCmd implements CmdInterface {
        EMPTY("empty"),

        // global commands
        DELAY("delay"),

        // audio commands
        PLAY_SFX("play_sfx"), STOP_SFX("stop_sfx"),
        PLAY_AMB("play_amb"), STOP_AMB("stop_amb"),
        PLAY_MUSIC("play_music"), STOP_MUSIC("stop_music"),

        // animation commands
        VISIBLE("visible"), ALPHA("alpha"),
        SET_ANIM("set_anim"), ADD_ANIM("add_anim"),

        // world commands
        CONTROLS("controls"), SPAWN("spawn"),
        LOSE("lose"),
        WIN("win"), SET_CAM_FOCUS("set_cam_focus"),

        // object commands
        SET_SPEED_X("set_move_speedX"), SET_SPEED_Y("set_move_speedY"),
        SET_SPEED_MOD_X("set_move_speedModX"), SET_SPEED_MOD_Y("set_move_speedModY"),
        ENABLE("enable"), DISABLE("disable"), ATTACH("attachTo"), DETACH("detach"),
        LIFECYCLE("set_lifecycle"), ACTIVITY("set_activity"),
        MOVE("move"), MOVE_TO("moveTo"), STOP("stop"),
        NEXT_WP("next_wp"), PREV_WP("prev_wp"),
        SET_WP("set_wp"), SET_POS("set_pos"),
        SET_ORIENTATION("set_orientation"), RESTORE("restore"), DIE("die"),
        DESPAWN("despawn"), DESPAWN_CHILDREN("despawnChildren"),
        BIND("bind"), UNBIND("unbind"),

        // physics commands
        SET_GHOST("set_ghost"),

        ;

        public final String name;

        private EJXCmd(String n) {
            name = n;
        }

        public static EJXCmd get(String name) {
            for (EJXCmd command : EJXCmd.values())
                if (command.name.equals(name)) return command;
            VJXLogger.log(LogCategory.ERROR,
                            "Command '" + name + "' not found!");
            return EJXCmd.EMPTY;
        }
    }

    public enum CMD {
        EMPTY("empty"),

        // global commands
        DELAY("delay"),

        // audio commands
        PLAY_SFX("play_sfx"),
        STOP_SFX("stop_sfx"),

        // utility commands
        SECRET("secret"),
        HINT("hint"),
        MONO("monologue"),
        STORY("dialogue"),
        TOGGLE_UI("toggle_ui"),

        // animation commands
        ALPHA("set_alpha"),
        ANIMATION("set_anim"),
        APPEAR("appear"),
        DISAPPEAR("disappear"),

        // game commands
        SWITCH_MENU("switchMenu"),
        LOAD_LEVEL("loadLevel"),
        UNLOAD_LEVEL("unloadLevel"),
        CONTROLS("controls"),
        SPAWN("spawn"),
        DESPAWN("despawn"),
        DESPAWN_CHILDREN("despawnChildren"),
        BONUS("bonus"),
        LOSE("lose"),
        WIN("win"),
        SET_CAM_FOCUS("set_cam_focus"),
        SET_CAM_AREA("set_cam_area"),
        SET_CAM_AREA_MIN_X("set_cam_area_minX"),
        SET_CAM_AREA_MAX_X("set_cam_area_maxX"),
        SET_CAM_AREA_MIN_Y("set_cam_area_minY"),
        SET_CAM_AREA_MAX_Y("set_cam_area_maxY"),
        GROW_DECO("growDeco"),

        // object commands
        DAMAGE("damage"),
        DISABLE("disable"),
        ENABLE("enable"),
        SET_ORIENTATION("set_orientation"),
        SET("set"),
        RESTORE("restore"),
        BIND("bind"),
        UNBIND("unbind"),
        ATTACH("attachTo"),
        DETACH("detach"),
        LIFECYCLE("set_lifecycle"),
        ACTIVITY("set_activity"),
        ADD_POS("add_pos"),
        SET_POS("set_pos"),
        MOVE_TO("moveTo"),
        MOVE("move"),
        STOP("stop"),
        NEXT_WP("next_wp"),
        SET_WP("set_wp"),
        GHOST("ghost"),
        SET_GHOST("set_ghost"),
        DIE("die"),
        TELEPORT("teleport"),

        // character commands
        JUMP("jump"),
        JUMP2("jump2"),
        ATTACK("attack"),
        ATTACK_MOVE("attackMove"),
        THROW("throw"),
        PLANT("plant"),
        INCR_ITEM("incr_itm"),
        ;

        public final String name;

        private CMD(String n) {
            name = n;
        }

        public static CMD get(String name) {
            for (CMD command : CMD.values())
                if (command.name.equals(name))
                    return command;
            VJXLogger.log(LogCategory.ERROR, "Command '" + name + "' not found!", 1);
            return CMD.EMPTY;
        }
    }

    private static class CommandParser2 {

        private static Command parseCommand(String cmdString,
                        GameControl listener, EJXEntity parsingEntity) {
            if (cmdString.isEmpty()) return null;
            Command command = null;
            String[] split = cmdString.split(VJXString.SEP_DDOT);
            String actor = split[0];
            String cmdName = split[1];
            EJXEntity executor = checkExecutor(actor, parsingEntity, listener);
            CMD cmd = CMD.get(cmdName);
            switch (cmd) {
                //                case ACTIVITY:
                //                case ADD_POS:
                //                case ALPHA:
                //                case ANIMATION:
                //                case APPEAR:
                //                case ATTACH:
                //                case ATTACK:
                //                case BIND:
                //                case BONUS:
                //                case CONTROLS:
                //                case DAMAGE:
                //                case DELAY:
                //                case DESPAWN:
                //                case DETACH:
                //                case DIE:
                //                case DISAPPEAR:
                //                case DISABLE:
                //                case ENABLE:
                //                case GROW_DECO:
                //                case HINT:
                //                case INCR_ITEM:
                //                case JUMP:
                //                case JUMP2:
                //                case LIFECYCLE:
                //                case LOSE:
                //                case MOVE:
                //                case MOVE_TO:
                //                case NEXT_WP:
                //                case PLANT:
                //                case PLAY_SFX:
                //                case RESTORE:
                //                case STOP_SFX:
                //                case SECRET:
                //                case SET:
                //                case SET_CAM_FOCUS:
                //                case SET_GHOST:
                //                case SET_ORIENTATION:
                //                case SET_POS:
                //                case SET_WP:
                //                case SPAWN:
                //                case STOP:
                //                case STORY:
                //                case TELEPORT:
                //                case THROW:
                //                case TOGGLE_UI:
                //                case UNBIND:
                //                case WIN:
                //                    command = CommandParser.parseCmd(cmdString, parsingEntity, listener);
                //                    break;
                default:
                    command = CommandParser.parseCmd(cmdString, parsingEntity, listener);
            }

            if (command != null) {
                command.setTargetString(parseString(split, 0));
                return command;
            }
            VJXLogger.log(LogCategory.ERROR,
                            "Parser " + parsingEntity + ", executor " + executor
                            + ": " + cmdString);
            return null;
        }

        private static EJXEntity checkExecutor(String target,
                        EJXEntity parsingEntity, GameControl listener) {
            if (target.equals(VJXString.ACTOR_PLAYER))
                return listener.getPlayerEntity();
            if (target.equals(VJXString.ACTOR_TARGET) && parsingEntity != null)
                return parsingEntity.getTargetEntity();
            if (target.equals(VJXString.ACTOR_THIS)) return parsingEntity;
            if (target.equals(VJXString.ACTOR_GAME)) return parsingEntity;
            return listener.getEntityByName(target);
        }
    }

    public static final Command[] getCommands(VJXCallback type,
                    MapProperties properties, GameControl listener,
                    EJXEntity parsingEntity) {
        if (!properties.containsKey(type.name)) return null;

        String cmdsStr = properties.get(type.name, String.class);
        if (cmdsStr == null) return null;

        cmdsStr = cmdsStr.replace("\r", "");

        return parseCommands(cmdsStr, listener, parsingEntity);
    }

    private static Command[] parseCommands(String cmdString,
                    GameControl listener, EJXEntity parsingEntity) {
        String[] split = cmdString.split(VJXString.SEP_NEW_LINE_N);
        Command[] cmds = new Command[split.length];
        for (int i = 0; i < cmds.length; i++)
            cmds[i] = CommandParser2.parseCommand(split[i], listener,
                            parsingEntity);
        return cmds;
    }

    public static String parseString(String[] split, int ind) {
        if (split.length <= ind) return VJXString.STR_EMPTY;
        return split[ind];
    }

    public static boolean parseBool(String[] split, int ind) {
        if (split.length <= ind) return false;
        return Boolean.parseBoolean(split[ind]);
    }

    public static int parseInt(String[] split, int ind) {
        if (split.length <= ind) return 0;
        return Integer.parseInt(split[ind]);
    }

    public static float parseFloat(String[] split, int ind) {
        if (split.length <= ind) return 0f;
        return Float.parseFloat(split[ind]);
    }

    public static <T extends Command> T command(Class<T> type) {
        Pool<T> pool = Pools.get(type);
        T cmd = pool.obtain();
        //        cmd.setPool(pool);
        return cmd;
    }

    public static final void addOnCommand(VJXCallback callback,
                    String cmdString, MapProperties properties) {
        if (!resolve(callback, properties, cmdString)) return;

        String cmds = properties.get(callback.name, VJXString.STR_EMPTY,
                        String.class);

        if (!cmds.isEmpty()) cmds += VJXString.SEP_NEW_LINE_N;
        cmds += cmdString;

        properties.put(callback.name, cmds);
    }

    public static final void insertOnCommand(VJXCallback callback,
                    String cmdString, MapProperties properties, int index) {
        if (!resolve(callback, properties, cmdString)) return;

        String cmdsStr = properties.get(callback.name, "", String.class);
        if (cmdsStr.isEmpty()) {
            cmdsStr += cmdString;
            properties.put(callback.name, cmdString);
        } else {
            cmdsStr = cmdsStr.replace("\r", "");
            String[] strCmds = cmdsStr.split("\n");

            if (index >= strCmds.length) {
                cmdsStr += "\n" + cmdString;
                properties.put(callback.name, cmdsStr);
                return;
            }

            cmdsStr = "";
            if (index == 0) {
                cmdsStr += cmdString;
                index = -1;
            }

            for (int i = 0; i < strCmds.length; i++) {
                if (i == index) cmdsStr += "\n" + cmdString;
                cmdsStr += "\n" + strCmds[i];
            }
            properties.put(callback.name, cmdsStr);
        }
    }

    public static final void setCallbackCmds(VJXCallback callback,
                    String cmdString,
                    MapProperties properties) {
        if (!resolve(callback, properties, cmdString)) return;
        properties.put(callback.name, cmdString);
    }

    private static final boolean resolve(VJXCallback cb, MapProperties props,
                    String str) {
        if (cb == null || props == null || str == null || str.isEmpty()) {
            VJXLogger.log(LogCategory.ERROR, "Callback '" + cb
                            + "' modifcation failed. Command '" + str
                            + "': Needed paramater null or empty.", 2);
            return false;
        }
        return true;
    }

    public static LifecycleCommand getLifecycleCommand(EJXEntity executor,
                    Lifecycle lifecycle) {
        LifecycleCommand cmd = command(LifecycleCommand.class);
        cmd.setExecutor(executor);
        cmd.setLifecycle(lifecycle);
        return cmd;
    }

    public static ActivityCommand getActivityCommand(EJXEntity executor,
                    Activity activity) {
        ActivityCommand cmd = command(ActivityCommand.class);
        cmd.setExecutor(executor);
        cmd.setActivity(activity);
        return cmd;
    }

    public static EnableCommand getEnableCommand(EJXEntity executor,
                    boolean enable) {
        EnableCommand cmd = command(EnableCommand.class);
        cmd.setExecutor(executor);
        cmd.setEnable(enable);
        return cmd;
    }

    public static GhostCommand getGhostCommand(EJXEntity executor,
                    boolean enableGhost) {
        GhostCommand cmd = command(GhostCommand.class);
        cmd.setExecutor(executor);
        cmd.setEnable(enableGhost);
        return cmd;
    }

    public static BindCommand getBindCommand(EJXEntity executor,
                    EJXEntity target) {
        BindCommand cmd = command(BindCommand.class);
        cmd.setExecutor(executor);
        cmd.setBind(true);
        cmd.setTarget(target);
        return cmd;
    }

    public static BindCommand getUnbindCommand(EJXEntity executor,
                    EJXEntity target) {
        BindCommand cmd = command(BindCommand.class);
        cmd.setExecutor(executor);
        cmd.setBind(false);
        cmd.setTarget(target);
        return cmd;
    }

    public static AnimCommand getAnimCommand(AnimatedEntity executor,
                    String animName, boolean loop) {
        AnimCommand cmd = command(AnimCommand.class);
        cmd.setExecutor(executor);
        cmd.setAnimName(animName);
        cmd.setLoop(loop);
        return cmd;
    }

    public static WPCommand getWPCommand(EJXEntity executor, String wp,
                    boolean nextWP) {
        WPCommand cmd = command(WPCommand.class);
        cmd.setExecutor(executor);
        cmd.setWaypointName(wp);
        cmd.setNextWP(nextWP);
        return cmd;
    }

    public static MoveCommand getMoveCommand(EJXEntity executor, boolean stop,
                    boolean updateActivity) {
        MoveCommand cmd = command(MoveCommand.class);
        cmd.setExecutor(executor);
        cmd.setMove(!stop);
        cmd.setUpdateActivity(updateActivity);
        return cmd;
    }

    public static AlphaCommand getAlphaCommand(float alpha, float duration) {
        AlphaCommand cmd = command(AlphaCommand.class);
        cmd.setDuration(duration);
        cmd.setAlpha(alpha);
        return cmd;
    }

    // game commands
    public static DelayCommand getDelayCommand(float delay) {
        DelayCommand cmd = command(DelayCommand.class);
        cmd.setDelay(delay);
        return cmd;
    }

    public static SpawnCommand getSpawnCommand(EJXGame game, String defName,
                    EJXEntity parent, float x, float y) {
        return getSpawnCommand(game, defName, parent.getLayer(), parent, x, y,
                        false);
    }

    public static SpawnCommand getSpawnCommand(EJXGame game, String defName,
                    EJXEntity parent, float x, float y, boolean pre) {
        return getSpawnCommand(game, defName, parent.getLayer(), parent, x, y,
                        pre);
    }

    public static SpawnCommand getSpawnCommand(EJXGame game, String defName,
                    EJXGroup layer, EJXEntity executor, float x, float y,
                    boolean behindParent) {
        SpawnCommand cmd = command(SpawnCommand.class);
        cmd.setController(game.gameControl);
        cmd.setExecutor(executor);
        cmd.setDefName(defName);
        cmd.setLayerNode(layer);
        cmd.setPosition(x, y);
        cmd.setSpawnBehindParent(behindParent);
        return cmd;
    }
}