package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class ToggleUICommand extends Command {

    private static final String VALID_COMMAND = " toggle_ui:<list_uiElements>:<bool_on>";

    private String uiElementStrList = VJXString.STR_EMPTY;
    private boolean on = true;

    @Override
    public float execute(SequenceAction sequence) {
        gameControl.getUI().hideUIElements(uiElementStrList, on);
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_2
                            + VALID_COMMAND);
            return -1;
        }
        uiElementStrList = split[2];

        if (split.length > 3) {
            if (!split[3].matches(EJXRegEx.matchBool)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                + EJXError.ERROR_INVALID_PARAM_BOOL);
                return -1;
            }
            on = Boolean.parseBoolean(split[3]);
        }
        return 0;
    }

    public void setElementsToHide(String uiElementStrList) {
        this.uiElementStrList = uiElementStrList;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + uiElementStrList
                        + VJXString.SEP_LIST_SPACE + on;
    }

}
