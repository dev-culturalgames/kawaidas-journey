package de.venjinx.ejx.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class MoveCommand extends Command {

    private static final String ERROR_NO_MOVELOGIC = "Target has no MoveLogic.";
    private static final String VALID_COMMAND = " move|stop(:<bool_updateActivity>)";

    private boolean move = true;
    private boolean updateActivity = false;

    @Override
    public float execute(SequenceAction sequence) {
        sequence.addAction(EJXActions.startMove(executor, move, updateActivity));
        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        if (execType == ExecutorType.NAME
                        && !executor.hasLogic(VJXLogicType.MOVE)
                        || execType == ExecutorType.TARGET
                        && !executor.getTargetEntity().hasLogic(
                                        VJXLogicType.MOVE)) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, ERROR_NO_MOVELOGIC);
            return -1;
        }

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split[1].equals(CMD.STOP.name)) move = false;
        else if (split[1].equals(CMD.MOVE.name)) move = true;
        else {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, VALID_COMMAND);
            return -1;
        }

        updateActivity = false;
        if (split.length > 2) {
            if (!split[2].matches(EJXRegEx.matchBool)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + EJXError.ERROR_INVALID_PARAM_BOOL);
                return -1;
            }
            updateActivity = Boolean.parseBoolean(split[2]);

            if (split.length > 3) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
            }
        }

        return 0;
    }

    public void setMove(boolean move) {
        this.move = move;
    }

    public void setUpdateActivity(boolean updateActivity) {
        this.updateActivity = updateActivity;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + move
                        + VJXString.SEP_LIST_SPACE + updateActivity;
    }
}