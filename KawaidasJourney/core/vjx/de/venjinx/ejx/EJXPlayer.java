/*******************************************************************************
 * Copyright 2020 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package de.venjinx.ejx;

import com.badlogic.gdx.maps.MapProperties;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.PlayerEntity;
import de.venjinx.ejx.save.GameSettings;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.util.VJXLogger;

/**
 * The {@link EJXPlayer} class represents the current user of the game. It is
 * used to save player specific settings and properties as well as a reference
 * to the current {@link PlayerEntity}.
 *
 * @author Torge Rothe (X-Ray-Jin, xrayjin@gmx.de )
 *
 */
public abstract class EJXPlayer {

    private GameSettings settings;
    protected MapProperties properties;
    protected PlayerEntity entity;

    /**
     * Creates a new {@link EJXPlayer} instance with settings.
     *
     * @param settings
     *                 the player settings
     */
    public EJXPlayer(GameSettings settings) {
        this.settings = settings;
        properties = new MapProperties();
    }

    /**
     * Loads the player properties from a {@link LocalSavegame}.
     *
     * @param savegame
     *                 the savegame to load from
     */
    public abstract void loadStatsFromSave(LocalSavegame savegame);

    /**
     * Resets the current players level progress and properties to default.
     */
    public abstract void reset();

    /**
     * Clears the current level progress of the player.
     */
    public abstract void clearCurrentLevelProgress();

    /**
     * @return the current player {@link GameSettings} object.
     */
    public GameSettings getSettings() {
        return settings;
    }

    /**
     * Sets the current {@link PlayerEntity}.
     *
     * @param entity
     *               the new player entity
     * @see EJXEntity
     */
    public void setEntity(PlayerEntity entity) {
        this.entity = entity;
    }

    /**
     * @return the current {@link PlayerEntity} object.
     */
    public PlayerEntity getEntity() {
        return entity;
    }

    /**
     * Prints all player properties to console.
     */
    public void printSettings() {
        VJXLogger.printMapProperties(properties);
        VJXLogger.log("");
    }
}