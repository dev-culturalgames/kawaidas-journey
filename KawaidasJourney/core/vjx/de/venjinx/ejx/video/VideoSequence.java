package de.venjinx.ejx.video;

import com.badlogic.gdx.files.FileHandle;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.animation.SpineActor;

public class VideoSequence {

    private String name;
    private FileHandle file;
    private SpineActor videoActor;
    private SpineActor monkeyActor;
    private String title;

    public VideoSequence(String name, String title, FileHandle file) {
        this.name = name;
        this.title = title;
        this.file = file;
    }

    public void init(final EJXGame game, float width, float height,
                    final boolean loop) {
        videoActor = game.factory.createSpineActor(name, width, height, name, file, null);
        videoActor.setAnimation("videoSequence", loop);
        videoActor.updateSkeletonScale(width, height);
    }

    public SpineActor getActor() {
        return videoActor;
    }

    public SpineActor getMonkey() {
        return monkeyActor;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public FileHandle getFile() {
        return file;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}