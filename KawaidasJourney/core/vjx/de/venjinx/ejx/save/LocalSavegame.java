package de.venjinx.ejx.save;

import java.util.HashMap;

import com.badlogic.gdx.utils.Json;

import de.venjinx.ejx.EJXPlayer;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public abstract class LocalSavegame {

    public static final int INT_SAVE_PROTOTYPE_VERSION = 0;

    private static final String INT_SAVE_PROTOTYP_VERSION = "int_save_prototype_version";
    private static final String WARNING_OLD_GAME_VERSION = "Cannot load savegame. You are running an old game version.";
    private static final String WARNING_OLD_VERSION = "Resolving old save version.";
    private static final String INFO_INIT_SAVE = "Old save detected. Initiating new version.";

    private static final String LOCAL_SAVE_SAVING = "Local savegame saving: ";
    private static final String LOCAL_SAVE_SAVED = "Local savegame saved: ";
    private static final String LOCAL_SAVE_LOADING = "Local savegame loading: ";
    private static final String LOCAL_SAVE_LOADED = "Local savegame loaded: ";

    public static class SavegamePrototype {
        public HashMap<String, String> strings = new HashMap<>();
        public HashMap<String, Integer> ints = new HashMap<>();
        public HashMap<String, Long> longs = new HashMap<>();
        public HashMap<String, Float> floats = new HashMap<>();
        public HashMap<String, Boolean> bools = new HashMap<>();

        public void writeToSettings(GameSettings settings) {
            for (String key : bools.keySet())
                settings.putBool(key, bools.get(key));
            for (String key : ints.keySet())
                settings.putInt(key, ints.get(key));
            for (String key : longs.keySet())
                settings.putLong(key, longs.get(key));
            for (String key : floats.keySet())
                settings.putFloat(key, floats.get(key));
            for (String key : strings.keySet())
                settings.putString(key, strings.get(key));
        }

        public void copy(SavegamePrototype savePrototype) {
            strings.putAll(savePrototype.strings);
            ints.putAll(savePrototype.ints);
            longs.putAll(savePrototype.longs);
            floats.putAll(savePrototype.floats);
            bools.putAll(savePrototype.bools);
        }
    }

    protected SavegamePrototype prototype;
    protected GameSettings settings;

    private boolean skipWrite = false;

    public LocalSavegame(GameSettings playerSettings) {
        prototype = new SavegamePrototype();
        settings = playerSettings;
        setupPrototype();

        int version = getVersion();
        if (version < 0) {
            VJXLogger.log(LogCategory.INFO, INFO_INIT_SAVE);
            settings.putInt(INT_SAVE_PROTOTYP_VERSION, INT_SAVE_PROTOTYPE_VERSION);
        } else if (version < INT_SAVE_PROTOTYPE_VERSION) {
            VJXLogger.log(LogCategory.WARNING, WARNING_OLD_VERSION);
            resolveOldVersion(version);
        } else if (version > INT_SAVE_PROTOTYPE_VERSION) {
            VJXLogger.log(LogCategory.WARNING, WARNING_OLD_GAME_VERSION);
        }
    }

    public abstract void savePlayer(EJXPlayer player, boolean write);

    protected abstract void resolveOldVersion(int oldVersion);

    public void setSkipWrite(boolean skipWrite) {
        this.skipWrite = skipWrite;
    }

    public int getVersion() {
        return settings.getInt(INT_SAVE_PROTOTYP_VERSION, -1);
    }

    public void copy(SavegamePrototype copyFromPrototype) {
        if (copyFromPrototype == null) return;
        VJXLogger.log(LogCategory.INFO, LOCAL_SAVE_LOADING + getName());
        prototype.copy(copyFromPrototype);
        VJXLogger.log(LogCategory.INFO, LOCAL_SAVE_LOADED + getName());
    }

    public boolean write() {
        if (skipWrite) return false;
        VJXLogger.log(LogCategory.INFO, LOCAL_SAVE_SAVING + getName());
        for (String key : prototype.bools.keySet())
            settings.putBool(key, prototype.bools.get(key));
        for (String key : prototype.ints.keySet())
            settings.putInt(key, prototype.ints.get(key));
        for (String key : prototype.longs.keySet())
            settings.putLong(key, prototype.longs.get(key));
        for (String key : prototype.floats.keySet())
            settings.putFloat(key, prototype.floats.get(key));
        for (String key : prototype.strings.keySet())
            settings.putString(key, prototype.strings.get(key));

        settings.putInt(INT_SAVE_PROTOTYP_VERSION, INT_SAVE_PROTOTYPE_VERSION);
        VJXLogger.log(LogCategory.INFO, LOCAL_SAVE_SAVED + getName());
        return true;
    }

    public boolean getBool(String name, boolean def) {
        name = VJXString.PRE_bool_ + name;
        if (prototype.bools.containsKey(name))
            return prototype.bools.get(name);
        else return def;
    }

    public void putBool(String name, boolean value) {
        name = VJXString.PRE_bool_ + name;
        prototype.bools.put(name, value);
    }

    public int getInt(String name, int def) {
        name = VJXString.PRE_int_ + name;
        if (prototype.ints.containsKey(name))
            return prototype.ints.get(name);
        else return def;
    }

    public void putInt(String name, int value) {
        name = VJXString.PRE_int_ + name;
        prototype.ints.put(name, value);
    }

    public long getLong(String name, long def) {
        name = VJXString.PRE_long_ + name;
        if (prototype.longs.containsKey(name))
            return prototype.longs.get(name);
        else return def;
    }

    public void putLong(String name, long value) {
        name = VJXString.PRE_long_ + name;
        prototype.longs.put(name, value);
    }

    public float getFloat(String name, float def) {
        name = VJXString.PRE_float_ + name;
        if (prototype.floats.containsKey(name))
            return prototype.floats.get(name);
        else return def;
    }

    public void putFloat(String name, float value) {
        name = VJXString.PRE_float_ + name;
        prototype.floats.put(name, value);
    }

    public String getString(String name, String def) {
        name = VJXString.PRE_string_ + name;
        if (prototype.strings.containsKey(name))
            return prototype.strings.get(name);
        else return def;
    }

    public void putString(String name, String value) {
        name = VJXString.PRE_string_ + name;
        prototype.strings.put(name, value);
    }

    public void clear() {
        prototype.bools.clear();
        prototype.ints.clear();
        prototype.longs.clear();
        prototype.floats.clear();
        prototype.strings.clear();
        settings.clear();
    }

    private void setupPrototype() {
        String keySplit[];
        for (String key : settings.getKeys()) {
            keySplit = key.split(VJXString.SEP_USCORE);

            String type = keySplit[0];
            switch (type) {
                case VJXString.VAR_bool:
                    prototype.bools.put(key, settings.getBool(key, false));
                    break;
                case VJXString.VAR_int:
                    prototype.ints.put(key, settings.getInt(key, 0));
                    break;
                case VJXString.VAR_long:
                    prototype.longs.put(key, settings.getLong(key, 0l));
                    break;
                case VJXString.VAR_float:
                    prototype.floats.put(key, settings.getFloat(key, 0f));
                    break;
                case VJXString.VAR_string:
                    prototype.strings.put(key, settings.getString(key, VJXString.STR_EMPTY));
                    break;
            }
        }
    }

    public String getName() {
        return settings.getName();
    }

    public byte[] toBytes() {
        return toString().getBytes();
    }

    @Override
    public String toString() {
        Json json = new Json();
        String s = json.toJson(prototype);
        return s;
    }
}