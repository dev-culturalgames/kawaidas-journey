package de.venjinx.ejx.save;

public abstract class CloudSave extends LocalSavegame {

    private CloudSaveHandler cloudHandler;

    public CloudSave(GameSettings playerStats) {
        super(playerStats);
    }

    public void setCloudHandler(CloudSaveHandler cloudHandler) {
        this.cloudHandler = cloudHandler;
    }

    public boolean write(boolean upload) {
        if (!super.write()) return false;
        if (upload) cloudHandler.upload();
        return true;
    }

    @Override
    public boolean write() {
        return write(true);
    }
}