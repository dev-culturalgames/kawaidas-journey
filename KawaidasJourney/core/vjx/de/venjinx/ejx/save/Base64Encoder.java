package de.venjinx.ejx.save;

import com.badlogic.gdx.utils.Base64Coder;

import de.venjinx.ejx.util.EJXTypes.VJXString;

public class Base64Encoder extends Encoder {

    @Override
    public String encode(String val) {
        if (val == null || val.isEmpty()) return VJXString.STR_EMPTY;
        //            VJXLogger.log(LogCategory.ALL, val);
        return Base64Coder.encodeString(val);
    }

    @Override
    public String decode(String val) {
        if (val == null || val.isEmpty()) return VJXString.STR_EMPTY;
        //            VJXLogger.log(LogCategory.ALL, val);
        return Base64Coder.decodeString(val);
    }
}