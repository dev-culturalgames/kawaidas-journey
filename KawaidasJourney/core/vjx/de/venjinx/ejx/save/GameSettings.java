package de.venjinx.ejx.save;

import java.util.Map;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;

public class GameSettings {

    public static String subDir = VJXString.STR_EMPTY;

    private Preferences prefs;
    private String name;

    private Encoder encoder;
    private String encryptedKey;
    private String encryptedValue;

    public GameSettings(String name) {
        this(name, null);
    }

    public GameSettings(String name, Encoder encoder) {
        this.encoder = encoder;
        this.name = name;
        prefs = Gdx.app.getPreferences(subDir + name + VJXString.FILE_pref);
    }

    public void setEncoder(Encoder encoder) {
        this.encoder = encoder;
    }

    public void putString(String name, String val) {
        if (encoder != null) {
            encryptedKey = encoder.encode(name);
            encryptedValue = encoder.encode(val);
            prefs.putString(encryptedKey, encryptedValue);
        } else prefs.putString(name, val);

        prefs.flush();
    }

    public void putBool(String name, boolean val) {
        if (encoder != null) {
            encryptedKey = encoder.encode(name);
            encryptedValue = encoder.encode(val);
            prefs.putString(encryptedKey, encryptedValue);
        } else prefs.putBoolean(name, val);

        prefs.flush();
    }

    public void putInt(String name, int val) {
        if (encoder != null) {
            encryptedKey = encoder.encode(name);
            encryptedValue = encoder.encode(val);
            prefs.putString(encryptedKey, encryptedValue);
        } else prefs.putInteger(name, val);

        prefs.flush();
    }

    public void putLong(String name, long val) {
        if (encoder != null) {
            encryptedKey = encoder.encode(name);
            encryptedValue = encoder.encode(val);
            prefs.putString(encryptedKey, encryptedValue);
        } else prefs.putLong(name, val);

        prefs.flush();
    }

    public void putFloat(String name, float val) {
        if (encoder != null) {
            encryptedKey = encoder.encode(name);
            encryptedValue = encoder.encode(val);
            prefs.putString(encryptedKey, encryptedValue);
        } else prefs.putFloat(name, val);

        prefs.flush();
    }

    public String getString(String name, String def) {
        if (encoder != null) {
            encryptedKey = encoder.encode(name);
            encryptedValue = prefs.getString(encryptedKey, encoder.encode(def));
            return encoder.decode(encryptedValue);
        } else
            return prefs.getString(name, def);
    }

    public boolean getBool(String name, boolean def) {
        if (encoder != null) {
            encryptedKey = encoder.encode(name);
            encryptedValue = prefs.getString(encryptedKey, encoder.encode(def));
            return Boolean.parseBoolean(encoder.decode(encryptedValue));
        } else
            return prefs.getBoolean(name, def);
    }

    public int getInt(String name, int def) {
        if (encoder != null) {
            encryptedKey = encoder.encode(name);
            encryptedValue = prefs.getString(encryptedKey, encoder.encode(def));
            return Integer.parseInt(encoder.decode(encryptedValue));
        } else
            return prefs.getInteger(name, def);
    }

    public long getLong(String name, long def) {
        if (encoder != null) {
            encryptedKey = encoder.encode(name);
            encryptedValue = prefs.getString(encryptedKey, encoder.encode(def));
            return Long.parseLong(encoder.decode(encryptedValue));
        } else
            return prefs.getLong(name, def);
    }

    public float getFloat(String name, float def) {
        if (encoder != null) {
            encryptedKey = encoder.encode(name);
            encryptedValue = prefs.getString(encryptedKey, encoder.encode(def));
            return Float.parseFloat(encoder.decode(encryptedValue));
        } else
            return prefs.getFloat(name, def);
    }

    public void clear() {
        prefs.clear();
        prefs.flush();
    }

    public String getName() {
        return name;
    }

    public Map<String, ?> getAll() {
        return prefs.get();
    }

    public Set<String> getKeys() {
        return prefs.get().keySet();
    }

    public void print() {
        VJXLogger.log("Game settings '" + name + "'");
        Map<String, ?> map = prefs.get();
        for (String s : map.keySet()) {
            VJXLogger.log("    " + String.format("%1$-20s", s) + ": "
                            + map.get(s));
        }
    }
}