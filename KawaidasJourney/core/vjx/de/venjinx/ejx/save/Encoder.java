package de.venjinx.ejx.save;

public abstract class Encoder {

    public abstract String encode(String val);

    public abstract String decode(String val);

    public String encode(boolean boolValue) {
        return encode(Boolean.toString(boolValue));
    }

    public String encode(int intValue) {
        return encode(Integer.toString(intValue));
    }

    public String encode(float floatValue) {
        return encode(Float.toString(floatValue));
    }

    public String encode(Long longValue) {
        return encode(Long.toString(longValue));
    }
}