package de.venjinx.ejx.save;

import com.badlogic.gdx.utils.Json;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.util.EJXGameService;
import de.venjinx.ejx.util.EJXGameService.GameServiceListener;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public abstract class CloudSaveHandler {

    protected static final String CLOUD_CONFLICT_SNAPSHOT = "Cloud - Conflict while fetching data: ";
    protected static final String CLOUD_CREATE_NEW_SAVEGAME = "Cloud save creating new savegame: ";
    protected static final String CLOUD_NOT_AVAILABLE = "Cloud save not available.";
    protected static final String CLOUD_NOT_CONNECTED = "Cloud save not connected.";
    protected static final String CLOUD_CONNECTING = "Cloud service connecting: ";
    protected static final String CLOUD_CONNECTED = "Cloud service connected: ";
    protected static final String CLOUD_DISCONNECTING = "Cloud service disconnecting: ";
    protected static final String CLOUD_DISCONNECTED = "Cloud service disconnected: ";
    protected static final String CLOUD_LOADING_SUCCESSFUL = "Cloud - Loading successful: ";
    protected static final String CLOUD_LOCAL_HIGHER_PROGRESS = "Cloud - Local progress higher than or equal to cloud progress. ";
    protected static final String CLOUD_LOADING = "Cloud savegame loading: ";
    protected static final String CLOUD_LOADED = "Cloud savegame loaded: ";
    protected static final String CLOUD_SAVING = "Cloud savegame saving: ";
    protected static final String CLOUD_SAVED = "Cloud savegame saved: ";

    protected Json json = new Json();
    protected EJXGame game;
    protected CloudSave save;

    protected EJXGameService gameService;
    private GameServiceListener listener;

    public abstract boolean isAvailable();

    public abstract void showSavedGamesUI();

    public abstract void deleteSave(String saveName);

    protected abstract boolean save();

    protected abstract boolean load();

    public abstract void loadGamestate(byte[] gameState);

    protected abstract void solveConflictingSaveData(Runnable useLocal, Runnable useCloud);

    public void upload() {
        if (isAvailable()) {
            if (gameService.isConnected()) {
                VJXLogger.log(LogCategory.INFO, CLOUD_SAVING + save.getName());
                save();
            } else VJXLogger.log(LogCategory.INFO, CLOUD_NOT_CONNECTED);
        } else {
            listener.onSaved();
            VJXLogger.log(LogCategory.INFO, CLOUD_NOT_AVAILABLE);
        }
    }

    public void loadGame() {
        if (isAvailable()) {
            if (gameService.isConnected()) {
                VJXLogger.log(LogCategory.INFO, CLOUD_LOADING + save.getName());
                load();
            } else VJXLogger.log(LogCategory.INFO, CLOUD_NOT_CONNECTED);
        } else {
            listener.onMenuLoaded();
            VJXLogger.log(LogCategory.INFO, CLOUD_NOT_AVAILABLE);
        }
    }

    protected void savegameLoaded() {
        VJXLogger.log(LogCategory.INFO, CLOUD_LOADED + save.getName());
        listener.onMenuLoaded();
    }

    protected void savegameSaved() {
        VJXLogger.log(LogCategory.INFO, CLOUD_SAVED + save.getName());
        listener.onSaved();
    }

    public void setGame(EJXGame game) {
        this.game = game;
        gameService = game.getGameService();
    }

    public void setListener(GameServiceListener listener) {
        this.listener = listener;
    }

    public void setSave(CloudSave save) {
        this.save = save;
        save.setCloudHandler(this);
    }

    public CloudSave getSave() {
        return save;
    }

    public void showInfo(String msg) {
        if (game.getScreen().getMenu() != null)
            game.getScreen().getMenu().showMessageToUser("INFO", msg);
        else VJXLogger.log("INFO: " + msg);
    }

    public void showError(String msg) {
        if (game.getScreen().getMenu() != null)
            game.getScreen().getMenu().showMessageToUser("ERROR", msg);
        else VJXLogger.log("ERROR: " + msg);
    }
}