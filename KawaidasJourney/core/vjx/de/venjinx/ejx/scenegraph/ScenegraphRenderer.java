package de.venjinx.ejx.scenegraph;

import static com.badlogic.gdx.graphics.g2d.Batch.C1;
import static com.badlogic.gdx.graphics.g2d.Batch.C2;
import static com.badlogic.gdx.graphics.g2d.Batch.C3;
import static com.badlogic.gdx.graphics.g2d.Batch.C4;
import static com.badlogic.gdx.graphics.g2d.Batch.U1;
import static com.badlogic.gdx.graphics.g2d.Batch.U2;
import static com.badlogic.gdx.graphics.g2d.Batch.U3;
import static com.badlogic.gdx.graphics.g2d.Batch.U4;
import static com.badlogic.gdx.graphics.g2d.Batch.V1;
import static com.badlogic.gdx.graphics.g2d.Batch.V2;
import static com.badlogic.gdx.graphics.g2d.Batch.V3;
import static com.badlogic.gdx.graphics.g2d.Batch.V4;
import static com.badlogic.gdx.graphics.g2d.Batch.X1;
import static com.badlogic.gdx.graphics.g2d.Batch.X2;
import static com.badlogic.gdx.graphics.g2d.Batch.X3;
import static com.badlogic.gdx.graphics.g2d.Batch.X4;
import static com.badlogic.gdx.graphics.g2d.Batch.Y1;
import static com.badlogic.gdx.graphics.g2d.Batch.Y2;
import static com.badlogic.gdx.graphics.g2d.Batch.Y3;
import static com.badlogic.gdx.graphics.g2d.Batch.Y4;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMapImageLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.entity.EJXGroup;
import de.venjinx.ejx.entity.logics.LightLogic;
import de.venjinx.ejx.stages.DebugStage;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;

public class ScenegraphRenderer extends OrthogonalTiledMapRenderer {

    private EJXScenegraph scene;

    private Color baseColor = new Color();
    private Color layerColor = new Color();
    private Color tmpColor = new Color();
    private ShapeRenderer shapeRenderer;

    private boolean lightsEnabled;
    public RayHandler lightRenderer;

    private Color ambLightColor;

    private Vector2 focus;
    private PointLight focusLight;

    private EJXCamera cam;

    private float paraValue = .1f;
    private float tmpPara;

    private Matrix4 paraTransMat = new Matrix4();
    private Vector3 viewPosition = new Vector3();

    private boolean debug = false;

    private static SpriteBatch fboBatch = new SpriteBatch();
    //    private FrameBuffer fbo;
    //    private TextureRegion fboRegion;
    private boolean useFBO = false;

    public ScenegraphRenderer(EJXGame game) {
        super(null, game.batch);
        //        objectBatch = game.batch;
        shapeRenderer = game.shapeRenderer;
        focus = new Vector2();
        lightRenderer = new RayHandler(null);
        fboRegion = new TextureRegion(new Texture(1280, 720, Format.RGBA8888),
                        0, 0, 1280, 720);
        fboRegion.flip(false, true);
    }

    public void setLevel(LevelStage level) {
        cam = (EJXCamera) level.getCamera();
        viewBounds.setSize(cam.viewportWidth * cam.zoom,
                        cam.viewportHeight * cam.zoom);
        scene = level.getScene();
    }

    public void setUseFBO(boolean useFBO) {
        this.useFBO = useFBO;
    }

    public boolean useFBO() {
        return useFBO;
    }

    public void setBaseColor(Color c) {
        baseColor = c;
    }

    public void setZoom(float zoom) {
        viewBounds.setSize(cam.viewportWidth * zoom, cam.viewportHeight * zoom);
    }

    public void setParallax(float value) {
        paraValue = value;
    }

    public void initLight(World b2dWorld, boolean lightsEnabled) {
        this.lightsEnabled = lightsEnabled;

        lightRenderer.setWorld(null);
        if (lightsEnabled) {
            lightRenderer.setWorld(b2dWorld);

            ambLightColor = map.getProperties().get(LightLogic.LIGHT_amb_color,
                            new Color(0, 0, 0, 1), Color.class);
            lightRenderer.setAmbientLight(ambLightColor);
            lightRenderer.setBlur(map.getProperties().get(LightLogic.LIGHT_blur, false,
                            Boolean.class));
            lightRenderer.setShadows(map.getProperties().get(LightLogic.LIGHT_shadows,
                            true, Boolean.class));

            if (focusLight == null)
                focusLight = new PointLight(lightRenderer, B2DWorld.LIGHT_RAY_COUNT,
                                map.getProperties().get(LightLogic.LIGHT_focus_color,
                                                new Color(0, 0, 0, .75f), Color.class),
                                map.getProperties().get(LightLogic.LIGHT_focus_radius,
                                                12.8f, Float.class),
                                0f, 0f);

            focusLight.setXray(map.getProperties().get(LightLogic.LIGHT_focus_xRay,
                            true, Boolean.class));
            focusLight.setSoft(map.getProperties().get(LightLogic.LIGHT_focus_soft,
                            false, Boolean.class));
            focusLight.setSoftnessLength(map.getProperties()
                            .get(LightLogic.LIGHT_focus_softness, 0f, Float.class));
        }
    }

    public void resize(Viewport view, int width, int height) {
        //        VJXLogger.log(width + ", " + height);
        //        createFBO();
        resizeLightViewport(view, width, height);
    }

    private void resizeLightViewport(Viewport view, int width, int height) {
        int gutterW = view.getLeftGutterWidth();
        int gutterH = view.getTopGutterHeight();
        int rhWidth = width - 2 * gutterW;
        int rhHeight = height - 2 * gutterH;

        if (lightRenderer != null)
            lightRenderer.useCustomViewport(gutterW, gutterH, rhWidth, rhHeight);
    }

    public void setLightFocus(Vector2 focus) {
        this.focus.set(focus);
    }

    public void setAmbLight(float alpha) {
        if (!lightsEnabled) return;
        ambLightColor.a = alpha;
        lightRenderer.setAmbientLight(alpha);
    }

    public float getAmbLight() {
        if (!lightsEnabled) return 1f;
        return ambLightColor.a;
    }

    public void setCamFocusRadius(float radius) {
        if (!lightsEnabled) return;
        focusLight.setDistance(radius);
    }

    public float getCamFocusRadius() {
        if (!lightsEnabled) return 0;
        return focusLight.getDistance();
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public void updateSceneParallax() {
        viewPosition.set(cam.position.x - viewBounds.width / 2f,
                        cam.position.y - viewBounds.height / 2f, 0);

        AnimatedTiledMapTile.updateAnimationBaseTime();

        Array<WorldLayer> layers = scene.getGameLayers();
        for (WorldLayer layer : layers) {
            updateLayerParallax(layer);
        }
    }

    private void updateLayerParallax(WorldLayer gameLayer) {
        EJXGroup layerNode;
        MapLayers layers;
        float newX;

        layers = gameLayer.getLayers();
        for (MapLayer l : layers) {
            layerNode = gameLayer.getNode(l);
            tmpPara = 1 + paraValue * layerNode.getDepth();
            newX = viewPosition.x * tmpPara - viewPosition.x;
            newX = (int) newX;
            newX += (int) (newX % 2);
            layerNode.setPosition(-newX, 0);
        }
    }

    public void renderScenegraph(OrthographicCamera b2dCam) {
        FrameBuffer.clearAllFrameBuffers(Gdx.app);

        shapeRenderer.setProjectionMatrix(cam.combined);

        tmpColor.set(batch.getColor());
        baseColor.a = scene.getColor().a;
        batch.begin();
        Array<WorldLayer> layers = scene.getGameLayers();
        //        HashMap<MapLayer, FrameBuffer> buffers = scene.getFBOs();
        for (WorldLayer layer : layers) {
            //            renderGameLayer(layer, buffers);
            renderGameLayer(layer);
        }

        //        for (WorldLayer layer : layers) {
        //            renderObjectLayers(layer, buffers);
        //        }
        //
        //        for (WorldLayer layer : layers) {
        //            drawFBOs(layer, buffers);
        //        }

        if (debug) {
            batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            batch.setProjectionMatrix(cam.combined);
            cam.drawBatchDebug(batch, DebugStage.dbgFont);
            batch.end();
            batch.setColor(tmpColor);

            shapeRenderer.begin();
            cam.drawShapeDebug(shapeRenderer);
            shapeRenderer.end();
        } else {
            batch.end();
            batch.setColor(tmpColor);
        }

        if (lightsEnabled) {
            focusLight.setPosition(focus);
            lightRenderer.setCombinedMatrix(b2dCam);
            lightRenderer.update();
            lightRenderer.render();
        }
    }

    private void renderGameLayer(WorldLayer gameLayer) {
        EJXGroup layerNode;
        MapLayers layers;
        float newX = 0;
        layers = gameLayer.getLayers();
        Color objBatchColor = batch.getColor();
        layerColor.set(baseColor);

        batch.setColor(layerColor);
        fboBatch.setColor(layerColor);

        for (MapLayer l : layers) {
            if (l.isVisible()) {
                layerNode = gameLayer.getNode(l);

                tmpPara = 1 + paraValue * layerNode.getDepth();

                newX = viewPosition.x * tmpPara;
                viewBounds.setPosition(newX, viewPosition.y);

                newX = newX - viewPosition.x;
                paraTransMat.set(cam.combined);
                paraTransMat.translate(-newX, 0, 0);

                if (l instanceof TiledMapTileLayer) {
                    batch.setBlendFunction(GL20.GL_SRC_ALPHA,
                                    GL20.GL_ONE_MINUS_SRC_ALPHA);
                    batch.setProjectionMatrix(paraTransMat);
                    renderTileLayer((TiledMapTileLayer) l);
                    batch.setBlendFunction(GL20.GL_ONE,
                                    GL20.GL_ONE_MINUS_SRC_ALPHA);
                    batch.setProjectionMatrix(cam.combined);
                } else if (l instanceof TiledMapImageLayer) {
                    batch.setBlendFunction(GL20.GL_SRC_ALPHA,
                                    GL20.GL_ONE_MINUS_SRC_ALPHA);
                    batch.setProjectionMatrix(paraTransMat);
                    renderImageLayer((TiledMapImageLayer) l);
                    batch.setBlendFunction(GL20.GL_ONE,
                                    GL20.GL_ONE_MINUS_SRC_ALPHA);
                    batch.setProjectionMatrix(cam.combined);
                }

                if (useFBO) drawFBO(layerNode);
                else drawNormal(layerNode);
            }
        }

        if (debug) {
            batch.end();
            shapeRenderer.begin();
            for (MapLayer l : layers) {
                if (l.isVisible()) {
                    layerNode = gameLayer.getNode(l);
                    layerNode.drawDebug(shapeRenderer);
                }
            }
            shapeRenderer.end();
            batch.begin();
        }
        batch.setColor(objBatchColor);
    }

    //    private void drawFBOs(WorldLayer gameLayer,
    //                    HashMap<MapLayer, FrameBuffer> buffers) {
    //        MapLayers layers = gameLayer.getLayers();
    //        FrameBuffer buffer;
    //        for (MapLayer l : layers) {
    //            if (l.isVisible()) {
    //                buffer = buffers.get(l);
    //                fboRegion.setTexture(buffer.getColorBufferTexture());
    //                batch.draw(fboRegion, viewPosition.x, viewPosition.y);
    //            }
    //        }
    //    }
    //
    //    private void renderGameLayer(WorldLayer gameLayer,
    //                    HashMap<MapLayer, FrameBuffer> buffers) {
    //        VJXGroup layerNode;
    //        MapLayers layers;
    //        float newX=0;
    //        layers = gameLayer.getLayers();
    //        Color objBatchColor = batch.getColor();
    //        layerColor.set(baseColor);
    //
    //        batch.setColor(layerColor);
    //        fboBatch.setColor(layerColor);
    //
    //        FrameBuffer buffer;
    //        for (MapLayer l : layers) {
    //            if (l.isVisible()) {
    //                layerNode = gameLayer.getNode(l);
    //
    //                tmpPara = 1 + paraValue * layerNode.getDepth();
    //
    //                newX = viewPosition.x * tmpPara;
    //                viewBounds.setPosition(newX, viewPosition.y);
    //
    //                newX = newX - viewPosition.x;
    //                paraTransMat.set(cam.combined);
    //                paraTransMat.translate(-newX, 0, 0);
    //
    //                if (l instanceof TiledMapTileLayer) {
    //                    buffer = buffers.get(l);
    //                    buffer.bind();
    //                    buffer.begin();
    //                    batch.setBlendFunction(GL20.GL_SRC_ALPHA,
    //                                    GL20.GL_ONE_MINUS_SRC_ALPHA);
    //                    batch.setProjectionMatrix(paraTransMat);
    //                    renderTileLayer((TiledMapTileLayer) l);
    //                    batch.setBlendFunction(GL20.GL_ONE,
    //                                    GL20.GL_ONE_MINUS_SRC_ALPHA);
    //                    batch.setProjectionMatrix(cam.combined);
    //                    buffer.end();
    //                } else if (l instanceof TiledMapImageLayer) {
    //                    buffer = buffers.get(l);
    //                    buffer.bind();
    //                    buffer.begin();
    //                    batch.setBlendFunction(GL20.GL_SRC_ALPHA,
    //                                    GL20.GL_ONE_MINUS_SRC_ALPHA);
    //                    batch.setProjectionMatrix(paraTransMat);
    //                    renderImageLayer((TiledMapImageLayer) l);
    //                    batch.setBlendFunction(GL20.GL_ONE,
    //                                    GL20.GL_ONE_MINUS_SRC_ALPHA);
    //                    batch.setProjectionMatrix(cam.combined);
    //                    buffer.end();
    //                }
    //            }
    //        }
    //
    //        if (debug) {
    //            batch.end();
    //            shapeRenderer.begin();
    //            for (MapLayer l : layers) {
    //                if (l.isVisible()) {
    //                    layerNode = gameLayer.getNode(l);
    //                    layerNode.drawDebug(shapeRenderer);
    //                }
    //            }
    //            shapeRenderer.end();
    //            batch.begin();
    //        }
    //        batch.setColor(objBatchColor);
    //    }
    //
    //    private void renderObjectLayers(WorldLayer gameLayer,
    //                    HashMap<MapLayer, FrameBuffer> buffers) {
    //        VJXGroup layerNode;
    //        MapLayers layers = gameLayer.getLayers();
    //        FrameBuffer buffer;
    //        batch.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);
    //        batch.setProjectionMatrix(cam.combined);
    //        for (MapLayer l : layers) {
    //            if (l.isVisible()) {
    //                layerNode = gameLayer.getNode(l);
    //                buffer = buffers.get(l);
    //                buffer.bind();
    //                buffer.begin();
    //                drawNormal(layerNode);
    //                buffer.end();
    //            }
    //        }
    //    }

    private TextureRegion fboRegion;

    public void drawNormal(EJXGroup layerNode) {
        if (layerNode.getChildren().size == 0) return;
        layerNode.draw(batch, 1);
    }

    public void drawFBO(EJXGroup layerNode) {
        //        fbo.begin();
        //        Gdx.gl20.glClearColor(0, 0, 0, 0);
        //        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // setup our alpha blending to avoid blending twice
        //        Gdx.gl20.glBlendFuncSeparate(GL20.GL_SRC_ALPHA,
        //                                     GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_ONE,
        //                                     GL20.GL_ONE);

        //        objectBatch.setColor(1, 1, 1, 1);
        //        batch.begin();

        // use -1 to ignore.. somebody should fix this in LibGDX :\
        //        objectBatch.setBlendFunction(-1, -1);
        //        objectBatch.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);

        //        sBatch.begin();
        layerNode.draw(batch, 1);
        //        sBatch.end();

        // now let's reset blending to the default...
        //        objectBatch.setBlendFunction(GL20.GL_SRC_ALPHA,
        //                                     GL20.GL_ONE_MINUS_SRC_ALPHA);
        //        fbo.end();

        fboBatch.setColor(1, 1, 1, 1);
        fboBatch.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);

        //        drawToBatch(sBatch);
        fboBatch.begin();
        layerNode.drawToBatch(fboBatch);
        fboBatch.end();

        //        objectBatch.begin();
        //        layerNode.drawToBatch(objectBatch);
        //        objectBatch.end();
    }

    //    private void drawToBatch(Batch batch) {
    //        // render the offscreen texture with "premultiplied alpha" blending
    //        batch.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);
    //        batch.begin();
    //
    //        batch.draw(fboRegion, 0, 0);
    //        batch.end();
    //    }

    @Override
    public void renderTileLayer(TiledMapTileLayer layer) {
        final Color batchColor = batch.getColor();
        final float color = Color.toFloatBits(batchColor.r, batchColor.g,
                        batchColor.b, batchColor.a * layer.getOpacity());

        final int layerWidth = layer.getWidth();
        final int layerHeight = layer.getHeight();

        final float layerTileWidth = layer.getTileWidth() * unitScale;
        final float layerTileHeight = layer.getTileHeight() * unitScale;

        final int col1 = Math.max(0, (int) (viewBounds.x / layerTileWidth));
        final int col2 = Math.min(layerWidth, (int) ((viewBounds.x
                        + viewBounds.width + layerTileWidth) / layerTileWidth));

        final int row1 = Math.max(0, (int) (viewBounds.y / layerTileHeight));
        final int row2 = Math.min(layerHeight, (int) ((viewBounds.y + viewBounds.height
                        + layerTileHeight)
                        / layerTileHeight));

        float y = row2 * layerTileHeight;
        float xStart = col1 * layerTileWidth;
        final float[] vertices = this.vertices;

        for (int row = row2; row >= row1; row--) {
            float x = xStart;
            for (int col = col1; col < col2; col++) {
                final TiledMapTileLayer.Cell cell = layer.getCell(col, row);
                if (cell == null) {
                    x += layerTileWidth;
                    continue;
                }
                final TiledMapTile tile = cell.getTile();

                if (tile != null) {
                    final boolean flipX = cell.getFlipHorizontally();
                    final boolean flipY = cell.getFlipVertically();
                    final int rotations = cell.getRotation();

                    TextureRegion region = tile.getTextureRegion();

                    float x1 = x + tile.getOffsetX() * unitScale * 2;
                    float y1 = y + tile.getOffsetY() * unitScale * 2;
                    float x2 = x1 + region.getRegionWidth() * unitScale * 2;
                    float y2 = y1 + region.getRegionHeight() * unitScale * 2;

                    float u1 = region.getU();
                    float v1 = region.getV2();
                    float u2 = region.getU2();
                    float v2 = region.getV();

                    vertices[X1] = x1;
                    vertices[Y1] = y1;
                    vertices[C1] = color;
                    vertices[U1] = u1;
                    vertices[V1] = v1;

                    vertices[X2] = x1;
                    vertices[Y2] = y2;
                    vertices[C2] = color;
                    vertices[U2] = u1;
                    vertices[V2] = v2;

                    vertices[X3] = x2;
                    vertices[Y3] = y2;
                    vertices[C3] = color;
                    vertices[U3] = u2;
                    vertices[V3] = v2;

                    vertices[X4] = x2;
                    vertices[Y4] = y1;
                    vertices[C4] = color;
                    vertices[U4] = u2;
                    vertices[V4] = v1;
                    //                    VJXLogger.log("" + vertices[X1] + ", " + vertices[Y1] + ", "
                    //                                    + vertices[X2] + ", " + vertices[Y2] + ", "
                    //                                    + vertices[X3] + ", " + vertices[Y3] + ", "
                    //                                                    + vertices[X4] + ", " + vertices[Y4]);
                    if (flipX) {
                        float temp = vertices[U1];
                        vertices[U1] = vertices[U3];
                        vertices[U3] = temp;
                        temp = vertices[U2];
                        vertices[U2] = vertices[U4];
                        vertices[U4] = temp;
                    }
                    if (flipY) {
                        float temp = vertices[V1];
                        vertices[V1] = vertices[V3];
                        vertices[V3] = temp;
                        temp = vertices[V2];
                        vertices[V2] = vertices[V4];
                        vertices[V4] = temp;
                    }
                    if (rotations != 0) {
                        switch (rotations) {
                            case Cell.ROTATE_90: {
                                float tempV = vertices[V1];
                                vertices[V1] = vertices[V2];
                                vertices[V2] = vertices[V3];
                                vertices[V3] = vertices[V4];
                                vertices[V4] = tempV;

                                float tempU = vertices[U1];
                                vertices[U1] = vertices[U2];
                                vertices[U2] = vertices[U3];
                                vertices[U3] = vertices[U4];
                                vertices[U4] = tempU;
                                break;
                            }
                            case Cell.ROTATE_180: {
                                float tempU = vertices[U1];
                                vertices[U1] = vertices[U3];
                                vertices[U3] = tempU;
                                tempU = vertices[U2];
                                vertices[U2] = vertices[U4];
                                vertices[U4] = tempU;
                                float tempV = vertices[V1];
                                vertices[V1] = vertices[V3];
                                vertices[V3] = tempV;
                                tempV = vertices[V2];
                                vertices[V2] = vertices[V4];
                                vertices[V4] = tempV;
                                break;
                            }
                            case Cell.ROTATE_270: {
                                float tempV = vertices[V1];
                                vertices[V1] = vertices[V4];
                                vertices[V4] = vertices[V3];
                                vertices[V3] = vertices[V2];
                                vertices[V2] = tempV;

                                float tempU = vertices[U1];
                                vertices[U1] = vertices[U4];
                                vertices[U4] = vertices[U3];
                                vertices[U3] = vertices[U2];
                                vertices[U2] = tempU;
                                break;
                            }
                        }
                    }

                    batch.draw(region.getTexture(), vertices, 0, NUM_VERTICES);
                }
                x += layerTileWidth;
            }
            y -= layerTileHeight;
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        if (lightsEnabled)
            lightRenderer.dispose();

        //        if (fbo != null) fbo.dispose();
    }
}