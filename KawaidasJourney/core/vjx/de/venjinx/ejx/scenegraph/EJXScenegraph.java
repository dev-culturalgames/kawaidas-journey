package de.venjinx.ejx.scenegraph;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.entity.EJXGroup;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class EJXScenegraph extends EJXGroup {

    //    private HashMap<MapLayer, FrameBuffer> layerFBOs = new HashMap<>();

    private EJXGroup world_root;
    private Array<WorldLayer> worldLayers;
    private WorldLayer skybox;

    private int depth = 1;
    private int focus = 0;
    private boolean isSetup = false;

    public EJXScenegraph() {
        super("scene_root");
        setVisible(false);

        worldLayers = new Array<>();
    }

    public WorldLayer getWorldLayer(String name) {
        for (WorldLayer layer : worldLayers) {
            if (layer.getName().equals(name)) return layer;
        }
        VJXLogger.log(LogCategory.ERROR, "layer '" + name + "' not found!");
        return null;
    }

    public Array<WorldLayer> getGameLayers() {
        return worldLayers;
    }

    public EJXGroup findGroup(String name) {
        Actor a = findActor(name);
        return a instanceof EJXGroup ? (EJXGroup) a : null;
    }

    public void setupScenegraph() {
        clear(true);
        world_root = new EJXGroup("world_root");

        skybox = new WorldLayer("skybox", Integer.MIN_VALUE);
        world_root.addActor(skybox);
        worldLayers.add(skybox);

        addActor(world_root);
    }

    public void setupLevelNodes(TiledMap map, boolean useDepth) {
        if (isSetup) return;

        //        for (FrameBuffer fb : layerFBOs.values()) {
        //            fb.dispose();
        //        }
        //        layerFBOs.clear();

        MapLayers layers = map.getLayers();
        focus = layers.getCount() - 1;
        if (focus == -1) return;

        if (useDepth) {
            depth = 1;
            createDepth(layers);
            map.getProperties().put("depth:focus", focus);
            map.getProperties().put("depth:world", depth);
        } else {
            MapLayer layer;
            for (int i = 0; i < layers.getCount(); i++) {
                layer = layers.get(i);
                layer.getProperties().put("world_layer", skybox.getName());
                skybox.addLayer(layer);
            }
        }
        isSetup = true;
    }

    private void createDepth(MapLayers layers) {
        int currentDepth = 1;
        int tmpDepth;
        MapLayer layer;
        findDepthLevel(layers);

        WorldLayer worldLayer = skybox;
        for (int i = 0; i < layers.getCount(); i++) {
            layer = layers.get(i);

            String name = layer.getName();
            if (name.contains("depth")) {
                tmpDepth = layer.getProperties().get("depth",
                                currentDepth - focus, Integer.class);

                worldLayer = new WorldLayer(name + "(" + tmpDepth + ")",
                                tmpDepth);
                world_root.addActor(worldLayer);
                worldLayers.add(worldLayer);

                worldLayer.clearLayer();
                currentDepth++;
                continue;
            }

            //            FrameBuffer fbo = new FrameBuffer(Format.RGBA8888, 1280, 720, false);
            //            layerFBOs.put(layer, fbo);

            layer.getProperties().put("world_layer", worldLayer.getName());
            worldLayer.addLayer(layer);
        }
    }

    private void findDepthLevel(MapLayers layers) {
        MapLayer layer;
        for (int i = 0; i < layers.getCount(); i++) {
            layer = layers.get(i);

            String name = layer.getName();
            if (name.contains("depth")) {
                if (name.contains("focus")) {
                    focus = depth;
                }
                depth++;
            }
        }
    }

    //    public HashMap<MapLayer, FrameBuffer> getFBOs() {
    //        return layerFBOs;
    //    }

    public void clear(boolean destroy) {
        clearActions();
        clearListeners();

        if (destroy) {
            worldLayers.clear();
            clear();
            isSetup = false;
        } else for (WorldLayer worldLayer : worldLayers) {
            MapLayers layers = worldLayer.getLayers();
            EJXGroup node;
            for (MapLayer layer : layers) {
                node = findGroup(layer.getName());
                node.setPosition(0, 0);
                node.clear();
            }
        }
    }
}