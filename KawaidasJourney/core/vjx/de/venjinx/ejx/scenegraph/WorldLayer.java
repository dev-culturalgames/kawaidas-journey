package de.venjinx.ejx.scenegraph;

import java.util.HashMap;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.SnapshotArray;

import de.venjinx.ejx.entity.EJXGroup;

public class WorldLayer extends EJXGroup {

    public static class LayerCell extends Cell {
        private final int origX;
        private final int origY;
        private final int caseID;

        private int localX = 0;
        private int localY = 0;

        private LayerCell[] neighbors = new LayerCell[4];

        private boolean visible = true;

        public LayerCell(int originX, int originY, int caseID) {
            origX = originX;
            origY = originY;
            setLocalXY(originX, originY);
            this.caseID = caseID;
        }

        public void setNeighbor(int id, LayerCell neighborCell) {
            neighbors[id] = neighborCell;
        }

        public LayerCell getNeighbor(int id) {
            return neighbors[id];
        }

        public void setLocalX(int x) {
            localX = x;
        }

        public int getLocalX() {
            return localX;
        }

        public void setLocalY(int y) {
            localY = y;
        }

        public int getLocalY() {
            return localY;
        }

        public void setLocalXY(int x, int y) {
            localX = x;
            localY = y;
        }

        public int getOriginX() {
            return origX;
        }

        public int getOriginY() {
            return origY;
        }

        public int getCase() {
            return caseID;
        }

        public Vector2 getLocalPosition(Vector2 pos) {
            pos.x = localX;
            pos.y = localY;
            return pos;
        }

        public void setVisible(boolean visible) {
            this.visible = visible;
        }

        public boolean isVisible() {
            return visible;
        }

        @Override
        public String toString() {
            return "Cell: origin(" + origX + ", " + origY + "), local(" + localX
                            + ", " + localY + ") - draw: " + visible;
        }
    }

    private MapLayers layers;
    private HashMap<MapLayer, EJXGroup> layerNodes;

    private int depthLevel = 0;

    public WorldLayer(String name, int depth) {
        super(name);
        depthLevel = depth;
        layers = new MapLayers();
        layerNodes = new HashMap<>();
    }

    public void addLayer(MapLayer layer) {
        EJXGroup node = new EJXGroup(layer.getName());
        node.setDepth(layer.getProperties().get("depth", depthLevel,
                        Integer.class));
        addActor(node);
        layers.add(layer);
        layerNodes.put(layer, node);
    }

    public MapLayers getLayers() {
        return layers;
    }

    public EJXGroup getNode(MapLayer layer) {
        return layerNodes.get(layer);
    }

    public int getDepthLevel() {
        return depthLevel;
    }

    public void clearLayer() {
        while (layers.getCount() != 0) {
            layers.remove(0);
        }

        SnapshotArray<Actor> children = getChildren();
        for (Actor a : children) {
            a.clear();
        }
    }
}