package de.venjinx.ejx.scenegraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class Waypoints {

    private static HashMap<String, Waypoint> points = new HashMap<>();

    public static void addPoint(Waypoint waypoint) {
        points.put(waypoint.name, waypoint);
    }

    public static Waypoint[] getWaypoints(String names) {
        if (names == null || names.isEmpty()) return null;
        String[] split = names.split(VJXString.SEP_LIST);
        Waypoint[] wps = new Waypoint[split.length];
        for (int i = 0; i < split.length; i++)
            wps[i] = getWaypoint(split[i]);
        return wps;
    }

    public static Waypoint getWaypoint(String name) {
        return points.get(name);
    }

    public static void loadWaypoints(EJXEntity entity, String wpsList,
                    Array<Waypoint> waypoints) {
        if (!wpsList.isEmpty()) {
            String[] wps = wpsList.split(",");
            Waypoint wp;
            for (String wpName : wps) {
                switch (wpName) {
                    case "this":
                        wp = new Waypoint(entity.getName() + "_center");
                        wp.setCenter(entity.getWorldCenter());
                        break;
                    case "origin":
                        wp = new Waypoint(entity.getName() + "_origin");

                        wp.setCenter(entity.getX() + entity.getOriginX(),
                                        entity.getY() + entity.getOriginY());
                        break;
                    case "left":
                        wp = new Waypoint(entity.getName() + "_left");
                        wp.setCenter(entity.getX(), entity.getY());
                        break;
                    case "right":
                        wp = new Waypoint(entity.getName() + "_right");
                        wp.setCenter(entity.getX() + entity.getScaledWidth(),
                                        entity.getY());
                        break;
                    case "bottom":
                        wp = new Waypoint(entity.getName() + "_bottom");
                        wp.setCenter(entity.getWCX(), entity.getY());
                        break;
                    case "top":
                        wp = new Waypoint(entity.getName() + "_top");
                        wp.setCenter(entity.getWCX(), entity.getY()
                                        + entity.getScaledHeight());
                        break;
                    default:
                        wp = getWaypoint(wpName);
                }
                waypoints.add(wp);
            }
        }
    }

    public static void loadWaypoints(String listName, EJXEntity entity,
                    Array<Waypoint> waypoints) {

        String wpString = entity.getDef().getCustomProperties().get(listName,
                        VJXString.STR_EMPTY, String.class);
        if (!wpString.isEmpty()) {
            String[] wps = wpString.split(",");
            Waypoint wp;
            for (String wpName : wps) {
                switch (wpName) {
                    case "this":
                        wp = new Waypoint(entity.getName() + "_center");
                        wp.setCenter(entity.getWorldCenter());
                        break;
                    case "origin":
                        wp = new Waypoint(entity.getName() + "_origin");

                        wp.setCenter(entity.getX() + entity.getOriginX(),
                                        entity.getY() + entity.getOriginY());
                        break;
                    case "left":
                        wp = new Waypoint(entity.getName() + "_left");
                        wp.setCenter(entity.getX(), entity.getY());
                        break;
                    case "right":
                        wp = new Waypoint(entity.getName() + "_right");
                        wp.setCenter(entity.getX() + entity.getScaledWidth(),
                                        entity.getY());
                        break;
                    case "bottom":
                        wp = new Waypoint(entity.getName() + "_bottom");
                        wp.setCenter(entity.getWCX(), entity.getY());
                        break;
                    case "top":
                        wp = new Waypoint(entity.getName() + "_top");
                        wp.setCenter(entity.getWCX(), entity.getY()
                                        + entity.getScaledHeight());
                        break;
                    default:
                        wp = getWaypoint(wpName);
                }
                waypoints.add(wp);
            }
        }
    }

    public static void removeWaypoint(Waypoint waypoint) {
        points.remove(waypoint.getName());
    }

    public static void removeWaypoint(String waypointName) {
        points.remove(waypointName);
    }

    public static void clearWaypoints() {
        points.clear();
    }

    public static class Waypoint {
        private String name = "waypoint";
        private Rectangle centerRect = new Rectangle(0, 0, 15f, 15f);

        private Command[] onSetCmds;
        private Command[] onReachCmds;

        private boolean executeOnce = false;
        private boolean isExecuted = false;
        private boolean updateActivity = true;
        private List<EJXEntity> entities;

        public Waypoint(String name) {
            this.name = name;
            entities = new ArrayList<>();
            onSetCmds = new Command[0];
            onReachCmds = new Command[0];
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setCenter(Vector2 pos) {
            setCenter(pos.x, pos.y);
        }

        public void setCenter(float x, float y) {
            centerRect.setCenter(x, y);
        }

        public Vector2 getCenter(Vector2 position) {
            return centerRect.getCenter(position);
        }

        public float getX() {
            return centerRect.x;
        }

        public float getY() {
            return centerRect.y;
        }

        public float getCX() {
            return centerRect.x + centerRect.getWidth() / 2;
        }

        public float getCY() {
            return centerRect.y + centerRect.getHeight() / 2;
        }

        public Rectangle getCenterRect() {
            return centerRect;
        }

        public boolean reachedBy(EJXEntity entity, boolean ignoreY) {
            if (!ignoreY)
                return centerRect.contains(entity.getWCX(), entity.getWCY());
            return centerRect.contains(entity.getWCX(), centerRect.y);
        }

        public void setExecuted() {
            isExecuted = true;
        }

        public boolean isExecuted() {
            return isExecuted;
        }

        public boolean executeOnce() {
            return executeOnce;
        }

        public void setExecuteOnce(boolean execOnce) {
            executeOnce = execOnce;
        }

        public void setUpdateActivity(boolean update) {
            updateActivity = update;
        }

        public boolean updateActivity() {
            return updateActivity;
        }

        public void setOnSetCmds(Command[] commands) {
            if (commands == null) return;
            onSetCmds = commands;
        }

        public void setOnReachCmds(Command[] commands) {
            if (commands == null) return;
            onReachCmds = commands;
        }

        public Command[] getOnSetCommands(EJXEntity target) {
            if (onSetCmds != null) {
                for (Command command : onSetCmds) {
                    command.updateTarget(target);
                }
            }
            return onSetCmds;
        }

        public boolean hasOnSetCMDs() {
            return onSetCmds.length > 0;
        }

        public Command[] getOnReachCmds(EJXEntity target) {
            if (onReachCmds != null) {
                for (Command command : onReachCmds) {
                    if (command != null) command.updateTarget(target);
                }
            }
            return onReachCmds;
        }

        public boolean hasOnReachCMDs() {
            return onReachCmds.length > 0;
        }

        public void reset() {
            isExecuted = false;
        }

        @Override
        public String toString() {
            String s = name;
            s += ":" + centerRect + ", executed " + isExecuted;
            return s;
        }

        public void add(EJXEntity e) {
            entities.add(e);
            e.setSpawnPoint(this);
        }

        public void remove(EJXEntity e) {
            entities.remove(e);
        }

        public boolean hasEntity() {
            return entities.size() > 0;
        }
    }
}