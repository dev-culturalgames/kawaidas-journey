package de.venjinx.ejx.tiled;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.venjinx.ejx.entity.component.ActorComponent;
import de.venjinx.ejx.entity.component.CommandComponent;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.tiled.entity.EJXObject;
import de.venjinx.ejx.tiled.object.EJXMapObject;
import de.venjinx.vjx.ui.MenuTable;

public class TiledMenu extends MenuTable {

    private EJXTiledMap map;

    public TiledMenu(String name, final MenuStage mc) {
        super(mc, name);
    }

    public void load() {
        TiledFactory factory = menuControl.getGame().tFactory;
        map = factory.getMap(getName());

        MapLayer layer = map.getLayers().get("content");
        MapObjects objects = layer.getObjects();

        EJXMapObject object;
        for (MapObject mapObject : objects) {
            object = (EJXMapObject) mapObject;
            if (object.getType() != null) {
                EJXObject entity = factory.createObject(object);
                ActorComponent objCmp = (ActorComponent) entity
                                .getComponent(VJXLogicType.ACTOR);
                addActor(objCmp.getActor());
                CommandComponent cmdCmp = (CommandComponent) entity
                                .getComponent(VJXLogicType.COMMAND);
                addActor(cmdCmp.getActor());
            }
        }
    }

    @Override
    public void updateLayout(Skin skin) {
    }

    public EJXTiledMap getMap() {
        return map;
    }
}