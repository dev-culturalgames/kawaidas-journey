package de.venjinx.ejx.tiled.entity;

import java.util.HashMap;
import java.util.HashSet;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.controllers.AudioControl;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.entity.EJXActor;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EJXGroup;
import de.venjinx.ejx.entity.EntityFactory;
import de.venjinx.ejx.entity.component.EJXComponent;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.EJXLogicType;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.stages.DebugStage.DebugStats;
import de.venjinx.ejx.stages.EJXStage;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.tiled.object.EJXMapObject;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

/**
 *
 * @author Torge Rothe (X-Ray-Jin, xrayjin@gmx.de)
 * </br>
 * This class represents the base class for all objects used in a game. This
 * includes characters as well as obstacles, decoration or items. An entity will
 * always be created via the entity {@link EntityFactory} and an {@link EntityDefinition}.
 * Entities are used to define different kinds of game objects based on general
 * mechanics. After being added to a {@link EJXStage} their life cycle will be
 * managed by the game. After instantiating an {@link EJXEntity} the {@link Lifecycle}
 * type controls the current status. The status of an {@link EJXEntity} determines
 * its current behavior.
 * Each {@link EJXEntity} spawns, respawns, idles, moves, gets damage, dies, is dead
 * and then either respawns or despawns completely.
 */
public abstract class EJXAbstractObject {

    protected EJXActor actor;

    // contains all information and properties that defines this object
    //    protected final EntityDef definition;

    // temporal vector object for local calculations.
    // handle with care no guarantee on values after other method calls
    // NOT THREAD SAFE!!
    protected Vector2 tmpVec;

    // unique identifier number managed by VJXGame.class
    private long id = -1;

    /*
     * Life cycle and activity status of the object in the world.
     */
    private boolean spawned = false; // true if the object is in the world
    //    private boolean alive = false; // true if the object is alive

    // The current life cycle status  of the object managed by the world.
    Lifecycle lifecycle;

    // the current activity of the object managed by the world, the player
    // and other objects in the world.
    Activity currentActivity;

    /*
     * Birth information of the object when spawned in the world.
     * The time when spawned. The position and velocity at which the object
     * spawns. And the time that passed since the object spawned in the world.
     */
    private boolean behindParent = false;
    protected float birthTime = 0, timeAlive = 0, lifespan = -1;
    public Vector2 birthPosition;

    /*
     * Spatial information of the object in the world.
     * The local center, the size, world position and orientation of the object.
     */
    protected boolean onScreen = false; // if true the object is on the screen
    private boolean drawOffScreen = false; // if true the object will be drawn even if not on screen
    private boolean markForPreload = false; // if true the object is currently preparing
    protected Orientation orientation = Orientation.RIGHT;

    private Polygon boundRect;
    private Vector2 wCenter;
    protected Vector2 position;

    // current frame to draw
    protected AtlasRegion frame;

    protected GameControl listener;

    // the entities physical body
    protected Body body;
    protected boolean useOrientation = true;
    private boolean usePhysics = false;
    private Box2DLogic physics;

    private boolean xFlipped = false;
    private boolean yFlipped = false;
    private float rotation = 0;

    private Array<CollisionListener> collListeners;

    //    protected float view = 0f;
    //    protected float range = 0f;

    //    private EJXEntity targetEntity;
    //    private boolean targetInView = false;
    //    private boolean targetInRange = false;

    private EJXGroup layerNode;
    protected boolean hasParent;
    private EJXEntity parent;
    protected HashSet<EJXEntity> children;

    //    private Array<EJXLogic> logics;

    private Array<EJXComponent> components;

    //    private HashMap<VJXCallback, Command[]> callBackCommands;
    private HashMap<String, Integer[]> soundCounter;
    private Boolean earthBound;

    private Waypoint spawnPoint;

    private boolean worldObject;

    private boolean enabled = true;
    private float disabledTime;

    public EJXAbstractObject(EJXMapObject mapObject) {
        //        definition = new EntityDef(def);
        id = EntityFactory.newID();
        soundCounter = new HashMap<>();
        //        logics = new Array<>();
        components = new Array<>();
        collListeners = new Array<>();
        birthPosition = new Vector2();
        position = new Vector2();
        wCenter = new Vector2();
        boundRect = new Polygon(new float[] { 0, 0, 0, 0, 0, 0, 0, 0 });

        setLifecycle(lifecycle = Lifecycle.CREATED);
        setActivity(currentActivity = Activity.DISABLED);

        tmpVec = new Vector2();
        children = new HashSet<>();

        //        callBackCommands = new HashMap<>();

        //        actor = new EJXActor(this);
        //        actor.setTouchable(Touchable.enabled);

        //        setName(def.getVJXProperties().get(EntityProperty.objName,
        //                        def.getDefName() + "#wID" + id, String.class));
        //        actor.setName(getName());
    }

    public void getBounds() {

    }

    //    private float stillTimer = 0;

    public void doStuff(float delta) {
        if (update(delta)) userAct(delta);

        //        if (getPhysics() != null) {
        //            if (isLifecycle(Lifecycle.STILL)) {
        //                stillTimer += delta;
        //                if (stillTimer > 1f) {
        //                    getPhysics().getBody().setAwake(false);
        //                    stillTimer -= 1f;
        //                }
        //            } else {
        //                getPhysics().getBody().setAwake(true);
        //                stillTimer = 0f;
        //            }
        //        }

        //        listener.getLevel().processLifecycle(this);
    }

    public SequenceAction interrupt(float duration) {
        SequenceAction sa = Actions.sequence();
        //        for (EJXComponent component : components) {
        //            sa.addAction(logic.interrupt(duration));
        //        }
        setActivity(Activity.EXECUTE);
        actor.addAction(sa);
        return sa;
    }

    public abstract void userAct(float deltaT);

    public abstract void userDraw(Batch batch, float parentAlpha);

    protected abstract void activityChanged(Activity oldA, Activity newA);

    protected boolean update(float deltaT) {
        //        long currentTime = System.nanoTime();
        if (!enabled) return false;

        if (disabledTime > 0f) {
            disabledTime -= deltaT;
            return false;
        }

        if (spawned) {
            if (isAlive()) {
                timeAlive += deltaT;
                //                checkTargetEntity();
            }
            //            if (usePhysics) physics.update(deltaT);
            for (int i = 0; i < components.size; i++) {
                //            currentTime = System.nanoTime();
                components.get(i).update(deltaT);
                //            DebugStats.objectsUpdateTime += System.nanoTime() - currentTime;
            }
            // translate object position to parallaxed actor position
            //            tmpVec.set(actor.getX(), actor.getY());
            //            actor.getParent().localToStageCoordinates(tmpVec);
            //            setPosition(tmpVec, false, false);
            //            updateBodyPos(false);
        }
        return spawned;
    }

    protected void render(Batch batch, float parentAlpha) {
        long currentTime = System.nanoTime();
        if (isOnScreen() || drawOffScreen) userDraw(batch, parentAlpha);

        //        if (actor.getDebug() && !isVisible()) userDraw(batch, .5f);
        DebugStats.objectsRenderTime += System.nanoTime() - currentTime;
    }

    protected void loadProperties(MapProperties properties) {
        Color c = properties.get("color", Color.WHITE, Color.class);
        actor.setColor(c);

        //        rotation = getDef().getValue(SpatialProperty.rotation, Float.class);
        //        setRotation(rotation);

        useOrientation = properties.get("useOrientation", true, Boolean.class);
        lifespan = properties.get("lifespan", -1f, Float.class);

        //        view = definition.getValue(EntityProperty.view, Float.class);
        //        view = view == -1 ? getScaledWidth() / 2 : view;
        //
        //        range = definition.getValue(EntityProperty.range, Float.class);
        //        range = range == -1 ? getScaledWidth() / 2 : range;

        //        boolean visible = properties.get("visible", true, Boolean.class);
        //        setVisible(visible);

        //        String prop = properties.get("target", "Kawaida", String.class);
        //        if (!(this instanceof PlayerEntity))
        //            setTargetEntity(listener.getEntityByName(prop));
        //        else setTargetEntity(null);

        earthBound = properties.get("earthBound", false, boolean.class);
        if (earthBound) {
            MassData md = body.getMassData();
            md.mass = 1000f;
            body.setMassData(md);
        }
    }

    public void drawShapeDebug(ShapeRenderer shapeRenderer) {

        // body center
        shapeRenderer.set(ShapeType.Line);
        shapeRenderer.setColor(Color.WHITE);
        if (body != null) {

            tmpVec.set(body.getPosition().x * B2DWorld.B2D_TO_WORLD,
                            body.getPosition().y * B2DWorld.B2D_TO_WORLD);
            actor.getParent().stageToLocalCoordinates(tmpVec);
            shapeRenderer.circle(tmpVec.x, tmpVec.y, 2.5f);
        }
        getLocalCenter(tmpVec);

        // draw view distance
        //        shapeRenderer.setColor(Color.ORANGE);
        //        shapeRenderer.circle(tmpVec.x, tmpVec.y, getView());

        // draw range
        //        shapeRenderer.setColor(Color.RED);
        //        shapeRenderer.circle(tmpVec.x, tmpVec.y, getRange());

        // draw origin
        shapeRenderer.setColor(Color.GREEN);
        shapeRenderer.circle(tmpVec.x, tmpVec.y, 5);

        shapeRenderer.polygon(boundRect.getTransformedVertices());

        drawFrameDebug(shapeRenderer);

        if (usePhysics) physics.drawShapeDebug(shapeRenderer);
        for (EJXComponent component : components) {
            component.drawShapeDebug(shapeRenderer);
        }
    }

    public void drawFrameDebug(ShapeRenderer shapeRenderer) {
    }

    public void drawBatchDebug(Batch batch, BitmapFont font) {
        if (!isOnScreen()) return;

        tmpVec.set(getWorldPosition());
        font.draw(batch, getName() + " (" + getLifecycle() + ", "
                        + getActivity() + ")", tmpVec.x + 5,
                        tmpVec.y + getScaledHeight() - 5);

        font.draw(batch, "x: " + getX() + ", y: " + getY() + " dir: "
                        + orientation, tmpVec.x + 5, tmpVec.y + 25);

        if (usePhysics) physics.drawBatchDebug(batch, font);
        for (EJXComponent component : components) {
            component.drawBatchDebug(batch, font);
        }
    }

    // TODO needed
    //    protected float execute(Command command, SequenceAction sequence) {
    //        //        VJXLogger.log(this + " exec single " + command);
    //        if (command == null) return 0f;
    //        return command.execute(sequence);
    //    }
    //
    //    public float execSingleCmd(Command cmd) {
    //        return execSingleCmd(cmd, 0f);
    //    }
    //
    //    public float execSingleCmd(Command cmd, float delay) {
    //        //        if (cmd.getExecutor() != null && cmd.getExecutor() != this)
    //        //            return cmd.getExecutor().execSingleCmd(cmd, delay);
    //
    //        SequenceAction sa = Actions.sequence();
    //        float duration = execSingleCmd(cmd, sa, delay);
    //
    //        actor.addAction(sa);
    //        return duration;
    //    }
    //
    //    public float execSingleCmd(Command cmd, SequenceAction sequence) {
    //        return execSingleCmd(cmd, sequence, 0f);
    //    }
    //
    //    public float execSingleCmd(Command cmd, SequenceAction sequence,
    //                    float delay) {
    //        if (cmd == null) return 0f;
    //
    //        VJXLogger.logIncr(LogCategory.EXEC | LogCategory.DETAIL,
    //                        this + " exec single (delay " + delay + "):" + cmd);
    //        //        if (cmd.getExecutor() != null && cmd.getExecutor() != this) {
    //        //            VJXLogger.logDecr(LogCategory.EXEC | LogCategory.DETAIL,
    //        //                            this + " delegate exec " + cmd + " to "
    //        //                                            + cmd.getExecutor());
    //        //            return cmd.getExecutor().execSingleCmd(cmd, sequence, delay);
    //        //        }
    //
    //        float duration = 0;
    //        if (delay > 0) {
    //            sequence.addAction(Actions.delay(delay));
    //            duration += delay;
    //        }
    //
    //        duration += execute(cmd, sequence);
    //
    //        VJXLogger.logDecr(LogCategory.EXEC | LogCategory.DETAIL, this
    //                        + " finished exec " + cmd + ". Runs " + duration + "s");
    //        return duration;
    //    }
    //
    //    public SequenceAction execMultiCmds(Command[] commands) {
    //        return execMultiCmds(commands, 0f);
    //    }
    //
    //    public SequenceAction execMultiCmds(Command[] commands, float delay) {
    //        SequenceAction sa = Actions.sequence();
    //        execMultiCmds(commands, sa, delay);
    //        actor.addAction(sa);
    //        return sa;
    //    }
    //
    //    public SequenceAction execMultiCmds(Command[] commands,
    //                    SequenceAction sequence) {
    //        return execMultiCmds(commands, sequence, 0f);
    //    }
    //
    //    public SequenceAction execMultiCmds(Command[] commands,
    //                    SequenceAction sequence, float delay) {
    //        if (commands == null || commands.length == 0) return sequence;
    //
    //        VJXLogger.logIncr(LogCategory.EXEC | LogCategory.DETAIL,
    //                        this + " exec multi commands(delay " + delay + ")");
    //
    //        float duration = 0f;
    //        if (delay > 0) {
    //            sequence.addAction(Actions.delay(delay));
    //            duration += delay;
    //        }
    //
    //        float execTime = 0;
    //        for (Command cmd : commands) {
    //            execTime = execSingleCmd(cmd, sequence, 0);
    //            duration += execTime;
    //        }
    //
    //        VJXLogger.logDecr(LogCategory.EXEC | LogCategory.DETAIL, this
    //                        + " finished multi commands. Runs " + duration + "s");
    //
    //        return sequence;
    //    }
    //
    //    public SequenceAction exec(VJXCallback type) {
    //        return exec(type, true);
    //    }
    //
    //    public SequenceAction exec(VJXCallback type, boolean selfExec) {
    //        //        VJXLogger.log("exec:" + this + ":" + type);
    //        SequenceAction sa = Actions.sequence();
    //        exec(type, sa);
    //        if (selfExec) actor.addAction(sa);
    //        else if (listener != null) listener.addActions(sa);
    //        return sa;
    //    }
    //
    //    public SequenceAction exec(VJXCallback type, SequenceAction sequence) {
    //        VJXLogger.logIncr(LogCategory.EXEC,
    //                        this + " execute commands '" + type + "'.");
    //        Command[] actions = callBackCommands.get(type);
    //
    //        if (actions == null || actions.length == 0) {
    //            VJXLogger.logDecr(LogCategory.EXEC,
    //                            this + " no commands for '" + type + "'.");
    //            return sequence;
    //
    //        }
    //
    //        execMultiCmds(actions, sequence);
    //        VJXLogger.logDecr(LogCategory.EXEC,
    //                        this + " finished commands '" + type + "'.");
    //        return sequence;
    //    }

    public void setGravityScale(float gs) {
        if (usePhysics) physics.setGravityScale(gs);
    }

    public void spawn(World world) {
        VJXLogger.log(LogCategory.INIT | LogCategory.DETAIL, this + " spawn");

        if (!isLifecycle(Lifecycle.CREATED)) despawn();

        //        setPosition(birthPosition);
        //        usePhysics = definition.getValue(DefinitionProperty.usePhysics, false,
        //                        Boolean.class);
        //        if (usePhysics) {
        //            LevelUtil.loadLogicProps(definition.getCustomProperties(), "box2d");
        //            physics = (Box2DLogic) EJXLogics.newLogic(VJXLogicType.BOX2D, this);
        //            physics.init(definition.getCustomProperties());
        //            body = physics.createBody(world);
        //        }

        spawned = true;
        respawn();
    }

    public void respawn() {
        if (!spawned) return;
        VJXLogger.log(LogCategory.INIT | LogCategory.DETAIL, this + " respawn");

        birthTime = System.currentTimeMillis();
        timeAlive = 0;

        actor.clearActions();
        actor.getColor().a = 1f;

        children.clear();

        //        setPosition(birthPosition);

        //        loadProperties(definition.getCustomProperties());

        setLifecycle(Lifecycle.STILL);
        setActivity(Activity.IDLE);

        //        EntityFactory.loadAnimations(this, listener.getGame().assets);

        if (usePhysics) {
            updateBody();
            physics.setGhost(false);
        }

        // TODO needed
        //        initLogics();
        //        loadCallBacks(definition.getCustomProperties());

        //        onRespawn();

        //        if (parent != null) {
        //            parent.childSpawned(this);
        //        }
        worldObject = getLayer().getName().contains(VJXString.WORLD);
    }

    public void despawn() {
        despawn(true, false);
    }

    public void despawn(boolean execDespawn, boolean despawnChildren) {
        VJXLogger.log(LogCategory.INIT | LogCategory.DETAIL,
                        "Despawning " + getName());
        //        if (execDespawn) onDespawn();

        spawned = false;

        actor.clearActions();
        actor.remove();

        if (usePhysics) physics.destroyBody();

        //        if (hasParent) parent.childDespawned(this);

        if (despawnChildren) {
            despawnChildren(execDespawn, despawnChildren);
        }

        setLifecycle(Lifecycle.CREATED);
    }

    /**
     * Object callback implementation
     */

    //    @Override
    //    public void onRespawn() {
    //        exec(VJXCallback.onRespawn);
    //
    //        //        if (usePhysics) physics.onRespawn();
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).onRespawn();
    //        }
    //    }
    //
    //    @Override
    //    public void onDespawn() {
    //        exec(VJXCallback.onDespawn, false);
    //
    //        //        if (usePhysics) physics.onDespawn();
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).onDespawn();
    //        }
    //    }
    //
    //    @Override
    //    public void onEnd() {
    //        exec(VJXCallback.onEnd, false);
    //
    //        //        if (usePhysics) physics.onEnd();
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).onEnd();
    //        }
    //    }
    //
    //    @Override
    //    public void onStart() {
    //        exec(VJXCallback.onStart, false);
    //
    //        //        if (usePhysics) physics.onStart();
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).onStart();
    //        }
    //    }
    //
    //    @Override
    //    public void onDamage(int oldHP, int newHP) {
    //        //        if (usePhysics) physics.onDamage(oldHP, newHP);
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).onDamage(oldHP, newHP);
    //        }
    //    }
    //
    //    @Override
    //    public void onDie() {
    //        //        if (usePhysics) physics.onDie();
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).onDie();
    //        }
    //    }
    //
    //    @Override
    //    public void onDead() {
    //        //        if (usePhysics) physics.onDead();
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).onDead();
    //        }
    //    }
    //
    //    @Override
    //    public void onTrigger(boolean triggered, VJXData thisData, EJXEntity other,
    //                    VJXData otherData) {
    //        if (!isAlive() || isActivity(Activity.DISABLED)) return;
    //
    //        exec(triggered ? VJXCallback.onTrigger : VJXCallback.offTrigger);
    //
    //        //        if (usePhysics)
    //        //            physics.onTrigger(triggered, thisData, other, otherData);
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).onTrigger(triggered, thisData, other, otherData);
    //        }
    //    }
    //
    //    @Override
    //    public void onActivated(EJXEntity activator, boolean activated) {
    //        if (!isAlive() || isActivity(Activity.DISABLED)) return;
    //
    //        exec(activated ? VJXCallback.onActivate : VJXCallback.onDeactivate);
    //
    //        //        if (usePhysics) physics.onActivated(activator, activated);
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).onActivated(activator, activated);
    //        }
    //    }
    //
    //
    //    @Override
    //    public void onScreenChanged(boolean onScreen) {
    //        this.onScreen = onScreen;
    //        exec(onScreen ? VJXCallback.onScreen : VJXCallback.offScreen);
    //
    //        //        if (usePhysics) physics.onScreenChanged(onScreen);
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).onScreenChanged(onScreen);
    //        }
    //
    //        if (onScreen)
    //            playSfx(getDefName() + VJXString.SEP_DDOT + VJXString.SFX_onScreen);
    //        else {
    //            playSfx(getDefName() + VJXString.SEP_DDOT
    //                            + VJXString.SFX_offScreen);
    //        }
    //    }
    //
    //    @Override
    //    public void targetInView(boolean inView) {
    //        exec(inView ? VJXCallback.targetInView : VJXCallback.targetOutView);
    //
    //        //        if (usePhysics) physics.targetInView(inView);
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).targetInView(inView);
    //        }
    //    }
    //
    //    @Override
    //    public void targetInRange(boolean inRange) {
    //        exec(inRange ? VJXCallback.targetInRange : VJXCallback.targetOutRange);
    //
    //        //        if (usePhysics) physics.targetInRange(inRange);
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).targetInRange(inRange);
    //        }
    //    }
    //
    //    @Override
    //    public void targetChanged(EJXEntity targetEntity) {
    //        //        if (usePhysics) physics.targetChanged(targetEntity);
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).targetChanged(targetEntity);
    //        }
    //    }
    //
    //    @Override
    //    public void positionReached() {
    //        //        if (usePhysics) physics.positionReached();
    //
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).positionReached();
    //        }
    //    }

    //    public void setName(String name) {
    //        if (name == null || name.isEmpty())
    //            name = definition.getDefName() + "#" + id;
    //        actor.setName(name);
    //    }

    public String getName() {
        return "EJXAbstractObject";
    }

    //    public String getDefName() {
    //        return definition.getDefName();
    //    }

    //    public void setBirthPlace(Vector2 position) {
    //        setBirthPlace(position.x, position.y);
    //    }
    //
    //    public void setBirthPlace(float x, float y) {
    //        birthPosition.set(x, y);
    //    }
    //
    //    public Vector2 getBirthPlace() {
    //        return birthPosition;
    //    }
    //
    //    public void setCenter(Vector2 center) {
    //        setCenter(center.x, center.y);
    //    }
    //
    //    public void setCenter(float x, float y) {
    //        setPosition(x - getOriginX(), y - getOriginY());
    //    }
    //
    //    public void setPosition(Vector2 pos) {
    //        setPosition(pos.x, pos.y, false, true);
    //    }
    //
    //    public void setPosition(float x, float y) {
    //        setPosition(x, y, false, true);
    //    }
    //
    //    public void setPosition(Vector2 pos, boolean setBody, boolean setActor) {
    //        setPosition(pos.x, pos.y, setBody, setActor);
    //    }

    public void setPosition(float x, float y, boolean setBodyPos,
                    boolean setActorPos) {
        //        if (!spawned) return;

        if (setActorPos) {
            Actor parent = actor.getParent();
            tmpVec.set(x, y);
            if (parent != null) parent.stageToLocalCoordinates(tmpVec);
            actor.setPosition(tmpVec.x, tmpVec.y);
        }
        position.set(x, y);

        boundRect.setPosition(position.x, position.y);

        if (setBodyPos) {
            updateBodyPos(true);
        }
    }

    private void updateBodyPos(boolean force) {
        if (lifecycle == Lifecycle.DEAD || lifecycle == Lifecycle.MOVE
                        || force) {
            if (usePhysics) physics.setBodyPosition(getX(), getY());
        }
    }

    //    public void updatePosFromBody() {
    //        if (usePhysics) {
    //            tmpVec.set(body.getPosition()).scl(B2DWorld.B2D_TO_WORLD);
    //            tmpVec.x = Math.round(tmpVec.x * 1000) / 1000f;
    //            tmpVec.y = Math.round(tmpVec.y * 1000) / 1000f;
    //            setPosition(tmpVec, false, true);
    //        }
    //    }

    //    protected void loadCallBacks(MapProperties properties) {
    //        String triggerType;
    //        VJXTrigger trigger;

    //        for (VJXCallback callback : VJXCallback.values()) {
    // TODO needed
    //            Command[] cmds = Commands.getCommands(callback, properties,
    //                            listener, this);
    //            if (cmds != null && cmds.length > 0) {
    //                if (callback == VJXCallback.onCommands) {
    //                    triggerType = properties.get(VJXString.TRIGGER_TYPE,
    //                                    VJXString.TRIGGER_RANGE, String.class);
    //                    trigger = VJXTrigger.get(triggerType);
    //                    callback = VJXTrigger.getCallback(trigger, true);
    //                } else if (callback == VJXCallback.offCommands) {
    //                    triggerType = properties.get(VJXString.TRIGGER_TYPE,
    //                                    VJXString.TRIGGER_RANGE, String.class);
    //                    trigger = VJXTrigger.get(triggerType);
    //                    callback = VJXTrigger.getCallback(trigger, true);
    //                }
    //                setCallBackCommands(callback, cmds);
    //            }
    //        }

    //        Waypoint wp = Waypoints.getWaypoint(getName());
    //        if (wp != null) {
    //            wp.setOnSetCmds(getCallbackCommands(VJXCallback.onSet));
    //            wp.setOnReachCmds(getCallbackCommands(VJXCallback.onReach));
    //        }
    //    }

    //    public void setCallBackCommands(VJXCallback type, Command... cmds) {
    //        callBackCommands.put(type, cmds);
    //    }
    //
    //    public Command[] getCallbackCommands(VJXCallback callback) {
    //        return callBackCommands.get(callback);
    //    }
    //
    //    public void removeCallback(VJXCallback callback) {
    //        callBackCommands.remove(callback);
    //    }

    // TODO needed
    //    private void initLogics() {
    //        collListeners.clear();
    //        MapProperties props = definition.getCustomProperties();
    //        String logics = props.get("logics", VJXString.STR_EMPTY, String.class);
    //        if (logics.isEmpty()) return;
    //
    //        String[] logicsA = logics.split(VJXString.SEP_LIST);
    //
    //        for (String logic : logicsA) {
    //            EJXLogicType l = EJXLogics.getType(logic);
    //            if (l != null) initLogic(l, props);
    //            else VJXLogger.log(LogCategory.ERROR,
    //                            this + " could not find logic for " + logic);
    //        }
    //    }

    //    private EJXLogic initLogic(EJXLogicType logic, MapProperties properties) {
    //        EJXLogic l = EJXLogics.newLogic(logic, this);
    //        if (l != null) {
    //            logics.add(l);
    //
    //            if (l instanceof CollisionListener)
    //                collListeners.add((CollisionListener) l);
    //            l.init(properties);
    //
    //            return l;
    //        }
    //        return null;
    //    }

    public void addComponent(EJXComponent component) {
        if (component != null) {
            components.add(component);

            if (component instanceof CollisionListener)
                collListeners.add((CollisionListener) component);
        }
    }

    public boolean hasComponent(EJXLogicType logic) {
        return getComponent(logic) != null;
    }

    public EJXComponent getComponent(EJXLogicType logic) {
        for (EJXComponent l : components) {
            if (l.getType() == logic) return l;
        }
        return null;
    }

    public void removeComponent(EJXLogicType logic) {
        EJXComponent l = getComponent(logic);
        components.removeValue(l, true);
    }

    public Array<EJXComponent> getComponents() {
        return components;
    }

    public boolean isAlive() {
        return !isLifecycle(Lifecycle.DEAD);
    }

    //    public void setTargetEntity(EJXEntity targetEntity) {
    //        this.targetEntity = targetEntity;
    //        targetChanged(targetEntity);
    //    }
    //
    //    public EJXEntity getTargetEntity() {
    //        return targetEntity;
    //    }
    //
    //    private void checkTargetEntity() {
    //        boolean tmpB = targetInView;
    //        targetInView = view > 0 ? checkTargetRange(view) : false;
    //
    //        // if target enters or leaves view range do stuff
    //        if (tmpB ^ targetInView) targetInView(targetInView);
    //
    //        tmpB = targetInRange;
    //        targetInRange = range > 0 ? checkTargetRange(range) : false;
    //
    //        // if target enters or leaves act range do stuff
    //        if (tmpB ^ targetInRange) targetInRange(targetInRange);
    //    }
    //
    //    private boolean checkTargetRange(float range) {
    //        if (targetEntity == null) return false;
    //        if (getWorldCenter().dst(targetEntity.getWorldCenter()) <= range)
    //            return true;
    //        return false;
    //    }
    //
    //    public boolean isTargetInView() {
    //        return targetInView;
    //    }
    //
    //    public boolean isTargetInRange() {
    //        return targetInRange;
    //    }

    public void setScale(float scale) {
        setScale(scale, scale);
    }

    public void setScale(float scaleX, float scaleY) {
        actor.setScale(scaleX, scaleY);
        boundRect.setScale(scaleX, scaleY);

        if (usePhysics) updateBody();
    }

    public float getScaleX() {
        return actor.getScaleX();
    }

    public float getScaleY() {
        return actor.getScaleY();
    }

    public Vector2 getScale(Vector2 scale) {
        return scale.set(actor.getScaleX(), actor.getScaleY());
    }

    public void setRotation(float rotation) {
        actor.setRotation(rotation);
        boundRect.setRotation(rotation);
    }

    public float getRotation() {
        return actor.getRotation();
    }

    private void updateBound(float width, float height) {
        float[] verts = boundRect.getVertices();
        tmpVec.set(getWorldPosition());
        verts[0] = 0;
        verts[1] = 0;

        verts[2] = width;
        verts[3] = 0;

        verts[4] = width;
        verts[5] = height;

        verts[6] = 0;
        verts[7] = height;

        boundRect.setVertices(verts);
    }

    public Polygon getBound() {
        return boundRect;
    }

    public void setSize(Vector2 newSize) {
        setSize(newSize.x, newSize.y);
    }

    public void setSize(float width, float height) {
        actor.setSize(width, height);
        updateBound(width, height);
        //        actor.addAction(EJXActions.updateBody(this));
    }

    public float getWidth() {
        return actor.getWidth();
    }

    public float getHeight() {
        return actor.getHeight();
    }

    public float getScaledWidth() {
        return actor.getWidth() * actor.getScaleX();
    }

    public float getScaledHeight() {
        return actor.getHeight() * actor.getScaleY();
    }

    public Vector2 getSize(Vector2 size) {
        return size.set(getScaledWidth(), getScaledHeight());
    }

    public float getOriginX() {
        return getScaledWidth() / 2f;
    }

    public float getOriginY() {
        return getScaledHeight() / 2f;
    }

    public void setFrame(AtlasRegion f) {
        frame = f;
    }

    public AtlasRegion getFrame() {
        return frame;
    }

    public long getID() {
        return id;
    }

    //    public EntityDef getDef() {
    //        return definition;
    //    }

    public boolean setLifecycle(Lifecycle lifecycle) {
        if (lifecycle == null || this.lifecycle == lifecycle) return false;

        VJXLogger.log(LogCategory.STATUS,
                        this + " lifecycle changed -->" + lifecycle, 1);

        Lifecycle old = this.lifecycle;
        this.lifecycle = lifecycle;

        lifecycleChanged(old, lifecycle);
        return true;
    }

    public boolean isLifecycle(Lifecycle lifecycle) {
        return this.lifecycle == lifecycle;
    }

    public Lifecycle getLifecycle() {
        return lifecycle;
    }

    public boolean setActivity(Activity activity) {
        if (activity == null || currentActivity == activity) return false;
        VJXLogger.log(LogCategory.STATUS,
                        this + " activity changed -->" + activity, 1);

        Activity old = currentActivity;
        currentActivity = activity;

        activityChanged(old, activity);
        return true;
    }

    /**
     * Disabled entities don't move, collide, act or do anything else. They just
     * remain in the scene in their last animation state and idle around.
     * Player controls are deactivated while disabled.
     *
     * @return true if the entity is enabled.
     */
    public boolean isEnabled() {
        return !isActivity(Activity.DISABLED);
    }

    /**
     * Executing entities are currently performing a sequence of game actions.
     * During this time they can collide and execute the predefined sequence.
     * Player controls are deactivated while executing.
     *
     * @return true if the entity is currently performing game actions.
     */
    public boolean isExecuting() {
        return isActivity(Activity.EXECUTE);
    }

    /**
     * An entity is active if it is alive, enabled and not executing. This
     * basically means it can do whatever it likes or has to. It can collide,
     * perform new game action sequences, react to game logic or be freely
     * controlled by the player.
     *
     * @return true if the entity is active
     */
    public boolean isActive() {
        return isAlive() && isEnabled() && !isExecuting();
    }

    public boolean isActivity(Activity a) {
        return currentActivity == a;
    }

    public Activity getActivity() {
        return currentActivity;
    }

    //    public void setVisible(boolean visible) {
    //        actor.setVisible(visible);
    //    }
    //
    //    public boolean isVisible() {
    //        return actor.isVisible();
    //    }

    public boolean usesPhysics() {
        return usePhysics;
    }

    public void setDrawOffScreen(boolean drawOffScreen) {
        this.drawOffScreen = drawOffScreen;
    }

    //    public void setView(float view) {
    //        this.view = view;
    //    }
    //
    //    public float getView() {
    //        return view;
    //    }
    //
    //    public void setRange(float range) {
    //        this.range = range;
    //    }
    //
    //    public float getRange() {
    //        return range;
    //    }

    public void setOrientation(EJXEntity other) {
        Vector2 wcO = other.getWorldCenter();
        Vector2 wcT = getWorldCenter();
        tmpVec.set(wcO).sub(wcT);
        setOrientation(tmpVec);
    }

    public void setOrientation(Vector2 direction) {
        if (direction.x < 0) setOrientation(Orientation.LEFT);
        else if (direction.x > 0) setOrientation(Orientation.RIGHT);
    }

    public void setOrientation(Orientation orientation) {
        setOrientation(orientation, true);
    }

    public void setOrientation(Orientation orientation, boolean flipBodyX) {
        if (orientation != this.orientation) {
            if (useOrientation && usePhysics && flipBodyX) {
                physics.flipBodyX();
            }
            this.orientation = orientation;
        }
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public Body getBody() {
        return body;
    }

    public Fixture getBodyFixture() {
        return getFixture("box2d_body");
    }

    //    public EJXActor getActor() {
    //        return actor;
    //    }
    //
    //    public float getDeltaVelX() {
    //        if (hasLogic(VJXLogicType.MOVE)) { return ((MoveLogic) getLogic(
    //                        VJXLogicType.MOVE)).getDeltaVelX(); }
    //        return 0;
    //    }
    //
    //    public float getDeltaVelY() {
    //        if (hasLogic(VJXLogicType.MOVE)) { return ((MoveLogic) getLogic(
    //                        VJXLogicType.MOVE)).getDeltaVelY(); }
    //        return 0;
    //    }
    //
    //    public Vector2 getVelocity() {
    //        if (hasLogic(VJXLogicType.MOVE)) { return ((MoveLogic) getLogic(
    //                        VJXLogicType.MOVE)).getVelocity(); }
    //        return tmpVec.setZero();
    //    }

    public Vector2 getLocalPosition(Vector2 v) {
        return v.set(actor.getX(), actor.getY());
    }

    public Vector2 getWorldPosition() {
        return position;
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public Vector2 getPosition() {
        return position;
    }

    public float getLCX() {
        tmpVec.x = getOriginX();
        tmpVec.y = getOriginY();
        tmpVec.rotate(actor.getRotation());
        return actor.getX() + tmpVec.x;
    }

    public float getLCY() {
        tmpVec.x = getOriginX();
        tmpVec.y = getOriginY();
        tmpVec.rotate(actor.getRotation());
        return actor.getY() + tmpVec.y;
    }

    public Vector2 getLocalCenter(Vector2 v) {
        v.set(getLCX(), getLCY());
        return v;
    }

    public float getWCX() {
        tmpVec.x = getOriginX();
        tmpVec.y = getOriginY();
        tmpVec.rotate(actor.getRotation());
        return position.x + tmpVec.x;
    }

    public float getWCY() {
        tmpVec.x = getOriginX();
        tmpVec.y = getOriginY();
        tmpVec.rotate(actor.getRotation());
        return position.y + tmpVec.y;
    }

    public Vector2 getWorldCenter() {
        wCenter.set(getWCX(), getWCY());
        return wCenter;
    }

    public boolean isSpawned() {
        return spawned;
    }

    public float getTimeAlive() {
        return timeAlive;
    }

    public void setLifespan(float lifespan) {
        this.lifespan = lifespan;
    }

    public float getLifespan() {
        return lifespan < 0 ? Float.MAX_VALUE : lifespan;
    }

    public void setGameControl(GameControl el) {
        listener = el;
    }

    public GameControl getGameControl() {
        return listener;
    }

    public void clearListeners() {
        listener = null;
        collListeners.clear();
    }

    protected void lifecycleChanged(Lifecycle oldLC, Lifecycle newLC) {
        switch (newLC) {
            case DEAD:
                //                onDead();
                break;
            default:
        }
    }

    public void updateBody() {
        setFlipX(xFlipped);
        setFlipY(yFlipped);

        if (!usePhysics) return;

        physics.clearFixtures();
        createBodyFixtures();

        physics.fixturesCreated();

        //        for (int i = 0; i < components.size; i++) {
        //            components.get(i).fixturesCreated();
        //        }

        body.setTransform(body.getPosition(),
                        getRotation() * MathUtils.degreesToRadians);

        //        if (orientation == Orientation.LEFT || xFlipped) {
        //            VJXLogger.log(this + " flip body");
        //            physics.flipBodyX();
        //        }

        if (yFlipped) physics.flipBodyY();
    }

    protected void createBodyFixtures() {
        if (usePhysics) physics.createBodyFixtures();
    }

    public Fixture getFixture(String name) {
        if (!usePhysics) return null;
        return physics.getFixture(name);
    }

    public Box2DLogic getPhysics() {
        return physics;
    }

    public void setPhysics(B2DBit bit, String fixtureName) {
        if (usePhysics) physics.setPhysics(bit, fixtureName);
    }

    public void removePhysics(B2DBit bit) {
        if (usePhysics) physics.removePhysics(bit);
    }

    public void setFlipX(boolean flip) {
        xFlipped = flip;
    }

    public boolean isFlipX() {
        return xFlipped;
    }

    public void setFlipY(boolean flip) {
        yFlipped = flip;
    }

    public boolean isFlipY() {
        return yFlipped;
    }

    public void addSoundCounter(String name, int count) {
        Integer[] counter = { count, count };
        soundCounter.put(name, counter);
    }

    public long playSfx(String sfxName) {
        return playSfx(sfxName, false);
    }

    public long playSfx(String sfxName, boolean loop) {
        AudioControl sfx = listener.getAudioControl();
        long id = -1;

        //        VJXLogger.log(this + "playSfx:" + sfxName);
        //        VJXLogger.log(this + "playSfx:" + sfxName, 1);
        //        VJXLogger.log(this + "-----------------------------------------" + sfxName);
        if (isOnScreen()) {
            //            VJXLogger.log(this + " play sound " + sfxName);
            if (soundCounter.containsKey(sfxName)) {
                //                VJXLogger.log("soundCounter.containsKey(" + sfxName + ")");
                Integer[] counter = soundCounter.get(sfxName);
                counter[0]++;
                if (counter[0] >= counter[1]) {
                    //                    VJXLogger.log("counter[0] >= counter[1]");
                    id = sfx.playDynSFX(sfxName, loop);
                    counter[0] = 0;
                }
            } else {
                //                VJXLogger.log("!soundCounter.containsKey(" + sfxName + ")");
                id = sfx.playDynSFX(sfxName, loop);
            }
        }
        return id;
    }

    public void stopSfx(String sfxName) {
        listener.stopSfx(sfxName);
    }

    public void stopSfx(String name, long id) {
        AudioControl sfx = listener.getAudioControl();
        sfx.stopSound(name, id);
    }

    public boolean isOnScreen() {
        return onScreen;
    }

    public void setParent(EJXEntity parent) {
        hasParent = parent != null;
        this.parent = parent;
    }

    public EJXEntity getParent() {
        return parent;
    }

    //    protected void childSpawned(EJXEntity child) {
    //        children.add(child);
    //        if (usePhysics) physics.childSpawned(child);
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).childSpawned(child);
    //        }
    //    }
    //
    //    public void childDespawned(EJXEntity child) {
    //        children.remove(child);
    //        if (usePhysics) physics.childDespawned(child);
    //        for (int i = 0; i < components.size; i++) {
    //            components.get(i).childDespawned(child);
    //        }
    //    }

    public void despawnChildren() {
        despawnChildren(true, false);
    }

    public void despawnChildren(boolean execDespawn, boolean despawnChildren) {
        for (EJXEntity child : children) {
            child.setLifecycle(Lifecycle.DESPAWN);
        }
        children.clear();
    }

    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (collListeners.size == 0) { return true; }

        boolean accept = true;
        if (usePhysics)
            physics.checkBeginCollision(thisFix, otherFix, contact, other);
        for (CollisionListener cl : collListeners) {
            accept |= cl.checkBeginCollision(thisFix, otherFix, contact, other);
        }
        return accept && !isActivity(Activity.DISABLED);
    }

    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (collListeners.size == 0) return true;

        boolean accept = true;
        if (usePhysics)
            physics.checkEndCollision(thisFix, otherFix, contact, other);
        for (CollisionListener cl : collListeners) {
            accept |= cl.checkEndCollision(thisFix, otherFix, contact, other);
        }
        return accept && !isActivity(Activity.DISABLED);
    }

    //    public void visitBeginColl(short bit, VJXData data, short otherBit,
    //                    EntityImpl other, VJXData otherData) {
    //        if (B2DBit.TRIGGER.isCategory(bit))
    //            onTrigger(true, data, other, otherData);
    //    };
    //
    //    public void visitEndColl(short bit, VJXData data, short otherBit,
    //                    EntityImpl other, VJXData otherData) {
    //        if (B2DBit.TRIGGER.isCategory(bit))
    //            onTrigger(false, data, other, otherData);
    //    };

    public String getInfo() {
        String s = toString();

        //        s += "\n  BodyType       : " + definition.getBodyType();
        s += "\n  Status         : " + lifecycle;
        s += "\n  Face           : " + orientation;
        s += "\n  Size           : " + getScaledWidth() + "x"
                        + getScaledHeight();
        s += "\n  Postion        : " + getWorldPosition();
        s += "\n  World center   : " + getWorldCenter();
        s += "\n  UserData       : " + actor.getUserObject();
        s += "\n  Body           : " + body;

        s += "\n";

        return s;
    }

    @Override
    public String toString() {
        return getName() + "(" + lifecycle + ", " + currentActivity + ")#"
                        + getID();
    }

    public boolean hasCollPrio() {
        return false;
    }

    public void setLayer(EJXGroup layerNode) {
        this.layerNode = layerNode;
    }

    public EJXGroup getLayer() {
        return layerNode;
    }

    public float getDistance(EJXEntity other) {
        return getDistance(other.getWorldCenter());
    }

    public float getDistance(Vector2 pos) {
        return getWorldCenter().dst(pos);
    }

    public Vector2 getDistance(EJXEntity target, Vector2 store) {
        if (store == null) store = new Vector2();

        store.x = Math.abs(getWCX() - target.getWCX());
        store.y = Math.abs(getWCY() - target.getWCY());
        return store;
    }

    public void setBehindParent(boolean behind) {
        behindParent = behind;
    }

    public boolean isBehindParent() {
        return behindParent;
    }

    public void setEnabled(boolean enable) {
        enabled = enable;
    }

    public void setSpawnPoint(Waypoint wp) {
        spawnPoint = wp;
    }

    public void removeSpawnPoint() {
        if (spawnPoint == null) return;
        //        spawnPoint.remove(this);
        spawnPoint = null;
    }

    public void markForPreload() {
        markForPreload = true;
    }

    public boolean isMarked() {
        return markForPreload;
    }

    public void unmark() {
        markForPreload = false;
    }

    public boolean isWorldObject() {
        return worldObject;
    }

    public void disable(float time) {
        disabledTime = time;
    }

    //    public String getSoundName() {
    //        return getDef().getVJXProperties().get(SourceProperty.tileName,
    //                        String.class);
    //    }
}