package de.venjinx.ejx.tiled.entity;

import com.badlogic.gdx.graphics.g2d.Batch;

import de.venjinx.ejx.tiled.object.EJXMapObject;
import de.venjinx.ejx.util.EJXTypes.Activity;

public class EJXObject extends EJXAbstractObject {

    private EJXMapObject mapObject;

    public EJXObject(EJXMapObject mapObject) {
        super(mapObject);
        this.mapObject = mapObject;
    }

    @Override
    public String getName() {
        return mapObject.getName();
    }

    @Override
    public void userAct(float deltaT) {
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
    }

    @Override
    protected void activityChanged(Activity oldA, Activity newA) {
    }
}