package de.venjinx.ejx.tiled.object;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.tiled.EJXTiledMap;
import de.venjinx.ejx.tiled.tile.EJXAnimatedTile;
import de.venjinx.ejx.tiled.tile.EJXTile;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.loader.EJXLoaderUtil;

public class EJXTileObject extends EJXMapObject {

    private Sprite sprite;
    private EJXTile tile;

    public EJXTileObject(String name, String type, int objectId) {
        this(name, type, objectId, 0, 0);
        sprite = new Sprite();
    }

    public EJXTileObject(String name, String type, int objectId, int localId, int idOffset) {
        super(name, type, objectId);
        sprite = new Sprite();
    }

    @Override
    public void init() {
        super.init();
        setRegion(tile.getTextureRegion());
    }

    public void draw(Batch batch) {
        if (tile instanceof EJXAnimatedTile) {
            EJXAnimatedTile aTile = (EJXAnimatedTile) tile;
            TextureRegion tr = aTile.getCurrentFrame().getTextureRegion();
            setRegion(tr);
        }
        sprite.draw(batch);
    }

    public float getSourceWidth() {
        return sprite.getWidth();
    }

    public float getSourceHeight() {
        return sprite.getHeight();
    }

    @Override
    public float getWidth() {
        return sprite.getRegionWidth() * sprite.getScaleX();
    }

    @Override
    public float getHeight() {
        return sprite.getRegionHeight() * sprite.getScaleY();
    }

    public Sprite getSprite() {
        return sprite;
    }

    @Override
    public float getX() {
        return sprite.getX();
    }

    public void setX(float x) {
        sprite.setX(x);
    }

    @Override
    public float getY() {
        return sprite.getY();
    }

    public void setY(float y) {
        sprite.setY(y);
    }

    public float getOriginX() {
        return sprite.getOriginX();
    }

    public void setOriginX(float x) {
        sprite.setOrigin(x, getOriginY());
    }

    public float getOriginY() {
        return sprite.getOriginY();
    }

    public void setOriginY(float y) {
        sprite.setOrigin(getOriginX(), y);
    }

    public float getScaleX() {
        return sprite.getScaleX();
    }

    public void setScaleX(float x) {
        sprite.setScale(x, getScaleY());
    }

    public float getScaleY() {
        return sprite.getScaleY();
    }

    public void setScaleY(float y) {
        sprite.setScale(getScaleX(), y);
    }

    public boolean isFlipX() {
        return sprite.isFlipX();
    }

    public boolean isFlipY() {
        return sprite.isFlipY();
    }

    public void setFlipX(boolean flipX) {
        setFlip(flipX, sprite.isFlipY());
    }

    public void setFlipY(boolean flipY) {
        setFlip(sprite.isFlipX(), flipY);
    }

    public void setFlip(boolean flipX, boolean flipY) {
        sprite.setFlip(flipX, flipY);
    }

    public void flipX() {
        flip(true, false);
    }

    public void flipY() {
        flip(false, true);
    }

    public void flip(boolean flipX, boolean flipY) {
        sprite.flip(flipX, flipY);
    }

    public float getRotation() {
        return sprite.getRotation();
    }

    public void setRotation(float rotation) {
        sprite.setRotation(rotation);
    }

    public void setTile(EJXTile tile) {
        this.tile = tile;
    }

    public void setRegion(TextureRegion region) {
        sprite.setSize(region.getRegionWidth(), region.getRegionHeight());
        sprite.setRegion(region);
        //        boolean flipX = (localId & EJXLayer.FLAG_FLIP_HORIZONTALLY) != 0;
        //        boolean flipY = (localId & EJXLayer.FLAG_FLIP_VERTICALLY) != 0;
        //        setFlip(flipX, flipY);
        //        setRotation(getRotation());
    }

    public static EJXTileObject create(Element objectElem, EJXTile tile,
                    EJXTiledMap map, boolean convert) {
        int objectId = objectElem.getIntAttribute(VJXString.id, 0);
        String name = objectElem.getAttribute(VJXString.name, null);
        if (name == null) {
            name = tile.getName() + VJXString.SEP_NR + objectId;
        }
        String type = objectElem.getAttribute(VJXString.type, null);
        if (type == null) {
            type = tile.getType();
        }

        float scaleX = convert ? 1.0f / map.getTileWidth() : 1.0f;
        float scaleY = convert ? 1.0f / map.getTileHeight() : 1.0f;

        float x = objectElem.getFloatAttribute(VJXString.x, 0) * scaleX;
        float y = (map.getHeight() - objectElem.getFloatAttribute(VJXString.y, 0)) * scaleY;

        float widthOnMap = objectElem.getFloatAttribute(VJXString.width, 0) * scaleX;
        float heightOnMap = objectElem.getFloatAttribute(VJXString.height, 0) * scaleY;
        float rotation = objectElem.getFloatAttribute(VJXString.rotation, 0f);
        boolean visible = objectElem.getIntAttribute(VJXString.visible, 1) == 1;

        // calculate object scale relative to original tile size
        scaleX *= widthOnMap / tile.getWidth();
        scaleY *= heightOnMap / tile.getHeight();

        EJXTileObject ejxObj = new EJXTileObject(name, type, objectId);
        ejxObj.setTile(tile);
        ejxObj.setX(x);
        ejxObj.setY(y);
        ejxObj.setScaleX(scaleX);
        ejxObj.setScaleY(scaleY);
        ejxObj.setRotation(-rotation);
        ejxObj.setVisible(visible);

        MapProperties props = ejxObj.getProperties();
        props.put(VJXString.type, type);
        props.put(VJXString.width, widthOnMap);
        props.put(VJXString.height, heightOnMap);

        String prefix = ejxObj.getName() + VJXString.SEP_DDOT;
        prefix = VJXString.STR_EMPTY;
        EJXLoaderUtil.addProps(props, tile.getProperties(), prefix);

        Element properties = objectElem.getChildByName(VJXString.TILED_properties);
        if (properties != null)
            EJXLoaderUtil.loadProperties(props, properties, prefix);
        return ejxObj;
    }
}