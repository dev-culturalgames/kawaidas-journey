package de.venjinx.ejx.tiled.object;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.tiled.EJXTiledMap;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.loader.EJXLoaderUtil;

public class EJXMapObject extends MapObject {

    private final String type;
    private final int objectId;

    private float x;
    private float y;

    private float width;
    private float height;

    public EJXMapObject(String name, String type, int objectId) {
        setName(name);
        this.type = type;
        this.objectId = objectId;
    }

    public void init() {
        VJXLogger.log("init " + getName());

    }

    public String getType() {
        return type;
    }

    public int getId() {
        return objectId;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public static EJXMapObject create(Element objectElem, EJXTiledMap map) {
        String type, name;
        MapProperties props;
        int objectId = objectElem.getIntAttribute(VJXString.id, 0);
        if (objectId == 0) {
            VJXLogger.log("This shouldnt happen. Object id = 0.");
        }
        name = objectElem.getAttribute(VJXString.name, null);
        type = objectElem.getAttribute(VJXString.type, null);
        float x = objectElem.getFloatAttribute(VJXString.x, 0f);
        float w = objectElem.getFloatAttribute(VJXString.width, 0f);
        float h = objectElem.getFloatAttribute(VJXString.height, 0f);
        float y = map.getHeight() - objectElem.getFloatAttribute(VJXString.y, 0) - h;

        if (name == null) {
            if (type != null) {
                name = type + "Object#" + objectId;
            } else name = "mapObject#" + objectId;
        }

        EJXMapObject ejxObj = new EJXMapObject(name, type, objectId);
        ejxObj.x = x;
        ejxObj.y = y;
        ejxObj.width = w;
        ejxObj.height = h;
        props = ejxObj.getProperties();

        String prefix = ejxObj.getName() + VJXString.SEP_DDOT;
        prefix = VJXString.STR_EMPTY;

        Element properties = objectElem
                        .getChildByName(VJXString.TILED_properties);
        if (properties != null)
            EJXLoaderUtil.loadProperties(props, properties, prefix);
        return ejxObj;
    }
}