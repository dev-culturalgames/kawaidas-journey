package de.venjinx.ejx.tiled.tileset;

import java.util.Iterator;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.tiled.tile.EJXAnimatedTile;
import de.venjinx.ejx.tiled.tile.EJXTile;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.loader.EJXLoaderUtil;

public class EJXTileSet extends EJXAbstractTileset {

    private String imageSource;
    private int imageHeight;
    private int imageWidth;
    private Color transColor;

    private Element imageElement;

    public EJXTileSet(Element tilesetElement) {
        super(tilesetElement);
        Element imageElement = tilesetElement.getChildByName(VJXString.TILED_image);
        if (imageElement == null)
            new GdxRuntimeException(getClass().getSimpleName()
                            + ": imageElement can not be null.");
        this.imageElement = imageElement;
    }

    public Color getTransColor() {
        return transColor;
    }

    @Override
    public void fetchResources(FileHandle file) {
        imageSource = imageElement.getAttribute(VJXString.source);
        imageWidth = imageElement.getIntAttribute(VJXString.width, 0);
        imageHeight = imageElement.getIntAttribute(VJXString.height, 0);
        FileHandle image = EJXLoaderUtil.convertRawPath(imageSource);
        addResource(getName(), new AssetDescriptor<>(image, Texture.class));
    }

    @Override
    public void init(EJXAssetManager assets) {
        Texture texture = assets.getTexture(getName());
        EJXTile tile;
        TextureRegion tr;
        Iterator<TiledMapTile> tiles = iterator();
        while (tiles.hasNext()) {
            tile = (EJXTile) tiles.next();
            tr = new TextureRegion();
            tr.setTexture(texture);
            tr.setRegion(tile.getX(), tile.getY(), tile.getWidth(), tile.getHeight());
            tile.setTextureRegion(tr);
        }

        for (EJXAnimatedTile aTile : animatedTiles) {
            tile = (EJXTile) getTile(aTile.getLocalId());
            aTile.setWidth(tile.getWidth());
            aTile.setHeight(tile.getHeight());
            putTile(aTile.getLocalId(), aTile);
        }
    }

    @Override
    protected void reset(Element tilesetElement) {
        super.reset(tilesetElement);
        String transColor = tilesetElement.getAttribute(VJXString.TILED_trans, null);
        this.transColor = (Color) EJXUtil.castProperty(tileset_bgColor,
                        transColor, VJXString.VAR_color);
    }

    @Override
    protected void createTiles(Array<Element> tiles) {
        int id = 0;
        int margin = getMargin();
        int spacing = getSpacing();
        int tileW = getTileWidth();
        int tileH = getTileWidth();
        int offX = getTileOffsetX();
        int offY = getTileOffsetY();

        int stopWidth = imageWidth - tileW;
        int stopHeight = imageHeight - tileH;

        EJXTile tile;
        for (int y = margin; y <= stopHeight; y += tileH + spacing) {
            for (int x = margin; x <= stopWidth; x += tileW + spacing) {
                tile = new EJXTile(imageElement.getParent(), id++);
                tile.setX(x);
                tile.setY(y);
                tile.setWidth(tileW);
                tile.setHeight(tileH);
                tile.setOffsetX(offX);
                tile.setOffsetY(-offY);
                //                VJXLogger.log("put tile " + tile);
                putTile(tile.getLocalId(), tile);
            }
        }
    }
}