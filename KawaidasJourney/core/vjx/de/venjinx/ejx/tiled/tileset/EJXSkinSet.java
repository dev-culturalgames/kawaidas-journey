//package de.venjinx.ejx.tiled.tileset;
//
//import java.util.Iterator;
//
//import com.badlogic.gdx.assets.AssetDescriptor;
//import com.badlogic.gdx.files.FileHandle;
//import com.badlogic.gdx.graphics.Texture;
//import com.badlogic.gdx.graphics.g2d.TextureRegion;
//import com.badlogic.gdx.maps.tiled.TiledMapTile;
//import com.badlogic.gdx.scenes.scene2d.ui.Skin;
//import com.badlogic.gdx.utils.Array;
//import com.badlogic.gdx.utils.XmlReader.Element;
//
//import de.venjinx.ejx.EJXAssetManager;
//import de.venjinx.ejx.tiled.tile.EJXAnimatedTile;
//import de.venjinx.ejx.tiled.tile.EJXTile;
//import de.venjinx.ejx.util.EJXTypes.VJXString;
//import de.venjinx.ejx.util.VJXLogger;
//import de.venjinx.ejx.util.loader.EJXLoaderUtil;
//
//public class EJXSkinSet extends EJXAbstractTileset {
//
//    private FileHandle skinFile;
//
//    public EJXSkinSet(Element tilesetElement, int firstId) {
//        super(tilesetElement, firstId);
//    }
//
//    @Override
//    public void fetchResources(FileHandle tilesetFile) {
//        skinFile = getProperties().get("skin", FileHandle.class);
//        if (skinFile != null) {
//            skinFile = EJXLoaderUtil.solveInternalPath(
//                            tilesetFile.parent(), skinFile.name());
//            VJXLogger.log("load skin " + getName() + ": " + skinFile);
//            addResource(getName(), new AssetDescriptor<>(skinFile, Skin.class));
//        }
//    }
//
//    @Override
//    public void prepare(Array<Element> tiles) {
//        prepared = false;
//        createTiles(tiles);
//    }
//
//    @Override
//    public void init(EJXAssetManager assets) {
//        if (prepared) return;
//        Skin skin = null;
//        Texture texture;
//        EJXTile tile;
//        TextureRegion tr;
//        Iterator<TiledMapTile> tiles = iterator();
//        Array<EJXAnimatedTile> animatedTiles = new Array<>();
//
//        if (skinFile != null) {
//            skin = assets.get(getName(), Skin.class);
//        }
//
//        while (tiles.hasNext()) {
//            tile = (EJXTile) tiles.next();
//
//            if (skin == null) {
//                texture = assets.getTexture(tile.getTextureName());
//                VJXLogger.log("get tex: " + tile.getTextureName());
//                tr = new TextureRegion(texture);
//                tr.setRegion(tile.getX(), tile.getY(), tile.getWidth(),
//                                tile.getHeight());
//                tile.setTextureRegion(tr);
//            } else {
//                if (skin != null) {
//                    VJXLogger.log("get tex: " + tile.getTextureName() + " - ");
//                    String name = tile.getTextureName().split(getName())[1];
//                    tr = skin.getRegion(name);
//                    tile.setTextureRegion(tr);
//                    VJXLogger.log("find region " + name + ": " + tr);
//                }
//            }
//        }
//
//        for (EJXAnimatedTile aTile : animatedTiles) {
//            putTile(aTile.getId(), aTile);
//        }
//        prepared = true;
//    }
//
//    public void createTiles(Array<Element> tiles) {
//        // TODO remove old loading after kawaida
//        boolean old = getProperties().get("old", false, Boolean.class);
//
//        EJXTile tile;
//        int offX = getTileOffsetX();
//        int offY = getTileOffsetY();
//        boolean flipY = true;
//        String imgSrc;
//        Element imageElement;
//        FileHandle image;
//        Array<EJXAnimatedTile> animatedTiles = new Array<>();
//        for (Element tileElement : tiles) {
//
//            imageElement = tileElement.getChildByName(VJXString.TILED_image);
//            imgSrc = imageElement.getAttribute(VJXString.source);
//            VJXLogger.log("imgSrc " + imgSrc);
//            image = EJXLoaderUtil.convertRawPath(imgSrc);
//
//            if (skinFile == null && !old) {
//                VJXLogger.log("add res " + image.pathWithoutExtension());
//                addResource(image.pathWithoutExtension(),
//                                new AssetDescriptor<>(image, Texture.class));
//            }
//
//            int id = tileElement.getIntAttribute(VJXString.id, 0);
//            tile = new EJXTile(tileElement, id, getFirstId());
//            tile.setOffsetX(offX);
//            tile.setOffsetY(flipY ? -offY : offY);
//            putTile(tile.getId(), tile);
//            loadTileStuff(tile, animatedTiles);
//        }
//    }
//
//}