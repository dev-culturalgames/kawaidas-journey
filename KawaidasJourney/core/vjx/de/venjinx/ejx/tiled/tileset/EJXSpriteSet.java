package de.venjinx.ejx.tiled.tileset;

import java.util.Iterator;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.tiled.tile.EJXAnimatedTile;
import de.venjinx.ejx.tiled.tile.EJXTile;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.loader.EJXLoaderUtil;

public class EJXSpriteSet extends EJXAbstractTileset {

    private FileHandle atlasFile;

    public EJXSpriteSet(Element tilesetElement) {
        super(tilesetElement);
    }

    @Override
    public void fetchResources(FileHandle tilesetFile) {
        atlasFile = getProperties().get("atlas", FileHandle.class);
        if (atlasFile != null) {
            addResource(getName(), new AssetDescriptor<>(atlasFile,
                            TextureAtlas.class));
        }
    }

    @Override
    public void init(EJXAssetManager assets) {
        TextureAtlas atlas = null;
        Texture texture;
        EJXTile tile;
        TextureRegion tr;
        Iterator<TiledMapTile> tiles = iterator();

        if (atlasFile != null) {
            atlas = assets.get(getName(), TextureAtlas.class);
        }

        while (tiles.hasNext()) {
            tile = (EJXTile) tiles.next();

            if (atlas == null) {
                texture = assets.getTexture(tile.getTextureName());
                tr = new TextureRegion(texture);
                tr.setRegion(tile.getX(), tile.getY(), tile.getWidth(),
                                tile.getHeight());
                tile.setTextureRegion(tr);
            } else {
                if (atlas != null) {
                    String name = tile.getTextureName();
                    String[] split = name.split(atlasFile.nameWithoutExtension()
                                    + VJXString.SEP_SLASH);
                    name = split[1];
                    tr = atlas.findRegion(name);
                    tile.setTextureRegion(tr);
                }
            }
        }

        for (EJXAnimatedTile aTile : animatedTiles) {
            putTile(aTile.getLocalId(), aTile);
        }
    }

    @Override
    protected void createTiles(Array<Element> tiles) {
        // TODO remove old loading after kawaida
        boolean old = getProperties().get("old", false, Boolean.class);

        EJXTile tile;
        int offX = getTileOffsetX();
        int offY = getTileOffsetY();
        boolean flipY = true;
        String imgSrc;
        Element imageElement;
        FileHandle image;
        for (Element tileElement : tiles) {

            imageElement = tileElement.getChildByName(VJXString.TILED_image);
            imgSrc = imageElement.getAttribute(VJXString.source);
            image = EJXLoaderUtil.convertRawPath(imgSrc);

            if (atlasFile == null && !old) {
                addResource(image.pathWithoutExtension(),
                                new AssetDescriptor<>(image, Texture.class));
            }

            int id = tileElement.getIntAttribute(VJXString.id, 0);
            tile = new EJXTile(tileElement, id);
            tile.setOffsetX(offX);
            tile.setOffsetY(flipY ? -offY : offY);
            //            VJXLogger.log("put tile " + tile.getId() + ": " + tile);
            putTile(tile.getLocalId(), tile);
        }
    }
}