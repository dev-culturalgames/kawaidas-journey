package de.venjinx.ejx.tiled.tileset;

import java.util.Iterator;

import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.tiled.tile.EJXTile;

public class EJXTilesets implements Iterable<EJXAbstractTileset> {

    private Array<EJXAbstractTileset> tilesets;

    /** Creates an empty collection of tilesets. */
    public EJXTilesets() {
        tilesets = new Array<>();
    }

    public void addTileSet(EJXAbstractTileset tileset) {
        tilesets.add(tileset);
    }

    public EJXAbstractTileset getSet(EJXTile tile) {
        EJXTile tmpTile;
        for (EJXAbstractTileset set : tilesets) {
            tmpTile = (EJXTile) set.getTile(tile.getLocalId());
            if (tmpTile != null) {
                if (tmpTile == tile)
                    return set;
            }
        }
        return null;
    }

    /** @param id id of the {@link TiledMapTile} to get.
     * @return tile with matching id, null if it doesn't exist */
    public EJXTile getTile(int id) {
        // The purpose of backward iteration here is to maintain backwards compatibility
        // with maps created with earlier versions of a shared tileset.  The assumption
        // is that the tilesets are in order of ascending firstgid, and by backward
        // iterating precedence for conflicts is given to later tilesets in the list,
        // which are likely to be the earlier version of any given gid.
        // See TiledMapModifiedExternalTilesetTest for example of this issue.
        for (int i = tilesets.size - 1; i >= 0; i--) {
            EJXAbstractTileset tileset = tilesets.get(i);
            EJXTile tile = (EJXTile) tileset.getTile(id);
            if (tile != null) { return tile; }
        }
        return null;
    }

    /** @return iterator to tilesets */
    @Override
    public Iterator<EJXAbstractTileset> iterator() {
        return tilesets.iterator();
    }
}
