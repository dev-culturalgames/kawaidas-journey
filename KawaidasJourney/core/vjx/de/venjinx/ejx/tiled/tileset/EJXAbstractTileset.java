package de.venjinx.ejx.tiled.tileset;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.tiled.tile.EJXAnimatedTile;
import de.venjinx.ejx.tiled.tile.EJXTile;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.loader.EJXLoaderUtil;

public abstract class EJXAbstractTileset extends TiledMapTileSet {

    public static final String EJXTileSet = "EJXTileSet";
    public static final String tileset_bgColor = "set:bgColor";

    private String tiledVersion;
    private String version;

    private int margin;
    private String orientation;
    private int spacing;
    private int columns;
    private Color bgColor;

    private int tileCount;
    private int tileWidth;
    private int tileHeight;
    private int tileOffsetX;
    private int tileOffsetY;

    private ObjectMap<String, AssetDescriptor<? extends Disposable>> resources;

    protected boolean prepared;
    protected boolean ready;
    protected Array<EJXAnimatedTile> animatedTiles;

    public EJXAbstractTileset(Element tilesetElement) {
        super();
        String name = tilesetElement.getAttribute(VJXString.name, EJXTileSet);
        setName(name);
        animatedTiles = new Array<>();
        resources = new ObjectMap<>();
        reset(tilesetElement);
    }

    public void prepare(Array<Element> tiles) {
        if (prepared) return;
        prepared = false;

        createTiles(tiles);

        EJXTile tile;
        for (Element tileElement : tiles) {
            int localTID = tileElement.getIntAttribute(VJXString.id, 0);
            tile = (EJXTile) getTile(localTID);
            tile.setTileElement(tileElement);
            loadTileStuff(tile);
        }
        prepared = true;
    }

    public void updateTextures(EJXAssetManager assets) {
        if (ready) return;
        ready = false;

        init(assets);

        ready = true;
    }

    public abstract void fetchResources(FileHandle file);

    protected abstract void createTiles(Array<Element> tiles);

    protected abstract void init(EJXAssetManager assets);

    public void loadProperties() {
    }

    public void addResource(String name, AssetDescriptor<? extends Disposable> resource) {
        resources.put(name, resource);
    }

    public ObjectMap<String, AssetDescriptor<? extends Disposable>> getResources() {
        return resources;
    }

    public String getTiledVersion() {
        return tiledVersion;
    }

    public String getVersion() {
        return version;
    }

    public int getMargin() {
        return margin;
    }

    public int getSpacing() {
        return spacing;
    }

    public int getColumns() {
        return columns;
    }

    public String getOrientation() {
        return orientation;
    }

    public Color getBgColor() {
        return bgColor;
    }

    public int getTileCount() {
        return tileCount;
    }

    public int getTileWidth() {
        return tileWidth;
    }

    public int getTileHeight() {
        return tileHeight;
    }

    public int getTileOffsetX() {
        return tileOffsetX;
    }

    public int getTileOffsetY() {
        return tileOffsetY;
    }

    protected void reset(Element tilesetElement) {
        // tiled default
        String backgroundColor = tilesetElement.getAttribute(VJXString.TILED_backgroundcolor, null);
        bgColor = (Color) EJXUtil.castProperty(tileset_bgColor, backgroundColor, VJXString.VAR_color);

        tiledVersion = tilesetElement.getAttribute(VJXString.TILED_tiledversion, null);
        tileWidth = tilesetElement.getIntAttribute(VJXString.TILED_tilewidth, 0);
        tileHeight = tilesetElement.getIntAttribute(VJXString.TILED_tileheight, 0);
        version = tilesetElement.getAttribute(VJXString.TILED_version, null);

        // tileset specific
        margin = tilesetElement.getIntAttribute(VJXString.TILED_margin, 0);
        spacing = tilesetElement.getIntAttribute(VJXString.TILED_spacing, 0);
        columns = tilesetElement.getIntAttribute(VJXString.TILED_columns, 0);
        tileCount = tilesetElement.getIntAttribute(VJXString.TILED_tilecount, 0);
        tileOffsetX = 0;
        tileOffsetY = 0;
        Element offset = tilesetElement.getChildByName(VJXString.TILED_tileoffset);
        if (offset != null) {
            tileOffsetX = offset.getIntAttribute(VJXString.x, 0);
            tileOffsetY = offset.getIntAttribute(VJXString.y, 0);
        }

        getProperties().clear();

        Element propertiesElement = tilesetElement
                        .getChildByName(VJXString.TILED_properties);
        EJXLoaderUtil.loadProperties(getProperties(), propertiesElement);
    }

    protected void loadTileStuff(EJXTile tile) {
        Element tileElement = tile.getTileElement();
        if (tile != null) {
            Element properties = tileElement
                            .getChildByName(VJXString.TILED_properties);
            if (properties != null) {
                EJXLoaderUtil.loadProperties(tile.getProperties(), properties);
            }

            Element animationElement = tileElement
                            .getChildByName(VJXString.animation);
            if (animationElement != null) {
                Array<EJXTile> staticTiles = new Array<>();
                IntArray intervals = new IntArray();
                int id;
                for (Element frameElement : animationElement
                                .getChildrenByName(VJXString.TILED_frame)) {
                    id = frameElement.getIntAttribute(EJXTile.tile_id);
                    EJXTile t = (EJXTile) getTile(id);
                    staticTiles.add(t);
                    intervals.add(frameElement
                                    .getIntAttribute(VJXString.duration));
                }

                EJXAnimatedTile aTile = new EJXAnimatedTile(tileElement,
                                tile.getLocalId(), staticTiles, intervals);
                animatedTiles.add(aTile);
                tile = aTile;
            }

            String terrain = tileElement.getAttribute(VJXString.TILED_terrain,
                            null);
            if (terrain != null) {
                tile.getProperties().put(VJXString.TILED_terrain, terrain);
            }

            String probability = tileElement
                            .getAttribute(VJXString.TILED_probability, null);
            if (probability != null) {
                tile.getProperties().put(VJXString.TILED_probability,
                                probability);
            }
        }
    }
}