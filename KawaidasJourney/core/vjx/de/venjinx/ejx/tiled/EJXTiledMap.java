package de.venjinx.ejx.tiled;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.tiled.layer.EJXLayer;
import de.venjinx.ejx.tiled.tile.EJXTile;
import de.venjinx.ejx.tiled.tileset.EJXAbstractTileset;
import de.venjinx.ejx.tiled.tileset.EJXTilesets;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.loader.EJXLoaderUtil;

public class EJXTiledMap extends Map {

    public static final String EJXTiledMap = "EJXTiledMap";

    private String name;
    private Color bgColor;
    private boolean initialized;

    private Element mapElement;
    private EJXTilesets tilesets;
    private ObjectMap<String, Integer> firstIds;
    private ObjectMap<String, AssetDescriptor<? extends Disposable>> resources;

    public EJXTiledMap() {
        this(EJXTiledMap);
    }

    public EJXTiledMap(String name) {
        super();
        this.name = name;
        tilesets = new EJXTilesets();
        firstIds = new ObjectMap<>();
        resources = new ObjectMap<>();
        initialized = false;
    }

    public void loadProperties(MapProperties mapProperties) {
    }

    public void init(EJXAssetManager assets) {
        if (initialized) return;
        //                VJXLogger.logIncr(LogCategory.INIT, "Initialize map " + getName());
        VJXLogger.logIncr("Initialize map " + getName());
        EJXAbstractTileset tileset;
        for (TiledMapTileSet tiledTileset : tilesets) {
            tileset = (EJXAbstractTileset) tiledTileset;
            tileset.updateTextures(assets);
        }

        for (MapLayer layer : getLayers()) {
            if (layer instanceof EJXLayer) {
                ((EJXLayer) layer).init(this, assets);
            }
        }
        initialized = true;
        VJXLogger.logDecr("Initialized map " + getName());
        VJXLogger.log("");
        //        VJXLogger.logDecr(LogCategory.INIT, "Initialized map " + getName());
    }

    public void setMapElement(Element mapElem) {
        mapElement = mapElem;
        String backgroundColor = mapElement
                        .getAttribute(VJXString.TILED_backgroundcolor, null);
        bgColor = (Color) EJXUtil.castProperty(VJXString.TILED_backgroundcolor,
                        backgroundColor, VJXString.VAR_color);

        getProperties().clear();
        EJXLoaderUtil.loadProperties(getProperties(),
                        mapElement.getChildByName(VJXString.TILED_properties));
    }

    public EJXTilesets getTilesets() {
        return tilesets;
    }

    public void addTileset(EJXAbstractTileset tileset, int firstId) {
        firstIds.put(tileset.getName(), firstId);
        tilesets.addTileSet(tileset);
    }

    public TiledMapTile getTile(int globalId) {
        // 0 => empty, -x => undefined
        if (globalId <= 0) return null;

        int firstId = 1;
        int localId = 0;
        TiledMapTile tile;
        //        VJXLogger.log("get tile " + globalId, 1);

        for (TiledMapTileSet tileset : tilesets) {
            firstId = firstIds.get(tileset.getName());

            // check if tile id is in the tilesets id range
            if (globalId < firstId)
                continue;

            localId = globalId - firstId;
            //            if (localId > tileset.size()) {
            //                continue;
            //            }
            //            VJXLogger.log("get tile from set " + tileset.getName() + "-"
            //                            + tileset.size() + " fid " + firstId + ", localId "
            //                            + localId);

            tile = tileset.getTile(localId);
            //            VJXLogger.log("");
            if (tile != null) return tile;
        }
        return null;
    }

    public int getIdOffset(EJXTile tile) {
        if (tile == null) return -1;

        TiledMapTile t = getTile(tile.getId());
        if (t == null) return -1;
        //        if (t.get)

        //        // 0 => empty, -x => undefined
        //        if (globalId <= 0) return globalId;

        //        int firstId = 1;
        for (TiledMapTileSet tileset : tilesets) {

            //            if (tileset.get) firstId = firstIds.get(tileset.getName());
            //
            //            // check if tile id is in the tilesets id range
            //            if (globalId < firstId || globalId > firstId + tileset.size())
            //                continue;
            //            return firstId;
        }
        return -1;
    }

    public void addResource(String name, AssetDescriptor<? extends Disposable> resource) {
        resources.put(name, resource);
    }

    public ObjectMap<String, AssetDescriptor<? extends Disposable>> getResources() {
        return resources;
    }

    public String getName() {
        return name;
    }

    public void setBgColor(Color bgColor) {
        this.bgColor = bgColor;
    }

    public Color getBgColor() {
        return bgColor;
    }

    public int getWidth() {
        return getTilesX() * getTileWidth();
    }

    public int getHeight() {
        return getTilesY() * getTileHeight();
    }

    public int getTilesX() {
        return mapElement.getIntAttribute(VJXString.width, 0);
    }

    public int getTilesY() {
        return mapElement.getIntAttribute(VJXString.height, 0);
    }

    public int getTileWidth() {
        return mapElement.getIntAttribute(VJXString.TILED_tilewidth, 0);
    }

    public int getTileHeight() {
        return mapElement.getIntAttribute(VJXString.TILED_tileheight, 0);
    }

    public String getTiledVersion() {
        return mapElement.getAttribute(VJXString.TILED_tiledversion, null);
    }

    public String getVersion() {
        return mapElement.getAttribute(VJXString.TILED_version, null);
    }

    public int getInfinite() {
        return mapElement.getIntAttribute(VJXString.TILED_infinite, 0);
    }

    public int getHexSideLength() {
        return mapElement.getIntAttribute(VJXString.TILED_hexsidelength, 0);
    }

    public String getOrientation() {
        return mapElement.getAttribute(VJXString.TILED_orientation, null);
    }

    public String getRenderOrder() {
        return mapElement.getAttribute(VJXString.TILED_renderorder, null);
    }

    public String getStaggerAxis() {
        return mapElement.getAttribute(VJXString.TILED_staggeraxis, null);
    }

    public String getStaggerIndex() {
        return mapElement.getAttribute(VJXString.TILED_staggerindex, null);
    }

    public int getNextLayerId() {
        return mapElement.getIntAttribute(VJXString.TILED_nextlayerid, 0);
    }

    public int getNextObjectId() {
        return mapElement.getIntAttribute(VJXString.TILED_nextobjectid, 0);
    }
}