package de.venjinx.ejx.tiled.layer;

import com.badlogic.gdx.graphics.g2d.Batch;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.tiled.EJXTiledMap;

public interface EJXLayer {

    public static final String EJXLayer = "EJXLayer";

    public static final int FLAG_FLIP_HORIZONTALLY = 0x80000000;
    public static final int FLAG_FLIP_VERTICALLY = 0x40000000;
    public static final int FLAG_FLIP_DIAGONALLY = 0x20000000;
    public static final int MASK_CLEAR = 0xE0000000;

    public abstract void init(EJXTiledMap map, EJXAssetManager assets);

    public abstract void draw(Batch batch);
}