package de.venjinx.ejx.tiled.layer;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.tiled.EJXTiledMap;

public class EJXGroupLayer extends MapLayer implements EJXLayer {

    private static final String EJXGroupLayer = "EJXGroupLayer";

    private MapLayers layers = new MapLayers();

    public EJXGroupLayer() {
        this(EJXGroupLayer);
    }

    public EJXGroupLayer(String name) {
        setName(name);
    }

    @Override
    public void init(EJXTiledMap map, EJXAssetManager assets) {
        EJXLayer ejxLayer;
        for (MapLayer mapLayer : layers) {
            if (!(mapLayer instanceof EJXLayer)) continue;
            ejxLayer = (EJXLayer) mapLayer;
            ejxLayer.init(map, assets);
        }
    }

    @Override
    public void draw(Batch batch) {
        EJXLayer ejxLayer;
        for (MapLayer mapLayer : layers) {
            if (!(mapLayer instanceof EJXLayer)) continue;
            ejxLayer = (EJXLayer) mapLayer;
            ejxLayer.draw(batch);
        }
    }

    public MapLayers getLayers() {
        return layers;
    }
}