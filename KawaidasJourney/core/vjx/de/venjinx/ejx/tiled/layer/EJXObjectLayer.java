package de.venjinx.ejx.tiled.layer;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.tiled.EJXTiledMap;
import de.venjinx.ejx.tiled.object.EJXMapObject;
import de.venjinx.ejx.tiled.object.EJXTileObject;

public class EJXObjectLayer extends MapLayer implements EJXLayer {

    private static final String EJXObjectLayer = "EJXObjectLayer";

    private Color color;

    public EJXObjectLayer() {
        this(EJXObjectLayer);
    }

    public EJXObjectLayer(String name) {
        setName(name);
        color = new Color();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color.set(color);
    }

    @Override
    public void init(EJXTiledMap map, EJXAssetManager assets) {
        EJXTileObject object;
        for (MapObject mo : getObjects()) {
            if (mo instanceof EJXTileObject) {
                object = (EJXTileObject) mo;
                object.init();
            }

            if (mo instanceof EJXMapObject) {
            }
        }
    }

    @Override
    public void draw(Batch batch) {
        for (MapObject mo : getObjects()) {
            if (mo instanceof EJXTileObject) {
                EJXTileObject tmo = (EJXTileObject) mo;
                tmo.draw(batch);
            }
        }
    }
}