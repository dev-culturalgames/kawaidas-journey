package de.venjinx.ejx.tiled.layer;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.BaseTmxMapLoader;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.scenegraph.WorldLayer.LayerCell;
import de.venjinx.ejx.tiled.EJXTiledMap;
import de.venjinx.ejx.util.loader.EJXLoaderUtil;

public class EJXTileLayer extends TiledMapTileLayer implements EJXLayer {

    private static final String EJXImageLayer = "EJXImageLayer";

    private Element data;

    public EJXTileLayer(String name, EJXTiledMap map, Element data) {
        this(name, map.getTilesX(), map.getTilesY(), map.getTileWidth(), map.getTileHeight());
        setData(data);
        create(map);
    }

    public EJXTileLayer(int width, int height, int tileWidth, int tileHeight) {
        this(EJXImageLayer, width, height, tileWidth, tileHeight);
    }

    public EJXTileLayer(String name, int width, int height, int tileWidth,
                    int tileHeight) {
        super(width, height, tileWidth, tileHeight);
        setName(name);
    }

    public void setData(Element data) {
        this.data = data;
        Element properties = data.getChildByName("properties");
        if (properties != null)
            EJXLoaderUtil.loadProperties(getProperties(), properties);
    }

    @Override
    public void draw(Batch batch) {
        if (!isVisible()) return;
    }

    @Override
    public void init(EJXTiledMap map, EJXAssetManager assets) {
    }

    private void create(EJXTiledMap map) {
        if (data == null) return;
        int width = getWidth();
        int height = getHeight();
        int[] ids = BaseTmxMapLoader.getTileIds(data, width, height);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int id = ids[y * width + x];
                TiledMapTile tile = map.getTile(id & ~MASK_CLEAR);
                if (tile != null) {
                    LayerCell cell = createTileLayerCell(x, height - 1 - y, id,
                                    tile.getProperties().get("tileCase", 0,
                                                    Integer.class));
                    cell.setTile(tile);
                    setCell(x, height - 1 - y, cell);
                }
            }
        }
    }

    private LayerCell createTileLayerCell(int x, int y, int id, int tileCase) {
        boolean flipHorizontally = (id & FLAG_FLIP_HORIZONTALLY) != 0;
        boolean flipVertically = (id & FLAG_FLIP_VERTICALLY) != 0;
        boolean flipDiagonally = (id & FLAG_FLIP_DIAGONALLY) != 0;
        LayerCell cell = new LayerCell(x, y, tileCase);
        if (flipDiagonally) {
            if (flipHorizontally && flipVertically) {
                cell.setFlipHorizontally(true);
                cell.setRotation(Cell.ROTATE_270);
            } else if (flipHorizontally) cell.setRotation(Cell.ROTATE_270);
            else if (flipVertically) cell.setRotation(Cell.ROTATE_90);
            else {
                cell.setFlipVertically(true);
                cell.setRotation(Cell.ROTATE_270);
            }
        } else {
            cell.setFlipHorizontally(flipHorizontally);
            cell.setFlipVertically(flipVertically);
        }
        return cell;
    }
}