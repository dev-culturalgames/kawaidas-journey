package de.venjinx.ejx.tiled.layer;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapLayer;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.tiled.EJXTiledMap;

public class EJXImageLayer extends MapLayer implements EJXLayer {

    private static final String EJXImageLayer = "EJXImageLayer";

    private Texture texture;
    private Color color;

    public EJXImageLayer() {
        this(EJXImageLayer);
    }

    public EJXImageLayer(String name) {
        setName(name);
        color = new Color();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color.set(color);
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    @Override
    public void init(EJXTiledMap map, EJXAssetManager assets) {
        setTexture(assets.getTexture(getName()));
    }

    @Override
    public void draw(Batch batch) {
        if (!isVisible() || texture == null) return;
        batch.draw(texture, getRenderOffsetX(), getRenderOffsetY());
    }
}