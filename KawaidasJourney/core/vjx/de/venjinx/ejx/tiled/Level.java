package de.venjinx.ejx.tiled;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.entity.component.ActorComponent;
import de.venjinx.ejx.entity.component.CommandComponent;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.scenegraph.EJXScenegraph;
import de.venjinx.ejx.stages.EJXStage;
import de.venjinx.ejx.tiled.entity.EJXObject;
import de.venjinx.ejx.tiled.layer.EJXGroupLayer;
import de.venjinx.ejx.tiled.object.EJXMapObject;

public class Level extends EJXStage {

    public static final String LevelTable = "LevelStage";

    private GameControl gameControl;
    private Array<EJXTiledMap> maps;
    private EJXTiledMap map;
    private EJXScenegraph scenegraph;

    public Level(EJXGame game) {
        super(game);
        setName(LevelTable);
        gameControl = game.gameControl;
        setRunBackground(true);
    }

    public void load(String levelName) {
        TiledFactory factory = gameControl.getGame().tFactory;
        setName(levelName);
        map = factory.getMap(levelName);

        createEntities(map.getLayers());
        init();
    }

    private void createEntities(MapLayers layers) {
        for (MapLayer mapLayer : layers) {
            createEntities(mapLayer);
        }
    }

    private void createEntities(MapLayer layer) {
        if (layer instanceof EJXGroupLayer) {
            createEntities(((EJXGroupLayer) layer).getLayers());
            return;
        }
        TiledFactory factory = gameControl.getGame().tFactory;
        EJXObject entity;
        EJXMapObject object;
        MapObjects objects = layer.getObjects();
        for (MapObject mapObject : objects) {
            object = (EJXMapObject) mapObject;
            if (object.getType() != null) {
                entity = factory.createObject(object);
                ActorComponent objCmp = (ActorComponent) entity
                                .getComponent(VJXLogicType.ACTOR);
                addActor(objCmp.getActor());
                CommandComponent cmdCmp = (CommandComponent) entity
                                .getComponent(VJXLogicType.COMMAND);
                addActor(cmdCmp.getActor());
            }
        }
    }

    public void unload() {
        setName(LevelTable);
        map = null;
    }

    public EJXTiledMap getMap() {
        return map;
    }

    @Override
    protected void userInit() {
    }

    @Override
    protected void preAct(float delta) {
    }

    @Override
    protected void postAct(float delta) {
    }

    @Override
    protected void preDraw() {
    }

    @Override
    protected void postDraw() {
    }
}