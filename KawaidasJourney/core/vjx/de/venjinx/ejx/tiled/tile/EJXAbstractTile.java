package de.venjinx.ejx.tiled.tile;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.loader.EJXLoaderUtil;

public abstract class EJXAbstractTile implements TiledMapTile {

    protected String type;
    protected String name;
    protected String attribute;
    protected int tilePart = 0;

    private int localId = 0;

    private int x = 0;
    private int y = 0;
    private int width = 0;
    private int height = 0;
    private float offsetX = 0;
    private float offsetY = 0;

    private String source;
    private String srcDir;
    private String textureName;
    private BlendMode blendMode = BlendMode.ALPHA;
    private TextureRegion textureRegion;

    private MapProperties properties;
    private MapObjects objects;

    private Element tileElement;
    private Element imageElement;

    public EJXAbstractTile(Element tileElement, int localId) {
        this.localId = localId;
        properties = new MapProperties();
        objects = new MapObjects();

        this.tileElement = tileElement;
        imageElement = tileElement.getChildByName(VJXString.TILED_image);
        if (imageElement != null) {
            source = imageElement.getAttribute(VJXString.source);
            String[] sourceSplit = EJXLoaderUtil.getSourceSplit(source);

            srcDir = sourceSplit[0];
            name = sourceSplit[1];
            attribute = sourceSplit[2];
            tilePart = Integer.parseInt(sourceSplit[3]);

            width = imageElement.getIntAttribute(VJXString.width, 0);
            height = imageElement.getIntAttribute(VJXString.height, 0);

            if (tileElement.getName().equals(VJXString.tileset)) {
                textureName = tileElement.get(VJXString.name, null);
            } else textureName = srcDir + name;
        }
    }

    public int getLocalId() {
        return localId;
    }

    public String getType() {
        return type;
    }

    public String getTextureName() {
        return textureName;
    }

    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Element getTileElement() {
        return tileElement;
    }

    public void setTileElement(Element tileElement) {
        this.tileElement = tileElement;
    }

    public Element getImageElement() {
        return imageElement;
    }

    @Override
    public int getId() {
        return localId + 1;
    }

    @Override
    public void setId(int globalId) {
        localId = globalId - 1;
    }

    @Override
    public float getOffsetX() {
        return offsetX;
    }

    @Override
    public void setOffsetX(float offsetX) {
        this.offsetX = offsetX;
    }

    @Override
    public float getOffsetY() {
        return offsetY;
    }

    @Override
    public void setOffsetY(float offsetY) {
        this.offsetY = offsetY;
    }

    @Override
    public BlendMode getBlendMode() {
        return blendMode;
    }

    @Override
    public void setBlendMode(BlendMode blendMode) {
        this.blendMode = blendMode;
    }

    @Override
    public TextureRegion getTextureRegion() {
        return textureRegion;
    }

    @Override
    public void setTextureRegion(TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
    }

    @Override
    public MapProperties getProperties() {
        return properties;
    }

    @Override
    public MapObjects getObjects() {
        return objects;
    }

    @Override
    public String toString() {
        String s = "Tile " + name;
        s += "\n    type: " + type;
        s += "\n    attribute: " + attribute;
        s += "\n    localId: " + localId;
        s += "\n    width: " + width;
        s += "\n    height: " + height;
        s += "\n    offsetX: " + offsetX;
        s += "\n    offsetY: " + offsetY;
        s += "\n    tilePart: " + tilePart;
        s += "\n    source: " + source;
        s += "\n    srcDir: " + srcDir;
        s += "\n    blendMode: " + getBlendMode();
        return "\n" + s;
    }
}