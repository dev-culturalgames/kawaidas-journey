package de.venjinx.ejx.tiled.tile;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.XmlReader.Element;

public class EJXAnimatedTile extends EJXTile {

    private static long lastTiledMapRenderTime = 0;
    private static final long initialTimeOffset = TimeUtils.millis();

    private EJXTile[] frameTiles;

    private int[] animationIntervals;
    private int loopDuration;

    public EJXAnimatedTile(Element tileElement, int localId,
                    Array<EJXTile> frameTiles, IntArray intervals) {
        super(tileElement, localId);
        this.frameTiles = new EJXTile[frameTiles.size];

        animationIntervals = intervals.toArray();
        loopDuration = 0;
        for (int i = 0; i < intervals.size; ++i) {
            this.frameTiles[i] = frameTiles.get(i);
            loopDuration += intervals.get(i);
        }
    }

    public int getCurrentFrameIndex() {
        int currentTime = (int) (lastTiledMapRenderTime % loopDuration);

        for (int i = 0; i < animationIntervals.length; ++i) {
            int animationInterval = animationIntervals[i];
            if (currentTime <= animationInterval) return i;
            currentTime -= animationInterval;
        }

        throw new GdxRuntimeException(
                        "Could not determine current animation frame in AnimatedTiledMapTile.  This should never happen.");
    }

    public TiledMapTile getCurrentFrame() {
        return frameTiles[getCurrentFrameIndex()];
    }

    public void setAnimationIntervals(int[] intervals) {
        if (intervals.length == animationIntervals.length) {
            animationIntervals = intervals;

            loopDuration = 0;
            for (int i = 0; i < intervals.length; i++) {
                loopDuration += intervals[i];
            }

        } else {
            throw new GdxRuntimeException("Cannot set " + intervals.length
                            + " frame intervals. The given int[] must have a size of "
                            + animationIntervals.length + ".");
        }
    }

    public EJXAbstractTile[] getFrameTiles() {
        return frameTiles;
    }

    public int getFrameCount() {
        return frameTiles.length;
    }

    public int getLoopDuration() {
        return loopDuration;
    }

    @Override
    public TextureRegion getTextureRegion() {
        return getCurrentFrame().getTextureRegion();
    }

    @Override
    public void setTextureRegion (TextureRegion textureRegion) {
        throw new GdxRuntimeException("Cannot set the texture region of AnimatedTiledMapTile.");
    }

    @Override
    public float getOffsetX () {
        return getCurrentFrame().getOffsetX();
    }

    @Override
    public void setOffsetX (float offsetX) {
        throw new GdxRuntimeException("Cannot set offset of AnimatedTiledMapTile.");
    }

    @Override
    public float getOffsetY () {
        return getCurrentFrame().getOffsetY();
    }

    @Override
    public void setOffsetY (float offsetY) {
        throw new GdxRuntimeException("Cannot set offset of AnimatedTiledMapTile.");
    }

    @Override
    public String toString() {
        String s = super.toString();
        s += ": frameCount: " + frameTiles.length;
        s += ", loopDuration: " + loopDuration;
        return s;
    }

    public static void updateAnimationBaseTime() {
        lastTiledMapRenderTime = TimeUtils.millis() - initialTimeOffset;
    }
}