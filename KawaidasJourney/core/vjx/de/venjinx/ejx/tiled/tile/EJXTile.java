package de.venjinx.ejx.tiled.tile;

import com.badlogic.gdx.utils.XmlReader.Element;

public class EJXTile extends EJXAbstractTile {

    public static final String tile = "tile";
    public static final String tile_id = "tileid";

    public EJXTile(Element tileElement, int localId) {
        super(tileElement, localId);
    }

    @Override
    public String toString() {
        return getName() + " (local: " + getLocalId() + ", global: " + getId() + ")";
    }
}