package de.venjinx.ejx.tiled;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.entity.EntityDef;
import de.venjinx.ejx.entity.component.ActorComponent;
import de.venjinx.ejx.entity.component.CommandComponent;
import de.venjinx.ejx.entity.component.EJXComponent;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.tiled.entity.EJXObject;
import de.venjinx.ejx.tiled.layer.EJXGroupLayer;
import de.venjinx.ejx.tiled.layer.EJXImageLayer;
import de.venjinx.ejx.tiled.layer.EJXLayer;
import de.venjinx.ejx.tiled.layer.EJXObjectLayer;
import de.venjinx.ejx.tiled.layer.EJXTileLayer;
import de.venjinx.ejx.tiled.object.EJXMapObject;
import de.venjinx.ejx.tiled.object.EJXTileObject;
import de.venjinx.ejx.tiled.tile.EJXTile;
import de.venjinx.ejx.tiled.tileset.EJXAbstractTileset;
import de.venjinx.ejx.tiled.tileset.EJXSpriteSet;
import de.venjinx.ejx.tiled.tileset.EJXTileSet;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.ejx.util.loader.EJXLoader;
import de.venjinx.ejx.util.loader.EJXLoaderUtil;
import de.venjinx.ejx.util.loader.FinalTask;
import de.venjinx.ejx.util.loader.TMXFileTask;
import de.venjinx.ejx.util.loader.TSXFileTask;

public class TiledFactory implements Disposable {

    private EJXGame game;
    private ObjectMap<String, EJXTiledMap> maps;
    private ObjectMap<String, EJXAbstractTileset> tilesets;
    private ObjectMap<Integer, EntityDef> entityDefs;
    private ObjectMap<String, EJXTile> definitions;
    private ObjectMap<String, EJXAbstractTileset> objectDefinitions;

    private EJXLoader loader;
    private int nextDefId = 0;

    public TiledFactory(EJXGame game, EJXLoader loader) {
        this.game = game;
        maps = new ObjectMap<>();
        tilesets = new ObjectMap<>();
        entityDefs = new ObjectMap<>();
        definitions = new ObjectMap<>();
        objectDefinitions = new ObjectMap<>();
        this.loader = loader;
    }

    public EJXTiledMap getMap(String name) {
        return maps.get(name);
    }

    public EJXAbstractTileset getTileset(String name) {
        return tilesets.get(name);
    }

    // TODO needed for engine assets - out for kawaida
    public FinalTask loadEntityDefinitions() {
        VJXLogger.logIncr("Loading definitions...");
        //        entityDefs.clear();
        //
        //        FileHandle entitiesFolder = Gdx.files.internal("data/definitions/");
        //        if (entitiesFolder.exists()) {
        //            FinalTask finalTask;
        //            FileHandle[] entityFiles = entitiesFolder.list();
        //            final TSXFileTask[] tasks = new TSXFileTask[entityFiles.length];
        //            for (int i = 0; i < entityFiles.length; i++) {
        //                VJXLogger.log("Load definitions " + entityFiles[i]);
        //                tasks[i] = newTilesetTask(entityFiles[i], false);
        //            }
        //            finalTask = new FinalTask(new Runnable() {
        //                @Override
        //                public void run() {
        //                    EJXAbstractTileset set;
        //                    EJXTile tile;
        //                    Iterator<TiledMapTile> tiles;
        //                    for (TSXFileTask loadFileTask : tasks) {
        //                        set = loadFileTask.getSet();
        //                        tilesets.put(set.getName(), set);
        //                        tiles = set.iterator();
        //                        while (tiles.hasNext()) {
        //                            tile = (EJXTile) tiles.next();
        //                            if (tile.getType() == null) continue;
        //                            if (tile.getType().equals("entity")) {
        //                                VJXLogger.log("Load def: " + tile.getName());
        //                                definitions.put(tile.getName(), tile);
        //                            }
        //                        }
        //                    }
        //                }
        //            });
        //            game.loader.addTask(finalTask);
        //            VJXLogger.logDecr("Loaded definitions.");
        //            return finalTask;
        //        }
        //        VJXLogger.logDecr("Loaded definitions.");
        return null;
    }

    public TMXFileTask newMapTask(FileHandle tmxFile, boolean loadOnFinish) {
        if (!checkMapFile(tmxFile)) return null;

        String name = tmxFile.nameWithoutExtension();
        if (maps.containsKey(name)) return null;

        final TMXFileTask tmxTask = new TMXFileTask(tmxFile, this, loadOnFinish);
        tmxTask.setOnFinished(new Runnable() {

            @Override
            public void run() {
                EJXTiledMap map = tmxTask.getMap();
                // TODO needed for engine assets - out for kawaida
                //                map.init(game.assets);
            }
        });
        loader.addTask(tmxTask);
        return tmxTask;
    }

    public TSXFileTask newTilesetTask(FileHandle tsxFile, boolean loadOnFinish) {
        if (!checkTilesetFile(tsxFile)) return null;
        if (tilesets.containsKey(tsxFile.nameWithoutExtension())) {

            return null;
        }
        final TSXFileTask tsxTask = new TSXFileTask(tsxFile, this, loadOnFinish);
        tsxTask.setOnFinished(new Runnable() {

            @Override
            public void run() {
                EJXAbstractTileset set = tsxTask.getSet();
                // TODO needed for engine assets - out for kawaida
                //                set.updateTextures(game.assets);
            }
        });
        loader.addTask(tsxTask);
        return tsxTask;
    }

    public EJXTiledMap mapFromTMX(FileHandle tmxFile) {
        if (!checkMapFile(tmxFile)) return null;

        String name = tmxFile.nameWithoutExtension();
        VJXLogger.logIncr("Load map " + name);
        if (maps.containsKey(name)) return maps.get(name);

        Element mapElement = EJXLoaderUtil.loadXMLFile(tmxFile);
        EJXTiledMap map = new EJXTiledMap(tmxFile.nameWithoutExtension());
        map.setMapElement(mapElement);

        loadMapElements(mapElement, map, tmxFile);
        maps.put(map.getName(), map);
        VJXLogger.logDecr("Loaded map");
        return map;
    }

    public EJXObject createObject(EJXMapObject mapObject) {
        //        VJXLogger.log("create " + mapObject.getName());
        //        VJXLogger.printMapProperties(mapObject.getProperties());

        EJXObject object = new EJXObject(mapObject);
        object.setGameControl(game.gameControl);

        CommandComponent commands = (CommandComponent) EJXComponent
                        .newComponent(VJXLogicType.COMMAND, object);
        commands.init(mapObject);
        object.addComponent(commands);

        ActorComponent objComp = (ActorComponent) EJXComponent
                        .newComponent(VJXLogicType.ACTOR, object);
        objComp.init(mapObject);
        object.addComponent(objComp);

        return object;
    }

    public void unloadMap(String name) {
        EJXTiledMap map = getMap(name);
        unloadMap(map);
    }

    public void unloadMap(EJXTiledMap map) {
        for (String resourceName : map.getResources().keys()) {
            game.assets.unloadResource(resourceName);
        }
        maps.remove(map.getName());
    }

    private void loadMapElements(Element mapElement, EJXTiledMap map,
                    FileHandle tmxMapFile) {
        Element element;
        for (int i = 0, j = mapElement.getChildCount(); i < j; i++) {
            element = mapElement.getChild(i);
            loadMapElement(element, null, map.getLayers(), map, tmxMapFile);
        }
    }

    private void loadMapElement(Element element, MapLayer parent,
                    MapLayers layers,
                    EJXTiledMap map,
                    FileHandle tmxMapFile) {
        String type = element.getName();
        String name = element.getAttribute(VJXString.name, EJXLayer.EJXLayer);
        MapLayer layer;
        switch (type) {
            case VJXString.TILED_layer:
                layer = createTileLayer(name, element, map);
                layer.setParent(parent);
                layers.add(layer);
                break;
            case VJXString.TILED_objectgroup:
                layer = createObjectLayer(name, element, map);
                layer.setParent(parent);
                layers.add(layer);
                break;
            case VJXString.TILED_imagelayer:
                layer = createImageLayer(name, element, map);
                layer.setParent(parent);
                layers.add(layer);
                break;
            case VJXString.TILED_group:
                layer = createGroupLayer(name, element, map, tmxMapFile);
                layer.setParent(parent);
                layers.add(layer);
                break;
            case VJXString.tileset:
                EJXAbstractTileset tileset;
                String source = element.getAttribute(VJXString.source, null);
                int firstId = element.getIntAttribute(VJXString.TILED_firstgid, 1);
                if (source != null) {
                    // load external
                    tmxMapFile = EJXLoaderUtil.getRelativeFileHandle(tmxMapFile, source);
                    element = EJXLoaderUtil.loadXMLFile(tmxMapFile);
                    name = element.getAttribute(VJXString.name,
                                    EJXAbstractTileset.EJXTileSet);
                }

                if (tilesets.containsKey(name)) {
                    VJXLogger.log("reuse set name " + name + " file: "
                                    + tmxMapFile.nameWithoutExtension()
                                    + " fId " + firstId);
                    tileset = tilesets.get(name);
                } else {
                    VJXLogger.log("create set name " + name + " file: "
                                    + tmxMapFile.nameWithoutExtension()
                                    + " fId " + firstId);
                    tileset = tilesetFromTSX(tmxMapFile, element, firstId);
                    tilesets.put(tileset.getName(), tileset);
                }

                map.addTileset(tileset, firstId);
                for (String key : tileset.getResources().keys()) {
                    map.addResource(key, tileset.getResources().get(key));
                }
            default:
                break;
        }
    }

    public EJXAbstractTileset tilesetFromTSX(FileHandle tsxFile) {
        Element element = EJXLoaderUtil.loadXMLFile(tsxFile);
        int firstId = element.getIntAttribute(VJXString.TILED_firstgid, 1);
        return tilesetFromTSX(tsxFile, element, firstId);
    }

    private EJXAbstractTileset tilesetFromTSX(FileHandle tsxFile,
                    Element element, int firstId) {
        EJXAbstractTileset tileset = tilesetFromElement(element, firstId);
        tileset.fetchResources(tsxFile);

        Array<Element> tiles = element.getChildrenByName(EJXTile.tile);
        tileset.prepare(tiles);
        return tileset;
    }

    private EJXAbstractTileset tilesetFromElement(Element element, int firstId) {
        EJXAbstractTileset tileset;
        Element imageElement = element.getChildByName(VJXString.TILED_image);
        if (imageElement != null) {
            tileset = new EJXTileSet(element);
        } else {
            tileset = new EJXSpriteSet(element);
        }
        return tileset;
    }

    private boolean checkMapFile(FileHandle mapFile) {
        if (mapFile == null || !mapFile.exists()) {
            VJXLogger.log(LogCategory.ERROR,
                            "TiledFactory - File null or not existing: "
                                            + mapFile);
            return false;
        }

        Element element = EJXLoaderUtil.loadXMLFile(mapFile);
        if (!element.getName().equals(VJXString.map)) {
            VJXLogger.log(LogCategory.ERROR,
                            "TiledFactory - No valid map file: "
                                            + mapFile.path());
            return false;
        }
        return true;
    }

    private boolean checkTilesetFile(FileHandle tilesetFile) {
        if (tilesetFile == null || !tilesetFile.exists()) {
            VJXLogger.log(LogCategory.ERROR,
                            "TiledFactory - File null or not existing.");
            return false;
        }

        Element element = EJXLoaderUtil.loadXMLFile(tilesetFile);
        if (!element.getName().equals(VJXString.tileset)) {
            VJXLogger.log(LogCategory.ERROR,
                            "TiledFactory - No valid tileset file: "
                                            + tilesetFile.path());
            return false;
        }
        return true;
    }

    private EJXGroupLayer createGroupLayer(String name, Element layerElement,
                    EJXTiledMap map, FileHandle tmxMapFile) {
        EJXGroupLayer layer = new EJXGroupLayer(name);
        Element element;
        for (int i = 0, j = layerElement.getChildCount(); i < j; i++) {
            element = layerElement.getChild(i);
            loadMapElement(element, layer, layer.getLayers(), map, tmxMapFile);
        }
        return layer;
    }

    private EJXImageLayer createImageLayer(String name, Element layerElement,
                    EJXTiledMap map) {
        float offsetX = layerElement.getFloatAttribute(VJXString.TILED_offsetX, 0);
        float offsetY = layerElement.getFloatAttribute(VJXString.TILED_offsetY, 0);
        float opacity = layerElement.getFloatAttribute(VJXString.TILED_opacity, 1f);
        boolean visible = layerElement.getBooleanAttribute(VJXString.visible, true);
        //        boolean locked = imageElem.getBooleanAttribute(VJXString.TILED_locked);

        EJXImageLayer layer = new EJXImageLayer(name);
        layer.setVisible(visible);
        layer.setOpacity(opacity);
        layer.setOffsetX(offsetX);
        layer.setOffsetY(-offsetY);

        Element imageElem = layerElement.getChildByName(VJXString.TILED_image);
        String source = imageElem.getAttribute(VJXString.source);
        String transColor = imageElem.getAttribute(VJXString.TILED_trans, null);
        Color c = (Color) EJXUtil.castProperty(name, transColor, VJXString.VAR_color);
        layer.setColor(c);

        FileHandle imageFile = EJXLoaderUtil.convertRawPath(source);
        map.addResource(name, new AssetDescriptor<>(imageFile.path(), Texture.class));
        layer.getProperties().put(VJXString.TILED_imageSource, imageFile);
        return layer;
    }

    private EJXTileLayer createTileLayer(String name, Element element,
                    EJXTiledMap map) {
        float offsetX = element.getFloatAttribute(VJXString.TILED_offsetX, 0);
        float offsetY = element.getFloatAttribute(VJXString.TILED_offsetY, 0);
        float opacity = element.getFloatAttribute(VJXString.TILED_opacity, 1f);
        boolean visible = element.getBooleanAttribute(VJXString.visible, true);
        boolean drawOrderY = element.getAttribute(VJXString.TILED_draworder, VJXString.STR_EMPTY).isEmpty();

        EJXTileLayer layer = new EJXTileLayer(name, map, element);
        layer.setOpacity(opacity);
        layer.setVisible(visible);
        layer.setOffsetX(offsetX);
        layer.setOffsetY(offsetY);
        layer.getProperties().put("drawOrderY", drawOrderY);
        return layer;
    }

    private EJXObjectLayer createObjectLayer(String name, Element layerElement,
                    EJXTiledMap map) {
        EJXObjectLayer layer = new EJXObjectLayer(name);
        String color = layerElement.getAttribute(VJXString.TILED_color, null);
        Color c = (Color) EJXUtil.castProperty(name, color, VJXString.VAR_color);
        layer.setColor(c);

        MapObject object;
        MapObjects objects = layer.getObjects();
        Array<Element> objectElems = layerElement
                        .getChildrenByName(VJXString.TILED_object);
        for (Element objectElement : objectElems) {
            object = createMapObject(objectElement, map, false);
            objects.add(object);
        }
        return layer;
    }

    private static MapObject createMapObject(Element objectElem, EJXTiledMap map,
                    boolean convertToTileSpace) {

        String gidStr = objectElem.getAttribute(VJXString.TILED_gid, null);
        if (gidStr != null) {
            int tileId = Integer.parseInt(gidStr);
            tileId = tileId & ~EJXTileLayer.MASK_CLEAR;
            EJXTile tile = (EJXTile) map.getTile(tileId);
            if (tile == null) {
                throw new GdxRuntimeException(
                                "Can not create EJXTileObject. Tile can not be null");
            }
            return EJXTileObject.create(objectElem, tile, map, convertToTileSpace);
        } else {
            return EJXMapObject.create(objectElem, map);
        }
    }

    @Override
    public void dispose() {
        for (EJXTiledMap map : maps.values()) {
            unloadMap(map);
        }
        maps.clear();
        objectDefinitions.clear();
    }
}