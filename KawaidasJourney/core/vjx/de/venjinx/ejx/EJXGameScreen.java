/*******************************************************************************
 * Copyright 2020 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package de.venjinx.ejx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.controllers.InputControl;
import de.venjinx.ejx.scenegraph.EJXRenderer;
import de.venjinx.ejx.scenegraph.ScenegraphRenderer;
import de.venjinx.ejx.stages.DebugStage;
import de.venjinx.ejx.stages.DebugStage.DebugStats;
import de.venjinx.ejx.stages.EJXStage;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.stages.LoadingStage;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.tiled.EJXTiledMap;
import de.venjinx.ejx.tiled.Level;
import de.venjinx.ejx.tiled.TiledMenu;
import de.venjinx.ejx.util.EJXTimer;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.ejx.util.loader.FinalTask;

/**
 * A {@link Screen} implementation that is used with an {@link EJXGame}. It
 * handles the game flow using {@link EJXStage EJXStages}. It provides a debug
 * and a loading stage by default and also allows to set a {@link MenuStage} and
 * a {@link LevelStage}. A {@link ScenegraphRenderer} is used to render the
 * {@link LevelStage}. In addition custom {@link EJXStage EJXStages} may be
 * added. This class is supposed to be extended and used as the main screen for
 * a game.
 *
 * @author Torge Rothe (X-Ray-Jin, xrayjin@gmx.de )
 *
 */
public abstract class EJXGameScreen implements Screen {

    protected EJXGame game;

    private boolean paused = false;

    private long frameNr = 0;
    private int loadingMark = EJXTimer.getMark();

    protected InputControl input;
    protected LoadingStage loader;
    protected DebugStage dbgStage;

    protected Level levelStage;

    protected MenuStage menu;
    protected LevelStage level;
    protected ScenegraphRenderer levelRenderer;

    protected Array<EJXStage> customStages;

    protected EJXRenderer renderer;

    //    private LoadFileTask[] menuTasks;

    /**
     * Creates a new {@link EJXGameScreen} instance.
     *
     * @param game
     *             the {@link EJXGame} this screen is used with.
     */
    public EJXGameScreen(EJXGame game) {
        this.game = game;

        customStages = new Array<>();

        dbgStage = new DebugStage(game);
        dbgStage.setActive(true);

        loader = new LoadingStage(game);

        ((InputMultiplexer) Gdx.input.getInputProcessor()).addProcessor(loader);
        ((InputMultiplexer) Gdx.input.getInputProcessor()).addProcessor(dbgStage);

        levelRenderer = new ScenegraphRenderer(game);

        input = new InputControl(2);
        input.init(game);
        setInputControl(input);

        renderer = new EJXRenderer(game.assets, game.getConfig(), game.batch,
                        game.getDisplayCamera());
    }

    /**
     * Called during game creation. Loads all {@link TiledMenu TiledMenus} from
     * tmx files located in folder: 'assets/data/ui/menus/'.
     */
    public FinalTask loadMenus() {
        if (menu == null) return null;
        int id = 0;
        FileHandle menuFolder = Gdx.files.internal("data/ui/menus/");
        FileHandle[] menuFiles;
        FinalTask finalTask;
        if (menuFolder.exists()) {
            menuFiles = menuFolder.list();
            for (int i = 0; i < menuFiles.length; i++) {
                final int menuID = id;
                game.tFactory.newMapTask(menuFiles[i], true);
                TiledMenu menuTable = new TiledMenu(
                                menuFiles[i].nameWithoutExtension(), menu);
                menu.addMenu(menuID, menuTable);
                id++;
            }
            finalTask = new FinalTask(new Runnable() {
                @Override
                public void run() {
                    if (menu != null) {
                        menu.initMenus();
                        menu.init();
                        menu.setActive(true);
                        menu.setRunBackground(false);
                        menu.switchMenu(1, 2, 2);
                    }
                    game.gameLoaded(false);
                }
            });
            game.loader.addTask(finalTask);
        } else {
            finalTask = new FinalTask(new Runnable() {
                @Override
                public void run() {
                }
            });
            game.loader.addTask(finalTask);
        }
        startLoadMenu();
        return finalTask;
    }

    public void userRender() {

    }

    public void startLoadMenu() {
        EJXTimer.setMark(loadingMark);

        loader.setActive(true);
        menu.setRunBackground(false, .5f);

        VJXLogger.logIncr(LogCategory.LOAD, "Screen start loading menu...");
        VJXLogger.logIncr(LogCategory.LOAD,
                        "Screen start preparing menu data...");
        VJXLogger.logDecr(LogCategory.LOAD, "Screen prepared menu. Time: "
                        + EJXTimer.timeSince(loadingMark) + "ms.");
    }

    public abstract void loadLevel(String path, boolean internal);

    public abstract void startLevel();

    public abstract void restartLevel();

    public abstract void unloadLevel(boolean isEnd);

    /**
     * Called during {@link #update(float)} after the custom stages have been
     * updated. May be used for custom update code.
     *
     * @param delta
     *              seconds since last frame
     */
    protected abstract void postProcess(float delta);

    /**
     * @return true if the screen is paused
     */
    public boolean isPaused() {
        return paused;
    }

    /**
     * @return the current screen frame number
     */
    public long getFrameNr() {
        return frameNr;
    }

    /**
     * Sets this screen into debug mode. This causes the screen to set all its
     * managed {@link EJXStage EJXStages} into debug mode as well.
     *
     * @param debug
     *              true to activate debug mode for all stages
     */
    public void setDebug(boolean debug) {
        loader.setDebugAll(debug);
        levelRenderer.setDebug(debug);
        for (EJXStage ejxStage : customStages) {
            ejxStage.setDebugAll(debug);
        }
    }

    /**
     * Removes the current input controller and sets the new input controller to
     * use for this screen instance.
     *
     * @param input
     *              the new input controller to use
     * @see InputControl
     */
    public void setInputControl(InputControl input) {
        if (this.input != null)
            ((InputMultiplexer) Gdx.input.getInputProcessor())
            .removeProcessor(this.input);
        this.input = input;
        dbgStage.setInputControl(input);
        ((InputMultiplexer) Gdx.input.getInputProcessor()).addProcessor(0, input);
    }

    /**
     * @return the currently used {@link InputControl}
     */
    public InputControl getInputControl() {
        return input;
    }

    /**
     * @return the currently used {@link MenuStage}
     */
    public MenuStage getMenu() {
        return menu;
    }

    /**
     * Casts current {@link MenuStage} to the specified 'clazz' and returns it.
     *
     * @param clazz
     *              the target return type
     * @return the current {@link MenuStage}
     */
    @SuppressWarnings("unchecked")
    public <T> T getMenu(Class<T> clazz) {
        return (T) menu;
    }

    /**
     * @return the currently used {@link LevelStage}
     */
    public LevelStage getLevel() {
        return level;
    }

    /**
     * Casts current {@link LevelStage} to the specified 'clazz' and returns it.
     *
     * @param clazz
     *              the target return type
     * @return the current {@link LevelStage}
     */
    @SuppressWarnings("unchecked")
    public <T> T getLevel(Class<T> clazz) {
        return (T) level;
    }

    public Level getLevelStage() {
        return levelStage;
    }

    /**
     * @return the currently used {@link LoadingStage}
     */
    public LoadingStage getLoader() {
        return loader;
    }

    /**
     * @return the currently used {@link DebugStage}
     */
    public DebugStage getDebugStage() {
        return dbgStage;
    }

    /**
     * Set the {@link EJXTiledMap} that is rendered by the {@link EJXRenderer}.
     *
     * @param map
     *            the map to render
     */
    public void setMapToRender(EJXTiledMap map) {
        renderer.setMap(map);
    }

    /**
     * @return the currently used {@link ScenegraphRenderer}
     */
    public ScenegraphRenderer getRenderer() {
        return levelRenderer;
    }

    /**
     * Updates {@link InputControl} and all {@link EJXStage EJXStages}. Except
     * {@link LoadingStage} and {@link DebugStage} all stages are only updated
     * if the screen is not paused. After the custom stages have been updated
     * {@link #postProcess(float)} is called.<br>
     *
     * Update loop:
     *
     * <pre>
     *  1. update input
     *  2. update loader
     *  3. if (!paused) update stages
     *  4. postProcess(deltaT)
     *  5. update debug
     *  6. reset input
     * </pre>
     *
     * @param delta
     *              seconds since last frame
     */
    public void update(float delta) {
        VJXLogger.logIncr(LogCategory.GAME | LogCategory.FLOW | LogCategory.DETAIL,
                        "Game screen update loop at frame #" + frameNr
                        + " started updating.");
        DebugStats.screenUpdateTime = System.nanoTime();

        input.update(delta);
        loader.act(delta);

        // actual game stuff
        if (!paused) {
            for (EJXStage ejxStage : customStages) {
                ejxStage.act(delta);
            }
            //            DebugStats.hudUpdateTime = System.nanoTime();
            //            menu.act(deltaT);
            //            DebugStats.hudUpdateTime = System.nanoTime() - DebugStats.hudUpdateTime;

            //            DebugStats.objectsUpdateTime = 0;
            //            DebugStats.levelUpdateTime = System.nanoTime();
            //            level.act(deltaT);
            //            DebugStats.levelUpdateTime = System.nanoTime() - DebugStats.levelUpdateTime;
        }

        postProcess(delta);

        DebugStats.screenUpdateTime = System.nanoTime() - DebugStats.screenUpdateTime;

        dbgStage.act(delta);
        input.resetInput();

        VJXLogger.logDecr(LogCategory.GAME | LogCategory.FLOW
                        | LogCategory.DETAIL,
                        "Game screen main update loop at frame #" + frameNr
                        + " finished updating.");
    }

    /**
     * Renders the screen:<br>
     *
     * <pre>
     *  1. set projection matrices
     *  2. render tiled map
     *  3. render stages
     *  4. render loader
     *  5. render debug
     * </pre>
     *
     * @param delta
     *              seconds since last frame
     */
    @Override
    public void render(float delta) {
        VJXLogger.logIncr(LogCategory.GAME | LogCategory.FLOW | LogCategory.DETAIL,
                        "Game screen render frame #" + frameNr + " started.");
        DebugStats.screenRenderTime = System.nanoTime();
        dbgStage.getGLProfiler().reset();

        game.batch.setProjectionMatrix(game.getDisplayCamera().combined);
        game.shapeRenderer.setProjectionMatrix(game.getDisplayCamera().combined);

        userRender();

        if (!customStages.isEmpty()) {
            DebugStats.objectsRenderTime = 0;
            for (EJXStage ejxStage : customStages) {
                ejxStage.draw();
            }
        } else {
            game.batch.begin();
            Texture t = game.assets.getTexture(EJXGame.SPLASH);
            game.batch.draw(t, 0, 0);
            game.batch.end();
        }

        //        DebugStats.levelRenderTime = System.nanoTime();
        //        level.draw();
        //        DebugStats.levelRenderTime = System.nanoTime() - DebugStats.levelRenderTime;

        //        DebugStats.hudRenderTime = System.nanoTime();
        //        menu.draw();
        //        DebugStats.hudRenderTime = System.nanoTime() - DebugStats.hudRenderTime;

        loader.draw();
        DebugStats.screenRenderTime = System.nanoTime() - DebugStats.screenRenderTime;

        dbgStage.getDebugUI().updateGraphics();
        dbgStage.draw();

        VJXLogger.logDecr(LogCategory.GAME | LogCategory.FLOW | LogCategory.DETAIL,
                        "Game screen render frame #" + frameNr + " finished.");
        frameNr++;
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        paused = false;
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void resize(int width, int height) {
        if (level != null) {
            level.getViewport().update(width, height);
            level.getViewport().apply();
            levelRenderer.resize(level.getViewport(), width, height);
        }
    }

    @Override
    public void dispose() {
        VJXLogger.logIncr(LogCategory.GAME | LogCategory.FLOW
                        | LogCategory.LOAD | LogCategory.INIT,
                        "Game screen disposing.");

        for (EJXStage ejxStage : customStages) {
            ((InputMultiplexer) Gdx.input.getInputProcessor()).removeProcessor(ejxStage);
            if (ejxStage.isInitialized()) {
                ejxStage.dispose();
            }
        }

        ((InputMultiplexer) Gdx.input.getInputProcessor()).removeProcessor(dbgStage);
        dbgStage.dispose();

        ((InputMultiplexer) Gdx.input.getInputProcessor()).removeProcessor(input);

        loader.dispose();
        levelRenderer.dispose();
        VJXLogger.logDecr(LogCategory.GAME | LogCategory.FLOW
                        | LogCategory.LOAD | LogCategory.INIT,
                        "Game screen disposed.");
    }
}