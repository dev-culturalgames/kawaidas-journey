package de.venjinx.ejx.controllers;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.VJXData;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class CollisionControl implements ContactListener {

    @Override
    public void beginContact(Contact contact) {
        checkBeginContact(contact);

        Fixture f0 = contact.getFixtureA();
        Fixture f1 = contact.getFixtureB();
        VJXData fData0 = (VJXData) f0.getUserData();
        VJXData fData1 = (VJXData) f1.getUserData();
        EJXEntity e0 = fData0.getEntity();
        EJXEntity e1 = fData1.getEntity();

        VJXLogger.logIncr(LogCategory.COLLISION, "Begin collision: " + e0 + " - " + e1);
        VJXLogger.log(LogCategory.COLLISION, e0.getName() + " - " + e1.getName()
        + " Fixtures: " + fData0.getName() + " - "
        + fData1.getName() + ": " + contact.isEnabled() + " - "
        + contact.isTouching());
        if (!contact.isEnabled()) {
            VJXLogger.logDecr(LogCategory.COLLISION,
                            "Begin collision disabled: " + e0 + " - " + e1);
            return;
        }

        if (e0.hasCollPrio()) {
            e1.acceptBeginColl(f1.getFilterData().categoryBits, fData1, e0,
                            f0.getFilterData().categoryBits, fData0);
            e0.acceptBeginColl(f0.getFilterData().categoryBits, fData0, e1,
                            f1.getFilterData().categoryBits, fData1);
        } else {
            e0.acceptBeginColl(f0.getFilterData().categoryBits, fData0, e1,
                            f1.getFilterData().categoryBits, fData1);
            e1.acceptBeginColl(f1.getFilterData().categoryBits, fData1, e0,
                            f0.getFilterData().categoryBits, fData0);
        }

        VJXLogger.logDecr(LogCategory.COLLISION,
                        "Begin collision finished: " + e0 + " - " + e1);
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        Fixture f0 = contact.getFixtureA();
        Fixture f1 = contact.getFixtureB();
        VJXData fData0 = (VJXData) f0.getUserData();
        VJXData fData1 = (VJXData) f1.getUserData();
        EJXEntity e0 = fData0.getEntity();
        EJXEntity e1 = fData1.getEntity();

        short otherCat = f0.getFilterData().categoryBits;
        short thisCat = f1.getFilterData().categoryBits;

        if (e0.getPhysics().isGhost(f1) || e1.getPhysics().isGhost(f0)
                        || e0.getPhysics().isGhost() || e1.getPhysics().isGhost()) {
            contact.setEnabled(false);
            return;
        }

        // allow alive enemies to collide physically to not stack
        if (B2DBit.OBJECT.isCategory(thisCat)
                        && B2DBit.OBJECT.isCategory(otherCat)) {
            boolean sameType = e0.getDef().getDefID() == e1.getDef().getDefID();

            contact.setEnabled(sameType && e0.isAlive() && e1.isAlive());
        }
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }

    @Override
    public void endContact(Contact contact) {
        Fixture f0 = contact.getFixtureA();
        Fixture f1 = contact.getFixtureB();
        VJXData fData0 = (VJXData) f0.getUserData();
        VJXData fData1 = (VJXData) f1.getUserData();
        EJXEntity e0 = fData0.getEntity();
        EJXEntity e1 = fData1.getEntity();
        checkEndContact(contact);

        VJXLogger.logIncr(LogCategory.COLLISION, "End collision: " + e0 + " - " + e1);
        VJXLogger.log(LogCategory.COLLISION, e0.getName() + " - " + e1.getName()
        + " Fixtures: " + fData0.getName() + " - "
        + fData1.getName() + ": " + contact.isEnabled());
        if (!contact.isEnabled()) {
            VJXLogger.logDecr(LogCategory.COLLISION,
                            "End collision disabled: " + e0 + " - " + e1);
            return;
        }

        if (e0.hasCollPrio()) {
            e1.acceptEndColl(f1.getFilterData().categoryBits, fData1, e0,
                            f0.getFilterData().categoryBits, fData0);
            e0.acceptEndColl(f0.getFilterData().categoryBits, fData0, e1,
                            f1.getFilterData().categoryBits, fData1);
        } else {
            e0.acceptEndColl(f0.getFilterData().categoryBits, fData0, e1,
                            f1.getFilterData().categoryBits, fData1);
            e1.acceptEndColl(f1.getFilterData().categoryBits, fData1, e0,
                            f0.getFilterData().categoryBits, fData0);
        }

        VJXLogger.logDecr(LogCategory.COLLISION,
                        "End collision finished: " + e0 + " - " + e1);
    }

    private void checkBeginContact(Contact contact) {
        Fixture f0 = contact.getFixtureA();
        Fixture f1 = contact.getFixtureB();
        EJXEntity e0 = ((VJXData) f0.getUserData()).getEntity();
        EJXEntity e1 = ((VJXData) f1.getUserData()).getEntity();

        //        VJXLogger.log("e0:" + e0 + " ghost " + e0.getPhysics().isGhost());
        //        VJXLogger.log("e1:" + e1 + " ghost " + e1.getPhysics().isGhost());
        boolean enabled = contact.isEnabled();
        enabled &= !e0.getPhysics().isGhost(f1)
                        && !e1.getPhysics().isGhost(f0);
        enabled &= !e0.getPhysics().isGhost() && !e1.getPhysics().isGhost();
        if (!enabled) {
            contact.setEnabled(enabled);
            return;
        }
        enabled &= e0.checkBeginCollision(f0, f1, contact, e1);
        enabled &= e1.checkBeginCollision(f1, f0, contact, e0);
        contact.setEnabled(enabled);
    }

    private void checkEndContact(Contact contact) {
        Fixture f0 = contact.getFixtureA();
        Fixture f1 = contact.getFixtureB();
        EJXEntity e0 = ((VJXData) f0.getUserData()).getEntity();
        EJXEntity e1 = ((VJXData) f1.getUserData()).getEntity();

        boolean enabled = true;
        enabled &= !e0.getPhysics().isGhost() && !e1.getPhysics().isGhost();
        if (!enabled) {
            contact.setEnabled(enabled);
            return;
        }
        enabled &= e0.checkEndCollision(f0, f1, contact, e1);
        enabled &= e1.checkEndCollision(f1, f0, contact, e0);
        contact.setEnabled(enabled);
    }

    public static boolean isCategory(short cat, B2DBit physCat) {
        return (cat & physCat.id) > 0;
    }

}