package de.venjinx.ejx.controllers;

import java.util.Locale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class TextControl {

    private static TextControl instance;

    public static TextControl getInstance() {
        if (instance == null) {
            instance = new TextControl();
        }
        return instance;
    }

    // localization identifiers
    public static final String LOCALE_EN_US = "en_US";
    public static final String LOCALE_DE_DE = "de_DE";
    public static final String LOCALE_SW_TZ = "sw_TZ";

    // static helper strings for building up dynamic text ids
    public static final String _SUBTITLE = "_subtitle";
    public static final String _TITLE = "_title";
    public static final String _TEXT = "_text";
    public static final String OFFER_TYPE_ = "offer_type_";
    public static final String _UNLOCK_TEXT = "_unlock_text";

    private static final String LOCALIZATION_FILE_PATH = "data/localization.xml";

    private static final String MISSING = " missing translation:";
    private static final String WARNING_TEXT_ID_EXISTS = "TextControl - TextID already exists: ";
    private static final String ERROR_LOCALE_NOT_FOUND = "TextControl - Locale not found: ";

    private static final String DEFAULT = "default";
    private static final String LANGUAGES = "languages";
    private static final String LANGUAGE_ID = "languageID";
    private static final String COUNTRY_ID = "countryID";

    private ObjectMap<Locale, ObjectMap<String, String>> languages;
    private Locale defaultLocale;
    private Locale currentLocale;

    private TextControl() {
        languages = new ObjectMap<>();
        createLanguages(Gdx.files.internal(LOCALIZATION_FILE_PATH));
        setLocale(TextControl.LOCALE_EN_US);
    }

    public Locale getCurrentLocale() {
        return currentLocale;
    }

    public String getCurrentLocaleName() {
        return currentLocale.toString();
    }

    public boolean hasText(String textID) {
        ObjectMap<String, String> language = languages.get(currentLocale);
        if (language == null || !language.containsKey(textID))
            return false;
        String text = language.get(textID);
        // check if text is actually a textID
        if (text.matches(EJXRegEx.matchVAR))
            return hasText(text.replaceAll(VJXString.VAR, VJXString.STR_EMPTY));
        return language.containsKey(textID);
    }

    public String get(String textID) {
        return get(textID, textID + MISSING + currentLocale);
    }

    public String get(String textID, String defaultString) {
        textID = textID.replaceAll(VJXString.VAR, VJXString.STR_EMPTY);
        ObjectMap<String, String> language = languages.get(currentLocale);
        if (language == null || !language.containsKey(textID))
            return defaultString;
        String text = language.get(textID);
        // check if text is actually a textID
        if (text.matches(EJXRegEx.matchVAR))
            return get(text);
        return language.get(textID);
    }

    public Locale getLocale(String localeID) {
        for (Locale key : languages.keys()) {
            if (key.toString().equals(localeID)) { return key; }
        }
        return defaultLocale;
    }

    public void setLocale(String localeID) {
        Locale locale = getLocale(localeID);
        if (locale == null) {
            if (currentLocale == null) currentLocale = defaultLocale;
            VJXLogger.log(LogCategory.ERROR, ERROR_LOCALE_NOT_FOUND + localeID);
            return;
        }
        currentLocale = locale;
    }

    private void createLanguages(FileHandle localizationFile) {
        if (localizationFile.exists()) {
            XmlReader xml = new XmlReader();
            Element localizationRoot = xml.parse(localizationFile);
            Element languagesElem = localizationRoot.getChildByName(LANGUAGES);
            if (languagesElem != null) {
                String languageID, countryID;
                Locale locale;
                for (int i = 0; i < languagesElem.getChildCount(); i++) {
                    Element languageElem = languagesElem.getChild(i);
                    if (!languageElem.hasAttribute(LANGUAGE_ID)) continue;
                    if (!languageElem.hasAttribute(COUNTRY_ID)) continue;

                    languageID = languageElem.getAttribute(LANGUAGE_ID);
                    countryID = languageElem.getAttribute(COUNTRY_ID);
                    locale = createLanguage(languageID, countryID);
                    if (i == 0) defaultLocale = locale;
                }
                loadLanguages(localizationRoot);
            }
        }

        if (languages.isEmpty()) {
            defaultLocale = new Locale(DEFAULT);
            languages.put(defaultLocale, new ObjectMap<String, String>());
        }
        currentLocale = defaultLocale;
    }

    private Locale createLanguage(String language, String country) {
        Locale locale = new Locale(language, country);
        ObjectMap<String, String> texts = new ObjectMap<>();
        languages.put(locale, texts);
        return locale;
    }

    private void loadLanguages(Element localizationRoot) {
        Element textIDsElement, textElement;
        String textID, localeID, text;
        for (int i = 0; i < localizationRoot.getChildCount(); i++) {
            textIDsElement = localizationRoot.getChild(i);
            for (int j = 0; j < textIDsElement.getChildCount(); j++) {
                textElement = textIDsElement.getChild(j);
                textID = textElement.getName();
                for (Locale locale : languages.keys()) {
                    ObjectMap<String, String> language = languages.get(locale);
                    localeID = locale.toString();
                    if (textElement.hasAttribute(localeID)) {
                        text = textElement.getAttribute(localeID);
                        // replace html symbols
                        // google sheet carriage return
                        text = text.replaceAll("&#xA;", VJXString.SEP_NEW_LINE_N);
                        text = text.replaceAll("&amp;", "&");
                        text = text.replaceAll("&quot;", "\"");
                        if (!text.isEmpty()) {
                            if (language.containsKey(textID)) {
                                VJXLogger.log(LogCategory.WARNING, WARNING_TEXT_ID_EXISTS + textID);
                                continue;
                            }
                            language.put(textID, text);
                        }
                    }
                }
            }
        }
    }
}