package de.venjinx.ejx.controllers;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.PositionTracker;

public abstract class PlayerInput {

    protected EJXGame game;
    protected LevelStage level;
    protected MenuStage menu;
    protected InputControl input;

    private boolean ingameControl;

    public abstract void userUpdate(float delta);

    public void init(EJXGame game) {
        this.game = game;
    }

    public void setMenu(MenuStage menu) {
        this.menu = menu;
    }

    public void setLevel(LevelStage level) {
        this.level = level;
    }

    public void update(float delta) {
        userUpdate(delta);
    }

    public void setInput(InputControl input) {
        this.input = input;
    }

    public boolean getIngameControl() {
        return ingameControl;
    }

    public void setIngameControl(boolean on) {
        ingameControl = on;
    }

    public boolean catchGesture(PositionTracker posTracker) {
        return false;
    }
}