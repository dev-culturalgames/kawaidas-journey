package de.venjinx.ejx.controllers;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.PositionTracker;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class InputControl extends InputAdapter {

    protected EJXGame game;
    protected PlayerInput playerInput;

    private int maxPointer;
    private int keyRange;

    private boolean[] touchDown;
    private boolean[] touchDrag;

    private boolean[] keyDown;
    private boolean[] keyPressed;
    private boolean[] keyReleased;

    private int leftPointer = -1;
    private int rightPointer = -1;

    private Vector2[] touchUIPos;
    private Vector2[] touchDragPos;

    private boolean hasScrolled;
    private boolean hasSwiped;
    private int scroll;

    protected MenuStage menuControl;
    private Actor hitActor;
    private boolean hasHitActor;
    private boolean isInit;

    private PositionTracker[] tracker;

    public InputControl(int maxTouchPointer) {
        keyRange = 256;
        maxPointer = maxTouchPointer;

        keyDown = new boolean[keyRange];
        keyPressed = new boolean[keyRange];
        keyReleased = new boolean[keyRange];

        touchDown = new boolean[maxPointer];
        touchDrag = new boolean[maxPointer];

        touchUIPos = new Vector2[maxPointer];
        touchDragPos = new Vector2[maxPointer];

        tracker = new PositionTracker[maxPointer];
        for (int i = 0; i < maxTouchPointer; i++) {
            tracker[i] = new PositionTracker();
            touchUIPos[i] = new Vector2();
            touchDragPos[i] = new Vector2();
        }
    }

    public void update(float delta) {
        if (playerInput != null) playerInput.update(delta);
    }

    public void init(EJXGame g) {
        init(g, null, null);
    }

    public void init(EJXGame g, MenuStage menu, LevelStage level) {
        isInit = true;
        game = g;
        menuControl = menu;

        if (playerInput != null) {
            playerInput.init(game);
            playerInput.setMenu(menu);
            playerInput.setLevel(level);
        }
    }

    public PlayerInput getPlayerInput() {
        return playerInput;
    }

    public void setPlayerInput(PlayerInput playerInput) {
        this.playerInput = playerInput;
        if (playerInput != null) playerInput.setInput(this);
    }

    public void resetInput() {
        resetKeyPressed();
        resetKeyReleased();

        hasScrolled = false;
        hasHitActor = false;
    }

    public void resetSwiped() {
        hasSwiped = false;
    }

    public boolean isKeyDown(int kc) {
        if (kc < keyRange)
            return keyDown[kc];
        return false;
    }

    public boolean isKeyPressed(int kc) {
        if (kc >= keyRange)
            return false;
        return keyPressed[kc];
    }

    public boolean isKeyReleased(int kc) {
        if (kc >= keyRange)
            return false;
        return keyReleased[kc];
    }

    public boolean hasScrolled() {
        return hasScrolled;
    }

    public boolean hasSwiped() {
        return hasSwiped;
    }

    public int getScrollAmount() {
        int s = scroll;
        return s;
    }

    public boolean leftPointer(int pointer) {
        return leftPointer == pointer;
    }

    public boolean leftTouched() {
        return leftPointer != -1;
    }

    public boolean leftDragging() {
        if (leftPointer < 0 || leftPointer >= maxPointer) return false;
        return touchDrag[leftPointer];
    }

    public boolean rightPointer(int pointer) {
        return rightPointer == pointer;
    }

    public boolean rightTouched() {
        return rightPointer != -1;
    }

    public boolean rightDragging() {
        if (rightPointer < 0 || rightPointer >= maxPointer) return false;
        return touchDrag[rightPointer];
    }

    public void setKeyDown(int kc) {
        setKeyDown(kc, true);
    }

    public void setKeyDown(int kc, boolean on) {
        if (kc < keyRange) keyDown[kc] = on;
    }

    public boolean getHasHit() {
        return hasHitActor;
    }

    public Actor getHit() {
        return hitActor;
    }

    @Override
    public boolean keyDown(int keyCode) {
        if (keyCode < keyRange) {
            keyReleased[keyCode] = false;
            keyPressed[keyCode] = true;
            keyDown[keyCode] = true;
        }
        VJXLogger.log(LogCategory.INPUT, "InputControl key down: " + keyCode);
        return super.keyDown(keyCode);
    }

    @Override
    public boolean keyUp(int keyCode) {
        if (keyCode < keyRange) {
            keyReleased[keyCode] = true;
            keyDown[keyCode] = false;
            keyPressed[keyCode] = false;
        }
        VJXLogger.log(LogCategory.INPUT, "InputControl key up: " + keyCode);
        return super.keyUp(keyCode);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (pointer >= maxPointer || !isInit)
            return false;

        touchDown[pointer] = true;
        touchUIPos[pointer].set(screenX, screenY);

        if (menuControl != null) {
            menuControl.screenToStageCoordinates(touchUIPos[pointer]);

            hitActor = menuControl.hit(touchUIPos[pointer].x,
                            touchUIPos[pointer].y, true);
            hasHitActor = hitActor != null;

            if (menuControl.getUI() != null) {
                if (hitActor == menuControl.getUI().getContent()) {
                    if (screenX < menuControl.getViewport().getScreenWidth()
                                    / 2) {
                        if (leftPointer == -1) leftPointer = pointer;
                    } else {
                        if (rightPointer == -1) {
                            tracker[pointer].init(touchUIPos[pointer].x,
                                            touchUIPos[pointer].y);
                            rightPointer = pointer;
                        }
                    }
                }
            }
        }

        VJXLogger.log(LogCategory.INPUT,
                        "InputControl touch down: " + touchUIPos[pointer]);
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (pointer >= maxPointer || !isInit)
            return false;

        boolean handled = false;

        hasSwiped = touchDrag[pointer];
        touchDown[pointer] = false;
        touchDrag[pointer] = false;

        touchUIPos[pointer].set(screenX, screenY);

        if (menuControl != null) {
            menuControl.screenToStageCoordinates(touchUIPos[pointer]);
        }

        if (pointer == leftPointer) {
            leftPointer = -1;
        }

        if (pointer == rightPointer) {
            tracker[rightPointer].stop(touchUIPos[pointer].x,
                            touchUIPos[pointer].y);

            if (tracker[rightPointer].isTracking())
                handled |= catchGesture(tracker[rightPointer]);
            rightPointer = -1;
        }
        handled |= super.touchUp(screenX, screenY, pointer, button);
        VJXLogger.log(LogCategory.INPUT, "InputControl touch up: " + touchUIPos[pointer]);
        return handled;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (pointer >= maxPointer || !isInit)
            return false;

        touchDrag[pointer] = true;
        touchDragPos[pointer].set(screenX, screenY);

        if (menuControl != null) {
            menuControl.screenToStageCoordinates(touchDragPos[pointer]);
        }

        if (pointer == rightPointer) {
            tracker[rightPointer].track(touchDragPos[pointer].x,
                            touchDragPos[pointer].y);
        }

        VJXLogger.log(LogCategory.INPUT, "InputControl dragged: " + touchDragPos[pointer]);
        return super.touchDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean scrolled(int amount) {
        scroll = amount;
        hasScrolled = true;
        VJXLogger.log(LogCategory.INPUT, "InputControl scrolled: " + amount);
        return super.scrolled(amount);
    }

    private boolean catchGesture(PositionTracker posTracker) {
        if (playerInput != null) return playerInput.catchGesture(posTracker);
        return false;
    }

    private void resetKeyPressed() {
        for (int i = 0; i < keyRange; i++) {
            keyPressed[i] = false;
        }
    }

    private void resetKeyReleased() {
        for (int i = 0; i < keyRange; i++) {
            keyReleased[i] = false;
        }
    }
}
