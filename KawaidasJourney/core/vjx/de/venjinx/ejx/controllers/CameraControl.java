package de.venjinx.ejx.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class CameraControl {

    public enum CameraType {
        ORTHO("ortho");

        public final String name;

        private CameraType(String name) {
            this.name = name;
        }

        public static final CameraType get(String name) {
            for (CameraType s : CameraType.values())
                if (s.name.equals(name)) return s;
            VJXLogger.log(LogCategory.ERROR,
                            "Camera type '" + name + "' not found!");
            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static EJXCamera createCamera(MapObject mapObject) {
        EJXCamera cam = new EJXCamera();

        if (mapObject != null)
            cam.configure(mapObject.getProperties());
        return cam;
    }

    public static Viewport createView(MapObject mapObject, Camera camera) {
        if (mapObject == null) return new ScreenViewport(camera);
        MapProperties properties = mapObject.getProperties();

        String type = properties.get("viewport:type", "none", String.class);
        int height = properties.get("viewport:height", -1, Integer.class);
        height = height < 0 ? Gdx.graphics.getHeight() : height;
        int width = properties.get("viewport:width", -1, Integer.class);
        width = width < 0 ? Gdx.graphics.getWidth() : width;
        Viewport viewport = null;
        switch (type) {
            case "extend":
                viewport = new ExtendViewport(width, height, camera);
                break;
            case "fill":
                viewport = new FillViewport(width, height, camera);
                break;
            case "fit":
                viewport = new FitViewport(width, height, camera);
                break;
            case "screen":
                viewport = new ScreenViewport(camera);
                break;
            case "stretch":
                viewport = new StretchViewport(width, height, camera);
                break;
            default:
                viewport = new ScalingViewport(Scaling.none, width, height, camera);
        }
        return viewport;
    }

}