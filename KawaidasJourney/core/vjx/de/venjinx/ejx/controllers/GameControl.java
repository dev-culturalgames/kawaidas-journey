package de.venjinx.ejx.controllers;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.EJXPlayer;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EJXGroup;
import de.venjinx.ejx.entity.EntityDef;
import de.venjinx.ejx.entity.PlayerEntity;
import de.venjinx.ejx.entity.logics.EJXLogics.EJXLogicType;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.vjx.ui.IngameHUD;

public class GameControl {

    private Actor executor;
    private EJXGame game;
    private MenuStage menu;
    private LevelStage level;
    private AudioControl sfx;
    private Vector2 projVec;

    public GameControl(EJXGame game) {
        this.game = game;
        projVec = new Vector2();
        executor = new Actor();
        executor.setName("EntityListener_executor");
        sfx = game.sfx;
    }

    public void act(float delta) {
        executor.act(delta);
    }

    public float execSingleCmd(Command cmd) {
        return execSingleCmd(cmd, 0f);
    }

    public float execSingleCmd(Command cmd, float delay) {
        SequenceAction sa = Actions.sequence();
        float duration = execSingleCmd(cmd, delay, sa);
        executor.addAction(sa);
        return duration;
    }

    public float execSingleCmd(Command cmd, SequenceAction sequence) {
        return execSingleCmd(cmd, 0f, sequence);
    }

    public float execSingleCmd(Command cmd, float delay,
                    SequenceAction sequence) {
        if (cmd == null) return 0f;

        //        VJXLogger.logIncr(LogCategory.EXEC | LogCategory.DETAIL,
        //                        "Entity listener exec single (delay " + delay + "):" + cmd);
        //        VJXLogger.log(LogCategory.EXEC, "Entity listener exec " + cmd.getAction());
        float duration = 0;
        if (delay > 0) {
            sequence.addAction(Actions.delay(delay));
            duration += delay;
        }

        duration += execute(cmd, sequence);

        //        VJXLogger.logDecr(LogCategory.EXEC | LogCategory.DETAIL,
        //                        "Entity listener finished exec " + cmd + ". Runs "
        //                                        + duration + "s");
        return duration;
    }

    public SequenceAction execMultiCmds(Command[] commands) {
        return execMultiCmds(commands, 0f);
    }

    public SequenceAction execMultiCmds(Command[] commands, float delay) {
        SequenceAction sa = Actions.sequence();
        execMultiCmds(commands, sa, delay);
        executor.addAction(sa);
        return sa;
    }

    public SequenceAction execMultiCmds(Command[] commands,
                    SequenceAction sequence) {
        return execMultiCmds(commands, sequence, 0f);
    }

    public SequenceAction execMultiCmds(Command[] commands,
                    SequenceAction sequence, float delay) {
        if (commands == null || commands.length == 0) return sequence;

        VJXLogger.logIncr(LogCategory.EXEC | LogCategory.DETAIL,
                        "Entity listener exec multi commands(delay " + delay
                        + ")");

        float duration = 0f;
        if (delay > 0) {
            sequence.addAction(Actions.delay(delay));
            duration += delay;
        }

        for (Command cmd : commands)
            duration += execSingleCmd(cmd, sequence);

        VJXLogger.logDecr(LogCategory.EXEC | LogCategory.DETAIL,
                        "Entity listener finished multi commands. Runs "
                                        + duration + "s");

        return sequence;
    }

    private float execute(Command command, SequenceAction sequence) {
        if (command != null) return command.execute(sequence);
        return 0f;
    }

    public void clearActions() {
        executor.clearActions();
    }

    public void snapCamToPlatform(float yPos) {
        EJXCamera cam = (EJXCamera) level.getCamera();
        cam.snapToPlatform(yPos);
    }

    public void camCatchYDown(boolean activate) {
        EJXCamera cam = (EJXCamera) level.getCamera();
        cam.catchYDown(activate);
    }

    public void camCatchYUp(boolean activate) {
        EJXCamera cam = (EJXCamera) level.getCamera();
        cam.catchYUp(activate);
    }

    public EJXEntity getEntityByName(String name) {
        return level.getEntityByName(name);
    }

    public Array<EJXEntity> getEntitiesByDefName(String name) {
        Array<EJXEntity> array = new Array<>();
        return level.getEntitiesByContainDefName(name, array);
    }

    public Array<EJXEntity> getEntitiesByLogic(EJXLogicType logic) {
        Array<EJXEntity> array = new Array<>();
        if (level != null)
            return level.getEntitiesByLogic(logic, array);
        else return array;
    }

    public Vector2 parsePoint(String name, Vector2 point) {
        return EJXUtil.parsePosition(name, point, level);
    }

    public PlayerEntity getPlayerEntity() {
        return level.getPlayerEntity();
    }

    public EJXPlayer getPlayer(){
        return game.getPlayer();
    }

    public PlayerInput getPlayerInput() {
        return game.getScreen().getInputControl().getPlayerInput();
    }

    public EJXGame getGame() {
        return game;
    }

    public Vector2 getLevelSize(Vector2 size) {
        return size.set(level.getWorldWidth(), level.getWorldHeight());
    }

    public EJXEntity createEntity(String name) {
        EntityDef def = game.factory.getEntityDef(name);
        if (def == null) {
            VJXLogger.log(LogCategory.ERROR,
                            "definition '" + name + "' not found!");
            return null;
        }
        EJXEntity e = game.factory.createEntity(def, null);
        if (e == null) {
            VJXLogger.log(LogCategory.ERROR,
                            "entity for def '" + name + "' not found!");
            return null;
        }
        return e;
    }

    public void addEntity(EJXEntity entity, EJXGroup layerNode) {
        level.addEntity(entity, layerNode);
    }

    public void addEntity(EJXEntity entity, EJXEntity parent) {
        level.addEntity(entity, parent);
    }

    public void addEntity(EJXEntity entity, EJXEntity parent, boolean pre) {
        level.addEntity(entity, parent, pre);
    }

    public void moveUIItem(String name, float x, float y) {
        projVec.set(x, y);
        level.getViewport().project(projVec);
        menu.getViewport().unproject(projVec);
        projVec.y = menu.getViewport().getWorldHeight() - projVec.y;
        menu.getUI().moveUIItem(name, projVec.x, projVec.y);
    }

    public boolean lvlProgrGreater(float mod) {
        return level.isProgrGreater(mod);
    }

    public void playSfx(String sfxName, boolean loop, float volume, float pitch) {
        sfx.playDynSFX(sfxName, loop, volume, pitch);
    }

    public void stopSfx(String sfxName) {
        sfx.stopSound(sfxName);
    }

    public AudioControl getAudioControl() {
        return sfx;
    }

    public void setLevel(LevelStage level) {
        this.level = level;
    }

    public LevelStage getLevel() {
        return level;
    }

    public EJXGroup getWorldLayer() {
        return level.getScene().findGroup("world");
    }

    public EJXGroup getGroup(String name) {
        return level.getScene().findGroup(name);
    }

    public void setMenu(MenuStage menu) {
        this.menu = menu;
    }

    public MenuStage getMenu() {
        return game.getScreen().getMenu();
    }

    public IngameHUD getUI() {
        return menu.getUI();
    }

    @SuppressWarnings("unchecked")
    public <T> T getUI(Class<T> clazz) {
        return (T) menu.getUI();
    }

    public void addActions(Action a) {
        executor.addAction(a);
    }

}