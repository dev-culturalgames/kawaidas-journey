package de.venjinx.ejx.controllers;

import java.util.HashMap;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.actions.game.FadeMusicAction;
import de.venjinx.ejx.audio.LongSFX;
import de.venjinx.ejx.audio.ShortSFX;
import de.venjinx.ejx.audio.VJXSound;
import de.venjinx.ejx.util.EJXTypes;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class AudioControl implements Disposable {

    //    private static final String NULL_EMPTY = "Audio: Dynamic sfx name null or empty!";
    private static final String UI_SFX = "Audio: UI sfx '";
    private static final String DYNAMIC_SFX = "Audio: Dynamic sfx '";

    //    private static final String NOT_EXIST = "' does not exist!";
    private static final String FILE_NOT_EXIST = "' file does not exist.";

    private static final String ERROR_WRONG_SFX_FORMAT = "Audio: Playing sound class not supported: ";

    private static final String PLAY_VOICE = "Audio play voice: ";
    private static final String STOP_VOICE = "Audio stop voice: ";
    private static final String PLAY_MUSIC = "Audio play music: ";
    private static final String PAUSE_MUSIC = "Audio pause music: ";
    private static final String RESUME_MUSIC = "Audio resume music: ";
    private static final String STOP_MUSIC = "Audio stop music: ";
    private static final String MUTE_MUSIC = "Audio mute music: ";
    private static final String STOP_ALL = "Audio stop all.";
    private static final String LOAD_SOUND = "    Loaded sounds for '";
    private static final String UNLOAD_SOUNDS = "    Unloaded dynamic sounds. New load: ";
    private static final String UNLOAD_ALL_SOUNDS = "    Unloaded dynamic sounds. New load: ";
    private static final String NEW_LOAD = ". New load: ";
    private static final String MB = "mb";

    private static final String PATH_SFX_LEVEL = "sfx/level/";

    private static final String PLAY_SFX = "Audio: Play sfx '";
    private static final String ADD_SFX = "Audio: Add dynamic sfx '";
    private static final String LOOP = "' loop: ";
    private static final String VOLUME = "', volume: ";
    private static final String PITCH = "', pitch: ";
    private static final String FILE = "' - file: '";
    private static final String PROBABILITY = ", prob: ";
    private static final String COUNT = ", count: ";
    private static final String ADDED_TO = " added to ";
    private static final String voice = "voice";

    public static final String MASTER_VOL = "float_master_vol";
    public static final String MUSIC_VOL = "float_music_vol";
    public static final String SOUND_VOL = "float_sound_vol";

    // TODO remove at some point -> custom game stuff
    public static final String SFX_CLICK = "click";
    public static final String MUSIC_MAIN = "music:main";

    private EJXGame game;

    private HashMap<String, Array<VJXSound>> entitySounds;
    private HashMap<String, VJXSound> sounds;
    private HashMap<String, FileHandle> musicFiles;

    private long currentLoad = 0;

    private Music curMusic, curVoice;
    private String curVoiceDir;
    private FileHandle curMusicFile;

    private float masterVolume = 1f;
    private float soundVolume = 1f;
    private float musicVolume = 1f;

    public AudioControl(EJXGame game) {
        this.game = game;
        entitySounds = new HashMap<>();
        sounds = new HashMap<>();
        musicFiles = new HashMap<>();
    }

    public void addUISounds(MapProperties properties) {
        Iterator<String> keys = properties.getKeys();
        String key;
        while (keys.hasNext()) {
            key = keys.next();

            boolean isSfx = key.contains(VJXString.PRE_sfx);
            isSfx |= key.contains(VJXString.PRE_ambient);

            if (!isSfx || isProp(key))
                continue;

            Object o = properties.get(key);
            if (o == null || !(o instanceof FileHandle))
                continue;

            FileHandle file = (FileHandle) o;
            if (!file.exists() || file.name().isEmpty()) {
                VJXLogger.log(LogCategory.WARNING, UI_SFX + key + FILE_NOT_EXIST, 1);
                continue;
            }

            ShortSFX sound = new ShortSFX(key, file);
            loadSFXProperties(sound, properties, key);
            currentLoad += sound.load();
            sounds.put(key, sound);
            VJXLogger.log(LogCategory.SFX,
                            "Audio: Global sfx '" + key
                            + "' added. File: '"
                            + file.pathWithoutExtension()
                            + "'. Size: "
                            + EJXTypes.byteToMB(sound.getSize(), 2)
                            + "mb -> new load: "
                            + EJXTypes.byteToMB(currentLoad, 2)
                            + "mb");
        }
    }

    public void addMusic(String name, FileHandle file) {
        if (musicFiles.containsKey(name)) {
            VJXLogger.log(LogCategory.INFO,
                            "Audio: Music '" + name + "' already exists!");
        } else musicFiles.put(name, file);
    }

    public void addDynamicSounds(String defName, MapProperties properties) {
        Array<VJXSound> sounds;
        if (!entitySounds.containsKey(defName)) {
            sounds = new Array<>();
            entitySounds.put(defName, sounds);
        } else sounds = entitySounds.get(defName);

        String key;
        Iterator<String> iter = properties.getKeys();
        while (iter.hasNext()) {
            key = iter.next();

            boolean isSfx = key.contains(VJXString.PRE_sfx);
            isSfx |= key.contains(VJXString.PRE_ambient);

            if (!isSfx || isProp(key))
                continue;

            Object o = properties.get(key);
            if (o == null || !(o instanceof FileHandle)) continue;

            FileHandle file = (FileHandle) o;
            if (!file.exists() || file.name().isEmpty()) {
                VJXLogger.log(LogCategory.WARNING, DYNAMIC_SFX + key + FILE_NOT_EXIST, 1);
                continue;
            }
            addDynamicSound(defName, key, file, properties, sounds);
        }
    }

    public Array<VJXSound> getSounds(String defName) {
        return entitySounds.get(defName);
    }

    public VJXSound getSound(String name) {
        return sounds.get(name);
    }

    public long playDynSFX(String name) {
        return playDynSFX(name, false, -1, -1, 0);
    }

    public long playDynSFX(String name, boolean loop) {
        return playDynSFX(name, loop, -1, -1, 0);
    }

    public long playDynSFX(String name, float volume) {
        return playDynSFX(name, false, volume, -1, 0);
    }

    public long playDynSFX(String name, boolean loop, float volume) {
        return playDynSFX(name, loop, volume, -1, 0);
    }

    public long playDynSFX(String name, boolean loop, float volume, float pitch) {
        return playDynSFX(name, loop, volume, pitch, 0);
    }

    public long playDynSFX(String name, boolean loop, float volume,
                    float pitch, float pan) {
        if (name == null || name.isEmpty()) return -1;
        if (!sounds.containsKey(name)) return -1;
        return playDynSFX(sounds.get(name), loop, volume, pitch, pan);
    }

    public long playDynSFX(VJXSound sound, boolean loop, float volume,
                    float pitch, float pan) {
        if (sound == null) return -1;
        if (soundVolume * masterVolume == 0f) return -1;
        if (!sounds.containsKey(sound.getName())) return -1;

        if (!(sound instanceof ShortSFX)) {
            VJXLogger.log(LogCategory.ERROR, ERROR_WRONG_SFX_FORMAT + sound.getClass());
            return -1;
        }

        ShortSFX sfx = (ShortSFX) sound;

        if (volume < 0)
            volume = sfx.getVolume();
        if (pitch < 0)
            pitch = sfx.getPitch();

        VJXLogger.log(LogCategory.SFX, PLAY_SFX + sound.getName()
        + LOOP + (loop || sfx.isLooping()) + VOLUME + volume
        + PITCH + pitch, 3);

        if (loop || sfx.isLooping()) {
            return sfx.loop(volume * soundVolume * masterVolume, pitch);
        }
        return sfx.play(volume * soundVolume * masterVolume, pitch);
    }

    public void pauseEntitySounds() {
        for (VJXSound sfx : sounds.values()) {
            sfx.pause();
        }
    }

    public void resumeEntitySounds() {
        for (VJXSound sfx : sounds.values()) {
            sfx.resume();
        }
    }

    public void stopSound(String name) {
        stopSound(name, -1);
    }

    public void stopSound(String name, long id) {
        if (!sounds.containsKey(name)) return;

        VJXSound s = sounds.get(name);
        if (id != -1 && s instanceof ShortSFX) {
            ((ShortSFX) sounds.get(name)).stop(id);
            return;
        }
        s.stop();
    }

    public void playVoiceOver(String name) {
        if (name == null || name.isEmpty())
            return;
        String file = PATH_SFX_LEVEL + name + VJXString.FILE_MP3;

        if (soundVolume * masterVolume == 0f)
            return;

        if (!file.equals(curVoiceDir)) {
            VJXLogger.log(LogCategory.SFX, PLAY_VOICE + file);

            if (curVoice != null) {
                curVoice.stop();
                curVoice.dispose();
            }
            curVoiceDir = file;
            curVoice = Gdx.audio.newMusic(Gdx.files.internal(file));
            curVoice.setLooping(false);
            curVoice.setVolume(.5f * soundVolume * masterVolume);
            curVoice.play();
        }
    }

    public void stopVoiceOver() {
        if (curVoice != null) {
            VJXLogger.log(LogCategory.SFX, STOP_VOICE + curVoiceDir);
            curVoice.stop();
            curVoice.dispose();
        }
        curVoiceDir = null;
    }

    public Action playMusic(String name) {
        return playMusic(musicFiles.get(name));
    }

    public Action playMusic(FileHandle file) {
        ParallelAction pa = Actions.parallel();
        if (file == null || !isMusicOn()) return pa;

        if (curMusicFile != file) {
            stopMusic();
        }

        curMusicFile = file;
        if (curMusic == null) curMusic = Gdx.audio.newMusic(curMusicFile);

        if (!curMusic.isPlaying()) {
            VJXLogger.log(LogCategory.SFX, PLAY_MUSIC + curMusicFile);
            curMusic.setLooping(true);
            curMusic.play();

            FadeMusicAction fma = EJXActions.fadeMusic(curMusic, true, 2f, game);
            pa.addAction(fma);
            curMusic.setVolume(musicVolume * masterVolume);
        }
        return pa;
    }

    public void pauseMusic() {
        if (curMusic != null) {
            VJXLogger.log(LogCategory.SFX, PAUSE_MUSIC + curMusicFile);
            curMusic.pause();
        }
    }

    public void stopMusic() {
        if (curMusic != null) {
            VJXLogger.log(LogCategory.SFX, STOP_MUSIC + curMusicFile);
            curMusic.stop();
            curMusic.dispose();
            curMusic = null;
        }
    }

    public void resumeMusic() {
        VJXLogger.log(LogCategory.SFX, RESUME_MUSIC + curMusicFile);
        if (curMusicFile != null) {
            playMusic();
        }
    }

    public void muteMusic() {
        if (curMusic != null) {
            VJXLogger.log(LogCategory.SFX, MUTE_MUSIC + curMusicFile);
            curMusic.setVolume(0);
        }
    }

    public void stopAllSounds() {
        VJXLogger.log(LogCategory.SFX, STOP_ALL);
        stopVoiceOver();

        for (VJXSound s : sounds.values()) {
            if (s.getName().contains(SFX_CLICK)) continue;
            s.stop();
        }
    }

    public void loadEntitySounds(String defName) {
        Array<VJXSound> sounds = entitySounds.get(defName);
        if (sounds != null) {
            for (VJXSound sfx : sounds) {
                currentLoad += sfx.load();
            }
            VJXLogger.log(LogCategory.SFX, LOAD_SOUND + defName + NEW_LOAD
                            + EJXTypes.byteToMB(currentLoad, 2) + MB);
        }
    }

    public void unloadEntitySounds() {
        for (Array<VJXSound> sounds : entitySounds.values()) {
            for (VJXSound sfx : sounds) {
                currentLoad -= sfx.unload();
            }
        }
        VJXLogger.log(LogCategory.SFX, UNLOAD_SOUNDS + EJXTypes.byteToMB(currentLoad, 2) + MB);
    }

    public void unloadAllSounds() {
        for (VJXSound sfx : sounds.values()) {
            currentLoad -= sfx.unload();
        }
        sounds.clear();
        VJXLogger.log(LogCategory.SFX, UNLOAD_ALL_SOUNDS + EJXTypes.byteToMB(currentLoad, 2) + MB);
    }

    @Override
    public void dispose() {
        stopMusic();
        stopVoiceOver();
        stopAllSounds();
        unloadAllSounds();
    }

    public void updateMusic() {
        // for music notes level
    }

    public float getMasterVolume() {
        return masterVolume;
    }

    public float getSoundVolume() {
        return soundVolume;
    }

    public float getMusicVolume() {
        return musicVolume;
    }

    public boolean isMasterOn() {
        return getMasterVolume() != 0;
    }

    public boolean isSoundOn() {
        return getSoundVolume() != 0;
    }

    public boolean isMusicOn() {
        return getMusicVolume() != 0;
    }

    public void setMasterVolume(float value) {
        masterVolume = value;
        updateMusicVolume();
    }

    public void setSoundVolume(float value) {
        soundVolume = value;
    }

    public void setMusicVolume(float value) {
        musicVolume = value;
        updateMusicVolume();
    }

    public void resetVolume() {
        setMasterVolume(1f);
        setSoundVolume(1f);
        setMusicVolume(1f);
    }

    private void addDynamicSound(String defName, String key, FileHandle file,
                    MapProperties properties, Array<VJXSound> sounds) {

        if (key.contains(voice)) LevelUtil.addVoice(defName, key);
        String uName = defName + VJXString.SEP_DDOT + key;
        VJXSound s;
        long size = file.length();
        if (size < EJXTypes.ONE_MB) {
            s = new ShortSFX(uName, file);
        } else s = new LongSFX(uName, file);

        loadSFXProperties(s, properties, key);

        sounds.add(s);
        this.sounds.put(uName, s);
        VJXLogger.log(LogCategory.SFX, ADD_SFX + uName + FILE + file.path()
        + LOOP + s.isLooping() + VOLUME + s.getVolume() + PITCH
        + s.getPitch() + PROBABILITY + s.getPropability()
        + COUNT + s.getCount() + ADDED_TO + defName);
    }

    private void loadSFXProperties(VJXSound sfx, MapProperties properties,
                    String key) {
        float volume = properties.get(key + VJXString.POST_vol, 1f, Float.class);
        float pitch = properties.get(key + VJXString.POST_pitch, 1f, Float.class);
        boolean loop = properties.get(key + VJXString.POST_loop, false, Boolean.class);
        float probability = properties.get(key + VJXString.POST_prob, 1f, Float.class);
        int count = properties.get(key + VJXString.POST_count, -1, Integer.class);

        sfx.setVolume(volume);
        sfx.setPitch(pitch);
        sfx.setLoop(loop);
        sfx.setPropability(probability);
        sfx.setCount(count);
    }

    private Action playMusic() {
        if (curMusicFile == null)
            curMusicFile = musicFiles.get(MUSIC_MAIN);
        return playMusic(curMusicFile);
    }

    private boolean isProp(String key) {
        boolean isProp = key.contains(VJXString.POST_prob);
        isProp |= key.contains(VJXString.POST_loop);
        isProp |= key.contains(VJXString.POST_vol);
        isProp |= key.contains(VJXString.POST_pitch);
        isProp |= key.contains(VJXString.POST_count);
        return isProp;
    }

    private void updateMusicVolume() {
        if (curMusic != null) curMusic.setVolume(masterVolume * musicVolume);
    }
}