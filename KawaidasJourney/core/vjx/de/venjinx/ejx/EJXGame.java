/*******************************************************************************
 * Copyright 2020 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package de.venjinx.ejx;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.venjinx.ejx.cmds.ActivityCommand;
import de.venjinx.ejx.cmds.AlphaCommand;
import de.venjinx.ejx.cmds.AnimCommand;
import de.venjinx.ejx.cmds.BindCommand;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.cmds.CommandParser;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.cmds.DelayCommand;
import de.venjinx.ejx.cmds.DespawnCommand;
import de.venjinx.ejx.cmds.EnableCommand;
import de.venjinx.ejx.cmds.GhostCommand;
import de.venjinx.ejx.cmds.LifecycleCommand;
import de.venjinx.ejx.cmds.LoadLevelCommand;
import de.venjinx.ejx.cmds.MoveCommand;
import de.venjinx.ejx.cmds.MoveToCommand;
import de.venjinx.ejx.cmds.OrientationCommand;
import de.venjinx.ejx.cmds.PlaySoundCommand;
import de.venjinx.ejx.cmds.SetCamAreaCommand;
import de.venjinx.ejx.cmds.SetCamFocusCommand;
import de.venjinx.ejx.cmds.SetPosCommand;
import de.venjinx.ejx.cmds.SpawnCommand;
import de.venjinx.ejx.cmds.SwitchMenuCommand;
import de.venjinx.ejx.cmds.ToggleUICommand;
import de.venjinx.ejx.cmds.WPCommand;
import de.venjinx.ejx.controllers.AudioControl;
import de.venjinx.ejx.controllers.CameraControl;
import de.venjinx.ejx.controllers.CollisionControl;
import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.entity.EntityFactory;
import de.venjinx.ejx.entity.component.ActorComponent;
import de.venjinx.ejx.entity.component.CommandComponent;
import de.venjinx.ejx.entity.component.EJXComponent;
import de.venjinx.ejx.entity.logics.AmbientLogic;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.CallbackSpawnLogic;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics;
import de.venjinx.ejx.entity.logics.EJXLogics.EJXLogicType;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.LightLogic;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.entity.logics.SpawnLogic;
import de.venjinx.ejx.save.CloudSaveHandler;
import de.venjinx.ejx.save.GameSettings;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.stages.DebugStage;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.tiled.EJXTiledMap;
import de.venjinx.ejx.tiled.TiledFactory;
import de.venjinx.ejx.util.EJXGameService;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXGraphix;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.ejx.util.loader.EJXLoader;
import de.venjinx.ejx.util.loader.FinalTask;
import de.venjinx.ejx.util.loader.TMXFileTask;

/**
 * A {@link Game} implementation that is the base for the LibGDX addon EnJinX.
 * It provides additional functionality for the use of various game components
 * and configurations using <a href="https://www.mapeditor.org/">Tiled</a> map
 * editor and <a href="http://de.esotericsoftware.com/">Spine</a> animations.
 *
 * @author Torge Rothe (X-Ray-Jin, xrayjin@gmx.de )
 *
 */
public abstract class EJXGame extends Game {

    public static final String VJXGame = "VenJinX Base Game";
    public static final String layer_components = "components";
    public static final String layer_splash = "splash";

    public static final String game_title = "game:title";

    public enum GameProperty implements EJXPropertyDefinition {
        title(game_title, PropertyType.stringProp, VJXGame);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private GameProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final SourceProperty get(String name) {
            for (SourceProperty property : SourceProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private static final String settings = "settings";
    private static final String settings_name = "settings:name";
    private static final String settings_subdir = "settings:subdir";

    public static final String LOCALE_ID = "string_localeID";

    public static final String EJX_GAMESETTINGS = "ejx_game_settings";
    public static final String SPLASH = "splash";

    public static long gameFrameNr = 0;

    private String[] args;
    private GameSettings gameSettings;

    protected EJXPlayer player;

    public EJXLoader loader;
    public EJXAssetManager assets;
    public TiledFactory tFactory;
    public EntityFactory factory;
    public GameControl gameControl;
    public CollisionControl collisionControl;
    public AudioControl sfx;
    public Batch batch;
    public ShapeRenderer shapeRenderer;

    private EJXTiledMap gameConfig;
    protected EJXGameScreen gameScreen;

    private long tmpTime = 0;
    protected long timePlayed = 0;

    private boolean devMode = false;
    private boolean initialized = false;
    private boolean running = false;

    private Color clrColor;
    private OrthographicCamera displayCam;
    private Viewport displayView;

    private FinalTask finalTask;

    /**
     * Creates a new {@link EJXGame} instance. Available parameters:<br>
     *
     * -[pathToLevel] : loads the specified level directly after game creation
     *
     * @param args
     *             runtime parameters
     */
    public EJXGame(String[] args) {
        this.args = args;
        assets = new EJXAssetManager();
        loader = new EJXLoader(assets);
        sfx = new AudioControl(this);
        collisionControl = new CollisionControl();
        factory = new EntityFactory(this);
        gameControl = new GameControl(this);
    }

    @Override
    public void create() {
        VJXLogger.logIncr(LogCategory.INIT | LogCategory.FLOW,
                        "Base game creation started...");

        tmpTime = System.currentTimeMillis();
        timePlayed = 0l;

        Gdx.input.setInputProcessor(new InputMultiplexer());
        Gdx.input.setCatchKey(Keys.BACK, true);

        final FileHandle file = Gdx.files.internal("data/game.tmx");

        tFactory = new TiledFactory(this, loader);
        if (file != null && file.exists()) {
            tFactory.loadEntityDefinitions();
            loader.finishLoading();

            TMXFileTask tmxTask = tFactory.newMapTask(file, true);
            loader.finishLoading();

            gameConfig = tmxTask.getMap();
            loadGameConfig(gameConfig);
        } else {
            Gdx.graphics.setTitle(getClass().getSimpleName());
            batch = new PolygonSpriteBatch();
            shapeRenderer = new ShapeRenderer();
            shapeRenderer.setAutoShapeType(true);
            VJXGraphix.spineRenderer.setPremultipliedAlpha(true);
            displayCam = CameraControl.createCamera(null);
            displayView = CameraControl.createView(null, displayCam);

            initSettings(null);
        }

        userCreate();

        registerComponents();
        registerLogics();
        registerCommands();

        tmpTime = System.currentTimeMillis() - tmpTime;
        VJXLogger.logDecr(LogCategory.INIT | LogCategory.FLOW,
                        "Base game creation finished. Took " + tmpTime + "ms.");
        VJXLogger.log(LogCategory.INIT | LogCategory.FLOW, VJXString.STR_EMPTY);
    }

    @Override
    public void render() {
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl20.glBlendFunc(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);

        float delta = Math.min(Gdx.graphics.getDeltaTime(), 1 / 20f);
        VJXLogger.logIncr(LogCategory.FLOW,
                        "Base game main update loop at frame #" + gameFrameNr
                        + " started updating. Delta time: "
                        + delta);
        timePlayed += delta;
        loader.process(50);
        if (running) gameLoop(delta);
        else initLoop();

        VJXLogger.logDecr(LogCategory.FLOW,
                        "Base game main update loop at frame #" + gameFrameNr
                        + " finished updating.");
        gameFrameNr++;
    }

    /**
     * Called after the {@link #create()} method finished and this
     * {@link EJXGame} has been created successfully. Using this to create the
     * game assures that all base components are initialized.
     */
    protected abstract void userCreate();

    /**
     * Overwrite this to register custom {@link EJXLogic EJXLogics}.
     *
     * @see EJXLogics#registerLogic(EJXLogicType, Class)
     */
    protected abstract void registerCustomLogics();

    /**
     * Overwrite this to register custom {@link Command EJXCommands}.
     *
     * @see CommandParser#registerCommand(CMD, Class)
     */
    protected abstract void registerCustomCommands();

    /**
     *
     * @return the currently used savegame.
     */
    public abstract LocalSavegame getSave();

    /**
     * Called when the game is reset. Use this to reset the savegame state.
     */
    public abstract void resetSave();

    public abstract CloudSaveHandler getCloudHandler();

    public abstract EJXGameService getGameService();

    public abstract float getPercentProgress();

    public abstract long getProgress();

    /**
     * Called when all base game components have been loaded. If a level path
     * has been passed to the game as a runtime parameter and it is supposed to
     * be loaded instead of entering the main menu it can be done here.
     *
     * @param loadLevel true if a level should be loaded
     */
    public abstract void gameLoaded(boolean loadLevel);

    /**
     * Called once after game creation finished. Initializes the default
     * {@link Game} functionality if no {@link EJXGameScreen} is provided and
     * sets the game into running state.
     */
    private void initLoop() {
        if (gameScreen == null) {
            VJXLogger.logIncr(LogCategory.INIT | LogCategory.FLOW,
                            "Base game initializing...");
            tmpTime = System.currentTimeMillis();
            initialized = true;
            gameLoaded(false);
            if (screen != null) super.render();

            tmpTime = System.currentTimeMillis() - tmpTime;
            VJXLogger.logDecr(LogCategory.INIT | LogCategory.FLOW,
                            "Base game initialized. Took " + tmpTime + "ms.");
            VJXLogger.log(LogCategory.INIT | LogCategory.FLOW, VJXString.STR_EMPTY);
            return;
        }

        if (!initialized && !loader.isActive()) {
            VJXLogger.logIncr(LogCategory.INIT | LogCategory.FLOW,
                            "Base game initializing...");
            tmpTime = System.currentTimeMillis();
            gameScreen.getLoader().setBackground(assets.getDrawable(EJXGame.SPLASH));

            initialized = true;
            gameScreen.render(0f);

            tmpTime = System.currentTimeMillis() - tmpTime;
            VJXLogger.logDecr(LogCategory.INIT | LogCategory.FLOW,
                            "Base game initialized. Took " + tmpTime + "ms.");
            VJXLogger.log(LogCategory.INIT | LogCategory.FLOW, VJXString.STR_EMPTY);
            return;
        }

        if (initialized && !loader.isActive()) {
            if (finalTask == null) {
                VJXLogger.logIncr(LogCategory.LOAD,
                                "Game screen load menus...");
                gameScreen.getDebugStage().load();
                finalTask = gameScreen.loadMenus();

                if (args != null && args.length != 0 && !args[0].equals("false")
                                && !args[0].equals("true")) {
                    String path = args[0].replace("\\", "/");
                    gameScreen.loadLevel(path, false);
                    gameScreen.getDebugStage().getDebugUI().resetStats();
                }
            } else if (finalTask.isFinished()) {
                running = true;
                finalTask = null;

                tmpTime = System.currentTimeMillis() - tmpTime;
                VJXLogger.logDecr(LogCategory.LOAD, "Game screen loaded menus.");
                VJXLogger.log(LogCategory.LOAD, "Game running...");
                VJXLogger.log(LogCategory.LOAD, VJXString.STR_EMPTY);
            }
        }
        gameScreen.render(0f);
    }

    /**
     * Configures the game from an {@link EJXTiledMap}.
     *
     * @param gameConfig the game configuration map object.
     */
    private void loadGameConfig(EJXTiledMap gameConfig) {
        this.gameConfig = gameConfig;
        MapProperties properties = gameConfig.getProperties();

        String title = getClass().getSimpleName();
        title = properties.get(GameProperty.title.name, title, String.class);
        Gdx.graphics.setTitle(title);

        initComponents(gameConfig.getLayers().get("components").getObjects());

        initSettings(properties);
    }

    /**
     * Initializes the game components.
     *
     * @param components a list of game components
     */
    private void initComponents(MapObjects components) {
        MapObject mo = components.get("display_config");
        if (mo != null) {
            setupDisplay(mo.getProperties());

            batch = new PolygonSpriteBatch();
            shapeRenderer = new ShapeRenderer();
            shapeRenderer.setAutoShapeType(true);
            VJXGraphix.spineRenderer.setPremultipliedAlpha(true);
        }

        mo = components.get("uiCam");
        displayCam = CameraControl.createCamera(mo);

        mo = components.get("uiView");
        displayView = CameraControl.createView(mo, displayCam);
    }

    /**
     * Initializes the main display and configures it.
     *
     * @param properties the display properties
     */
    private void setupDisplay(MapProperties properties) {
        clrColor = properties.get("display:clearColor", Color.class);
        Gdx.gl20.glClearColor(clrColor.r, clrColor.g, clrColor.b, clrColor.a);

        Gdx.graphics.setResizable(properties.get("display:resizable", true,
                        Boolean.class));
        Gdx.graphics.setVSync(properties.get("display:vSyncEnabled", true,
                        Boolean.class));

        int height = properties.get("display:height", Gdx.graphics.getHeight(), Integer.class);
        int width = properties.get("display:width", Gdx.graphics.getWidth(), Integer.class);
        if (properties.get("display:fullscreen", false, Boolean.class)) {
            DisplayMode bestMode = null;
            DisplayMode[] modes = Gdx.graphics.getDisplayModes();
            for (DisplayMode mode : modes) {
                if (mode.width == width && mode.height == height) {
                    if (bestMode == null || bestMode.refreshRate < Gdx.graphics
                                    .getDisplayMode().refreshRate) {
                        bestMode = mode;
                    }
                }
            }
            if (bestMode == null) {
                bestMode = Gdx.graphics.getDisplayMode();
            }
            Gdx.graphics.setFullscreenMode(bestMode);
        } else {
            Gdx.graphics.setWindowedMode(width, height);
        }
    }

    /**
     * Initializes the {@link GameSettings} of this game.
     *
     * @param properties settings properties
     */
    private void initSettings(MapProperties properties) {
        String className = getClass().getSimpleName();
        String subdir = className + VJXString.SEP_SLASH;
        String settingsName = className + VJXString.SEP_USCORE + settings;
        if (properties != null) {
            subdir = properties.get(settings_subdir, subdir, String.class);
            settingsName = properties.get(settings_name, settingsName, String.class);
        }
        if (Gdx.app.getType() == ApplicationType.Desktop)
            GameSettings.subDir = subdir;
        gameSettings = new GameSettings(settingsName);
    }

    /**
     * Registers EnJinX base {@link VJXLogicType logic types}. Calls abstract
     * {@link #registerCustomLogics()} to register custom {@link EJXLogic
     * logics}.
     */
    private void registerComponents() {
        EJXComponent.registerLogic(VJXLogicType.ACTOR, ActorComponent.class);
        EJXComponent.registerLogic(VJXLogicType.COMMAND, CommandComponent.class);
    }

    /**
     * Registers EnJinX base {@link VJXLogicType logic types}. Calls abstract
     * {@link #registerCustomLogics()} to register custom {@link EJXLogic
     * logics}.
     */
    private void registerLogics() {
        EJXLogics.registerLogic(VJXLogicType.TEMPLATE, AmbientLogic.class);
        EJXLogics.registerLogic(VJXLogicType.AMBIENT, AmbientLogic.class);
        EJXLogics.registerLogic(VJXLogicType.BOX2D, Box2DLogic.class);
        EJXLogics.registerLogic(VJXLogicType.END_SPAWN, CallbackSpawnLogic.class);
        EJXLogics.registerLogic(VJXLogicType.LIGHT, LightLogic.class);
        EJXLogics.registerLogic(VJXLogicType.MOVE, MoveLogic.class);
        EJXLogics.registerLogic(VJXLogicType.SPAWN, SpawnLogic.class);
        registerCustomLogics();
    }

    /**
     * Registers EnJinX base {@link CMD command types}. Calls abstract
     * {@link #registerCustomCommands()} to register custom {@link Command
     * commands}.
     */
    private void registerCommands() {
        CommandParser.registerCommand(CMD.ACTIVITY, ActivityCommand.class);
        CommandParser.registerCommand(CMD.ADD_POS, SetPosCommand.class);
        CommandParser.registerCommand(CMD.ALPHA, AlphaCommand.class);
        CommandParser.registerCommand(CMD.ANIMATION, AnimCommand.class);
        CommandParser.registerCommand(CMD.BIND, BindCommand.class);
        CommandParser.registerCommand(CMD.DELAY, DelayCommand.class);
        CommandParser.registerCommand(CMD.DESPAWN, DespawnCommand.class);
        CommandParser.registerCommand(CMD.DESPAWN_CHILDREN, DespawnCommand.class);
        CommandParser.registerCommand(CMD.DISABLE, EnableCommand.class);
        CommandParser.registerCommand(CMD.ENABLE, EnableCommand.class);
        CommandParser.registerCommand(CMD.LIFECYCLE, LifecycleCommand.class);
        CommandParser.registerCommand(CMD.LOAD_LEVEL, LoadLevelCommand.class);
        CommandParser.registerCommand(CMD.MOVE, MoveCommand.class);
        CommandParser.registerCommand(CMD.MOVE_TO, MoveToCommand.class);
        CommandParser.registerCommand(CMD.NEXT_WP, WPCommand.class);
        CommandParser.registerCommand(CMD.PLAY_SFX, PlaySoundCommand.class);
        CommandParser.registerCommand(CMD.SET_CAM_FOCUS, SetCamFocusCommand.class);
        CommandParser.registerCommand(CMD.SET_CAM_AREA, SetCamAreaCommand.class);
        CommandParser.registerCommand(CMD.SET_CAM_AREA_MIN_X, SetCamAreaCommand.class);
        CommandParser.registerCommand(CMD.SET_CAM_AREA_MAX_X, SetCamAreaCommand.class);
        CommandParser.registerCommand(CMD.SET_CAM_AREA_MIN_Y, SetCamAreaCommand.class);
        CommandParser.registerCommand(CMD.SET_CAM_AREA_MAX_Y, SetCamAreaCommand.class);
        CommandParser.registerCommand(CMD.SET_GHOST, GhostCommand.class);
        CommandParser.registerCommand(CMD.SET_ORIENTATION, OrientationCommand.class);
        CommandParser.registerCommand(CMD.SET_POS, SetPosCommand.class);
        CommandParser.registerCommand(CMD.SET_WP, WPCommand.class);
        CommandParser.registerCommand(CMD.SPAWN, SpawnCommand.class);
        CommandParser.registerCommand(CMD.STOP, MoveCommand.class);
        CommandParser.registerCommand(CMD.STOP_SFX, PlaySoundCommand.class);
        CommandParser.registerCommand(CMD.SWITCH_MENU, SwitchMenuCommand.class);
        CommandParser.registerCommand(CMD.TOGGLE_UI, ToggleUICommand.class);
        CommandParser.registerCommand(CMD.UNBIND, BindCommand.class);
        CommandParser.registerCommand(CMD.UNLOAD_LEVEL, LoadLevelCommand.class);
        registerCustomCommands();
    }

    /**
     * Updates and then renders a frame based on the current game state. If no
     * {@link EJXGameScreen} is used the default {@link Game} behavior is used.
     * If no {@link Screen} is provided at all the splash image from the game
     * configuration is drawn. In that case this function can be overwritten to
     * do the custom rendering.
     *
     * @param delta time passed since last frame
     */
    private void gameLoop(float delta) {
        if (gameScreen != null) {
            // enjinx update and render loop
            gameScreen.update(delta);
            gameScreen.render(delta);
        } else {
            if (screen != null) {
                // default libgdx game loop
                super.render();
            } else {
                // custom loop
                batch.begin();
                Texture t = assets.getTexture(SPLASH);
                if (t != null)
                    batch.draw(t, 0, 0);
                batch.end();
            }
        }

        //            VJXLogger.log("calls " + GLProfiler.calls);
        //            VJXLogger.log("draw calls " + GLProfiler.drawCalls);
        //            VJXLogger.log("switches " + GLProfiler.shaderSwitches);
        //            VJXLogger.log("bindings " + GLProfiler.textureBindings);
        //            VJXLogger.log("vertices " + GLProfiler.vertexCount.count);
        //            GLProfiler.reset();
    }

    /**
     * Sets the game into developer mode. This enables additional debug tools
     * and can be used to activate or enable custom game elements for debugging
     * purposes. A recommended way of using this would be to have two launch
     * configurations. One in development mode and one in release mode.
     *
     * @param devMode set true to use development mode
     */
    public void setDevMode(boolean devMode) {
        this.devMode = devMode;
    }

    /**
     * @return true if game is in development mode
     */
    public boolean isDevMode() {
        return devMode;
    }

    /**
     * @return the game configuration object
     */
    public EJXTiledMap getConfig() {
        return gameConfig;
    }

    /**
     * @return the game settings object
     */
    public GameSettings getSettings() {
        return gameSettings;
    }

    /**
     * @return the current player
     */
    public EJXPlayer getPlayer() {
        return player;
    }

    /**
     * Gets the main display camera. This camera is used as the default camera
     * for a {@link MenuStage} if one is used. The camera can be configured in
     * game configuration file.
     *
     * @return the main display camera
     */
    public OrthographicCamera getDisplayCamera() {
        return displayCam;
    }

    /**
     * Gets the main display viewport. The viewport uses the {@link #displayCam}
     * as default camera. The viewport can be configured in game configuration
     * file.
     *
     * @return the main display viewport
     */
    public Viewport getDisplayView() {
        return displayView;
    }

    /**
     * @return the current game frame number
     */
    public long getFrameNr() {
        return gameFrameNr;
    }

    /**
     * Saves the current game {@link GameSettings settings} to disk.
     */
    public void saveSettings() {
        gameSettings.putFloat(AudioControl.MASTER_VOL, sfx.getMasterVolume());
        gameSettings.putFloat(AudioControl.SOUND_VOL, sfx.getSoundVolume());
        gameSettings.putFloat(AudioControl.MUSIC_VOL, sfx.getMusicVolume());
        gameSettings.putString(LOCALE_ID, TextControl.getInstance()
                        .getCurrentLocale().toString());
    }

    /**
     * Loads game {@link GameSettings settings} from disk.
     */
    public void loadSettings() {
        sfx.setMasterVolume(gameSettings.getFloat(AudioControl.MASTER_VOL, 1f));
        sfx.setSoundVolume(gameSettings.getFloat(AudioControl.SOUND_VOL, 1f));
        sfx.setMusicVolume(gameSettings.getFloat(AudioControl.MUSIC_VOL, 1f));
        // TODO put back in for release. load saved or default language
        //        TextControl.getInstance().setLocale(gameSettings.getString(LOCALE_ID,
        //                        Locale.getDefault().toString()));
        getScreen().getMenu().updateLanguage();
    }

    /**
     * Sets the game into debug mode. This enables additional visual debug
     * information like bounding boxes and collision boxes.
     *
     * @param debug set true to show visual debug information
     *
     * @see DebugStage
     */
    public void setDebug(boolean debug) {
        gameScreen.setDebug(debug);
    }

    /**
     * @return the time the game is running
     */
    public long getTimePlayed() {
        return timePlayed;
    }

    /**
     * Gets the default game assets path.
     *
     * @param raw whether to return the rawAssets root folder
     * @return the game assets root folder
     */
    public static String getAssetsPath(boolean raw) {
        String usrDir = System.getProperty("user.dir");
        if (raw) {
            if (usrDir.contains("desktop")) {
                usrDir = "../android/rawAssets/";
                return usrDir;
            }
            if (usrDir.contains("assets")) {
                usrDir = "../rawAssets/";
                return usrDir;
            }
        } else {
            if (usrDir.contains("desktop")) {
                usrDir = "../android/assets/";
                return usrDir;
            }
            if (usrDir.contains("assets")) {
                usrDir = "../assets/";
                return usrDir;
            }
        }
        return usrDir;
    }

    @Override
    public EJXGameScreen getScreen() {
        return (EJXGameScreen) super.getScreen();
    }

    @Override
    public void resize(int width, int height) {
        if (gameScreen != null) gameScreen.resize(width, height);
        displayView.update(width, height, true);
        displayView.apply();
    }

    @Override
    public void dispose() {
        VJXLogger.logIncr(LogCategory.INIT | LogCategory.LOAD | LogCategory.FLOW,
                        "Base game disposing.");
        super.dispose();
        batch.dispose();
        sfx.dispose();

        if (gameConfig != null) {
            assets.unloadResource(SPLASH);
        }

        if (gameScreen != null) {
            gameScreen.dispose();
        }

        tFactory.dispose();
        assets.finishLoading();
        assets.dispose();


        VJXLogger.logDecr(LogCategory.INIT | LogCategory.LOAD | LogCategory.FLOW,
                        "Base game disposed.");
    }
}