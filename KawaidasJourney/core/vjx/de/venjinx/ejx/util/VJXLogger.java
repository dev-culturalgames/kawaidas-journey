package de.venjinx.ejx.util;

import java.util.Iterator;

import com.badlogic.gdx.maps.MapProperties;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.VJXProperties;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class VJXLogger {

    private static final String logInit = "#";
    private static final String logStart = ".(";
    private static final String logMid = ":";
    private static final String logEnd = "): ";

//    public static final int Z_INDEX_BACKGROUND = 1000;
//    public static final int Z_INDEX_HORIZON = 2000;
//    public static final int Z_INDEX_WORLD = 3000;
//    public static final int Z_INDEX_FOREGROUND = 4000;

    public static class LogCategory {
        public static final int ALL = 0x0fffffff;
        public static final int NONE = 0x0;
        public static final int LOAD = 0x1;
        public static final int FLOW = 0x2;
        public static final int STAGE = 0x4;
        public static final int EXEC = 0x8;
        public static final int DRAW = 0x10;
        public static final int INIT = 0x20;
        public static final int ANIM = 0x40;
        public static final int LEVEL = 0x80;
        public static final int GAME = 0x100;
        public static final int COLLISION = 0x200;
        public static final int STATUS = 0x400;
        public static final int SFX = 0x800;
        public static final int INPUT = 0x1000;
        public static final int UI = 0x2000;

        public static final int WARNING = 0x10000000;
        public static final int ERROR = 0x20000000;
        public static final int INFO = 0x40000000;
        public static final int DETAIL = 0x80000000;
    }

    private static int indentation = 0;

    private static final int logCategory =
                    LogCategory.NONE
//                    | LogCategory.INFO
//                    | LogCategory.STATUS
//                    | LogCategory.LOAD
//                    | LogCategory.FLOW
//                    | LogCategory.DRAW
//                    | LogCategory.ANIM
//                    | LogCategory.UI
//                    | LogCategory.INPUT
//                    | LogCategory.INIT
//                    | LogCategory.EXEC
//                    | LogCategory.SFX
//                    | LogCategory.COLLISION
                    ;
    private static final int filterCategory =
                    LogCategory.NONE
//                    | LogCategory.FLOW
//                    | LogCategory.COLLISION
//                    | LogCategory.ANIM
//                    | LogCategory.LOAD
//                    | LogCategory.INIT
//                    | LogCategory.STATUS
//                    | LogCategory.EXEC
//                    | LogCategory.SFX
                    ;

    private static String[] whiteList = {};
    //    private static String filter = "";
    //    private static String filter2 = "";
    private static String[] blackList = {};
    //    private static String filterNegative = "sfx";
    //    private static String filterNegative2 = "Audio";

    public static boolean printErrors = true;
    public static boolean printWarnings = false;
    public static boolean printDetails = false;

    public static void setWhiteFilter(String... whiteList) {
        VJXLogger.whiteList = whiteList;
    }

    public static void setBlackFilter(String... blackList) {
        VJXLogger.blackList = blackList;
    }

    public static void log(String msg) {
        log(LogCategory.ALL, msg, 0);
    }

    public static void log(String msg, int depth) {
        log(LogCategory.ALL, msg, depth);
    }

    public static void log(int logCategories, String msg, boolean condition) {
        if (condition) log(logCategories, msg, 0);
    }

    public static void log(int logCategories, String msg, int lvl,
                    boolean condition) {
        if (condition) log(logCategories, msg, lvl);
    }

    public static void log(int logCategories, String msg) {
        log(logCategories, msg, 0);
    }

    public static void log(int logCategories, String msg, int depth) {
        if (!checkPrint(logCategories, msg)) return;

        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        int mID = 1;
        for (int i = 1; i < trace.length - 1; i++)
            if (!trace[i].getMethodName().contains("log")
                            && !trace[i].getMethodName().contains("print")
                            && !trace[i].getClassName().contains("Thread")) {
                mID = i + depth;
                break;
            }

        String src = logInit + EJXGame.gameFrameNr + logStart
                     + trace[mID].getFileName() + logMid +
                     trace[mID].getLineNumber() + logEnd;

        src = String.format("%1$-40s", src);
        String m = src;

        if (logCategories == LogCategory.WARNING) m += "WARNING ---| ";
        else if (logCategories == LogCategory.ERROR) m += "ERROR ---| ";

        StringBuilder indentationStr = new StringBuilder();
        for (int i = 0; i < indentation; i++)
            indentationStr.append("|   ");
        m += indentationStr + msg;
        m = String.format("%1$-175s", m) + "|";
        System.out.println(m);
    }

    public static void logIncr(String msg) {
        logIncr(LogCategory.ALL, msg, 0);
    }

    public static void logIncr(String msg, int depth) {
        logIncr(LogCategory.ALL, msg, depth);
    }

    public static void logIncr(int logCategories, String msg) {
        logIncr(logCategories, msg, 0);
    }

    public static void logIncr(int logCategories, String msg, int depth) {
        log(logCategories, msg, depth);

        if (checkPrint(logCategories, msg)) incrementIndent();
    }

    public static void logDecr(String msg) {
        logDecr(LogCategory.ALL, msg, 0);
    }

    public static void logDecr(String msg, int depth) {
        logDecr(LogCategory.ALL, msg, depth);
    }

    public static void logDecr(int logCategories, String msg) {
        logDecr(logCategories, msg, 0);
    }

    public static void logDecr(int logCategories, String msg, int depth) {
        if (checkPrint(logCategories, msg)) decrementIndent();

        log(logCategories, msg, depth);
    }

    public static void printVJXProperties(VJXProperties props) {
        printVJXProperties(props, LogCategory.ALL, true, "");
    }

    public static void printVJXProperties(VJXProperties props, String prefix) {
        printVJXProperties(props, LogCategory.ALL, true, prefix);
    }

    public static void printVJXProperties(VJXProperties props, int logCat,
                    boolean condition, String prefix) {
        if (!condition) return;

        Iterator<EJXPropertyDefinition> iter = props.getKeys();
        EJXPropertyDefinition key;
        String name = props.getName();
        while (iter.hasNext()) {
            key = iter.next();

            if (props.get(key) != null)
                VJXLogger.log(logCat,
                                prefix + " " + name + " - "
                                                + String.format("%1$-20s", key)
                                                + ": " + props.get(key));
        }
    }

    public static void printMapProperties(MapProperties props) {
        printMapProperties(props, LogCategory.ALL, true, "");
    }

    public static void printMapProperties(MapProperties props, String prefix) {
        printMapProperties(props, LogCategory.ALL, true, prefix);
    }

    public static void printMapProperties(MapProperties props, int logCat) {
        printMapProperties(props, logCat, true, "");
    }

    public static void printMapProperties(MapProperties props, int logCat,
                    String prefix) {
        printMapProperties(props, logCat, true, prefix);
    }

    public static void printMapProperties(MapProperties props, int logCat,
                    boolean condition, String prefix) {
        if (!condition) return;

        Iterator<String> iter = props.getKeys();
        String key, name;
        while (iter.hasNext()) {
            key = iter.next();
            name = props.get("name", props.get("defName",
                            props.get("pl_name", props.get("mo:name",
                                            VJXString.STR_NO_NAME,
                                            String.class),
                                            String.class),
                            String.class), String.class);

            VJXLogger.log(logCat, prefix + "    " + name + " - "
                            + String.format("%1$-20s", key)
                            + ": " + props.get(key));
        }
    }

    public static String logCatToStr(int locCategory) {
        String string = "Logger category: ";
        if ((locCategory & LogCategory.ALL) == LogCategory.ALL) string += "all";
        if ((locCategory & LogCategory.LOAD) > 0) string += "load-";
        if ((locCategory & LogCategory.FLOW) > 0) string += "flow-";
        if ((locCategory & LogCategory.STAGE) > 0) string += "stage-";
        if ((locCategory & LogCategory.EXEC) > 0) string += "exec-";
        if ((locCategory & LogCategory.DRAW) > 0) string += "draw-";
        if ((locCategory & LogCategory.INIT) > 0) string += "init-";
        if ((locCategory & LogCategory.ANIM) > 0) string += "anim-";
        if ((locCategory & LogCategory.LEVEL) > 0) string += "level-";
        if ((locCategory & LogCategory.WARNING) > 0) string += "warning-";
        if ((locCategory & LogCategory.ERROR) > 0) string += "error-";
        if ((locCategory & LogCategory.GAME) > 0) string += "game-";
        if ((locCategory & LogCategory.COLLISION) > 0) string += "collision-";
        if ((locCategory & LogCategory.STATUS) > 0) string += "status-";
        if ((locCategory & LogCategory.SFX) > 0) string += "sfx-";
        if ((locCategory & LogCategory.INPUT) > 0) string += "input-";
        if ((locCategory & LogCategory.UI) > 0) string += "ui-";

        return string;
    }

    public static void incrementIndent() {
        indentation++;
    }

    public static void decrementIndent() {
        if (indentation == 0) return;
        indentation--;
    }

    private static boolean checkPrint(int logCategories, String msg) {
        if ((logCategories & LogCategory.ERROR) > 0 && printErrors
                        || (logCategories & LogCategory.WARNING) > 0
                        && printWarnings)
            return true;

        if (msg == null || logCategories != LogCategory.ALL
                        && (logCategories & filterCategory) > 0)
            return false;

        for (String string : blackList)
            if (msg.contains(string)) return false;

        boolean print = true;

        if (whiteList != null) if (whiteList.length > 0) {
            print = false;
            for (String string : whiteList)
                if (msg.contains(string)) {
                    print = true;
                    break;
                }
        }

        if (!print) return false;

        if (logCategories == LogCategory.ALL
                        || (logCategories & logCategory) > 0) {
            return (logCategories & LogCategory.DETAIL) == 0 || printDetails;
        }

        return (logCategories & LogCategory.ERROR) > 0 && printErrors
                || (logCategories & LogCategory.WARNING) > 0
                && printWarnings;
    }
}