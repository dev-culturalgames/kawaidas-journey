package de.venjinx.ejx.util;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.ObjectMap;

import de.venjinx.ejx.EJXCamera.CamMode;
import de.venjinx.ejx.entity.CollisionDef;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class EJXTypes {

    private static EJXTypes typeInstance = new EJXTypes();

    public static long ONE_MB = 1024l * 1024l;

    public static float byteToMB(long bytes, int precision) {
        float prec = (float) Math.pow(10, precision);
        bytes = (long) (bytes * 10f / 1048576 * prec);
        return bytes / prec;
    }

    public enum ClickType {
        CLICK, DOWN, UP;
    }

    public enum Lifecycle {
        // entity lifecycle
        CREATED("created"),
        STILL("still"), MOVE("move"),
        DEAD("dead"), DESPAWN("despawn");

        public final String name;

        private Lifecycle(String name) {
            this.name = name;
        }

        public static final Lifecycle get(String name) {
            for (Lifecycle s : Lifecycle.values())
                if (s.name.equals(name)) return s;
            VJXLogger.log(LogCategory.ERROR,
                            "Lifecycle '" + name + "' not found!");
            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public enum Activity {

        // entity activities
        SLEEP("sleep"),
        IDLE("idle"), CLIMB_IDLE("climb_idle"),
        WALK("walk"), CLIMB_WALK("climb_walk"),
        RUN("run"), CLIMB_RUN("climb_run"),
        RISE("rise"), FALL("fall"),
        FLY("fly"),
        SWIM("swim"),
        ACTIVATED("activated"), TRIGGERED("triggered"),
        GROWN("grown"),
        ATTACKING("attacking"),
        DIE("die"),
        EXECUTE("execute"), DISABLED("disabled");

        public final String name;

        private Activity(String name) {
            this.name = name;
        }

        public static final Activity get(String name) {
            for (Activity s : Activity.values())
                if (s.name.equals(name)) return s;
            VJXLogger.log(LogCategory.ERROR,
                            "Activity '" + name + "' not found!");
            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    };

    public enum B2DBit {
        // Base categories
        NONE("none", (short) 0), ALL("all", Short.MAX_VALUE),
        SOLID("solid", (short) 1), PASSABLE("passable",(short) 2),
        FERTILE("fertile", (short) 4), CLIMBABLE("climbable",(short) 8),
        WATER("water", (short) 16),
        OBJECT("object", (short) 32),
        TRIGGER("trigger", (short) 64), HITBOX("hitbox", (short) 128),
        DANGER("danger", (short) 256), SIDE("side", (short) 512),

        // Custom categories
        GROUND("ground", (short) 3);

        public final String name;
        public final short id;

        private B2DBit(String name, final short id) {
            this.name = name;
            this.id = id;
        }

        public boolean isCategory(short cat) {
            return (cat & id) > 0;
        }

        public static final B2DBit get(String name) {
            for (B2DBit cat : B2DBit.values())
                if (cat.name.equals(name)) return cat;

            return NONE;
        }
    };

    public enum Category {
        ALL("all", 0),
        COLLISION("collision", 1),
        OBJECT("object", 2),
        TILED("tiled", 3),
        ITEM("item", 4),
        DECORATION("decoration", 5),
        CHARACTER("character", 6),
        ;

        public final String catName;
        public final int id;

        private Category(final String name, final int id) {
            catName = name;
            this.id = id;
        }

        public static final Category get(int id) {
            for (Category c : Category.values())
                if (c.id == id) return c;

            return null;
        }

        public static final Category get(String name) {
            for (Category c : Category.values())
                if (c.catName.equals(name)) return c;

            return null;
        }
    }

    public enum AnimationType {
        FRAME("frame"), FBF("fbf"), SPINE("spine");

        public final String name;

        private AnimationType(final String name) {
            this.name = name;
        }

        public static final AnimationType get(String name) {
            for (AnimationType c : AnimationType.values())
                if (c.name.equals(name)) return c;
            return null;
        }
    }

    public enum VJXInterpolation {
        NONE("none"), FADE("fade"), LINEAR("linear"),
        BOUNCE("bounce"), BOUNCEIN("bouncein"), BOUNCEOUT("bounceout"),
        CIRCLE("circle"), CIRCLEIN("circlein"), CIRCLEOUT("circleout"),
        ELASTIC("elastic"), ELASTICIN("elasticin"), ELASTICOUT("elasticout"),
        EXP5("exp5"), EXP5IN("exp5in"), EXP5OUT("exp5out"),
        EXP10("exp10"), EXP10IN("exp10in"), EXP10OUT("exp10out"),
        POW2("pow2"), POW2IN("pow2in"), POW2OUT("pow2out"),
        POW3("pow3"), POW3IN("pow3in"), POW3OUT("pow3out"),
        POW4("pow4"), POW4IN("pow4in"), POW4OUT("pow4out"),
        POW5("pow5"), POW5IN("pow5in"), POW5OUT("pow5out"),
        SINE("sine"), SINEIN("sinein"), SINEOUT("sineout"),
        SWING("swing"), SWINGIN("swingin"), SWINGOUT("swingout");

        public final String name;

        private VJXInterpolation(final String name) {
            this.name = name;
        }

        public static final VJXInterpolation get(String name) {
            for (VJXInterpolation interpolation : VJXInterpolation.values())
                if (interpolation.name.equals(name)) return interpolation;
            return null;
        }

        public static final Interpolation getByName(String name) {
            for (VJXInterpolation interpolation : VJXInterpolation.values())
                if (interpolation.name.equals(name)) return get(interpolation);
            return null;
        }

        public static final Interpolation get(VJXInterpolation interpolation) {
            switch (interpolation) {
                case BOUNCE:
                    return Interpolation.bounce;
                case BOUNCEIN:
                    return Interpolation.bounceIn;
                case BOUNCEOUT:
                    return Interpolation.bounceOut;
                case CIRCLE:
                    return Interpolation.circle;
                case CIRCLEIN:
                    return Interpolation.circleIn;
                case CIRCLEOUT:
                    return Interpolation.circleOut;
                case ELASTIC:
                    return Interpolation.elastic;
                case ELASTICIN:
                    return Interpolation.elasticIn;
                case ELASTICOUT:
                    return Interpolation.elasticOut;
                case EXP10:
                    return Interpolation.exp10;
                case EXP10IN:
                    return Interpolation.exp10In;
                case EXP10OUT:
                    return Interpolation.exp10Out;
                case EXP5:
                    return Interpolation.exp5;
                case EXP5IN:
                    return Interpolation.exp5In;
                case EXP5OUT:
                    return Interpolation.exp5Out;
                case FADE:
                    return Interpolation.fade;
                case LINEAR:
                    return Interpolation.linear;
                case POW2:
                    return Interpolation.pow2;
                case POW2IN:
                    return Interpolation.pow2In;
                case POW2OUT:
                    return Interpolation.pow2Out;
                case POW3:
                    return Interpolation.pow3;
                case POW3IN:
                    return Interpolation.pow3In;
                case POW3OUT:
                    return Interpolation.pow3Out;
                case POW4:
                    return Interpolation.pow4;
                case POW4IN:
                    return Interpolation.pow4In;
                case POW4OUT:
                    return Interpolation.pow4Out;
                case POW5:
                    return Interpolation.pow5;
                case POW5IN:
                    return Interpolation.pow5In;
                case POW5OUT:
                    return Interpolation.pow5Out;
                case SINE:
                    return Interpolation.sine;
                case SINEIN:
                    return Interpolation.sineIn;
                case SINEOUT:
                    return Interpolation.sineOut;
                case SWING:
                    return Interpolation.swing;
                case SWINGIN:
                    return Interpolation.swingIn;
                case SWINGOUT:
                    return Interpolation.swingOut;
                default:
                    return null;

            }
        }
    }

    public enum WinCondition {
        PROGR_GROW("grow"),
        PROGR_EXT("extinguish"),
        PROGR_COLLECT("collect"),
        PROGR_NOTES("notes"),
        PROGR_REPAIR("repair");

        public final String name;

        private WinCondition(final String name) {
            this.name = name;
        }

        public static final WinCondition get(String name) {
            for (WinCondition logic : WinCondition.values())
                if (logic.name.equals(name))
                    return logic;
            VJXLogger.log(LogCategory.ERROR, "win condition '" + name + "' not found!");
            return null;
        }
    }

    public enum ActionContext {
        NONE("none", -1, false),
        JUMP_DOWN("jumpDown", 0, false, ClickType.CLICK),
        TELEPORT("teleport", 1, true, ClickType.CLICK),
        REPAIR("repair", 2, true, ClickType.CLICK),
        BROKEN("broken", 3, true, ClickType.CLICK),
        PLANT("plant", 4, false, ClickType.CLICK),
        TRASH("trash", 5, true, ClickType.CLICK),
        EXTINGUISH("extinguish", 6, true, ClickType.DOWN, ClickType.UP),
        USE("use", 7, false, ClickType.CLICK),
        ALL("all", 8, false, ClickType.CLICK);

        public final String name;
        public final int prio;
        public ClickType[] types;
        public final boolean isLogic;

        private ActionContext(String name, int prio, boolean isLogic,
                        ClickType... types) {
            this.name = name;
            this.prio = prio;
            this.isLogic = isLogic;
            this.types = types;
        }

        public static final ActionContext get(String name) {
            for (ActionContext context : ActionContext.values())
                if (context.name.equals(name)) return context;
            return null;
        }

        public static final ActionContext getByPrio(int prio) {
            for (ActionContext context : ActionContext.values())
                if (context.prio == prio)
                    return context;
            return null;
        }

        @Override
        public String toString() {
            return name + "(" + prio + ")";
        }
    }

    /**
     * @author Torge Rothe (XRJ, X-Ray-Jin)<br>
     *<br>
     * General game layer types - top level structure. Types have layer name and
     * a zIndex base value. Use these to split the game world in different
     * general levels of layers. E.g.: 2D sidescroller<br>
     *      - background (decoration)<br>
     *      - horizon (decoration)<br>
     *      - focus (main layer, player, items, enemies, decoration, etc.)<br>
     *      - foreground (decoration)<br>
     *<br>
     * The DEFAULT and FOCUS type should remain.<br>
     * DEFAULT is used whenever no custom loading on tmx files is done or
     * placing, naming errors occur while loading. It can also be used as a
     * temporary layer for objects that are not currently in the scene but
     * needs to be updated, e.g. instance bases.<br>Be creative.<br>
     * <br>
     * FOCUS is used for the actual game world. Characters, items, obstacles,
     * enemies and stuff like that. Also effects like parallaxing or blur are
     * relative to that layer.
     *
     * All other custom layers can be brought into structure via a tmx editor
     * by correct naming ("<{@link LayerDepth}.name>_base_layer") and use of
     * {@link Depth} as a layer property.
     */
    public enum LayerDepth {
        DEFAULT("default", 0), BACKGROUND("background", -2),
        HORIZON("horizon", -1), FOCUS("focus", 0),
        FOREGROUND("foreground", 1);

        public final String name;
        public final float zOffset;

        private LayerDepth(final String name, final int zOffset) {
            this.name = name;
            this.zOffset = zOffset;
        }

        public static final LayerDepth get(float zIndex) {
            for (LayerDepth depth : LayerDepth.values())
                if (depth.zOffset == zIndex) return depth;

            return null;
        }

        public static final LayerDepth get(String name) {
            for (LayerDepth depth : LayerDepth.values())
                if (depth.name.equals(name)) return depth;
            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    /**
     * @author Torge Rothe (XRJ, X-Ray-Jin)
     *
     * Internal depth information for layers inside a game layer.
     * Can be used for structuring the world draw order more specifically and
     * allows for automatic parallaxing or other layer effects.
     * CENTER is default and should always remain.
     */
    public enum Depth {
        BACK("back", -2), FAR("far", -1),
        CENTER("center", 0), NEAR("near", 1),
        FRONT("front", 2);

        public final String name;
        public final float zOffset;

        private Depth(final String depth, final int zOffset) {
            name = depth;
            this.zOffset = zOffset;
        }

        public static final Depth get(float zIndex) {
            for (Depth depth : Depth.values())
                if (depth.zOffset == zIndex) return depth;

            return null;
        }

        public static final Depth get(String name) {
            for (Depth depth : Depth.values())
                if (depth.name.equals(name)) return depth;
            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public enum VJXSide {
        BOTTOM(0, "bottom", new Vector2(0f, -1f)), RIGHT(1, "right", new Vector2(1f, 0f)),
        TOP(2, "top", new Vector2(0f, 1f)), LEFT(3, "left", new Vector2(-1f, 0f));

        public final String name;
        public final Vector2 direction;
        public int ind;

        //        public final boolean flipX;
        //        public final boolean flipY;

        private VJXSide(int i, String name, Vector2 direction) {
            ind = i;
            this.name = name;
            this.direction = direction;
        }

        @Override
        public String toString() {
            return name + " - " + getClass().getSimpleName();
        }
    };

    public enum VJXDirection {
        CENTER(-1, "center", new Vector2()), DOWN(0, "down", new Vector2(0f, -1f)),
        RIGHT(1, "right", new Vector2(1f, 0f)), UP(2, "up", new Vector2(0f, 1f)),
        LEFT(3, "left", new Vector2(-1f, 0f));

        public final String name;
        public final Vector2 direction;
        public int id;

        private VJXDirection(int i, String name, Vector2 direction) {
            id = i;
            this.name = name;
            this.direction = direction;
        }

        public static final VJXDirection get(String name) {
            for (VJXDirection direction : VJXDirection.values())
                if (name.contains(direction.name)) return direction;

            return null;
        }

        public static final VJXDirection get(int id) {
            for (VJXDirection direction : VJXDirection.values())
                if (direction.id == id) return direction;

            return null;
        }

        public static final boolean isDirection(VJXDirection dir,
                        Vector2 dirVec) {
            switch (dir) {
                case DOWN:
                    return dirVec.y <= 0;
                case LEFT:
                    return dirVec.x <= 0;
                case RIGHT:
                    return dirVec.x >= 0;
                case UP:
                    return dirVec.y >= 0;
                default:
                    return false;
            }
        }

        public static final boolean isOppositeDirection(VJXDirection dir,
                        Vector2 dirVec) {
            switch (dir) {
                case DOWN:
                    return dirVec.y >= 0;
                case LEFT:
                    return dirVec.x >= 0;
                case RIGHT:
                    return dirVec.x <= 0;
                case UP:
                    return dirVec.y <= 0;
                default:
                    return false;
            }
        }

        @Override
        public String toString() {
            return name + " - " + getClass().getSimpleName();
        }
    };

    public enum Orientation {
        DOWN("down", new Vector2(0f, -1f)), RIGHT("right", new Vector2(1f, 0f)),
        UP("up", new Vector2(0f, 1f)), LEFT("left", new Vector2(-1f, 0f));

        public final String name;
        public final Vector2 direction;

        private Orientation(String name, Vector2 direction) {
            this.name = name;
            this.direction = direction;
        }

        public static final Orientation get(String name) {
            for (Orientation s : Orientation.values())
                if (s.name.equals(name)) return s;

            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    };

    public static VJXProperties newProps() {
        return newProps(VJXProperties.VJXProperties);
    }

    public static VJXProperties newProps(EJXPropertyDefinition[] properties) {
        VJXProperties props = newProps();

        for (EJXPropertyDefinition prop : properties)
            props.put(prop, prop.getDefault());
        return props;
    }

    public static VJXProperties newProps(String name) {
        return typeInstance.new VJXProperties(name);
    }

    @SuppressWarnings("unchecked")
    public class VJXProperties {
        public static final String VJXProperties = "VJXProperties";

        private String name;
        private ObjectMap<EJXPropertyDefinition, Object> properties;

        /** Creates an empty properties set */
        public VJXProperties() {
            this(VJXProperties);
        }

        /** Creates an empty properties set */
        public VJXProperties(String name) {
            this.name = name;
            properties = new ObjectMap<>();
        }

        /** @param key property name
         * @return true if and only if the property exists */
        public boolean containsKey(EJXPropertyDefinition key) {
            return properties.containsKey(key);
        }

        /** @param key property name
         * @return the value for that property if it exists, otherwise, null */
        public Object get(EJXPropertyDefinition key) {
            return properties.get(key);
        }

        /** Returns the object for the given key, casting it to clazz.
         * @param key the key of the object
         * @param clazz the class of the object
         * @return the object or null if the object is not in the map
         * @throws ClassCastException if the object with the given key is not of type clazz */
        public <T> T get(EJXPropertyDefinition key, Class<T> clazz) {
            return (T) get(key);
        }

        /** Returns the object for the given key, casting it to clazz.
         * @param key the key of the object
         * @param defaultValue the default value
         * @param clazz the class of the object
         * @return the object or the defaultValue if the object is not in the map
         * @throws ClassCastException if the object with the given key is not of type clazz */
        public <T> T get(EJXPropertyDefinition key, T defaultValue,
                        Class<T> clazz) {
            Object object = get(key);
            return object == null ? defaultValue : (T) object;
        }

        /** @param key property name
         * @param value value to be inserted or modified (if it already existed) */
        public void put(EJXPropertyDefinition key, Object value) {
            properties.put(key, value);
        }

        /** @param properties set of properties to be added */
        public void putAll(VJXProperties properties) {
            this.properties.putAll(properties.properties);
        }

        /** @param key property name to be removed */
        public void remove(EJXPropertyDefinition key) {
            properties.remove(key);
        }

        /** Removes all properties */
        public void clear() {
            properties.clear();
        }

        /** @return iterator for the property names */
        public Iterator<EJXPropertyDefinition> getKeys() {
            return properties.keys();
        }

        /** @return iterator to properties' values */
        public Iterator<Object> getValues() {
            return properties.values();
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public abstract class EJXProperty<T> {
        protected T baseValue;

        public EJXProperty(T baseValue) {
            this.baseValue = baseValue;
        }

        public abstract T get();
        public abstract void set(Object value);

        public T getBaseValue() {
            return baseValue;
        }

        public void setBaseValue(T baseValue) {
            this.baseValue = baseValue;
        }

        public void restore() {
            set(baseValue);
        }
    }

    public class EJXFloat extends EJXProperty<Float> {
        public float value;

        public EJXFloat() {
            this(0f);
        }

        public EJXFloat(float baseValue) {
            super(baseValue);
            value = baseValue;
        }

        @Override
        public Float get() {
            return value;
        }

        @Override
        public void set(Object value) {
            this.value = (float) value;
        }
    }

    public static EJXFloat newFloat() {
        return newFloat(0f);
    }

    public static EJXFloat newFloat(float initValue) {
        return typeInstance.new EJXFloat(initValue);
    }

    public enum PropertyType {
        // new
        ejxBool(VJXString.boolType, Boolean.class),
        ejxInt(VJXString.intType, Integer.class),
        ejxFloat(VJXString.ejxFloat, EJXFloat.class),
        ejxColor(VJXString.colorType, Color.class),

        // old
        floatProp(VJXString.floatType, Float.class),
        intProp(VJXString.intType, Integer.class),
        boolProp(VJXString.boolType, Boolean.class),
        stringProp(VJXString.stringType, String.class),
        colorProp(VJXString.colorType, Color.class),

        categoryProp(VJXString.categoryType, Category.class),
        physicsType(VJXString.physicsType, BodyType.class),
        animationType(VJXString.animationType, AnimationType.class),
        colliderDef(VJXString.colliderType, CollisionDef.class),
        camModeType(VJXString.camModeType, CamMode.class);

        public final String name;
        private final Class<?> clazz;

        private PropertyType(final String name, Class<?> clazz) {
            this.name = name;
            this.clazz = clazz;
        }

        public static final PropertyType get(String name) {
            for (PropertyType type : PropertyType.values())
                if (type.name.equals(name)) return type;
            return null;
        }

        public Class<?> getClazz() {
            return clazz;
        }
    }

    public interface EJXPropertyDefinition {
        String getName();
        PropertyType getType();
        Object getDefault();
    }

    public enum SourceProperty implements EJXPropertyDefinition {
        mapTileId(VJXString.mapTileId, PropertyType.intProp, -1),
        id(VJXString.id, PropertyType.intProp, -1),
        source(VJXString.source, PropertyType.stringProp, VJXString.STR_EMPTY),
        srcWidth(VJXString.srcWidth, PropertyType.intProp, 0),
        srcHeight(VJXString.srcHeight, PropertyType.intProp, 0),
        tileName(VJXString.tileName, PropertyType.stringProp, VJXString.STR_EMPTY),
        tileset(VJXString.tileset, PropertyType.stringProp, VJXString.STR_EMPTY),
        fromSet(VJXString.fromSet, PropertyType.boolProp, true),
        environment(VJXString.environment, PropertyType.stringProp, VJXString.STR_EMPTY);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private SourceProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final SourceProperty get(String name) {
            for (SourceProperty property : SourceProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    public enum DefinitionProperty implements EJXPropertyDefinition {
        defID(VJXString.defID, PropertyType.intProp, 0),
        defName(VJXString.defName, PropertyType.stringProp, VJXString.STR_EMPTY),
        category(VJXString.category, PropertyType.categoryProp, Category.OBJECT),
        attributes(VJXString.attributes, PropertyType.stringProp, VJXString.STR_EMPTY),
        body(VJXString.body, PropertyType.physicsType,BodyType.StaticBody),
        animation(VJXString.animation, PropertyType.animationType, AnimationType.FRAME),
        skeleton(VJXString.skeleton, PropertyType.stringProp, VJXString.STR_EMPTY),

        visible(VJXString.visible, PropertyType.boolProp, true),
        emotional(VJXString.emotional, PropertyType.boolProp, false),
        usePhysics(VJXString.usePhysics, PropertyType.boolProp, false),
        skeletonDef(VJXString.skeletonDef, PropertyType.boolProp, false),
        skin(VJXString.skin, PropertyType.boolProp, false),
        growable(VJXString.growable, PropertyType.boolProp, false),
        flying(VJXString.flying, PropertyType.boolProp, false),
        logics(VJXString.logics, PropertyType.stringProp, VJXString.STR_EMPTY),
        collider(VJXString.collider, PropertyType.colliderDef,  new CollisionDef()),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private DefinitionProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final DefinitionProperty get(String name) {
            for (DefinitionProperty property : DefinitionProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    public enum SpatialProperty implements EJXPropertyDefinition {
        x(VJXString.x, PropertyType.floatProp, 0f),
        y(VJXString.y, PropertyType.floatProp, 0f),
        localX(VJXString.localX, PropertyType.floatProp, 0f),
        localY(VJXString.localY, PropertyType.floatProp, 0f),
        width(VJXString.width, PropertyType.floatProp, 0f),
        height(VJXString.height, PropertyType.floatProp, 0f),
        scaleX(VJXString.scaleX, PropertyType.floatProp, 1f),
        scaleY(VJXString.scaleY, PropertyType.floatProp, 1f),
        flipX(VJXString.flipX, PropertyType.boolProp, false),
        flipY(VJXString.flipY, PropertyType.boolProp, false),
        rotation(VJXString.rotation, PropertyType.floatProp, 0f);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private SpatialProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final SpatialProperty get(String name) {
            for (SpatialProperty property : SpatialProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    public enum EntityProperty implements EJXPropertyDefinition {
        objName(VJXString.name, PropertyType.stringProp, VJXString.STR_EMPTY),
        range(VJXString.range, PropertyType.floatProp, 0f),
        view(VJXString.view, PropertyType.floatProp, 0f);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private EntityProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final EntityProperty get(String name) {
            for (EntityProperty property : EntityProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    public static class Tiles {

        public static Vector2[] getTileVertices(int tileCase) {
            int[] ids = tilePoints[tileCase];
            Vector2[] points = new Vector2[ids.length];

            for (int i = 0; i < ids.length; i++)
                points[i] = new Vector2(vertices[ids[i]][0],
                                vertices[ids[i]][1]);

            return points;
        }

        public static Vector2 getVertex(int id) {
            return new Vector2(vertices[id][0], vertices[id][1]);
        }

        private static final float[][] vertices = new float[][] {
            { .0f, 0f }, { .25f, 0f }, { .5f, 0f }, { .75f, 0f }, { 1f, 0f },
            { .0f, .25f }, { .25f, .25f }, { .5f, .25f }, { .75f, .25f }, { 1f, .25f },
            { .0f, .5f }, { .25f, .5f }, { .5f, .5f }, { .75f, .5f }, { 1f, .5f },
            { .0f, .75f }, { .25f, .75f }, { .5f, .75f }, { .75f, .75f }, { 1f, .75f },
            { .0f, 1f }, { .25f, 1f }, { .5f, 1f }, { .75f, 1f }, { 1f, 1f }
        };

        private static final int[][] tilePoints = new int[][] {
            {},

            { 9, 3, 4 }, { 9, 8, 3, 4 }, { 9 ,5 ,0 ,4 },
            { 24, 23, 5, 0, 4 }, { 24, 23, 8, 5, 0, 4 }, { 3, 9, 14, 2 },
            { 2, 9, 14, 1 }, { 2, 14, 19, 1 },

            { 14, 2, 4 }, { 14, 12, 2, 4 }, { 14, 10, 0, 4 },
            { 24, 22, 10, 0, 4 }, { 24, 22, 12, 10, 0, 4 },
            { 3, 8, 9, 14, 12, 2 }, { 2, 7, 9, 14, 11, 1 }, { 2, 12, 14, 19, 16, 1 },

            { 19, 1, 4 }, { 19, 16, 1, 4 }, { 19, 15, 0, 4 }, { 24, 21, 15, 0, 4 },
            { 24, 21, 16, 15, 0, 4 }, { 5, 9, 14, 13, 23, 22, 12, 10 },
            { 5, 7, 2, 3, 8, 9, 14, 13, 23, 22, 12, 10 },
            { 5, 7, 2, 3, 8, 9, 14, 12, 22, 21, 11, 10 },

            { 9, 2, 4 }, { 9, 7, 2, 4 }, { 14, 5, 0, 4 }, {14, 12, 7, 5, 0, 4 },
            { 24, 23, 10, 0, 4 }, { 24, 23, 13, 10, 0, 4 }, { 14, 0, 4 },
            { 24, 10, 0, 4},

            { 9, 1, 4 }, { 9, 6, 1, 4}, { 19, 5, 0, 4 }, { 19, 17, 7, 5, 0, 4 },
            { 24, 23, 15, 0, 4 }, { 24, 23, 18, 15, 0, 4}, { 9, 0, 4 }, { 24, 5, 0, 4 },

            { 14, 1, 4 }, { 14, 11, 1, 4 }, { 19, 10, 0, 4 }, { 19, 17, 12, 10, 0, 4 },
            { 24, 22, 15, 0, 4 }, { 24, 22, 17, 15, 0, 4}, { 19, 0, 4 }, { 24, 15, 0, 4 },

            { 24, 0, 4 }, { 0, 4, 24, 20 }, { 0, 4, 9, 8, 23, 21, 6, 5 },
            { 0, 4, 9, 8, 23, 22, 7, 5 }, { 0, 4, 14, 13, 23, 21, 11, 10 },
            { 0, 4, 14, 13, 23, 22, 12, 10 }, { 0, 4, 19, 18, 23, 21, 16, 15 },
            { 0, 4, 19, 18, 23, 22, 17, 15 },

            { 6, 8, 18, 16 }, { 5, 9, 19, 15 }, { 5, 6, 1, 3, 18, 15 },
            { 1, 3, 8, 9, 19, 18, 23, 21 }, { 5, 6, 1, 3, 8, 9, 19, 18, 23, 21, 16, 15 },
            { 1, 3, 8, 6 }, { 1, 3, 13, 11 }, { 1, 3, 18, 16 },

            { 5, 6, 11, 10 }, { 5, 7, 12, 10 }, { 5, 8, 13, 10 }, { 5, 9, 14, 10 },
            { 5, 7, 12, 14, 19, 16, 11, 10 }, { 5, 6, 13, 14, 19, 18, 11, 10 },
            { 5, 14, 19, 10 }, { 10, 19, 24, 15 },

            {6, 8, 13, 11 }, {0, 1, 9, 14 }, { 5, 19, 24, 10 }
        };


        public static final int[][] tileNeighbors = new int[][] {
            {},
            { 0, 1 }, { 0, 1 }, { 3, 1, 0 }, { 3, 2, 0, 1 },
            { 3, 2, 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 },

            { 0, 1 }, { 0, 1 }, { 3, 1, 0 }, { 3, 2, 0, 1 },
            { 3, 2, 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 },

            { 0, 1 }, { 0, 1 }, { 3, 1, 0 }, { 3, 2, 0, 1 },
            { 3, 2, 0, 1 }, { 1, 2, 3 }, { 0, 1, 2, 3 }, { 0, 1, 2, 3 },

            { 0, 1 }, { 0, 1 }, { 3, 1, 0 }, { 3, 1, 0 },
            { 3, 2, 0, 1 }, { 3, 2, 0, 1 }, { 0, 1 }, { 3, 1, 0 },

            { 0, 1 }, { 0, 1 }, { 3, 1, 0 }, { 3, 1, 0 },
            { 3, 2, 0, 1 }, { 3, 2, 0, 1 }, { 0, 1 }, { 3, 1, 0 },

            { 0, 1 }, { 0, 1 }, { 3, 1, 0 }, { 3, 1, 0 },
            { 3, 2, 0, 1 }, { 3, 2, 0, 1 }, { 0, 1 }, { 3, 1, 0 },

            { 0, 1 }, { 0, 1, 2, 3 }, { 0, 1, 2, 3 }, { 0, 1, 2, 3 },
            { 0, 1, 2, 3 }, { 0, 1, 2, 3 }, { 0, 1, 2, 3 }, { 0, 1, 2, 3 },

            {}, { 1, 3 }, { 0, 3 }, { 0, 1, 2 },
            { 0, 1, 2, 3 }, { 0 }, { 0 }, { 0 },

            { 3 }, { 3 }, { 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 },

            {}, { 0, 1, 2, 3 }, { 0, 1 }, { 3, 1 }, {}, {}, {}, {}
        };

        public static final int[][] tileEdges = new int[][] { { 0, 4 },
            { 4, 24 }, { 24, 20 }, { 20, 0 } };

            public static final int[][] neighborDir = new int[][]
                            { { 0, -1 }, { 1, 0 }, { 0, 1 }, { -1, 0 } };
    }

    public static class EJXRegEx {
        public static final String matchVAR = "^\\$\\w*$";
        public static final String matchEmpty = "()";
        public static final String matchSkip = "(-)";
        public static final String matchBool = "(true|false)";
        public static final String matchFloat = "([+-]?(\\d+|(\\d+\\.\\d+)|(\\.\\d+)|(\\d+\\.))([eE]\\d+)?[f]?)";
        public static final String matchInteger = "([+-]?(\\d+))";
        public static final String matchWord = "(\\w+\\s?\\w+)";
        public static final String matchCmdVariable = "(\\w+\\.\\w+)";
        public static final String matchVector = matchFloat + "," + matchFloat;
        public static final String matchDirection = "(left|right|up|down)";
        public static final String matchCommand = matchWord + ":\\w+(:("
                        + matchEmpty + "|" + matchSkip + "|" + matchWord
                        + "|" + matchCmdVariable + "|" + matchBool
                        + "|" + matchInteger + "|" + matchFloat
                        + "|" + matchVector + "))*";
    }

    public static class EJXError {
        public static final String WARNING_COMMAND_NOT_EXECUTED = " Command not executed: ";
        public static final String WARNING_PARAMS_COUNT_0 = "Ignored all parameters.";
        public static final String WARNING_PARAMS_COUNT_1 = "Ignored parameters after position 1: ";
        public static final String WARNING_PARAMS_COUNT_2 = "Ignored parameters after position 2: ";
        public static final String WARNING_PARAMS_COUNT_3 = "Ignored parameters after position 3: ";
        public static final String WARNING_PARAMS_COUNT_4 = "Ignored parameters after position 4: ";
        public static final String WARNING_PARAMS_COUNT_5 = "Ignored parameters after position 5: ";

        public static final String ERROR_PARAMS_COUNT_1 = "Invalid parameter count. 1 parameter expected: ";
        public static final String ERROR_PARAMS_COUNT_1_2 = "Invalid parameter count. 1 or 2 parameters expected: ";
        public static final String ERROR_PARAMS_COUNT_2 = "Invalid parameter count. 2 parameters expected: ";
        public static final String ERROR_PARAMS_COUNT_2_3 = "Invalid parameter count. 2 or 3 parameters expected: ";
        public static final String ERROR_PARAMS_COUNT_3 = "Invalid parameter count. 3 parameters expected: ";
        public static final String ERROR_PARAMS_COUNT_1_4 = "Invalid parameter count. 1 to 4 parameters expected: ";
        public static final String ERROR_PARAMS_COUNT_4 = "Invalid parameter count. 4 parameters expected: ";
        public static final String ERROR_PARAM_POS1 = "Invalid parameter at position 1.";
        public static final String ERROR_PARAM_POS2 = "Invalid parameter at position 2.";
        public static final String ERROR_PARAM_POS3 = "Invalid parameter at position 3.";
        public static final String ERROR_PARAM_POS4 = "Invalid parameter at position 4.";

        public static final String ERROR_INVALID_PARAM_STRING = " String parameter may not be null or empty.";
        public static final String ERROR_INVALID_PARAM_BOOL = " Only 'true' or 'false' are allowed.";
        public static final String ERROR_INVALID_PARAM_INT = " Only positive or negative integers are allowed.";
        public static final String ERROR_INVALID_PARAM_FLOAT = " Only positive or negative float numbers are allowed.";
        public static final String ERROR_INVALID_PARAM_VECTOR = " Vector string format is: <<float_x>,<float_y>>";
        public static final String ERROR_INVALID_PARAM_POSITION = " No valid vector string or object not found.";
        public static final String ERROR_INVALID_PARAM_DIRECTION = " Only 'left', 'right', 'up' or 'down' are allowed.";
        public static final String ERROR_INVALID_PARAM_VARIABLE = " Variable format: <locicName>.<variableName>";
        public static final String ERROR_INVALID_PARAM_DEF_NOT_FOUND = " EntityDef not found.";
        public static final String ERROR_INVALID_PARAM_LAYER_NOT_FOUND = " Map layer not found.";
    }

    public static abstract class VJXString {

        public static final String subcategory = "subcategory";

        // alignment
        public static final String ALIGN_BOTTOM = "alignBottom";
        public static final String ALIGN_CENTER = "alignCenter";
        public static final String ALIGN_LEFT = "alignLeft";
        public static final String ALIGN_MIDDLE = "alignMiddle";
        public static final String ALIGN_RIGHT = "alignRight";
        public static final String ALIGN_TOP = "alignTop";

        // commands
        public static final String ACTOR_GAME = "game";
        public static final String ACTOR_PLAYER = "player";
        public static final String ACTOR_TARGET = "target";
        public static final String ACTOR_THIS = "this";

        // entity source properties
        public static final String mapTileId = "mapTileId";
        public static final String id = "id";
        public static final String src = "src";
        public static final String source = "source";
        public static final String srcWidth = "srcWidth";
        public static final String srcHeight = "srcHeight";
        public static final String map = "map";
        public static final String tileset = "tileset";
        public static final String tileName = "tileName";
        public static final String fromSet = "fromSet";
        public static final String environment = "environment";

        // entity definition properties
        public static final String animation = "animation";
        public static final String attributes = "attributes";
        public static final String body = "body";
        public static final String category = "category";
        public static final String collider = "collider";
        public static final String defID = "defID";
        public static final String defName = "defName";
        public static final String emotional = "emotional";
        public static final String flying = "flying";
        public static final String growable = "growable";
        public static final String logics = "logics";
        public static final String skeleton = "skeleton";
        public static final String skeletonDef = "skeletonDef";
        public static final String skin = "skin";
        public static final String usePhysics = "usePhysics";
        public static final String visible = "visible";

        // entity spatial properties
        public static final String flipX = "flipX";
        public static final String flipY = "flipY";
        public static final String height = "height";
        public static final String localX = "localX";
        public static final String localY = "localY";
        public static final String rotation = "rotation";
        public static final String scaleX = "scaleX";
        public static final String scaleY = "scaleY";
        public static final String width = "width";
        public static final String x = "x";
        public static final String y = "y";

        // file extensions
        public static final String FILE_JPG = ".jpg";
        public static final String FILE_PNG = ".png";
        public static final String FILE_MP3 = ".mp3";
        public static final String FILE_JSON = ".json";
        public static final String FILE_ATLAS = ".atlas";
        public static final String FILE_TMX = ".tmx";
        public static final String FILE_pref = ".pref";

        // folders
        public static final String DIR_rawAssets = "rawAssets/";

        // level
        public static final String player_start = "player_start";

        // miscellaneous
        public static final String duration = "duration";
        public static final String name = "name";
        public static final String range = "range";
        public static final String type = "type";;
        public static final String utility = "utility";
        public static final String value = "value";
        public static final String view = "view";

        public static final String STR_EMPTY = "";
        public static final String STR_NO_NAME = "<no_name>";
        public static final String STR_PLUS = "+";

        // numbers
        public static final String INT_0 = "0";
        public static final String HEX_255 = "ff";

        // paths
        public static final String PATH_DIR_UP = "../";

        // pre- / suffixes, fill in
        public static final String PRE_int_ = "int_";
        public static final String PRE_bool_ = "bool_";
        public static final String PRE_float_ = "float_";
        public static final String PRE_long_ = "long_";
        public static final String PRE_string_ = "string_";
        public static final String PRE_image_ = "image_";

        public static final String PRE_ico_ = "ico_";
        public static final String PRE_sfx = "sfx:";
        public static final String PRE_ambient = "ambient:";
        public static final String PRE_mo = "mo:";
        public static final String PRE_but = "but";

        public static final String FILL_sfx = ":sfx:";

        public static final String POST_prob = ":prop";
        public static final String POST_vol = ":vol";
        public static final String POST_loop = ":loop";
        public static final String POST_pitch = ":pitch";
        public static final String POST_count = ":count";
        public static final String POST_new = "_new";
        public static final String POST_icon = "_icon";
        public static final String POST_path = "_path";
        public static final String POST_stack = "_stack";
        public static final String POST_table = "_table";
        public static final String POST_background = "_background";

        public static final String POST_up = "_up";
        public static final String POST_down = "_down";

        // property types
        public static final String intType = "intType";
        public static final String floatType = "floatType";
        public static final String ejxFloat = "float2Type";
        public static final String boolType = "boolType";
        public static final String stringType = "stringType";
        public static final String colorType = "colorType";
        public static final String categoryType = "categoryType";
        public static final String lifecycleType = "lifecycleType";
        public static final String activityType = "activityType";
        public static final String vector2Type = "vector2Type";
        public static final String physicsType = "physicsType";
        public static final String animationType = "animationType";
        public static final String entityType = "entityType";
        public static final String colliderType = "colliderType";
        public static final String fileHandleType = "fileHandleType";
        public static final String camModeType = "camModeType";

        // separators
        public static final String SEP_USCORE = "_";
        public static final String SEP_BRACKET_OPEN = "(";
        public static final String SEP_BRACKET_CLOSE = ")";
        public static final String SEP_DASH = "-";
        public static final String SEP_DDOT = ":";
        public static final String SEP_DDOT_SPACE = ": ";
        public static final String SEP_DOT = "\\.";
        public static final String SEP_LIST = ",";
        public static final String SEP_LIST_SPACE = ", ";
        public static final String SEP_NEW_LINE_N = "\n";
        public static final String SEP_NEW_LINE_R = "\r";
        public static final String SEP_NR = "#";
        public static final String SEP_SLASH = "/";

        // sound
        public static final String SFX_offScreen = "sfx:offScreen";
        public static final String SFX_onScreen = "sfx:onScreen";

        // tiled originals
        public static final String TILED_gid = "gid";
        public static final String TILED_backgroundcolor = "backgroundcolor";
        public static final String TILED_color = "color";
        public static final String TILED_columns = "columns";
        public static final String TILED_draworder = "draworder";
        public static final String TILED_firstgid = "firstgid";
        public static final String TILED_frame = "frame";
        public static final String TILED_image = "image";
        public static final String TILED_imagelayer = "imagelayer";
        public static final String TILED_imageSource = "imageSource";
        public static final String TILED_infinite = "infinite";
        public static final String TILED_hexsidelength = "hexsidelength";
        public static final String TILED_margin = "margin";
        public static final String TILED_nextlayerid = "nextlayerid";
        public static final String TILED_nextobjectid = "nextobjectid";
        public static final String TILED_group = "group";
        public static final String TILED_layer = "layer";
        public static final String TILED_locked = "locked";
        public static final String TILED_object = "object";
        public static final String TILED_objectgroup = "objectgroup";
        public static final String TILED_offsetX = "offsetx";
        public static final String TILED_offsetY = "offsety";
        public static final String TILED_opacity = "opacity";
        public static final String TILED_orientation = "orientation";
        public static final String TILED_probability = "probability";
        public static final String TILED_properties = "properties";
        public static final String TILED_property = "property";
        public static final String TILED_renderorder = "renderorder";
        public static final String TILED_spacing = "spacing";
        public static final String TILED_staggeraxis = "staggeraxis";
        public static final String TILED_staggerindex = "staggerindex";
        public static final String TILED_terrain = "terrain";
        public static final String TILED_tiledversion = "tiledversion";
        public static final String TILED_tilecount = "tilecount";
        public static final String TILED_tileheight = "tileheight";
        public static final String TILED_tileoffset = "tileoffset";
        public static final String TILED_tilewidth = "tilewidth";
        public static final String TILED_trans = "trans";
        public static final String TILED_version = "version";

        // trigger types
        public static final String TRIGGER_TYPE = "triggerType";
        public static final String TRIGGER_SCREEN = "screen";
        public static final String TRIGGER_RESPAWN = "respawn";
        public static final String TRIGGER_TRIGGER = "trigger";
        public static final String TRIGGER_ACTIVATE = "activate";
        public static final String TRIGGER_VIEW = "view";
        public static final String TRIGGER_RANGE = "range";

        // variable types
        public static final String VAR = "\\$";
        public static final String VAR_string = "string";
        public static final String VAR_int = "int";
        public static final String VAR_long = "long";
        public static final String VAR_float = "float";
        public static final String VAR_bool = "bool";
        public static final String VAR_file = "file";
        public static final String VAR_color = "color";

        // world
        public static final String WORLD = "world";
    }

}