package de.venjinx.ejx.util;

import de.venjinx.ejx.entity.EJXEntity;

public class VJXData {

    private String name;
    private EJXEntity entity;
    private int collId;
    private boolean instanciated;
    private float width;
    private float height;

    public VJXData(String name, EJXEntity entity) {
        this(name, entity, 0f, 0f, 0);
    }

    public VJXData(String name, EJXEntity entity, float width, float height) {
        this(name, entity, width, height, 0);
    }

    public VJXData(String name, EJXEntity entity, float width, float height, int id) {
        this.name = name;
        this.entity = entity;
        this.width = width;
        this.height = height;
        collId = id;
    }

    public String getName() {
        return name;
    }

    public void setEntity(EJXEntity entity) {
        this.entity = entity;
    }

    public EJXEntity getEntity() {
        return entity;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public int getCollID() {
        return collId;
    }

    public boolean hasInstance(){return instanciated;}

    @Override
    public String toString() {
        return name;
    }
}