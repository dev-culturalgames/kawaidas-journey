package de.venjinx.ejx.util;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.esotericsoftware.spine.SkeletonRenderer;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.SpatialProperty;
import de.venjinx.ejx.util.EJXTypes.VJXProperties;

public class VJXGraphix {

    public static SkeletonRenderer spineRenderer = new SkeletonRenderer();
    private static Vector2 tmpVec = new Vector2();

    private static Matrix3 flipTrans = new Matrix3();
    private static Matrix3 rotationTrans = new Matrix3();
    private static Matrix3 localOffTrans = new Matrix3();

    public static void updateDrawTransform(Vector2 pos, MapProperties properties,
                    TextureRegion tr, Affine2 drawTransform) {
        updateDrawTransform(pos, properties, tr, drawTransform, 1, 1);
    }

    public static void updateDrawTransform(Vector2 pos, MapProperties properties,
                    TextureRegion tr, Affine2 drawTransform, float scaleX, float scaleY) {
        float lx, ly, objW, objH, rotation, sclX, sclY;
        sclX = properties.get(SpatialProperty.scaleX.name, 1f, Float.class);
        sclY = properties.get(SpatialProperty.scaleY.name, 1f, Float.class);
        objW = properties.get(SpatialProperty.width.name, 0f, Float.class);
        objH = properties.get(SpatialProperty.height.name, 0f, Float.class);

        rotation = properties.get(SpatialProperty.rotation.name, 0f, Float.class);

        boolean flipX = properties.get(SpatialProperty.flipX.name, false, Boolean.class);
        boolean flipY = properties.get(SpatialProperty.flipY.name, false, Boolean.class);

        lx = properties.get(SpatialProperty.localX.name, 0f, Float.class) * scaleX;
        ly = properties.get(SpatialProperty.localY.name, 0f, Float.class) * scaleY;

        float offX = 0, offY = 0;

        if (tr instanceof AtlasRegion) {
            AtlasRegion ar = (AtlasRegion) tr;
            offX = ar.offsetX * 2 * sclX * scaleX;
            offY = ar.offsetY * 2 * sclY * scaleY;
        }

        setTransform(drawTransform, objW, objH, pos.x, pos.y, lx, ly, offX,
                        offY, flipX, flipY, rotation);
    }

    public static void updateDrawTransform(EJXEntity entity, Affine2 drawTransform) {
        float objW, objH, rotation, sclX, sclY;
        sclX = entity.getScaleX();
        sclY = entity.getScaleY();
        objW = entity.getScaledWidth();
        objH = entity.getScaledHeight();
        rotation = entity.getRotation();
        boolean flipX = entity.isFlipX();
        boolean flipY = entity.isFlipY();
        float offX = 0, offY = 0;

        if (entity.getFrame() instanceof AtlasRegion) {
            AtlasRegion ar = entity.getFrame();
            offX = ar.offsetX * 2 * sclX;
            offY = ar.offsetY * 2 * sclY;
        }
        entity.getLocalPosition(tmpVec);
        setTransform(drawTransform, objW, objH, tmpVec.x, tmpVec.y, 0, 0, offX,
                        offY, flipX, flipY, rotation);
    }

    public static void updateDrawTransform(Vector2 pos, VJXProperties properties,
                    TextureRegion tr, Affine2 drawTransform) {
        float lx, ly, objW, objH, rotation, sclX, sclY;
        sclX = properties.get(SpatialProperty.scaleX, 1f, Float.class);
        sclY = properties.get(SpatialProperty.scaleY, 1f, Float.class);
        objW = properties.get(SpatialProperty.width, 0f, Float.class);
        objH = properties.get(SpatialProperty.height, 0f, Float.class);

        rotation = properties.get(SpatialProperty.rotation, 0f, Float.class);

        boolean flipX = properties.get(SpatialProperty.flipX, false,
                        Boolean.class);
        boolean flipY = properties.get(SpatialProperty.flipY, false,
                        Boolean.class);

        lx = properties.get(SpatialProperty.localX, 0f, Float.class);
        ly = properties.get(SpatialProperty.localY, 0f, Float.class);

        float offX = 0, offY = 0;

        if (tr instanceof AtlasRegion) {
            AtlasRegion ar = (AtlasRegion) tr;
            offX = ar.offsetX * 2 * sclX;
            offY = ar.offsetY * 2 * sclY;
        }

        setTransform(drawTransform, objW, objH, pos.x, pos.y, lx, ly, offX,
                        offY, flipX, flipY, rotation);
    }

    //    public static void updateDrawTransform(Vector2 pos,
    //                    VJXProperties properties,
    //                    TextureRegion tr, Affine2 drawTransform, float scaleX,
    //                    float scaleY) {
    //        float lx, ly, objW, objH, rotation, sclX, sclY;
    //        sclX = properties.get(SpatialProperty.scaleX, 1f, Float.class);
    //        sclY = properties.get(SpatialProperty.scaleY, 1f, Float.class);
    //        objW = properties.get(SpatialProperty.width, 0f, Float.class);
    //        objH = properties.get(SpatialProperty.height, 0f, Float.class);
    //
    //        rotation = properties.get(SpatialProperty.rotation, 0f, Float.class);
    //
    //        boolean flipX = properties.get(SpatialProperty.flipX, false, Boolean.class);
    //        boolean flipY = properties.get(SpatialProperty.flipY, false, Boolean.class);
    //
    //        lx = properties.get(SpatialProperty.localX, 0f, Float.class) * scaleX;
    //        ly = properties.get(SpatialProperty.localY, 0f, Float.class) * scaleY;
    //
    //        float offX = 0, offY = 0;
    //
    //        if (tr instanceof AtlasRegion) {
    //            AtlasRegion ar = (AtlasRegion) tr;
    //            offX = ar.offsetX * 2 * sclX * scaleX;
    //            offY = ar.offsetY * 2 * sclY * scaleY;
    //        }
    //
    //        setTransform(drawTransform, objW, objH, pos.x, pos.y, lx, ly, offX,
    //                        offY, flipX, flipY, rotation);
    //    }

    private static void setTransform(Affine2 drawTransform, float width, float height,
                    float x, float y, float lx, float ly, float offX,
                    float offY, boolean flipX, boolean flipY, float rotation) {
        float sclX, sclY;
        drawTransform.idt();

        tmpVec.set(lx + offX, ly + offY);
        drawTransform.translate(tmpVec);

        drawTransform.translate(-(tmpVec.x - (x + lx)), -(tmpVec.y - (y + ly)));
        drawTransform.rotate(rotation);
        sclX = flipX ? -1 : 1;
        sclY = flipY ? -1 : 1;

        // translate to draw
        drawTransform.scale(sclX, sclY);
        drawTransform.translate(tmpVec.x - (x + lx), tmpVec.y - (y + ly));

        tmpVec.setZero();
        if (flipX) tmpVec.x -= width;
        if (flipY) tmpVec.y -= height;

        drawTransform.translate(tmpVec);
        drawTransform.translate(x, y);
    }

    public static FixtureDef createChain(Vector2[] verts, int x, int y,
                    boolean flipX, boolean flipY, int rotation, String physCat,
                    short b2dMask) {
        if (verts == null || verts.length == 0) return null;

        ChainShape cShape = new ChainShape();
        FixtureDef fixDef;
        String[] pCats = physCat.split("_");
        short tmpCat = 0;
        for (String pCat : pCats) {
            tmpCat |= B2DBit.get(pCat).id;
        }

        cShape.createLoop(verts);

        fixDef = new FixtureDef();
        fixDef.shape = cShape;
        fixDef.filter.categoryBits = tmpCat;
        fixDef.filter.maskBits = b2dMask;

        fixDef.restitution = 0f;
        fixDef.friction = 0f;
        fixDef.isSensor = physCat.contains("climbable") ? true : false;

        return fixDef;
        //        fixtureDefs.add(fixDef);
    }

    public static FixtureDef createPolys(Vector2[] verts, String physCat,
                    short b2dMask) {
        if (verts == null || verts.length == 0) return null;
        //        VJXLogger.log(LoggerCategory.ALL, "createPolys:" + this + ",physCat " + physCat
        //                                          + ",b2dMask " + b2dMask);

        PolygonShape pShape = new PolygonShape();
        FixtureDef fixDef;
        String[] pCats = physCat.split("_");
        short tmpCat = 0;
        for (String pCat : pCats) {
            tmpCat |= B2DBit.get(pCat).id;
        }

        pShape.set(verts);

        fixDef = new FixtureDef();
        fixDef.shape = pShape;
        fixDef.filter.categoryBits = tmpCat;
        fixDef.filter.maskBits = b2dMask;

        fixDef.restitution = 0f;
        fixDef.friction = 0f;
        fixDef.isSensor = physCat.contains("climbable") ? true : false;

        return fixDef;
        //        fixtureDefs.add(fixDef);
    }

    public static int convertRotation(int rotation) {
        switch (rotation) {
            case Cell.ROTATE_90:
                rotation = 90;
                break;
            case Cell.ROTATE_180:
                rotation = 180;
                break;
            case Cell.ROTATE_270:
                rotation = 270;
                break;
            default:
                rotation = 0;
                break;
        }
        return rotation;
    }

    public static void convertPolyPoints(Vector2[] verts, int x, int y, int rotation,
                    boolean flipX, boolean flipY, int width, int height) {
        if (verts == null || verts.length == 0) return;

        float xOff = x;
        float yOff = y;

        flipTrans.idt();
        rotationTrans.idt();
        localOffTrans.idt();

        rotationTrans.rotate(rotation);
        localOffTrans.translate(width / 2f, height / 2f);

        if (flipX) flipTrans.scale(-1, 1);
        if (flipY) flipTrans.scale(1, -1);

        for (int i = 0; i < verts.length; i++) {
            verts[i].scl(width, height);
            verts[i].mul(localOffTrans.inv());
            verts[i].mul(flipTrans);
            verts[i].mul(rotationTrans);
            verts[i].mul(localOffTrans.inv());
            verts[i].add(xOff, yOff).scl(.01f);
        }
    }

    /**
     * Adds padding to the given input tile set image and outputs it to the given
     * destination or the default sub folder "processed". If intended it can also
     * extract the single tiles in the image.
     *
     * @param src - {@link FileHandle} to the image that should be padded.
     * @param tileW - The width of the tiles inside the image.
     * @param tileH - The height of the tiles inside the image.
     * @param padX - The target amount of padding to add on the x-axis.
     * @param padY - The target amount of padding to add on the y-axis.
     * @param duplicatePad - Whether to use duplicate padding or not.
     * @param subDir - The relative path to the destination output folder.
     * @param suffix - The suffix used when extracting single tile images.
     * @param extract - Whether to extract single tiles or not.
     *
     * @author Torge Rothe ( X-Ray-Jin )
     */
    public static void padTilesInImage(FileHandle src,
                    int tileW, int tileH, int padX, int padY,
                    boolean duplicatePad, String subDir, String suffix,
                    boolean extract) {
        String USCORE = "_";
        String SLASH = "/";
        String EMPTY = "";

        suffix = suffix == null ? EMPTY
                        : suffix.isEmpty() ? suffix : USCORE + suffix;
        subDir = subDir == null ? "processed"
                        : subDir.isEmpty() ? subDir
                                        : subDir + SLASH;

        String fileName = src.nameWithoutExtension();
        String ext = "." + src.extension();
        String tilesPath = src.nameWithoutExtension() + SLASH;

        String destPath = src.path().replace(src.name(), EMPTY);
        FileHandle dest;

        Pixmap source = new Pixmap(src);
        int w = source.getWidth();
        int h = source.getHeight();

        int tx = w / tileW;
        int ty = h / tileH;
        int addW = 0;
        int addH = 0;

        Pixmap tmpTile;
        addW = tx * padX + padX;
        addH = ty * padY + padY;
        tmpTile = new Pixmap(tileW + padX * 2, tileH + padY * 2,
                        Format.RGBA8888);
        Pixmap target = new Pixmap(w + addW, h + addH, Format.RGBA8888);

        target.setBlending(Blending.None);

        int posX, posY, offX, offY, color;
        for (int x = 0; x < tx; x++) {
            for (int y = 0; y < ty; y++) {
                posX = x * tileW;
                posY = y * tileH;

                offX = x * padX + padX;
                offY = y * padY + padY;

                tmpTile.setColor(0);
                tmpTile.fill();
                for (int tmpX = 0; tmpX < tileW; tmpX++) {
                    for (int tmpY = 0; tmpY < tileH; tmpY++) {
                        color = source.getPixel(posX + tmpX, posY + tmpY);

                        target.drawPixel(posX + tmpX + offX, posY + tmpY + offY,
                                        color);
                        tmpTile.drawPixel(tmpX + padX, tmpY + padY, color);

                        if (duplicatePad) {
                            // duplicate left
                            if (tmpX == 0) {
                                for (int px = 1; px <= padX / 2; px++) {
                                    target.drawPixel(posX + tmpX + offX - px,
                                                    posY + tmpY + offY, color);
                                    tmpTile.drawPixel(tmpX + padX - px,
                                                    tmpY + padY,
                                                    color);
                                }
                            }

                            // duplicate top
                            if (tmpY == 0) {
                                for (int py = 1; py <= padY / 2; py++) {
                                    target.drawPixel(posX + tmpX + offX,
                                                    posY + tmpY + offY - py,
                                                    color);
                                    tmpTile.drawPixel(tmpX + padX,
                                                    tmpY + padY - py,
                                                    color);
                                }
                            }

                            //duplicate top left
                            if (tmpX == 0 && tmpY == 0) {
                                for (int px = 1; px <= padX / 2; px++) {
                                    for (int py = 1; py <= padY / 2; py++) {
                                        target.drawPixel(
                                                        posX + tmpX + offX - px,
                                                        posY + tmpY + offY - py,
                                                        color);
                                        tmpTile.drawPixel(tmpX + padX - px,
                                                        tmpY + padY - py,
                                                        color);
                                    }
                                }
                            }

                            //duplicate right
                            if (tmpX == tileW - 1) {
                                for (int px = 1; px <= padX / 2; px++) {
                                    target.drawPixel(posX + tmpX + offX + px,
                                                    posY + tmpY + offY, color);
                                    tmpTile.drawPixel(tmpX + padX + px,
                                                    tmpY + padY,
                                                    color);
                                }
                            }

                            //duplicate bottom
                            if (tmpY == tileH - 1) {
                                for (int py = 1; py <= padY / 2; py++) {
                                    target.drawPixel(posX + tmpX + offX,
                                                    posY + tmpY + offY + py,
                                                    color);
                                    tmpTile.drawPixel(tmpX + padX,
                                                    tmpY + padY + py,
                                                    color);
                                }
                            }

                            //duplicate bottom left
                            if (tmpX == 0 && tmpY == tileH - 1) {
                                for (int px = 1; px <= padX / 2; px++) {
                                    for (int py = 1; py <= padY / 2; py++) {
                                        target.drawPixel(
                                                        posX + tmpX + offX - px,
                                                        posY + tmpY + offY + py,
                                                        color);
                                        tmpTile.drawPixel(tmpX + padX - px,
                                                        tmpY + padY + py,
                                                        color);
                                    }
                                }
                            }

                            //duplicate top right
                            if (tmpX == tileW - 1 && tmpY == 0) {
                                for (int px = 1; px <= padX / 2; px++) {
                                    for (int py = 1; py <= padY / 2; py++) {
                                        target.drawPixel(
                                                        posX + tmpX + offX + px,
                                                        posY + tmpY + offY - py,
                                                        color);
                                        tmpTile.drawPixel(tmpX + padX + px,
                                                        tmpY + padY - py,
                                                        color);
                                    }
                                }
                            }

                            //duplicate bottom right
                            if (tmpX == tileW - 1 && tmpY == tileH - 1) {
                                for (int px = 1; px <= padX / 2; px++) {
                                    for (int py = 1; py <= padY / 2; py++) {
                                        target.drawPixel(
                                                        posX + tmpX + offX + px,
                                                        posY + tmpY + offY + py,
                                                        color);
                                        tmpTile.drawPixel(tmpX + padX + px,
                                                        tmpY + padY + py,
                                                        color);
                                    }
                                }
                            }
                        }
                    }
                }

                if (extract) {
                    dest = Gdx.files.absolute(destPath + subDir + tilesPath
                                    + fileName + USCORE + y + USCORE + x
                                    + suffix + ext);
                    PixmapIO.writePNG(dest, tmpTile);
                }
            }
        }

        dest = Gdx.files.absolute(destPath + subDir + fileName + suffix + ext);
        PixmapIO.writePNG(dest, target);

        System.out.println("VJXGraphix tiles padded successfully. Saved: " + dest);
    }

    public static Array<MapObject> getObjectsByLayer(TiledMap map,
                    String layerName,
                    Array<MapObject> targetArray, boolean contains) {
        MapLayers layers = map.getLayers();
        for (MapLayer layer : layers) {
            if (layer.getName().equals(layerName) || contains
                            && layer.getName().contains(layerName)) {
                for (MapObject mObj : layer.getObjects()) {
                    targetArray.add(mObj);
                }
            }
        }
        return targetArray;
    }

    //        KawaidaStats ks = KawaidaStats.getInstance();
    public static Pixmap drawFOW(int fw, int fh,TiledMap map, Pixmap mask) {

        Pixmap sepia;
        Drawable sepiaDrawable;
        Image sepiaImage;

        sepia = new Pixmap(fw, fh,
                        Format.RGBA8888);
        sepiaDrawable = new TextureRegionDrawable(
                        new TextureRegion(new Texture(sepia)));
        sepiaImage = new Image(sepiaDrawable);
        sepiaImage.setTouchable(Touchable.disabled);

        float x, y, w, h, rotation;
        boolean flipX, flipY;
        Array<MapObject> objects = new Array<>();
        Vector2 pos = new Vector2();

        sepia.setBlending(Blending.None);
        //        sepia.setColor(1, .5f, 0, .5f);
        //        sepia.fill();

        getObjectsByLayer(map, "mask", objects, false);
        for (MapObject obj : objects) {
            if (obj == null) continue;

            int id = obj.getProperties().get("gid", Integer.class);
            id = id & ~0xE0000000;

            //                dir = flipHorizontally ? Orientation.LEFT : Orientation.RIGHT;

            //            TiledMapTile tile = map.getTileSets().getTile(id);

            flipX = obj.getProperties().get("flipX", Boolean.class);
            flipY = obj.getProperties().get("flipY", Boolean.class);
            rotation = obj.getProperties().get("rotation", Float.class);
            w = obj.getProperties().get("width", Integer.class);
            h = obj.getProperties().get("height", Integer.class);

            x = obj.getProperties().get("x", Float.class);
            y = sepiaImage.getHeight()
                            - obj.getProperties().get("y", Float.class)
                            - h;

            if (mask == null) continue;

            for (int px = 0; px < mask.getWidth(); px++) {
                for (int py = 0; py < mask.getHeight(); py++) {
                    int colorInt = mask.getPixel(px, py);

                    pos.set(px, py);
                    pos.rotate(-rotation);
                    Math.round(pos.x);
                    Math.round(pos.y);

                    if (rotation == 90) {
                        pos.y += w - 1;
                    } else {
                        if (rotation == -180) {
                        } else if (rotation == -90) {
                            pos.x += h - 1;
                        }
                    }

                    if (flipX) {
                        pos.scl(-1, 1);
                        pos.add(w - 1, 0);
                    }
                    if (flipY) {
                        pos.scl(1, -1);
                        pos.add(0, h - 1);
                    }
                    Math.round(pos.x);
                    Math.round(pos.y);

                    if (colorInt != 0) {
                        sepia.drawPixel((int) x + Math.round(pos.x),
                                        (int) y + Math.round(pos.y), 0);

                    }
                }
            }
        }
        sepiaDrawable = new TextureRegionDrawable(
                        new TextureRegion(new Texture(sepia)));

        sepiaImage.setDrawable(sepiaDrawable);
        sepia.setBlending(Blending.SourceOver);

        return sepia;
    }

    public static int alphaStamp(int x, int y, Pixmap source,
                    Pixmap alphaMask, float threshold) {
        //        float removedAlpha = 0;
        float srcAlpha, maskAlpha, newAlpha;
        //        float totalAlpha = source.getWidth() * source.getHeight();
        int count = 0;
        source.setBlending(Blending.None);
        x = x - alphaMask.getWidth() / 2;
        y = source.getHeight() - 1 - y - (alphaMask.getHeight() - 1) / 2;
        for (int mx = 0; mx < alphaMask.getWidth(); mx++) {
            for (int my = 0; my < alphaMask.getHeight(); my++) {
                tmpColor1.set(alphaMask.getPixel(mx, my));
                maskAlpha = tmpColor1.a;
                //                if (maskAlpha <= 0) continue;
                //                if (maskAlpha >= threshold) maskAlpha = 1;

                tmpColor0.set(source.getPixel(x + mx, y + my));
                //                VJXLogger.log("source alpha " + tmpColor0.a);
                srcAlpha = tmpColor0.a;
                //                if (srcAlpha <= 0) continue;

                maskAlpha = maskAlpha > srcAlpha ? srcAlpha : maskAlpha;
                newAlpha = srcAlpha - maskAlpha;

                //                removedAlpha += maskAlpha;
                tmpColor0.a = newAlpha;
                if (srcAlpha != 0 && newAlpha <= 0) count++;
                source.setColor(tmpColor0);
                source.drawPixel(x + mx, y + my);
            }
        }
        //        VJXLogger.log("removed " + removedAlpha + " / " + totalAlpha);
        return count;
    }

    public static Color createColor(float r, float g, float b, float a) {
        return new Color(r / 255, g / 255, b / 255, a);
    }

    public static final Color tmpColor0 = new Color();
    public static final Color tmpColor1 = new Color();
    public static final Color invCol1 = createColor(0, 0, 0, 0);
    public static final Color invCol2 = createColor(1, 0, 0, 1);

    public static void invertPixmap(Pixmap pm) {
        pm.setBlending(Blending.None);
        for (int y = 0; y < pm.getHeight(); y++) {
            for (int x = 0; x < pm.getWidth(); x++) {
                if (pm.getPixel(x, y) == 0) pm.setColor(invCol2);
                else pm.setColor(invCol1);
                pm.drawPixel(x, y);
            }
        }
    }

    public static void cropPixmap(Pixmap source, Pixmap mask) {
        source.setBlending(Blending.None);
        for (int y = 0; y < source.getHeight(); y++) {
            for (int x = 0; x < source.getWidth(); x++) {
                if (mask.getPixel(x, y) != 0) {
                    source.setColor(invCol1);
                    source.drawPixel(x, y);
                }
            }
        }
    }

    public static Image createBorderedImage(Drawable tr, Color borderColor,
                    int borderWidth) {
        if (tr instanceof SpriteDrawable)
            return createBorderedImage((SpriteDrawable) tr, borderColor,
                            borderWidth);
        else if (tr instanceof TextureRegionDrawable)
            return createBorderedImage((TextureRegionDrawable) tr, borderColor,
                            borderWidth);
        else return null;
    }

    private static Image createBorderedImage(SpriteDrawable sd,
                    Color boderColor, int borderWidth) {
        Sprite sprite = sd.getSprite();
        Pixmap pm = new Pixmap(sprite.getRegionWidth() + borderWidth * 2,
                        sprite.getRegionHeight() + borderWidth * 2,
                        Format.RGBA8888);
        pm.setColor(boderColor);
        pm.fill();

        Texture texture = sprite.getTexture();
        if (!texture.getTextureData().isPrepared()) {
            texture.getTextureData().prepare();
        }
        Pixmap pixmap = texture.getTextureData().consumePixmap();

        for (int x = 0; x < sprite.getRegionWidth(); x++) {
            for (int y = 0; y < sprite.getRegionHeight(); y++) {
                int colorInt = pixmap.getPixel(sprite.getRegionX() + x,
                                sprite.getRegionY() + y);
                pm.drawPixel(x + borderWidth, y + borderWidth, colorInt);
            }
        }

        return new Image(new Texture(pm));
    }

    private static Image createBorderedImage(TextureRegionDrawable trd,
                    Color boderColor, int borderWidth) {
        TextureRegion tr = trd.getRegion();
        Pixmap pm = new Pixmap(tr.getRegionWidth() + borderWidth * 2,
                        tr.getRegionHeight() + borderWidth * 2,
                        Format.RGBA8888);
        pm.setColor(boderColor);
        pm.fill();

        Texture texture = tr.getTexture();
        if (!texture.getTextureData().isPrepared()) {
            texture.getTextureData().prepare();
        }
        Pixmap pixmap = texture.getTextureData().consumePixmap();

        for (int x = 0; x < tr.getRegionWidth(); x++) {
            for (int y = 0; y < tr.getRegionHeight(); y++) {
                int colorInt = pixmap.getPixel(tr.getRegionX() + x,
                                tr.getRegionY() + y);
                pm.drawPixel(x + borderWidth, y + borderWidth, colorInt);
            }
        }

        return new Image(new Texture(pm));
    }

    public static void drawButWindow(Pixmap pm, Color c, int x, int y, int w,
                    int h, int r, int offset) {
        pm.setBlending(Blending.None);
        pm.setColor(c);

        //top left circle
        pm.fillCircle(x + r + offset, y + r, r);
        //top right circle
        pm.fillCircle(x + w - r, y + r + offset, r);
        //bottom left circle
        pm.fillCircle(x + r, y + h - r, r);
        //bottom right circle
        pm.fillCircle(x + w - r, y + h - r - offset, r);
        //        pm.setColor(Color.BLUE);
        //top triangle
        pm.fillTriangle(x + r + offset, y, x + w - r, y + offset,
                        x + r + offset, y + offset);
        //left triangle
        pm.fillTriangle(x + offset, y + r, x + offset, y + h - r, x, y + h - r);
        //bot triangle
        pm.fillTriangle(x + r, y + h - offset - 1, x + w - r,
                        y + h - offset - 1, x + r, y + h);

        //center rectangle
        //        pm.setColor(Color.RED);
        pm.fillRectangle(x + offset, y + offset, w - offset - r,
                        h - 2 * offset + 1);
        //right rectangle
        //        pm.setColor(Color.GREEN);
        pm.fillRectangle(x + w - r, y + offset + r, r + 1,
                        h - 2 * offset - 2 * r);
        //top left rectangle
        pm.fillRectangle(x + offset, y + r, r, r);
    }

    public static void drawGradButWindow(Pixmap pm, Color c1, Color c2, int x,
                    int y, int w, int h, int r, int offset) {
        Pixmap source = new Pixmap(pm.getWidth(), pm.getHeight(),
                        Format.RGBA8888);
        Pixmap mask = new Pixmap(pm.getWidth(), pm.getHeight(),
                        Format.RGBA8888);
        drawButWindow(mask, c1, x, y, w, h, r, offset);
        invertPixmap(mask);
        drawGradRect(source, c1, c2, x, y, w, h);
        cropPixmap(source, mask);
        pm.setBlending(Blending.SourceOver);
        pm.drawPixmap(source, 0, 0);
        mask.dispose();
        source.dispose();
    }

    public static void drawDiaWindow(Pixmap pm, Color c, int x, int y, int w,
                    int h, int r, int offset) {
        pm.setBlending(Blending.None);
        pm.setColor(c);
        //        pm.setColor(Color.RED);
        //top left circle
        pm.fillCircle(x + r, y + r, r);
        //top right circle
        pm.fillCircle(x + w - r, y + r, r);
        //bottom left circle
        pm.fillCircle(x + offset, y + h - r, r);
        //bottom right circle
        pm.fillCircle(x + w - r, y + h - offset, r);

        //left triangle
        pm.fillTriangle(x, y + r, x + offset - r, y + r, x + offset - r,
                        y + h - r);
        //bottom triangle
        pm.fillTriangle(x + offset, y + h - offset + r, x + w - r,
                        y + h - offset + r, x + offset, y + h);
        //right rectangle
        //        pm.setColor(Color.BLUE);
        pm.fillRectangle(x + w - 2 * r, y + r, 2 * r + 1, h - r - offset);
        //mid rectangle
        pm.fillRectangle(x + r, y, w - 2 * r, h - r);
    }

    public static void drawGradDiaWindow(Pixmap pm, Color c1, Color c2, int x,
                    int y, int w, int h, int r, int offset) {

        //        Pixmap.setBlending(Blending.None);
        //        pm.setColor(c1);
        //        pm.fillCircle(x + r, y + r, r);
        //        pm.fillRectangle(x + r, y + 0, w - 2 * r, 2 * r);
        //        pm.fillCircle(x + w - 1 * r, y + r, r);
        //
        //        pm.setColor(c2);
        //        pm.fillCircle(x + left, y + h - r, r);
        //        pm.fillCircle(x + w - r, y + h - bot, r);
        //        pm.fillRectangle(x + left - r, y + h - 3 * r, r, r * 2);
        //        pm.fillRectangle(x + left - r, y + h - 4 * r, w - left, r);
        //        pm.fillTriangle(x + left, y + h - bot + r, x + w - r, y + h - bot + r,
        //                        x + left, y + h);
        //
        //        //        drawGradRect(pm, c1, c2, x + left - r, y + r, w - left + r + 1,
        //        //                        h - bot - r);
        //        drawGradRect(pm, c1, c2, x + left - r, y + r, w - left + r + 1,
        //                        h - bot - r);
        //        //        drawGradTri(pm, c1, c2, x, y + r, left - r, h - r - bot);
        //        drawGradTri(pm, c1, c2, x, y + r, left - r, h - 2 * r, bot - r);
        Pixmap source = new Pixmap(pm.getWidth(), pm.getHeight(),
                        Format.RGBA8888);
        Pixmap mask = new Pixmap(pm.getWidth(), pm.getHeight(),
                        Format.RGBA8888);
        drawDiaWindow(mask, c1, x, y, w, h, r, offset);
        invertPixmap(mask);
        drawGradRect(source, c1, c2, x, y, w, h);
        cropPixmap(source, mask);
        pm.setBlending(Blending.SourceOver);
        pm.drawPixmap(source, 0, 0);
        mask.dispose();
        source.dispose();
    }

    //    public static void drawGradTri(Pixmap pm, Color c1, Color c2, int x, int y,
    //                    int w, int h) {
    //        drawGradTri(pm, c1, c2, x, y, w, h, 0);
    //    }

    //    public static void drawGradTri(Pixmap pm, Color c1, Color c2, int x, int y,
    //                    int w, int h, int offset) {
    //        //        Pixmap.setBlending(Blending.SourceOver);
    //        //        for (int i = 0; i < h; i++) {
    //        //            int wid = (int) Math.max(w - i * (float) w / h, 1);
    //        //            pm.setColor(c1);
    //        //            pm.fillRectangle(x + w - wid, y + i, wid, 1);
    //        //            if (i > h - offset - 1) pm.setColor(c2);
    //        //            else pm.setColor(c2.r, c2.g, c2.b, (float) i / (h + 1 - offset));
    //        //            pm.fillRectangle(x + w - wid, y + i, wid, 1);
    //        //        }
    //        drawGradTri(pm, c1, c2, x, y, w, h, 0, 0);
    //    }

    //    public static void drawGradTri(Pixmap pm, Color c1, Color c2, int x, int y,
    //                    int w, int h, int offsetTop, int offsetBot) {
    //        Pixmap.setBlending(Blending.SourceOver);
    //        for (int i = 0; i < h; i++) {
    //            int wid = (int) Math.max(w - i * (float) w / h, 1);
    //            pm.setColor(c1);
    //            pm.fillRectangle(x + w - wid, y + i, wid, 1);
    //            if (i >= offsetTop) {
    //                if (i > h - offsetBot - 1) pm.setColor(c2);
    //                else pm.setColor(c2.r, c2.g, c2.b,
    //                                (float) i / (h + 1 - offsetBot - offsetTop));
    //                pm.fillRectangle(x + w - wid, y + i, wid, 1);
    //            }
    //        }
    //    }

    public static void drawRect(Pixmap pm, Color c, int x, int y, int w,
                    int h) {
        pm.setBlending(Blending.None);
        pm.setColor(c);
        pm.fillRectangle(x, y, w, h);
    }

    public static void drawRoundRect(Pixmap pm, Color c, int x, int y, int w,
                    int h, int r) {
        pm.setBlending(Blending.None);
        pm.setColor(c);
        pm.fillCircle(r + x, r + y, r);
        pm.fillCircle(w - r + x, r + y, r);
        pm.fillCircle(w - r + x, h - r + y, r);
        pm.fillCircle(r + x, h - r + y, r);
        //mid rectangle
        pm.fillRectangle(r + x, y, w - 2 * r, h + 1);
        //left rectangle
        pm.fillRectangle(x, r + y, 2 * r, h - 2 * r);
        //right rectangle
        pm.fillRectangle(w - 2 * r + x, r + y, 2 * r + 1, h - 2 * r);
    }

    public static void drawRoundGradRect(Pixmap pm, Color c1, Color c2, int x,
                    int y, int w, int h, int r) {
        pm.setBlending(Blending.None);
        pm.setColor(c1);
        pm.fillCircle(r + x, r + y, r);
        pm.fillCircle(w - r + x, r + y, r);
        pm.fillRectangle(x + r, y, w - 2 * r, 2 * r);

        pm.setColor(c2);
        pm.fillCircle(w - r + x, h - r + y, r);
        pm.fillCircle(r + x, h - r + y, r);
        //bottom rectangle
        pm.fillRectangle(x + r, y + h - 2 * r, w - 2 * r, 2 * r + 1);
        drawGradRect(pm, c1, c2, x, y + r, w, h - 2 * r);
    }

    public static void drawGradRect(Pixmap pm, Color c1, Color c2, int x, int y,
                    int w, int h) {
        pm.setBlending(Blending.SourceOver);
        for (int i = 0; i < h; i++) {
            pm.setColor(c1);
            pm.fillRectangle(x, y + i, w + 1, 1);
            pm.setColor(c2.r, c2.g, c2.b, (float) i / (h + 1));
            pm.fillRectangle(x, y + i, w + 1, 1);
        }
    }

    public static void drawRadio(Pixmap pm, Color c, int x, int y, int r1,
                    int r2) {
        Pixmap source = new Pixmap(pm.getWidth(), pm.getHeight(),
                        Format.RGBA8888);
        Pixmap mask = new Pixmap(pm.getWidth(), pm.getHeight(),
                        Format.RGBA8888);
        mask.setColor(c);
        mask.fillCircle(x + r1, y + r1, r2);
        //        invertPixmap(mask);
        source.setColor(c);
        source.fillCircle(x + r1, y + r1, r1);
        cropPixmap(source, mask);
        pm.setBlending(Blending.SourceOver);
        pm.drawPixmap(source, 0, 0);
        mask.dispose();
        source.dispose();
    }

    public static void drawGradRadio(Pixmap pm, Color c1, Color c2, int x,
                    int y, int r1, int r2) {
        Pixmap source = new Pixmap(pm.getWidth(), pm.getHeight(),
                        Format.RGBA8888);
        Pixmap mask = new Pixmap(pm.getWidth(), pm.getHeight(),
                        Format.RGBA8888);
        mask.setColor(c1);
        mask.fillCircle(x + r1, y + r1, r2);
        //        invertPixmap(mask);
        source.setColor(c1);
        source.fillCircle(x + r1, y + r1, r1);
        cropPixmap(source, mask);
        pm.setBlending(Blending.SourceOver);
        pm.drawPixmap(source, 0, 0);
        mask.dispose();
        source.dispose();
    }

    public static void drawGradCircle(Pixmap pm, Color c1, Color c2, int x,
                    int y, int r) {
        Pixmap source = new Pixmap(pm.getWidth(), pm.getHeight(),
                        Format.RGBA8888);
        Pixmap mask = new Pixmap(pm.getWidth(), pm.getHeight(),
                        Format.RGBA8888);
        pm.setBlending(Blending.None);
        mask.setColor(c1);
        mask.fillCircle(x + r, y + r, r);
        invertPixmap(mask);
        drawGradRect(source, c1, c2, 0, 0, pm.getWidth(), pm.getHeight());
        cropPixmap(source, mask);
        pm.setBlending(Blending.SourceOver);
        pm.drawPixmap(source, 0, 0);
        //        pm.drawPixmap(mask, 0, 0);
        mask.dispose();
        source.dispose();
    }

    public static ArrayList<Vector2> quickHull(ArrayList<Vector2> points) {
        ArrayList<Vector2> convexHull = new ArrayList<>();
        if (points.size() < 3) {
            convexHull.addAll(points);
            return convexHull;
        }
        // find extremals
        int minVector2 = -1, maxVector2 = -1;
        int minX = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;
        for (int i = 0; i < points.size(); i++) {
            if (points.get(i).x < minX) {
                minX = (int) points.get(i).x;
                minVector2 = i;
            }
            if (points.get(i).x > maxX) {
                maxX = (int) points.get(i).x;
                maxVector2 = i;
            }
        }
        Vector2 A = points.get(minVector2);
        Vector2 B = points.get(maxVector2);
        convexHull.add(A);
        convexHull.add(B);
        points.remove(A);
        points.remove(B);

        ArrayList<Vector2> leftSet = new ArrayList<>();
        ArrayList<Vector2> rightSet = new ArrayList<>();

        for (int i = 0; i < points.size(); i++) {
            Vector2 p = points.get(i);
            if (pointLocation(A, B, p) == -1) leftSet.add(p);
            else rightSet.add(p);
        }
        hullSet(A, B, rightSet, convexHull);
        hullSet(B, A, leftSet, convexHull);

        return convexHull;
    }

    /*
     * Computes the square of the distance of point C to the segment defined by points AB
     */
    private static int distance(Vector2 A, Vector2 B, Vector2 C) {
        int ABx = (int) B.x - (int) A.x;
        int ABy = (int) B.y - (int) A.y;
        int num = ABx * (int) (A.y - C.y) - ABy * (int) (A.x - C.x);
        if (num < 0) num = -num;
        return num;
    }

    private static void hullSet(Vector2 A, Vector2 B, ArrayList<Vector2> set,
                    ArrayList<Vector2> hull) {
        int insertPosition = hull.indexOf(B);
        if (set.size() == 0) return;
        if (set.size() == 1) {
            Vector2 p = set.get(0);
            set.remove(p);
            hull.add(insertPosition, p);
            return;
        }
        int dist = Integer.MIN_VALUE;
        int furthestVector2 = -1;
        for (int i = 0; i < set.size(); i++) {
            Vector2 p = set.get(i);
            int distance = distance(A, B, p);
            if (distance > dist) {
                dist = distance;
                furthestVector2 = i;
            }
        }
        Vector2 P = set.get(furthestVector2);
        set.remove(furthestVector2);
        hull.add(insertPosition, P);

        // Determine who's to the left of AP
        ArrayList<Vector2> leftSetAP = new ArrayList<>();
        for (int i = 0; i < set.size(); i++) {
            Vector2 M = set.get(i);
            if (pointLocation(A, P, M) == 1) {
                leftSetAP.add(M);
            }
        }

        // Determine who's to the left of PB
        ArrayList<Vector2> leftSetPB = new ArrayList<>();
        for (int i = 0; i < set.size(); i++) {
            Vector2 M = set.get(i);
            if (pointLocation(P, B, M) == 1) {
                leftSetPB.add(M);
            }
        }
        hullSet(A, P, leftSetAP, hull);
        hullSet(P, B, leftSetPB, hull);

    }

    private static int pointLocation(Vector2 A, Vector2 B, Vector2 P) {
        int cp1 = (int) (B.x - A.x) * (int) (P.y - A.y)
                        - (int) (B.y - A.y) * (int) (P.x - A.x);
        return cp1 > 0 ? 1 : -1;
    }

    private static float _cross(Vector2 o, Vector2 a, Vector2 b) {
        return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
    }

    private static Array<Vector2> _upperTangent(Array<Vector2> pointset) {
        Array<Vector2> lower = new Array<>();
        for (int l = 0; l < pointset.size; l++) {
            while (lower.size >= 2 && _cross(lower.get(lower.size - 2),
                            lower.get(lower.size - 1), pointset.get(l)) <= 0) {
                lower.pop();
            }
            lower.add(pointset.get(l));
        }
        lower.pop();
        return lower;
    }

    private static Array<Vector2> _lowerTangent(Array<Vector2> pointset) {
        pointset.reverse();
        Array<Vector2> upper = new Array<>();
        for (int u = 0; u < pointset.size; u++) {
            while (upper.size >= 2 && _cross(upper.get(upper.size - 2),
                            upper.get(upper.size - 1), pointset.get(u)) <= 0) {
                upper.pop();
            }
            upper.add(pointset.get(u));
        }
        upper.pop();
        return upper;
    }

    // pointset has to be sorted by X
    public static Array<Vector2> convex(Array<Vector2> pointset) {
        Array<Vector2> convex = new Array<>(),
                        upper = _upperTangent(pointset),
                        lower = _lowerTangent(pointset);
        convex.addAll(lower);
        convex.addAll(upper);
        convex.add(pointset.get(0));
        return convex;
    }

    //    private static float pointLocation(Vector2 A, Vector2 B, Vector2 P) {
    //        float cp1 = (B.x - A.x) * (P.y - A.y) - (B.y - A.y) * (P.x - A.x);
    //        return cp1 > 0 ? 1 : -1;
    //    }
    //
    //    private static float distance(Vector2 A, Vector2 B, Vector2 C) {
    //        float ABx = B.x - A.x;
    //        float ABy = B.y - A.y;
    //        float num = ABx * (A.y - C.y) - ABy * (A.x - C.x);
    //        if (num < 0) num = -num;
    //        return num;
    //    }
    //
    //    public static Array<Vector2> quickHull(Array<Vector2> points) {
    //        Array<Vector2> convexHull = new Array<Vector2>();
    //        if (points.size < 3) return new Array<Vector2>(points);
    //        // find extremals
    //        int minPoint = -1, maxPoint = -1;
    //        float minX = Integer.MAX_VALUE;
    //        float maxX = Integer.MIN_VALUE;
    //        for (int i = 0; i < points.size; i++) {
    //            if (points.get(i).x < minX) {
    //                minX = points.get(i).x;
    //                minPoint = i;
    //            }
    //            if (points.get(i).x > maxX) {
    //                maxX = points.get(i).x;
    //                maxPoint = i;
    //            }
    //        }
    //        Vector2 A = points.get(minPoint);
    //        Vector2 B = points.get(maxPoint);
    //        convexHull.add(A);
    //        convexHull.add(B);
    //        points.removeValue(A, false);
    //        points.removeValue(B, false);
    //
    //        Array<Vector2> leftSet = new Array<>();
    //        Array<Vector2> rightSet = new Array<>();
    //
    //        for (int i = 0; i < points.size; i++) {
    //            Vector2 p = points.get(i);
    //            if (pointLocation(A, B, p) == -1) leftSet.add(p);
    //            else rightSet.add(p);
    //        }
    //        hullSet(A, B, rightSet, convexHull);
    //        hullSet(B, A, leftSet, convexHull);
    //
    //        return convexHull;
    //    }
    //
    //    private static void hullSet(Vector2 A, Vector2 B, Array<Vector2> set,
    //                    Array<Vector2> hull) {
    //        //        int insertPosition = hull.indexOf(B);
    //        int insertPosition = hull.indexOf(B, false);
    //        if (set.size == 0) return;
    //        if (set.size == 1) {
    //            Vector2 p = set.get(0);
    //            set.removeValue(p, false);
    //            hull.insert(insertPosition, p);
    //            //            hull.add(insertPosition, p);
    //            return;
    //        }
    //        float dist = Integer.MIN_VALUE;
    //        int furthestPoint = -1;
    //        for (int i = 0; i < set.size; i++) {
    //            Vector2 p = set.get(i);
    //            float distance = distance(A, B, p);
    //            if (distance > dist) {
    //                dist = distance;
    //                furthestPoint = i;
    //            }
    //        }
    //        Vector2 P = set.get(furthestPoint);
    //        set.removeIndex(furthestPoint);
    //        hull.insert(insertPosition, P);
    //
    //        // Determine who's to the left of AP
    //        Array<Vector2> leftSetAP = new Array<>();
    //        for (int i = 0; i < set.size; i++) {
    //            Vector2 M = set.get(i);
    //            if (pointLocation(A, P, M) == 1) {
    //                //set.remove(M);
    //                leftSetAP.add(M);
    //            }
    //        }
    //
    //        // Determine who's to the left of PB
    //        Array<Vector2> leftSetPB = new Array<>();
    //        for (int i = 0; i < set.size; i++) {
    //            Vector2 M = set.get(i);
    //            if (pointLocation(P, B, M) == 1) {
    //                //set.remove(M);
    //                leftSetPB.add(M);
    //            }
    //        }
    //        hullSet(A, P, leftSetAP, hull);
    //        hullSet(P, B, leftSetPB, hull);
    //    }
}
