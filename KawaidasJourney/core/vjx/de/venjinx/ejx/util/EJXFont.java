package de.venjinx.ejx.util;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Disposable;

import de.venjinx.vjx.ui.EJXSkin;

public class EJXFont implements Disposable {

    public static final Color fntColorDflt = VJXGraphix.createColor(42f, 42f, 42f, 1f);
    public static final Color fntColorGreen = VJXGraphix.createColor(126f, 211f, 33f, 1f);
    public static final Color fntColorGray = VJXGraphix.createColor(155f, 155f, 155f, 1f);
    public static final Color fntColorBlue = VJXGraphix.createColor(74f, 144f, 226f, 1f);
    public static final Color fntColorPink = VJXGraphix.createColor(255f, 31f, 118f, 1f);

    public static final Color fntRedD = VJXGraphix.createColor(157f, 39f, 8f, 1f);
    public static final Color fntGreenD = VJXGraphix.createColor(137f, 200f, 157f, 1f);
    public static final Color fntGreenL = VJXGraphix.createColor(226f, 252f, 186f, 1f);
    public static final Color fntGreenW = VJXGraphix.createColor(228f, 242f, 227f, 1f);

    public static final String TITLE_26 = "title_26";
    public static final String TITLE_28 = "title_28";
    public static final String TITLE_32 = "title_32";
    public static final String TITLE_36 = "title_36";
    public static final String TITLE_48 = "title_48";
    public static final String TITLE_64 = "title_64";

    public static final String TEXT_28 = "text_28";
    public static final String TEXT_32 = "text_32";
    public static final String TEXT_36 = "text_36";
    public static final String TEXT_48 = "text_48";
    public static final String TEXT_24_GREEN = "text_24_green";
    public static final String TEXT_32_GREEN = "text_32_green";
    public static final String TEXT_36_GREEN = "text_36_green";
    public static final String TEXT_48_GREEN = "text_48_green";
    public static final String TEXT_32_BLUE = "text_32_blue";
    public static final String TEXT_32_GRAY = "text_36_gray";

    public static final String KANKIN_60 = "kankin_60";
    public static final String KANKIN_128 = "kankin_128";
    public static final String KANKIN_SMALL = "kankin_small";
    public static final String CHAPTER = "chaper";


    public enum FONT_DEF{
        DEF_TITLE_64(TITLE_64, "LondrinaSolid-Regular", 64, Color.WHITE, 2, Color.BLACK,
                        2, 2, EJXSkin.uiTrans50),
        DEF_TITLE_48(TITLE_48, "LondrinaSolid-Regular", 48, Color.WHITE, 2, Color.BLACK,
                        2, 2, EJXSkin.uiTrans50),
        DEF_TITLE_36(TITLE_36, "LondrinaSolid-Regular", 36, Color.WHITE, 2, Color.BLACK,
                        2, 2, EJXSkin.uiTrans50),
        DEF_TITLE_32(TITLE_32, "LondrinaSolid-Regular", 32, Color.WHITE, 2, Color.BLACK,
                        2, 2, EJXSkin.uiTrans50),
        DEF_TITLE_28(TITLE_28, "LondrinaSolid-Regular", 28, Color.WHITE, 2, Color.BLACK,
                        2, 2, EJXSkin.uiTrans50),
        DEF_TITLE_26(TITLE_26, "LondrinaSolid-Regular", 26, Color.WHITE, 2, Color.BLACK,
                        2, 2, EJXSkin.uiTrans50),

        DEF_TEXT_48(TEXT_48, "LondrinaSolid-Regular", 48, fntColorDflt),
        DEF_TEXT_36(TEXT_36, "LondrinaSolid-Regular", 36, fntColorDflt),
        DEF_TEXT_32(TEXT_32, "LondrinaSolid-Regular", 32, fntColorDflt),
        DEF_TEXT_28(TEXT_28, "LondrinaSolid-Regular", 28, fntColorDflt),
        DEF_TEXT_24_GREEN(TEXT_24_GREEN, "LondrinaSolid-Regular", 24, fntColorGreen),
        DEF_TEXT_32_GREEN(TEXT_32_GREEN, "LondrinaSolid-Regular", 32, fntColorGreen),
        DEF_TEXT_36_GREEN(TEXT_36_GREEN, "LondrinaSolid-Regular", 36, fntColorGreen),
        DEF_TEXT_48_GREEN(TEXT_48_GREEN, "LondrinaSolid-Regular", 48, fntColorGreen),
        DEF_TEXT_32_GRAY(TEXT_32_GRAY, "LondrinaSolid-Regular", 32, fntColorGray),
        DEF_TEXT_32_BLUE(TEXT_32_BLUE, "LondrinaSolid-Regular", 32, fntColorBlue),

        DEF_KANKIN_128(KANKIN_128, "Kankin", 128, fntGreenL, 5, fntGreenD, 3, 8, EJXSkin.uiTrans50),
        DEF_KANKIN_60(KANKIN_60, "Kankin", 60, fntGreenL, 5, fntGreenD, 3, 8, EJXSkin.uiTrans50),
        DEF_KANKIN_SMALL(KANKIN_SMALL, "Kankin", 60, fntGreenW, 0, fntGreenD, 2, 4, EJXSkin.uiTrans50),

        DEF_CHAPTER(CHAPTER, "LondrinaSolid-Regular", 48, fntRedD, 4, fntGreenD, 0, 0, EJXSkin.uiBlank),
        ;

        public final String name;
        public final int size;
        public final Color color;
        public final int border;
        public final Color borderColor;
        public final int shadowX;
        public final int shadowY;
        public final Color shadowColor;
        private String generator;

        private FONT_DEF(String name, String generator, int size) {
            this(name, generator, size, Color.BLACK);
        }

        private FONT_DEF(String name, String generator, int size, Color color) {
            this(name, generator, size, color, 0, null, 0, 0, null);
        }

        private FONT_DEF(String name, String generator, int size, Color color, int border,
                        Color borderColor, int shadowX, int shadowY, Color shadowColor) {
            this.name = name;
            this.generator = generator;
            this.size = size;
            this.color = color;
            this.border = border;
            this.borderColor = borderColor;
            this.shadowX = shadowX;
            this.shadowY = shadowY;
            this.shadowColor = shadowColor;
        }
    }

    private HashMap<String, FreeTypeFontGenerator> generators;
    private HashMap<String, BitmapFont> fonts;
    private HashMap<String, LabelStyle> labelStyles;

    //    private BitmapFont defaultFont;
    //    private LabelStyle defaultStyle;

    public EJXFont() {
        fonts = new HashMap<>();
        labelStyles = new HashMap<>();
        generators = new HashMap<>();
        for (FONT_DEF def : FONT_DEF.values())
            initBMF(def);

        for (FreeTypeFontGenerator generator : generators.values())
            generator.dispose();
    }

    @Override
    public void dispose() {
    }

    private void initBMF(FONT_DEF def) {
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = (int) Math.ceil(def.size);
        parameter.color = def.color;
        parameter.borderWidth = def.border;
        parameter.borderColor = def.borderColor;
        parameter.shadowOffsetX = def.shadowX;
        parameter.shadowOffsetY = def.shadowY;
        parameter.shadowColor = def.shadowColor;
        parameter.minFilter = TextureFilter.Linear;
        parameter.magFilter = TextureFilter.Linear;

        FreeTypeFontGenerator generator = getGenerator(def.generator);
        generator.scaleForPixelHeight((int) Math.ceil(def.size));
        BitmapFont font = generator.generateFont(parameter);
        fonts.put(def.name, font);
        addBMF(def.name, font, Color.WHITE);
    }

    private FreeTypeFontGenerator getGenerator(String name) {
        if (generators.containsKey(name))
            return generators.get(name);
        String fileName = "data/fonts/" + name + ".otf";
        FileHandle file = Gdx.files.internal(fileName);
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(file);
        generators.put(name, generator);
        return generator;
    }

    private void addBMF(String name, BitmapFont bmf, Color c) {
        if (!labelStyles.containsKey(name)) {
            LabelStyle style = new LabelStyle(bmf, c);
            labelStyles.put(name, style);
        }
    }

    public BitmapFont getFont(String name) {
        return fonts.get(name);
    }

    public LabelStyle getLabelStyle(String name) {
        return labelStyles.get(name);
    }
}