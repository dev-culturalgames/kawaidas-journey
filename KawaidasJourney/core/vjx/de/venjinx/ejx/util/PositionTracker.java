package de.venjinx.ejx.util;

import com.badlogic.gdx.math.Vector2;

public class PositionTracker {

    private boolean initialized = false;
    private long initialTime = -1L;
    private Vector2 initialPosition = new Vector2();

    private boolean stopped = true;
    private long stopTime = -1L;
    private Vector2 stopPosition = new Vector2();

    private long[] timeStamp;
    private Vector2[] samplePositions;
    private Vector2 velocity = new Vector2();

    private boolean tracking = false;
    private int currentSample = 0;
    private int maxSamples;

    public PositionTracker() {
        this(10);
    }

    public PositionTracker(int maxSamples) {
        this.maxSamples = maxSamples;
        timeStamp = new long[maxSamples];
        samplePositions = new Vector2[maxSamples];

        for (int i = 0; i < maxSamples; i++) {
            samplePositions[i] = new Vector2();
        }
    }

    public void init(float x, float y) {
        reset();

        initialPosition.set(x, y);
        initialTime = System.nanoTime();
        initialized = true;
        stopped = false;
    }

    public void reset() {
        initialized = false;
        initialTime = -1L;
        initialPosition.setZero();

        for (Vector2 pos : samplePositions) {
            pos.setZero();
        }

        velocity.setZero();

        tracking = false;
        currentSample = 0;
    }

    public void track(float x, float y) {
        if (!initialized || stopped) return;

        tracking = true;

        samplePositions[currentSample].set(x, y);
        timeStamp[currentSample] = System.nanoTime();

        currentSample = (currentSample + 1) % maxSamples;
    }

    public void stop(float x, float y) {
        stopPosition.set(x, y);
        stopTime = System.nanoTime();
        stopped = true;
    }

    public Vector2 getPosition(int sampleIndex) {
        if (sampleIndex < 0 || sampleIndex > maxSamples - 1) return null;
        if (!initialized) {
            initialPosition.setZero();
            return initialPosition;
        }
        return samplePositions[sampleIndex];
    }

    public Vector2 getVelocity() {
        velocity.setZero();
        if (!initialized) return velocity;
        long msTime = 0L;
        int previous = (currentSample == 0 ? maxSamples : currentSample) - 1;
        if (stopped) {
            msTime = stopTime - initialTime;
        } else {
            msTime = timeStamp[previous] - initialTime;
        }
        if (msTime <= 0) return velocity;

        msTime /= 1000000;
        float sTime = msTime / 1000f;
        velocity.set(samplePositions[previous]).sub(initialPosition);
        velocity.set(velocity.x / sTime, velocity.y / sTime);
        return velocity;
    }

    public Vector2 getInitialPosition() {
        return initialPosition;
    }

    public long getInitialTime() {
        return initialTime;
    }

    public long getStopTime() {
        return stopTime;
    }

    public long getDuration() {
        return stopTime - initialTime;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public boolean isStopped() {
        return stopped;
    }

    public boolean isTracking() {
        return tracking;
    }

    public int getMaxSamples() {
        return maxSamples;
    }

    @Override
    public String toString() {
        String name = "PositionTracker (" + initialized + ", " + tracking + ", "
                        + stopped + "): " + initialPosition + " -> "
                        + stopPosition + ", " + velocity + ", " + getDuration();
        return name;
    }
}