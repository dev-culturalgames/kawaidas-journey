package de.venjinx.ejx.util;

import java.util.ArrayDeque;
import java.util.HashMap;

public class EJXTimer {

    private static int currentMark = 0;
    private static ArrayDeque<Integer> freeMarks = new ArrayDeque<>();
    private static HashMap<Integer, Long> marks = new HashMap<>();

    public static void setMark(int markID) {
        setMark(markID, false);
    }

    public static void setMark(int markID, boolean nano) {
        if (nano) marks.put(markID, System.nanoTime());
        else marks.put(markID, System.currentTimeMillis());
    }

    public static long timeSince(int markID) {
        return timeSince(markID, false);
    }

    public static long timeSince(int markID, boolean nano) {
        if (!marks.containsKey(markID)) return -1;
        if (nano) return System.nanoTime() - marks.get(markID);
        else return System.currentTimeMillis() - marks.get(markID);
    }

    public static long getTime() {
        return System.currentTimeMillis();
    }

    public static long getTime(boolean nano) {
        if (nano) return System.nanoTime();
        else return System.currentTimeMillis();
    }

    public static int getMark() {
        int id = -1;
        if (!freeMarks.isEmpty()) id = freeMarks.removeFirst();
        else id = currentMark++;

        return id;
    }

    public static void releaseMark(int id) {
        freeMarks.add(id);
        marks.remove(id);
    }

    public static void clearAll() {
        currentMark = 0;
        freeMarks.clear();
        marks.clear();
    }
}