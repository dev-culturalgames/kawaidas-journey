package de.venjinx.ejx.util;

import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.save.LocalSavegame;

public class EJXTresor {

    public interface Lockable {
    }

    private LocalSavegame save;
    private Array<Lockable> items;

    public EJXTresor(LocalSavegame save) {
        this.save = save;
        items = new Array<>();
    }

    public void lock(Lockable item, String saveName) {
        //        VJXLogger.log("lock " + saveName, 1);
        items.add(item);
        save.putBool(saveName, true);
    }

    public void unlock(Lockable item, String saveName) {
        //        VJXLogger.log("unlock " + saveName);
        items.removeValue(item, true);
        save.putBool(saveName, false);
    }

    public boolean isLocked(String saveName) {
        boolean isLocked = save.getBool(saveName, false);
        //        VJXLogger.log("islocked " + saveName + " " + isLocked);
        return isLocked;
    }
}
