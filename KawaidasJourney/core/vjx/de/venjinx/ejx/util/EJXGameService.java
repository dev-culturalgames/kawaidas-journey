package de.venjinx.ejx.util;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public abstract class EJXGameService {

    public static final String CLOUD_AUTOCONNECT = "bool_gps_autoConnect";
    protected static final String CLOUD_CONNECTING = "Cloud service connecting: ";
    protected static final String CLOUD_CONNECTED = "Cloud service connected: ";
    protected static final String CLOUD_DISCONNECTING = "Cloud service disconnecting: ";
    protected static final String CLOUD_DISCONNECTED = "Cloud service disconnected: ";

    public interface GameServiceListener {

        void onConnected();

        void onDisconnected();

        void onSaved();

        void onMenuLoaded();
    }

    protected EJXGame game;
    private GameServiceListener listener;

    private boolean autoConnect = false;
    private boolean connected = false;

    protected abstract void connect();

    public abstract void onConnected();

    protected abstract void disconnect();

    public abstract void onDisconnected();

    public abstract void share();

    public void init() {
    }

    public void setGame(EJXGame game) {
        this.game = game;
    }

    public void setListener(GameServiceListener listener) {
        this.listener = listener;
    }

    public GameServiceListener getListener() {
        return listener;
    }

    public void connectService() {
        VJXLogger.log(LogCategory.INFO, CLOUD_CONNECTING);
        connect();
    }

    public void disconnectCloud() {
        VJXLogger.log(LogCategory.INFO, CLOUD_DISCONNECTING);
        disconnect();
    }

    public boolean isAutoConnect() {
        return autoConnect;
    }

    public boolean isConnected() {
        return connected;
    }

    protected void setConnected(boolean connected) {
        this.connected = connected;
        setAutoConnect(connected);

        if (connected) {
            VJXLogger.log(LogCategory.INFO, CLOUD_CONNECTED + game.getSave().getName());
            if (listener != null) listener.onConnected();
        } else {
            VJXLogger.log(LogCategory.INFO, CLOUD_DISCONNECTED + game.getSave().getName());
            if (listener != null) listener.onDisconnected();
        }
    }

    public void setAutoConnect(boolean autoConnect) {
        this.autoConnect = autoConnect;
        game.getSettings().putBool(CLOUD_AUTOCONNECT, autoConnect);
    }
}