package de.venjinx.ejx.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

/*******************************************************************************
 * Copyright 2011 See AUTHORS file. Licensed under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 ******************************************************************************/

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.assets.loaders.resolvers.AbsoluteFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.ImageResolver;
import com.badlogic.gdx.maps.ImageResolver.AssetManagerImageResolver;
import com.badlogic.gdx.maps.ImageResolver.DirectImageResolver;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.BaseTmxMapLoader;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TiledMapTileSets;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.audio.VJXSound;
import de.venjinx.ejx.controllers.AudioControl;
import de.venjinx.ejx.entity.EntityDef;
import de.venjinx.ejx.scenegraph.WorldLayer.LayerCell;
import de.venjinx.ejx.tiled.tile.EJXTile;
import de.venjinx.ejx.util.EJXTypes.AnimationType;
import de.venjinx.ejx.util.EJXTypes.Category;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;
import de.venjinx.ejx.util.EJXTypes.SpatialProperty;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

/** @brief synchronous loader for TMX maps created with the Tiled tool */
public class VJXMapLoader extends BaseTmxMapLoader<TmxMapLoader.Parameters> {

    private static final String PATH_ANIMATION = "data/definitions/animations.tsx";
    private static final String PATH_CHARACTER = "data/definitions/character.tsx";
    private static final String PATH_DECORATION = "data/definitions/decoration.tsx";
    private static final String PATH_ITEM = "data/definitions/item.tsx";
    private static final String PATH_LOGIC = "data/definitions/logics.tsx";
    private static final String PATH_MAP_DECO = "data/definitions/mapDeco.tsx";
    private static final String PATH_MASKS = "data/definitions/masks.tsx";
    private static final String PATH_UTILITY = "data/definitions/utility.tsx";
    private static final String PATH_SKIN = "data/definitions/skin.tsx";
    private static final String PATH_TILED = "data/definitions/tiled.tsx";
    private static final String PATH_UI_SOUNDS = "data/definitions/ui_sounds.tsx";
    private static final String PATH_VEHICLE = "data/definitions/vehicle.tsx";

    private int currentID = 0;
    private boolean external = false;

    private AbsoluteFileHandleResolver absResolver = new AbsoluteFileHandleResolver();
    private AudioControl audio;

    private Element defsElem;
    private HashMap<Integer, EntityDef> defs;
    private HashMap<String, VJXSound> sounds;

    public VJXMapLoader(AudioControl audioController) {
        super(new InternalFileHandleResolver());
        defs = new HashMap<>();
        sounds = new HashMap<>();
        audio = audioController;
    }

    public void clear() {
        currentID = 0;
        defs.clear();
        sounds.clear();
    }

    public void loadGlobalDefs() {
        try {
            VJXLogger.logIncr(LogCategory.LOAD,
                            "LevelLoader load definitions...");
            defsElem = xml.parse(resolve(PATH_MAP_DECO));
            loadObjectDefinitions(defsElem);

            defsElem = xml.parse(resolve(PATH_MASKS));
            loadObjectDefinitions(defsElem);

            defsElem = xml.parse(resolve(PATH_UTILITY));
            loadObjectDefinitions(defsElem);

            defsElem = xml.parse(resolve(PATH_TILED));
            loadObjectDefinitions(defsElem);

            defsElem = xml.parse(resolve(PATH_DECORATION));
            loadObjectDefinitions(defsElem);

            defsElem = xml.parse(resolve(PATH_CHARACTER));
            loadObjectDefinitions(defsElem);

            defsElem = xml.parse(resolve(PATH_ITEM));
            loadObjectDefinitions(defsElem);

            defsElem = xml.parse(resolve(PATH_VEHICLE));
            loadObjectDefinitions(defsElem);

            defsElem = xml.parse(resolve(PATH_SKIN));
            loadSkinDefinitions(defsElem);

            defsElem = xml.parse(resolve(PATH_UI_SOUNDS));
            loadUISounds(defsElem);

            defsElem = xml.parse(resolve(PATH_UI_SOUNDS));
            loadMusic(defsElem);

            loadProjectDefs();
            loadTerrainDef();

            VJXLogger.logDecr(LogCategory.LOAD,
                            "LevelLoader definitions loaded.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public TiledMap load(String fileName, boolean external) {
        this.external = external;
        TmxMapLoader.Parameters param = new TmxMapLoader.Parameters();
        param.generateMipMaps = true;
        param.textureMinFilter = TextureFilter.Nearest;
        param.textureMagFilter = TextureFilter.Nearest;
        return load(fileName, param);
    }

    /**
     * Loads the {@link TiledMap} from the given file. The file is resolved via
     * the {@link FileHandleResolver} set in the constructor of this class. By
     * default it will resolve to an internal file.
     *
     * @param fileName
     *            the filename
     * @param parameters
     *            specifies whether to use y-up, generate mip maps etc.
     * @return the TiledMap
     */
    public TiledMap load(String fileName, TmxMapLoader.Parameters parameters) {
        try {
            long time = System.currentTimeMillis();
            convertObjectToTileSpace = parameters.convertObjectToTileSpace;
            FileHandle tmxFile;

            /**
             * @author Torge Rothe (XRJ, X-Ray-Jin)
             * Added absolute file resolver to allow directly loading the
             * working copy of a map via Tiled to make life for level designers
             * more easy.
             */
            String path = fileName;
            if (external)
                tmxFile = absResolver.resolve(fileName);
            else tmxFile = resolve(fileName);
            VJXLogger.logIncr(LogCategory.LOAD, "LevelLoader load map: "
                            + tmxFile.nameWithoutExtension());

            if (!fileName.contains("ui")) {
                if (external) {
                    path = fileName.split("data/")[0];
                    defsElem = xml.parse(absResolver.resolve(path + PATH_ANIMATION));
                }
                else defsElem = xml.parse(resolve(PATH_ANIMATION));
                loadAnimDefinitions(defsElem);

                if (external) {
                    path = fileName.split("data/")[0];
                    defsElem = xml.parse(absResolver.resolve(path + PATH_LOGIC));
                }
                else defsElem = xml.parse(resolve(PATH_LOGIC));
                loadLogicDefinitions(defsElem);
            }
            // end custom

            root = xml.parse(tmxFile);
            ObjectMap<String, Texture> textures = new ObjectMap<>();
            Array<FileHandle> textureFiles = loadTilesets(root, tmxFile);
            textureFiles.addAll(loadImages(root, tmxFile));

            for (FileHandle textureFile : textureFiles) {

                Texture texture = new Texture(textureFile,
                                parameters.generateMipMaps);
                texture.setFilter(parameters.textureMinFilter,
                                parameters.textureMagFilter);
                textures.put(textureFile.path(), texture);
            }

            DirectImageResolver imageResolver = new DirectImageResolver(
                            textures);
            map = loadTilemap(root, tmxFile, imageResolver);
            map.setOwnedResources(textures.values().toArray());

            VJXLogger.logDecr(LogCategory.LOAD,
                            "LevelLoader finished loading map '"
                                            + tmxFile.nameWithoutExtension()
                                            + "'. Time "
                                            + (System.currentTimeMillis()
                                                            - time)
                                            + "ms.");
            return map;
        } catch (IOException e) {
            throw new GdxRuntimeException(
                            "Couldn't load tilemap '" + fileName + "'", e);
        }
    }

    public HashMap<Integer, EntityDef> getDefs() {
        return defs;
    }

    public HashMap<String, VJXSound> getSounds() {
        return sounds;
    }

    /**
     * Loads the map data, given the XML root element and an
     * {@link ImageResolver} used to return the tileset Textures
     *
     * @param root
     *            the XML root element
     * @param tmxFile
     *            the Filehandle of the tmx file
     * @param imageResolver
     *            the {@link ImageResolver}
     * @return the {@link TiledMap}
     */
    protected TiledMap loadTilemap(Element root, FileHandle tmxFile, ImageResolver imageResolver) {
        map = new TiledMap();

        String mapOrientation = root.getAttribute("orientation", null);
        int mapWidth = root.getIntAttribute("width", 0);
        int mapHeight = root.getIntAttribute("height", 0);
        int tileWidth = root.getIntAttribute("tilewidth", 0);
        int tileHeight = root.getIntAttribute("tileheight", 0);
        int hexSideLength = root.getIntAttribute("hexsidelength", 0);
        String staggerAxis = root.getAttribute("staggeraxis", null);
        String staggerIndex = root.getAttribute("staggerindex", null);
        String mapBackgroundColor = root.getAttribute("backgroundcolor", null);

        MapProperties mapProperties = map.getProperties();
        if (mapOrientation != null)
            mapProperties.put("orientation", mapOrientation);
        mapProperties.put("width", mapWidth);
        mapProperties.put("height", mapHeight);
        mapProperties.put("tileWidth", tileWidth);
        mapProperties.put("tileHeight", tileHeight);
        mapProperties.put("hexsidelength", hexSideLength);
        if (staggerAxis != null)
            mapProperties.put("staggeraxis", staggerAxis);
        if (staggerIndex != null)
            mapProperties.put("staggerindex", staggerIndex);
        if (mapBackgroundColor != null)
            mapProperties.put("backgroundcolor", mapBackgroundColor);
        mapTileWidth = tileWidth;
        mapTileHeight = tileHeight;
        mapWidthInPixels = mapWidth * tileWidth;
        mapHeightInPixels = mapHeight * tileHeight;

        if (mapOrientation != null) if ("staggered".equals(mapOrientation)) if (mapHeight > 1)
            if ("staggered".equals(mapOrientation))
                if (mapHeight > 1) {
                    mapWidthInPixels += tileWidth / 2;
                    mapHeightInPixels = mapHeightInPixels / 2 + tileHeight / 2;
                }

        Element properties = root.getChildByName("properties");
        if (properties != null) {
            loadDfltLevelProps();
            loadProperties(map.getProperties(), properties);
        }
        Array<Element> tilesets = root.getChildrenByName("tileset");
        for (Element element : tilesets) {
            loadTileSet(map, element, tmxFile, imageResolver);
            root.removeChild(element);
        }
        for (int i = 0, j = root.getChildCount(); i < j; i++) {
            Element element = root.getChild(i);
            loadLayer(map, map.getLayers(), element, tmxFile, imageResolver);
        }
        return map;
    }

    @Override
    protected void loadBasicLayerInfo(MapLayer layer, Element element) {
        String name = element.getAttribute("name", null);
        float opacity = Float
                        .parseFloat(element.getAttribute("opacity", "1.0"));
        boolean visible = element.getIntAttribute("visible", 1) == 1;
        boolean drawOrderY = element.getAttribute("draworder", VJXString.STR_EMPTY)
                        .isEmpty();

        layer.setName(name);
        layer.setOpacity(opacity);
        layer.setVisible(visible);
        layer.getProperties().put("drawOrderY", drawOrderY);
    }

    @Override
    protected void loadTileLayer(TiledMap map, MapLayers parentLayers,
                    Element element) {
        if (element.getName().equals("layer")) {
            int width = element.getIntAttribute("width", 0);
            int height = element.getIntAttribute("height", 0);
            int tileWidth = element.getParent().getIntAttribute("tilewidth", 0);
            int tileHeight = element.getParent().getIntAttribute("tileheight",
                            0);
            TiledMapTileLayer layer = new TiledMapTileLayer(width, height,
                            tileWidth, tileHeight);

            loadBasicLayerInfo(layer, element);

            int[] ids = getTileIds(element, width, height);
            TiledMapTileSets tilesets = map.getTileSets();
            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++) {
                    int id = ids[y * width + x];
                    boolean flipHorizontally = (id & FLAG_FLIP_HORIZONTALLY) != 0;
                    boolean flipVertically = (id & FLAG_FLIP_VERTICALLY) != 0;
                    boolean flipDiagonally = (id & FLAG_FLIP_DIAGONALLY) != 0;

                    TiledMapTile tile = tilesets.getTile(id & ~MASK_CLEAR);
                    if (tile != null) {
                        LayerCell cell = createTileLayerCell(x,
                                        height - 1 - y,
                                        flipHorizontally,
                                        flipVertically,
                                        flipDiagonally,
                                        tile.getProperties().get("tileCase", 0,
                                                        Integer.class));
                        cell.setTile(tile);
                        layer.setCell(x, height - 1 - y, cell);
                    }
                }

            Element properties = element.getChildByName("properties");
            if (properties != null) loadProperties(layer.getProperties(), properties);
            map.getLayers().add(layer);
        }
    }

    /**
     * Loads the tilesets
     *
     * @param root
     *            the root XML element
     * @return a list of filenames for images containing tiles
     * @throws IOException
     */
    protected Array<FileHandle> loadTilesets(Element root, FileHandle tmxFile)
                    throws IOException {
        Array<FileHandle> images = new Array<>();
        for (Element tileset : root.getChildrenByName("tileset")) {
            String source = tileset.getAttribute("source", null);
            if (source != null) {
                FileHandle tsxFile = getRelativeFileHandle(tmxFile, source);
                tileset = xml.parse(tsxFile);
                Element imageElement = tileset.getChildByName("image");
                if (imageElement != null) {
                    String imageSource = tileset.getChildByName("image")
                                    .getAttribute("source");
                    imageSource = imageSource.replace("../rawAssets/", "");
                    FileHandle image = getRelativeFileHandle(tsxFile,
                                    imageSource);
                    images.add(image);
                } else for (Element tile : tileset.getChildrenByName("tile")) {
                    String imageSource = tile.getChildByName("image")
                                    .getAttribute("source");
                    imageSource = imageSource.replaceAll("rawAssets/",
                                    external ? "assets/" : "");
                }
            } else {
                Element imageElement = tileset.getChildByName("image");
                if (imageElement != null) {
                    String imageSource = tileset.getChildByName("image")
                                    .getAttribute("source");
                    imageSource = imageSource.replace("../rawAssets/", "");
                    FileHandle image = getRelativeFileHandle(tmxFile,
                                    imageSource);
                    images.add(image);
                } else for (Element tile : tileset.getChildrenByName("tile")) {
                    String imageSource = tile.getChildByName("image")
                                    .getAttribute("source");
                    imageSource = imageSource.replaceAll("rawAssets/",
                                    external ? "assets/" : "");
                }
            }
        }
        return images;
    }

    /**
     * Loads the images in image layers
     *
     * @param root
     *            the root XML element
     * @return a list of filenames for images inside image layers
     * @throws IOException
     */
    protected Array<FileHandle> loadImages(Element root, FileHandle tmxFile)
                    throws IOException {
        Array<FileHandle> images = new Array<>();

        for (Element imageLayer : root.getChildrenByName("imagelayer")) {
            Element image = imageLayer.getChildByName("image");

            if (image == null)
                continue;
            String source = image.getAttribute("source", null);
            String layerName = imageLayer.getAttribute("name",
                            VJXString.STR_EMPTY);
            if (layerName.contains("ignore")) continue;

            if (source != null) {
                FileHandle handle = getRelativeFileHandle(tmxFile, source);

                if (!images.contains(handle, false)) images.add(handle);
            }
        }

        return images;
    }

    /**
     * Loads the specified tileset data, adding it to the collection of the
     * specified map, given the XML element, the tmxFile and an
     * {@link ImageResolver} used to retrieve the tileset Textures.
     * <p>
     * Default tileset's property keys that are loaded by default are:
     * </p>
     * <ul>
     * <li><em>firstgid</em>, (int, defaults to 1) the first valid global id
     * used for tile numbering</li>
     * <li><em>imagesource</em>, (String, defaults to empty string) the tileset
     * source image filename</li>
     * <li><em>imagewidth</em>, (int, defaults to 0) the tileset source image
     * width</li>
     * <li><em>imageheight</em>, (int, defaults to 0) the tileset source image
     * height</li>
     * <li><em>tilewidth</em>, (int, defaults to 0) the tile width</li>
     * <li><em>tileheight</em>, (int, defaults to 0) the tile height</li>
     * <li><em>margin</em>, (int, defaults to 0) the tileset margin</li>
     * <li><em>spacing</em>, (int, defaults to 0) the tileset spacing</li>
     * </ul>
     * <p>
     * The values are extracted from the specified Tmx file, if a value can't be
     * found then the default is used.
     * </p>
     *
     * @param map
     *            the Map whose tilesets collection will be populated
     * @param tilesetElement
     *            the XML element identifying the tileset to load
     * @param tmxFile
     *            the Filehandle of the tmx file
     * @param imageResolver
     *            the {@link ImageResolver}
     */
    protected void loadTileSet(TiledMap map, Element tilesetElement,
                    FileHandle tmxFile, ImageResolver imageResolver) {
        if (tilesetElement.getName().equals("tileset")) {
            String name = tilesetElement.get("name", null);
            int firstgid = tilesetElement.getIntAttribute("firstgid", 1);
            int tilewidth = tilesetElement.getIntAttribute("tilewidth", 0);
            int tileheight = tilesetElement.getIntAttribute("tileheight", 0);
            int spacing = tilesetElement.getIntAttribute("spacing", 0);
            int margin = tilesetElement.getIntAttribute("margin", 0);
            String source = tilesetElement.getAttribute("source", null);

            int offsetX = 0;
            int offsetY = 0;

            String imageSource = "";
            int imageWidth = 0, imageHeight = 0;

            FileHandle image = null;
            long time = System.nanoTime();
            if (source != null) {
                FileHandle tsx = getRelativeFileHandle(tmxFile, source);
                tilesetElement = xml.parse(tsx);
                name = tilesetElement.get("name", null);

                VJXLogger.logIncr(LogCategory.LOAD | LogCategory.DETAIL,
                                "LevelLoader loading external tileset '"
                                                + name
                                                + "' - firstgid: "
                                                + firstgid);
                tilewidth = tilesetElement.getIntAttribute("tilewidth", 0) / 2;
                tileheight = tilesetElement.getIntAttribute("tileheight", 0) / 2;
                spacing = tilesetElement.getIntAttribute("spacing", 0) / 2;
                margin = tilesetElement.getIntAttribute("margin", 0) / 2;
                Element offset = tilesetElement
                                .getChildByName("tileoffset");
                if (offset != null) {
                    offsetX = offset.getIntAttribute("x", 0);
                    offsetY = offset.getIntAttribute("y", 0);
                }
                Element imageElement = tilesetElement.getChildByName("image");
                if (imageElement != null) {
                    VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                                    "Loading base image for '" + name + "' "
                                                    + imageSource);
                    imageSource = imageElement.getAttribute("source");
                    imageSource = imageSource.replace("../rawAssets/", "");
                    imageWidth = imageElement.getIntAttribute("width", 0);
                    imageHeight = imageElement.getIntAttribute("height", 0);
                    image = getRelativeFileHandle(tsx, imageSource);
                }
            } else {
                name = tilesetElement.get("name", null);
                VJXLogger.logIncr(LogCategory.LOAD,
                                "LevelLoader loading internal tileset '" + name
                                + "' - firstgid: " + firstgid);
                Element offset = tilesetElement.getChildByName("tileoffset");
                if (offset != null) {
                    offsetX = offset.getIntAttribute("x", 0);
                    offsetY = offset.getIntAttribute("y", 0);
                }
                Element imageElement = tilesetElement.getChildByName("image");
                if (imageElement != null) {
                    VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                                    "Loading base image for '" + name + "' "
                                                    + imageSource);
                    imageSource = imageElement.getAttribute("source");
                    imageSource = imageSource.replace("../rawAssets/", "");
                    imageWidth = imageElement.getIntAttribute("width", 0);
                    imageHeight = imageElement.getIntAttribute("height", 0);
                    image = getRelativeFileHandle(tmxFile, imageSource);
                }
            }

            TiledMapTileSet tileset = new TiledMapTileSet();
            tileset.setName(name);

            tileset.getProperties().put("firstgid", firstgid);

            Element properties = tilesetElement.getChildByName("properties");
            if (properties != null)
                loadProperties(tileset.getProperties(), properties);

            if (image != null) {
                VJXLogger.logIncr(LogCategory.LOAD | LogCategory.DETAIL,
                                "Loading spritesheet based tiles for '" + name
                                + "'...");
                TextureRegion texture = imageResolver.getImage(image.path());

                MapProperties props = tileset.getProperties();
                props.put("imagesource", imageSource);
                props.put("imagewidth", imageWidth);
                props.put("imageheight", imageHeight);
                props.put("tilewidth", tilewidth);
                props.put("tileheight", tileheight);
                props.put("margin", margin);
                props.put("spacing", spacing);

                int stopWidth = texture.getRegionWidth() - tilewidth;
                int stopHeight = texture.getRegionHeight() - tileheight;

                int id = firstgid;

                for (int y = margin; y <= stopHeight; y += tileheight
                                + spacing)
                    for (int x = margin; x <= stopWidth; x += tilewidth
                    + spacing) {
                        TextureRegion tileRegion = new TextureRegion(texture, x,
                                        y, tilewidth,
                                        tileheight);
                        TiledMapTile tile = new StaticTiledMapTile(tileRegion);
                        tile.setId(id);
                        tile.setOffsetX(offsetX);
                        tile.setOffsetY(-offsetY);

                        /**
                         * Custom stuff added
                         *
                         * @author Torge Rothe (X-Ray-Jin) E-Mail:
                         *         xrayjin@gmx.de Fixes the path to custom
                         *         development environment. Sets the source path
                         *         of the tile. Adds the name of the tileset.
                         *         Sets the global id of the tile.
                         */
                        VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                                        "Loading tile#" + tile.getId()
                                        + " " + x + ", " + y);

                        // end custom stuff
                        tileset.putTile(id++, tile);
                    }

                /**
                 * Custom stuff added
                 *
                 * @author Torge Rothe (X-Ray-Jin)
                 *
                 * load tile properties
                 */
                Array<Element> tileElements = tilesetElement
                                .getChildrenByName("tile");
                for (Element tileElement : tileElements) {
                    int localtid = tileElement.getIntAttribute("id", 0);
                    TiledMapTile tile = tileset.getTile(firstgid + localtid);
                    if (tile != null) {
                        properties = tileElement
                                        .getChildByName("properties");
                        if (properties != null) {
                            tile.getProperties().putAll(tileset.getProperties());
                            loadProperties(tile.getProperties(), properties);
                            tile.getProperties().put("tileset",
                                            tileset.getName());
                        }
                    }
                }

                VJXLogger.logDecr(LogCategory.LOAD | LogCategory.DETAIL,
                                "Loading spritesheet based tiles for '" + name
                                + "' finished.");

                // end custom stuff
            } else {
                VJXLogger.logIncr(LogCategory.LOAD | LogCategory.DETAIL,
                                "Loading image based tiles for '" + name + "'...");
                // load image based tiles or objects
                Array<Element> tileElements = tilesetElement
                                .getChildrenByName("tile");
                FileHandle fh;
                for (Element tileElement : tileElements) {
                    if (source != null)
                        fh = getRelativeFileHandle(tmxFile, source);
                    else fh = tmxFile;

                    Element imageElement = tileElement.getChildByName("image");
                    if (imageElement != null) {
                        imageSource = imageElement.getAttribute("source");
                        imageWidth = imageElement.getIntAttribute("width", 0);
                        imageHeight = imageElement.getIntAttribute("height", 0);
                        image = getRelativeFileHandle(fh, imageSource);
                    }

                    TextureRegion texture = null;
                    TiledMapTile tile = new StaticTiledMapTile(texture);
                    tile.setId(firstgid + tileElement.getIntAttribute("id"));
                    tile.setOffsetX(offsetX);
                    tile.setOffsetY(-offsetY);

                    /**
                     * Custom stuff added
                     *
                     * @author Torge Rothe (X-Ray-Jin) E-Mail: xrayjin@gmx.de
                     *         Fixes the path to custom development environment.
                     *         Sets the source path of the tile. Adds the name
                     *         of the tileset. Loads object definitions
                     */

                    properties = tileElement.getChildByName("properties");
                    EntityDef definition;

                    MapProperties tilesetProperties = new MapProperties();
                    // load base properties from tile set
                    Element tilesetPropertiesElem = tilesetElement
                                    .getChildByName("properties");
                    loadProperties(tilesetProperties, tilesetPropertiesElem);
                    definition = loadDef(tileElement, tilesetProperties);
                    defs.put(definition.getDefID(), definition);

                    /*
                     * overwrite definition properties with custom tile
                     * properties and set a link to the tile set.
                     */
                    if (properties != null)
                        if (!tileset.getName().contains("defs")) {

                            loadProperties(definition.getCustomProperties(), properties);
                            definition.setValue(SourceProperty.tileset.name,
                                            tileset.getName());

                        }

                    // load definition properties into tile properties
                    tile.getProperties().putAll(definition.getCustomProperties());

                    // end custom stuff
                    tileset.putTile(tile.getId(), tile);
                }
                VJXLogger.logDecr(LogCategory.LOAD | LogCategory.DETAIL,
                                "Loading imaged based tiles for '" + name
                                + "' finished.");
            }

            // load animated tiles, terrain and probability
            Array<Element> tileElements = tilesetElement
                            .getChildrenByName("tile");
            Array<AnimatedTiledMapTile> animatedTiles = new Array<>();

            for (Element tileElement : tileElements) {
                int localtid = tileElement.getIntAttribute("id", 0);
                TiledMapTile tile = tileset.getTile(firstgid + localtid);
                if (tile != null) {
                    Element animationElement = tileElement
                                    .getChildByName("animation");
                    if (animationElement != null) {

                        Array<StaticTiledMapTile> staticTiles = new Array<>();
                        IntArray intervals = new IntArray();
                        for (Element frameElement : animationElement
                                        .getChildrenByName("frame")) {
                            staticTiles.add((StaticTiledMapTile) tileset
                                            .getTile(firstgid
                                                            + frameElement.getIntAttribute(
                                                                            "tileid")));
                            intervals.add(frameElement
                                            .getIntAttribute("duration"));
                        }

                        AnimatedTiledMapTile animatedTile = new AnimatedTiledMapTile(
                                        intervals,
                                        staticTiles);
                        animatedTile.setId(tile.getId());
                        animatedTiles.add(animatedTile);
                        tile = animatedTile;
                    }

                    String terrain = tileElement.getAttribute("terrain", null);
                    if (terrain != null) tile.getProperties().put("terrain", terrain);
                    String probability = tileElement.getAttribute("probability",
                                    null);
                    if (probability != null) tile.getProperties().put("probability", probability);

                    /**
                     *  original moved up for some custom stuff - XRJ
                     */
                    //                    Element properties = tileElement.getChildByName("properties");
                    //                    if (properties != null) {
                    //                        loadProperties(tile.getProperties(), properties);
                    //                        tile.getProperties().put("tileset", tileset.getName());
                    //                    }
                }
            }

            for (AnimatedTiledMapTile tile : animatedTiles)
                tileset.putTile(tile.getId(), tile);

            map.getTileSets().addTileSet(tileset);

            time = System.nanoTime() - time;
            VJXLogger.logDecr(LogCategory.LOAD | LogCategory.DETAIL,
                            "LevelLoader tileset '" + name + "' finished. Time: "
                                            + time / 1000000f + "ms");
        }
    }

    /**
     * Custom stuff added
     *
     * @author Torge Rothe (X-Ray-Jin, XRJ)
     *
     */

    @Override
    protected void loadProperties(MapProperties properties, Element propElem) {
        EJXUtil.loadProperties(properties, propElem);
    }

    protected LayerCell createTileLayerCell(int x, int y,
                    boolean flipHorizontally, boolean flipVertically,
                    boolean flipDiagonally, int tileCase) {
        LayerCell cell = new LayerCell(x, y, tileCase);
        if (flipDiagonally) {
            if (flipHorizontally && flipVertically) {
                cell.setFlipHorizontally(true);
                cell.setRotation(Cell.ROTATE_270);
            } else if (flipHorizontally)
                cell.setRotation(Cell.ROTATE_270);
            else if (flipVertically)
                cell.setRotation(Cell.ROTATE_90);
            else {
                cell.setFlipVertically(true);
                cell.setRotation(Cell.ROTATE_270);
            }
        } else {
            cell.setFlipHorizontally(flipHorizontally);
            cell.setFlipVertically(flipVertically);
        }
        return cell;
    }

    private MapProperties loadDfltLevelProps() {
        MapProperties properties = map.getProperties();

        // identifier and name
        properties.put("name", "Level 0 - <noName>");
        properties.put("levelNr", 0);

        // camera mode default = focus player
        properties.put("type", "default");

        return properties;
    }

    private void loadUISounds(Element tsxFile) {
        Array<Element> tiles = tsxFile.getChildrenByName("tile");
        MapProperties props = new MapProperties();
        for (Element tile : tiles) {
            Element propElem = tile.getChildByName("properties");
            props.clear();
            loadProperties(props, propElem);
            audio.addUISounds(props);
        }
    }

    private void loadMusic(Element tsxFile) {
        Array<Element> tiles = tsxFile.getChildrenByName("tile");
        MapProperties properties = new MapProperties();
        for (Element tile : tiles) {
            properties.clear();
            Element propElem = tile.getChildByName("properties");
            loadProperties(properties, propElem);
            Iterator<String> keys = properties.getKeys();
            String key;
            while (keys.hasNext()) {
                key = keys.next();
                if (key.contains("music"))
                    audio.addMusic(key, properties.get(key, null, FileHandle.class));
            }
        }
    }

    private void loadObjectDefinitions(Element tsxFile) {
        VJXLogger.logIncr(LogCategory.LOAD,
                        "LevelLoader load definitions for tile set '"
                                        + tsxFile.getAttribute("name") + "'...");

        MapProperties tilesetProperties = new MapProperties();
        Element tilesetPropertiesElem = tsxFile.getChildByName("properties");
        loadProperties(tilesetProperties, tilesetPropertiesElem);

        Array<Element> tiles = tsxFile.getChildrenByName("tile");

        EntityDef def;
        for (Element tile : tiles) {
            def = loadDef(tile, tilesetProperties);
            defs.put(def.getDefID(), def);
        }
        VJXLogger.logDecr(LogCategory.LOAD,
                        "LevelLoader loaded definitions for tile set '"
                                        + tsxFile.getAttribute("name") + "'.");
    }

    private void loadSkinDefinitions(Element tsxFile) {
        VJXLogger.logIncr(LogCategory.LOAD,
                        "LevelLoader load definitions for tile set '"
                                        + tsxFile.getAttribute("name") + "'...");

        MapProperties tilesetProperties = new MapProperties();
        Element tilesetPropertiesElem = tsxFile.getChildByName("properties");
        loadProperties(tilesetProperties, tilesetPropertiesElem);

        Array<Element> tiles = tsxFile.getChildrenByName("tile");

        EntityDef def;
        for (Element tile : tiles) {
            def = loadSkinDef(tile, tilesetProperties);
            if (def != null)
                defs.put(def.getDefID(), def);
        }
        VJXLogger.logDecr(LogCategory.LOAD,
                        "LevelLoader loaded definitions for tile set '"
                                        + tsxFile.getAttribute("name") + "'.");
    }

    private EntityDef loadSkinDef(Element tile, MapProperties tilesetProps) {
        EntityDef def = null;
        Element propertiesElem;
        String src, defName;
        MapProperties tileProperties;

        propertiesElem = tile.getChildByName("properties");
        src = tile.getChildByName("image").getAttribute("source");

        String[] sourceSplit = getSourceSplit(src, VJXString.FILE_PNG);

        src = sourceSplit[0];
        defName = sourceSplit[2];

        VJXLogger.logIncr(LogCategory.LOAD | LogCategory.DETAIL,
                        "LevelLoader load skin definition for tile '" + defName
                        + "_" + sourceSplit[3] + "'...");

        tileProperties = new MapProperties();
        loadProperties(tileProperties, propertiesElem);

        String skeleton = tileProperties.get(DefinitionProperty.skeleton.name,
                        VJXString.STR_EMPTY, String.class);

        if (skeleton.isEmpty()) {

            VJXLogger.logDecr(LogCategory.LOAD | LogCategory.DETAIL,
                            "LevelLoader unknown skeleton for skin '"
                                            + defName + "'.");
            return null;
        }

        for (EntityDef tmpDef : defs.values())
            if (tmpDef.getDefName().equals(skeleton)) {
                def = new EntityDef(tmpDef);

                VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                                "LevelLoader creating skin '" + defName + "_"
                                                + sourceSplit[3]
                                                                + "' for skeleton definition '"
                                                                + tmpDef.getDefName()
                                                                + "'. ID = " + def.getDefID());
                tileProperties = def.getCustomProperties();
                tileProperties.put(DefinitionProperty.skin.name, true);

                // overwrite with tile properties
                propertiesElem = tile.getChildByName("properties");
                loadProperties(tileProperties, propertiesElem);

                // overwrite definition properties
                tileProperties.put(DefinitionProperty.defName.name, sourceSplit[1]);
                tileProperties.put(DefinitionProperty.defID.name, currentID++);
                tileProperties.put(SourceProperty.tileName.name, sourceSplit[1]);
                tileProperties.put(DefinitionProperty.attributes.name, sourceSplit[3]);
                tileProperties.put(SourceProperty.source.name, tmpDef.getSource());

                Element collGroup = tile.getChildByName("objectgroup");
                if (collGroup != null)
                    LevelUtil.loadCollision(def.getCustomProperties(), collGroup);

                def.setFromProps(tileProperties, false);
                break;
            }

        VJXLogger.logDecr(LogCategory.LOAD | LogCategory.DETAIL,
                        "LevelLoader loaded skin definition for tile '"
                                        + defName + "'.");
        return def;
    }

    private void loadLogicDefinitions(Element tsxFile) {
        Array<Element> tiles = tsxFile.getChildrenByName("tile");

        Element propertiesElem;
        for (Element tile : tiles) {
            propertiesElem = tile.getChildByName("properties");
            if (propertiesElem == null) continue;

            LevelUtil.loadLogic(tile);
        }
    }

    private void loadAnimDefinitions(Element tsxFile) {
        Array<Element> tiles = tsxFile.getChildrenByName("tile");

        Element propertiesElem;
        for (Element tile : tiles) {
            propertiesElem = tile.getChildByName("properties");
            if (propertiesElem == null) continue;

            LevelUtil.loadAnimation(tile, audio);
        }
    }

    private EntityDef findDef(String defName, String attributes) {

        // search for existing def
        for (EntityDef tmpDef : defs.values())
            // similar def exists
            if (tmpDef.getDefName().equals(defName) || tmpDef.getDefName()
                            .equals(defName.replace("_" + attributes, ""))) {

                // check attributes to verify if it is a new object
                if (!tmpDef.getAttributes().equals(attributes)) {
                    if (tmpDef.getCategory() != Category.TILED)
                        // still might be a new object
                        continue;

                    // a similar tiled def exists
                    VJXLogger.log(defName + " similar to "
                                    + tmpDef.getDefName());
                }

                VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                                "LevelLoader definition for tile '" + defName
                                + "_" + attributes
                                + "' found. ");
                return tmpDef;
            }
        return null;
    }

    private EntityDef extendBaseDef(String baseDef, String[] sourceSplit,
                    Element propertiesElem, MapProperties tilesetProps) {
        // if a defName is given to the object, find the definition and use it as base.
        if (baseDef.isEmpty()) return null;

        EntityDef def;
        for (EntityDef d : defs.values())
            if (d.getDefName().equals(baseDef)) {
                def = new EntityDef(d);

                MapProperties tileProperties = def.getCustomProperties();
                tileProperties.putAll(tilesetProps);

                // overwrite with tile properties
                loadProperties(tileProperties, propertiesElem);

                // overwrite definition properties
                tileProperties.put(DefinitionProperty.defName.name, sourceSplit[1]);
                tileProperties.put(VJXString.name, sourceSplit[1]);
                tileProperties.put(DefinitionProperty.defID.name, currentID++);
                tileProperties.put(DefinitionProperty.attributes.name, sourceSplit[3]);

                tileProperties.put(SourceProperty.tileName.name, sourceSplit[1]);
                if (!tileProperties.get(DefinitionProperty.skin.name, false, Boolean.class))
                    tileProperties.put(SourceProperty.source.name, sourceSplit[0]);
                else {
                    // TODO remove this
                    VJXLogger.log(LogCategory.ERROR, " illegal call ");
                    tileProperties.put(SourceProperty.source.name,
                                    d.getSource());
                }

                def.setFromProps(tileProperties, false);
                VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                                "LevelLoader creating '" + sourceSplit[1]
                                                + "' from referenced definition '"
                                                + baseDef + "'. ID = "
                                                + def.getDefID());
                return def;
            }
        return null;
    }

    private void updateSize(EntityDef def, float w, float h) {
        def.getCustomProperties().put(SpatialProperty.width.name, w);
        def.getCustomProperties().put(SpatialProperty.height.name, h);

        float origW = def.getValue(SourceProperty.srcWidth, 0, Integer.class);
        float origH = def.getValue(SourceProperty.srcHeight, 0, Integer.class);
        def.getCustomProperties().put(SpatialProperty.scaleX.name, w / origW);
        def.getCustomProperties().put(SpatialProperty.scaleY.name, h / origH);
    }

    private EntityDef loadDef(Element tile, MapProperties addProps) {
        EntityDef def = null;
        Element propertiesElem, imageElement, collObjectGroup;
        String src, defName, baseDef = null;
        MapProperties tileProperties;

        propertiesElem = tile.getChildByName("properties");
        imageElement = tile.getChildByName("image");
        collObjectGroup = tile.getChildByName("objectgroup");

        if (imageElement != null) src = imageElement.getAttribute("source");
        else src = addProps.get("imagesource", null, String.class);

        String[] sourceSplit = getSourceSplit(src, VJXString.FILE_PNG);

        src = sourceSplit[0];
        defName = sourceSplit[2];

        VJXLogger.logIncr(LogCategory.LOAD | LogCategory.DETAIL,
                        "LevelLoader load definition for tile '"
                                        + sourceSplit[1] + "'...");

        // search for existing def
        def = findDef(sourceSplit[1], sourceSplit[3]);

        // if no def was found check if it is a tiled object, skin or has referenced defName
        if (def == null) {
            propertiesElem = tile.getChildByName("properties");

            if (propertiesElem != null)
                for (Element property : propertiesElem
                                .getChildrenByName("property")) {
                    String name = property.getAttribute("name",
                                    VJXString.STR_EMPTY);
                    // check if object is a skin for definition
                    if (name.equals(DefinitionProperty.skeleton.name)) {

                        baseDef = property.getAttribute("value",
                                        VJXString.STR_EMPTY);
                        def = extendBaseDef(baseDef, sourceSplit,
                                        propertiesElem, addProps);

                        if (collObjectGroup != null)
                            LevelUtil.loadCollision(def.getCustomProperties(),
                                                            collObjectGroup);
                        int w, h;
                        if (imageElement != null) {
                            w = imageElement.getIntAttribute("width", 0);
                            h = imageElement.getIntAttribute("height", 0);
                        } else {
                            VJXLogger.log(defName + " strange width ");
                            float fw = addProps.get(SpatialProperty.width.name,
                                            0f, Float.class);
                            float fh = addProps.get(SpatialProperty.height.name,
                                            0f, Float.class);
                            w = (int) fw;
                            h = (int) fh;
                        }
                        updateSize(def, w, h);
                    }
                }
        } else if (def.getCategory() != Category.TILED) {
            // overwrite with tile properties
            propertiesElem = tile.getChildByName("properties");
            loadProperties(def.getCustomProperties(), propertiesElem);

            int w, h;
            if (imageElement != null) {
                w = imageElement.getIntAttribute("width", 0);
                h = imageElement.getIntAttribute("height", 0);
            } else {
                VJXLogger.log(defName + " strange width ");
                float fw = addProps.get(SpatialProperty.width.name, 0f,
                                Float.class);
                float fh = addProps.get(SpatialProperty.height.name, 0f,
                                Float.class);
                w = (int) fw;
                h = (int) fh;
            }

            float origW = def.getValue(SourceProperty.srcWidth, 0, Integer.class);
            float origH = def.getValue(SourceProperty.srcHeight, 0, Integer.class);
            def.getCustomProperties().put(SpatialProperty.scaleX.name, w / origW);
            def.getCustomProperties().put(SpatialProperty.scaleY.name, h / origH);
        }

        // create completely new definition from tile set and tile properties
        if (def == null) {
            tileProperties = new MapProperties();

            // set definition properties
            tileProperties.put(DefinitionProperty.defName.name, defName);
            tileProperties.put(DefinitionProperty.defID.name, currentID++);
            tileProperties.put(SourceProperty.source.name, src);
            tileProperties.put(SourceProperty.tileName.name, sourceSplit[1]);

            int w, h;
            if (imageElement != null) { // single image object
                w = imageElement.getIntAttribute("width", 0);
                h = imageElement.getIntAttribute("height", 0);
            } else { // tile sheet object
                w = addProps.get("tilewidth", 0, Integer.class) * 2;
                h = addProps.get("tileheight", 0, Integer.class) * 2;
                defName = defName + VJXString.SEP_NR + addProps.get("gid");
                tileProperties.put(DefinitionProperty.defName.name, defName);
                tileProperties.put(SourceProperty.tileName.name, defName);
            }

            tileProperties.put(SourceProperty.srcWidth.name, w);
            tileProperties.put(SourceProperty.srcHeight.name, h);

            // Default minimum entity properties
            tileProperties.put(DefinitionProperty.category.name, "object");
            tileProperties.put(DefinitionProperty.body.name, "static");
            tileProperties.put(DefinitionProperty.animation.name, AnimationType.FRAME.name);

            // overwrite with base tile set properties
            tileProperties.putAll(addProps);

            // overwrite with tile properties
            propertiesElem = tile.getChildByName("properties");
            loadProperties(tileProperties, propertiesElem);

            tileProperties.put(DefinitionProperty.attributes.name,
                            sourceSplit[3]);

            if (collObjectGroup != null)
                LevelUtil.loadCollision(tileProperties, collObjectGroup);

            def = new EntityDef(tileProperties);
            VJXLogger.log(LogCategory.LOAD | LogCategory.DETAIL,
                            "LevelLoader created new definition for tile '"
                                            + defName + "'. ");
        }

        VJXLogger.logDecr(LogCategory.LOAD | LogCategory.DETAIL,
                        "LevelLoader loaded definition for tile '"
                                        + def.getDefName() + "'. ID = "
                                        + def.getDefID());
        return def;
    }

    private void loadTerrainDef() {
        EntityDef def;
        MapProperties properties = new MapProperties();

        // identifier and source properties
        properties.put(SourceProperty.source.name, "tiles/");
        properties.put(DefinitionProperty.defID.name, currentID++);
        properties.put(SourceProperty.mapTileId.name, -1);
        properties.put(SourceProperty.id.name, -1);
        properties.put(DefinitionProperty.defName.name, "collision");
        properties.put(DefinitionProperty.category.name, "collision");
        properties.put(DefinitionProperty.usePhysics.name, true);

        // entity type definition
        properties.put(DefinitionProperty.body.name, "static");
        properties.put(DefinitionProperty.animation.name, "frame");

        def = new EntityDef(properties);
        defs.put(def.getDefID(), def);
    }

    private void loadProjectDefs() throws IOException {
        defsElem = xml.parse(resolve("data/definitions/project.tsx"));
        Array<Element> tiles = defsElem.getChildrenByName(EJXTile.tile);
        for (Element tile : tiles) {
            MapProperties tilesetProperties = new MapProperties();
            Element tilesetPropertiesElem = tile.getChildByName(VJXString.TILED_properties);
            loadProperties(tilesetProperties, tilesetPropertiesElem);
            tilesetProperties.put(VJXString.source,
                            tile.getChildByName(VJXString.TILED_image)
                            .getAttribute(VJXString.source,
                                            VJXString.STR_EMPTY));
            LevelUtil.addProjectDef(tilesetProperties);
        }
    }

    @Override
    protected void loadObject(TiledMap map, MapLayer layer, Element element) {
        if (element.getName().equals("object")) {
            MapObject object = null;

            float scaleX = convertObjectToTileSpace ? 1.0f / mapTileWidth : 1.0f;
            float scaleY = convertObjectToTileSpace ? 1.0f / mapTileHeight : 1.0f;

            float x = element.getFloatAttribute("x", 0) * scaleX;
            float y = (mapHeightInPixels - element.getFloatAttribute("y", 0))
                            * scaleY;
            float width = element.getFloatAttribute("width", 0) * scaleX;
            float height = element.getFloatAttribute("height", 0) * scaleY;

            if (element.getChildCount() > 0) {
                Element child = null;
                if ((child = element.getChildByName("polygon")) != null) {
                    String[] points = child.getAttribute("points").split(" ");
                    float[] vertices = new float[points.length * 2];
                    for (int i = 0; i < points.length; i++) {
                        String[] point = points[i].split(",");
                        vertices[i * 2] = Float.parseFloat(point[0]) * scaleX;
                        vertices[i * 2 + 1] = -Float.parseFloat(point[1])
                                        * scaleY;
                    }
                    Polygon polygon = new Polygon(vertices);
                    polygon.setPosition(x, y);
                    object = new PolygonMapObject(polygon);
                } else if ((child = element
                                .getChildByName("polyline")) != null) {
                    String[] points = child.getAttribute("points").split(" ");
                    float[] vertices = new float[points.length * 2];
                    for (int i = 0; i < points.length; i++) {
                        String[] point = points[i].split(",");
                        vertices[i * 2] = Float.parseFloat(point[0]) * scaleX;
                        vertices[i * 2 + 1] = -Float.parseFloat(point[1])
                                        * scaleY;
                    }
                    Polyline polyline = new Polyline(vertices);
                    polyline.setPosition(x, y);
                    object = new PolylineMapObject(polyline);
                } else if ((child = element
                                .getChildByName("ellipse")) != null)
                    object = new EllipseMapObject(x, y - height, width, height);
            }
            if (object == null) {
                String gid = null;
                if ((gid = element.getAttribute("gid", null)) != null) {
                    int id = (int) Long.parseLong(gid);
                    TextureMapObject textureMapObject = new TextureMapObject(
                                    null);
                    textureMapObject.getProperties().put("gid", id);
                    textureMapObject.setX(x);
                    textureMapObject.setY(y - height);
                    textureMapObject.setScaleX(scaleX);
                    textureMapObject.setScaleY(scaleY);
                    textureMapObject.setRotation(
                                    element.getFloatAttribute("rotation", 0));
                    object = textureMapObject;
                } else object = new RectangleMapObject(x, y - height, width,
                                height);
            }
            object.setName(element.getAttribute("name", null));

            object.getProperties().put(VJXString.name, object.getName());
            float rotation = element.getFloatAttribute("rotation", 0f);

            String type = element.getAttribute("type", null);
            if (type != null)
                object.getProperties().put("type", type);
            int id = element.getIntAttribute("id", 0);

            if (id != 0)
                object.getProperties().put("id", id);

            rotation = rotation % 360;

            if (object.getProperties().containsKey("gid")) {
                int tileID = object.getProperties().get("gid", Integer.class);
                boolean flipHorizontally = (tileID & FLAG_FLIP_HORIZONTALLY) != 0;
                boolean flipVertically = (tileID & FLAG_FLIP_VERTICALLY) != 0;

                tileID = tileID & ~0xE0000000;

                boolean flipY = flipVertically;
                //            float sign = Math.signum(rotation);
                boolean flipX = flipVertically && rotation == 180;

                if (flipVertically && rotation == 180) {
                    flipX = true;
                    flipY = false;
                    rotation = 0;
                    x -= width;
                    y -= height;
                }

                if (!flipHorizontally && !flipVertically
                                && Math.abs(rotation) == 180) {
                    flipX = true;
                    flipY = true;
                    rotation = 0;
                    x -= width;
                    y -= height;
                }

                flipX |= flipHorizontally;

                object.getProperties().put(SpatialProperty.x.name, x);
                object.getProperties().put(SpatialProperty.y.name, y);
                object.getProperties().put(SpatialProperty.width.name, width);
                object.getProperties().put(SpatialProperty.height.name, height);
                object.getProperties().put(SpatialProperty.flipX.name, flipX);
                object.getProperties().put(SpatialProperty.flipY.name, flipY);
                object.getProperties().put(SpatialProperty.rotation.name, -rotation);
                object.setVisible(element.getIntAttribute("visible", 1) == 1);
                Element properties = element.getChildByName("properties");
                if (properties != null)
                    loadProperties(object.getProperties(), properties);

                /**
                 * Custom stuff added
                 *
                 * @author Torge Rothe (X-Ray-Jin) E-Mail: xrayjin@gmx.de
                 *         Fixes the path to custom development environment.
                 *         Sets the source path of the tile. Adds the name of
                 *         the tileset. Sets the global id of the tile.
                 */

                int defID;
                TiledMapTile t = map.getTileSets().getTile(tileID);
                defID = t.getProperties().get(DefinitionProperty.defID.name, -1,
                                Integer.class);


                EntityDef def = defs.get(defID);

                if (def == null) {
                    object.getProperties().putAll(t.getProperties());
                    def = loadDef(element, object.getProperties());
                    defs.put(def.getDefID(), def);
                }

                MapProperties props = new MapProperties();

                props.putAll(def.getCustomProperties());

                props.putAll(t.getProperties());

                props.putAll(object.getProperties());

                float origW = def.getValue(SourceProperty.srcWidth, 0, Integer.class);
                float origH = def.getValue(SourceProperty.srcHeight, 0, Integer.class);
                props.put(SpatialProperty.scaleX.name, width / origW);
                props.put(SpatialProperty.scaleY.name, height / origH);

                if (object.getName() == null)
                    object.setName(def.getDefName() + "#mObj"
                                    + props.get("id"));

                props.put(VJXString.name, object.getName());

                object.getProperties().clear();
                object.getProperties().putAll(props);

                //                VJXLogger.printMapProperties(object.getProperties());
                layer.getObjects().add(object);
            } else {
                // load tiled map polygon objects
                object.getProperties().put(SpatialProperty.x.name, x);
                object.getProperties().put(SpatialProperty.y.name, y);
                object.getProperties().put(SpatialProperty.width.name, (int) width);
                object.getProperties().put(SpatialProperty.height.name, (int) height);
                object.getProperties().put(SpatialProperty.rotation.name, -rotation);
                object.setVisible(element.getIntAttribute("visible", 1) == 1);
                Element properties = element.getChildByName("properties");
                if (properties != null)
                    loadProperties(object.getProperties(), properties);

                layer.getObjects().add(object);
            }
        }
    }

    /**
     * Extracts various information from a source string and splits them into
     * an array.
     *
     * @param source - Input source string to be converted.
     * @param fileExtension - The file extension of the target file.
     * @return
     * Returns an array containing the internal absolute path of
     * the source folder, the file name without extension and the definition name.
     * That is the file names first part. The other parts are attributes.
     *
     * Return array:
     * sourceSplit[0] = path to source folder
     * sourceSplit[1] = full tile name with attributes without extension
     * sourceSplit[2] = definition name
     * sourceSplit[3] = attributes
     */
    public static String[] getSourceSplit(String source, String fileExtension) {
        String[] split;
        String[] sourceSplit = new String[4];
        if (source.isEmpty()) return null;

        // remove file extension and relativity from tileset file
        source = source.replace(fileExtension, VJXString.STR_EMPTY);
        source = source.replace(VJXString.PATH_DIR_UP, VJXString.STR_EMPTY);
        //        VJXLogger.log(source);

        // split source by "/" to get the path parts
        split = source.split(VJXString.SEP_SLASH);

        // get file name without extension
        sourceSplit[1] = split[split.length - 1];
        //        VJXLogger.log(sourceSplit[1]);

        // remove file name from source
        sourceSplit[0] = source.replace(sourceSplit[1], VJXString.STR_EMPTY);
        sourceSplit[0] = sourceSplit[0].replace("rawAssets/", VJXString.STR_EMPTY);

        // remove file attributes to get clean tile name
        sourceSplit[1] = sourceSplit[1].replace("ico_", VJXString.STR_EMPTY);
        sourceSplit[1] = sourceSplit[1].replace("_icon", VJXString.STR_EMPTY);

        sourceSplit[2] = sourceSplit[1];

        split = sourceSplit[1].split(VJXString.SEP_USCORE);
        if (split.length > 1) sourceSplit[3] = split[1];
        else sourceSplit[3] = VJXString.STR_EMPTY;

        if (!sourceSplit[3].isEmpty()) {
            // split attributes from tile number
            String[] attSplit = sourceSplit[3].split(VJXString.SEP_NR);
            if (attSplit.length > 1)
                sourceSplit[3] = attSplit[0];
        }

        return sourceSplit;
    }

    public static String srcToFileName(String source, String fileExtension) {
        String[] pathArray;
        if (source.isEmpty()) return VJXString.STR_EMPTY;

        // remove file extension and relativity from tileset file
        source = source.replace(fileExtension, VJXString.STR_EMPTY);
        source = source.replace(VJXString.PATH_DIR_UP, VJXString.STR_EMPTY);

        // split source by "/" to get the path parts
        pathArray = source.split("/");

        // get file name from path parts
        return pathArray[pathArray.length - 1];
    }

    public static String getDefName(Element props, String dfltValue) {
        if (props == null) return dfltValue;
        Array<Element> propElems = props.getChildrenByName("property");
        for (Element property : propElems) {
            String name = property.getAttribute("name", VJXString.STR_EMPTY);
            if (name.equals(DefinitionProperty.defName.name)) {
                String strValue = property.getAttribute("value", VJXString.STR_EMPTY);
                if (strValue.isEmpty()) break;
                dfltValue = strValue;
                break;
            }
        }
        return dfltValue;
    }

    @Override
    public void loadAsync(AssetManager manager, String fileName,
                    FileHandle tmxFile, TmxMapLoader.Parameters parameter) {
        map = null;

        if (parameter != null)
            convertObjectToTileSpace = parameter.convertObjectToTileSpace;
        else convertObjectToTileSpace = false;
        try {
            map = loadTilemap(root, tmxFile,
                            new AssetManagerImageResolver(manager));
        } catch (Exception e) {
            throw new GdxRuntimeException(
                            "Couldn't load tilemap '" + fileName + "'", e);
        }
    }

    @Override
    public TiledMap loadSync(AssetManager manager, String fileName,
                    FileHandle file, TmxMapLoader.Parameters parameter) {
        return map;
    }

    /**
     * Retrieves TiledMap resource dependencies
     *
     * @param fileName
     * @param parameter
     *            not used for now
     * @return dependencies for the given .tmx file
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Array<AssetDescriptor> getDependencies(String fileName,
                    FileHandle tmxFile, TmxMapLoader.Parameters parameter) {
        Array<AssetDescriptor> dependencies = new Array<>();
        try {
            root = xml.parse(tmxFile);
            boolean generateMipMaps = parameter != null
                            ? parameter.generateMipMaps : false;
            TextureLoader.TextureParameter texParams = new TextureParameter();
            texParams.genMipMaps = generateMipMaps;
            if (parameter != null) {
                texParams.minFilter = parameter.textureMinFilter;
                texParams.magFilter = parameter.textureMagFilter;
            }
            for (FileHandle image : loadTilesets(root, tmxFile))
                dependencies.add(new AssetDescriptor<>(image, Texture.class,
                                texParams));
            for (FileHandle image : loadImages(root, tmxFile))
                dependencies.add(new AssetDescriptor<>(image, Texture.class,
                                texParams));
            return dependencies;
        } catch (IOException e) {
            throw new GdxRuntimeException(
                            "Couldn't load tilemap '" + fileName + "'", e);
        }
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Array<AssetDescriptor>
    getDependencyAssetDescriptors(FileHandle tmxFile,
                    TextureParameter textureParameter) {
        return null;
    }

    @Override
    protected void addStaticTiles(FileHandle tmxFile, ImageResolver imageResolver,
                    TiledMapTileSet tileset, Element element,
                    Array<Element> tileElements, String name, int firstgid,
                    int tilewidth, int tileheight, int spacing, int margin,
                    String source, int offsetX, int offsetY,
                    String imageSource, int imageWidth, int imageHeight,
                    FileHandle image) {
    }
}