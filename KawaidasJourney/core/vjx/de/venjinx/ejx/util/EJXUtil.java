package de.venjinx.ejx.util;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.resolvers.AbsoluteFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class EJXUtil {

    public static final Color tiledDflt = new Color(137 / 255f, 137 / 255f, 137 / 255f, 1f);
    private static XmlReader xml = new XmlReader();
    private static InternalFileHandleResolver internalResolver = new InternalFileHandleResolver();
    private static AbsoluteFileHandleResolver absoluteResolver = new AbsoluteFileHandleResolver();

    public static Element parseXML(String path) {
        return parseXML(path, true);
    }

    public static Element parseXML(String path, boolean internal) {
        if (internal) return xml.parse(internalResolver.resolve(path));
        else return xml.parse(absoluteResolver.resolve(path));
    }

    public static void loadProperties(MapProperties properties, Element propElem) {
        if (propElem == null) return;
        if (propElem.getName().equals(VJXString.TILED_properties)) {
            Array<Element> propertyElements = propElem
                            .getChildrenByName(VJXString.TILED_property);
            for (Element property : propertyElements) {
                String name = property.getAttribute(VJXString.name, VJXString.STR_EMPTY);
                String strValue = property.getAttribute(VJXString.value, null);
                String type = property.getAttribute(VJXString.type, null);
                if (strValue == null) strValue = property.getText();

                if (name.isEmpty() || strValue.isEmpty()) continue;
                Object castValue = castProperty(name, strValue, type);
                properties.put(name, castValue);
            }
        }
    }

    public static Object castProperty(String name, String value, String type) {
        // if type is null we assume value should actually be a string
        if (type == null) return value;
        switch (type) {
            case VJXString.VAR_bool:
                if (value == null || value.isEmpty()) return false;
                return Boolean.valueOf(value);
            case VJXString.VAR_color:
                if (value == null || value.isEmpty())
                    return new Color(tiledDflt);

                value = value.replace(VJXString.SEP_NR, VJXString.STR_EMPTY);
                String sr, sg, sb, sa;
                int l = value.length();
                if (l < 8) {
                    sa = VJXString.HEX_255;
                    sr = value.substring(0, 2);
                    sg = value.substring(2, 4);
                    sb = value.substring(4, 6);
                } else {
                    sa = value.substring(0, 2);
                    sr = value.substring(2, 4);
                    sg = value.substring(4, 6);
                    sb = value.substring(6, 8);
                }

                float a = Integer.valueOf(sa, 16) / 255f;
                float r = Integer.valueOf(sr, 16) / 255f;
                float g = Integer.valueOf(sg, 16) / 255f;
                float b = Integer.valueOf(sb, 16) / 255f;
                return new Color(r, g, b, a);
            case VJXString.VAR_float:
                if (value == null || value.isEmpty()) return 0f;
                return Float.valueOf(value);
            case VJXString.VAR_int:
                if (value == null || value.isEmpty()) return 0;
                return Integer.valueOf(value);
            case VJXString.VAR_file:
                if (value == null || value.isEmpty()) return null;
                value = value.replace(VJXString.PATH_DIR_UP, VJXString.STR_EMPTY);
                return Gdx.files.internal(value);
            default:
                throw new GdxRuntimeException("Wrong type given for property "
                                + name + ", given : " + type
                                + ", supported : string, bool, int, float, color");
        }
    }

    /**
     * Extracts various information from a source string and splits them into
     * an array.
     *
     * @param source - Input source string to be converted.
     * @param fileExtension - The file extension of the target file.
     * @return
     * Returns an array containing the internal absolute path of
     * the source folder, the file name without extension and the definition name.
     * That is the file names first part. The other parts are attributes.
     *
     * Return array:
     * sourceSplit[0] = path to source folder
     * sourceSplit[1] = full tile name with attributes without extension
     * sourceSplit[2] = definition name
     * sourceSplit[3] = attributes
     */
    public static String[] getSourceSplit(String source, String fileExtension) {
        String[] split;
        String[] sourceSplit = new String[4];
        if (source.isEmpty()) return null;

        // remove file extension and relativity from tileset file
        source = source.replace(fileExtension, VJXString.STR_EMPTY);
        source = source.replace(VJXString.PATH_DIR_UP, VJXString.STR_EMPTY);
        //        VJXLogger.log(source);

        // split source by "/" to get the path parts
        split = source.split(VJXString.SEP_SLASH);

        // get file name without extension
        sourceSplit[1] = split[split.length - 1];
        //        VJXLogger.log(sourceSplit[1]);

        // remove file name from source
        sourceSplit[0] = source.replace(sourceSplit[1], VJXString.STR_EMPTY);
        sourceSplit[0] = sourceSplit[0].replace(VJXString.DIR_rawAssets, VJXString.STR_EMPTY);

        // remove file attributes to get clean tile name
        sourceSplit[1] = sourceSplit[1].replace(VJXString.PRE_ico_, VJXString.STR_EMPTY);
        sourceSplit[1] = sourceSplit[1].replace(VJXString.POST_icon, VJXString.STR_EMPTY);

        sourceSplit[2] = sourceSplit[1];

        split = sourceSplit[1].split(VJXString.SEP_USCORE);
        if (split.length > 1) sourceSplit[3] = split[1];
        else sourceSplit[3] = VJXString.STR_EMPTY;

        if (!sourceSplit[3].isEmpty()) {
            // split attributes from tile number
            String[] attSplit = sourceSplit[3].split(VJXString.SEP_NR);
            if (attSplit.length > 1)
                sourceSplit[3] = attSplit[0];
        }

        return sourceSplit;
    }

    public static String parseString(String[] split, int ind) {
        if (split.length <= ind) return VJXString.STR_EMPTY;
        return split[ind];
    }

    public static boolean parseBool(String[] split, int ind) {
        if (split.length <= ind) return false;
        return Boolean.parseBoolean(split[ind]);
    }

    public static int parseInt(String[] split, int ind) {
        if (split.length <= ind) return 0;
        return Integer.parseInt(split[ind]);
    }

    public static float parseFloat(String[] split, int ind) {
        if (split.length <= ind) return 0f;
        return Float.parseFloat(split[ind]);
    }

    public static final String ERROR_STRING_NULL_OR_EMPTY = "String null or empty.";
    public static final String ERROR_PARSING_POSITION = "Error parsing position: ";
    public static final String ERROR_PARSING_VECTOR = "Error parsing vector: ";
    public static final String ERROR_VALID_VECTOR = "Vector string format is <<float_x>,<float_y>>";

    public static Vector2 parsePosition(String positionString, Vector2 position,
                    LevelStage level) {
        if (positionString == null || positionString.isEmpty()) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_POSITION + positionString);
            VJXLogger.log(LogCategory.ERROR, ERROR_STRING_NULL_OR_EMPTY);
            return null;
        }

        EJXEntity entity = level.getEntityByName(positionString);
        if (entity != null) {
            if (entity.getDef().getCustomProperties()
                            .get(VJXString.subcategory, VJXString.STR_EMPTY, String.class)
                            .equals("point")) {
                Waypoints.getWaypoint(positionString).getCenter(position);
            } else position.set(entity.getWorldCenter());
        } else {
            position = EJXUtil.parseVector(positionString, position);

            if (position == null) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_POSITION + positionString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_INVALID_PARAM_POSITION);
                return null;
            }
        }

        return position;
    }

    public static Vector2 parseVector(String vectorString, Vector2 vector) {
        if (vectorString == null || vectorString.isEmpty()) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_VECTOR + vectorString);
            VJXLogger.log(LogCategory.ERROR, ERROR_STRING_NULL_OR_EMPTY);
            return null;
        }

        String[] vectorSplit = vectorString.split(VJXString.SEP_LIST);

        if (!vectorString.matches(EJXRegEx.matchVector)) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_VECTOR + vectorString);

            if (!vectorSplit[0].matches(EJXRegEx.matchFloat)) {
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + EJXError.ERROR_INVALID_PARAM_FLOAT);
                return null;
            }

            if (vectorSplit.length < 2) {
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_2
                                + ERROR_VALID_VECTOR);
                return null;
            }

            if (!vectorSplit[1].matches(EJXRegEx.matchFloat)) {
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                + EJXError.ERROR_INVALID_PARAM_FLOAT);
                return null;
            }
        }

        vector.x = Float.parseFloat(vectorSplit[0]);
        vector.y = Float.parseFloat(vectorSplit[1]);

        if (vectorSplit.length > 2) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_2 + vectorString);
        }
        return vector;
    }

    public static Vector2 parseVector(Vector2 vector, String[] split, int ind) {

        if (ind < 0 || ind >= split.length) {
            VJXLogger.log(LogCategory.ERROR,
                            "Error parsing vector: Index out of bounds.");
            return vector;
        }
        String[] vectorSplit = split[ind].split(VJXString.SEP_LIST);

        if (vectorSplit.length != 2) {
            VJXLogger.log(LogCategory.ERROR,
                            "Error parsing vector from " + split[ind]);
            return vector;
        }

        vector.x = Float.parseFloat(vectorSplit[0]);
        vector.y = Float.parseFloat(vectorSplit[1]);

        return vector;
    }

    public static float modAngle(float a, float b) {
        a += b;
        if (a > 360f) a -= 360f;
        else if (a < 0f) a += 360f;
        return a;
    }

    public static String shortNameTMX(String name) {
        return shortName(name, VJXString.SEP_SLASH, VJXString.FILE_TMX);
    }

    public static String shortNamePNG(String name) {
        return shortName(name, VJXString.SEP_SLASH, VJXString.FILE_PNG);
    }

    public static String shortNameJPG(String name) {
        return shortName(name, VJXString.SEP_SLASH, VJXString.FILE_JPG);
    }

    public static String shortNameImage(String name) {
        if (name.contains(VJXString.FILE_PNG))
            return shortName(name, VJXString.SEP_SLASH, VJXString.FILE_PNG);
        if (name.contains(VJXString.FILE_JPG))
            return shortName(name, VJXString.SEP_SLASH, VJXString.FILE_JPG);
        return name;
    }

    public static String shortNameMMP3(String name) {
        return shortName(name, VJXString.SEP_SLASH, VJXString.FILE_MP3);
    }

    public static String shortName(String name, String sep, String... ends) {
        if (name == null) return null;
        String[] split;
        if (name.contains(sep)) {
            split = name.split(sep);
            name = split[split.length - 1];
        }
        for (String end : ends)
            if (name.contains(end)) {
                split = name.split(end);
                return split[0];
            }
        return name;
    }

    @SuppressWarnings("unchecked")
    public static ArrayList<Vector2> quickHull(ArrayList<Vector2> points) {
        ArrayList<Vector2> convexHull = new ArrayList<>();
        if (points.size() < 3) return (ArrayList<Vector2>) points.clone();
        // find extremals
        int minPoint2 = -1, maxPoint2 = -1;
        float minX = Integer.MAX_VALUE;
        float maxX = Integer.MIN_VALUE;
        for (int i = 0; i < points.size(); i++) {
            if (points.get(i).x < minX) {
                minX = points.get(i).x;
                minPoint2 = i;
            }
            if (points.get(i).x > maxX) {
                maxX = points.get(i).x;
                maxPoint2 = i;
            }
        }
        Vector2 A = points.get(minPoint2);
        Vector2 B = points.get(maxPoint2);
        convexHull.add(A);
        convexHull.add(B);
        points.remove(A);
        points.remove(B);

        ArrayList<Vector2> leftSet = new ArrayList<>();
        ArrayList<Vector2> rightSet = new ArrayList<>();

        for (int i = 0; i < points.size(); i++) {
            Vector2 p = points.get(i);
            if (Vector2Location(A, B, p) == -1) leftSet.add(p);
            else rightSet.add(p);
        }
        hullSet(A, B, rightSet, convexHull);
        hullSet(B, A, leftSet, convexHull);

        return convexHull;
    }

    private static void hullSet(Vector2 A, Vector2 B, ArrayList<Vector2> set,
                    ArrayList<Vector2> hull) {
        int insertPosition = hull.indexOf(B);
        if (set.size() == 0) return;
        if (set.size() == 1) {
            Vector2 p = set.get(0);
            set.remove(p);
            hull.add(insertPosition, p);
            return;
        }
        float dist = Integer.MIN_VALUE;
        int furthestVector2 = -1;
        for (int i = 0; i < set.size(); i++) {
            Vector2 p = set.get(i);
            float distance = distance(A, B, p);
            if (distance > dist) {
                dist = distance;
                furthestVector2 = i;
            }
        }
        Vector2 P = set.get(furthestVector2);
        set.remove(furthestVector2);
        hull.add(insertPosition, P);

        // Determine who's to the left of AP
        ArrayList<Vector2> leftSetAP = new ArrayList<>();
        for (int i = 0; i < set.size(); i++) {
            Vector2 M = set.get(i);
            if (Vector2Location(A, P, M) == 1) //set.remove(M);
                leftSetAP.add(M);
        }

        // Determine who's to the left of PB
        ArrayList<Vector2> leftSetPB = new ArrayList<>();
        for (int i = 0; i < set.size(); i++) {
            Vector2 M = set.get(i);
            if (Vector2Location(P, B, M) == 1) //set.remove(M);
                leftSetPB.add(M);
        }
        hullSet(A, P, leftSetAP, hull);
        hullSet(P, B, leftSetPB, hull);
    }

    private static int Vector2Location(Vector2 A, Vector2 B, Vector2 P) {
        float cp1 = (B.x - A.x) * (P.y - A.y) - (B.y - A.y) * (P.x - A.x);
        return cp1 > 0 ? 1 : -1;
    }

    private static float distance(Vector2 A, Vector2 B, Vector2 C) {
        float ABx = B.x - A.x;
        float ABy = B.y - A.y;
        float num = ABx * (A.y - C.y) - ABy * (A.x - C.x);
        if (num < 0) num = -num;
        return num;
    }

    /**
     * @author Torge Rothe (XRJ, X-Ray-Jin)
     *
     *           n2
     * y=1   v3--e2--v2
     *       |       |
     *    n3 e3      e1 n1
     *       |       |
     * y=0   v0--e0--v1
     *           n0
     *       x=0    x=1
     */
    //    public static class MarchingSquares {
    //
    //        private static Pool<Vector2> vec2Pool = new Pool<Vector2>() {
    //
    //            @Override
    //            protected Vector2 newObject() {
    //
    //                return new Vector2();
    //            }
    //        };
    //
    //        public static Vector2[] getCasePoints(int tileCase) {
    //            int[] ids = casePoints[tileCase];
    //            Vector2[] points = new Vector2[ids.length];
    //
    //            for (int i = 0; i < ids.length; i++) {
    //                points[i] = new Vector2(vertices[ids[i]][0],
    //                                vertices[ids[i]][1]);
    //            }
    //
    //            return points;
    //        }
    //
    //        private static final float[][] vertices = new float[][] { { .0f, 0f },
    //                        { .25f, 0f }, { .5f, 0f }, { .75f, 0f }, { 1f, 0f },
    //                        { .0f, .25f }, { .25f, .25f }, { .5f, .25f },
    //                        { .75f, .25f }, { 1f, .25f }, { .0f, .5f },
    //                        { .25f, .5f }, { .5f, .5f }, { .75f, .5f }, { 1f, .5f },
    //                        { .0f, .75f }, { .25f, .75f }, { .5f, .75f },
    //                        { .75f, .75f }, { 1f, .75f }, { .0f, 1f },
    //                        { .25f, .1f }, { .5f, 1f }, { .75f, 1f }, { 1f, 1f } };
    //
    //        private static final int[][] casePoints = new int[][] { {},
    //                        { 14, 2, 4 }, { 14, 0, 4 }, { 14, 12, 2, 4 },
    //                        { 2, 22, 20, 0 }, { 24, 12, 20, 0, 4 },
    //                        { 0, 11, 13, 4, 24, 20 }, { 24, 2, 20, 0, 4 },
    //                        { 0, 4, 24, 20, 0 }, { 22, 10, 0, 4, 24 },
    //                        { 24, 10, 0, 4, 24 }, { 22, 12, 10, 0, 4, 24 },
    //                        { 22, 2, 4, 24 }, { 4, 12, 0 }, { 4, 13, 11, 0 },
    //                        { 4, 22, 0 }, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
    //                        {}, {}, {}, {}, {}, {} };
    //
    //        public static final int[][] neighbors = new int[][] { {}, { 0, 1 },
    //                        { 0, 1 }, { 0, 1 }, { 2, 0, 3 }, { 3, 1, 0 },
    //                        { 1, 3, 2 }, { 3, 1, 0 }, { 0, 1, 2, 3 },
    //                        { 3, 2, 0, 1 }, { 3, 1, 0 }, { 3, 2, 0, 1 },
    //                        { 0, 2, 1 }, { 0 }, { 0 }, { 0 }, {}, {}, {}, {}, {},
    //                        {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {} };
    //
    //        public static final int[][] neighborDir = new int[][] { { 0, -1 },
    //                        { 1, 0 }, { 0, 1 }, { -1, 0 } };
    //
    //        private static final int[][] edges = new int[][]
    //                { { },                                                          // 0
    //                  { 3, 0 },                                                     // 1
    //                  { 0, 1 },                                                     // 2
    //                  { 3, 1 },                                                     // 3
    //                  { 1, 2 },                                                     // 4
    //                  { 1, 0, 3, 2 },                                               // 5
    //                  { 0, 2 },                                                     // 6
    //                  { 3, 2 },                                                     // 7
    //
    //                  { 2, 3 },                                                     // 8
    //                  { 2, 0 },                                                     // 9
    //                  { 0, 3, 2, 1 },                                               // 10
    //                  { 2, 1 },                                                     // 11
    //                  { 1, 3 },                                                     // 12
    //                  { 1, 0 },                                                     // 13
    //                  { 0, 3 },                                                     // 14
    //                  { } };                                                        // 15
    //
    //        //        private static final boolean[][] neighbors = new boolean[][]
    //        //                // bottom, right, top, left
    //        //                { { false, false, false, false },                             // 0
    //        //                  { true, false, false, true },                               // 1
    //        //                  { true, true, false, false },                               // 2
    //        //                  { false, true, false, true },                               // 3
    //        //                  { false, true, true, false },                                   // 4
    //        //                  { true, true, true, true },                                   // 5
    //        //                  { true, false, true, false },                                  // 6
    //        //                  { false, false, true, true },                                 // 7
    //        //
    //        //                  { false, false, true, true },                                   // 8
    //        //                  { true, false, true, false },                                  // 9
    //        //                  { true, true, true, true },                                   // 10
    //        //                  { false, true, true, false },                                 // 11
    //        //                  { false, true, false, true },                                  // 12
    //        //                  { true, true, false, false },                                 // 13
    //        //                  { true, false, false, true },                                 // 14
    //        //                  { false, false, false, false } };                             // 15
    //
    //        //        private static LayerCell findNext(TiledMapTileLayer layer, String name,
    //        //                        int x, int y, boolean checked[][],
    //        //                        List<Vector2> points) {
    //        //            int width = layer.getWidth();
    //        //            int height = layer.getHeight();
    //        //
    //        //            TiledMapTile tile;
    //        //            int caseID;
    //        //
    //        //            if (checked[x][y]) return null;
    //        //
    //        //            LayerCell cell = (LayerCell) layer.getCell(x, y);
    //        //            if (cell == null) return null;
    //        //
    //        //            tile = layer.getCell(x, y).getTile();
    //        //            caseID = tile.getProperties().get("caseID", 15, Integer.class);
    //        //
    //        //            if (caseID == 0 || caseID == 15) return null;
    //        //
    //        //            if (neighbors[caseID][0] && y > 0) {
    //        //                if (!checked[x][y - 1]) {
    //        //                    cell.setNeighbor(0, findNext(layer, "ground", x, y - 1,
    //        //                                    checked, points));
    //        //                }
    //        //            }
    //        //            if (neighbors[caseID][1] && x < width - 1) {
    //        //                if (!checked[x + 1][y]) {
    //        //                    cell.setNeighbor(1, findNext(layer, "ground", x + 1, y,
    //        //                                    checked, points));
    //        //                }
    //        //            }
    //        //            if (neighbors[caseID][2] && y < height - 1) {
    //        //                if (!checked[x][y + 1]) {
    //        //                    cell.setNeighbor(2, findNext(layer, "ground", x, y + 1,
    //        //                                    checked, points));
    //        //                }
    //        //            }
    //        //            if (neighbors[caseID][3] && x > 0) {
    //        //                if (!checked[x - 1][y]) {
    //        //                    cell.setNeighbor(3, findNext(layer, "ground", x - 1, y,
    //        //                                    checked, points));
    //        //                }
    //        //            }
    //        //
    //        //            return cell;
    //        //        }
    //        private static ArrayList<Vector2> pts = new ArrayList<>();
    //
    //        public static void findChainVerts(LayerCell cell, int x, int y,
    //                        int direction,
    //                        TiledMapTileLayer layer, boolean[][] checked) {
    //            VJXUtility.log(LoggerCategory.ALL,
    //                            "    find direction: " + direction);
    //
    //            TiledMapTile tile;
    //            MapProperties tileProps;
    //            Vector2 vert;
    //            int caseID, edgeID, tmpX, tmpY;
    //            float tileWidth = layer.getTileWidth();
    //            float tileHeight = layer.getTileHeight();
    //            boolean flipped;
    //
    //            tmpX = x;
    //            tmpY = y;
    //            while (cell != null) {
    //                checked[tmpX][tmpY] = true;
    //
    //                tile = cell.getTile();
    //                tileProps = tile.getProperties();
    //                caseID = Integer.parseInt(
    //                                tileProps.get("caseID", "15", String.class));
    //                if (caseID == 0 || caseID == 15) {
    //                    cell = null;
    //                    continue;
    //                }
    //                int id = direction;
    //                VJXUtility.log(LoggerCategory.ALL, "       add cell rotation"
    //                                + cell.getRotation());
    //
    //                // if flipped switch insert order of the next vertex
    //                // and switch next search edge
    //                flipped = cell.getFlipHorizontally() || cell.getFlipVertically();
    //                edgeID = edges[caseID][direction];
    //                VJXUtility.log(LoggerCategory.ALL,
    //                                "       add cell " + tmpX + ", " + tmpY
    //                                                + ", case: " + caseID
    //                                                + ", edgeID: " + edgeID);
    //
    //                vert = new Vector2(vertices[edgeID][0], vertices[edgeID][1])
    //                                .scl(tileWidth, tileHeight);
    //                VJXUtility.log(LoggerCategory.ALL, "            vert " + vert);
    //
    //                // set/flip vertex coordinates
    //                vert.x = cell.getFlipHorizontally() ? -vert.x + tileWidth : vert.x;
    //                vert.y = cell.getFlipVertically() ? -vert.y + tileWidth : vert.y;
    //
    //                VJXUtility.log(LoggerCategory.ALL, "            vert " + vert);
    //
    //                // translate vertex to cell position
    //                vert.x += tmpX * tileWidth;
    //                vert.y += tmpY * tileHeight;
    //                VJXUtility.log(LoggerCategory.ALL, "            vert " + vert);
    //
    //                // insert and scale vertex for box2d world
    //                // 0 = forward (add at the end), 1 = backwards (insert at start)
    //
    //                if (direction == 0) pts.add(0, vert.scl(.01f));
    //                else if (direction == 1) pts.add(vert.scl(.01f));
    //                VJXUtility.log(LoggerCategory.ALL, "            vert " + vert);
    //
    //                // find next neighbor
    //                tmpX = tmpX + neighborDir[edgeID][0];
    //                tmpY = tmpY + neighborDir[edgeID][1];
    //                VJXUtility.log(LoggerCategory.ALL,
    //                                "       next cell " + tmpX + ", " + tmpY);
    //                cell = (LayerCell) layer.getCell(tmpX, tmpY);
    //            }
    //        }
    //
    //        public static Array<FixtureDef> marchingSquares(
    //                        TiledMapTileLayer layer) {
    //            VJXUtility.log(LoggerCategory.ALL, "layer: " + layer.getName()
    //                            + " - find cells, create fixtures");
    //            int width = layer.getWidth();
    //            int height = layer.getHeight();
    //            //            float tileWidth = layer.getTileWidth();
    //            //            float tileHeight = layer.getTileHeight();
    //
    //            Array<FixtureDef> fixtureDefs = new Array<>();
    //            ChainShape groundShape = new ChainShape();
    //
    //            LayerCell cell;
    //            TiledMapTile tile;
    //            MapProperties tileProps;
    //            Vector2 start, end;
    //            //            ArrayList<Vector2> pts = new ArrayList<>();
    //            LayerCell tmpCell;
    //            int edge0, edge1;
    //            int caseID, tmpX, tmpY;
    //            boolean checked[][] = new boolean[width][height];
    //            for (int x = 0; x < width; x++) {
    //                for (int y = 0; y < height; y++) {
    //                    if (checked[x][y]) continue;
    //
    //                    cell = (LayerCell) layer.getCell(x, y);
    //                    if (cell == null) {
    //                        checked[x][y] = true;
    //                        continue;
    //                    }
    //
    //                    tile = cell.getTile();
    //                    tileProps = tile.getProperties();
    //                    caseID = Integer.parseInt(
    //                                    tileProps.get("caseID", "15", String.class));
    //
    //                    if (caseID == 0 || caseID == 15) {
    //                        checked[x][y] = true;
    //                        continue;
    //                    }
    //                    VJXUtility.log(LoggerCategory.ALL, x + ", " + y
    //                                    + ": fixture found, caseID: " + caseID);
    //
    //                    pts.clear();
    //
    //                    findChainVerts(cell, x, y, 1, layer, checked);
    //
    //                    findChainVerts(cell, x, y, 0, layer, checked);
    //
    //                    VJXUtility.log(LoggerCategory.ALL,
    //                                    "shape vertCount: " + pts.size());
    //                    VJXUtility.log(LoggerCategory.ALL,
    //                                    "----------------------");
    //
    //                    groundShape = new ChainShape();
    //                    Vector2[] verts = new Vector2[pts.size()];
    //                    groundShape.createChain(pts.toArray(verts));
    //
    //                    FixtureDef fixDef = new FixtureDef();
    //                    fixDef.shape = groundShape;
    //                    //                    fixDef.filter.categoryBits = tmpCat;
    //                    //                    fixDef.filter.categoryBits |= b2dCategory;
    //                    //
    //                    //                    fixDef.filter.maskBits = 0;
    //                    //                    fixDef.filter.maskBits |= PhysCat.PLAYER.id;
    //                    //                    fixDef.filter.maskBits |= PhysCat.ENEMY.id;
    //                    //                    fixDef.filter.maskBits |= PhysCat.NEUTRAL.id;
    //                    //                    fixDef.filter.maskBits |= PhysCat.OBJECT.id;
    //                    //                    fixDef.filter.maskBits |= b2dMask;
    //                    //                    fixDef.restitution = 0f;
    //                    //                    //        fixDef.friction = .5f;
    //                    //
    //                    //                    fixDef.isSensor = physCat.contains("ground") ? false : true;
    //                    fixtureDefs.add(fixDef);
    //
    //                    VJXUtility.log(LoggerCategory.ALL,
    //                                    "   vert count: " + pts.size());
    //                }
    //            }
    //
    //            VJXUtility.log(LoggerCategory.ALL,
    //                            "fixtures count: " + fixtureDefs.size);
    //            VJXUtility.log(LoggerCategory.ALL,
    //                            "layer finished-----------------------------");
    //            return fixtureDefs;
    //        }
    //    }
}