package de.venjinx.ejx.util.loader;

import com.badlogic.gdx.files.FileHandle;

import de.venjinx.ejx.tiled.TiledFactory;

public abstract class TiledTask extends LoadFileTask {

    protected TiledFactory factory;

    public TiledTask(FileHandle file, TiledFactory factory, boolean loadOnFinish) {
        super(file, loadOnFinish);
        this.factory = factory;
    }
}