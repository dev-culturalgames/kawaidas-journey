package de.venjinx.ejx.util.loader;

import com.badlogic.gdx.utils.Array;

import de.venjinx.ejx.EJXAssetManager;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class EJXLoader {

    private EJXAssetManager assets;

    private boolean active;
    private LoaderTask currentTask;

    private Array<LoaderTask> tasks;

    public EJXLoader(EJXAssetManager assets) {
        this.assets = assets;
        tasks = new Array<>();
    }

    public long process(float processTimeMilli) {
        long timer = 0;
        while (active && timer < processTimeMilli) {
            long time = System.currentTimeMillis();
            finishCurrentTask();
            startNextTask();
            timer += System.currentTimeMillis() - time;
        }
        return timer;
    }

    public LoaderTask addTask(LoaderTask task) {
        tasks.add(task);
        if (!active) {
            if (tasks.size == 1) {
                VJXLogger.logIncr(LogCategory.LOAD, "Loader start loading...");
            }
            startNextTask();
        }
        return task;
    }

    public void finishLoading() {
        VJXLogger.logIncr(LogCategory.LOAD, "Loader finish loading...");
        if (currentTask != null && !currentTask.isFinished()) {
            finishCurrentTask();
        }
        while (!tasks.isEmpty()) {
            currentTask = tasks.removeIndex(0);
            finishCurrentTask();
        }
        VJXLogger.logDecr(LogCategory.LOAD, "Loader finished loading.");
        startNextTask();
    }

    private void finishCurrentTask() {
        VJXLogger.logIncr(LogCategory.LOAD | LogCategory.DETAIL,
                        "Loader starting task: " + currentTask);
        currentTask.execute();
        if (currentTask instanceof LoadFileTask) {
            if (!((LoadFileTask) currentTask).loadOnFinish()) return;
            loadResources((LoadFileTask) currentTask);
        }
        VJXLogger.logDecr(LogCategory.LOAD | LogCategory.DETAIL,
                        "Loader task finished: '" + currentTask + "'. Time: "
                                        + currentTask.getTime() + "ms");
        currentTask.onFinished();
    }

    private void loadResources(LoadFileTask task) {
        for (String name : task.getResources().keys()) {
            assets.addResource(name, task.getResources().get(name));
        }
        assets.finishLoading();
    }

    public boolean isActive() {
        return active;
    }

    private void startNextTask() {
        if (!tasks.isEmpty()) {
            currentTask = tasks.removeIndex(0);
            active = true;
        } else {
            currentTask = null;
            if (active)
                VJXLogger.logDecr(LogCategory.LOAD,
                                "Loader finished: No more tasks.");
            active = false;
        }
    }
}