package de.venjinx.ejx.util.loader;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;

import de.venjinx.ejx.tiled.EJXTiledMap;
import de.venjinx.ejx.tiled.TiledFactory;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class TMXFileTask extends TiledTask {

    private EJXTiledMap map;

    public TMXFileTask(FileHandle file, TiledFactory factory, boolean loadOnFinish) {
        super(file, factory, loadOnFinish);
    }

    @Override
    public void process() {
        loadTMXFile();
    }

    public EJXTiledMap getMap() {
        return map;
    }

    private void loadTMXFile() {
        if (file == null || !file.exists()) {
            VJXLogger.log(LogCategory.ERROR,
                            "TMXFileTask failed: File null or not existing.");
            return;
        }
        map = factory.mapFromTMX(file);
    }

    @Override
    public ObjectMap<String, AssetDescriptor<? extends Disposable>> getResources() {
        return map.getResources();
    }
}