package de.venjinx.ejx.util.loader;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;

import de.venjinx.ejx.tiled.TiledFactory;
import de.venjinx.ejx.tiled.tileset.EJXAbstractTileset;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class TSXFileTask extends TiledTask {

    private EJXAbstractTileset tileset;

    public TSXFileTask(FileHandle file, TiledFactory factory, boolean loadOnFinish) {
        super(file, factory, loadOnFinish);
    }

    @Override
    public void process() {
        loadTSXFile();
    }

    public EJXAbstractTileset getSet() {
        return tileset;
    }

    private void loadTSXFile() {
        if (file == null || !file.exists()) {
            VJXLogger.log(LogCategory.ERROR,
                            "TSXFileTask failed: File null or not existing.");
            return;
        }
        tileset = factory.tilesetFromTSX(file);
    }

    @Override
    public ObjectMap<String, AssetDescriptor<? extends Disposable>> getResources() {
        return tileset.getResources();
    }
}