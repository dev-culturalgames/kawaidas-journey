package de.venjinx.ejx.util.loader;

import java.util.Iterator;
import java.util.StringTokenizer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.util.EJXTypes;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.VJXProperties;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.LevelUtil;

public class EJXLoaderUtil {

    private static XmlReader xml = new XmlReader();

    public static Element loadXMLFile(String path) {
        return loadXMLFile(path, false);
    }

    public static Element loadXMLFile(String path, boolean absolute) {
        if (!absolute) return loadXMLFile(Gdx.files.internal(path));
        else return loadXMLFile(Gdx.files.absolute(path));
    }

    public static Element loadXMLFile(FileHandle file) {
        Element root;
        root = xml.parse(file);
        return root;
    }

    public static Element getLayerElement(String layerName, Element mapElem) {
        Element element;
        String name;
        for (int i = 0, j = mapElem.getChildCount(); i < j; i++) {
            element = mapElem.getChild(i);
            name = element.getAttribute(VJXString.name, VJXString.name);
            if (name.equals(layerName)) { return element; }
        }
        return null;
    }

    public static MapProperties loadProperties(MapProperties properties,
                    Element propElem) {
        return loadProperties(properties, propElem, null);
    }

    public static MapProperties loadProperties(MapProperties properties,
                    Element propElem, String prefix) {
        if (properties == null) properties = new MapProperties();
        if (propElem == null) return properties;

        Array<Element> propertyElements = propElem
                        .getChildrenByName(VJXString.TILED_property);
        for (Element property : propertyElements) {
            String name = property.getAttribute(VJXString.name, null);
            if (name == null) continue;

            String type = property.getAttribute(VJXString.type, null);

            String strValue = property.getAttribute("value", null);
            if (strValue == null) strValue = property.getText();
            if (strValue == null) continue;

            if (prefix != null) name = prefix + name;
            properties.put(name, EJXUtil.castProperty(name, strValue, type));
        }
        return properties;
    }

    private static MapProperties tmpProperties = new MapProperties();

    public static VJXProperties loadEJXProperties(EJXPropertyDefinition[] propList,
                    VJXProperties properties, Element propElem) {
        if (properties == null) properties = EJXTypes.newProps();
        if (propElem == null) return properties;

        tmpProperties.clear();

        Array<Element> propertyElements = propElem
                        .getChildrenByName(VJXString.TILED_property);
        for (Element property : propertyElements) {
            String name = property.getAttribute(VJXString.name, null);
            if (name == null) continue;

            String type = property.getAttribute(VJXString.type, null);

            String strValue = property.getAttribute(VJXString.value, null);
            if (strValue == null) strValue = property.getText();
            if (strValue == null) continue;

            tmpProperties.put(name, EJXUtil.castProperty(name, strValue, type));
        }
        LevelUtil.convertProperties(propList, properties, tmpProperties);
        return properties;
    }

    public static void addProps(MapProperties dstProps, MapProperties srcProps,
                    String prefix) {
        Iterator<String> iter = srcProps.getKeys();
        String key;
        while (iter.hasNext()) {
            key = iter.next();
            dstProps.put(prefix + key, srcProps.get(key));
        }
    }

    public static FileHandle solveInternalPath(FileHandle file, String path) {
        FileHandle result = new FileHandle(
                        file.path() + VJXString.SEP_SLASH + path);
        return result;
    }

    public static FileHandle getRelativeFileHandle(FileHandle file,
                    String path) {
        StringTokenizer tokenizer = new StringTokenizer(path, "\\/");
        FileHandle result = file.parent();
        while (tokenizer.hasMoreElements()) {
            String token = tokenizer.nextToken();
            if (token.equals("..")) result = result.parent();
            else {
                result = result.child(token);
            }
        }
        return result;
    }

    public static FileHandle convertRawPath(String path) {
        path = path.replace(VJXString.PATH_DIR_UP, VJXString.STR_EMPTY);
        String split[] = path.split("rawAssets/");
        if (split.length > 1) path = split[1];
        return new FileHandle(path);
    }

    /**
     * Extracts various information from a source string and splits them into
     * an array.
     *
     * @param source - Input source string to be converted.
     * @param fileExtension - The file extension of the target file.
     * @return
     * Returns an array containing the internal absolute path of
     * the source folder, the file name without extension and the definition name.
     * That is the file names first part. The other parts are attributes.
     *
     * Return array:
     * sourceSplit[0] = path to source folder
     * sourceSplit[1] = full tile name with attributes without extension
     * sourceSplit[2] = definition name
     * sourceSplit[3] = attributes
     */
    public static String[] getSourceSplit(String source) {
        String[] split;
        String[] sourceSplit = new String[4];
        if (source.isEmpty()) return null;

        //        fileExtension = split[1];
        source = source.replace(VJXString.PATH_DIR_UP, VJXString.STR_EMPTY);
        source = source.split(VJXString.SEP_DOT)[0];

        // remove file extension and relativity from tileset file
        //        source = source.replace(fileExtension, VJXString.STR_EMPTY);

        // split source by "/" to get the path parts
        split = source.split(VJXString.SEP_SLASH);

        // get file name without extension
        sourceSplit[1] = split[split.length - 1];

        // remove file name from source
        sourceSplit[0] = source.replace(sourceSplit[1], VJXString.STR_EMPTY);
        sourceSplit[0] = sourceSplit[0].replace(VJXString.DIR_rawAssets,
                        VJXString.STR_EMPTY);

        // remove file attributes to get clean tile name
        sourceSplit[1] = sourceSplit[1].replace(VJXString.PRE_ico_,
                        VJXString.STR_EMPTY);
        sourceSplit[1] = sourceSplit[1].replace(VJXString.POST_icon,
                        VJXString.STR_EMPTY);

        split = sourceSplit[1].split(VJXString.SEP_USCORE);
        if (split.length > 1) sourceSplit[2] = split[1];
        else sourceSplit[2] = VJXString.STR_EMPTY;

        sourceSplit[3] = VJXString.INT_0;
        if (!sourceSplit[2].isEmpty()) {
            // split attributes from tile number
            String[] attSplit = sourceSplit[2].split(VJXString.SEP_NR);
            if (attSplit.length > 1) {
                sourceSplit[2] = attSplit[0];
                sourceSplit[3] = attSplit[1];
            }
        }

        return sourceSplit;
    }
}