package de.venjinx.ejx.util.loader;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;

import de.venjinx.ejx.util.EJXTypes.VJXString;

public abstract class LoadFileTask extends LoaderTask {

    protected FileHandle file;
    private boolean loadOnFinish;

    public LoadFileTask(FileHandle file, boolean loadOnFinish) {
        super();
        this.file = file;
        this.loadOnFinish = loadOnFinish;
    }

    public boolean loadOnFinish() {
        return loadOnFinish;
    }

    public abstract ObjectMap<String, AssetDescriptor<? extends Disposable>> getResources();

    @Override
    public String toString() {
        return getClass().getSimpleName() + VJXString.SEP_DDOT_SPACE + file.path();
    }
}