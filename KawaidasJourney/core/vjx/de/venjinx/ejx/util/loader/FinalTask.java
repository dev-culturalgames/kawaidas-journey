package de.venjinx.ejx.util.loader;

public class FinalTask extends LoaderTask {

    public FinalTask(Runnable finalTask) {
        setOnFinished(finalTask);
    }

    @Override
    public void process() {
    }
}