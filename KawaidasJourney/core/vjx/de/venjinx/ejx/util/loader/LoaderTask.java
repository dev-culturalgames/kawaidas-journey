package de.venjinx.ejx.util.loader;

import de.venjinx.ejx.util.EJXTypes.VJXString;

public abstract class LoaderTask {

    private Runnable onFinished;

    private long time = 0;
    private boolean finished = false;

    public LoaderTask() {
    }

    public abstract void process();

    public void execute() {
        time = System.currentTimeMillis();
        process();
        finished = true;
        time = System.currentTimeMillis() - time;
    }

    public void reset() {
        time = 0;
        finished = false;
    }

    public float getTime() {
        return time;
    }

    public boolean isFinished() {
        return finished;
    }

    public void onFinished() {
        if (onFinished != null) onFinished.run();
    }

    public void setOnFinished(Runnable onFinished) {
        this.onFinished = onFinished;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + VJXString.SEP_DDOT_SPACE;
    }
}