package de.venjinx.ejx.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.EJXCamera.CamMode;
import de.venjinx.ejx.animation.EJXAnimation;
import de.venjinx.ejx.audio.VJXSound;
import de.venjinx.ejx.controllers.AudioControl;
import de.venjinx.ejx.entity.CollisionDef;
import de.venjinx.ejx.entity.CollisionDef.CircShape;
import de.venjinx.ejx.entity.CollisionDef.CollShape;
import de.venjinx.ejx.entity.CollisionDef.PolyShape;
import de.venjinx.ejx.entity.CollisionDef.RectShape;
import de.venjinx.ejx.entity.logics.Box2DLogic.Box2DProperty;
import de.venjinx.ejx.entity.logics.EJXLogicDef;
import de.venjinx.ejx.util.EJXTypes.AnimationType;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.Category;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.EJXFloat;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;
import de.venjinx.ejx.util.EJXTypes.SpatialProperty;
import de.venjinx.ejx.util.EJXTypes.VJXProperties;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class LevelUtil {

    public static final Vector2 TMP_VEC0 = new Vector2();

    private static List<MapProperties> projectDefs = new ArrayList<>();
    private static HashMap<String, List<String>> voices = new HashMap<>();
    private static HashMap<String, HashSet<VJXSound>> sounds = new HashMap<>();
    private static HashMap<String, EJXAnimation> animations = new HashMap<>();
    private static HashMap<String, EJXLogicDef> logics = new HashMap<>();

    private static MapProperties tmpProps = new MapProperties();

    public static void addProjectDef(MapProperties props) {
        projectDefs.add(props);
    }

    public static List<MapProperties> getProjectDefs() {
        return projectDefs;
    }

    public static void loadAnimation(Element tile, AudioControl audio) {

        MapProperties props = LevelUtil.xmlToProperties(tile, VJXString.animation);

        if (!animations.containsKey(props.get(EJXAnimation.ANIM_NAME))) {
            EJXAnimation anim = new EJXAnimation(props);
            animations.put(anim.getName(), anim);
            audio.addDynamicSounds(anim.getName(), props);
        }
    }

    public static HashMap<String, EJXAnimation> getAnims() {
        return animations;
    }

    public static void loadLogic(Element tile) {
        MapProperties props = LevelUtil.xmlToProperties(tile, EJXLogicDef.logic);

        if (!logics.containsKey(props.get(EJXLogicDef.LOGIC_NAME))) {
            EJXLogicDef logic = new EJXLogicDef(props);
            logics.put(logic.getName(), logic);
            Element group = tile.getChildByName("objectgroup");
            if (group != null)
                loadCollision(logic.getProperties(), group, logic.getName(), true);
        }
    }

    public static void loadLogicProps(MapProperties properties, String logics) {
        if (!logics.isEmpty()) {
            String[] logicsSplit = logics.split(VJXString.SEP_LIST);

            for (String logic : logicsSplit) {
                EJXLogicDef log = LevelUtil.getLogics().get(logic);

                if (log == null) continue;

                Iterator<String> keys = log.getProperties().getKeys();

                String key;
                while (keys.hasNext()) {
                    key = keys.next();
                    if (key.contains(logic) && !properties.containsKey(key))
                        properties.put(key, log.getProperties().get(key));
                }
            }
        }
    }

    public static void loadCollision(MapProperties objProps, Element objectGroup) {
        loadCollision(objProps, objectGroup, false);
    }

    public static void loadCollision(MapProperties objProps,
                    Element objectGroup, boolean createNew) {
        loadCollision(objProps, objectGroup, VJXString.STR_EMPTY, createNew);
    }

    public static void loadCollision(MapProperties objProps, Element objectGroup,
                    String colliderName, boolean extend) {
        if (colliderName == null)
            colliderName = objProps.get(DefinitionProperty.defName.name,
                            VJXString.STR_EMPTY, String.class);

        Array<Element> objects = objectGroup.getChildrenByName("object");

        if (objects.size == 0) return;
        CollisionDef collDef;

        String objName;
        CollShape shape;

        float w = objProps.get(SourceProperty.srcWidth.name, Integer.class);
        float h = objProps.get(SourceProperty.srcHeight.name, Integer.class);
        w = objProps.get(SpatialProperty.width.name, w, Float.class);
        h = objProps.get(SpatialProperty.height.name, h, Float.class);
        if (!colliderName.isEmpty()) {
            objName = colliderName;
            colliderName += VJXString.SEP_DDOT + DefinitionProperty.collider.name;
        } else {
            colliderName = DefinitionProperty.collider.name;
            objName = objProps.get(DefinitionProperty.defName.name,
                            VJXString.STR_EMPTY, String.class);
        }

        CollisionDef baseCollisions = objProps.get(colliderName, null,
                        CollisionDef.class);
        if (!extend) {
            collDef = new CollisionDef(objName, baseCollisions);
            objProps.put(colliderName, collDef);
        } else {
            if (baseCollisions != null) collDef = baseCollisions;
            else {
                collDef = new CollisionDef(objName);
                objProps.put(colliderName, collDef);
            }
        }

        for (Element collElem : objects) {
            shape = loadShape(collDef, collElem);
            shape.convert(w, h);
            collDef.add(shape);
        }
    }

    public static CollShape loadShape(CollisionDef collDef, Element element) {
        String name = element.getAttribute("name", VJXString.collider)
                        + VJXString.SEP_NR
                        + collDef.getShapeCount();
        tmpProps.clear();
        LevelUtil.xmlToProperties(element, tmpProps);
        tmpProps.put(VJXString.name, name);

        String bitStr = tmpProps.get(Box2DProperty.category.name,
                        B2DBit.NONE.name, String.class);
        String[] bitSplit = bitStr.split(VJXString.SEP_LIST);
        short catBit = 0;
        for (String s : bitSplit) {
            catBit |= B2DBit.get(s).id;
        }

        bitStr = tmpProps.get(Box2DProperty.mask.name, B2DBit.NONE.name, String.class);
        bitSplit = bitStr.split(VJXString.SEP_LIST);
        short maskBits = 0;
        for (String s : bitSplit) {
            maskBits |= B2DBit.get(s).id;
        }

        CollShape shape;
        Element child = element.getChildByName("polygon");
        if (child != null) shape = loadPolygon(collDef, child, name);
        else if (element.getChildByName("ellipse") != null)
            shape = loadCircle(collDef, element, name);
        else shape = loadRect(collDef, element, name);

        shape.x = Float.parseFloat(element.getAttribute("x"));
        shape.y = Float.parseFloat(element.getAttribute("y"));

        shape.categoryBits = catBit;
        shape.maskBits = solvePhysicsBits(catBit);

        shape.friction = tmpProps.get(Box2DProperty.friction.name, 0f, Float.class);
        shape.restitution = tmpProps.get(Box2DProperty.restitution.name, 0f, Float.class);
        shape.isSensor = !(B2DBit.GROUND.isCategory(catBit)
                        || B2DBit.OBJECT.isCategory(catBit));

        if (shape.isSensor)
            shape.isSensor = tmpProps.get(Box2DProperty.isSensor.name, true,
                            Boolean.class);

        shape.maskBits |= maskBits;

        return shape;
    }

    public static CollShape loadPolygon(CollisionDef collDef, Element element,
                    String defName) {
        Vector2 min = new Vector2(Float.MAX_VALUE, Float.MAX_VALUE);
        Vector2 max = new Vector2(Float.MIN_VALUE, Float.MIN_VALUE);

        String pointsAttr = element.getAttribute("points");
        String[] pointsAttrSplit = pointsAttr.split(" ");
        PolyShape shape = collDef.new PolyShape(defName,
                        pointsAttrSplit.length, 0, 0);
        for (int i = 0; i < pointsAttrSplit.length; i++) {
            String[] pointSplit = pointsAttrSplit[i].split(",");
            TMP_VEC0.set(Float.parseFloat(pointSplit[0]),
                            Float.parseFloat(pointSplit[1]));

            shape.addPoint(i, TMP_VEC0.x, TMP_VEC0.y);

            min.x = Math.min(min.x, TMP_VEC0.x);
            min.y = Math.min(min.y, TMP_VEC0.y);

            max.x = Math.max(max.x, TMP_VEC0.x);
            max.y = Math.max(max.y, TMP_VEC0.y);
        }

        shape.w = max.x - min.x;
        shape.h = max.y - min.y;

        return shape;
    }

    public static CollShape loadCircle(CollisionDef collDef, Element element,
                    String defName) {
        float w = Float.parseFloat(element.getAttribute("width"));
        float h = Float.parseFloat(element.getAttribute("height"));
        CircShape shape = collDef.new CircShape(defName, w, h);
        return shape;
    }

    public static CollShape loadRect(CollisionDef collDef, Element element,
                    String defName) {
        float w = Float.parseFloat(element.getAttribute("width"));
        float h = Float.parseFloat(element.getAttribute("height"));
        RectShape shape = collDef.new RectShape(defName, w, h);
        return shape;
    }

    public static short solvePhysicsBits(short categoryBits) {
        short maskBits = 0;

        if (B2DBit.OBJECT.isCategory(categoryBits)) {
            maskBits |= B2DBit.GROUND.id;
            maskBits |= B2DBit.OBJECT.id;
            maskBits |= B2DBit.TRIGGER.id;
        }

        if (B2DBit.DANGER.isCategory(categoryBits)) {
            maskBits |= B2DBit.HITBOX.id;
        }
        if (B2DBit.HITBOX.isCategory(categoryBits)) {
            maskBits |= B2DBit.DANGER.id;
            maskBits |= B2DBit.TRIGGER.id;
        }
        if (B2DBit.CLIMBABLE.isCategory(categoryBits)) {
            maskBits |= B2DBit.SIDE.id;
        }
        if (B2DBit.TRIGGER.isCategory(categoryBits)) {
            maskBits |= B2DBit.OBJECT.id;
            maskBits |= B2DBit.HITBOX.id;
        }
        if (B2DBit.SIDE.isCategory(categoryBits)) {
            maskBits |= B2DBit.GROUND.id;
            maskBits |= B2DBit.CLIMBABLE.id;
            maskBits |= B2DBit.FERTILE.id;
        }
        if (B2DBit.WATER.isCategory(categoryBits)) {
            maskBits |= B2DBit.OBJECT.id;
        }

        // terrain solid
        if (B2DBit.PASSABLE.isCategory(categoryBits)) {
            maskBits |= B2DBit.SIDE.id;
            maskBits |= B2DBit.OBJECT.id;
        }
        if (B2DBit.SOLID.isCategory(categoryBits)) {
            maskBits |= B2DBit.SIDE.id;
            maskBits |= B2DBit.OBJECT.id;
            maskBits |= B2DBit.SOLID.id;
        }
        if (B2DBit.FERTILE.isCategory(categoryBits)) {
            maskBits |= B2DBit.SIDE.id;
        }

        return maskBits;
    }

    public static HashMap<String, EJXLogicDef> getLogics() {
        return logics;
    }

    public static void addEntitySound(String defName, VJXSound sound) {
        HashSet<VJXSound> list = null;
        if (sounds.containsKey(defName)) list = sounds.get(defName);
        else {
            list = new HashSet<>();
            sounds.put(defName, list);
        }
        if (!list.contains(sound)) {
            list.add(sound);

        }
    }

    public static HashSet<VJXSound> getEntitySounds(String defName) {
        return sounds.get(defName);
    }

    public static void convertProperties(EJXPropertyDefinition[] propList,
                    VJXProperties vjxProps, MapProperties props) {
        if (props == null) return;
        for (int i = 0; i < propList.length; i++) {
            LevelUtil.convertProperty(vjxProps, propList[i], props);
        }
    }

    public static void convertProperty(VJXProperties vjxProps,
                    EJXPropertyDefinition prop, MapProperties props) {
        Object o = props.get(prop.getName());
        if (o == null) {
            VJXLogger.log(LogCategory.DETAIL, "Property '"
                            + prop.getName()
                            + "' not found: Using default value: "
                            + prop.getDefault());
            vjxProps.put(prop, prop.getDefault());
            return;
        }

        if (prop.getDefault() != null
                        && o.getClass() != prop.getDefault().getClass()) {
            boolean solved = false;
            if (o instanceof String) {
                String s = (String) o;
                switch (prop.getType()) {
                    case categoryProp:
                        Category cat = Category.get(s);
                        vjxProps.put(prop, cat);
                        solved = true;
                        break;
                        //                    case activityProp:
                        //                        Activity activity = Activity.get(s);
                        //                        vjxProps.put(prop, activity);
                        //                        solved = true;
                        //                        break;
                    case animationType:
                        AnimationType animType = AnimationType.get(s);
                        vjxProps.put(prop, animType);
                        solved = true;
                        break;
                    case boolProp:
                        boolean b = Boolean.parseBoolean(s);
                        vjxProps.put(prop, b);
                        solved = true;
                        break;
                    case floatProp:
                        float f = Float.parseFloat(s);
                        vjxProps.put(prop, f);
                        solved = true;
                        break;
                    case ejxFloat:
                        f = Float.parseFloat(s);
                        vjxProps.put(prop, EJXTypes.newFloat(f));
                        solved = true;
                        break;
                    case intProp:
                        int i = Integer.parseInt(s);
                        vjxProps.put(prop, i);
                        solved = true;
                        break;
                        //                    case lifecycleProp:
                        //                        Lifecycle lifeCycle = Lifecycle.get(s);
                        //                        vjxProps.put(prop, lifeCycle);
                        //                        solved = true;
                        //                        break;
                    case physicsType:
                        switch (s) {
                            case "kinematic":
                                vjxProps.put(prop, BodyType.KinematicBody);
                                break;
                            case "dynamic":
                                vjxProps.put(prop, BodyType.DynamicBody);
                                break;
                            default:
                                vjxProps.put(prop, BodyType.StaticBody);
                                break;
                        }
                        solved = true;
                        break;
                    case stringProp:
                        vjxProps.put(prop, s);
                        solved = true;
                        break;
                        //                    case vector2Prop:
                        //                        Vector2 v = new Vector2();
                        //                        String[] split = s.split(VJXString.SEP_PROP);
                        //                        if (s.length() != 2) break;
                        //                        v.x = Float.parseFloat(split[0]);
                        //                        v.y = Float.parseFloat(split[1]);
                        //                        vjxProps.put(prop, v);
                        //                        solved = true;
                        //                        break;
                    case colorProp:
                        Color c = Color.valueOf(s);
                        vjxProps.put(prop, c);
                        solved = true;
                        break;
                    case colliderDef:
                        break;
                    case camModeType:
                        vjxProps.put(prop, CamMode.get(s));
                        solved = true;
                        break;
                    default:
                        break;
                }
            }

            if (o instanceof Integer
                            && prop.getDefault().getClass() == Float.class) {
                int i = props.get(prop.getName(), 0, Integer.class);
                vjxProps.put(prop, (float) i);
                solved = true;
            }

            if (o instanceof Float
                            && prop.getDefault().getClass() == Integer.class) {
                float f = props.get(prop.getName(), 0f, Float.class);
                vjxProps.put(prop, (int) f);
                solved = true;
            }

            if (o instanceof Float
                            && prop.getDefault().getClass() == EJXFloat.class) {
                float f = props.get(prop.getName(), 0f, Float.class);
                vjxProps.put(prop, EJXTypes.newFloat(f));
                solved = true;
            }

            if (o instanceof Color
                            && prop.getDefault().getClass() == Color.class) {
                Color c = props.get(prop.getName(), null, Color.class);
                vjxProps.put(prop, c);
                solved = true;
            }

            if (solved) {
                return;
            }

            VJXLogger.log(LogCategory.ERROR, "'" + prop.getName()
            + "' conversion failed: Unsuspected value type."
            + " Value: '" + o.getClass() + "' - Expected: '"
            + prop.getDefault().getClass()
            + "'. Property overwritten.");
        }
        vjxProps.put(prop, o);
    }

    public static MapProperties xmlToProperties(Element tile) {
        return xmlToProperties(tile, VJXString.STR_EMPTY);
    }

    public static MapProperties xmlToProperties(Element tile,
                    MapProperties properties) {
        return xmlToProperties(tile, VJXString.STR_EMPTY, properties);
    }

    public static MapProperties xmlToProperties(Element tile, String prefix) {
        return xmlToProperties(tile, prefix, new MapProperties());
    }

    public static MapProperties xmlToProperties(Element tile, String prefix,
                    MapProperties properties) {
        if (properties == null) properties = new MapProperties();

        if (prefix == null) prefix = VJXString.STR_EMPTY;
        if (!prefix.isEmpty()) prefix += VJXString.SEP_DDOT;

        String[] pathArray;
        String src, defName;

        Element image = tile.getChildByName("image");
        if (image != null) {
            src = image.getAttribute("source");

            pathArray = getSourceSplit(src, VJXString.FILE_PNG);

            src = pathArray[0];
            defName = pathArray[2];

            properties.put(prefix + VJXString.name, defName);
            properties.put(VJXString.srcWidth,
                            image.getIntAttribute(VJXString.width, 0));
            properties.put(VJXString.srcHeight,
                            image.getIntAttribute(VJXString.height, 0));
            properties.put("icon_src", src);

            prefix = defName + VJXString.SEP_DDOT;
        }

        Element propertiesElem = tile.getChildByName("properties");
        if (propertiesElem == null) return properties;
        if (propertiesElem.getName().equals("properties")) {
            for (Element property : propertiesElem
                            .getChildrenByName("property")) {
                String name = property.getAttribute("name");
                if (!name.contains(VJXString.SEP_DDOT)) name = prefix + name;
                //                else {
                //                    pathArray = name.split(VJXString.SEP_PROP);
                //                    name = pathArray[0] + VJXString.SEP_PROP + defName
                //                                    + VJXString.SEP_ATTR + pathArray[1];
                //                }
                String strValue = property.getAttribute("value", null);
                //                VJXLogger.log(name);
                //                VJXLogger.log("?" + strValue);
                if (strValue == null) strValue = property.getText();
                //                VJXLogger.log("name:" + name);
                //                VJXLogger.log(" strValue:" + strValue);
                if (name.isEmpty()) continue;
                switch (property.getAttribute("type", VJXString.STR_EMPTY)) {
                    case "bool":
                        properties.put(name, Boolean.parseBoolean(strValue));
                        break;
                    case "color":
                        float a = Integer.parseInt(strValue.substring(1, 3), 16)
                        / 255f;
                        float r = Integer.parseInt(strValue.substring(3, 5), 16)
                                        / 255f;
                        float g = Integer.parseInt(strValue.substring(5, 7), 16)
                                        / 255f;
                        float b = Integer.parseInt(strValue.substring(7, 9), 16)
                                        / 255f;
                        properties.put(name, new Color(r, g, b, a));
                        break;
                    case "float":
                        properties.put(name, Float.parseFloat(strValue));
                        break;
                    case "int":
                        properties.put(name, Integer.parseInt(strValue));
                        break;
                    case "file":
                        // clear directory to internal parent
                        strValue = strValue.replace(VJXString.PATH_DIR_UP,
                                        VJXString.STR_EMPTY);
                        properties.put(name, Gdx.files.internal(strValue));
                        break;
                    default:
                        properties.put(name, strValue);
                }
            }
        }
        return properties;
    }

    /**
     * Extracts various information from a source string and splits them into
     * an array.
     *
     * @param source - Input source string to be converted.
     * @param fileExtension - The file extension of the target file.
     * @return
     * Returns an array containing the internal absolute path of
     * the source folder, the file name without extension and the definition name.
     * That is the file names first part. The other parts are attributes.
     *
     * Return array:
     * sourceSplit[0] = path to source folder
     * sourceSplit[1] = full tile name with attributes without extension
     * sourceSplit[2] = definition name
     * sourceSplit[3] = attributes
     */
    public static String[] getSourceSplit(String source, String fileExtension) {
        String[] split;
        String[] sourceSplit = new String[4];
        if (source.isEmpty()) return null;

        // remove file extension and relativity from tileset file
        source = source.replace(fileExtension, VJXString.STR_EMPTY);
        source = source.replace(VJXString.PATH_DIR_UP, VJXString.STR_EMPTY);

        // split source by "/" to get the path parts
        split = source.split(VJXString.SEP_SLASH);

        // get file name without extension
        sourceSplit[1] = split[split.length - 1];

        // remove file name from source
        sourceSplit[0] = source.replace(sourceSplit[1], VJXString.STR_EMPTY);
        sourceSplit[0] = sourceSplit[0].replace("rawAssets/", VJXString.STR_EMPTY);

        // remove file attributes to get clean tile name
        sourceSplit[1] = sourceSplit[1].replace("ico_", VJXString.STR_EMPTY);
        sourceSplit[1] = sourceSplit[1].replace("_icon", VJXString.STR_EMPTY);

        sourceSplit[2] = sourceSplit[1];

        split = sourceSplit[1].split(VJXString.SEP_USCORE);
        if (split.length > 1) sourceSplit[3] = split[1];
        else sourceSplit[3] = VJXString.STR_EMPTY;

        if (!sourceSplit[3].isEmpty()) {
            // split attributes from tile number
            String[] attSplit = sourceSplit[3].split(VJXString.SEP_NR);
            if (attSplit.length > 1) {
                sourceSplit[3] = attSplit[0];
            }
        }

        return sourceSplit;
    }

    public static void addVoice(String defName, String key) {
        //        VJXLogger.log("addVoice:" + defName + ", " + key);
        List<String> list = voices.get(defName);
        if (voices.containsKey(defName)) list = voices.get(defName);
        else {
            list = new ArrayList<>();
            voices.put(defName, list);
        }
        if (!list.contains(key)) list.add(key);
    }

    public static List<String> getVoices(String defName) {
        return voices.get(defName);
    }

}