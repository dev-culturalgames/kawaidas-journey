package de.culturalgames.kwjourney.actions.game;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.KawaidaHUD;
import de.culturalgames.kwjourney.KawaidaHUD.UIElement;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.actions.GameAction;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class WinProgressAction extends GameAction {

    private static final String ANIMATE_PROGRESS_ACTION = "animate_progress_action";
    private static final String EXEC_MSG_COLLECTED = " (collected: ";
    private static final String EXEC_MSG_MULTIPLIER = ", multiplier: ";
    private static final String EXEC_MSG_INTERVAL = ", interval: ";

    private EJXLabel collectedSunsLabel;
    private EJXLabel totalSunsLabel;

    private int collectedCoins;
    private float coinMultiplier;
    private float countInterval;

    private int counter;
    private int playerCoins;
    private float timer;
    private float x;
    private float y;

    private int maxImages = 10;
    private List<Image> itemImages;
    private int curImgInd;
    private Vector2 tmpVec;

    public WinProgressAction() {
        name = ANIMATE_PROGRESS_ACTION;
        itemImages = new ArrayList<>();
        tmpVec = new Vector2();
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_COLLECTED + collectedCoins;
        execMsg += EXEC_MSG_MULTIPLIER + coinMultiplier;
        execMsg += EXEC_MSG_INTERVAL + countInterval;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        timer += delta;
        if (timer >= countInterval) {
            counter--;

            collectedSunsLabel.setText(Integer.toString(counter));
            moveSun();
            timer -= countInterval;
        }
        return counter == 0;
    }

    @Override
    public void reset() {
        super.reset();
        timer = 0;
        counter = collectedCoins;
    }

    public void setCountInterval(float interval) {
        countInterval = interval;
        maxImages = (int) (.5f / interval) + 5;
    }

    public void setCoinMultiplier(float coinMultiplier) {
        this.coinMultiplier = coinMultiplier;
    }

    public void setCoins(int collectedCoins, int playerCoins) {
        this.collectedCoins = collectedCoins;
        counter = collectedCoins;
        this.playerCoins = playerCoins;
    }

    public void setSrcActor(Actor srcActor) {
        tmpVec.set(0, 0);
        srcActor.localToStageCoordinates(tmpVec);

        x = tmpVec.x;
        y = tmpVec.y;
    }

    public void setLabels(EJXLabel collectedSunsLabel, EJXLabel totalSunsLabel) {
        this.collectedSunsLabel = collectedSunsLabel;
        this.totalSunsLabel = totalSunsLabel;
    }

    public static final String flyingSun = "flyingSun";

    public void moveSun() {
        Image itemImg = null;

        if (itemImages.size() < maxImages) {
            itemImg = new Image();
            itemImg.setName(flyingSun + itemImages.size());
            itemImg.setTouchable(Touchable.disabled);
            itemImages.add(itemImg);
        } else {
            itemImg = itemImages.get(curImgInd);
            itemImg.clearActions();
            curImgInd++;
            curImgInd %= maxImages;
        }

        MenuStage menu = game.getScreen().getMenu();
        KawaidaHUD hud = menu.getUI(KawaidaHUD.class);
        itemImg.setDrawable(hud.getSkin().getDrawable(KJAssets.ICON_SUN));
        itemImg.pack();

        Actor suns = hud.getUIElement(UIElement.SUN);
        tmpVec.set(0, 0);
        suns.localToStageCoordinates(tmpVec);

        itemImg.setPosition(x, y);
        itemImg.addAction(Actions.sequence(
                        Actions.moveTo(tmpVec.x, tmpVec.y, .5f),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                playerCoins++;
                                totalSunsLabel.setText(Integer.toString(playerCoins));
                            }
                        }), Actions.removeActor(),
                        EJXActions.updateUI(game)));
        menu.getPopup(POPUP.WIN.id).addActor(itemImg);
    }
}