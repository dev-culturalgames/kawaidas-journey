package de.culturalgames.kwjourney.actions.game;

import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.entity.logics.DialogLogic.Dialog;
import de.venjinx.ejx.actions.GameAction;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class ShowStoryAction extends GameAction {

    private static final String SHOW_STORY_ACTION = "show_story_action";
    private static final String EXEC_MSG_STORY = " (story: ";

    private Dialog story;

    public ShowStoryAction() {
        name = SHOW_STORY_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_STORY + story.getName();
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        ((KJMenu) menu).showDialog(story);
        return true;
    }

    public void setStory(Dialog story) {
        this.story = story;
    }
}