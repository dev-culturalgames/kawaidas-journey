package de.culturalgames.kwjourney.actions.game;

import com.badlogic.gdx.utils.Array;

import de.culturalgames.kwjourney.entity.logics.InventoryLogic;
import de.culturalgames.kwjourney.entity.logics.ItemLogic;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SuckCoinsAction extends ObjectAction {

    private static final String SUN_SUCK__ACTION = "sun_suck_action";
    private static final String EXEC_MSG_RANGE = " (range: ";
    private static final String EXEC_MSG_DURATION = ", duration: ";

    private InventoryLogic inventory;
    private float range = 0;
    private float duration = 0;

    private float timer = 0;

    public SuckCoinsAction() {
        name = SUN_SUCK__ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_RANGE + range;
        execMsg += EXEC_MSG_DURATION + duration;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (!executor.isVisible()) {
            suckCoins(false);
            return false;
        }

        suckCoins(true);

        timer += delta;
        if (timer > duration || !inventory.isSucking()) {
            suckCoins(false);
            return true;
        }
        return false;
    }

    @Override
    public void reset() {
        super.reset();
        timer = 0;
    }

    @Override
    public void setEntity(EJXEntity entity) {
        executor = entity;

        if (executor.hasLogic(KJLogicType.INVENTORY))
            inventory = (InventoryLogic) executor.getLogic(KJLogicType.INVENTORY);
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public float getTimeLeft() {
        return duration - timer;
    }

    public void setRange(float range) {
        this.range = range;
    }

    private void suckCoins(boolean activate) {
        Array<EJXEntity> sunArrray = new Array<>();
        LevelStage level = inventory.getOwner().getListener().getLevel();
        level.getEntitiesByContainDefName("sun0", sunArrray);

        for (EJXEntity e : sunArrray) {
            ItemLogic itmLogic = (ItemLogic) e.getLogic(KJLogicType.ITEM);
            MoveLogic moveLogic = (MoveLogic) e.getLogic(VJXLogicType.MOVE);

            if (!activate) {
                moveLogic.stop();
            } else {
                if (e.getWorldCenter().dst(inventory.getOwner()
                                .getWorldCenter()) <= range
                                && !itmLogic.isGettingSucked()) {
                    itmLogic.getSucked(true);
                    moveLogic.setDestination(e.getTargetEntity().getWorldCenter());
                    moveLogic.startMove();
                }
            }
        }
        if (!activate && executor.isVisible())
            inventory.suck(false);
    }
}