package de.culturalgames.kwjourney.actions.game;

import de.culturalgames.kwjourney.KJMenu;
import de.venjinx.ejx.actions.GameAction;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class ShowPopupAction extends GameAction {

    private static final String SHOW_MENU_ACTION = "show_menu_action";
    private static final String EXEC_MSG_MENU = " (menu: ";
    private static final String EXEC_MSG_BLOCK_CONTINUE = ", block continue: ";

    private int dialogueIndex = 0;
    private boolean blockContinue = false;
    private boolean bounceIn = true;

    public ShowPopupAction() {
        name = SHOW_MENU_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_MENU + dialogueIndex;
        execMsg += EXEC_MSG_BLOCK_CONTINUE + blockContinue;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        ((KJMenu) menu).setContinueBlock(blockContinue);
        menu.showPopup(dialogueIndex, bounceIn);
        return true;
    }

    public void setDialogueIndex(int dialogue) {
        dialogueIndex = dialogue;
    }

    public void setContinueBlock(boolean cont) {
        blockContinue = cont;
    }

    public void setBounceIn(boolean bounceIn) {
        this.bounceIn = bounceIn;
    }
}