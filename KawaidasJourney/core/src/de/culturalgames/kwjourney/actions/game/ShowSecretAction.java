package de.culturalgames.kwjourney.actions.game;

import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.venjinx.ejx.actions.GameAction;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class ShowSecretAction extends GameAction {

    private static final String SHOW_SECRET_ACTION = "show_secret_action";
    private static final String EXEC_MSG_SECRET = " (secret: ";

    private Secret secret;
    private EJXEntity triggerObject;

    public ShowSecretAction() {
        name = SHOW_SECRET_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_SECRET + secret.getName();
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        ((KJMenu) menu).showSecret(secret,
                        triggerObject);
        return true;
    }

    public void setSecret(Secret s) {
        secret = s;
    }

    public void setTriggerObject(EJXEntity triggerObject) {
        this.triggerObject = triggerObject;
    }
}