package de.culturalgames.kwjourney.actions.game;

import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.entity.logics.HintLogic.Hint;
import de.venjinx.ejx.actions.GameAction;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class ShowHintAction extends GameAction {

    private static final String SHOW_HINT_ACTION = "show_hint_action";
    private static final String EXEC_MSG_HINT = " (hint: ";

    private Hint hint;

    public ShowHintAction() {
        name = SHOW_HINT_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_HINT + hint.getText();
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        ((KJMenu) menu).showHint(hint);
        return true;
    }

    public void setHint(Hint h) {
        hint = h;
    }
}