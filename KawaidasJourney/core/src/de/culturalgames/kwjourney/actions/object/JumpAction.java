package de.culturalgames.kwjourney.actions.object;

import de.culturalgames.kwjourney.entity.logics.JumpLogic;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class JumpAction extends ObjectAction {

    private static final String JUMP_ACTION = "jump_action";
    private static final String EXEC_MSG_HEIGHT = " (target: ";
    private static final String EXEC_MSG_FORCE = ", force: ";

    private JumpLogic jumpLogic;
    private float height;
    private boolean force;

    public JumpAction() {
        name = JUMP_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_HEIGHT + height;
        execMsg += EXEC_MSG_FORCE + force;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        if (executor != null && executor.hasLogic(KJLogicType.JUMP))
            jumpLogic = (JumpLogic) executor.getLogic(KJLogicType.JUMP);
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (jumpLogic == null) return true;
        jumpLogic.jump(height, force);
        return true;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void setForce(boolean force) {
        this.force = force;
    }
}