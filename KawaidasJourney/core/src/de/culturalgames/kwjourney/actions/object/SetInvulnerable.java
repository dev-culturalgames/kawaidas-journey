package de.culturalgames.kwjourney.actions.object;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.LifeLogic;
import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class SetInvulnerable extends ObjectAction {

    private static final String SET_INVULNERABLE_ACTION = "set_invulnerable_action";
    private static final String EXEC_MSG_INVULNERABLE = " (invulnerable: ";
    private static final String EXEC_MSG_DURATION = ", duration: ";

    private LifeLogic lifeLogic;
    private boolean invulnerable = false;
    private float duration = 0;

    public SetInvulnerable() {
        name = SET_INVULNERABLE_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_INVULNERABLE + invulnerable;
        execMsg = EXEC_MSG_DURATION + duration;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        if (executor != null && executor.hasLogic(KJLogicType.LIFE))
            lifeLogic = (LifeLogic) executor.getLogic(KJLogicType.LIFE);
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (lifeLogic == null) return true;
        lifeLogic.setInvulnerable(invulnerable, duration);
        return true;
    }

    public void setInvulnerable(boolean invulnerable) {
        this.invulnerable = invulnerable;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }
}