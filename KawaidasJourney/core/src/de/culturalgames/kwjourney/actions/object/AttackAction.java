package de.culturalgames.kwjourney.actions.object;

import de.culturalgames.kwjourney.entity.logics.AttackLogic;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.actions.ObjectAction;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class AttackAction extends ObjectAction {

    private static final String ATTACK_ACTION = "attack_action";
    private static final String EXEC_MSG_TARGET = " (target: ";
    private static final String EXEC_MSG_MOVE = ", move: ";
    private static final String EXEC_MSG_RANGED = ", ranged: ";
    private static final String EXEC_MSG_DURATION = ", duration: ";

    private AttackLogic attackLogic;
    private EJXEntity targetEntity;
    private boolean move = false;
    private boolean ranged = false;
    private boolean updateActivity = true;
    private float duration = 0;

    public AttackAction() {
        name = ATTACK_ACTION;
    }

    @Override
    protected void init() {
        execMsg = EXEC_MSG_TARGET + targetEntity;
        execMsg += EXEC_MSG_MOVE + move;
        execMsg += EXEC_MSG_RANGED + ranged;
        execMsg += EXEC_MSG_DURATION + duration;
        execMsg += VJXString.SEP_BRACKET_CLOSE;
        if (executor != null && executor.hasLogic(KJLogicType.ATTACK)) {
            attackLogic = (AttackLogic) executor.getLogic(KJLogicType.ATTACK);
        }
        super.init();
    }

    @Override
    public boolean act(float delta) {
        super.act(delta);
        if (attackLogic == null) return true;
        if (duration == 0) {

            if (!ranged) {
                if (targetEntity == null)
                    attackLogic.attack(move, updateActivity);
                else attackLogic.attack(targetEntity, move, updateActivity);
            } else {
                if (targetEntity == null)
                    duration = attackLogic.throwCocos(updateActivity);
                else duration = attackLogic.throwCocos(targetEntity, updateActivity);
            }
            return true;
        } else {
            duration -= delta;
            if (duration < 0) return true;
        }
        return false;
    }

    @Override
    public void reset() {
        super.reset();
        duration = 0;
    }

    public void setMove(boolean move) {
        this.move = move;
    }

    public void setRanged(boolean ranged) {
        this.ranged = ranged;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public void setAttackTarget(EJXEntity targetEntity) {
        this.targetEntity = targetEntity;
    }

    public void setUpdateActivity(boolean updateActivity) {
        this.updateActivity = updateActivity;
    }
}
