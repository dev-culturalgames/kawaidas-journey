package de.culturalgames.kwjourney.actions;

import com.badlogic.gdx.scenes.scene2d.Actor;

import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.actions.game.ShowHintAction;
import de.culturalgames.kwjourney.actions.game.ShowPopupAction;
import de.culturalgames.kwjourney.actions.game.ShowSecretAction;
import de.culturalgames.kwjourney.actions.game.ShowStoryAction;
import de.culturalgames.kwjourney.actions.game.SuckCoinsAction;
import de.culturalgames.kwjourney.actions.game.WinProgressAction;
import de.culturalgames.kwjourney.actions.object.AttackAction;
import de.culturalgames.kwjourney.actions.object.JumpAction;
import de.culturalgames.kwjourney.actions.object.SetInvulnerable;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.culturalgames.kwjourney.entity.logics.DialogLogic.Dialog;
import de.culturalgames.kwjourney.entity.logics.HintLogic.Hint;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class KJActions extends EJXActions {

    /*
     * Game actions
     */

    public static ShowPopupAction showPopup(POPUP popup, EJXGame game) {
        return showPopup(popup, game, false, true);
    }

    public static ShowPopupAction showPopup(POPUP popup, EJXGame game,
                    boolean cont, boolean bounceIn) {
        ShowPopupAction popupAction = action(ShowPopupAction.class);
        popupAction.setGame(game);
        popupAction.setDialogueIndex(popup.id);
        popupAction.setContinueBlock(cont);
        popupAction.setBounceIn(bounceIn);
        return popupAction;
    }

    public static ShowStoryAction showStory(Dialog dialog, EJXGame game) {
        ShowStoryAction dialogAction = action(ShowStoryAction.class);
        dialogAction.setStory(dialog);
        dialogAction.setGame(game);
        return dialogAction;
    }

    public static ShowHintAction showHint(Hint hint, EJXGame game) {
        ShowHintAction hintAction = action(ShowHintAction.class);
        hintAction.setHint(hint);
        hintAction.setGame(game);
        return hintAction;
    }

    public static ShowSecretAction showSecret(Secret secret,
                    EJXEntity triggerObject, EJXGame game) {
        ShowSecretAction secretAction = action(ShowSecretAction.class);
        secretAction.setSecret(secret);
        secretAction.setTriggerObject(triggerObject);
        secretAction.setGame(game);
        return secretAction;
    }

    public static SuckCoinsAction suckCoins(float duration, float range,
                    EJXEntity entity) {
        SuckCoinsAction sunSuckAction = action(SuckCoinsAction.class);
        sunSuckAction.setDuration(duration);
        sunSuckAction.setRange(range);
        sunSuckAction.setEntity(entity);
        return sunSuckAction;
    }

    public static WinProgressAction winProgressAction(EJXGame game, Actor srcActor,
                    EJXLabel collectedSuns, EJXLabel totalSunsLable,
                    int collectedCoins, int playerCoins, float counterInterval,
                    float coinMultiplier) {
        WinProgressAction winProgressAction = action(WinProgressAction.class);
        winProgressAction.setGame(game);
        winProgressAction.setCoins(collectedCoins, playerCoins);
        winProgressAction.setSrcActor(srcActor);
        winProgressAction.setLabels(collectedSuns, totalSunsLable);
        winProgressAction.setCountInterval(counterInterval);
        winProgressAction.setCoinMultiplier(coinMultiplier);
        return winProgressAction;
    }

    /*
     * Object actions
     */

    public static AttackAction attack(EJXEntity entity,
                    boolean updateActivity) {
        return attack(entity.getTargetEntity(), entity, updateActivity);
    }

    public static AttackAction attack(EJXEntity attackedEntity,
                    EJXEntity entity, boolean updateActivity) {
        AttackAction attackAction = action(AttackAction.class);
        attackAction.setEntity(entity);
        attackAction.setMove(false);
        attackAction.setRanged(false);
        attackAction.setAttackTarget(attackedEntity);
        attackAction.setUpdateActivity(updateActivity);
        return attackAction;
    }

    public static AttackAction attackMove(EJXEntity entity) {
        return attackMove(entity.getTargetEntity(), entity);
    }

    public static AttackAction attackMove(EJXEntity attackedEntity,
                    EJXEntity entity) {
        AttackAction attackAction = action(AttackAction.class);
        attackAction.setEntity(entity);
        attackAction.setMove(true);
        attackAction.setRanged(false);
        attackAction.setAttackTarget(attackedEntity);
        return attackAction;
    }

    public static AttackAction attackThrow(EJXEntity entity,
                    boolean updateActivity) {
        return attackThrow(entity.getTargetEntity(), entity, updateActivity);
    }

    public static AttackAction attackThrow(EJXEntity attackedEntity,
                    EJXEntity entity, boolean updateActivity) {
        AttackAction attackAction = action(AttackAction.class);
        attackAction.setEntity(entity);
        attackAction.setMove(false);
        attackAction.setRanged(true);
        attackAction.setAttackTarget(attackedEntity);
        attackAction.setUpdateActivity(updateActivity);
        return attackAction;
    }

    //    public static Action fall(VJXEntity entity) {
    //        FallAction fallAction = action(FallAction.class);
    //        fallAction.setEntity(entity);
    //        return fallAction;
    //    }

    public static JumpAction jump(EJXEntity entity, float height,
                    boolean force) {
        JumpAction jumpAction = action(JumpAction.class);
        jumpAction.setEntity(entity);
        jumpAction.setHeight(height);
        jumpAction.setForce(force);
        return jumpAction;
    }

    public static SetInvulnerable setInvulnerable(EJXEntity entity,
                    boolean invulnerable) {
        return setInvulnerable(entity, invulnerable, 0);
    }

    public static SetInvulnerable setInvulnerable(EJXEntity entity,
                    boolean invulnerable, float duration) {
        SetInvulnerable invulnerableAction = action(SetInvulnerable.class);
        invulnerableAction.setInvulnerable(invulnerable);
        invulnerableAction.setDuration(duration);
        invulnerableAction.setEntity(entity);
        return invulnerableAction;
    }

}