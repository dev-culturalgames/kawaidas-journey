
package de.culturalgames.kwjourney;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import de.culturalgames.kwjourney.KJGame.GameStats;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.KawaidaHUD.UIElement;
import de.culturalgames.kwjourney.api.AdMobController;
import de.culturalgames.kwjourney.cmds.KJCommands;
import de.culturalgames.kwjourney.content.Secrets;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.culturalgames.kwjourney.entity.logics.ContextLogic;
import de.culturalgames.kwjourney.entity.logics.DialogLogic;
import de.culturalgames.kwjourney.entity.logics.HintLogic;
import de.culturalgames.kwjourney.entity.logics.InventoryLogic;
import de.culturalgames.kwjourney.entity.logics.ItemLogic;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.LifeLogic;
import de.culturalgames.kwjourney.save.KJCloudSave;
import de.culturalgames.kwjourney.save.KJPlayerStats.PlayerStats;
import de.culturalgames.kwjourney.ui.menus.VideoMenu;
import de.culturalgames.kwjourney.ui.popups.StoryPopup;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.AudioControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EntityCallbacks.VJXCallback;
import de.venjinx.ejx.entity.logics.CallbackSpawnLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXTypes.WinCondition;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.ejx.video.VideoSequence;
import de.venjinx.vjx.ui.IngameHUD.Transition;
import de.venjinx.vjx.ui.debug.LevelStatsWindow.LevelStats;

public class KJLevel extends LevelStage {

    private final static Color EVO_BASE_COLOR = new Color(.6f, .6f, .6f, 1f);
    private final static float EVO_PROGR_0 = 0f;
    private final static float EVO_PROGR_1 = .2f;
    private final static float EVO_PROGR_2 = .4f;
    private final static float EVO_PROGR_3 = .6f;
    private final static float EVO_PROGR_4 = .8f;

    private static final String HIDE_UI = "hideUI";
    private static final String IS_END_LEVEL = "isEndLevel";
    private static final String IS_BOSS_LEVEL = "isBossLevel";
    private static final String WIN_CONDITION = "condition:win";

    private Vector2 tmpVec = new Vector2();
    private Array<EJXEntity> tmpEntities = new Array<>();

    private KJMenu kjMenu;
    private KJPlayerControl playerControl;
    private KawaidaHUD hud;

    private Array<String> hideUINames;

    private AudioControl ac;
    private boolean playMusicOnStart;

    private boolean autoRunX;
    private boolean autoRunY;

    private float plantRange;
    private boolean groundIsDeadly;

    private WinCondition winCondition;
    protected Color evoColor;
    private float evoMod;
    private boolean isEndLevel;
    private boolean isBossLevel;
    private int totalSuns;
    private boolean bonusFound;

    private VideoSequence intro;
    private Runnable onIntroClose = new Runnable() {

        @Override
        public void run() {
            start();
        }
    };

    public KJLevel(EJXGame game, KJMenu menu) {
        super(game);
        setName("kawaida_level_stage");
        kjMenu = menu;
        ac = game.sfx;
        evoColor = new Color(1f, 1f, 1f, 1f);

        hideUINames = new Array<>();
    }

    @Override
    protected void loadProperties(MapProperties props) {
        super.loadProperties(props);

        autoRunX = props.get("autoRunX", false, Boolean.class);
        autoRunY = props.get("autoRunY", false, Boolean.class);
        plantRange = props.get("plantRange", 500f, Float.class);
        game.factory.getEntityDef("palmSeedling0").getCustomProperties().put("range",
                        plantRange);
        groundIsDeadly = props.get("deadlyGround", false, Boolean.class);
        String wcName = props.get(WIN_CONDITION, VJXString.STR_EMPTY, String.class);
        if (wcName != null && !wcName.isEmpty())
            winCondition = WinCondition.get(wcName);
        else winCondition = null;

        hud = kjMenu.getUI(KawaidaHUD.class);
        playerControl = (KJPlayerControl) game.gameControl.getPlayerInput();

        hideUINames.clear();
        String elementsToHide = props.get(HIDE_UI, VJXString.STR_EMPTY,
                        String.class);
        if (elementsToHide != null) {
            String[] elements = elementsToHide.split(VJXString.SEP_LIST);
            for (String uiElement : elements)
                hideUINames.add(uiElement);
        }

        isEndLevel = props.get(IS_END_LEVEL, false, Boolean.class);
        isBossLevel = props.get(IS_BOSS_LEVEL, false, Boolean.class);
    }

    @Override
    public void restart() {
        long time = System.currentTimeMillis();
        VJXLogger.logIncr(LogCategory.LEVEL | LogCategory.FLOW
                        | LogCategory.INIT,
                        "User level '" + getName() + "' restarting.");
        game.gameControl.clearActions();

        evoColor.set(1f, 1f, 1f, 1f);
        game.getScreen().getRenderer().setBaseColor(evoColor);

        ac.stopAllSounds();
        kjMenu.stopMusic();

        stageFrameNr = 0;

        evoMod = 0;
        totalSuns = 0;
        resetProgress();

        HintLogic.clearHints();
        DialogLogic.clearDialogs();
        clearWorld(false);
        b2dWorld.clearForces();
        kjMenu.getRoot().clearActions();
        getRoot().clearActions();
        Waypoints.removeWaypoint(VJXString.player_start);

        hud.enableAllElements();
        hud.toggleUIElement(UIElement.JOYSTICK.name, true);
        for (String name : hideUINames) {
            hud.enableUIElement(name, false);
            hud.toggleUIElement(name, false);
        }

        playerControl.setAutoRun(autoRunX, autoRunY);
        setCamFocusObject(null, true);

        createCollision();
        createObjects();

        kjMenu.lvlLoaded();

        spawnAllNow();
        addEntity(playerEntity, playerEntity.getLayer());

        ((ContextLogic) playerEntity.getLogic(KJLogicType.CONTEXT))
        .setContext(ActionContext.NONE, null);

        ((LifeLogic) playerEntity.getLogic(KJLogicType.LIFE)).setHitPoints(1);
        ((LifeLogic) playerEntity.getLogic(KJLogicType.LIFE))
        .setInvulnerable(false);

        InventoryLogic inventory = (InventoryLogic) playerEntity
                        .getLogic(KJLogicType.INVENTORY);
        inventory.suck(false);
        playerEntity.getPhysics().detach();

        Waypoint wp = Waypoints.getWaypoint(VJXString.player_start);
        if (wp != null)
            playerEntity.setPosition(wp.getCenter(tmpVec).sub(
                            playerEntity.getScaledWidth() / 2f,
                            playerEntity.getScaledHeight() / 2f), true, true);

        bonusFound = false;
        if (isBossLevel()) {
            setBonusFound(true);
            playerEntity.setCallBackCommands(VJXCallback.onDamage,
                            KJCommands.getBonusCommand(game, false));
        }

        KJPlayer player = (KJPlayer) game.getPlayer();
        if (!player.getSkin().isEmpty()) {
            playerEntity.setSkin(player.getSkin());
        }

        player.clearCurrentLevelProgress();
        inventory.setAmount(ItemCategory.COIN, 0);

        inventory.setAmount(ItemCategory.POTION, 1);
        inventory.setAmountMax(ItemCategory.POTION,
                        player.getAmount(ItemCategory.POTION, true));

        inventory.setAmount(ItemCategory.AMMO, 0);
        inventory.setAmountMax(ItemCategory.AMMO,
                        player.getAmount(ItemCategory.AMMO, true));

        int toolMax = map.getProperties().get(KJPlayer.PL_TOOLS_MAX, -1, Integer.class);
        if (toolMax > -1)
            inventory.setAmountMax(ItemCategory.TOOL, toolMax);

        int tools = map.getProperties().get(KJPlayer.PL_TOOLS_START, -1, Integer.class);
        if (tools > -1)
            inventory.setAmount(ItemCategory.TOOL, tools);

        player.setAmount(ItemCategory.CONTINUE,
                        game.getSave().getInt(PlayerStats.CONTINUES.name, 0));

        tmpEntities.clear();

        if (winCondition != null)
            switch (winCondition) {
                case PROGR_GROW:
                    EJXEntity e = game.factory.createEntity("amb_forest_plant");
                    if (e == null) break;
                    addEntity(e);
                    spawnEntity(e);
                    updateEvoModificator();
                    break;
                    //                case PROGR_NOTES:
                    //                    maxProgress = getEntityCountByLogic(VJXLogic.NOTE);
                    //                    //                    VJXLogger.log("maxProgress:" + maxProgress);
                    //                    //                    updateEvoModificator();
                    //                    break;
                default:
            }

        playerControl.levelLoaded(game);
        playerControl.setIngameControl(true);
        kjMenu.toggleOverlay(Transition.VIDEO_SEQUENCE, false);

        kjMenu.updateHUD();

        game.getScreen().getDebugStage().getDebugUI().setLevel(this);

        AdMobController adControl = ((KJGame) game).getAdControl();
        if (adControl != null) {
            adControl.loadAd(KJString.AD_double_suns);
        }

        ((EJXCamera) getCamera()).reset();
        playMusicOnStart = map.getProperties().get("music:start", true,
                        boolean.class);

        StoryPopup dia = (StoryPopup) game.getScreen().getMenu()
                        .getPopup(POPUP.STORY.id);
        dia.init();

        LevelStats.tries++;
        game.getScreen().getDebugStage().getDebugUI().updateStats(false);

        float tmpVol = game.sfx.getMasterVolume();
        game.sfx.setMasterVolume(0);
        setActive(true);
        act(0);
        setActive(false);
        game.sfx.setMasterVolume(tmpVol);
        VJXLogger.logDecr(LogCategory.LEVEL | LogCategory.FLOW
                        | LogCategory.INIT,
                        "User level '" + getName() + "' restarted. Took "
                                        + (System.currentTimeMillis() - time)
                                        + "ms.");
    }

    @Override
    public void start() {
        boolean introPlayed = game.getSave()
                        .getBool(GameStats.INTRO_PLAYED.name, false);
        if (intro != null && !introPlayed) {
            VideoMenu videoMenu = (VideoMenu) kjMenu.getMenu(MENU.VIDEO.id);
            videoMenu.startVideo(intro, KJAssets.MUSIC_INTRO, onIntroClose);

            kjMenu.switchMenu(MENU.VIDEO.id, false);

            game.getSave().putBool(GameStats.INTRO_PLAYED.name, true);
            setRunBackground(true);
        } else {
            kjMenu.stopMusic();
            if (playMusicOnStart)
                playMusic();

            kjMenu.switchMenu(MENU.GAME.id, .15f, .25f, false, Transition.BLACKSCREEN);
            kjMenu.getUI().toggleHUD(true);

            setActive(true);
            setRunBackground(false);
        }
    }

    public void continueAfterDeath(boolean rewarded) {
        playMusic();
        buyBack(rewarded);
        hud.restoreHUD();
        kjMenu.hidePopup(false);
        kjMenu.getScreen().getLevel().setActive(true);
    }

    private void buyBack(boolean rewarded) {
        KJPlayer player = (KJPlayer) game.getPlayer();
        if (!rewarded) {
            player.incr(ItemCategory.CONTINUE, -1);
            ((KJCloudSave) game.getSave()).saveInventory(player);
            game.getSave().write();
        }

        LifeLogic lLogic = (LifeLogic) playerEntity.getLogic(KJLogicType.LIFE);
        lLogic.revive(camEntity, player.getSkin());

        kjMenu.updateHUD();
        playerControl.levelLoaded(game);
        LevelStats.continues++;

        kjMenu.getScreen().getDebugStage().getDebugUI().updateStats(false);
    }

    @Override
    protected void initResources() {
        TiledMapTileSet howSet = map.getTileSets().getTileSet("how");
        if (howSet != null)
            resources.put(KJAssets.ATLAS_GLOBAL_HOW,
                            new AssetDescriptor<>(KJAssets.PATH_ATLAS_GLOBAL_HOW,
                                            TextureAtlas.class));

        intro = null;
        boolean introPlayed = game.getSave()
                        .getBool(GameStats.INTRO_PLAYED.name, false);
        if (introPlayed) return;

        FileHandle file = map.getProperties().get(KJAssets.VIDEO_INTRO,
                        null, FileHandle.class);
        if (file != null && file.exists()) {
            intro = new VideoSequence(KJAssets.VIDEO_INTRO, VJXString.STR_EMPTY, file);
            resources.put(KJAssets.VIDEO_INTRO,
                            new AssetDescriptor<>(
                                            file.pathWithoutExtension()
                                            + ".atlas",
                                            TextureAtlas.class));
        }
    }

    public void playMusic() {
        if (winCondition == WinCondition.PROGR_GROW
                        || winCondition == WinCondition.PROGR_NOTES)
            if (!isProgressFinished()) return;

        FileHandle musicFile = map.getProperties().get("music", FileHandle.class);
        kjMenu.playMusic(musicFile);
    }

    @Override
    protected void progressComplete() {
        FileHandle musicFile = map.getProperties().get("music", FileHandle.class);
        kjMenu.playMusic(musicFile);
    }

    public boolean isEndLevel() {
        return isEndLevel;
    }

    public boolean isBossLevel() {
        return isBossLevel;
    }

    public boolean hasIntro() {
        return intro != null;
    }

    public VideoSequence getIntro() {
        return intro;
    }

    public VideoSequence getVideo(String name) {
        return intro;
    }

    public boolean isGroundDeadly() {
        return groundIsDeadly;
    }

    public void growDeco(Vector2 pos, float radius) {
        growDeco(pos.x, pos.y, radius);
    }

    public void growDeco(float x, float y, float radius) {
        radius = radius == -1 ? Float.MAX_VALUE : radius;
        LifeLogic life;
        for (EJXEntity e : entities.values()) {
            life = (LifeLogic) e.getLogic(KJLogicType.LIFE);
            if (life == null) continue;
            if (life.isGrowable()) {
                tmpVec.set(e.getWorldCenter());
                e.getActor().getParent().localToStageCoordinates(tmpVec);
                if (tmpVec.dst(x, y) <= radius) {
                    life.grow();
                    life.incrHitPoints();
                    incrLvlProgress();
                }
            }
        }
        updateEvoModificator();
    }

    private void updateEvoModificator() {
        evoMod = getLvlProgress();

        float colorMod = .5f;
        if (evoMod >= EVO_PROGR_4) colorMod = .4f;
        else if (evoMod >= EVO_PROGR_3) colorMod = .3f;
        else if (evoMod >= EVO_PROGR_2) colorMod = .2f;
        else if (evoMod >= EVO_PROGR_1) colorMod = .1f;
        else if (evoMod >= EVO_PROGR_0) colorMod = 0f;

        evoColor.set(EVO_BASE_COLOR);
        evoColor.add(colorMod, colorMod, colorMod, colorMod);
    }

    @Override
    public boolean isProgressFinished() {
        if (winCondition != null) switch (winCondition) {
            case PROGR_GROW:
            case PROGR_NOTES:
            case PROGR_REPAIR:
                return getLvlProgress() >= winProgress;
            case PROGR_EXT:
            case PROGR_COLLECT:
                return getLvlProgress() <= winProgress;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void checkSpawn(EJXEntity entity, boolean spawned) {
        if (spawned) {
            String secret = entity.getDef().getCustomProperties().get("secret",
                            VJXString.STR_EMPTY, String.class);
            if (!secret.isEmpty()) {
                Secret s = Secrets.getSecret(secret);
                if (game.getSave().getBool(s.getKsString(), false)) {
                    entity.setLifecycle(Lifecycle.DESPAWN);
                }
            }

            if (winCondition != null)
                switch (winCondition) {
                    case PROGR_COLLECT:
                        if (entity.hasLogic(KJLogicType.TRASH)) {
                            maxProgress++;
                            incrLvlProgress();
                        }
                        break;
                    case PROGR_EXT:
                        if (entity.hasLogic(KJLogicType.FIRE)) {
                            maxProgress++;
                            incrLvlProgress();
                        }
                        break;
                    case PROGR_GROW:
                        if (entity.hasLogic(KJLogicType.LIFE))
                            if (((LifeLogic) entity.getLogic(KJLogicType.LIFE))
                                            .isGrowable())
                                maxProgress++;
                        break;
                    case PROGR_NOTES:
                        if (entity.hasLogic(KJLogicType.NOTE)) maxProgress++;
                        break;
                    case PROGR_REPAIR:
                        if (entity.hasLogic(KJLogicType.BROKEN)) maxProgress++;
                        break;
                    default:
                        break;
                }

            if (entity.hasLogic(KJLogicType.ITEM)) {
                ItemLogic item = (ItemLogic) entity.getLogic(KJLogicType.ITEM);
                if (item.getCategory() == ItemCategory.COIN)
                    totalSuns += item.getAmount();
            }

            if (entity.hasLogic(VJXLogicType.END_SPAWN)) {
                CallbackSpawnLogic logic = (CallbackSpawnLogic) entity
                                .getLogic(VJXLogicType.END_SPAWN);
                if (logic.getSpawnName().contains("sun")) {
                    totalSuns += logic.getCount();
                }
                if (logic.getSpawnName().contains("note")
                                && winCondition == WinCondition.PROGR_NOTES) {
                    maxProgress += logic.getCount();
                }
            }
        } else {
            if (winCondition != null)
                switch (winCondition) {
                    case PROGR_COLLECT:
                        if (entity.hasLogic(KJLogicType.TRASH)) decrLvlProgress();
                        break;
                    case PROGR_EXT:
                        if (entity.hasLogic(KJLogicType.FIRE)) decrLvlProgress();
                        break;
                    case PROGR_NOTES:
                        if (entity.hasLogic(KJLogicType.NOTE)) incrLvlProgress();
                        break;
                    default:
                        break;
                }
        }
    }

    @Override
    public void incrLvlProgress() {
        super.incrLvlProgress();
        kjMenu.getUI(KawaidaHUD.class).setProgressBar(getLvlProgress());
    }

    @Override
    public void decrLvlProgress() {
        super.decrLvlProgress();
        kjMenu.getUI(KawaidaHUD.class).setProgressBar(getLvlProgress());
    }

    @Override
    public void setProgress(float percent) {
        super.setProgress(percent);
        kjMenu.getUI(KawaidaHUD.class).setProgressBar(percent);
    }

    @Override
    public void setProgress(int absolute) {
        super.setProgress(absolute);
        kjMenu.getUI(KawaidaHUD.class).setProgressBar(getLvlProgress());
    }

    public boolean bonusFound() {
        return bonusFound;
    }

    public void setBonusFound(boolean found) {
        bonusFound = found;
    }

    public int getTotalSuns() {
        return totalSuns;
    }

    public void enableEntities(String category, boolean enable) {
        for (EJXEntity e : entities.values()) {
            String logics = e.getDef().getValue(DefinitionProperty.logics, String.class);
            if (logics.contains(category))
                e.setEnabled(enable);
        }
    }

    @Override
    public void win() {
        kjMenu.hidePopup();
        kjMenu.showPopup(POPUP.WIN.id, false);
    }

    public WinCondition getWinCondition() {
        return winCondition;
    }

    public Color getWorldColor() {
        return evoColor;
    }

    @Override
    public void dispose() {
        super.dispose();
        if (intro != null)
            intro = null;
    }
}