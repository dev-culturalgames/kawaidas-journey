package de.culturalgames.kwjourney;

import com.badlogic.gdx.pay.PurchaseManager;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.api.AdMobController;
import de.culturalgames.kwjourney.api.EJXRewardVideoAdListener.RewardItem;
import de.culturalgames.kwjourney.api.PurchaseHandler;
import de.culturalgames.kwjourney.cmds.*;
import de.culturalgames.kwjourney.content.Secrets;
import de.culturalgames.kwjourney.entity.logics.*;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.save.KJCloudSave;
import de.culturalgames.kwjourney.ui.ShopMenu;
import de.culturalgames.kwjourney.ui.menus.MapMenu;
import de.culturalgames.kwjourney.ui.popups.WinPopup;
import de.culturalgames.kwjourney.ui.widgets.KJIconTextButton;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.cmds.CommandParser;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.cmds.SetPropertyCommand;
import de.venjinx.ejx.entity.logics.EJXLogics;
import de.venjinx.ejx.save.CloudSaveHandler;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXGameService;
import de.venjinx.ejx.util.EJXGameService.GameServiceListener;

public class KJGame extends EJXGame {

    public enum GameStats {
        // hints and secrets
        HINTS("hints:", Boolean.class),
        NEW_SECRETS("secrets_new", Integer.class),

        // level progress
        LVL_FINISH("lvl_finish:", Boolean.class),
        LVL_BONUS("lvl_bonus:", Boolean.class),
        LVL_SUNS("lvl_suns:", Boolean.class),

        // game progress
        SKIN_LOCKED("lock_skin:", Boolean.class),
        CHAPTER_LOCKED("lock_chapter:", Boolean.class),
        STARS("stars", Integer.class),
        PROGRESS("progress", Float.class),

        // miscellaneous
        TIME_PLAYED("timePlayed", Long.class),
        GAME_FIRST_START("firstStart", Boolean.class),
        INTRO_PLAYED("introPlayed", Boolean.class);

        public final String name;

        private final Class<?> clazz;

        GameStats(String name, Class<?> clazz) {
            this.name = name;
            this.clazz = clazz;
        }

        public static GameStats get(String name) {
            for (GameStats stat : GameStats.values())
                if (stat.name.equals(name)) return stat;
            return null;
        }

        public static Class<?> getClass(String name) {
            for (GameStats stat : GameStats.values())
                if (stat.name.equals(name))
                    return stat.clazz;
            return null;
        }
    }

    private EJXGameService gameService;
    private CloudSaveHandler cloudHandler;
    private PurchaseManager pManager;
    private PurchaseHandler pHandler;
    private AdMobController adMobController;

    public KJGame(String[] args) {
        super(args);
    }

    @Override
    protected void userCreate() {
        player = new KJPlayer();

        gameScreen = new KJScreen(this);
        setScreen(gameScreen);

        KJDebugUI kjDebug = new KJDebugUI(this);
        gameScreen.getDebugStage().setLevel(gameScreen.getLevel());
        gameScreen.getDebugStage().getDebugUI().setCustomDebug(kjDebug);

        gameControl.setLevel(gameScreen.getLevel());
        gameControl.setMenu(gameScreen.getMenu());

        gameService.init();
        gameService.setListener((GameServiceListener) gameScreen);
        gameService.setAutoConnect(getSettings().getBool(EJXGameService.CLOUD_AUTOCONNECT, false));

        cloudHandler.setListener((GameServiceListener) gameScreen);
        cloudHandler.setSave(new KJCloudSave(player.getSettings()));

        pHandler = new PurchaseHandler(this);

        gameScreen.getMenu().setMenu(MENU.LOADING.id);
    }

    @Override
    protected void registerCustomLogics() {
        EJXLogics.registerLogic(KJLogicType.ANT, AntLogic.class);
        EJXLogics.registerLogic(KJLogicType.ATTACK, AttackLogic.class);
        EJXLogics.registerLogic(KJLogicType.BONUS, BonusLogic.class);
        EJXLogics.registerLogic(KJLogicType.BONUS_CLOUD, BonusCloudLogic.class);
        EJXLogics.registerLogic(KJLogicType.BROKEN, BrokenLogic.class);
        EJXLogics.registerLogic(KJLogicType.BUMPER, BumperLogic.class);
        EJXLogics.registerLogic(KJLogicType.STORY, StoryLogic.class);
        EJXLogics.registerLogic(KJLogicType.CHICKEN, ChickenLogic.class);
        EJXLogics.registerLogic(KJLogicType.CLIMB, ClimbLogic.class);
        EJXLogics.registerLogic(KJLogicType.CLOUD, CloudLogic.class);
        EJXLogics.registerLogic(KJLogicType.COCO, CocoLogic.class);
        EJXLogics.registerLogic(KJLogicType.COLOBUS, ColobusLogic.class);
        EJXLogics.registerLogic(KJLogicType.CONTEXT, ContextLogic.class);
        EJXLogics.registerLogic(KJLogicType.CRAB, CrabLogic.class);
        EJXLogics.registerLogic(KJLogicType.CROW, CrowLogic.class);
        EJXLogics.registerLogic(KJLogicType.DEATHWALL, DeathWallLogic.class);
        EJXLogics.registerLogic(KJLogicType.DIALOGUE, DialogLogic.class);
        EJXLogics.registerLogic(KJLogicType.EXTINGUISH, ExtinguishLogic.class);
        EJXLogics.registerLogic(KJLogicType.FERTILE, FertileLogic.class);
        EJXLogics.registerLogic(KJLogicType.FIRE, FireLogic.class);
        EJXLogics.registerLogic(KJLogicType.HINT, HintLogic.class);
        EJXLogics.registerLogic(KJLogicType.INVENTORY, InventoryLogic.class);
        EJXLogics.registerLogic(KJLogicType.ITEM, ItemLogic.class);
        EJXLogics.registerLogic(KJLogicType.JUMP, JumpLogic.class);
        EJXLogics.registerLogic(KJLogicType.LIFE, LifeLogic.class);
        EJXLogics.registerLogic(KJLogicType.MARABU, MarabuLogic.class);
        EJXLogics.registerLogic(KJLogicType.MOSQUITO, MosquitoLogic.class);
        EJXLogics.registerLogic(KJLogicType.NOTE, NoteLogic.class);
        EJXLogics.registerLogic(KJLogicType.PICKUP, PickUpRepairLogic.class);
        EJXLogics.registerLogic(KJLogicType.PIPE, PipeLogic.class);
        EJXLogics.registerLogic(KJLogicType.PLANT, PlantLogic.class);
        EJXLogics.registerLogic(KJLogicType.PLASTIC_BAG, PlasticBagLogic.class);
        EJXLogics.registerLogic(KJLogicType.STEAMGHOST, SteamGhostLogic.class);
        EJXLogics.registerLogic(KJLogicType.TELEPORT, TeleportLogic.class);
        EJXLogics.registerLogic(KJLogicType.TRAMPOLINE, TrampolineLogic.class);
        EJXLogics.registerLogic(KJLogicType.TRAP, TrapLogic.class);
        EJXLogics.registerLogic(KJLogicType.TRASH, TrashLogic.class);
        EJXLogics.registerLogic(KJLogicType.TUTOR, TutorLogic.class);
        EJXLogics.registerLogic(KJLogicType.VEHICLE, VehicleLogic.class);
        EJXLogics.registerLogic(KJLogicType.WIND, WindLogic.class);
    }

    @Override
    protected void registerCustomCommands() {
        CommandParser.registerCommand(CMD.APPEAR, AppearCommand.class);
        CommandParser.registerCommand(CMD.ATTACH, AttachCommand.class);
        CommandParser.registerCommand(CMD.ATTACK, AttackCommand.class);
        CommandParser.registerCommand(CMD.BONUS, BonusCommand.class);
        CommandParser.registerCommand(CMD.CONTROLS, SetControlsCommand.class);
        CommandParser.registerCommand(CMD.DAMAGE, DamageCommand.class);
        CommandParser.registerCommand(CMD.DETACH, AttachCommand.class);
        CommandParser.registerCommand(CMD.DIE, DieCommand.class);
        CommandParser.registerCommand(CMD.DISAPPEAR, AppearCommand.class);
        CommandParser.registerCommand(CMD.GROW_DECO, GrowDecoCommand.class);
        CommandParser.registerCommand(CMD.HINT, HintCommand.class);
        CommandParser.registerCommand(CMD.INCR_ITEM, IncrItemCommand.class);
        CommandParser.registerCommand(CMD.JUMP, JumpCommand.class);
        CommandParser.registerCommand(CMD.JUMP2, Jump2Command.class);
        CommandParser.registerCommand(CMD.LOSE, LoseCommand.class);
        CommandParser.registerCommand(CMD.PLANT, PlantCommand.class);
        CommandParser.registerCommand(CMD.SECRET, SecretCommand.class);
        CommandParser.registerCommand(CMD.SET, SetPropertyCommand.class);
        CommandParser.registerCommand(CMD.RESTORE, SetPropertyCommand.class);
        CommandParser.registerCommand(CMD.STORY, StoryCommand.class);
        CommandParser.registerCommand(CMD.TELEPORT, TeleportCommand.class);
        CommandParser.registerCommand(CMD.THROW, ThrowCommand.class);
        CommandParser.registerCommand(CMD.WIN, WinCommand.class);
    }

    @Override
    public void gameLoaded(boolean loadLevel) {
        loadSettings();
        timePlayed = getSave().getLong(GameStats.TIME_PLAYED.name, timePlayed);

        player.loadStatsFromSave(getSave());
        pHandler.loadOffers();

        MenuStage menu = gameScreen.getMenu();
        if (loadLevel) {
            ((KJScreen) gameScreen).startLoadLevel();
            menu.getMenu(MENU.LOADING.id).updateLayout(menu.getSkin());
        } else {
            menu.playMusic(KJAssets.MUSIC_MAIN);
            if (menu.getCurrentMenuId() != MENU.SETT.id) {
                menu.switchMenu(MENU.START.id, false);

                SequenceAction sa = Actions.sequence();
                sa.addAction(Actions.delay(1f));
                sa.addAction(Actions.run(new Runnable() {

                    @Override
                    public void run() {
                        assets.unloadResource(EJXGame.SPLASH);
                        assets.finishLoading();
                    }
                }));
                menu.addAction(sa);
            }

            if (!pManager.installed()) {
                menu.showWaiting(true);
                pManager.install(pHandler, pHandler.getConfig(), true);
            }
        }
    }

    public void setAdController(AdMobController adControl) {
        adMobController = adControl;
    }

    public KJIconTextButton getWatchAdButton() {
        return ((KJMenu) getScreen().getMenu()).getWatchAdButton();
    }

    public void updateWatchAdButton(String adName) {
        MenuStage menu = gameScreen.getMenu();
        if (menu.getCurrentPopupId() == POPUP.WIN.id) {
            WinPopup winPopup = (WinPopup) menu.getPopup(POPUP.WIN.id);
            winPopup.updateWatchAdText(adMobController);
            return;
        }
        getWatchAdButton().setEnabled(getAdControl().isLoaded(adName));
    }

    @Override
    public LocalSavegame getSave() {
        return cloudHandler.getSave();
    }

    @Override
    public void resetSave() {
        getSave().clear();
        getPlayer().reset();

        getSave().putBool(GameStats.GAME_FIRST_START.name, false);
        getSave().setSkipWrite(true);

        MenuStage menu = gameScreen.getMenu();
        resetShop(menu);

        MapMenu mapMenu = (MapMenu) menu.getMenu(MENU.MAP.id);
        mapMenu.resetLevels();

        //        if (getPurchaseManager().installed()) {
        //            menu.showWaiting(true);
        //            getPurchaseManager().purchaseRestore();
        //        } else
        initSave();
    }

    public void initSave() {
        getSave().putFloat(GameStats.PROGRESS.name, getPercentProgress());
        getSave().setSkipWrite(false);
        getSave().savePlayer(getPlayer(), true);
        getPlayer().loadStatsFromSave(getSave());

        gameScreen.getMenu().getUI(KawaidaHUD.class).fillStatusBar();
    }

    private void resetShop(MenuStage menu) {
        getPurchaseHandler().resetShop();
        ShopMenu shopMenu = (ShopMenu) menu.getMenu(MENU.SHOP_SUNS.id);
        shopMenu.resetShop();

        shopMenu = (ShopMenu) menu.getMenu(MENU.SHOP_PRIME.id);
        shopMenu.resetShop();

        shopMenu = (ShopMenu) menu.getMenu(MENU.SHOP_SKINS.id);
        shopMenu.resetShop();
    }

    public void setGameService(EJXGameService gameService) {
        this.gameService = gameService;
        gameService.setGame(this);
        gameService.setListener((GameServiceListener) gameScreen);
    }

    @Override
    public EJXGameService getGameService() {
        return gameService;
    }

    public void setCloudHandler(CloudSaveHandler cloudHandler) {
        this.cloudHandler = cloudHandler;
        cloudHandler.setGame(this);
    }

    @Override
    public CloudSaveHandler getCloudHandler() {
        return cloudHandler;
    }

    public AdMobController getAdControl() {
        return adMobController;
    }

    public PurchaseHandler getPurchaseHandler() {
        return pHandler;
    }

    public void setPurchaseManager(PurchaseManager pManager) {
        this.pManager = pManager;
    }

    public PurchaseManager getPurchaseManager() {
        return pManager;
    }

    public void rewardPlayer(RewardItem rewardItem) {
        WinPopup winPopup = (WinPopup) gameScreen.getMenu()
                        .getPopup(POPUP.WIN.id);
        switch (rewardItem.name) {
            case KJString.AD_test:
                if (getScreen().getMenu().getCurrentMenuId() == MENU.SETT.id) {
                    getScreen().getMenu(KJMenu.class).showReward(KJString.REWARD_test, rewardItem.amount);
                    break;
                }
                if (getPlayer().getEntity().isAlive()) {
                    winPopup.reward();
                    winPopup.updateWatchAdText(getAdControl());
                } else {
                    getAdControl().loadAd(KJString.AD_double_suns);
                    ((KJLevel) gameScreen.getLevel()).continueAfterDeath(true);
                }
                break;
            case KJString.AD_continue:
                ((KJLevel) gameScreen.getLevel()).continueAfterDeath(true);
                getAdControl().loadAd(KJString.AD_double_suns);
                break;
            case KJString.AD_double_suns:
                winPopup.reward();
                winPopup.updateWatchAdText(getAdControl());
                break;
            default:
        }
    }

    @Override
    public float getPercentProgress() {
        MapMenu mapMenu = (MapMenu) gameScreen.getMenu().getMenu(MENU.MAP.id);
        int totalStarCount = mapMenu.getLevelCount() * 3;
        int totalSecretCount = Secrets.getSecretsCount();
        float total = totalStarCount + totalSecretCount;
        return getProgress() / total;
    }

    @Override
    public long getProgress() {
        int starsFound = getSave().getInt(GameStats.STARS.name, 0);
        int secretsFound = Secrets.getFoundSecretsCount(getSave());
        return starsFound + secretsFound;
    }

    @Override
    public void dispose() {
        super.dispose();
        pManager.dispose();
        System.exit(0);
    }
}