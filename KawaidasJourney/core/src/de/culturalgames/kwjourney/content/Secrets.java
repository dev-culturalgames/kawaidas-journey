package de.culturalgames.kwjourney.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.tiled.tile.EJXTile;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class Secrets {
    public static final String PRE_secret = "secret_";
    public static final String PRE_secrets = "secrets_";

    private static HashMap<String, MapProperties> secretCatDefs = new HashMap<>();
    private static HashMap<String, MapProperties> secretDefs = new HashMap<>();
    private static HashMap<String, Secret> secrets = new HashMap<>();
    private static HashMap<String, List<String>> secretMap = new HashMap<>();

    public static void addSecret(Secret secret) {
        secrets.put(secret.getName(), secret);
    }

    public static Secret getSecret(String name) {
        return secrets.get(name);
    }

    public static int getSecretsCount() {
        return secrets.size();
    }

    public static Collection<Secret> getSecrets() {
        return secrets.values();
    }

    public static int getFoundSecretsCount(LocalSavegame saveGame) {
        int secretsCount = 0;
        for (MapProperties props : secretCatDefs.values()) {
            String name = props.get(VJXString.name, VJXString.STR_EMPTY, String.class);
            String saveString = PRE_secrets + name;
            secretsCount += saveGame.getInt(saveString, 0);
        }
        return secretsCount;
    }

    public static void addSecretDef(MapProperties props) {
        String name = props.get(VJXString.name, String.class);
        //        VJXLogger.log("add secret def: " + name);
        secretDefs.put(name, props);
        String cat = props.get("secretCategory", String.class);
        if (secretCatDefs.containsKey(cat)) {
            List<String> list = secretMap.get(cat);
            list.add(name);
        } else VJXLogger.log("Secret category for '" + name + "'. '" + cat
                        + "' not found!");
    }

    public static void addSecretCategoryDef(String name, MapProperties props) {
        secretCatDefs.put(name, props);
        secretMap.put(name, new ArrayList<String>());
    }

    public static int getSecretCount(String category) {
        if (secretMap.containsKey(category))
            return secretMap.get(category).size();
        return 0;
    }

    public static List<String> getSecretList(String category) {
        if (secretMap.containsKey(category)) return secretMap.get(category);
        return null;
    }

    public static MapProperties getSecretProperties(String name) {
        return secretDefs.get(name);
    }

    public static MapProperties getSecretCategoryProperties(String name) {
        return secretCatDefs.get(name);
    }

    public static Set<String> getSecretCategoriesNames() {
        return secretCatDefs.keySet();
    }

    public static Collection<MapProperties> getSecretCategories() {
        return secretCatDefs.values();
    }

    public static void loadSecretCategoryDefs() {
        Element defsElem = EJXUtil
                        .parseXML("data/definitions/secret_category.tsx");
        Array<Element> tiles = defsElem.getChildrenByName(EJXTile.tile);
        String imgPath;
        for (Element tile : tiles) {
            MapProperties tilesetProperties = new MapProperties();
            Element tilesetPropertiesElem = tile.getChildByName(VJXString.TILED_properties);
            EJXUtil.loadProperties(tilesetProperties, tilesetPropertiesElem);
            String name = tilesetProperties.get(VJXString.name, String.class);

            Element imageElem = tile.getChildByName("image");
            imgPath = imageElem.getAttribute("source");
            imgPath = EJXUtil.shortNameImage(imgPath);
            tilesetProperties.put("image", imgPath);
            if (name != null)
                Secrets.addSecretCategoryDef(name, tilesetProperties);
        }
    }

    public static void loadSecretDefs() {
        Element defsElem = EJXUtil.parseXML("data/definitions/secret.tsx");
        Array<Element> tiles = defsElem.getChildrenByName(EJXTile.tile);
        String imgPath;
        for (Element tile : tiles) {
            MapProperties secretProperties = new MapProperties();
            Element tilesetPropertiesElem = tile
                            .getChildByName(VJXString.TILED_properties);
            EJXUtil.loadProperties(secretProperties, tilesetPropertiesElem);

            Element imageElem = tile.getChildByName("image");
            imgPath = imageElem.getAttribute("source");
            secretProperties.put("icon", new FileHandle(imgPath));
            Secrets.addSecretDef(secretProperties);
            createSecret(secretProperties);
        }
    }

    private static void createSecret(MapProperties mp) {
        String name = mp.get(VJXString.name, String.class);
        if (name != null && !name.isEmpty()) {
            if (!mp.containsKey(VJXString.name)) {
                VJXLogger.log(LogCategory.ERROR,
                                "secret for '" + name + "' has no name!");
                return;
            }
            if (!mp.containsKey("secretCategory")) {
                VJXLogger.log(LogCategory.ERROR,
                                "secret for '" + name + "' has no category!");
                return;
            }
            Secrets.addSecret(new Secret(mp));
        }
    }

    public static class Secret {
        private String name, titleSW, category, iconName, reward, url;
        private int position, rewardAmount;

        public Secret(MapProperties props) {
            name = props.get(VJXString.name, "<no name>", String.class);
            titleSW = props.get("titleSW", "<missing sw title>", String.class);
            category = props.get("secretCategory", String.class);
            url = props.get("encyUrl", String.class);
            iconName = props.get("icon", FileHandle.class).path();
            iconName = EJXUtil.shortName(iconName, KJString.PATH_icons,
                            VJXString.FILE_JPG, VJXString.FILE_PNG);
            iconName = KJString.PATH_icons + iconName;

            reward = props.get("reward", String.class);
            rewardAmount = props.get("reward:amount", Integer.class);

            position = props.get("position", Integer.class);
        }

        public boolean equals(String s) {
            return name.equals(s);
        }

        public String getTitleSW() {
            return titleSW;
        }

        public String getCategory() {
            return category;
        }

        public String getIconName() {
            return iconName;
        }

        public String getReward() {
            return reward;
        }

        public int getRewardAmount() {
            return rewardAmount;
        }

        public String getUrl() {
            return url;
        }

        public String getName() {
            return name;
        }

        public String getKsString() {
            return getSaveString(category, name);
        }

        public int getPosition() {
            return position;
        }

        public static String getSaveString(String category, String name) {
            return PRE_secrets + category + VJXString.SEP_DDOT + PRE_secret + name;
        }
    }
}