package de.culturalgames.kwjourney;

import java.util.ArrayList;
import java.util.List;

import de.culturalgames.kwjourney.api.APILoader;
import de.culturalgames.kwjourney.api.PurchaseHandler;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.culturalgames.kwjourney.entity.logics.HintLogic.Hint;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.save.KJPlayerStats;
import de.culturalgames.kwjourney.save.KJPlayerStats.PlayerStats;
import de.venjinx.ejx.EJXPlayer;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class KJPlayer extends EJXPlayer {

    // player properties
    public static final String PL_AMMO_MAX = "player:ammo:max";
    public static final String PL_POT_MAX = "player:potions:max";
    public static final String PL_TOOLS_START = "player:tools:start";
    public static final String PL_TOOLS_MAX = "player:tools:max";
    public static final String PL_CONT_MAX = "player:continues:max";
    public static final String PL_COINS = "player:coins";
    public static final String PL_AMMO = "player:ammo";
    public static final String PL_POT = "player:potions";
    public static final String PL_TOOLS = "player:tools";
    public static final String PL_CONT = "player:continues";
    public static final String dflSkin = "kawaida0";

    private List<Secret> secrets;
    private List<Hint> hints;
    private int collectedNotes;
    private int[] noteBounds = { 1, 2, 3, 4 };

    public KJPlayer() {
        super(new KJPlayerStats());
        secrets = new ArrayList<>();
        hints = new ArrayList<>();
        reset();
    }

    @Override
    public void loadStatsFromSave(LocalSavegame stats) {
        if (stats == null) return;

        // global settings
        properties.put(PlayerStats.SKINS.name, stats.getString(
                        PlayerStats.SKINS.name, dflSkin));
        properties.put(PlayerStats.SKIN.name, stats
                        .getString(PlayerStats.SKIN.name, dflSkin));

        properties.put(PlayerStats.COINS.name,
                        stats.getInt(PlayerStats.COINS.name, 0));
        properties.put(PlayerStats.COIN_MULT.name,
                        stats.getInt(PlayerStats.COIN_MULT.name, 1));
        properties.put(PlayerStats.AMMO_MAX.name,
                        stats.getInt(PlayerStats.AMMO_MAX.name, 6));
        properties.put(PlayerStats.POTIONS_MAX.name,
                        stats.getInt(PlayerStats.POTIONS_MAX.name, 1));
        properties.put(PlayerStats.CONTINUES.name,
                        stats.getInt(PlayerStats.CONTINUES.name, 0));
        properties.put(PlayerStats.CONTINUES_MAX.name,
                        stats.getInt(PlayerStats.CONTINUES_MAX.name, 1));

        properties.put(PlayerStats.MAGNET_DURATION.name,
                        stats.getFloat(PlayerStats.MAGNET_DURATION.name, 0f));
        properties.put(PlayerStats.MAGNET_RANGE.name,
                        stats.getFloat(PlayerStats.MAGNET_RANGE.name, 0f));
        properties.put(PlayerStats.GOD_DURATION.name,
                        stats.getFloat(PlayerStats.GOD_DURATION.name, 0f));
        properties.put(PlayerStats.LAMP_RANGE.name,
                        stats.getFloat(PlayerStats.LAMP_RANGE.name, 0f));

        String activeSkin = getSkin();
        if (!activeSkin.equals("kawaida0")) {
            APILoader.getOffer(PurchaseHandler.OFFER_SKIN + activeSkin).setActive(true);
        }
    }

    public void incr(ItemCategory item) {
        incr(item, 1, false);
    }

    public void incr(ItemCategory item, boolean max) {
        incr(item, 1, max);
    }

    public void incr(ItemCategory item, int amount) {
        incr(item, amount, false);
    }

    public void incr(ItemCategory item, int amount, boolean max) {
        int current = getAmount(item, max);
        setAmount(item, current + amount, max);
    }

    public void setAmount(ItemCategory item, int amount) {
        setAmount(item, amount, false);
    }

    public void setAmount(ItemCategory item, int amount, boolean max) {
        amount = amount >= 0 ? amount : 0;
        if (max) {
            switch (item) {
                case AMMO:
                    properties.put(PlayerStats.AMMO_MAX.name, amount);
                    break;
                case TOOL:
                    properties.put(PlayerStats.TOOLS_MAX.name, amount);
                    break;
                case CONTINUE:
                    properties.put(PlayerStats.CONTINUES_MAX.name, amount);
                    break;
                case POTION:
                    properties.put(PlayerStats.POTIONS_MAX.name, amount);
                    break;
                default:
                    break;
            }
            // remove items if max amount is smaller
            if (getAmount(item) >= amount) setAmount(item, amount);
        } else {
            int maxA = getAmount(item, true);
            amount = amount <= maxA ? amount : maxA;
            switch (item) {
                case CONTINUE:
                    properties.put(PlayerStats.CONTINUES.name, amount);
                    break;
                case COIN:
                    properties.put(PlayerStats.COINS.name, amount);
                    break;
                default:
                    break;
            }
        }
    }

    public int getAmount(ItemCategory item) {
        return getAmount(item, false);
    }

    public int getAmount(ItemCategory item, boolean max) {
        if (max) {
            switch (item) {
                case AMMO:
                    return properties.get(PlayerStats.AMMO_MAX.name, 6,
                                    Integer.class);
                case TOOL:
                    return properties.get(PlayerStats.TOOLS_MAX.name, 24,
                                    Integer.class);
                case CONTINUE:
                    return properties.get(PlayerStats.CONTINUES_MAX.name, 1,
                                    Integer.class);
                case POTION:
                    return properties.get(PlayerStats.POTIONS_MAX.name, 1,
                                    Integer.class);
                case COIN:
                    return Integer.MAX_VALUE;
                default:
                    break;
            }
        } else {
            switch (item) {
                case CONTINUE:
                    return properties.get(PlayerStats.CONTINUES.name, 0,
                                    Integer.class);
                case COIN:
                    return properties.get(PlayerStats.COINS.name, 0,
                                    Integer.class);
                default:
                    break;
            }
        }
        return 0;
    }

    public void incrCoins(int count) {
        int newAmount = getCoins() + count;
        properties.put(PlayerStats.COINS.name, newAmount);
    }

    public void setCoins(int amount) {
        amount = amount > 0 ? amount : 0;
        properties.put(PlayerStats.COINS.name, amount);
    }

    public int getCoins() {
        return properties.get(PlayerStats.COINS.name, 0, Integer.class);
    }

    public void setCoinMultiply(int multiply) {
        properties.put(PlayerStats.COIN_MULT.name, multiply);
    }

    public int getCoinMultiply() {
        return properties.get(PlayerStats.COIN_MULT.name, 1, Integer.class);
    }

    public void setGodDuration(float amount) {
        properties.put(PlayerStats.GOD_DURATION.name, amount);
    }

    public float getGodDuration() {
        return properties.get(PlayerStats.GOD_DURATION.name, Float.class);
    }

    public void setMagnetDuration(float amount) {
        properties.put(PlayerStats.MAGNET_DURATION.name, amount);
    }

    public float getMagnetDuration() {
        return properties.get(PlayerStats.MAGNET_DURATION.name, Float.class);
    }

    public void setMagnetRange(float amount) {
        properties.put(PlayerStats.MAGNET_RANGE.name, amount);
    }

    public float getMagnetRange() {
        return properties.get(PlayerStats.MAGNET_RANGE.name, Float.class);
    }

    public void setLampRange(float amount) {
        properties.put(PlayerStats.LAMP_RANGE.name, amount);
    }

    public float getLampRange() {
        return properties.get(PlayerStats.LAMP_RANGE.name, Float.class);
    }

    public void setSkin(String skin) {
        properties.put(PlayerStats.SKIN.name, skin);
    }

    public String getSkin() {
        return properties.get(PlayerStats.SKIN.name, dflSkin, String.class);
    }

    public void addSkin(String name) {
        if (!getSkins().isEmpty())
            name = getSkins() + VJXString.SEP_LIST + name;
        setSkins(name);
    }

    public void setSkins(String skins) {
        properties.put(PlayerStats.SKINS.name, skins);
    }

    public String getSkins() {
        return properties.get(PlayerStats.SKINS.name, VJXString.STR_EMPTY,
                        String.class);
    }

    public boolean hasSkin(String skin) {
        return getSkins().contains(skin);
    }

    public void incrCollNotes(int amount) {
        int oldVal = collectedNotes;
        collectedNotes += amount;
        for (int i : noteBounds) {
            if (oldVal < i && collectedNotes >= i) {
                entity.getListener().getAudioControl().updateMusic();
            }
        }
    }

    public void foundSecret(Secret s, LocalSavegame saveGame) {
        boolean alreadyFound = saveGame.getBool(s.getKsString(), false);
        if (!alreadyFound && !hasSecret(s)) {
            secrets.add(s);
        }
    }

    private boolean hasSecret(Secret s) {
        for (Secret secret : secrets)
            if (secret.getName().equals(s.getName())) return true;
        return false;
    }

    public List<Secret> getSecrets() {
        return secrets;
    }

    public void foundHint(Hint hint) {
        hints.add(hint);
    }

    public List<Hint> getHints() {
        return hints;
    }

    @Override
    public void clearCurrentLevelProgress() {
        secrets.clear();
        hints.clear();
    }

    @Override
    public void reset() {
        clearStats();
        for (ItemCategory itemCat : ItemCategory.values()) {
            setAmount(itemCat, 0);
        }
        clearCurrentLevelProgress();
    }

    private void clearStats() {
        properties.clear();

        // global settings
        properties.put(PlayerStats.SKINS.name, dflSkin);
        properties.put(PlayerStats.SKIN.name, dflSkin);

        properties.put(PlayerStats.COINS.name, 0);
        properties.put(PlayerStats.COIN_MULT.name, 1);
        properties.put(PlayerStats.AMMO_MAX.name, 6);
        properties.put(PlayerStats.POTIONS_MAX.name, 1);
        properties.put(PlayerStats.CONTINUES.name, 0);
        properties.put(PlayerStats.CONTINUES_MAX.name, 1);

        properties.put(PlayerStats.MAGNET_DURATION.name, 0f);
        properties.put(PlayerStats.MAGNET_RANGE.name, 0f);
        properties.put(PlayerStats.GOD_DURATION.name, 0f);
        properties.put(PlayerStats.LAMP_RANGE.name, 0f);
    }

}