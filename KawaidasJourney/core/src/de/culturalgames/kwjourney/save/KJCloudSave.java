package de.culturalgames.kwjourney.save;

import java.util.List;

import de.culturalgames.kwjourney.KJGame.GameStats;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.api.APILoader;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.content.Secrets;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.culturalgames.kwjourney.entity.logics.HintLogic.Hint;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.save.KJPlayerStats.PlayerStats;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.EJXPlayer;
import de.venjinx.ejx.save.CloudSave;
import de.venjinx.ejx.save.GameSettings;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class KJCloudSave extends CloudSave {

    public KJCloudSave(GameSettings playerStats) {
        super(playerStats);
    }

    public void saveAll(EJXGame game, boolean bonus, boolean suns) {
        saveLevel(game.getScreen().getLevel().getLevelName(), bonus, suns);
        savePlayer(game.getPlayer(), true);
    }

    @Override
    public void savePlayer(EJXPlayer player, boolean write) {
        savePlayer((KJPlayer) player, write);
    }

    public void saveOffer(VJXOffer offer) {
        putInt(offer.getIdentifier() + "_amount", offer.getAmount());
        putInt(offer.getIdentifier() + "_level", offer.getLevel());
    }

    public void saveOffers() {
        for (VJXOffer offer : APILoader.getOffers().values()) {
            saveOffer(offer);
        }
    }

    public void saveInventory(KJPlayer player) {
        putInt(PlayerStats.COINS.name, player.getAmount(ItemCategory.COIN));
        putInt(PlayerStats.AMMO_MAX.name, player.getAmount(ItemCategory.AMMO, true));
        putInt(PlayerStats.POTIONS_MAX.name, player.getAmount(ItemCategory.POTION, true));
        putInt(PlayerStats.CONTINUES.name, player.getAmount(ItemCategory.CONTINUE));
        putInt(PlayerStats.CONTINUES_MAX.name, player.getAmount(ItemCategory.CONTINUE, true));
    }

    public void saveUpgrades(KJPlayer player) {
        putInt(PlayerStats.COIN_MULT.name, player.getCoinMultiply());
        putFloat(PlayerStats.GOD_DURATION.name, player.getGodDuration());
        putFloat(PlayerStats.MAGNET_DURATION.name, player.getMagnetDuration());
        putFloat(PlayerStats.MAGNET_RANGE.name, player.getMagnetRange());
        putString(PlayerStats.SKINS.name, player.getSkins());
        putString(PlayerStats.SKIN.name, player.getSkin());
    }

    public void saveLevel(String lvlName, boolean bonus, boolean suns) {
        int oldStars = getInt(GameStats.STARS.name, 0);
        int newStars = oldStars;

        if (!getBool(GameStats.LVL_FINISH.name + lvlName, false)) {
            newStars++;
            putBool(GameStats.LVL_FINISH.name + lvlName, true);
        }

        if (!getBool(GameStats.LVL_BONUS.name + lvlName, false) && bonus) {
            newStars++;
            putBool(GameStats.LVL_BONUS.name + lvlName, true);
        }

        if (!getBool(GameStats.LVL_SUNS.name + lvlName, false) && suns) {
            newStars++;
            putBool(GameStats.LVL_SUNS.name + lvlName, true);
        }

        if (oldStars < newStars) putInt(GameStats.STARS.name, newStars);
    }

    public void saveSecrets(List<Secret> secrets) {
        putInt(GameStats.NEW_SECRETS.name,
                        getInt(GameStats.NEW_SECRETS.name, 0) + secrets.size());
        for (Secret s : secrets) {
            saveSecret(s);
        }
    }

    private void savePlayer(KJPlayer player, boolean write) {
        saveInventory(player);
        saveUpgrades(player);
        saveSecrets(player.getSecrets());
        saveHints(player.getHints());
        saveOffers();

        if (write) write();
    }

    private void saveHints(List<Hint> hints) {
        for (Hint hint : hints) {
            saveHint(hint);
        }
    }

    private void saveHint(Hint hint) {
        putBool(GameStats.HINTS.name + hint.getTargetName(), true);
    }

    private void saveSecret(Secret secret) {
        String secretStr = secret.getKsString();
        String saveString = Secrets.PRE_secrets + secret.getCategory();
        boolean found = getBool(secretStr, false);
        if (!found) {
            putBool(secretStr, true);
            putBool(secretStr + VJXString.POST_new, true);
            int count = getInt(saveString, 0);
            putInt(saveString, count + 1);

            saveString += VJXString.POST_new;
            count = getInt(saveString, 0);
            putInt(saveString, count + 1);
        }
    }

    @Override
    protected void resolveOldVersion(int oldVersion) {
    }
}