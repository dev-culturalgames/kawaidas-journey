package de.culturalgames.kwjourney.save;

import de.venjinx.ejx.save.GameSettings;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class KJPlayerStats extends GameSettings {

    public enum PlayerStats {
        // inventory
        COINS("pl_coins", Integer.class),
        COIN_MULT("pl_coin_multiply", Integer.class),
        POTIONS("pl_potions", Integer.class),
        POTIONS_MAX("pl_potions_max", Integer.class),
        AMMO("pl_ammo", Integer.class),
        AMMO_MAX("pl_ammo_max", Integer.class),
        CONTINUES("pl_continues", Integer.class),
        CONTINUES_MAX("pl_continues_max", Integer.class),
        TOOLS("pl_tools", Integer.class),
        TOOLS_MAX("pl_tools_max", Integer.class),

        // upgrades
        MAGNET_DURATION("pl_magnet_duration", Float.class),
        MAGNET_RANGE("pl_magnet_range", Float.class),
        GOD_DURATION("pl_god_duration", Float.class),
        LAMP_RANGE("pl_lamp_range", Float.class),

        // miscellaneous
        SKIN("pl_skin", String.class),
        SKINS("pl_skins", String.class);

        public final String name;

        private Class<?> clazz;

        private PlayerStats(String name, Class<?> clazz) {
            this.name = name;
            this.clazz = clazz;
        }

        public static final PlayerStats get(String name) {
            for (PlayerStats stat : PlayerStats.values())
                if (stat.name.equals(name)) return stat;
            return null;
        }

        public static final Class<?> getClass(String name) {
            for (PlayerStats stat : PlayerStats.values())
                if (stat.name.equals(name))
                    return stat.clazz;
            return null;
        }
    }

    // default values for player, will be set on initiate and clear
    private final int pl_ammo_max = 6;
    private final int pl_tools_max = 36;
    private final int pl_potions_max = 1;
    private final int pl_continues_max = 1;
    private final int pl_coins = 0;
    private final float pl_solar_range = 0f;
    private final float pl_solar_duration = 0f;
    private final float pl_karafuu_duration = 0f;
    private final float pl_lamp_range = 0f;

    private static final String KJ_STATS = "kj_player_stats";

    public KJPlayerStats() {
        super(KJ_STATS);
    }

    @Override
    public void clear() {
        super.clear();
        putInt(VJXString.PRE_int_ + PlayerStats.COINS.name, pl_coins);
        putInt(VJXString.PRE_int_ + PlayerStats.AMMO_MAX.name, pl_ammo_max);
        putInt(VJXString.PRE_int_ + PlayerStats.CONTINUES_MAX.name, pl_continues_max);
        putInt(VJXString.PRE_int_ + PlayerStats.POTIONS_MAX.name, pl_potions_max);
        putInt(VJXString.PRE_int_ + PlayerStats.TOOLS_MAX.name, pl_tools_max);
        putFloat(VJXString.PRE_float_ + PlayerStats.MAGNET_DURATION.name, pl_solar_duration);
        putFloat(VJXString.PRE_float_ + PlayerStats.MAGNET_RANGE.name, pl_solar_range);
        putFloat(VJXString.PRE_float_ + PlayerStats.GOD_DURATION.name, pl_karafuu_duration);
        putFloat(VJXString.PRE_float_ + PlayerStats.LAMP_RANGE.name, pl_lamp_range);
        putString(VJXString.PRE_string_ + PlayerStats.SKINS.name, VJXString.STR_EMPTY);
        putString(VJXString.PRE_string_ + PlayerStats.SKIN.name, VJXString.STR_EMPTY);
    }
}