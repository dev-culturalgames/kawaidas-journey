package de.culturalgames.kwjourney.save;

import de.culturalgames.kwjourney.KJGame.GameStats;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.save.CloudSaveHandler;
import de.venjinx.ejx.save.LocalSavegame.SavegamePrototype;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public abstract class KJCloudSaveHandler extends CloudSaveHandler {

    private static final String LOADING_FAILED = "LOADING CLOUD SAVE FAILED";

    @Override
    public void loadGamestate(byte[] gameState) {
        SavegamePrototype cloudProtype;
        if (gameState != null) {
            String jsonData = new String(gameState);
            if (jsonData.isEmpty()) {
                VJXLogger.log(LogCategory.INFO,
                                CLOUD_CREATE_NEW_SAVEGAME + save.getName());
                upload();
                savegameLoaded();
            } else {
                cloudProtype = json.fromJson(SavegamePrototype.class, jsonData);
                float cloudProgress = cloudProtype.floats
                                .getOrDefault(VJXString.PRE_float_
                                                + GameStats.PROGRESS.name, 0f);
                float localProgress = game.getSave().getFloat(GameStats.PROGRESS.name, 0f);

                if (cloudProgress > localProgress) {
                    game.getSave().copy(cloudProtype);
                    VJXLogger.log(LogCategory.INFO,
                                    CLOUD_LOADING_SUCCESSFUL + save.getName());
                    savegameLoaded();
                } else {
                    VJXLogger.log(LogCategory.INFO, CLOUD_LOCAL_HIGHER_PROGRESS);
                    savegameLoaded();
                    if (cloudProgress != localProgress)
                        upload();
                }
                //                VJXLogger.log("local data: " + game.getSave().toString());
                //                VJXLogger.log("-----------------------------------------");
                //                VJXLogger.log("cloud data: " + jsonData);
            }
        } else {
            showError(LOADING_FAILED);
        }
    }

    @Override
    public void solveConflictingSaveData(Runnable useLocal, Runnable useCloud) {
        VJXLogger.log(LogCategory.ERROR, CLOUD_CONFLICT_SNAPSHOT + save.getName());
        TextControl texts = TextControl.getInstance();
        String title = texts.get(KJTexts.CONFIRM_CONFLICTING_DATA_TITLE);
        String text = texts.get(KJTexts.CONFIRM_CONFLICTING_DATA_TEXT);
        game.getScreen().getMenu(KJMenu.class).showConfirm(title, text, useCloud, useLocal);
    }
}