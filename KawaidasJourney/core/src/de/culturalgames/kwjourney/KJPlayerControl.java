package de.culturalgames.kwjourney;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;

import de.culturalgames.kwjourney.entity.logics.AttackLogic;
import de.culturalgames.kwjourney.entity.logics.ClimbLogic;
import de.culturalgames.kwjourney.entity.logics.ContextLogic;
import de.culturalgames.kwjourney.entity.logics.ExtinguishLogic;
import de.culturalgames.kwjourney.entity.logics.JumpLogic;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.PickUpRepairLogic;
import de.culturalgames.kwjourney.entity.logics.PlantLogic;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.PlayerInput;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.ClickType;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.VJXDirection;
import de.venjinx.ejx.util.PositionTracker;

public class KJPlayerControl extends PlayerInput {
    private KJLevel kjLevel;
    private EJXEntity playerEntity;
    private Vector2 inputDir;
    private Vector2 inputPower;
    private Touchpad touchPad;
    private Button jumpButton;
    private boolean toggleRun = true;
    private boolean upTriggered;
    private boolean downTriggered;
    private boolean autoRunX;
    private boolean autoRunY;
    //    private boolean spaceTriggered;
    //    private int holdDownCounter = 0;
    private boolean spaceDown;

    private Box2DLogic physicsLogic;
    private MoveLogic moveLogic;
    private AttackLogic attackLogic;
    private JumpLogic jumpLogic;
    private ClimbLogic climbLogic;
    private PickUpRepairLogic pickUpLogic;
    private ExtinguishLogic extinguishLogic;
    private PlantLogic plantLogic;
    private ContextLogic contextLogic;

    private Rectangle screenBound;

    public KJPlayerControl() {
        super();
        inputDir = new Vector2();
        inputPower = new Vector2();
    }

    public void setAutoRun(boolean autoX, boolean autoY) {
        autoRunX = autoX;
        autoRunY = autoY;
    }

    @Override
    public void setLevel(LevelStage levelStage) {
        kjLevel = (KJLevel) levelStage;
    }

    @Override
    public void userUpdate(float delta) {
        if (getIngameControl()) {
            if (playerEntity.isActive() || climbLogic == null
                            || !moveLogic.isGrounded()
                            && !climbLogic.climbingUp()
                            && !climbLogic.climbingDown()) {

                checkTouch();

                checkKeyInput(delta);

                checkAutoRun();

                if (toggleRun()) toggleRun = !toggleRun;

                if (setRun() && Math.abs(inputPower.x) <= .5f)
                    inputPower.scl(2);

                inputDir.nor();

                if (game.getScreen().getLevel().isActive()) {
                    updateMovement();

                    updateActions();
                }
            }

            if (input.isKeyReleased(Keys.K) || input.isKeyReleased(Keys.R)) {
                execContextAction(contextLogic.getContext(), ClickType.UP);
            }
        }

        checkGameKey();
        checkScroll();
    }

    public void levelLoaded(EJXGame g) {
        playerEntity = g.getPlayer().getEntity();
        physicsLogic = playerEntity.getPhysics();
        moveLogic = (MoveLogic) playerEntity.getLogic(VJXLogicType.MOVE);
        climbLogic = (ClimbLogic) playerEntity.getLogic(KJLogicType.CLIMB);
        attackLogic = (AttackLogic) playerEntity.getLogic(KJLogicType.ATTACK);
        jumpLogic = (JumpLogic) playerEntity.getLogic(KJLogicType.JUMP);
        pickUpLogic = (PickUpRepairLogic) playerEntity.getLogic(KJLogicType.PICKUP);
        extinguishLogic = (ExtinguishLogic) playerEntity.getLogic(KJLogicType.EXTINGUISH);
        plantLogic = (PlantLogic) playerEntity.getLogic(KJLogicType.PLANT);
        contextLogic = (ContextLogic) playerEntity.getLogic(KJLogicType.CONTEXT);
        touchPad = ((KJMenu) g.getScreen().getMenu()).getTouchPad();
        jumpButton = ((KJMenu) g.getScreen().getMenu()).getJumpButton();
    }

    public void execContextAction(ActionContext context) {
        execContextAction(context, ClickType.CLICK, false);
    }

    public void execContextAction(ActionContext context, ClickType type) {
        execContextAction(context, type, false);
    }

    public void execContextAction(ActionContext context, ClickType type,
                    boolean boolValue) {
        if (!playerEntity.isAlive() || context == null)
            return;
        boolean stop = true;
        for (ClickType t : context.types)
            if (t == type) {
                stop = false;
                break;
            }
        if (stop)
            return;

        switch (context) {
            case TELEPORT:
                playerEntity.getTargetEntity().onActivated(playerEntity, true);
                break;
            case JUMP_DOWN:
                climbLogic.climbDown(boolValue);
                break;
            case PLANT:
                plantLogic.plant();
                break;
            case EXTINGUISH:
                extinguishLogic.toggleSquirt(type);
                break;
            case TRASH:
                pickUpLogic.pickUp();
                break;
            case REPAIR:
                pickUpLogic.pickPipe();
                break;
            case BROKEN:
                pickUpLogic.repairPipe();
                break;
            case USE:
                break;
            default:
        }
    }

    private void checkScroll() {
        if (input.hasScrolled()) {
            int amount = input.getScrollAmount();
            float value = 0;
            if (input.isKeyDown(Keys.CONTROL_LEFT)) {
                value = game.getScreen().getRenderer().getAmbLight() + amount / 10f;
                if (value < 0)
                    value = 0;
                else if (value > 1)
                    value = 1;
                game.getScreen().getRenderer().setAmbLight(value);
            } else if (input.isKeyDown(Keys.SHIFT_LEFT)) {
                value = game.getScreen().getRenderer().getCamFocusRadius() + amount;
                game.getScreen().getRenderer().setCamFocusRadius(value >= 0 ? value : 0);
            } else {
                value = kjLevel.getZoom() + Math.signum(amount) * .1f;
                value = MathUtils.round(value * 10) / 10f;
                kjLevel.setZoom(value >= 0 ? value : 0);
            }
        }
    }

    private void checkGameKey() {
        if (input.isKeyPressed(Keys.ENTER))
            menu.enterCallback();
        //        if (isKeyPressed(Keys.ESCAPE) && !menuControl.isMenu(MENU.GAME.id))
        //            menuControl.restoreUI();
    }

    private void updateActions() {
        if (spaceDown) {
            jumpLogic.jump();
        } else if (jumpLogic.isJumping() && !jumpLogic.stoppedJump()) {
            jumpLogic.initJump(Math.min(.5f, jumpLogic.getJumpDist())
                            * B2DWorld.WORLD_UNIT);
            jumpLogic.stopJump();
        }

        if (input.isKeyPressed(Keys.M) || input.isKeyPressed(Keys.E)) {
            attackLogic.throwCocos();
        }

        if (input.isKeyPressed(Keys.K) || input.isKeyPressed(Keys.R))
            if (contextLogic.getContext() == ActionContext.EXTINGUISH)
                execContextAction(contextLogic.getContext(), ClickType.DOWN);
            else execContextAction(contextLogic.getContext(), ClickType.CLICK);
    }

    private void updateMovement() {
        if (!checkScreenBounds()) {
            physicsLogic.detach();
            moveLogic.stop();
            inputDir.setZero();
            inputPower.setZero();
        }
        if (inputPower.len() > 0) {
            moveLogic.setSpeedMod(inputPower.x, inputPower.y);

            if (climbLogic != null && !autoRunX) {
                if (!climbLogic.isClimbing()) {
                    climbLogic.startClimb(inputDir);
                } else if (climbLogic.canClimb(inputDir)) {
                    inputDir.x = 0;
                    inputDir.nor();
                } else inputDir.setZero();

                if (!playerEntity.isActivity(Activity.EXECUTE))
                    moveLogic.moveDirection(inputDir, climbLogic.isClimbing()
                                    || climbLogic.climbingDown());
            } else {
                moveLogic.moveDirection(inputDir, false);
            }
        } else if (moveLogic.isMoving()) {
            moveLogic.stop();
            moveLogic.moveDirection(inputDir, false);
        }
    }

    private void checkAutoRun() {
        if (autoRunX) {
            inputDir.x = 1;
            inputPower.x = .5f;
        }

        if (autoRunY) {
            inputDir.y = 1;
            inputPower.y = .5f;
        }
    }

    private void checkKeyInput(float delta) {
        if (input.isKeyDown(Keys.A)) {
            inputDir.x = -1;
            inputPower.x = .5f;
        } else if (input.isKeyDown(Keys.D)) {
            inputDir.x = 1;
            inputPower.x = .5f;
        }

        if (input.isKeyDown(Keys.S)) {
            inputDir.y = -1;
            inputPower.y = .5f;

            if (!climbLogic.isClimbing()) {
                if (physicsLogic.touchesB2DBit(B2DBit.PASSABLE, VJXDirection.DOWN)
                                && climbLogic.canClimb(VJXDirection.DOWN.direction, false)) {
                    execContextAction(ActionContext.JUMP_DOWN, ClickType.CLICK, true);
                    //                    climbLogic.startClimb(VJXDirection.DOWN.direction);

                }
            }
        } else if (input.isKeyDown(Keys.W)) {
            inputDir.y = 1;
            inputPower.y = .5f;
        }

        if (input.isKeyDown(Keys.SPACE) || jumpButton.isPressed()) {
            spaceDown = true;
        }

        if (!input.isKeyDown(Keys.SPACE) && !jumpButton.isPressed())
            spaceDown = false;
    }

    private void checkTouch() {
        inputPower.set(touchPad.getKnobPercentX(), touchPad.getKnobPercentY());

        if (Math.abs(inputPower.x) < .2f) inputPower.x = 0;
        if (Math.abs(inputPower.y) < .2f) inputPower.y = 0;

        if (inputPower.y >= .75f && !upTriggered) {
            upTriggered = true;
            input.setKeyDown(Keys.W);
        } else if (inputPower.y < .75f && upTriggered) {
            upTriggered = false;
            input.setKeyDown(Keys.W, false);
        } else if (inputPower.y <= -.75f && !downTriggered) {
            downTriggered = true;
            input.setKeyDown(Keys.S);
        } else if (inputPower.y > -.75f && downTriggered) {
            downTriggered = false;
            input.setKeyDown(Keys.S, false);
        }

        inputDir.set(inputPower);
        inputPower.set(Math.abs(inputPower.x), Math.abs(inputPower.y));
    }

    private boolean checkScreenBounds() {
        screenBound = game.getScreen().getLevel().getScreenbound();
        if (inputDir.x <= 0 && playerEntity.getWorldPosition().x < screenBound.x) {
            return false;
        } else if (inputDir.x >= 0 && playerEntity.getWorldPosition().x
                        + playerEntity.getScaledWidth() > screenBound.x
                        + screenBound.width) {
            return false;
        }

        return true;
    }

    private boolean setRun() {
        if (autoRunX || autoRunY)
            return true;
        if (toggleRun) return !input.isKeyDown(Input.Keys.SHIFT_LEFT)
                        && !input.isKeyDown(Input.Keys.SHIFT_RIGHT);
        return input.isKeyDown(Input.Keys.SHIFT_LEFT)
                        || input.isKeyDown(Input.Keys.SHIFT_RIGHT);
    }

    private boolean toggleRun() {
        return input.isKeyPressed(Input.Keys.ALT_LEFT)
                        || input.isKeyPressed(Input.Keys.ALT_RIGHT);
    }

    @Override
    public boolean catchGesture(PositionTracker posTracker) {
        if (!getIngameControl()) return false;
        Vector2 velocity = posTracker.getVelocity();
        float angle = velocity.angle();

        // swiped up
        if (angle >= 45 && angle <= 135) {
            //            float max = jumpLogic.getMaxJumpHeight() < velocity.y / 1000f
            //                            ? jumpLogic.getMaxJumpHeight()
            //                                            : velocity.y / 1000f;
            //                            jumpLogic.jump(max);
            //                            spaceDown = true;
            //                            return true;
        }

        // swiped down
        if (angle >= 225 && angle <= 315) {
            if (physicsLogic.touchesB2DBit(B2DBit.PASSABLE, VJXDirection.DOWN))
                execContextAction(ActionContext.JUMP_DOWN, ClickType.CLICK);
            else if (climbLogic.isClimbing()) {
                climbLogic.stopClimbing();
                playerEntity.setActivity(Activity.IDLE);
            }
            return false;
        }

        if (playerEntity.isActivity(Activity.EXECUTE)) return false;

        // swiped left
        if (angle > 135 && angle < 225) {
            if (!climbLogic.isClimbing())
                playerEntity.setOrientation(Orientation.LEFT);
            else if (playerEntity.getOrientation() == Orientation.RIGHT)
                return false;
            attackLogic.throwCocos();
            return false;
        }

        // swiped right
        if (angle > 315 || angle < 45) {
            if (!climbLogic.isClimbing())
                playerEntity.setOrientation(Orientation.RIGHT);
            else if (playerEntity.getOrientation() == Orientation.LEFT)
                return false;
            attackLogic.throwCocos();
            return false;
        }
        return false;
    }
}