package de.culturalgames.kwjourney;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.VJXGraphix;
import de.venjinx.vjx.ui.EJXSkin;

public class KJSkin extends EJXSkin {

    public KJSkin(EJXFont font) {
        super(font);
    }

    @Override
    public void init() {
        TextureRegion textureRegion;
        Texture texture;
        Drawable d0, d1;

        //UI colors
        Color uiBlank = VJXGraphix.createColor(0, 0, 0, 0);

        // add touch pad
        d0 = getDrawable(KJAssets.BUTTON_BACKGROUND);
        d1 = getDrawable(KJAssets.ICON_JOY_KNOB);
        add("style_ui_joystick", new TouchpadStyle(d0, d1));

        Touchpad tp = new Touchpad(0, this, "style_ui_joystick");
        tp.setName("ui_joystick");
        add("ui_joystick", tp);

        // button styles with down or active images
        addButtonStyle(KJAssets.BUTTON_SETTINGS);
        addButtonStyle(KJAssets.BUTTON_SHARE);

        // transparent button styles
        texture = createButtonTexture(256, 256, uiBlank, uiBlank, uiBlank, uiBlank, 0);
        textureRegion = new TextureRegion(texture);
        d0 = new TextureRegionDrawable(textureRegion);
        addButtonStyle(KJAssets.BUTTON_TRANSPARENT_256, d0, d0);

        //transparent background
        texture = createBackgroundTexture(1, 1, 0f, 0f, 0f, .8f);
        d0 = new TextureRegionDrawable(new TextureRegion(texture));
        add(KJAssets.BUTTON_BG_TRANS_80, d0, Drawable.class);

        texture = createBackgroundTexture(1, 1, 0f, 0f, 0f, 0f);
        d0 = new TextureRegionDrawable(new TextureRegion(texture));
        add(KJAssets.BUTTON_BG_TRANS_00, d0, Drawable.class);

        createProgressBar();
    }

    private void createProgressBar() {
        TextureRegion textureRegion;
        Texture texture;
        Drawable d0;

        Color uiMainCol1 = VJXGraphix.createColor(237, 237, 237, 1);
        Color uiBorderCol = VJXGraphix.createColor(26, 25, 25, 1);

        // in-game level progress bar
        int progWidth = 64 * 5 + 2 * 16;
        texture = makeProgBarBG(progWidth, 64, uiBorderCol, 16);
        textureRegion = new TextureRegion(texture);
        d0 = new TextureRegionDrawable(textureRegion);
        add(KJAssets.ICON_LVL_PBAR_BG, d0, Drawable.class);

        texture = makeProgBar(progWidth - 24, 56, Color.WHITE, 16);
        textureRegion = new TextureRegion(texture);
        d0 = new TextureRegionDrawable(textureRegion);
        add(KJAssets.ICON_LVL_PBAR_FILL, d0, Drawable.class);

        texture = makeProgBarCover(progWidth, 64, uiMainCol1, 16);
        textureRegion = new TextureRegion(texture);
        d0 = new TextureRegionDrawable(textureRegion);
        add(KJAssets.ICON_LVL_PBAR_OVERLAY, d0, Drawable.class);
    }

    private Texture makeProgBarBG(int w, int h, Color c, int r) {
        Pixmap pm = new Pixmap(w, h, Format.RGBA8888);
        VJXGraphix.drawRoundRect(pm, c, 0, 0, w, h, r);
        Texture tex = new Texture(pm);
        pm.dispose();
        return tex;
    }

    private Texture makeProgBar(int w, int h, Color c, int r) {
        Pixmap pm = new Pixmap(w, h, Format.RGBA8888);
        VJXGraphix.drawRoundRect(pm, c, 0, 0, w, h, r);
        Texture tex = new Texture(pm);
        pm.dispose();
        return tex;
    }

    private Texture makeProgBarCover(int w, int h, Color c, int r) {
        Pixmap pm = new Pixmap(w, h, Format.RGBA8888);
        VJXGraphix.drawRoundRect(pm, c, 0, 0, w, h, r);
        Pixmap pm2 = new Pixmap(w, h, Format.RGBA8888);
        VJXGraphix.drawRoundRect(pm2, c, r + 5, 5, h - 10, h - 10, r);
        VJXGraphix.drawRoundRect(pm2, c, r + 5 + h, 5, h - 10, h - 10, r);
        VJXGraphix.drawRoundRect(pm2, c, r + 5 + h * 2, 5, h - 10, h - 10, r);
        VJXGraphix.drawRoundRect(pm2, c, r + 5 + h * 3, 5, h - 10, h - 10, r);
        VJXGraphix.drawRoundRect(pm2, c, r + 5 + h * 4, 5, h - 10, h - 10, r);
        VJXGraphix.cropPixmap(pm, pm2);
        Texture tex = new Texture(pm);
        pm.dispose();
        pm2.dispose();
        return tex;
    }
}