package de.culturalgames.kwjourney.api;

import java.util.HashMap;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader.Element;

import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;

public class APILoader {

    private static HashMap<String, VJXOffer> offerDefs = new HashMap<>();

    public static void loadIAPShopDefs() {
        Element defsElem = EJXUtil.parseXML("data/definitions/offers.tsx");
        Array<Element> tiles = defsElem.getChildrenByName("tile");
        for (Element tile : tiles) {
            Element imgElem = tile.getChildByName("image");
            String src = imgElem.getAttribute("source", null);
            if (src == null) continue;
            MapProperties tilesetProperties = new MapProperties();
            Element tilesetPropertiesElem = tile.getChildByName("properties");
            EJXUtil.loadProperties(tilesetProperties, tilesetPropertiesElem);

            String[] split = EJXUtil.getSourceSplit(src, VJXString.FILE_PNG);
            src = split[1];

            tilesetProperties.put(VJXString.tileName, src);
            VJXOffer offer = new VJXOffer(tilesetProperties);
            addOffer(offer);
        }
    }

    private static void addOffer(VJXOffer offer) {
        offerDefs.put(offer.getIdentifier(), offer);
    }

    public static VJXOffer getOffer(String name) {
        return getOffer(name, false);
    }

    public static VJXOffer getOffer(String name, boolean resolveLevel) {
        if (resolveLevel) return offerDefs.get(resolveUpgradeName(name));
        return offerDefs.get(name);
    }

    private static String resolveUpgradeName(String name) {
        return name.replaceAll("_\\d+", VJXString.STR_EMPTY);
    }

    public static HashMap<String, VJXOffer> getOffers() {
        return offerDefs;
    }
}