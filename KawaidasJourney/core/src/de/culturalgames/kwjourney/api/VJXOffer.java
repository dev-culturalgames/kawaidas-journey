package de.culturalgames.kwjourney.api;

import java.util.HashMap;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.pay.Offer;
import com.badlogic.gdx.pay.OfferType;

import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class VJXOffer extends Offer {
    private static final String consumable = "consumable";
    private static final String entitlement = "entitlement";

    public enum VJXOfferType {
        ITEM("item"), UPGRADE("upgrade"),
        CONSUMABLE("consumable"), SKIN("skin");

        public final String name;

        private VJXOfferType(String name) {
            this.name = name;
        }

        public static final VJXOfferType get(String name) {
            for (VJXOfferType type : VJXOfferType.values())
                if (type.name.equals(name)) return type;
            return null;
        }

        public static final OfferType getGoogleType(String type) {
            if (type == null) return null;
            switch (type) {
                case consumable:
                    return OfferType.CONSUMABLE;
                case entitlement:
                    return OfferType.ENTITLEMENT;
                default:
                    break;
            }
            return null;
        }
    }

    private VJXOfferType type;
    private int level = 0;
    private int maxLevel = 1;
    private boolean isPremium = false;
    private boolean isReward = false;
    private boolean isActivatable = false;
    private boolean isActive = false;
    private int amount = 1;
    private int amountMax = 1;

    private HashMap<String, float[]> levelValues;

    private String skin = VJXString.STR_EMPTY;

    //    private String title;
    private String titleSW;
    //    private String text;
    private String labelText;
    //    private String unlockText;
    private String drawableName;

    public VJXOffer(final MapProperties properties) {
        setIdentifier(properties.get(VJXString.tileName, null, String.class));
        setType(VJXOfferType.getGoogleType(properties.get("googleType", null, String.class)));

        type = VJXOfferType.get(properties.get("offerType", String.class));
        //        title = properties.get("title", "<missing eng title>", String.class);
        //        text = properties.get("text", "<missing eng text>", String.class);
        //        unlockText = properties.get("unlockText", "<missing eng text>", String.class);
        titleSW = properties.get("titleSW", "<missing sw title>", String.class);
        labelText = properties.get("label", VJXString.STR_EMPTY, String.class);
        isPremium = properties.get("isPremium", false, Boolean.class);
        isReward = properties.get("isReward", false, Boolean.class);
        isActivatable = properties.get("activatable", false, Boolean.class);
        amountMax = properties.get("amount:max", 1, Integer.class);
        skin = properties.get("skin", VJXString.STR_EMPTY, String.class);

        amount = amountMax;

        level = 0;
        maxLevel = 0;
        levelValues = new HashMap<>();
        String levelString = properties.get("level", VJXString.STR_EMPTY, String.class);
        String[] levels = null;
        if (!levelString.isEmpty()) {
            levels = levelString.split("level_");
            if (levels != null && levels.length > 0)
                maxLevel = levels.length - 1;
        }

        levelValues.put("price", new float[maxLevel + 1]);
        levelValues.put("value", new float[maxLevel + 1]);

        levelValues.get("price")[0] = properties.get("price", 0f, Float.class);
        levelValues.get("value")[0] = properties.get("value", 0f, Float.class);

        if (levels != null && levels.length > 1) {
            String[] levelProps;
            for (int i = 1; i < levels.length; i++) {
                levelProps = levels[i].split(VJXString.SEP_NEW_LINE_N);
                for (int j = 1; j < levelProps.length; j++) {
                    String[] propSplit = levelProps[j].split(VJXString.SEP_DDOT);
                    levelValues.get(propSplit[0])[i] = Float
                                    .parseFloat(propSplit[1]);
                }
            }
        }
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public int getMaxAmount() {
        return amountMax;
    }

    public void incrLvl() {
        if (level < maxLevel) level++;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public String getLevelIdentifier() {
        if (hasLevels())
            return getIdentifier() + VJXString.SEP_USCORE + (getLevel() + 1);
        else return getIdentifier();
    }

    public boolean hasLevels() {
        return maxLevel > 1;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public boolean isMaxLevel() {
        return level == maxLevel;
    }

    public float getPrice() {
        if (type == VJXOfferType.UPGRADE) {
            if (level >= maxLevel) return 0f;
            return levelValues.get("price")[level + 1];
        } else return levelValues.get("price")[0];
    }

    public int getIntPrice() {
        return (int) getPrice();
    }

    public String getStringPrice() {
        String priceString;
        float price = getPrice();
        if (price - (int) price != 0) {
            priceString = Float.toString(price);
            priceString = priceString.replace(".", ",");
        } else priceString = Integer.toString((int) price) + ",-";
        return priceString;
    }

    public float getValue() {
        if (type == VJXOfferType.UPGRADE)
            return levelValues.get("value")[level];
        else return levelValues.get("value")[0];
    }

    public boolean isAvailable() {
        return amount != 0;
    }

    public boolean isAvailable(int playerCoins) {
        return isAvailable() && playerCoins >= getPrice();
    }

    public VJXOfferType getOfferType() {
        return type;
    }

    // TODO better localization structure use properties + references
    public String getTitle() {
        return TextControl.getInstance().get(getIdentifier() + TextControl._TITLE);
    }

    public String getText() {
        return TextControl.getInstance().get(getIdentifier() + TextControl._TEXT);
    }

    public String getUnlockText() {
        return TextControl.getInstance().get(getIdentifier() + TextControl._UNLOCK_TEXT);
    }

    public String getTitleSW() {
        return titleSW;
    }

    public String getLabelText() {
        return labelText;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public boolean isReward() {
        return isReward;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean isActivatable() {
        return isActivatable;
    }

    public String getSkin() {
        return skin;
    }

    public int reset() {
        level = 0;
        amount = amountMax;
        isActive = false;
        return amount;
    }

    public void setDrawableName(String drawableName) {
        this.drawableName = drawableName;
    }

    public String getDrawableName() {
        return drawableName;
    }
}