package de.culturalgames.kwjourney.api;

import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.ui.widgets.KJIconTextButton;
import de.venjinx.ejx.EJXGame;

public abstract class AdMobController {

    protected EJXGame game;
    protected EJXRewardVideoAdListener ejxListener;

    protected boolean rewarded = false;
    protected String currentLoadingAd;
    protected String currentAd;

    public AdMobController(KJGame game) {
        this.game = game;
        ejxListener = new EJXRewardVideoAdListener(game);
        game.setAdController(this);
    }

    public EJXGame getGame() {
        return game;
    }

    public boolean isLoaded(String adName) {
        if (currentAd == null) return false;
        return currentAd.equals(adName);
    }

    public abstract void loadAd(final String adName);

    public abstract void showAd(final String adName);
}