package de.culturalgames.kwjourney.api;

import java.util.HashMap;

import de.culturalgames.kwjourney.KJGame;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class EJXRewardVideoAdListener {

    public static class RewardItem {

        public String name;
        public String type;
        public int amount;

        public RewardItem(String name, String type, int amount) {
            this.name = name;
            this.type = type;
            this.amount = amount;
        }
    }

    private final KJGame game;
    private final HashMap<String, RewardItem> rewards;

    public EJXRewardVideoAdListener(KJGame game) {
        this.game = game;

        rewards = new HashMap<>();
    }

    public void onRewardedVideoStarted(String adName, String adId,
                    AdMobController controller) {
        VJXLogger.log(adName + " (Id: " + adId + ") started.");
    }

    public void onRewardedVideoAdOpened(String adName, String adId,
                    AdMobController controller) {
        VJXLogger.log(adName + " (Id: " + adId + ") opened.");
    }

    public void onRewardedVideoAdLoaded(String adName, String adId,
                    AdMobController controller) {
        VJXLogger.log(adName + " (Id: " + adId + ") loaded.");
        controller.currentAd = adName;
        controller.currentLoadingAd = null;
        game.updateWatchAdButton(adName);
    }

    public void onRewardedVideoAdLeftApplication(String adName, String adId,
                    AdMobController controller) {
        VJXLogger.log(adName + " (Id: " + adId + ") left application.");
        if (rewards.containsKey(adName)) {
            VJXLogger.log("Reward player");
            game.rewardPlayer(rewards.remove(adId));
        }
    }

    public void onRewardedVideoAdFailedToLoad(String adName, String adId,
                    long errorCode, AdMobController controller) {
        VJXLogger.log(LogCategory.ERROR, adName + " (Id: " + adId + ") failed to load: " + errorCode);
        controller.currentAd = null;
        controller.currentLoadingAd = null;
    }

    public void onRewardedVideoAdClosed(String adName, String adId, AdMobController controller) {
        onRewardedVideoAdClosed(adName, adId, controller, false, null);
    }

    public void onRewardedVideoAdClosed(String adName, String adId, AdMobController controller, String loadNewAdName) {
        onRewardedVideoAdClosed(adName, adId, controller, true, loadNewAdName);
    }

    public void onRewardedVideoAdClosed(String adName, String adId, AdMobController controller, boolean reload,
                                        String loadNewAdName) {
        VJXLogger.log(adName + " (Id: " + adId + ") closed.");
        controller.currentAd = null;

        if (rewards.containsKey(adId)) {
            VJXLogger.log("Reward player");
            game.rewardPlayer(rewards.remove(adId));
        } else {
            VJXLogger.log("Reload " + adName);
            controller.loadAd(adName);
        }

        if (!reload) return;

        if (loadNewAdName != null && !loadNewAdName.isEmpty()) {
            VJXLogger.log("load new ad " + loadNewAdName);
            controller.loadAd(loadNewAdName);
        }
    }

    public void onRewarded(String adName, String adId, String type, int amount, AdMobController controller) {
        VJXLogger.log(adName + " (Id: " + adId + ") rewarded: " + type + " (" + amount + ")");
        rewards.put(adId, new RewardItem(adName, type, amount));
    }

    public void onRewardedVideoCompleted(String adName, String adId, AdMobController controller) {
        VJXLogger.log(adName + " (Id: " + adId + ") completed.");
    }
}