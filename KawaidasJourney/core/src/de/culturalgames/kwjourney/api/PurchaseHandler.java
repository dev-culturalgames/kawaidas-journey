package de.culturalgames.kwjourney.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.badlogic.gdx.pay.Offer;
import com.badlogic.gdx.pay.PurchaseManagerConfig;
import com.badlogic.gdx.pay.PurchaseObserver;
import com.badlogic.gdx.pay.Transaction;

import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.KawaidaHUD;
import de.culturalgames.kwjourney.api.VJXOffer.VJXOfferType;
import de.culturalgames.kwjourney.content.Secrets;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.save.KJCloudSave;
import de.culturalgames.kwjourney.ui.ShopMenu;
import de.culturalgames.kwjourney.ui.menus.MapMenu;
import de.culturalgames.kwjourney.ui.menus.ShopMenuBasic;
import de.culturalgames.kwjourney.ui.widgets.ShopButton;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.vjx.ui.EJXMenuTable;

public class PurchaseHandler implements PurchaseObserver {

    public static final String OFFER_SKIN = "offer_skin_";

    public enum ShopItem {
        // normal
        BANANA_SLOT("offer_banana_slot"),
        COCO_SLOT("offer_coconut_slot"),
        KARAFUU_DUR("offer_karafuu_duration"),
        SOLAR_DUR("offer_solarPanel_duration"), SOLAR_RANGE("offer_solarPanel_range"),
        TOTEM("offer_totem"),

        // premium
        SUN_S("offer_sun_s"),
        SUN_M("offer_sun_m"),
        SUN_L("offer_sun_l"),
        SUN_DOUBLE("offer_sun_double"),
        TOTEM_SLOT("offer_totem_slot"),
        UNLOCK_CHAPTER("offer_unlock_chapter"),
        UNLOCK_ALL("offer_unlock_all"),

        TOTEM_SLOT_1("offer_totem_slot_1"),
        TOTEM_SLOT_2("offer_totem_slot_2"),
        UNLOCK_CHAPTER_1("offer_unlock_chapter_1"),
        UNLOCK_CHAPTER_2("offer_unlock_chapter_2"),
        UNLOCK_CHAPTER_3("offer_unlock_chapter_3"),
        UNLOCK_CHAPTER_4("offer_unlock_chapter_4"),

        // skins
        SKIN_BURKA("offer_skin_burkaMonkey0"),
        SKIN_FIRE("offer_skin_fireFighterMonkey0"),
        SKIN_GRADUATE("offer_skin_graduateMonkey0"),
        SKIN_HIPHOP("offer_skin_hipHopMonkey0"),
        SKIN_HOLY("offer_skin_holyMonkey0"),
        SKIN_KANGA("offer_skin_kangaMonkey0"),
        SKIN_KANZU("offer_skin_kanzuMonkey0"),
        SKIN_MASSAI("offer_skin_massaiMonkey0"),
        SKIN_RASTA("offer_skin_rastaMonkey0"),
        SKIN_SOCCER_GREEN("offer_skin_soccerMonkey0_green"),
        SKIN_SOCCER_RED("offer_skin_soccerMonkey0_red"),
        SKIN_ROYAL("offer_skin_royalMonkey0"),
        SKIN_SAILOR("offer_skin_sailorMonkey0")
        ;

        public final String name;

        private ShopItem(String name) {
            this.name = name;
        }

        public static final ShopItem get(String name) {
            for (ShopItem item : ShopItem.values())
                if (name.equals(item.name)) return item;
            VJXLogger.log(LogCategory.ERROR, "ShopItem '" + name + "' not found !", 1);
            return null;
        }
    }

    private KJGame game;
    private KJPlayer player;
    private MenuStage menu;

    private PurchaseManagerConfig config;

    private HashMap<String, VJXOffer> offers;

    private boolean buyingAll;

    public PurchaseHandler(KJGame game) {
        this.game = game;
        player = (KJPlayer) game.getPlayer();

        config = new PurchaseManagerConfig();
        menu = game.getScreen().getMenu();

        // add stores
        // IOS_APPLE doesn't actually have an encoded key
        // so pass any string as the second parameter
        //        config.addStoreParam(PurchaseManagerConfig.STORE_NAME_ANDROID_GOOGLE,
        //                        "<Google key>");
        //        config.addStoreParam(PurchaseManagerConfig.STORE_NAME_IOS_APPLE,
        //                        "anyString");
    }

    public void loadOffers() {
        LocalSavegame savegame = game.getSave();
        offers = APILoader.getOffers();
        String saveString;
        for (VJXOffer offer : offers.values()) {
            saveString = offer.getIdentifier() + "_amount";
            offer.setAmount(savegame.getInt(saveString, offer.getMaxAmount()));

            saveString = offer.getIdentifier() + "_level";
            offer.setLevel(savegame.getInt(saveString, offer.getLevel()));

            if (offer.isPremium()) {
                if (offer.hasLevels()) {
                    Offer o;
                    String identifier;
                    for (int i = 1; i <= offer.getMaxLevel(); i++) {
                        identifier = offer.getIdentifier() + VJXString.SEP_USCORE + i;
                        o = new Offer();
                        o.setIdentifier(identifier);
                        o.setType(offer.getType());
                        config.addOffer(o);
                    }
                } else {
                    config.addOffer(offer);
                }
            }
        }
    }

    public void resetShop() {
        if (offers == null) return;
        for (VJXOffer o : offers.values()) {
            o.reset();
        }
    }

    public HashMap<String, VJXOffer> getOffers() {
        return offers;
    }

    public VJXOffer getOffer(String name) {
        return offers.get(name);
    }

    public int getPurchasedAmount(VJXOffer offer) {
        LocalSavegame savegame = game.getSave();
        return savegame.getInt(offer.getIdentifier() + "_amount", 0);
    }

    public PurchaseManagerConfig getConfig() {
        return config;
    }

    private void execPurchase(String identifier) {
        final VJXOffer offer = APILoader.getOffer(identifier, true);
        if (offer == null || offer.getAmount() == 0 || offer.isReward()) return;
        offer.setAmount(offer.getAmount() - 1);

        int price = offer.getIntPrice();
        if (offer.getOfferType() == VJXOfferType.UPGRADE) {
            if (offer.isMaxLevel()) return;
            offer.incrLvl();
        }
        ShopItem item = ShopItem.get(offer.getIdentifier());
        if (item == null) return;
        switch (item) {
            case BANANA_SLOT:
                player.incr(ItemCategory.POTION, true);
                menu.getUI(KawaidaHUD.class).addBananaSlot();
                break;
            case COCO_SLOT:
                player.incr(ItemCategory.AMMO,
                                (int) offer.getValue(), true);
                break;
            case TOTEM:
                player.incr(ItemCategory.CONTINUE);
                menu.getCurrentMenu().entered(game, menu);
                break;
            case TOTEM_SLOT:
            case TOTEM_SLOT_1:
            case TOTEM_SLOT_2:
                player.incr(ItemCategory.CONTINUE, true);
                break;
            case KARAFUU_DUR:
                player.setGodDuration(player.getGodDuration()
                                + offer.getValue());
                break;
            case SOLAR_DUR:
                player.setMagnetDuration(player.getMagnetDuration()
                                + offer.getValue());
                break;
            case SOLAR_RANGE:
                player.setMagnetRange(player.getMagnetRange()
                                + offer.getValue());
                break;
            case SKIN_BURKA:
            case SKIN_FIRE:
            case SKIN_GRADUATE:
            case SKIN_SOCCER_GREEN:
            case SKIN_HIPHOP:
            case SKIN_HOLY:
            case SKIN_KANGA:
            case SKIN_KANZU:
            case SKIN_MASSAI:
            case SKIN_SOCCER_RED:
            case SKIN_RASTA:
            case SKIN_ROYAL:
            case SKIN_SAILOR:
                String skin = offer.getSkin();
                player.addSkin(skin);
                if (!offer.getSkin().isEmpty()) {
                    if (!player.getSkin().isEmpty() && !player.getSkin()
                                    .equals(KJPlayer.dflSkin)) {

                        offers.get(OFFER_SKIN + player.getSkin()).setActive(false);
                    }

                    if (!buyingAll) {
                        player.setSkin(offer.getSkin());
                        offer.setActive(true);
                    }
                }
                break;
            case SUN_S:
            case SUN_M:
            case SUN_L:
                player.incr(ItemCategory.COIN,
                                (int) offer.getValue());
                break;
            case SUN_DOUBLE:
                player.setCoinMultiply(2);
                break;
            case UNLOCK_CHAPTER:
            case UNLOCK_CHAPTER_1:
            case UNLOCK_CHAPTER_2:
            case UNLOCK_CHAPTER_3:
            case UNLOCK_CHAPTER_4:
                MapMenu mapMenu = (MapMenu) menu.getMenu(MENU.MAP.id);
                mapMenu.unlockChapter((KJCloudSave) game.getSave());
                //                if (!buyingAll) {
                //                    mapMenu.showFireworks();
                //                    game.getScreen().getMenu(KJMenu.class).switchMenu(MENU.MAP, true);
                //                }
                break;
            case UNLOCK_ALL:
                buyAll(game);
                break;
            default:
                VJXLogger.log(LogCategory.ERROR,
                                "PurchaseHandler - Unknown item purchase: "
                                                + identifier);
                break;
        }
        KJCloudSave savegame = (KJCloudSave) game.getSave();
        savegame.saveOffer(offer);

        if (!buyingAll) {
            if (!offer.isPremium())
                player.setCoins(player.getCoins() - price);

            game.getSave().savePlayer(player, true);

            if (item == ShopItem.UNLOCK_ALL
                            || item == ShopItem.UNLOCK_CHAPTER
                            || item == ShopItem.UNLOCK_CHAPTER_1
                            || item == ShopItem.UNLOCK_CHAPTER_2
                            || item == ShopItem.UNLOCK_CHAPTER_3
                            || item == ShopItem.UNLOCK_CHAPTER_4) {
                MapMenu mapMenu = (MapMenu) menu.getMenu(MENU.MAP.id);
                mapMenu.showFireworks();
                menu.clearHistory();
                menu.switchMenu(MENU.START.id);
                menu.switchMenu(MENU.MAP.id);
            } else {

                menu.getCurrentMenu().entered(game, menu);
            }
        }

        EJXMenuTable shopT = menu.getMenu(menu.getCurrentMenuId());
        if (shopT instanceof ShopMenu) {
            ShopMenu shop = (ShopMenu) shopT;
            shop.updateSuns();
        }
    }

    private void buyAll(EJXGame game) {
        buyingAll = true;
        List<Secret> secrets = new ArrayList<>(Secrets.getSecrets());
        ((KJCloudSave) game.getSave()).saveSecrets(secrets);

        for (VJXOffer offer : offers.values()) {
            switch (offer.getOfferType()) {
                case UPGRADE:
                    for (int i = 0; i < offer.getMaxAmount(); i++) {
                        execPurchase(offer.getIdentifier());
                    }
                    break;
                case SKIN:
                    if (!offer.isReward()) execPurchase(offer.getIdentifier());
                    else {
                        ShopMenu shop = (ShopMenu) menu.getMenu(MENU.SHOP_SKINS.id);
                        ShopButton shopButton = shop.getOfferButtons()
                                        .get(offer.getIdentifier());
                        if (shopButton.getLockIcon().isLocked()) {
                            ((KJMenu) menu).unlock(shopButton,
                                            shopButton.getLockIcon());
                        }
                    }
                    break;
                case ITEM:
                    execPurchase(offer.getIdentifier());
                    break;
                case CONSUMABLE:
                    break;
                default:
                    break;
            }
        }
        buyingAll = false;
    }

    @Override
    public void handleInstall() {
        VJXLogger.log(LogCategory.INFO,
                        "PurchaseHandler - Install successfull.");
        game.getScreen().getMenu().showWaiting(false);

        ShopMenuBasic shop = (ShopMenuBasic) menu.getMenu(MENU.SHOP_SUNS.id);
        shop.setPrimeAvailable(true);
        game.getPurchaseManager().purchaseRestore();
    }

    @Override
    public void handleInstallError(Throwable e) {
        VJXLogger.log(LogCategory.INFO,
                        "PurchaseHandler - Install error: " + e.getMessage());
        game.getScreen().getMenu().showWaiting(false);

        menu.showMessageToUser(TextControl.getInstance()
                        .get(KJTexts.INFO_PURCHASE_INSTALL_FAILED_TITLE),
                        TextControl.getInstance().get(
                                        KJTexts.INFO_PURCHASE_INSTALL_FAILED_TEXT));
        ShopMenuBasic shop = (ShopMenuBasic) menu.getMenu(MENU.SHOP_SUNS.id);
        shop.setPrimeAvailable(false);
    }

    @Override
    public void handleRestore(Transaction[] transactions) {
        VJXLogger.log(LogCategory.INFO, "PurchaseHandler - Handle restores: ");
        game.getSave().setSkipWrite(true);
        game.getScreen().getMenu().showWaiting(false);

        String name;
        for (Transaction transaction : transactions) {
            name = transaction.getIdentifier();
            VJXLogger.log(LogCategory.INFO, "    Handle: " + name);
            execPurchase(name);
        }

        game.initSave();
    }

    @Override
    public void handleRestoreError(Throwable e) {
        VJXLogger.log(LogCategory.INFO, "PurchaseHandler - Purchase restore error: "
                        + e.getMessage());
        game.getScreen().getMenu().showWaiting(false);
        game.initSave();
    }

    @Override
    public void handlePurchase(Transaction transaction) {
        game.getScreen().getMenu().showWaiting(false);
        execPurchase(transaction.getIdentifier());
        menu.showMessageToUser(TextControl.getInstance().get(KJTexts.INFO_PURCHASE_SUCCESS_TITLE),
                        TextControl.getInstance().get(KJTexts.INFO_PURCHASE_SUCCESS_TEXT));
    }

    @Override
    public void handlePurchaseError(Throwable e) {
        game.getScreen().getMenu().showWaiting(false);
        VJXLogger.log(LogCategory.INFO, "PurchaseHandler - Purchase error: " + e.getMessage());
        String msg[] = e.getMessage().split(":");
        if (msg.length > 1)
            menu.showMessageToUser(TextControl.getInstance().get(KJTexts.INFO_PURCHASE_ERROR_TITLE),
                            TextControl.getInstance().get(
                                            KJTexts.INFO_PURCHASE_ERROR_TEXT)
                            + "\n" + msg[0] + "\n" + msg[1]);
        else {
            msg = e.getMessage().split(" with ");
            menu.showMessageToUser(
                            TextControl.getInstance()
                            .get(KJTexts.INFO_PURCHASE_ERROR_TITLE),
                            TextControl.getInstance()
                            .get(KJTexts.INFO_PURCHASE_ERROR_TEXT) + "\n"
                            + e.getMessage());
        }

        ShopMenuBasic shop = (ShopMenuBasic) menu.getMenu(MENU.SHOP_SUNS.id);
        shop.setPrimeAvailable(false);
    }

    @Override
    public void handlePurchaseCanceled() {
        game.getScreen().getMenu().showWaiting(false);
        VJXLogger.log(LogCategory.INFO, "PurchaseHandler - Purchase cancelled.");
        menu.showMessageToUser(TextControl.getInstance().get(KJTexts.INFO_PURCHASE_CANCELLED_TITLE),
                        TextControl.getInstance().get(KJTexts.INFO_PURCHASE_CANCELLED_TEXT));
    }
}