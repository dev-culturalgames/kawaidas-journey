package de.culturalgames.kwjourney;

import java.util.HashMap;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;

import de.culturalgames.kwjourney.KJGame.GameStats;
import de.culturalgames.kwjourney.KawaidaHUD.UIElement;
import de.culturalgames.kwjourney.api.APILoader;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.cmds.HintCommand;
import de.culturalgames.kwjourney.cmds.StoryCommand;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.culturalgames.kwjourney.entity.logics.DialogLogic.Dialog;
import de.culturalgames.kwjourney.entity.logics.HintLogic.Hint;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.ui.menus.CreditsMenu;
import de.culturalgames.kwjourney.ui.menus.EmptyMenu;
import de.culturalgames.kwjourney.ui.menus.EncyMenu;
import de.culturalgames.kwjourney.ui.menus.EndMenu;
import de.culturalgames.kwjourney.ui.menus.LoadingMenu;
import de.culturalgames.kwjourney.ui.menus.MapMenu;
import de.culturalgames.kwjourney.ui.menus.MapMenu.MAPS;
import de.culturalgames.kwjourney.ui.menus.ProjectsMenu;
import de.culturalgames.kwjourney.ui.menus.SettingsMenu;
import de.culturalgames.kwjourney.ui.menus.ShopMenuBasic;
import de.culturalgames.kwjourney.ui.menus.ShopMenuPrime;
import de.culturalgames.kwjourney.ui.menus.ShopMenuSkins;
import de.culturalgames.kwjourney.ui.menus.StartMenu;
import de.culturalgames.kwjourney.ui.menus.VideoMenu;
import de.culturalgames.kwjourney.ui.popups.BuyLosePopup;
import de.culturalgames.kwjourney.ui.popups.BuyPopup;
import de.culturalgames.kwjourney.ui.popups.ConfirmPopup;
import de.culturalgames.kwjourney.ui.popups.EmptyPopup;
import de.culturalgames.kwjourney.ui.popups.HelpPopup;
import de.culturalgames.kwjourney.ui.popups.HintPopup;
import de.culturalgames.kwjourney.ui.popups.InfoPopup;
import de.culturalgames.kwjourney.ui.popups.LosePopup;
import de.culturalgames.kwjourney.ui.popups.PausePopup;
import de.culturalgames.kwjourney.ui.popups.RewardPopup;
import de.culturalgames.kwjourney.ui.popups.SecretPopup;
import de.culturalgames.kwjourney.ui.popups.StoryPopup;
import de.culturalgames.kwjourney.ui.popups.WinPopup;
import de.culturalgames.kwjourney.ui.widgets.KJIconTextButton;
import de.culturalgames.kwjourney.ui.widgets.LockIcon;
import de.culturalgames.kwjourney.ui.widgets.ShopButton;
import de.culturalgames.kwjourney.util.KJTexts;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EntityCallbacks.VJXCallback;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTresor;
import de.venjinx.ejx.util.EJXTresor.Lockable;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.AnimationType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.vjx.ui.EJXMenuTable;
import de.venjinx.vjx.ui.EJXSkin;

public class KJMenu extends MenuStage {

    private static final String url = "url";

    private static int MENU_IND, POPUP_IND;

    public enum MENU {
        NONE, START, END, CREDITS, SETT, MAP, PROJ, ENCY, SHOP_SUNS, SHOP_PRIME, SHOP_SKINS, ENCYSUB,
        GAME, LOADING, VIDEO;

        public final int id;

        private MENU() {
            id = MENU_IND++;
        }
    };

    public enum POPUP {
        NONE, INFO, CONFIRM, REWARD, PROJ, HELP, BUY, PAUSE, SECRET, STORY,
        HINT, WIN, LOSE, BUY_LOSE;

        public final int id;

        private POPUP() {
            id = POPUP_IND++;
        }
    };

    private KawaidaHUD kjHUD;
    private KJLevel level;

    private Touchpad tp;

    private EncyMenu encyMenu;

    public TiledMap mapMenuTZ;
    public TiledMap mapMenuST;
    public TiledMap mapMenuDAR;
    public TiledMap mapShop;
    public TiledMap mapPrimeShop;
    public TiledMap mapSkinShop;
    private MapMenu mapMenu;

    private Button closeButton;
    private Button closeVideoButton;
    private KJIconTextButton watchAdButton;
    private boolean endDialogue;

    private HelpPopup helpPopup;
    private HelpPopup projPopup;
    private SecretPopup secretPopup;
    private HintPopup hintPopup;
    private StoryPopup storyPopup;
    private LosePopup losePopup;
    private BuyPopup buyPopup;

    private Image waitIcon;
    private Drawable loadingDrawable;

    private float fullRotationTime = 1f;
    private float rotateStep = 360f / fullRotationTime;
    private float rotation = 0;

    private SpineActor spActor;

    private Vector2 tmpVec;
    private boolean loaded;

    private SettingsMenu settings;
    private Runnable onClose;

    private EJXTresor tresor;

    public KJMenu(EJXGame game) {
        super(game, "kawaida_menu_stage");
        initResources();
        tmpVec = new Vector2();
        spActor = new SpineActor("hint_spine_actor");
        spActor.setSize(256, 256);

        LoadingMenu loadingScreen = new LoadingMenu(this);
        menus.put(MENU.LOADING.id, loadingScreen);
    }

    public void lock(Group actor, LockIcon icon) {
        if (!(actor instanceof Lockable)) return;
        icon.setLocked(true);
        tresor.lock((Lockable) actor, icon.getName());

        if (icon.getUnlockedListener() != null)
            actor.removeListener(icon.getUnlockedListener());

        actor.addActor(icon);

        if (icon.getLockedListener() != null)
            actor.addListener(icon.getLockedListener());
    }

    public void unlock(Group actor, LockIcon icon) {
        unlock(actor, icon, 0f);
    }

    public void unlock(Group actor, LockIcon icon, float delay) {
        if (!(actor instanceof Lockable)) return;
        icon.setLocked(false, delay);
        tresor.unlock((Lockable) actor, icon.getName());

        if (icon.getLockedListener() != null)
            actor.removeListener(icon.getLockedListener());

        if (icon.getUnlockedListener() != null)
            actor.addListener(icon.getUnlockedListener());
    }

    public boolean isLocked(String saveName) {
        return tresor.isLocked(saveName);
    }

    @Override
    protected void userInit() {
        level = (KJLevel) game.getScreen().getLevel();
        tresor = new EJXTresor(game.getSave());

        initSkin();

        closeButton = EJXSkin.createTButton(KJAssets.BUTTON_CLOSE, skin);
        closeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.sfx.stopVoiceOver();
                game.sfx.playDynSFX(KJAssets.SFX_BACK);
                if (endDialogue)
                    hidePopup(currentPopup != getPopup(POPUP.STORY.id));
                else currentPopup.exited(game, KJMenu.this);
            }
        });

        closeVideoButton = EJXSkin.createTButton(KJAssets.BUTTON_CLOSE, skin);
        closeVideoButton.setPosition(getWidth() - 153, getHeight() - 128);
        closeVideoButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                game.sfx.playDynSFX(KJAssets.SFX_BACK);
                if (onClose != null) onClose.run();
            }
        });

        //        ClickListener watchAdListener = new ClickListener() {
        //            @Override
        //            public void clicked(InputEvent event, float x, float y) {
        //                super.clicked(event, x, y);
        //                game.sfx.playDynSFX(KJAssets.SFX_CLICK);
        //                if (getCurrentMenuId() == MENU.SETT.id) {
        //                    ((KJGame) game).getAdControl().showAd(KJString.AD_test);
        //                }
        //                else
        //                    if (getCurrentPopupId() == POPUP.WIN.id)
        //                        ((KJGame) game).getAdControl().showAd(KJString.AD_double_suns);
        //                    else
        //                        ((KJGame) game).getAdControl().showAd(KJString.AD_continue);
        //                watchAdButton.setEnabled(false);
        //            }
        //        };

        watchAdButton = new KJIconTextButton(KJAssets.BUTTON_YELLOW, VJXString.STR_EMPTY,
                        skin, skin.getDrawable(KJAssets.ICON_EMBLEM_BG));
        watchAdButton.getIcon().setFont(skin.get(EJXFont.TITLE_26, LabelStyle.class));
        watchAdButton.alignIcon(Align.topRight);
        watchAdButton.setIconOffset(16, 32);
        //        watchAdButton.addListener(watchAdListener);

        initMenus();
        initPopups();

        popupBG = new Image(skin.getDrawable(KJAssets.BUTTON_BG_TRANS_80));
        popupBG.setFillParent(true);
        popupBG.setTouchable(Touchable.disabled);

        kjHUD = getUI(KawaidaHUD.class);
        tp = (Touchpad) kjHUD.getUIElement(UIElement.JOYSTICK);

        loadingDrawable = skin.getDrawable(KJAssets.ICON_LOADING);
        waitIcon = new Image(loadingDrawable);
        waitIcon.setPosition(getWidth() - (waitIcon.getWidth() + 25), 25);
        waitIcon.setOrigin(waitIcon.getWidth() / 2f, waitIcon.getHeight() / 2f);
        waitIcon.setTouchable(Touchable.disabled);

        loaded = true;
    }

    public KJIconTextButton getWatchAdButton() {
        return watchAdButton;
    }

    @Override
    public void setUserInterface(HashMap<String, TiledMap> menus, Skin skin) {
        mapMenuTZ = menus.get("map_tz");
        mapMenuST = menus.get("map_st");
        mapMenuDAR = menus.get("map_dar");
        mapShop = menus.get("shop");
        mapPrimeShop = menus.get("shop_prime");
        mapSkinShop = menus.get("shop_skins");

        super.setUserInterface(menus, skin);
    }

    @Override
    protected void preAct(float delta) {
    }


    @Override
    protected void postAct(float delta) {
        rotation -= rotateStep * delta;

        if (waitIcon != null) waitIcon.setRotation(rotation);
    }

    @Override
    protected void preDraw() {
        getBatch().setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    }

    @Override
    protected void postDraw() {

    }

    @Override
    public void dispose() {
        if (skin != null)
            skin.dispose();
        super.dispose();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer,
                    int button) {
        if (!loaded)
            return false;

        tmpVec.set(screenX, screenY);
        getViewport().unproject(tmpVec);
        VJXLogger.log(LogCategory.INPUT,
                        "Menu touch down: orig (" + screenX + ", "
                                        + screenY + "), ui (" + tmpVec + ")");
        if (game.getScreen().getInputControl().leftPointer(pointer)) {
            float x = Math.min(Math.max(25, tmpVec.x - tp.getWidth() / 2),
                            960 - tp.getWidth() - 100);
            float y = Math.min(Math.max(25, tmpVec.y - tp.getHeight() / 2),
                            1080 - tp.getHeight() - 25);
            tp.setPosition(x, y);
        }
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (!loaded)
            return false;
        tmpVec.set(screenX, screenY);
        getViewport().unproject(tmpVec);
        VJXLogger.log(LogCategory.INPUT,
                        "Menu touch up: orig (" + screenX + ", "
                                        + screenY + "), ui (" + tmpVec + ")");
        if (game.getScreen().getInputControl().leftPointer(-1)) {
            tp.setPosition(50, 50);
        }
        return super.touchUp(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (!loaded)
            return false;
        tmpVec.set(screenX, screenY);
        getViewport().unproject(tmpVec);
        VJXLogger.log(LogCategory.INPUT, "Menu dragged: orig (" + screenX + ", "
                        + screenY + "), ui (" + tmpVec + ")");
        return super.touchDragged(screenX, screenY, pointer);
    }

    public void showInfo(String heading, String text) {
        InfoPopup confirm = (InfoPopup) popups.get(POPUP.INFO.id);
        confirm.setTitle(heading);
        confirm.setText(text);
        showPopup(POPUP.INFO.id);
    }

    public void showConfirm(String title, String text, Runnable onConfirm,
                    Runnable onCancel) {
        ConfirmPopup confirm = (ConfirmPopup) popups.get(POPUP.CONFIRM.id);
        confirm.setTitle(title);
        confirm.setText(text);
        confirm.setOnConfirm(onConfirm);
        confirm.setOnCancel(onCancel);
        showPopup(POPUP.CONFIRM.id);
    }

    @Override
    public void showWaiting(boolean show) {
        waitIcon.setRotation(0);
        if (show) addActor(waitIcon);
        else waitIcon.remove();
    }

    public void switchMenu(MENU menuIndex) {
        switchMenu(menuIndex.id, fadeUI, fadeUI);
    }

    //    public void switchMenu(MENU menuIndex, boolean clearHistory) {
    //        switchMenu(menuIndex.id, fadeUI, fadeUI, clearHistory);
    //    }

    public void switchMenu(MENU menuIndex, float fadeOutDuration,
                    float fadeInDuration) {
        switchMenu(menuIndex.id, fadeOutDuration, fadeInDuration);
    }

    public void showHelp(String name) {
        showHelp(name, null);
    }

    public void showHelp(String name, String url) {
        helpPopup.setHelp(KJTexts.HELP_ + name, url);
        showPopup(getPopup(POPUP.HELP.id), getWidth() - 153, getHeight() - 128,
                        null, true);
    }

    public void showProject(String name, MapProperties props) {
        projPopup.setHelp(KJTexts.PROJECT_ + name, props.get(url, String.class));
        showPopup(getPopup(POPUP.PROJ.id), getWidth() - 153, getHeight() - 128,
                        null, true);
    }

    public void showSecret(Secret s, EJXEntity triggerObject) {
        secretPopup.setSecret(s, triggerObject);
        showPopup(getPopup(POPUP.SECRET.id), getWidth() - 153, getHeight() - 153, null, true);
    }

    public void showDialog(Dialog dialog) {
        storyPopup.initStory(dialog);
        showPopup(getPopup(POPUP.STORY.id), getWidth() - 153, getHeight() - 153, null, false);
    }

    public void showHint(Hint hint) {
        String name = hint.getTargetName();
        if (name != null) {
            if (game.getSave().getBool(GameStats.HINTS.name + name, false)
                            && !hint.showAllways()) {
                kjHUD.enableUIElement(name, true);
                kjHUD.toggleUIElement(name, true);
                hint.getObject().onEnd();
                return;
            }

            game.gameControl.getPlayerInput().setIngameControl(false);
            Actor actor;
            EJXEntity e = level.getEntityByName(name);
            if (e != null) {
                if (e.getDef().getAnimationType() == AnimationType.FRAME
                                || e.getDef().getAnimationType() == AnimationType.FBF) {
                    actor = e.getActor();
                } else {
                    spActor.setEntity((AnimatedEntity) e);
                    spActor.setUserObject(e);
                    actor = spActor;
                }
                hint.setTarget(actor);
            } else {
                if (name.contains("context")) {
                    String[] split = name.split(VJXString.SEP_USCORE);
                    name = split[0];
                    ActionContext context = ActionContext.get(split[1]);
                    if (context != null)
                        kjHUD.showContextActionBtn(context, true, null, true);
                }
                if (name.contains("progress")) {
                    String[] split = name.split(VJXString.SEP_USCORE);
                    name = split[0];
                }
                actor = kjHUD.getUIElement(UIElement.get(name));
                hint.setTarget(actor, actor.isTouchable());
                actor.setTouchable(Touchable.disabled);
            }
        }
        hintPopup.setHint(hint);

        Command[] endCmds = hint.getObject()
                        .getCallbackCommands(VJXCallback.onEnd);
        if (endCmds != null) {
            for (Command command : endCmds) {
                if (command instanceof HintCommand
                                || command instanceof StoryCommand) {
                    endDialogue = false;
                    break;
                }
            }
        }

        showPopup(getPopup(POPUP.HINT.id), getWidth() - 153, getHeight() - 153,
                        hint.getHide(), true);
    }

    @Override
    public void showMessageToUser(String title, String msg) {
        InfoPopup info = (InfoPopup) game.getScreen().getMenu().getPopup(POPUP.INFO.id);
        info.setTitle(title);
        info.setText(msg);
        info.resetButtonText();
        info.setOnConfirm(null);
        showPopup(POPUP.INFO.id, true);
    }

    public void showReward(String reward, int amount) {
        showReward(reward, amount, null);
    }

    public void showReward(String reward, int amount, LockIcon lock) {
        KJPlayer player = (KJPlayer) game.getPlayer();
        Drawable d = null;
        switch (reward) {
            case KJString.REWARD_test:
                d = skin.getDrawable(KJAssets.ICON_DEBUG);
                break;
            case KJString.REWARD_coco:
                player.incr(ItemCategory.AMMO, amount);
                d = skin.getDrawable(KJAssets.ICON_COCO);
                break;
            case KJString.REWARD_banana:
                player.incr(ItemCategory.POTION, amount);
                d = skin.getDrawable(KJAssets.ICON_BANANA);
                break;
            case KJString.REWARD_totem:
                player.incr(ItemCategory.CONTINUE, amount);
                d = skin.getDrawable(KJAssets.ICON_TOTEM);
                break;
            case KJString.REWARD_sun:
                player.incr(ItemCategory.COIN, amount);
                d = skin.getDrawable(KJAssets.ICON_SUN);
                break;
        }
        if (reward.contains("skin")) {
            VJXOffer offer = APILoader.getOffer(reward);
            d = skin.getDrawable(offer.getDrawableName());
        }
        RewardPopup popup = (RewardPopup) getPopup(POPUP.REWARD.id);
        if (d != null) {
            popup.setRewardDrawable(d, amount);
            popup.setLockIcon(lock);
            showPopup(POPUP.REWARD.id);
        }
    }

    @Override
    public void hidePopup(Runnable onClosed, boolean bounceOut) {
        super.hidePopup(onClosed, bounceOut);
        closeButton.remove();
        endDialogue = true;
    }

    private void showPopup(EJXMenuTable popupTable, float x, float y, String hide,
                    boolean bounceIn) {
        showPopup(popupTable, bounceIn, hide);
        closeButton.setPosition(x, y);
        addActor(closeButton);
        endDialogue = true;
    }

    private void initResources() {
        // background images
        resources.put(KJAssets.IMAGE_KJ_LOGO, new AssetDescriptor<>(
                        KJAssets.PATH_IMAGE_KJ_LOGO, Texture.class));

        resources.put(KJAssets.IMAGE_MENU_START, new AssetDescriptor<>(
                        KJAssets.PATH_IMAGE_MENU_START, Texture.class));

        resources.put(KJAssets.IMAGE_MAP_EASTAFRICA, new AssetDescriptor<>(
                        KJAssets.PATH_IMAGE_MAP_EASTAFRICA, Texture.class));

        resources.put(KJAssets.IMAGE_CREDITS0, new AssetDescriptor<>(
                        KJAssets.PATH_IMAGE_CREDITS0, Texture.class));

        resources.put(KJAssets.IMAGE_CREDITS1, new AssetDescriptor<>(
                        KJAssets.PATH_IMAGE_CREDITS1, Texture.class));

        resources.put(KJAssets.ATLAS_GLOBAL_UI, new AssetDescriptor<>(
                        KJAssets.PATH_ATLAS_GLOBAL_UI, TextureAtlas.class));

        resources.put(KJAssets.ATLAS_GLOBAL_HUD, new AssetDescriptor<>(
                        KJAssets.PATH_ATLAS_GLOBAL_HUD, TextureAtlas.class));

        resources.put(KJAssets.ATLAS_SHOP_CHARACTER, new AssetDescriptor<>(
                        KJAssets.PATH_ATLAS_SHOP_CHARACTER, TextureAtlas.class));
    }

    private void initSkin() {
        skin.addRegions(game.assets.getTextureAtlas(KJAssets.ATLAS_GLOBAL_UI));
        skin.addRegions(game.assets.getTextureAtlas(KJAssets.ATLAS_GLOBAL_HUD));
        ((KJSkin) skin).init();
        ((LoadingMenu) getMenu(MENU.LOADING.id)).initLevelLoadUI(skin);
    }

    @Override
    public void initMenus() {
        super.initMenus();
        menus.put(MENU.NONE.id, new EmptyMenu(this));

        hud = new KawaidaHUD(this);
        menus.put(MENU.GAME.id, hud);

        menus.put(MENU.START.id, new StartMenu(this));

        menus.put(MENU.END.id,new EndMenu(this));

        menus.put(MENU.CREDITS.id, new CreditsMenu(this));

        mapMenu = new MapMenu(this);
        menus.put(MENU.MAP.id, mapMenu);
        mapMenu.setMap(MAPS.TZ);

        settings = new SettingsMenu(this);
        menus.put(MENU.SETT.id, settings);

        menus.put(MENU.PROJ.id, new ProjectsMenu(this));

        encyMenu = new EncyMenu(this);
        menus.put(MENU.ENCY.id, encyMenu);
        menus.put(MENU.ENCYSUB.id, encyMenu.getSubMenu());

        menus.put(MENU.SHOP_SUNS.id, new ShopMenuBasic(this));
        menus.put(MENU.SHOP_SKINS.id, new ShopMenuSkins(this));
        menus.put(MENU.SHOP_PRIME.id, new ShopMenuPrime(this));

        menus.put(MENU.VIDEO.id, new VideoMenu(this));
    }

    private void initPopups() {
        popups.put(POPUP.NONE.id, new EmptyPopup(this));
        popups.put(POPUP.INFO.id, new InfoPopup(this));
        popups.put(POPUP.CONFIRM.id, new ConfirmPopup(this));
        popups.put(POPUP.REWARD.id, new RewardPopup(this));
        popups.put(POPUP.PAUSE.id, new PausePopup(this));

        projPopup = new HelpPopup(this);
        popups.put(POPUP.PROJ.id, projPopup);

        helpPopup = new HelpPopup(this);
        popups.put(POPUP.HELP.id, helpPopup);

        buyPopup = new BuyPopup(this);
        popups.put(POPUP.BUY.id, buyPopup);

        secretPopup = new SecretPopup(this);
        popups.put(POPUP.SECRET.id, secretPopup);

        storyPopup = new StoryPopup(this);
        popups.put(POPUP.STORY.id, storyPopup);

        hintPopup = new HintPopup(this);
        popups.put(POPUP.HINT.id, hintPopup);

        popups.put(POPUP.WIN.id, new WinPopup(this));

        losePopup = new LosePopup(this);
        popups.put(POPUP.LOSE.id, losePopup);

        popups.put(POPUP.BUY_LOSE.id, new BuyLosePopup(this));
    }

    public MapObject getObjectByName(String name) {
        return getObjectByName(name, false);
    }

    public MapObject getObjectByName(String name, boolean contains) {
        MapLayers layers = mapMenuTZ.getLayers();
        String objName;
        for (MapLayer layer : layers) {
            for (MapObject mObj : layer.getObjects()) {
                objName = mObj.getProperties().get(VJXString.name,
                                VJXString.STR_EMPTY, String.class);
                if (objName.equals(name)
                                || contains && objName.contains(name)) {
                    return mObj;
                }
            }
        }
        return null;
    }

    public Array<MapObject> getObjectsByName(String name,
                    Array<MapObject> targetArray, boolean contains) {
        MapLayers layers = mapMenuTZ.getLayers();
        String objName;
        for (MapLayer layer : layers) {
            for (MapObject mObj : layer.getObjects()) {
                objName = mObj.getProperties().get(VJXString.name,
                                VJXString.STR_EMPTY, String.class);
                if (objName.equals(name)
                                || contains && objName.contains(name)) {
                    targetArray.add(mObj);
                }
            }
        }
        return targetArray;
    }

    public Array<MapObject> getObjectsByLayer(String layerName,
                    Array<MapObject> targetArray, boolean contains) {
        MapLayers layers = mapMenuTZ.getLayers();
        for (MapLayer layer : layers) {
            if (layer.getName().equals(layerName) || contains
                            && layer.getName().contains(layerName)) {
                for (MapObject mObj : layer.getObjects()) {
                    targetArray.add(mObj);
                }
            }
        }
        return targetArray;
    }

    public Touchpad getTouchPad() {
        return tp;
    }

    public Button getJumpButton() {
        return (Button) kjHUD.getUIElement(UIElement.JUMP);
    }

    public boolean isDialogue(POPUP dialogue) {
        return currentMenu == menus.get(dialogue.id);
    }

    public void setContinueBlock(boolean cont) {
        losePopup.setContinueBlock(cont);
    }

    public void showBuyDialogue(ShopButton shopButton) {
        buyPopup.setOffer(shopButton, game.getPlayer(), skin);
        showPopup(POPUP.BUY.id);
    }

    public void setReturnMap(String mapName) {
        mapMenu.setReturnName(mapName);
    }

    public void showPartIndicator(EJXEntity e) {
        kjHUD.showPartIndicator(e);
    }

    public boolean canShowBtn(String name, ActionContext context) {
        return kjHUD.canShowContext(name, context);
    }
}