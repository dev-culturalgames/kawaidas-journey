package de.culturalgames.kwjourney;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.SnapshotArray;

import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.entity.logics.ContextLogic;
import de.culturalgames.kwjourney.entity.logics.InventoryLogic;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.save.KJPlayerStats.PlayerStats;
import de.culturalgames.kwjourney.ui.widgets.ContextButton;
import de.culturalgames.kwjourney.ui.widgets.KJIconTextButton;
import de.culturalgames.kwjourney.ui.widgets.ProgressBar;
import de.culturalgames.kwjourney.util.KJTexts;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXTypes.WinCondition;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.IngameHUD;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.VJXIcon;

public class KawaidaHUD extends IngameHUD {

    public enum UIElement {
        JOYSTICK("joystick"), BANANA("banana"), SUN("sun"), COCO("coconut"),
        TOTEM("totem"), JUMP("jump"), ACTION0("action0"), CONTEXT0("context0"),
        STATUS("status"), PROGRESS("progress"),
        CAM_INDICATOR("camIndicator"), SWIPE_DOWN("swipe_down"),
        SWIPE_LEFT_RIGHT("swipe_left_right");

        public final String name;

        private UIElement(String name) {
            this.name = name;
        }

        public static final UIElement get(String name) {
            for (UIElement element : UIElement.values())
                if (element.name.equals(name)) return element;
            return null;
        }
    }

    private final int maxImages = 10;

    private Image uiElementImage;
    private Touchpad joystick;
    private Button pauseBtn;
    private KJIconTextButton jumpBtn;
    private ContextButton contextBtn;

    private Table statusBar;
    private Cell<Table> bananaCell;
    private Cell<VJXIcon> sunCell;
    private VJXIcon sunsIcon, totemIcon, cocoIcon;
    private EJXLabel progReady;

    private Table bananas;
    private ProgressBar pBar;
    private Table progress;

    private List<Image> itemImages;
    private int curImgInd;
    private Image camIndicator;

    private Image partIndicator;

    private EJXEntity playerEntity;
    private KJLevel kjLevel;

    public KawaidaHUD(final MenuStage mc) {
        super(mc, "ui_ingameHUD");
        itemImages = new ArrayList<>();

        uiElementImage = new Image();
        uiElementImage.setSize(256, 256);

        pauseBtn = EJXSkin.createTButton(KJAssets.BUTTON_PAUSE, getSkin());
        pauseBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                menuControl.showPopup(POPUP.PAUSE.id, false);
            }
        });

        jumpBtn = new KJIconTextButton(KJAssets.BUTTON_BACKGROUND,
                        VJXString.STR_EMPTY, getSkin(),
                        getSkin().getDrawable(KJAssets.ICON_JUMP));
        jumpBtn.setName(UIElement.JUMP.name);
        jumpBtn.setPosition(mc.getWidth() - jumpBtn.getWidth() - 50, 50);
        jumpBtn.setSize(192, 192);
        mainTable.addActor(jumpBtn);

        contextBtn = new ContextButton(getSkin(), menuControl);
        contextBtn.setName(UIElement.CONTEXT0.name);
        contextBtn.setPosition(mc.getWidth() - 400, 20);
        mainTable.addActor(contextBtn);

        joystick = getSkin().get("ui_joystick", Touchpad.class);
        joystick.setName(UIElement.JOYSTICK.name);
        joystick.setPosition(50, 50);
        joystick.setTouchable(Touchable.enabled);
        mainTable.addActor(joystick);

        statusBar = new Table();
        statusBar.setName(UIElement.STATUS.name);

        bananas = new Table();
        bananas.setName("ui_bananas");

        cocoIcon = new VJXIcon("0", getSkin().getDrawable(KJAssets.ICON_COCO), getSkin());
        cocoIcon.setName(UIElement.COCO.name);

        sunsIcon = new VJXIcon("0", getSkin().getDrawable(KJAssets.ICON_SUN), getSkin());
        sunsIcon.setName(UIElement.SUN.name);

        fillStatusBar();

        totemIcon = new VJXIcon("0", getSkin().getDrawable(KJAssets.ICON_TOTEM), getSkin());
        totemIcon.setName(UIElement.TOTEM.name);

        progress = new Table();
        progress.setName(UIElement.PROGRESS.name);
        progress.setTransform(true);

        Drawable background = getSkin().getDrawable(KJAssets.ICON_LVL_PBAR_BG);
        Drawable fill = getSkin().getDrawable(KJAssets.ICON_LVL_PBAR_FILL);
        Drawable overlay = getSkin().getDrawable(KJAssets.ICON_LVL_PBAR_OVERLAY);
        pBar = new ProgressBar(mc, background, fill, overlay);
        pBar.showIcon(false);
        pBar.showMultiplier(false);

        progress.add(pBar);
        progress.pack();
        progress.setPosition(getWidth() / 2f - pBar.getWidth() / 2f, 50);
        mainTable.addActor(progress);

        progReady = new EJXLabel(getSkin(), TextControl.getInstance().get(KJTexts.HUD_READY_LABEL));
        progReady.setPosition(getWidth() / 2f - progReady.getWidth() / 2f, 128);
        mainTable.addActor(progReady);

        partIndicator = new Image();
        partIndicator.setVisible(false);
        progress.addActor(partIndicator);

        top = new Table();

        top.add(statusBar).expand().left().top();
        top.add(pauseBtn).top().right();

        mainTable.add(top).expand().fill().top().pad(20, 25, 20, 25);
        mainTable.row();

        camIndicator = new Image(getSkin().getDrawable(KJAssets.ICON_SWIPE_DOWN));
        camIndicator.pack();
        camIndicator.setPosition(
                        mc.getWidth() / 2 - camIndicator.getWidth() / 2, 50);
        mainTable.addActor(camIndicator);
        camIndicator.setTouchable(Touchable.disabled);
        camIndicator.setVisible(false);

        kjLevel = (KJLevel) level;
    }

    public void fillStatusBar() {
        statusBar.clear();
        bananas.clear();
        Image img;
        int n = menuControl.getGame().getSave().getInt(PlayerStats.POTIONS_MAX.name, 1);
        for (int i = 0; i < n; i++) {
            img = new Image(getSkin().getDrawable(KJAssets.ICON_BANANA));
            img.setName("ui_img_banana" + i);
            img.setVisible(false);
            img.setColor(Color.GRAY);
            bananas.add(img).pad(0, 5, 0, 5);
        }
        bananaCell = statusBar.add(bananas);

        statusBar.add(cocoIcon).pad(0, 5, 0, 5).size(70);

        sunCell = statusBar.add(sunsIcon).pad(0, 5, 0, 5);
    }

    public Actor getUIElement(UIElement element) {
        switch (element) {
            case JUMP:
                return jumpBtn;
            case CONTEXT0:
                return contextBtn;
            case BANANA:
                return bananas;
            case JOYSTICK:
                return joystick;
            case PROGRESS:
                return progress;
            case SUN:
                return sunsIcon;
            case COCO:
                return cocoIcon;
            case TOTEM:
                return totemIcon;
            case STATUS:
                return statusBar;
            case CAM_INDICATOR:
                return camIndicator;
            case SWIPE_DOWN:
                uiElementImage.setDrawable(getSkin(), KJAssets.ICON_SWIPE_DOWN);
                return uiElementImage;
            case SWIPE_LEFT_RIGHT:
                uiElementImage.setDrawable(getSkin(), KJAssets.ICON_SWIPE_LEFT_RIGHT);
                return uiElementImage;
            default:
                return null;
        }
    }

    public void addBananas() {
        bananas.setVisible(true);
        bananaCell.setActor(bananas);
        invalidate();
    }

    public void addSuns() {
        sunsIcon.setColor(1, 1, 1, 0);
        sunCell.setActor(sunsIcon);
        invalidate();
    }

    public void restoreHUD() {
        addBananas();

        InventoryLogic inventory = (InventoryLogic) playerEntity
                        .getLogic(KJLogicType.INVENTORY);
        sunsIcon.setText(Integer
                        .toString(inventory.getAmount(ItemCategory.COIN)));
        addSuns();
    }

    @Override
    public void lvlLoaded() {
        WinCondition winCond = kjLevel.getWinCondition();
        if (winCond != null)
            switch (winCond) {
                case PROGR_EXT:
                    pBar.setFillColor(KJSkin.uiRed0);
                    break;
                case PROGR_COLLECT:
                    pBar.setFillColor(KJSkin.uiYellow0);
                    break;
                case PROGR_NOTES:
                    pBar.setFillColor(KJSkin.uiLila0);
                    break;
                case PROGR_REPAIR:
                    pBar.setFillColor(KJSkin.uiBlue0);
                    break;
                default:
                    pBar.setFillColor(KJSkin.uiGreen0);
            }
        playerEntity = player.getEntity();
        camFocusEntity = menuControl.getGame().gameControl
                        .getEntityByName(EJXCamera.cam_target);
        movingSuns = 0;
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (camFocusEntity != null && player.getEntity()
                        .getWCY() > camFocusEntity.getWCY()) {
            float dist = camFocusEntity.getDistance(player.getEntity());
            camIndicator.setVisible(dist > 1000f);
        } else camIndicator.setVisible(false);
    }

    public boolean showContextActionBtn(ActionContext context, boolean show) {
        return showContextActionBtn(context, show, null, false);
    }

    public boolean showContextActionBtn(ActionContext context, boolean show,
                    EJXEntity contextEntity, boolean force) {
        if (show) {
            if (force) {
                contextBtn.setColor(1, 1, 1, 1);
                contextBtn.setContext(context, contextEntity);
                toggleUIElement(UIElement.CONTEXT0.name, true);
                return true;
            } else if (canShowContext(UIElement.CONTEXT0.name, context)) {
                contextBtn.setContext(context, contextEntity);
                toggleUIElement(UIElement.CONTEXT0.name, true);
                return true;
            } else {
                toggleUIElement(UIElement.CONTEXT0.name, false);
                return false;
            }
        } else {
            toggleUIElement(UIElement.CONTEXT0.name, false);
            return false;
        }
    }

    public boolean canShowContext(String name, ActionContext context) {
        for (String string : hiddenElements)
            if (string.equals(name) || string.equals(name + "_" + context.name))
                return false;
        return true;
    }

    @Override
    public void update(boolean decrMovingItems) {
        player = menuControl.getGame().getPlayer();
        updateAmmo();
        updateBananas();
        updateSuns();
        updateTools();
        setProgressBar(kjLevel.getLvlProgress());

        if (decrMovingItems) {
            movingSuns--;
            if (movingSuns == 0) {
                SequenceAction sa = Actions.sequence();
                sa.addAction(Actions.delay(1.5f));
                sa.addAction(Actions.alpha(0f, .2f));
                sunsIcon.addAction(sa);
            }
        }
    }

    public void updateSuns() {
        InventoryLogic inventory = (InventoryLogic) playerEntity
                        .getLogic(KJLogicType.INVENTORY);
        int suns = inventory.getAmount(ItemCategory.COIN);
        sunsIcon.setText(Integer.toString(suns));
    }

    public void updateAmmo() {
        InventoryLogic inventory = (InventoryLogic) playerEntity
                        .getLogic(KJLogicType.INVENTORY);
        int ammo = inventory.getAmount(ItemCategory.AMMO);
        cocoIcon.setText(Integer.toString(ammo));

        if (ammo > 0)
            toggleUIElement(UIElement.COCO.name, true);
        else toggleUIElement(UIElement.COCO.name, false);
    }

    public void updateTools() {
        InventoryLogic inventory = (InventoryLogic) playerEntity
                        .getLogic(KJLogicType.INVENTORY);
        int tools = inventory.getAmount(ItemCategory.TOOL);
        contextBtn.setText(Integer.toString(tools));
    }

    public void addBananaSlot() {
        Image img = new Image(getSkin().getDrawable(KJAssets.ICON_BANANA));
        img.setName("ui_img_banana" + bananas.getChildren().size);
        img.setVisible(false);
        img.setColor(Color.GRAY);
        bananas.add(img).pad(0, 5, 0, 5);
    }

    public void updateBananas() {
        InventoryLogic inventory = (InventoryLogic) playerEntity
                        .getLogic(KJLogicType.INVENTORY);
        int amount = inventory.getAmount(ItemCategory.POTION);
        int max = ((KJPlayer) player).getAmount(ItemCategory.POTION, true);
        SnapshotArray<Actor> tmpArray = bananas.getChildren();
        for (int i = 0; i < max; i++)
            if (i < max) {
                tmpArray.items[i].setVisible(true);
                if (i < amount)
                    tmpArray.items[i].setColor(Color.WHITE);
                else tmpArray.items[i].setColor(Color.GRAY);
            } else tmpArray.items[i].setVisible(false);
    }

    public void setProgressBar(float newProgress) {
        pBar.setValue(newProgress);
        progReady.setVisible(kjLevel.getWinCondition() != null
                        && kjLevel.isProgressFinished());
    }

    @Override
    public void toggleUIElement(String name, boolean on) {
        if (name == null) return;

        String[] split = name.split(VJXString.SEP_USCORE);
        UIElement uiElem = UIElement.get(split[0]);
        Touchable touchable = Touchable.disabled;
        if (uiElem != null) {
            switch (uiElem) {
                case ACTION0:
                    if (on) touchable = Touchable.enabled;
                    break;
                case CONTEXT0:
                    if (on) {
                        if (split.length > 1) {
                            ActionContext context = ActionContext.get(split[1]);
                            ContextLogic contextLogic = (ContextLogic) playerEntity
                                            .getLogic(KJLogicType.CONTEXT);
                            on = contextLogic.checkContext(context);
                        }
                        touchable = Touchable.enabled;
                    }
                    break;
                case JOYSTICK:
                    if (on) touchable = Touchable.enabled;
                    break;
                case JUMP:
                    if (on) touchable = Touchable.enabled;
                    break;
                default:
            }
            toggleUIElement(findActor(uiElem.name), on, touchable);
        }
    }

    @Override
    public void moveUIItem(String name, float x, float y) {
        float destX = 0;
        float destY = 0;
        Image itemImg = null;

        if (itemImages.size() < maxImages) {
            itemImg = new Image();
            itemImg.setTouchable(Touchable.disabled);
            itemImages.add(itemImg);
        } else {
            itemImg = itemImages.get(curImgInd);
            itemImg.clearActions();
            curImgInd++;
            curImgInd %= maxImages;
        }

        if (name.contains(KJString.REWARD_banana)) {
            itemImg.setDrawable(getSkin().getDrawable(KJAssets.ICON_BANANA));
            destX = top.getX() + statusBar.getX() + bananas.getX()
            + bananas.getHeight() / 2;
            destY = top.getY() + statusBar.getY() + bananas.getY()
            + bananas.getHeight() / 2;
        } else if (name.contains(KJString.REWARD_coco)) {
            itemImg.setDrawable(getSkin().getDrawable(KJAssets.ICON_COCO));
            destX = top.getX() + statusBar.getX() + cocoIcon.getX()
            + cocoIcon.getWidth() / 2;
            destY = top.getY() + statusBar.getY() + cocoIcon.getY()
            + cocoIcon.getHeight() / 2;
        } else if (name.contains(KJString.OBJECT_note)) {
            itemImg.setDrawable(menuControl.getGame().assets
                            .getDrawable("notes/note1_sixt_black"));
            //            itemImg.setDrawable(menuControl.getGame().assets.drawableFromSheet(
            //                            "global_item", "notes/note1_sixt_black"));
            destX = progress.getX() + progress.getWidth() / 2;
            destY = progress.getY() + progress.getHeight() / 2;
        } else {
            itemImg.setDrawable(getSkin().getDrawable(KJAssets.ICON_SUN));
            destX = top.getX() + statusBar.getX() + sunsIcon.getX()
            + sunsIcon.getWidth() / 2;
            destY = top.getY() + statusBar.getY() + sunsIcon.getY()
            + sunsIcon.getHeight() / 2;

            sunsIcon.clearActions();
            sunsIcon.addAction(Actions.alpha(1, .2f));
            movingSuns++;
        }

        itemImg.pack();
        itemImg.setPosition(x - itemImg.getWidth() / 2,
                        y - itemImg.getHeight() / 2);
        addActor(itemImg);
        destX -= itemImg.getWidth() / 2;
        destY -= itemImg.getHeight() / 2;

        itemImg.setColor(kjLevel.getWorldColor());
        itemImg.addAction(Actions.sequence(Actions.moveTo(destX, destY, .25f),
                        EJXActions.updateUI(name.contains(KJString.REWARD_sun),
                                        menuControl.getGame()),
                        Actions.removeActor()));
    }

    private int movingSuns = 0;

    @Override
    public void enterCallback() {
    }

    public void showPartIndicator(EJXEntity e) {
        if (e == null) {
            partIndicator.setVisible(false);
            return;
        }
        partIndicator.setVisible(true);
        partIndicator.setDrawable(getSkin().getDrawable("icons/ico_" + e.getDefName()));
        partIndicator.pack();
        partIndicator.setSize(pBar.getHeight(), pBar.getHeight());
        partIndicator.setPosition(pBar.getWidth() - pBar.getHeight() - 16, 0);
    }
}