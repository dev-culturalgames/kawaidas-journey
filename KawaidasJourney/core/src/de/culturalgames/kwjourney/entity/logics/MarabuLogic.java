package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class MarabuLogic extends EJXLogic {

    public static final String marabu_attackDelay = "marabu:attackDelay";

    public enum MarabuProperty implements EJXPropertyDefinition {
        attackDelay(MarabuLogic.marabu_attackDelay, PropertyType.floatProp, 5f),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private MarabuProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final MarabuProperty get(String name) {
            for (MarabuProperty property : MarabuProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }
    private float timer;
    private float attackDelay;

    private boolean fightStarted;

    private MoveLogic moveLogic;
    private AttackLogic attackLogic;

    private EJXEntity target;
    private LifeLogic targetLife;

    public MarabuLogic() {
        super(KJLogicType.MARABU);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(MarabuProperty.values(), vjxProps, properties);
        attackDelay = vjxProps.get(MarabuProperty.attackDelay, float.class);
        owner.getActor().addAction(KJActions.setActivity(Activity.DISABLED, owner));
        fightStarted = false;

        attackLogic = (AttackLogic) owner.getLogic(KJLogicType.ATTACK);
        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);

        target = owner.getListener().getPlayerEntity();
        targetLife = (LifeLogic) target.getLogic(KJLogicType.LIFE);
    }

    @Override
    public void update(float deltaT) {
        if (!owner.isAlive() || !target.isAlive()) return;

        if (!owner.isActivity(Activity.DISABLED) && !fightStarted) {
            fightStarted = true;
        }

        if (fightStarted) {
            if (!attackLogic.isAttacking()) {
                if (owner.isActivity(Activity.FLY))
                    timer += deltaT;

                if (timer >= attackDelay) {
                    owner.getActor().clearActions();
                    attackLogic.attackMove(target);
                    timer -= attackDelay;
                }
            } else {
                if (targetLife.isInvulnerable()) {
                    moveLogic.stop(true);
                    attackLogic.stopAttack(true);
                }
                else
                    attackLogic.updateWP();
            }
        }
    }

    @Override
    public void onDamage(int oldHP, int newHP) {
        timer = 0;
    }
}