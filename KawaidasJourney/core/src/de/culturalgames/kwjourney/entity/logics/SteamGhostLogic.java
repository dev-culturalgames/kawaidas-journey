package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.cmds.Commands;
import de.venjinx.ejx.cmds.SpawnCommand;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXData;

public class SteamGhostLogic extends EJXLogic {

    public static final String steamGhost_chaseTime = "steamGhost:chaseTime";

    public enum SteamGhostProperty implements EJXPropertyDefinition {
        chaseTime(steamGhost_chaseTime, PropertyType.floatProp, -1f);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private SteamGhostProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final SteamGhostProperty get(String name) {
            for (SteamGhostProperty property : SteamGhostProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private float chaseTime = -1;
    private float chaseTimer = 0;

    private boolean fightOver = false;
    private Command[] attackCmds;
    private SpawnCommand spawnCmd;

    private LifeLogic lifeLogic;
    private MoveLogic moveLogic;
    private AttackLogic attackLogic;

    private EJXEntity player, tree;

    private Rectangle dieArea;

    public SteamGhostLogic() {
        super(KJLogicType.STEAMGHOST);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(SteamGhostProperty.values(), vjxProps,
                        properties);

        chaseTime = vjxProps.get(SteamGhostProperty.chaseTime, Float.class);

        lifeLogic = (LifeLogic) owner.getLogic(KJLogicType.LIFE);
        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        attackLogic = (AttackLogic) owner.getLogic(KJLogicType.ATTACK);
        owner.setTargetEntity(owner.getListener().getPlayerEntity());

        attackCmds = new Command[1];
        attackCmds[0] = Commands.getAnimCommand((AnimatedEntity) owner,
                        Activity.RUN.name, true);

        attackLogic.attackWP.setOnSetCmds(attackCmds);

        spawnCmd = Commands.getSpawnCommand(listener.getGame(),
                        "tigerMosquito0", owner, 0, 0);

        player = listener.getPlayerEntity();
        tree = listener.getEntityByName("kHome");

        AnimatedEntity aTree = (AnimatedEntity) tree;
        float x = aTree.getBone("body").getX() + tree.getX();
        float y = aTree.getBone("body").getY() + tree.getY();
        dieArea = new Rectangle(x, y, 1280, 512);
    }

    @Override
    public void update(float deltaT) {
        if (attackLogic.isAttacking()) {
            if (attackLogic.getTarget() == player) {
                chaseTimer += deltaT;

                if (chaseTimer > chaseTime) {
                    // stop chase kawaida
                    SequenceAction sa = stopAttack(true);
                    sa.addAction(KJActions.setWPAction(moveLogic.nextWaypoint(),
                                    owner));
                    owner.getActor().addAction(sa);
                } else {
                    // chase kawaida

                    tmpVec.set(player.getWorldCenter());
                    tmpVec.sub(owner.getWorldCenter());

                    if (Math.signum(moveLogic.getVelocity().x) != Math
                                    .signum(tmpVec.x)) {
                        if (Math.abs(tmpVec.x) > 250) {
                            SequenceAction sa = stopAttack(false);
                            sa.addAction(KJActions.setInvulnerable(owner, true,
                                            chaseTime - chaseTimer));
                            sa.addAction(KJActions.attackMove(owner));
                            owner.getActor().addAction(sa);
                        } else return;
                    }
                }
            }
            if (owner.getTargetEntity() == null) return;
            Fixture f = owner.getTargetEntity().getPhysics()
                            .getFixture("life_hitbox");
            if (f == null) return;
            tmpVec.set(B2DWorld.getFixtureWorldCenter(f));
            tmpVec.x += (tmpVec.x < owner.getWCX() ? -1 : 1) * 256;
            tmpVec.y = owner.getWCY();
            attackLogic.attackWP.setCenter(tmpVec);
        }
    }

    private SequenceAction stopAttack(boolean setIdle) {
        attackLogic.stopAttack(false);
        moveLogic.stop();
        owner.setLifecycle(Lifecycle.STILL);
        SequenceAction sa = Actions.sequence();
        sa.addAction(KJActions.setAnimation("idle", true, owner));
        sa.addAction(Actions.delay(1f));
        if (setIdle)
            sa.addAction(KJActions.setActivity(Activity.IDLE, owner));
        return sa;
    }

    @Override
    public void onTrigger(boolean triggered, VJXData thisData,
                    EJXEntity other, VJXData otherData) {
        if (attackLogic.isAttacking() && other == owner.getTargetEntity()) {
            if (otherData.getName().contains("life"))
                owner.getActor().addAction(stopAttack(true));
        }

        if (fightOver) {
            if (other == tree) {
                lifeLogic.die();
                LifeLogic treeLife = (LifeLogic) tree
                                .getLogic(KJLogicType.LIFE);
                if (tree.isAlive()) {
                    treeLife.die();
                    if (((ClimbLogic) player.getLogic(KJLogicType.CLIMB))
                                    .isClimbing()) {
                        ((JumpLogic) player.getLogic(KJLogicType.JUMP))
                        .jump(.5f);
                    }
                }
                return;
            }
        } else owner.setTargetEntity(player);
    }

    @Override
    public void onDamage(int oldHP, int newHP) {
        owner.setLifecycle(Lifecycle.STILL);
        float delay = ((AnimatedEntity) owner).getAnimDuration("damage");

        if (newHP >= 2) {
            owner.setTargetEntity(listener.getPlayerEntity());
            moveLogic.stop();

            Waypoint wp = Waypoints.getWaypoint("boss_mos0");
            Waypoint wp2 = Waypoints.getWaypoint("boss_mos1");

            wp.getCenter(tmpVec);
            spawnCmd.setPosition(tmpVec.x, tmpVec.y);
            owner.execSingleCmd(spawnCmd);

            wp2.getCenter(tmpVec);
            spawnCmd.setPosition(tmpVec.x, tmpVec.y);
            owner.execSingleCmd(spawnCmd);

            SequenceAction sa = Actions.sequence();
            sa.addAction(Actions.delay(delay));
            sa.addAction(KJActions.setActivity(Activity.EXECUTE, owner));
            sa.addAction(KJActions.setInvulnerable(owner, true, chaseTime));
            sa.addAction(KJActions.attackMove(owner));
            owner.getActor().addAction(sa);

            chaseTimer = 0;
        } else if (newHP == 1) {
            if (!dieArea.contains(owner.getWorldCenter()) && tree.isAlive()) {
                owner.setTargetEntity(tree);

                SequenceAction sa = Actions.sequence();
                sa.addAction(Actions.delay(delay));
                sa.addAction(KJActions.setActivity(Activity.EXECUTE, owner));
                sa.addAction(KJActions.setInvulnerable(owner, true, chaseTime));
                sa.addAction(KJActions.attackMove(owner));
                owner.getActor().addAction(sa);
            } else {
                lifeLogic.die();
            }
            fightOver = true;
            owner.despawnChildren();
        }
    }

    @Override
    public void onDie() {
        ClimbLogic climbLogic = (ClimbLogic) player.getLogic(KJLogicType.CLIMB);
        JumpLogic jumpLogic = (JumpLogic) player.getLogic(KJLogicType.JUMP);
        if (climbLogic.isClimbing()) jumpLogic.jump();
    }
}