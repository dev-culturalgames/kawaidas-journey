package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;

import de.culturalgames.kwjourney.cmds.Jump2Command;
import de.culturalgames.kwjourney.cmds.KJCommands;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXData;

public class BumperLogic extends EJXLogic implements CollisionListener {

    public static final String bumper_height = "bumper:height";
    public static final String bumper_speed = "bumper:speed";
    public static final String bumper_target = "bumper:target";

    public enum BumpProperty implements EJXPropertyDefinition {
        height(BumperLogic.bumper_height, PropertyType.floatProp, .1f),
        speed(BumperLogic.bumper_speed, PropertyType.floatProp, 14f),
        target(BumperLogic.bumper_target, PropertyType.stringProp, EJXCamera.cam_target),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private BumpProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final BumpProperty get(String name) {
            for (BumpProperty property : BumpProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private String targetName;
    private EJXEntity target;

    private float bumpHeight;
    private float bumpSpeed;
    private Jump2Command jumpCmd;

    public BumperLogic() {
        super(KJLogicType.BUMPER);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(BumpProperty.values(), vjxProps, properties);

        bumpHeight = vjxProps.get(BumpProperty.height, Float.class);
        bumpSpeed = vjxProps.get(BumpProperty.speed, Float.class);

        targetName = vjxProps.get(BumpProperty.target, String.class);
        target = listener.getEntityByName(targetName);

        jumpCmd = KJCommands.getJump2Command(target, bumpHeight, bumpSpeed, null, true);

        owner.getBody().setSleepingAllowed(false);
    }

    @Override
    public void update(float deltaT) {
        super.update(deltaT);
        //        owner.getBody().setAwake(true);
    }

    @Override
    public void onTrigger(boolean triggered, VJXData thisData,
                    EJXEntity other, VJXData otherData) {
        if (other != target || !triggered)
            return;
        target.execSingleCmd(jumpCmd);
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        short otherCat = otherFix.getFilterData().categoryBits;
        short thisCat = thisFix.getFilterData().categoryBits;
        if (B2DBit.HITBOX.isCategory(thisCat)
                        && B2DBit.TRIGGER.isCategory(otherCat))
            return true;
        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        return false;
    }
}