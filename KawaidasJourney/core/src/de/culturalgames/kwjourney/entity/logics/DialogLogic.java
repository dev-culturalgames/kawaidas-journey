package de.culturalgames.kwjourney.entity.logics;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.HashMap;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class DialogLogic extends EJXLogic {
    private static HashMap<String, Dialog> dialogs = new HashMap<>();

    private static void addDialog(String name, Dialog dialog) {
        dialogs.put(name, dialog);
    }

    public static Dialog getDialog(String name) {
        return dialogs.get(name);
    }

    public static void removeDialog(String name) {
        dialogs.remove(name);
    }

    public static void clearDialogs() {
        dialogs.clear();
    }

    public static class Dialog {

        private String name;
        private String levelNr;
        private ArrayDeque<String> textParts;
        private HashMap<String, String> participants;
        private boolean loop;
        private float delay;
        private boolean end;
        private EJXEntity object;

        public Dialog(EJXEntity object, String levelNr, MapProperties properties) {
            this.object = object;
            name = object.getName();
            this.levelNr = levelNr;
            delay = properties.get("delay", 0f, Float.class);
            loop = properties.get("loop", false, Boolean.class);
            end = properties.get("end", false, boolean.class);
            textParts = new ArrayDeque<>();
            participants = new HashMap<>();

            loadNew(properties);
        }

        private void loadNew(MapProperties properties) {
            TextControl texts = TextControl.getInstance();
            String textID = KJTexts.STORY_ + levelNr + VJXString.SEP_USCORE + name;
            String parts;
            if (texts.hasText(textID)) {
                parts = texts.get(textID);

                // new localization
                if (parts != null && !parts.isEmpty()) {
                    String[] partsSplit = parts.split(VJXString.SEP_NEW_LINE_N);
                    for (int i = 0; i < partsSplit.length; i++) {
                        textParts.add(partsSplit[i]);
                    }
                }
            } else {
                // TODO remove old localization when finished
                VJXLogger.log(LogCategory.ERROR, "Loading old dialog: " + name);
                loadOld(properties);
            }
        }

        private void loadOld(MapProperties properties) {
            String parts = properties.get("parts", null, String.class);
            if (parts != null && !parts.isEmpty()) {
                parts = parts.replace("\r", "");
                String[] partsSplit = parts.split("\n");
                for (int i = 0; i < partsSplit.length; i++) {
                    textParts.add(partsSplit[i]);
                }
            }

            int i = 1;
            String key = "speaker0";
            String participant = properties.get(key, null, String.class);
            while (participant != null) {
                participants.put(key, participant);
                key = "speaker" + i;
                participant = properties.get(key, null, String.class);
                i++;
            }
        }

        public EJXEntity getObject() {
            return object;
        }

        public void addParticipant(String key, EJXEntity participant) {
            participants.put(key, participant.getName());
        }

        public String getParticipant(String key) {
            return participants.get(key);
        }

        public Collection<String> getParticipants() {
            return participants.values();
        }

        public String getName() {
            return name;
        }

        public boolean isEnd() {
            return end;
        }

        public ArrayDeque<String> getParts() {
            return textParts;
        }

        public boolean isRepeat() {
            return loop;
        }

        public float getDelay() {
            return delay;
        }
    }

    public static final String story_name = "hint:name";
    public static final String story_rangeRadius = "hint:radius";
    public static final String story_rangeX = "hint:rangeX";
    public static final String story_rangeY = "hint:rangeY";

    public enum StoryProperty implements EJXPropertyDefinition {
        rangeRadius(story_rangeRadius, PropertyType.floatProp, 0f),
        rangeX(story_rangeX, PropertyType.floatProp, 0f),
        rangeY(story_rangeY, PropertyType.floatProp, 0f);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private StoryProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final StoryProperty get(String name) {
            for (StoryProperty property : StoryProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    public DialogLogic() {
        super(KJLogicType.DIALOGUE);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(StoryProperty.values(), vjxProps, properties);

        String name = owner.getName();
        String level = listener.getLevel().getLevelName();
        if (name != null && !name.isEmpty()) {
            if (!dialogs.containsKey(name))
                addDialog(name, new Dialog(owner, level, properties));
        }
    }
}