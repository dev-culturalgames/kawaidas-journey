package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.EJXFloat;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class JumpLogic extends EJXLogic implements CollisionListener {

    public static final String jump_height_max = "jump:height_max";
    public static final String jump_speed = "jump:speed";

    public enum JumpProperty implements EJXPropertyDefinition {
        heightMax(JumpLogic.jump_height_max, PropertyType.ejxFloat, EJXTypes.newFloat(1f)),
        speed(JumpLogic.jump_speed, PropertyType.floatProp, 1f),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private JumpProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final JumpProperty get(String name) {
            for (JumpProperty property : JumpProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private Vector2 jumpStart, jumpEnd;
    private boolean jumping = false;
    private boolean stopJump = true;

    private float speed;
    private EJXFloat heightMax;

    private MoveLogic moveLogic;
    private Box2DLogic physicsLogic;

    public JumpLogic() {
        super(KJLogicType.JUMP, JumpProperty.values());
        jumpStart = new Vector2();
        jumpEnd = new Vector2();
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(JumpProperty.values(), vjxProps, properties);

        heightMax = getProperty(JumpProperty.heightMax, EJXFloat.class);
        speed = vjxProps.get(JumpProperty.speed, Float.class);

        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);

        if (owner.getPhysics() != null) physicsLogic = owner.getPhysics();
    }

    @Override
    public void update(float deltaT) {
        if (owner.isLifecycle(Lifecycle.DEAD)) stopJump();
        if (owner.isActivity(Activity.DISABLED) || owner.isActivity(Activity.EXECUTE)) return;
        if (jumping) {
            if (moveLogic.getVelocity().y < 0f && moveLogic.applyGravity()) {
                owner.setActivity(Activity.FALL);
                stopJump();
            }
            if (!owner.isActivity(Activity.RISE)
                            && moveLogic.getVelocity().y > 0
                            || !owner.isActivity(Activity.FALL)
                            && moveLogic.getVelocity().y < 0)
                jumping = false;
        }
    }

    private Runnable stopJumpRunnable = new Runnable() {

        @Override
        public void run() {
            owner.setLifecycle(Lifecycle.STILL);
            owner.setActivity(Activity.IDLE);
        }
    };

    public void newJump(float height, float duration,
                    Interpolation interpolation, boolean force) {
        duration /= 2;

        SequenceAction sa = Actions.sequence();
        sa.addAction(Actions.moveBy(0, height, duration,
                        Interpolation.exp5Out));
        sa.addAction(Actions.moveBy(0, -height, duration,
                        Interpolation.pow3In));
        sa.addAction(Actions.run(stopJumpRunnable));
        owner.getActor().addAction(sa);

        owner.setLifecycle(Lifecycle.MOVE);
        owner.setActivity(Activity.EXECUTE);

        if (physicsLogic == null) return;

        if (listener.getPlayerEntity().getPhysics().isAttachedTo(owner)) {
            JumpLogic pJump = (JumpLogic) listener.getPlayerEntity()
                            .getLogic(KJLogicType.JUMP);
            pJump.jump();
        }

        for (EJXEntity e : physicsLogic.getAttachedChildren()) {
            JumpLogic jl = (JumpLogic) e.getLogic(KJLogicType.JUMP);
            if (jl != null) {
                jl.newJump(height, duration * 2, interpolation, force);
                jl.owner.setLifecycle(Lifecycle.STILL);
                e.setActivity(Activity.EXECUTE);
                e.getActor().addAction(EJXActions.setActivity(Activity.IDLE, e));
            }
        }
    }

    public void jump() {
        jump(heightMax.value, false);
    }

    public void jump(boolean force) {
        jump(heightMax.value, force);
    }

    public void jump(float maxHeight) {
        jump(maxHeight, false);
    }

    public void jump(float maxHeight, boolean force) {
        if (moveLogic != null) {

            //            VJXLogger.log(owner + " jump " + moveLogic.applyGravity());
            if (!moveLogic.applyGravity() && !jumping || force) {
                //                VJXLogger.log(owner + " jump " + force);
                if (!moveLogic.isGrounded()) listener.camCatchYUp(true);

                final float height = maxHeight * B2DWorld.WORLD_UNIT;
                physicsLogic.detach();
                jumping = true;
                stopJump = false;
                jumpStart.y = owner.getWCY();
                jumpEnd.y = jumpStart.y + height;

                moveLogic.setLastSavePosition();
                moveLogic.setIgnoreGravity(false);
                moveLogic.setDestination(moveLogic.getDestinationX(),
                                jumpEnd.y);

                initJump(height);
                physicsLogic.setIgnorePassable(false);

                owner.setLifecycle(Lifecycle.MOVE);
                owner.setActivity(Activity.RISE);
            }
        }
    }

    public float initJump(float height) {
        float v0;
        float beta = (float) Math.toRadians(90);
        float g = B2DWorld.GRAVITY.y * moveLogic.getGravityScale()
                        * B2DWorld.WORLD_UNIT;

        v0 = height * 2 * g;
        v0 /= Math.sin(beta) * Math.sin(beta);
        v0 = (float) Math.sqrt(v0);

        float vY0;
        vY0 = (float) (v0 * Math.sin(beta));

        moveLogic.setVelocity(moveLogic.getVelocity().x, vY0);
        return vY0;
    }

    private void land() {
        jumping = false;
    }

    public float getJumpDist() {
        float dist = (jumpEnd.y - owner.getWCY()) / B2DWorld.WORLD_UNIT;
        return dist >= 0 ? dist : 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public boolean stoppedJump() {
        return stopJump;
    }

    public float getSpeed() {
        return speed;
    }

    public void stopJump() {
        stopJump(false);
    }

    public void stopJump(boolean now) {
        jumping = moveLogic.applyGravity();
        stopJump = true;
        jumpStart.set(owner.getWorldCenter());
        jumpEnd.set(owner.getWorldCenter());

        if (now)
            initJump(0);
    }

    public float getMaxJumpHeight() {
        return heightMax.value;
    }

    @Override
    public SequenceAction interrupt(float delay) {
        stopJump(true);
        return super.interrupt(delay);
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (jumping) {
            if (physicsLogic.isGrounded()) {
                land();
                return true;
            }

            if (physicsLogic.headCollision(B2DBit.SOLID)) {
                stopJump(true);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        return false;
    }

    @Override
    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        super.drawShapeDebug(shapeRenderer);

        if (!jumping) return;

        shapeRenderer.setColor(Color.ORANGE);

        // draw destination
        shapeRenderer.line(owner.getWCX(), jumpStart.y, owner.getWCX(), jumpEnd.y);
    }
}