package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.KJLevel;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.VJXData;

public class BonusLogic extends EJXLogic {

    private KJLevel level;

    public BonusLogic() {
        super(KJLogicType.BONUS);
    }

    @Override
    public void init(MapProperties properties) {
        level = (KJLevel) listener.getLevel();
    }

    @Override
    public void onTrigger(boolean triggered, VJXData thisData,
                    EJXEntity other, VJXData otherData) {
        if (!triggered)
            return;
        level.setBonusFound(true);
    }
}