package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.logics.EJXLogic;

public class NoteLogic extends EJXLogic {

    public NoteLogic() {
        super(KJLogicType.NOTE);
    }

    @Override
    public void init(MapProperties properties) {

    }
}