package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.ActionContext;

public class FertileLogic extends EJXLogic {

    private ContextLogic contextLogic;

    public FertileLogic() {
        super(KJLogicType.FERTILE);
    }

    @Override
    public void init(MapProperties properties) {
        contextLogic = (ContextLogic) owner.getTargetEntity()
                        .getLogic(KJLogicType.CONTEXT);
    }

    @Override
    public void targetInView(boolean inView) {
        super.targetInView(inView);
        EJXEntity target = owner.getTargetEntity();
        if (inView) {
            target.setTargetEntity(owner);
            contextLogic.setContext(ActionContext.PLANT, owner);
        } else {
            contextLogic.setContext(ActionContext.NONE, owner);
        }
    }
}