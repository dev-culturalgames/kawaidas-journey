package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.LifeLogic.DamageType;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXData;

public class TrapLogic extends EJXLogic implements CollisionListener {

    public static final String trap_smoke = "trap:smoke";
    //    public static final String life_hitpointsMax = "life:hitpoints_max";

    public enum TrapProperty implements EJXPropertyDefinition {
        smoke(trap_smoke, PropertyType.stringProp, VJXString.STR_EMPTY);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private TrapProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final TrapProperty get(String name) {
            for (TrapProperty property : TrapProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private boolean triggered;
    private LifeLogic lifeLogic;

    public TrapLogic() {
        super(KJLogicType.TRAP);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(TrapProperty.values(), vjxProps,
                        properties);

        lifeLogic = (LifeLogic) owner.getLogic(KJLogicType.LIFE);
    }

    @Override
    public void onTrigger(boolean triggered, VJXData thisData,
                    EJXEntity other, VJXData otherData) {
        super.onTrigger(triggered, thisData, other, otherData);
        LifeLogic otherLife = (LifeLogic) other.getLogic(KJLogicType.LIFE);
        AttackLogic otherAttack = (AttackLogic) other
                        .getLogic(KJLogicType.ATTACK);

        if (otherLife == null && otherAttack == null) return;

        boolean otherEnemy = otherLife != null && otherLife.isEnemy();
        otherEnemy |= otherAttack != null && otherAttack.isEnemy();

        if (triggered && lifeLogic.isEnemy() != otherEnemy) triggerTrap(other);
    }

    @Override
    public void onDamage(int oldHP, int newHP) {
        triggerTrap(null);
    }

    private void triggerTrap(EJXEntity target) {
        if (!triggered) {
            triggered = true;
            lifeLogic.die();

            if (target != null) {
                LifeLogic lLogic = (LifeLogic) target
                                .getLogic(KJLogicType.LIFE);
                if (lLogic != null)
                    lLogic.takeDamage(1, DamageType.melee);
            }
        }
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        //        short otherCat = otherFix.getFilterData().categoryBits;
        short thisCat = thisFix.getFilterData().categoryBits;

        if (B2DBit.TRIGGER.isCategory(thisCat)) return true;
        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        return true;
    }
}
