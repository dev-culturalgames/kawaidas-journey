package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.LifeLogic.DamageType;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.ClickType;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class ExtinguishLogic extends EJXLogic {

    private static final String POST_extinguish = "_extinguish";

    //    public static final String bonus_speedX = "climb:speedX";
    //    public static final String bonus_speedY = "climb:speedY";

    public enum ExtinguishProperty implements EJXPropertyDefinition {
        //        speedX(ClimbLogic.climb_speedX, PropertyType.floatProp, 1f),
        //        speedY(ClimbLogic.climb_speedY, PropertyType.floatProp, 1f)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private ExtinguishProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final ExtinguishProperty get(String name) {
            for (ExtinguishProperty property : ExtinguishProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private ClimbLogic climbLogic;

    private boolean startSquirt;
    private boolean squirting;
    private boolean squirtedOnce;
    private boolean endSquirt;

    public ExtinguishLogic() {
        super(KJLogicType.EXTINGUISH);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(ExtinguishProperty.values(), vjxProps, properties);

        climbLogic = (ClimbLogic) owner.getLogic(KJLogicType.CLIMB);

        squirting = false;
        startSquirt = false;
        squirtedOnce = false;
        endSquirt = false;
    }

    @Override
    public void update(float delta) {
        if (owner.isActivity(Activity.IDLE)
                        || owner.isActivity(Activity.CLIMB_IDLE)) {
            if (startSquirt) {
                squirt();
                return;
            } else {
                if (endSquirt) {
                    stopSquirt();
                    return;
                } else if (squirting) {
                    squirt();
                    return;
                }
            }
        }
    }

    public void toggleSquirt(ClickType type) {
        AnimatedEntity aObj = (AnimatedEntity) owner;
        if (type == ClickType.DOWN) {
            if (owner.isActivity(Activity.EXECUTE)) return;
            startSquirt = true;
            String anim = climbLogic.isClimbing() ? "climb_squirt_start"
                            : "squirt_start";
            aObj.setAnimation(anim, false);

            float delay = aObj.getAnimDuration(anim);
            owner.interrupt(delay);
            owner.getActor().addAction(Actions.sequence(Actions.delay(delay,
                            EJXActions.setActivity(Activity.IDLE, owner))));
        } else {
            if (type == ClickType.UP && (squirting || startSquirt)) {
                if (squirtedOnce) {
                    endSquirt = true;
                    startSquirt = false;
                    squirting = false;
                } else {
                    endSquirt = true;
                    squirting = false;
                }
            }
        }
    }

    private void squirt() {
        AnimatedEntity aObj = (AnimatedEntity) owner;
        EJXEntity target = owner.getTargetEntity();
        if (target != null && target.hasLogic(KJLogicType.FIRE)) {
            if (!owner.isActivity(Activity.EXECUTE)) {
                if (!squirting) {
                    if (!squirtedOnce) {
                        squirtedOnce = true;
                    }
                    squirting = true;
                } else {
                    squirting = !endSquirt;
                }
                startSquirt = false;
                String anim = climbLogic.isClimbing() ? "climb_squirt" : "squirt";
                aObj.setAnimation(anim, true);

                float delay = aObj.getAnimDuration(anim);
                owner.interrupt(delay);
                owner.getActor().addAction(Actions.sequence(Actions.delay(
                                aObj.getAnimDuration(anim),
                                EJXActions.setActivity(Activity.IDLE, owner))));

                owner.playSfx(owner.getDefName() + POST_extinguish);
                LifeLogic l = (LifeLogic) target.getLogic(KJLogicType.LIFE);
                l.takeDamage(1, DamageType.melee, false);
            }
        } else {
            stopSquirt();
        }
    }

    private void stopSquirt() {
        endSquirt = false;
        squirting = false;
        squirtedOnce = false;
        startSquirt = false;
        owner.setActivity(Activity.EXECUTE);

        AnimatedEntity aObj = (AnimatedEntity) owner;
        String anim = climbLogic.isClimbing() ? "climb_squirt_end"
                        : "squirt_end";
        aObj.setAnimation(anim, false);
        owner.getActor().addAction(Actions.sequence(Actions.delay(
                        aObj.getAnimDuration(anim),
                        EJXActions.setActivity(Activity.IDLE, owner))));
    }

    @Override
    public SequenceAction interrupt(float delay) {
        owner.setOrientation(owner.getTargetEntity());
        return super.interrupt(delay);
    }
}
