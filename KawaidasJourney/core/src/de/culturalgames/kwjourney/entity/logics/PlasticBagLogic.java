package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class PlasticBagLogic extends EJXLogic {

    public static final String playsticBag_waitTime = "plasticBag:waitTime";
    public static final String playsticBag_rangeX = "plasticBag:rangeX";
    public static final String playsticBag_rangeY = "plasticBag:rangeY";

    public enum PlasticBagProperty implements EJXPropertyDefinition {
        waitTime(playsticBag_waitTime, PropertyType.floatProp, 0f),
        rangeX(playsticBag_rangeX, PropertyType.floatProp, 0f),
        rangeY(playsticBag_rangeY, PropertyType.floatProp, 0f);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private PlasticBagProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final PlasticBagProperty get(String name) {
            for (PlasticBagProperty property : PlasticBagProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private float waitTime;
    private boolean waiting;
    private float rangeX;
    private float rangeY;
    private Waypoint destination;

    private MoveLogic moveLogic;
    private Runnable updateRunnable = new Runnable() {

        @Override
        public void run() {
            float x = (float) (owner.getBirthPlace().x
                            + Math.random() * rangeX * 2 - rangeX);
            float y = (float) (owner.getBirthPlace().y
                            + Math.random() * rangeY * 2 - rangeY);
            destination.reset();
            destination.setCenter(x, y);
            waiting = false;
        }
    };

    public PlasticBagLogic() {
        super(KJLogicType.PLASTIC_BAG);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(PlasticBagProperty.values(), vjxProps, properties);
        destination = new Waypoint(owner.getName() + "_dest");
        destination.setUpdateActivity(false);

        waitTime = vjxProps.get(PlasticBagProperty.waitTime, float.class);
        rangeX = vjxProps.get(PlasticBagProperty.rangeX, 256f, float.class);
        rangeY = vjxProps.get(PlasticBagProperty.rangeY, 256f, float.class);
        float x = (float) (owner.getBirthPlace().x + Math.random() * rangeX - rangeX / 2f);
        float y = (float) (owner.getBirthPlace().y + Math.random() * rangeY - rangeY / 2f);
        destination.setCenter(x, y);

        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        moveLogic.addWaypoint(destination);
    }

    @Override
    public void positionReached() {
        if (waiting)
            return;
        updateDestination();
        owner.playSfx(owner.getDefName() + VJXString.SEP_DDOT + "sfx:fly");
    }

    private void updateDestination() {
        SequenceAction sa = new SequenceAction();
        sa.addAction(Actions.delay(waitTime));
        sa.addAction(Actions.run(updateRunnable));
        owner.getActor().addAction(sa);
        waiting = true;
    }

    @Override
    public void drawShapeDebug(ShapeRenderer renderer) {
        renderer.rect(owner.getBirthPlace().x - rangeX,
                        owner.getBirthPlace().y - rangeY, rangeX * 2f,
                        rangeY * 2f);
    }
}
