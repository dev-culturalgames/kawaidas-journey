package de.culturalgames.kwjourney.entity.logics;

import java.util.HashMap;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Array;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KawaidaHUD;
import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.actions.game.SuckCoinsAction;
import de.culturalgames.kwjourney.api.AdMobController;
import de.culturalgames.kwjourney.cmds.KJCommands;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.util.KJTypes;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.CollisionDef;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.AmbientLogic;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXData;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.vjx.ui.debug.LevelStatsWindow.LevelStats;

public class LifeLogic extends EJXLogic implements CollisionListener {
    public static final String damageType_melee = "melee";
    public static final String damageType_ranged = "ranged";

    public static final String ANIM_BLOCK = "block";

    public enum DamageType {
        melee(damageType_melee), ranged(damageType_ranged);

        public final String name;

        private DamageType(String name) {
            this.name = name;
        }

        public static final DamageType get(String name) {
            for (DamageType s : DamageType.values())
                if (s.name.equals(name)) return s;

            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static final String life_canBlock = "life:canBlock";
    public static final String life_collider = "life:collider";
    public static final String life_damageTypes = "life:damageTypes";
    public static final String life_despawnOnDeath = "life:despawnOnDeath";
    public static final String life_despawnDelay = "life:despawn_delay";
    public static final String life_hitpoints = "life:hitpoints";
    public static final String life_hitpointsMax = "life:hitpoints_max";
    public static final String life_growable = "life:growable";
    public static final String life_growing = "life:growing";
    public static final String life_growRandom = "life:growRandomize";
    public static final String life_growTime = "life:growTime";
    public static final String life_invulTime = "life:invulTime";
    public static final String life_isEnemy = "life:isEnemy";
    //    public static final String life_jumpToKill = "life:jumpKill";

    public enum LifeProperty implements EJXPropertyDefinition {
        canBlock(LifeLogic.life_canBlock, PropertyType.boolProp, false),
        collider(LifeLogic.life_collider, PropertyType.colliderDef,  new CollisionDef()),
        damageTypes(LifeLogic.life_damageTypes, PropertyType.stringProp, "melee,ranged"),
        despawnOnDeath(LifeLogic.life_despawnOnDeath, PropertyType.boolProp, true),
        despawnDelay(LifeLogic.life_despawnDelay, PropertyType.floatProp, 0f),
        growable(LifeLogic.life_growable, PropertyType.boolProp, false),
        growing(LifeLogic.life_growing, PropertyType.boolProp, false),
        growTime(LifeLogic.life_growTime, PropertyType.floatProp, 1f),
        growRandom(LifeLogic.life_growRandom, PropertyType.floatProp, 0f),
        hitpoints(LifeLogic.life_hitpoints, PropertyType.intProp, 1),
        hitpointsMax(LifeLogic.life_hitpointsMax, PropertyType.intProp, 1),
        invulTime(LifeLogic.life_invulTime, PropertyType.floatProp, 2f),
        isEnemy(LifeLogic.life_isEnemy, PropertyType.boolProp, true),
        //        jumpToKill(LifeLogic.life_jumpToKill, PropertyType.boolProp, false),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private LifeProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final LifeProperty get(String name) {
            for (LifeProperty property : LifeProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    public static final String ANIM_EVENT_GROWN = "grown";
    public static final String ANIM_EVENT_BRANCH_HIT = "branch_hit";

    //    private boolean isAlive;
    private boolean invulnerable;
    private float invulTime;
    private int invulAnimTrack;
    private DelayAction invDurationAction;

    private boolean isEnemy;
    private int hitpointsMax = 1, hitpoints = 1;
    private boolean canBlock;
    private HashMap<DamageType, Boolean> damageTypes;
    private boolean despawnOnDeath;
    private float despawnDelay;
    private boolean inDanger;

    // object growing can manually be triggered
    private boolean growable;
    private boolean growing;
    private float growTime;
    private float growRandom;
    private float growTimer;
    private float nextGrowTime;

    private AmbientLogic ambientLogic;
    private AttackLogic thisAttackLogic;
    private AttackLogic otherAttackLogic;
    private InventoryLogic inventoryLogic;

    public LifeLogic() {
        super(KJLogicType.LIFE);
        damageTypes = new HashMap<>();
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(LifeProperty.values(), vjxProps, properties);

        hitpoints = vjxProps.get(LifeProperty.hitpoints, Integer.class);
        hitpointsMax = vjxProps.get(LifeProperty.hitpointsMax, Integer.class);

        isEnemy = vjxProps.get(LifeProperty.isEnemy, boolean.class);
        invulTime = vjxProps.get(LifeProperty.invulTime, float.class);
        canBlock = vjxProps.get(LifeProperty.canBlock, boolean.class);

        String[] types = vjxProps
                        .get(LifeProperty.damageTypes, String.class)
                        .split(VJXString.SEP_LIST);
        for (String type : types) {
            damageTypes.put(DamageType.get(type), true);
        }

        despawnOnDeath = vjxProps.get(LifeProperty.despawnOnDeath, boolean.class);
        despawnDelay = vjxProps.get(LifeProperty.despawnDelay, float.class);

        growable = vjxProps.get(LifeProperty.growable, boolean.class);

        growing = vjxProps.get(LifeProperty.growing, boolean.class);
        growTime = vjxProps.get(LifeProperty.growTime, float.class);
        growRandom = vjxProps.get(LifeProperty.growRandom, float.class);

        nextGrowTime = newGrowTime();

        thisAttackLogic = (AttackLogic) owner.getLogic(KJLogicType.ATTACK);
        inventoryLogic = (InventoryLogic) owner.getLogic(KJLogicType.INVENTORY);

        inDanger = false;
    }

    @Override
    public void update(float delta) {
        if (owner.isAlive() && getHP() <= 0
                        && !owner.isActivity(Activity.DISABLED)) {
            die();
            return;
        }

        if (growing) {
            growTimer += delta;
            if (growTimer >= nextGrowTime) {
                if (!hasFullHP()) {
                    grow();
                }
                growTimer -= nextGrowTime;
                nextGrowTime = newGrowTime();
            }
        }

        if (growable && hasFullHP()) {
            EJXEntity e = listener.getLevel().getEntityByName("amb_forest_plant", true);
            if (e != null) {
                ambientLogic = (AmbientLogic) e.getLogic(VJXLogicType.AMBIENT);
                if (owner.isTargetInRange()) {
                    e.setCenter(owner.getWorldCenter());
                    ambientLogic.setAreaSize(owner.getRange());
                    ambientLogic.setAreaCenter(owner.getWorldCenter());
                }
            }
        }

        if (!owner.isActivity(Activity.EXECUTE)) {
            AnimatedEntity aObj = (AnimatedEntity) owner;
            aObj.setAnimation(owner.getActivity().name + "_HP" + getHP(), true);
        }

        if (inDanger) {
            takeDamage(otherAttackLogic.getDamage(), DamageType.melee);
        }
    }

    @Override
    public void onScreenChanged(boolean onScreen) {
        if (owner != listener.getPlayerEntity()) return;
        if (!onScreen) {
            die();
        }
    }

    @Override
    public void onDamage(int oldHP, int newHP) {
        growTimer = 0;
        nextGrowTime = newGrowTime();
        owner.getPhysics().setGhost(false);
    }

    public boolean isGrowable() {
        return growable && !hasFullHP();
    }

    public void grow() {
        SequenceAction sa = Actions.sequence();
        grow(sa);
        owner.getActor().addAction(sa);
    }

    public void grow(SequenceAction sequence) {
        owner.setActivity(Activity.EXECUTE);

        AnimatedEntity aObj = (AnimatedEntity) owner;
        float duration = aObj.getAnimDuration("grow_HP" + getHP());
        SequenceAction sa = Actions.sequence();
        //delay so the growing object is on screen and can trigger sounds
        sa.addAction(Actions.delay(.1f));
        sa.addAction(EJXActions.setAnimation("grow_HP" + getHP(), false, owner));
        sa.addAction(Actions.delay(duration));
        sa.addAction(EJXActions.setActivity(Activity.IDLE, owner));
        sa.addAction(EJXActions.setAnimation("grown", true, owner));
        incrHitPoints();

        if (hasFullHP()) {
            if (growable) {
                if (owner.hasLogic(VJXLogicType.SPAWN)) {
                    owner.removeLogic(VJXLogicType.SPAWN);
                }
                owner.despawnChildren();
            }
        }

        sequence.addAction(sa);
    }

    public void block(EJXEntity target) {
        if (owner.isActivity(Activity.EXECUTE)) return;
        owner.setTargetEntity(target);
        owner.setActivity(Activity.EXECUTE);

        owner.setLifecycle(Lifecycle.STILL);
        if (owner.hasLogic(VJXLogicType.MOVE))
            ((MoveLogic) owner.getLogic(VJXLogicType.MOVE)).stop();

        AnimatedEntity aObj = (AnimatedEntity) owner;
        aObj.setAnimation(ANIM_BLOCK, false);
        owner.getActor().addAction(Actions.sequence(
                        Actions.delay(aObj.getAnimDuration(ANIM_BLOCK)),
                        EJXActions.setActivity(Activity.IDLE, aObj)));

    }

    private boolean canTakeDmg(DamageType type) {
        if (damageTypes.containsKey(type)) return damageTypes.get(type);
        return false;
    }

    public boolean takeDamage(int dmg, DamageType dmgType) {
        return takeDamage(dmg, dmgType, true);
    }

    public boolean takeDamage(int dmg, DamageType dmgType,
                    boolean setInvulnerable) {

        if (!owner.isAlive() || invulnerable || !canTakeDmg(dmgType)
                        || owner.isActivity(Activity.DISABLED)
                        || hitpoints == 0)
            return false;

        //        VJXLogger.log(owner + " take damage hit points: " + getHP()
        //                      + " -------------------------------> get damage "
        //                      + " alive " + owner.isAlive() + ", type " + dmgType
        //                      + ": " + canTakeDmg(dmgType), 2);

        int oldHP = hitpoints;

        if (thisAttackLogic != null) {
            thisAttackLogic.stopThrow();
        }

        Action suck = null;
        for (Action a : owner.getActor().getActions()) {
            if (a instanceof SuckCoinsAction) {
                suck = a;
                owner.getActor().removeAction(a);
                break;
            }
        }
        owner.getActor().clearActions();

        if (suck != null) owner.getActor().addAction(suck);

        if (dmg >= 0) decrHitPoints(dmg);
        else setHitPoints(0);

        if (hitpoints > 0) {
            if (setInvulnerable)
                setInvulnerable(true, invulTime);
        }

        owner.setActivity(Activity.DISABLED);

        float delay = 0;
        AnimatedEntity aObj = (AnimatedEntity) owner;
        delay = Math.max(aObj.getAnimDuration("damage"),
                        aObj.getAnimDuration("damage_HP" + hitpoints));

        SequenceAction sa = Actions.sequence();
        if (delay > 0) {
            aObj.setAnimation("damage", false);
            aObj.setAnimation("damage_HP" + hitpoints, false);

            sa.addAction(Actions.delay(delay));
            sa.addAction(EJXActions.setActivity(Activity.IDLE, owner));
            owner.getActor().addAction(sa);
        } else {
            owner.setActivity(Activity.IDLE);
        }
        owner.exec(VJXCallback.onDamage, sa);

        Array<EJXLogic> logics = owner.getLogics();
        for (int i = 0; i < logics.size; i++) {
            logics.get(i).onDamage(oldHP, hitpoints);
        }

        if (owner == listener.getPlayerEntity()) {
            LevelStats.hits++;
            listener.getGame().getScreen().getDebugStage().getDebugUI()
            .updateStats(false);
        }

        return true;
    }

    public void die() {
        owner.getActor().clearActions();
        owner.setActivity(Activity.DISABLED);
        owner.setLifecycle(Lifecycle.DEAD);

        AnimatedEntity aObj = (AnimatedEntity) owner;
        SequenceAction sa = Actions.sequence();
        aObj.setAnimation(Activity.DIE.name, false);
        float delay = aObj.getAnimDuration(Activity.DIE.name);
        sa.addAction(Actions.delay(delay));

        owner.exec(VJXCallback.onDie, sa);

        sa.addAction(EJXActions.setAnimation(Lifecycle.DEAD.name, true, owner));
        owner.exec(VJXCallback.onDead, sa);

        if (despawnOnDeath) {
            sa.addAction(Actions.delay(despawnDelay));
            sa.addAction(EJXActions.setAnimation(Lifecycle.DESPAWN.name, false, owner));
            delay = aObj.getAnimDuration(Lifecycle.DESPAWN.name);
            sa.addAction(Actions.delay(delay));
            sa.addAction(Actions.alpha(0f, 1f));
            sa.addAction(Actions.delay(1f));
            sa.addAction(EJXActions.setLifecycle(Lifecycle.DESPAWN, owner));
        }

        owner.getPhysics().setIgnorePassable(false);
        if (owner.getBody() != null) {
            owner.removePhysics(B2DBit.OBJECT);
            owner.removePhysics(B2DBit.TRIGGER);
        }

        owner.getActor().addAction(sa);
        owner.onDie();

        if (owner == listener.getPlayerEntity()) {
            LevelStats.deaths++;

            AdMobController adControl = ((KJGame) listener.getGame()).getAdControl();
            if (adControl != null) {
                adControl.loadAd(KJTypes.KJString.AD_continue);
            }
            listener.getGame().getScreen().getDebugStage().getDebugUI()
            .updateStats(false);
        }

        if (inventoryLogic != null) {
            if (inventoryLogic.isSucking()) {
                owner.getActor().addAction(inventoryLogic.getSuckAction());
            }
        }
    }

    public void revive(EJXEntity camEntity, String skin) {
        if (owner != listener.getPlayerEntity()) return;

        // reset die position
        if (camEntity == owner) {
            MoveLogic mLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
            owner.setPosition(mLogic.getLastSavePosition().add(0, 5), true, true);
            camEntity = null;
        } else {
            if (!camEntity.hasLogic(KJLogicType.DEATHWALL))
                owner.setPosition(camEntity.getWorldCenter().add(0, 5), true, true);
        }

        final AnimatedEntity aObj = (AnimatedEntity) owner;
        final ActionContext context = ((ContextLogic) aObj
                        .getLogic(KJLogicType.CONTEXT)).getContext();
        listener.getUI(KawaidaHUD.class).showContextActionBtn(context,
                        false, null, false);
        EJXEntity currentItem = inventoryLogic.getPickedItem();
        inventoryLogic.setAmount(ItemCategory.POTION, 4);
        owner.updateBody();
        if (!skin.isEmpty()) aObj.setSkin(skin);
        aObj.setLifecycle(Lifecycle.STILL);
        aObj.setActivity(Activity.DISABLED);
        incrHitPoints();
        setInvulnerable(false);
        setInvulnerable(true, 10);
        aObj.setAnimation("revive", false);

        float dur = aObj.getAnimDuration("revive");
        SequenceAction sa = Actions.sequence();
        sa.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                listener.getPlayerInput().setIngameControl(false);
            }
        }));
        sa.addAction(Actions.delay(dur));
        sa.addAction(KJActions.setActivity(Activity.IDLE, aObj));
        sa.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                if (context != ActionContext.NONE)
                    listener.getUI(KawaidaHUD.class).showContextActionBtn(
                                    context, true, aObj.getTargetEntity(), false);
                listener.getPlayerInput().setIngameControl(true);
            }
        }));
        aObj.getActor().addAction(sa);
        inventoryLogic.setPickedItem(currentItem);

        if (camEntity != null) {
            ((AnimatedEntity) camEntity).setAnimation(Activity.IDLE.name, true);
            MoveLogic ml = (MoveLogic) camEntity.getLogic(VJXLogicType.MOVE);
            sa = Actions.sequence();
            sa.addAction(Actions.delay(dur));
            sa.addAction(KJActions.setWPAction(ml.getWaypoint(), camEntity));
            camEntity.getActor().addAction(sa);
            camEntity.setLifecycle(Lifecycle.STILL);
        }
    }

    public void setMaxHP(int newMaxHP) {
        hitpointsMax = newMaxHP > 0 ? newMaxHP : 0;
        if (hitpoints > hitpointsMax) hitpoints = hitpointsMax;
    }

    public int getMaxHP() {
        return hitpointsMax;
    }

    public void setHitPoints(int newHealth) {
        hitpoints = newHealth <= hitpointsMax ? newHealth : hitpointsMax;
    }

    public void incrHitPoints() {
        incrHitPoints(1);
    }

    public void incrHitPoints(int amount) {
        hitpoints = hitpoints + amount >= hitpointsMax ? hitpointsMax
                        : hitpoints + amount;
    }

    public void decrHitPoints() {
        decrHitPoints(1);
    }

    public void decrHitPoints(int amount) {
        if (inventoryLogic != null) {
            int potions = inventoryLogic.getAmount(ItemCategory.POTION);
            if (potions < 0) return;
            if (potions >= amount) {
                inventoryLogic.incr(ItemCategory.POTION, -amount);
                return;
            } else {
                inventoryLogic.setAmount(ItemCategory.POTION, 0);
                decrHP(amount - potions);
            }
        } else decrHP(amount);
    }

    private void decrHP(int amount) {
        hitpoints -= amount;
    }

    public int getHP() {
        return hitpoints;
    }

    public boolean hasFullHP() {
        return hitpoints == hitpointsMax;
    }

    public void setInvulnerable(boolean turnOn) {
        invulnerable = turnOn;
        if (!turnOn) {
            AnimatedEntity aObj = (AnimatedEntity) owner;
            aObj.stopAnim("karafuu", invulAnimTrack);
            listener.getAudioControl().stopSound("karafuu0:sfx:active");
            if (invDurationAction != null) {
                owner.getActor().removeAction(invDurationAction);
                invDurationAction = null;
            }
        }
    }

    public void setInvulnerable(boolean turnOn, float duration) {
        setInvulnerable(turnOn);
        if (turnOn) {
            if (duration > 0) {
                if (invDurationAction != null) {
                    float timeLeft = invDurationAction.getDuration()
                                    - invDurationAction.getTime();
                    invDurationAction.setDuration(timeLeft + duration);
                } else {
                    SequenceAction sa = Actions.sequence();
                    invDurationAction = Actions.delay(duration);
                    sa.addAction(invDurationAction);
                    sa.addAction(KJActions.setInvulnerable(owner, false));
                    owner.getActor().addAction(sa);

                    AnimatedEntity aObj = (AnimatedEntity) owner;
                    invulAnimTrack = aObj.addAnimation("karafuu", true);
                    listener.getAudioControl().playDynSFX("karafuu0:sfx:active");
                }
            }
        } else {
            owner.getActor().removeAction(invDurationAction);
            invDurationAction = null;
        }
    }

    public boolean isInvulnerable() {
        return invulnerable;
    }

    public void setEnemy(boolean isEnemy) {
        this.isEnemy = isEnemy;
    }

    public boolean isEnemy() {
        return isEnemy;
    }

    public boolean canBlock() {
        return canBlock;
    }

    private float newGrowTime() {
        return growTime + MathUtils.random(growRandom);
    }

    public void setInDanger(boolean inDanger) {
        this.inDanger = inDanger;
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {

        if (!other.hasLogic(KJLogicType.ATTACK))
            return false;

        if (other.getTargetEntity() != owner) return false;

        if (!owner.isAlive() || !other.isAlive() || !other.isVisible()
                        || !owner.isVisible()
                        || other.isActivity(Activity.DISABLED))
            return false;

        otherAttackLogic = (AttackLogic) other.getLogic(KJLogicType.ATTACK);

        short thisCat = thisFix.getFilterData().categoryBits;
        short otherCat = otherFix.getFilterData().categoryBits;
        boolean otherDanger = B2DBit.DANGER.isCategory(otherCat);

        if (isEnemy() == otherAttackLogic.isEnemy() || !otherDanger
                        || owner == other.getParent()
                        || other == owner.getParent())
            return false;

        if (B2DBit.HITBOX.isCategory(thisCat)) {
            VJXData data = (VJXData) thisFix.getUserData();
            if (data.getName().contains("life")) {
                data = (VJXData) otherFix.getUserData();

                if (otherAttackLogic.killsInstant()) {
                    if (!owner.isAlive() || invulnerable) return false;
                    die();
                    return true;
                } else {
                    takeDamage(1, DamageType.melee);
                    inDanger = other.getTargetEntity() == owner;
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (!other.hasLogic(KJLogicType.ATTACK))
            return false;

        if (!owner.isAlive() || !other.isAlive() || !other.isVisible()
                        || !owner.isVisible()
                        || other.isActivity(Activity.DISABLED)) {
            inDanger = false;
            return false;
        }

        otherAttackLogic = (AttackLogic) other.getLogic(KJLogicType.ATTACK);

        short thisCat = thisFix.getFilterData().categoryBits;
        short otherCat = otherFix.getFilterData().categoryBits;
        boolean otherDanger = B2DBit.DANGER.isCategory(otherCat);

        if (isEnemy() == otherAttackLogic.isEnemy() || !otherDanger
                        || owner == other.getParent()
                        || other == owner.getParent())
            return false;

        if (B2DBit.HITBOX.isCategory(thisCat)) {
            VJXData data = (VJXData) thisFix.getUserData();
            if (data.getName().contains("life")) {
                inDanger = false;
                return true;
            }
        }

        return false;
    }

    @Override
    public void executeAnimEvent(String name, String string) {
        switch (name) {
            case ANIM_EVENT_GROWN:
                tmpVec.set(owner.getWorldCenter());
                Command growCmd = KJCommands.getGrowDecoCommand(
                                owner.getListener().getGame(),
                                owner.getWCX(), owner.getWCY(),
                                owner.getRange());
                owner.execSingleCmd(growCmd);
                break;
            case ANIM_EVENT_BRANCH_HIT:
                //                LifeLogic ll = (LifeLogic)
                //                listener.getPlayerEntity().getLogic(KJLogicType.LIFE);
                //                ll.takeDamage(1, DamageType.melee);
                //                ((KJLevel) listener.getLevel()).setBonusFound(true);
                break;
            default:
        }
    }
}