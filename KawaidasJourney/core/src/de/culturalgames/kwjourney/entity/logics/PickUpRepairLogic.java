package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.KJLevel;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class PickUpRepairLogic extends EJXLogic {

    private static final String POST_repair = "_repair";

    private static final String ANIM_REPAIR_START = "repair_start";
    private static final String ANIM_REPAIR = "repair";
    private static final String ANIM_REPAIR_END = "repair_end";

    //    public static final String bonus_speedX = "climb:speedX";
    //    public static final String bonus_speedY = "climb:speedY";

    public enum PickUpProperty implements EJXPropertyDefinition {
        //        speedX(ClimbLogic.climb_speedX, PropertyType.floatProp, 1f),
        //        speedY(ClimbLogic.climb_speedY, PropertyType.floatProp, 1f)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private PickUpProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final PickUpProperty get(String name) {
            for (PickUpProperty property : PickUpProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private ClimbLogic climbLogic;
    private InventoryLogic inventoryLogic;

    public PickUpRepairLogic() {
        super(KJLogicType.PICKUP);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(PickUpProperty.values(), vjxProps, properties);
        climbLogic = (ClimbLogic) owner.getLogic(KJLogicType.CLIMB);
        inventoryLogic = (InventoryLogic) owner.getLogic(KJLogicType.INVENTORY);
    }

    public void pickUp() {
        AnimatedEntity aObj = (AnimatedEntity) owner;
        EJXEntity target = aObj.getTargetEntity();
        if (!owner.isActivity(Activity.EXECUTE) && target != null
                        && !target.hasLogic(KJLogicType.LIFE)
                        || target.hasLogic(KJLogicType.LIFE)
                        && target.isLifecycle(Lifecycle.DEAD)) {
            String anim = climbLogic.isClimbing() ? "climb_pickUp" : "pickUp";
            aObj.setAnimation(anim, false);

            float delay = aObj.getAnimDuration(anim);
            owner.interrupt(delay);
            owner.getActor().addAction(Actions.sequence(Actions.delay(
                            aObj.getAnimDuration(anim),
                            EJXActions.setActivity(Activity.IDLE, owner))));
        }
    }

    public void pickPipe() {
        AnimatedEntity aObj = (AnimatedEntity) owner;
        EJXEntity target = aObj.getTargetEntity();
        if (!owner.isActivity(Activity.EXECUTE) && target != null) {
            String anim = climbLogic.isClimbing() ? "climb_pickUp" : "pickUp";
            aObj.setAnimation(anim, false);

            float delay = aObj.getAnimDuration(anim);
            owner.interrupt(delay);
            owner.getActor().addAction(Actions.sequence(Actions.delay(
                            aObj.getAnimDuration(anim),
                            EJXActions.setActivity(Activity.IDLE, owner))));
            inventoryLogic.setPickedItem(target);
        }
    }

    public void repairPipe() {
        AnimatedEntity aObj = (AnimatedEntity) owner;
        EJXEntity target = aObj.getTargetEntity();
        if (!owner.isActivity(Activity.EXECUTE) && target != null) {
            inventoryLogic.setPickedItem(null);
            BrokenLogic bl = (BrokenLogic) target.getLogic(KJLogicType.BROKEN);
            bl.startRepair();
            ((KJLevel) listener.getLevel()).incrLvlProgress();

            aObj.setAnimation(ANIM_REPAIR_START, false);

            float delay0 = aObj.getAnimDuration(ANIM_REPAIR_START);
            float delay1 = aObj.getAnimDuration(ANIM_REPAIR);
            float delay2 = aObj.getAnimDuration(ANIM_REPAIR_END);

            owner.interrupt(delay0 + delay1 + delay2);
            owner.playSfx(owner.getDefName() + POST_repair);
            owner.getActor().addAction(Actions.sequence(
                            Actions.delay(delay0),
                            EJXActions.setAnimation(ANIM_REPAIR, false, owner),
                            Actions.delay(delay1),
                            EJXActions.setAnimation(ANIM_REPAIR_END, false, owner),
                            Actions.delay(delay2),
                            EJXActions.setActivity(Activity.IDLE, owner)));
        }
    }

    @Override
    public SequenceAction interrupt(float delayResume) {
        owner.setOrientation(owner.getTargetEntity());
        return super.interrupt(delayResume);
    }
}