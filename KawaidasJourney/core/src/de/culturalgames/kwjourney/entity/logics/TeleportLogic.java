package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.cmds.KJCommands;
import de.culturalgames.kwjourney.cmds.TeleportCommand;
import de.culturalgames.kwjourney.entity.logics.AttackLogic.AttackProperty;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class TeleportLogic extends EJXLogic {

    public static final String teleport_destination = "port:destination";
    public static final String teleport_speed = "port:speed";

    public enum TeleportProperty implements EJXPropertyDefinition {
        destination(TeleportLogic.teleport_destination, PropertyType.stringProp, VJXString.STR_EMPTY),
        speed(TeleportLogic.teleport_speed, PropertyType.floatProp, 1f)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private TeleportProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final AttackProperty get(String name) {
            for (AttackProperty property : AttackProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private EJXEntity target;
    private Vector2 destination;
    private float speed;
    private float xOff;
    private float yOff;

    private TeleportCommand portCMD;
    private SequenceAction sequence;

    public TeleportLogic() {
        super(KJLogicType.TELEPORT);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(TeleportProperty.values(), vjxProps, properties);
        String destName = vjxProps.get(TeleportProperty.destination, String.class);
        destination = new Vector2();
        speed = vjxProps.get(TeleportProperty.speed, float.class);

        target = listener.getEntityByName(destName);
        if (target != null) {
            portCMD = KJCommands.getPortCommand(owner.getTargetEntity(), 0, 0, speed);
            destination.set(target.getWorldCenter());
        } else owner.removeLogic(getType());
        sequence = Actions.sequence();
    }

    @Override
    public void onActivated(EJXEntity activator, boolean activated) {
        super.onActivated(activator, activated);
        if (portCMD == null) return;
        if (activated) {
            if (activator.hasLogic(VJXLogicType.MOVE))
                ((MoveLogic) activator.getLogic(VJXLogicType.MOVE)).stop(true);
            activator.setActivity(Activity.EXECUTE);
            portCMD.setExecutor(activator);

            destination.set(target.getWorldCenter());
            xOff = -activator.getOriginX();
            yOff = -activator.getOriginY();

            Box2DLogic physicsLogic = activator.getPhysics();
            if (physicsLogic != null) {
                tmpVec.set(0f, -1f);
                yOff = physicsLogic.getDistance(activator.getWorldCenter(),
                                tmpVec, activator.getScaledHeight());
                yOff = -activator.getOriginY() + (yOff - target.getOriginY()) + 5;
                physicsLogic.clearGrounded();
            }
            portCMD.setPosition(destination.x + xOff, destination.y + yOff);
            portCMD.setSpeed(speed);
            sequence.reset();
            activator.execSingleCmd(portCMD, sequence);

            activator.getActor().addAction(sequence);
        }
    }

    @Override
    public void targetInRange(boolean inRange) {
        super.targetInRange(inRange);
        if (portCMD == null) return;
        ContextLogic contextLogic = (ContextLogic) owner.getTargetEntity()
                        .getLogic(KJLogicType.CONTEXT);
        if (contextLogic == null) return;

        if (inRange) {
            contextLogic.setContext(ActionContext.TELEPORT, owner);
        } else {
            contextLogic.setContext(ActionContext.NONE, owner);
        }
    }

    @Override
    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        super.drawShapeDebug(shapeRenderer);
        shapeRenderer.setColor(Color.PINK);
        shapeRenderer.line(owner.getWCX(), owner.getWCY(), destination.x + xOff,
                        destination.y + yOff);
    }
}