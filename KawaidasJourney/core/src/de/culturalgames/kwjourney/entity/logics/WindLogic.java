package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;

public class WindLogic extends EJXLogic {

    public WindLogic() {
        super(KJLogicType.WIND);
    }

    @Override
    public void init(MapProperties properties) {
        float dist = properties.get("move:dist", 256f, float.class);
        boolean right = properties.get("move:right", true, boolean.class);

        Waypoint wp1 = new Waypoint(owner.getDefName() + "1");
        wp1.setCenter(owner.getX(), owner.getWCY());
        Waypoint wp2 = new Waypoint(owner.getDefName() + "2");
        if (right)
            wp2.setCenter(owner.getX() + dist, owner.getWCY());
        else wp2.setCenter(owner.getX() - dist, owner.getWCY());

        MoveLogic moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        moveLogic.addWaypoint(wp1);
        moveLogic.addWaypoint(wp2);
    }
}