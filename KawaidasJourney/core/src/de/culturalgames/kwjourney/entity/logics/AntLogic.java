package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.utils.Array;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class AntLogic extends EJXLogic {

    public static final String ant_viewDistance = "view";

    public enum AntProperty implements EJXPropertyDefinition {
        viewDistance(AntLogic.ant_viewDistance, PropertyType.floatProp, -1f),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private AntProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final AntProperty get(String name) {
            for (AntProperty property : AntProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private boolean follow;

    private MoveLogic moveLogic;
    private AttackLogic attackLogic;

    private EJXEntity target;
    private LifeLogic targetLife;

    private float viewDistance;

    public AntLogic() {
        super(KJLogicType.ANT);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(AntProperty.values(), vjxProps,
                        properties);

        viewDistance = vjxProps.get(AntProperty.viewDistance, Float.class);
        viewDistance = viewDistance == -1 ? owner.getScaledWidth() / 2f : viewDistance;
        //        viewDistance = properties.get(AntProperty.viewDistance.name, -1f, Float.class);

        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        attackLogic = (AttackLogic) owner.getLogic(KJLogicType.ATTACK);

        target = owner.getListener().getPlayerEntity();
        targetLife = (LifeLogic) target.getLogic(KJLogicType.LIFE);

        if (moveLogic.isStill())
            return;

        String name = owner.getName();
        if (moveLogic.getWaypointCount() == 0) {
            Waypoint wp0 = new Waypoint(name + "_wp0");
            wp0.setCenter(owner.getWorldCenter().add(-500, 0));

            Waypoint wp1 = new Waypoint(name + "_wp1");
            wp1.setCenter(owner.getWorldCenter().add(500, 0));

            if (owner.getOrientation() == Orientation.RIGHT) {
                moveLogic.addWaypoint(wp1);
                moveLogic.addWaypoint(wp0);
            } else {
                moveLogic.addWaypoint(wp0);
                moveLogic.addWaypoint(wp1);
            }
        }
    }

    @Override
    public void update(float deltaT) {
        if (!owner.isAlive()) return;
        if (follow) {
            if (owner.isOnScreen()) {
                if (!targetLife.isInvulnerable()) {
                    attackLogic.updateWP();
                } else {
                    follow = false;
                    attackLogic.stopAttack();
                    moveLogic.stop(true);
                }
            } else {
                follow = false;
                attackLogic.stopAttack();
                moveLogic.stop(true);
            }
        }
    }

    @Override
    public void onDamage(int oldHP, int newHP) {
        Array<EJXEntity> ants = listener.getEntitiesByDefName(owner.getDefName());
        for (EJXEntity e : ants) {
            if (e.isAlive() && viewDistance >= owner.getDistance(e)) {
                attackLogic = (AttackLogic) e.getLogic(KJLogicType.ATTACK);
                attackLogic.attackMove(target);
                ((AntLogic) e.getLogic(KJLogicType.ANT)).setFollow(true);
            }
        }
    }

    private void setFollow(boolean follow) {
        this.follow = follow;
    }
}