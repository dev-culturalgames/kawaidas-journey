package de.culturalgames.kwjourney.entity.logics;

import java.util.Iterator;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.MoveLogic.MoveProperty;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXData;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class VehicleLogic extends EJXLogic implements CollisionListener {

    public static final String vehicle_driver = "vehicle:driver";
    public static final String vehicle_passenger = "vehicle:passenger";

    public enum VehicleProperty implements EJXPropertyDefinition {
        driver(VehicleLogic.vehicle_driver, PropertyType.stringProp, VJXString.STR_EMPTY),
        passanger(VehicleLogic.vehicle_passenger, PropertyType.stringProp, VJXString.STR_EMPTY),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private VehicleProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final MoveProperty get(String name) {
            for (MoveProperty property : MoveProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    public VehicleLogic() {
        super(KJLogicType.VEHICLE);
    }

    @Override
    public void init(MapProperties properties) {
        Iterator<String> iter = properties.getKeys();
        String key, value = "";

        while (iter.hasNext()) {
            key = iter.next();
            EJXEntity entity;
            Box2DLogic ml;
            if (key.equals(vehicle_driver)) {
                value = properties.get(key, VJXString.STR_EMPTY, String.class);
                entity = listener.getEntityByName(value);
                if (owner.getPhysics() != null) {
                    ml = owner.getPhysics();
                    ml.attach(entity);
                } else VJXLogger.log(LogCategory.ERROR,
                                "No moveLogic for driver '" + value + "'!");
            }
            else if (key.contains(vehicle_passenger)) {
                value = properties.get(key, VJXString.STR_EMPTY, String.class);
                entity = listener.getEntityByName(value);
                if (owner.getPhysics() != null) {
                    ml = owner.getPhysics();
                    ml.attach(entity);
                } else VJXLogger.log(LogCategory.ERROR,
                                "No moveLogic for passanger '" + value + "'!");
            }
        }
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        short otherCat = otherFix.getFilterData().categoryBits;
        short thisCat = thisFix.getFilterData().categoryBits;
        if (B2DBit.HITBOX.isCategory(otherCat)
                        && B2DBit.TRIGGER.isCategory(thisCat))
            return true;
        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        return false;
    }

    @Override
    public void onTrigger(boolean triggered, VJXData thisData,
                    EJXEntity other, VJXData otherData) {
        other.onTrigger(triggered, null, owner, null);
    }
}