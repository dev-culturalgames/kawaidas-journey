package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.cmds.Commands;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class FireLogic extends EJXLogic {

    private static final String STR_CONTEXT_RANGE = "context:range";
    private static final String fire_smoke = "fire:smoke";

    public enum FireProperty implements EJXPropertyDefinition {
        smoke(fire_smoke, PropertyType.stringProp, "smoke0_clean0")
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private FireProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final FireProperty get(String name) {
            for (FireProperty property : FireProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private String smokeDef;
    private EJXEntity smoke;

    private LifeLogic lifeLogic;
    private boolean inRange;

    private ContextLogic contextLogic;

    public FireLogic() {
        super(KJLogicType.FIRE);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(FireProperty.values(), vjxProps,
                        properties);

        smokeDef = vjxProps.get(FireProperty.smoke, String.class);

        float contextRange = properties.get(STR_CONTEXT_RANGE, 0f, float.class);
        Command cmd = Commands.getSpawnCommand(listener.getGame(), smokeDef,
                        owner, owner.getWCX(), owner.getY(), true);
        listener.execSingleCmd(cmd);

        owner.setRange(contextRange);

        lifeLogic = (LifeLogic) owner.getLogic(KJLogicType.LIFE);
        contextLogic = (ContextLogic) owner.getTargetEntity()
                        .getLogic(KJLogicType.CONTEXT);
    }

    @Override
    public void update(float deltaT) {
        EJXEntity target = owner.getTargetEntity();
        if (contextLogic == null) return;
        if (contextLogic.getContext() == ActionContext.EXTINGUISH) return;
        if (inRange && !(target.isActivity(Activity.RISE)
                        || target.isActivity(Activity.FALL))) {
            contextLogic.setContext(ActionContext.EXTINGUISH, owner);
        }
    }

    @Override
    public void childSpawned(EJXEntity child) {
        super.childSpawned(child);
        if (!child.getDefName().equals(smokeDef)) {
            return;
        }
        lifeLogic.setInvulnerable(false);
        smoke = child;
        child.setPosition(owner.getWCX() - child.getScaledWidth() / 2,
                        owner.getWCY(), true, true);
    }

    @Override
    public void targetInRange(boolean inRange) {
        super.targetInView(inRange);
        if (!owner.isWorldObject())
            return;

        EJXEntity target = owner.getTargetEntity();
        if (inRange && !(target.isActivity(Activity.RISE)
                        || target.isActivity(Activity.FALL)))
            contextLogic.setContext(ActionContext.EXTINGUISH, owner);
        else {
            if (contextLogic.getContext() == ActionContext.EXTINGUISH)
                contextLogic.setContext(ActionContext.NONE, owner);
        }
        this.inRange = inRange;
    }

    @Override
    public void onDespawn() {
        super.onDespawn();
        if (smoke != null) smoke.setLifecycle(Lifecycle.DESPAWN);
        contextLogic.setContext(ActionContext.NONE, owner);
    }
}