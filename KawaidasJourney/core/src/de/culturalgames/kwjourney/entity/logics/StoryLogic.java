package de.culturalgames.kwjourney.entity.logics;

import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Skeleton;

import de.culturalgames.kwjourney.cmds.KJCommands;
import de.culturalgames.kwjourney.cmds.StoryCommand;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.RangeTracker;
import de.venjinx.ejx.entity.logics.RangeTracker.RangeTrackerListener;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class StoryLogic extends EJXLogic implements RangeTrackerListener {

    public static final String character_emotional = "character:emotional";
    public static final String story_range = "story:range";
    public static final String story_rangeX = "story:rangeX";
    public static final String story_rangeY = "story:rangeY";

    public enum StoryProperty implements EJXPropertyDefinition {
        range(story_range, PropertyType.floatProp, 0f),
        rangeX(story_rangeX, PropertyType.floatProp, 0f),
        rangeY(story_rangeY, PropertyType.floatProp, 0f),
        emotional(StoryLogic.character_emotional, PropertyType.boolProp, false);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private StoryProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final StoryProperty get(String name) {
            for (StoryProperty property : StoryProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    protected Skeleton portraitSkeleton;
    protected AnimationState portraitState;

    private List<String> voices;
    private String currentVoice;
    private long currentID;

    private RangeTracker rangeTracker;
    private StoryCommand storyCommand;

    private VJXTrigger trigger;

    public StoryLogic() {
        super(KJLogicType.STORY);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(StoryProperty.values(), vjxProps,
                        properties);

        String type = properties.get("triggerType", VJXString.TRIGGER_RANGE,
                        String.class);
        trigger = VJXTrigger.get(type);

        rangeTracker = new RangeTracker(getType().getName(), this, properties);
        if (owner != listener.getPlayerEntity())
            rangeTracker.track(listener.getPlayerEntity());

        voices = LevelUtil.getVoices(owner.getDefName());

        String dName = properties.get("dialogue", String.class);
        String dCond = properties.get("dialogueCond", String.class);
        storyCommand = KJCommands.getStoryCommand(listener.getGame(), dName, dCond, owner);
    }

    @Override
    public void update(float deltaT) {
        if (!owner.isAlive()) return;

        if (trigger == VJXTrigger.range) rangeTracker.update(owner);
    }

    @Override
    public void onScreenChanged(boolean onScreen) {
        super.onScreenChanged(onScreen);
        if (trigger != VJXTrigger.screen) return;
        if (owner.isOnScreen()) {
            startDialog();
        }
    }

    @Override
    public void targetInRange(EJXEntity target) {
        startDialog();
    }

    @Override
    public void targetOutRange(EJXEntity target) {
    }

    @Override
    public Vector2 getCurrentPosition(Vector2 storeVector) {
        storeVector.set(owner.getX(), owner.getY());
        return storeVector;
    }

    private void startDialog() {
        owner.execSingleCmd(storyCommand);
    }

    public void onDialogStart() {
        owner.exec(VJXCallback.onDialogueStart);
    }

    public void onDialogEnd() {
        owner.exec(VJXCallback.onDialogueEnd);
    }

    public void clearDialog() {
        definition.getCustomProperties().remove("dialogue");
    }

    public void playVoice() {
        if (voices == null) return;
        int index = MathUtils.random(voices.size() - 1);
        currentVoice = owner.getDefName() + VJXString.SEP_DDOT + voices.get(index);
        currentID = owner.playSfx(currentVoice);
    }

    public void stopVoice() {
        owner.stopSfx(currentVoice, currentID);
    }

    @Override
    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(rangeTracker.getX(), rangeTracker.getY(),
                        rangeTracker.getRangeX() * 2,
                        rangeTracker.getRangeY() * 2);
        shapeRenderer.circle(rangeTracker.getX(), rangeTracker.getY(),
                        rangeTracker.getRange());
    }
}