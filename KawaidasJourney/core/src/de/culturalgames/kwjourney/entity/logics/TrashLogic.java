package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class TrashLogic extends EJXLogic {

    public static final String trash_range = "trash:range";
    public static final String trash_rangeX = "trash:rangeX";
    public static final String trash_rangeY = "trash:rangeY";

    public enum TrashProperty implements EJXPropertyDefinition {
        range(TrashLogic.trash_range, PropertyType.floatProp, 256f),
        rangeX(TrashLogic.trash_rangeX, PropertyType.floatProp, 0f),
        rangeY(TrashLogic.trash_rangeY, PropertyType.floatProp, 0f)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private TrashProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final TrashProperty get(String name) {
            for (TrashProperty property : TrashProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private ContextLogic contextLogic;
    private float range;

    public TrashLogic() {
        super(KJLogicType.TRASH);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(TrashProperty.values(), vjxProps, properties);
        range = vjxProps.get(TrashProperty.range, Float.class);
        owner.setRange(range);

        contextLogic = (ContextLogic) owner.getTargetEntity()
                        .getLogic(KJLogicType.CONTEXT);
    }

    @Override
    public void update(float deltaT) {
        if (owner.isActivity(Activity.DISABLED)) {
            EJXEntity target = owner.getTargetEntity();
            EJXEntity trashObject = target.getTargetEntity();
            if (owner.getDistance(target) < owner.getRange()) {
                if (trashObject == null) trashObject = owner;
                else trashObject = owner.getID() > trashObject.getID()
                                || !trashObject.hasLogic(KJLogicType.TRASH)
                                ? owner
                                                : trashObject;
                contextLogic.setContext(ActionContext.TRASH, trashObject);
            } else {
                if (contextLogic.getContext() == ActionContext.TRASH)
                    contextLogic.setContext(ActionContext.NONE, owner);
            }
        }
    }

    @Override
    public void targetInRange(boolean inRange) {
        if (!owner.isWorldObject())
            return;

        if (inRange) {
            EJXEntity target = owner.getTargetEntity();
            EJXEntity trashObject = target.getTargetEntity();
            if (trashObject == null || !trashObject.hasLogic(KJLogicType.TRASH)
                            || !trashObject.isTargetInRange())
                trashObject = owner;
            else trashObject = owner.getID() > trashObject.getID() ? owner
                            : trashObject;

            contextLogic.setContext(ActionContext.TRASH, trashObject);
        }
        else {
            if (contextLogic.getContext() == ActionContext.TRASH)
                contextLogic.setContext(ActionContext.NONE, owner);
        }
    }

    @Override
    public void onDespawn() {
        super.onDespawn();
        contextLogic.setContext(ActionContext.NONE, owner);
    }
}