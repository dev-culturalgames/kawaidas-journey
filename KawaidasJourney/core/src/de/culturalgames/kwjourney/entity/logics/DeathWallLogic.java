package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class DeathWallLogic extends EJXLogic {

    public static final String deathWall_startMoveDelay = "deathWall:startMoveDelay";
    //    public static final String bonus_speedY = "climb:speedY";

    public enum DeathWallProperty implements EJXPropertyDefinition {
        startMoveDelay(DeathWallLogic.deathWall_startMoveDelay, PropertyType.floatProp, 3.5f),
        //        speedY(ClimbLogic.climb_speedY, PropertyType.floatProp, 1f)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private DeathWallProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final DeathWallProperty get(String name) {
            for (DeathWallProperty property : DeathWallProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private static float resetOffX;

    private Waypoint levelEndWP;
    private MoveLogic moveLogic;
    private boolean leading;
    private boolean targetWasDead;
    private float startMoveDelay;
    private float startMoveTimer;

    public DeathWallLogic() {
        super(KJLogicType.DEATHWALL);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(DeathWallProperty.values(), vjxProps, properties);

        startMoveDelay = vjxProps.get(DeathWallProperty.startMoveDelay, Float.class);
        startMoveTimer = -1;

        levelEndWP = Waypoints.getWaypoint("level_end");
        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);

        if (moveLogic != null) moveLogic.setWaypoint(levelEndWP);

        leading = false;
    }

    @Override
    public void update(float delta) {
        if (moveLogic == null) return;
        EJXEntity target = owner.getTargetEntity();
        if (target == null) return;

        EJXCamera cam = (EJXCamera) listener.getLevel().getCamera();

        // check if wall overtook player and update cam focus
        if (!target.isActivity(Activity.DISABLED) && startMoveTimer == -1) {
            if (cam.getCamTarget() == target && owner.getWCX() > target.getWCX()) {
                if (owner.getName().contains("head")) {
                    leading = true;
                    resetOffX = 0;
                    listener.getLevel().setCamFocusObject(owner, true);
                }
            } else if (cam.getCamTarget() == owner && owner.getWCX() < target.getWCX()) {
                if (owner.getName().contains("head")) {
                    leading = false;
                    resetOffX = 0;
                    listener.getGame().getScreen().getLevel()
                    .setCamFocusObject(target, true);
                }
            }
        }

        if (leading) resetOffX = owner.getWCX() - target.getWCX() + 128;

        // stop moving if player dead
        if (!target.isAlive() && !targetWasDead) {
            targetWasDead = true;
            moveLogic.setWaypoint(null);
            moveLogic.stop();
            return;
        }

        // start delayed moving after player revived
        if (target.isAlive() && targetWasDead) {
            targetWasDead = false;
            startMoveTimer = 0;
            resetPosition();
            listener.getLevel().setCamFocusObject(target, true);
            return;
        }

        if (startMoveTimer == -1) return;

        startMoveTimer += delta;
        if (startMoveTimer >= startMoveDelay) {
            moveLogic.setWaypoint(levelEndWP);
            startMoveTimer = -1;
            resetOffX = 0;
            leading = false;
        }
    }

    private void resetPosition() {
        float dist = 1000 + resetOffX;
        owner.setPosition(owner.getX() - dist, owner.getY(), true, true);
    }
}