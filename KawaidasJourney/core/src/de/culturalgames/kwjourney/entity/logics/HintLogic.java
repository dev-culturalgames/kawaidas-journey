package de.culturalgames.kwjourney.entity.logics;

import java.util.HashMap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

import de.culturalgames.kwjourney.cmds.KJCommands;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class HintLogic extends EJXLogic {

    private static HashMap<String, Hint> hints = new HashMap<>();

    private static void addHint(String name, Hint h) {
        if (!hints.containsKey(name))
            hints.put(name, h);
    }

    public static Hint getHint(String name) {
        return hints.get(name);
    }

    public static void clearHints() {
        hints.clear();
    }

    public static class Hint {
        private String name;
        private String showActorName;
        private Actor targetActor;
        private String text;
        private float delay;
        private String hide;
        private boolean reactivateActor;
        private boolean showAlways;
        private EJXEntity object;

        public Hint(String name, String text, String showActorName, String hide,
                        float delay,
                        EJXEntity object) {
            this.name = name;
            this.object = object;
            this.text = text;
            if (showActorName != null) {
                this.showActorName = showActorName;
            }
            this.hide = hide;
            this.delay = delay;
        }

        public String getName() {
            return name;
        }

        public EJXEntity getObject() {
            return object;
        }

        public String getText() {
            // TODO maybe remove hint.getText when localization finished
            String textID = KJTexts.HINT_ + name + TextControl._TEXT;
            return TextControl.getInstance().get(textID, text);
        }

        public String getTargetName() {
            return showActorName;
        }

        public float getDelay() {
            return delay;
        }

        public String getHide() {
            return hide;
        }

        public void setTarget(Actor targetActor) {
            setTarget(targetActor, false);
        }

        public void setTarget(Actor targetActor, boolean reactivateActor) {
            this.targetActor = targetActor;
            this.reactivateActor = reactivateActor;
        }

        public Actor getTarget() {
            return targetActor;
        }

        public boolean reactivateActor() {
            return reactivateActor;
        }

        public void setShowAllways(boolean showAlways) {
            this.showAlways = showAlways;
        }

        public boolean showAllways() {
            return showAlways;
        }
    }

    // TODO this should be used. due to too much entanglement for now range used
    public static final String hint_range = "range";
    public static final String hint_rangeX = "hint:rangeX";
    public static final String hint_rangeY = "hint:rangeY";
    public static final String hint_showAlways = "hint:showAllways";

    public enum HintProperty implements EJXPropertyDefinition {
        range(hint_range, PropertyType.floatProp, 0f),
        rangeX(hint_rangeX, PropertyType.floatProp, 0f),
        rangeY(hint_rangeY, PropertyType.floatProp, 0f),
        showAlways(hint_showAlways, PropertyType.boolProp, false);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private HintProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final HintProperty get(String name) {
            for (HintProperty property : HintProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private float range;
    private float rangeX;
    private float rangeY;
    private boolean showAlways;

    private Circle triggerCircle;
    private Rectangle triggerBox;
    private boolean inRange;

    private VJXTrigger trigger;
    private Command[] triggerCmds;

    public HintLogic() {
        super(VJXLogicType.HINT);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(HintProperty.values(), vjxProps, properties);
        createHint(owner, properties);

        //        rangeRadius = owner.getRange();
        range = vjxProps.get(HintProperty.range, Float.class);
        //        range = properties.get(HintProperty.range.name, -1f, Float.class);
        range = range == -1 ? owner.getScaledWidth() / 2f : range;

        //        if (rangeRadius < 0) {
        //            rangeRadius = Math.max(owner.getWidth() / 2f,
        //                            owner.getHeight() / 2f);
        //        }
        rangeX = vjxProps.get(HintProperty.rangeX, Float.class) * 2f;
        rangeY = vjxProps.get(HintProperty.rangeY, Float.class) * 2f;

        triggerCircle = new Circle(owner.getWorldCenter(), range);
        triggerBox = new Rectangle(0, 0, rangeX, rangeY);

        triggerCmds = new Command[] {
                        KJCommands.getHintCommand(listener.getGame(),
                                        HintLogic.getHint(owner.getName())),
                        KJCommands.getLifecycleCommand(owner, Lifecycle.DESPAWN)
        };

        showAlways = vjxProps.get(HintProperty.showAlways, Boolean.class);

        String type = properties.get(VJXString.TRIGGER_TYPE,
                        VJXString.TRIGGER_RANGE, String.class);
        trigger = VJXTrigger.get(type);
        if (trigger == VJXTrigger.screen)
            owner.setCallBackCommands(VJXTrigger.getCallback(trigger, true),
                            triggerCmds);
    }

    @Override
    public void update(float deltaT) {
        if (owner.isLifecycle(Lifecycle.DESPAWN)) return;
        tmpVec = owner.getWorldCenter();
        triggerBox.setCenter(tmpVec);
        triggerCircle.setPosition(tmpVec);
        checkRange(owner.getTargetEntity());
    }

    private void checkRange(EJXEntity targetEntity) {
        tmpVec = targetEntity.getWorldCenter();
        if (!inRange && (triggerCircle.contains(tmpVec)
                        || triggerBox.contains(tmpVec))) {
            inRange = true;
            owner.execMultiCmds(triggerCmds);
        }
        if (inRange && !triggerBox.contains(tmpVec)
                        && !triggerCircle.contains(tmpVec)) {
            inRange = false;
        }
    }

    private void createHint(EJXEntity object, MapProperties mp) {
        String name = object.getName();
        if (name != null && !name.isEmpty()) {
            String text = mp.get("text", "Hint text not found!", String.class);
            String showActor = mp.get("hint", String.class);
            String hide = mp.get("hide", String.class);
            float delay = mp.get("delay", 0f, Float.class);

            Hint h = new Hint(name, text, showActor, hide, delay, object);
            h.setShowAllways(showAlways);
            addHint(name, h);
        }
    }

    @Override
    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        super.drawShapeDebug(shapeRenderer);
        tmpVec.set(owner.getWorldCenter());
        tmpVec.sub(rangeX, rangeY);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(tmpVec.x, tmpVec.y, rangeX, rangeY);
    }

    @Override
    public String toString() {
        String string = VJXLogicType.HINT.getName() + ":\n";
        string += "    rangeRadius: " + range;
        string += "    rangeX: " + rangeX;
        string += "    rangeY: " + rangeY;
        return string;
    }
}