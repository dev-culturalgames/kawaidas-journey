package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.esotericsoftware.spine.Bone;

import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.PlayerEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class PlantLogic extends EJXLogic implements CollisionListener {

    public static final String ANIM_EVENT_PLANTED = "planted";

    public static final String plant_seeds = "plant:seeds";
    public static final String plant_seedsMax = "plant:seeds_max";

    public enum PlantProperty implements EJXPropertyDefinition {
        seeds(PlantLogic.plant_seeds, PropertyType.intProp, 0),
        seedsMax(PlantLogic.plant_seedsMax, PropertyType.intProp, 0)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private PlantProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final PlantProperty get(String name) {
            for (PlantProperty property : PlantProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private boolean plantTiled;
    private MoveLogic moveLogic;
    private Box2DLogic physicsLogic;
    private int touchesFertile = 0;

    private ContextLogic contextLogic;

    public PlantLogic() {
        super(KJLogicType.PLANT);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(PlantProperty.values(), vjxProps,
                        properties);
        if (owner.getPhysics() != null) {
            physicsLogic = owner.getPhysics();
        }

        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        contextLogic = (ContextLogic) owner.getLogic(KJLogicType.CONTEXT);
    }

    public void plant() {
        if (canPlantTarget()) {
            plant(false);
        } else if (canPlantTile()) {
            plant(true);
        }
    }

    public SequenceAction plant(boolean tiled) {
        SequenceAction sa = Actions.sequence();
        if (owner instanceof PlayerEntity) {
            InventoryLogic inventory;
            inventory = (InventoryLogic) owner.getLogic(KJLogicType.INVENTORY);
            int tools = inventory.getAmount(ItemCategory.TOOL);
            if (tools == 0) return sa;
        }

        moveLogic.stop();
        owner.setActivity(Activity.EXECUTE);

        AnimatedEntity aObj = (AnimatedEntity) owner;
        aObj.setAnimation("plant", false);

        sa.addAction(Actions.delay(aObj.getAnimDuration("plant")));
        sa.addAction(KJActions.setActivity(Activity.IDLE, owner));
        owner.getActor().addAction(sa);
        plantTiled = tiled;
        return sa;
    }

    private void plantAt(float x, float y) {
        if (!plantTiled) {
            EJXEntity target = owner.getTargetEntity();
            x = target.getWorldPosition().x + target.getScaledWidth() / 2f;
            y = target.getWorldPosition().y + target.getScaledHeight() / 2f + 25f;
            target.removeLogic(KJLogicType.FERTILE);
        }
        EJXEntity seedling = listener.createEntity("palmSeedling0");
        seedling.setBirthPlace(x - seedling.getOriginX(), y);
        listener.addEntity(seedling, owner, true);
        listener.getLevel().spawnEntity(seedling);
        InventoryLogic inventory;
        inventory = (InventoryLogic) owner.getLogic(KJLogicType.INVENTORY);
        if (inventory == null) return;
        int tools = inventory.getAmount(ItemCategory.TOOL);
        if (tools > 0) inventory.incr(ItemCategory.TOOL, -1);
    }

    public boolean isPlanting() {
        return owner.isActivity(Activity.DISABLED);
    }

    public boolean touchesPlant() {
        return touchesFertile > 0;
    }

    private boolean canPlantTile() {
        return physicsLogic != null && touchesFertile > 0;
    }

    private boolean canPlantTarget() {
        if (owner.getTargetEntity() == null) return false;
        return owner.getTargetEntity().hasLogic(KJLogicType.FERTILE);
    }

    @Override
    public void executeAnimEvent(String name, String string) {
        switch (name) {
            case ANIM_EVENT_PLANTED:
                Bone b = ((AnimatedEntity) owner).getBone("seedling");
                tmpVec.set(b.getWorldX(), b.getWorldY());
                plantAt(tmpVec.x, tmpVec.y);
                break;
            default:
        }
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (thisFix != owner.getBodyFixture()) return false;
        short otherCat = otherFix.getFilterData().categoryBits;
        if (B2DBit.FERTILE.isCategory(otherCat)) {
            touchesFertile++;
            if (owner instanceof PlayerEntity)
                contextLogic.setContext(ActionContext.PLANT, null);
            return true;
        }
        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (thisFix != owner.getBodyFixture()) return false;
        short otherCat = otherFix.getFilterData().categoryBits;
        if (B2DBit.FERTILE.isCategory(otherCat)) {
            touchesFertile--;
            if (touchesFertile == 0 && owner instanceof PlayerEntity)
                contextLogic.setContext(ActionContext.NONE, null);
            return true;
        }
        return false;
    }
}