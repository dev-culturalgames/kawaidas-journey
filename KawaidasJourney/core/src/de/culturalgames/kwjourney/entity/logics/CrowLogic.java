package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.Activity;

public class CrowLogic extends EJXLogic {

    private static final String SFX_attack = ":sfx:attack";

    private Box2DLogic physicsLogic;
    private MoveLogic moveLogic;
    private AttackLogic attackLogic;
    private EJXEntity attachedTo;
    private Vector2 attachedHomePos;

    private EJXEntity target;
    private LifeLogic targetLife;

    public CrowLogic() {
        super(KJLogicType.CROW);
    }

    @Override
    public void init(MapProperties properties) {
        physicsLogic = owner.getPhysics();
        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        attackLogic = (AttackLogic) owner.getLogic(KJLogicType.ATTACK);

        attachedTo = physicsLogic.getAttachedParent();
        attachedHomePos = new Vector2();
        attachedHomePos.set(physicsLogic.getAttachedOffset());

        target = owner.getListener().getPlayerEntity();
        targetLife = (LifeLogic) target.getLogic(KJLogicType.LIFE);
    }

    @Override
    public void update(float deltaT) {
        if (!owner.isAlive()) return;
        if (moveLogic.isStill()) return;

        if (owner.isTargetInView()) {
            if (targetLife.isInvulnerable() || !target.isAlive()) {
                returnHome();
            } else {
                if (!owner.isTargetInRange()) {
                    moveLogic.setDestination(target.getWCX(), target.getWCY());
                }
            }

            if (attackLogic.isAttacking()) {
                attackLogic.updateWP();
            }
        }
    }

    @Override
    public void positionReached() {
        physicsLogic.attachTo(attachedTo);
        owner.setActivity(Activity.IDLE);
    }

    @Override
    public void targetInView(boolean inView) {
        if (moveLogic.isStill()) return;

        // target enters view start moving
        if (inView && !targetLife.isInvulnerable()) {
            physicsLogic.detach();
            owner.playSfx(owner.getDefName() + SFX_attack);

            moveLogic.setDestination(target.getWCX(), owner.getWCY());
            moveLogic.startMove();
        } else {
            // target leaves view move back home or to attached object
            returnHome();
        }
    }

    @Override
    public void targetInRange(boolean inRange) {
        if (moveLogic.isStill()) return;

        // target enters range start attacking
        if (inRange && !targetLife.isInvulnerable()) {
            attackLogic.attackMove(target);
            owner.playSfx(owner.getDefName() + SFX_attack);
        } else {
            // target leaves range stop attack
            attackLogic.stopAttack();
        }
    }

    private void returnHome() {
        if (attachedTo != null) {
            tmpVec.set(attachedTo.getWorldCenter());
            tmpVec.add(attachedHomePos);
            moveLogic.setDestination(tmpVec);
            moveLogic.startMove();
        } else {
            moveLogic.returnHome();
        }
    }
}