package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.cmds.JumpCommand;
import de.culturalgames.kwjourney.cmds.KJCommands;
import de.culturalgames.kwjourney.cmds.SetControlsCommand;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.EJXCamera.CamMode;
import de.venjinx.ejx.cmds.AnimCommand;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.cmds.GhostCommand;
import de.venjinx.ejx.cmds.MoveCommand;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXData;

public class TrampolineLogic extends EJXLogic {

    private static final String SFX_throw = ":sfx:throw";

    public static final String trampoline_allowMove = "trampoline:allowMove";
    public static final String trampoline_height = "trampoline:height";
    public static final String trampoline_singleUse = "trampoline:singleUse";

    public enum TrampolineProperty implements EJXPropertyDefinition {
        allowMove(TrampolineLogic.trampoline_allowMove, PropertyType.boolProp, true),
        height(TrampolineLogic.trampoline_height, PropertyType.floatProp, 1f),
        singleUse(TrampolineLogic.trampoline_singleUse, PropertyType.boolProp, false)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private TrampolineProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final TrampolineProperty get(String name) {
            for (TrampolineProperty property : TrampolineProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private boolean allowMove;
    private boolean singleUse;

    private float height;

    private Command[] commands;
    private MoveCommand stopCmd;
    private SetControlsCommand setControlsCmd;
    private GhostCommand ghostCmd;
    private JumpCommand jumpCmd;
    private AnimCommand animCmd;

    private EJXEntity target;
    private JumpLogic targetJump;

    public TrampolineLogic() {
        super(KJLogicType.TRAMPOLINE);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(TrampolineProperty.values(), vjxProps,
                        properties);

        allowMove = vjxProps.get(TrampolineProperty.allowMove, Boolean.class);
        singleUse = vjxProps.get(TrampolineProperty.singleUse, Boolean.class);

        height = vjxProps.get(TrampolineProperty.height, Float.class);

        target = owner.getListener().getPlayerEntity();
        targetJump = (JumpLogic) target.getLogic(KJLogicType.JUMP);

        // default
        animCmd = KJCommands.getAnimCommand((AnimatedEntity) owner, "bounce", false);

        jumpCmd = KJCommands.getJumpCommand(target, 1f, true);
        jumpCmd.setTargetString(VJXString.ACTOR_TARGET);

        // no movement / controls allowed
        stopCmd = KJCommands.getMoveCommand(target, true, false);
        stopCmd.setTargetString(VJXString.ACTOR_TARGET);

        setControlsCmd = KJCommands.getControlsCommand(listener.getGame(), false);

        ghostCmd = KJCommands.getGhostCommand(target, true);
        ghostCmd.setTargetString(VJXString.ACTOR_TARGET);

        if (!allowMove) {
            commands = new Command[5];
            commands[0] = stopCmd;
            commands[1] = setControlsCmd;
            commands[2] = ghostCmd;
            commands[3] = animCmd;
            commands[4] = jumpCmd;
        } else {
            commands = new Command[2];
            commands[0] = animCmd;
            commands[1] = jumpCmd;
        }
    }

    @Override
    public void update(float deltaT) {
        if (owner.isActivity(Activity.EXECUTE)) {
            if (!target.isActivity(Activity.DISABLED)
                            && targetJump.stoppedJump()) {

                if (!allowMove) {
                    ghostCmd.setEnable(false);
                    target.execSingleCmd(ghostCmd);

                    setControlsCmd.setActivateControls(true);
                    owner.execSingleCmd(setControlsCmd);

                    owner.setActivity(Activity.IDLE);
                }

                if (singleUse) owner.removeLogic(getType());
                ((EJXCamera) listener.getLevel().getCamera()).setMode(
                                CamMode.FORWARD_FOCUS, CamMode.FORWARD_FOCUS);
            }
        }
    }

    @Override
    public void onTrigger(boolean triggered, VJXData thisData,
                    EJXEntity other, VJXData otherData) {
        if (!thisData.getName().contains("trampoline") || !triggered) return;
        if (other != target) return;

        setControlsCmd.setActivateControls(false);

        stopCmd.setExecutor(target);

        ghostCmd.setEnable(true);
        ghostCmd.setExecutor(target);

        jumpCmd.setExecutor(target);
        jumpCmd.setHeight(height);

        target.setActivity(Activity.DISABLED);
        target.getPhysics().clearGrounded();

        owner.setActivity(Activity.EXECUTE);
        owner.execMultiCmds(commands);
        owner.playSfx(owner.getDefName() + SFX_throw);

        if (singleUse) ((AnimatedEntity) owner).setAnimation("sleep", true);

        EJXCamera cam = (EJXCamera) listener.getLevel().getCamera();
        cam.setMode(CamMode.POSITION_LOCK, CamMode.POSITION_LOCK);
        cam.setLockOffset(cam.position.x - owner.getWCX(),
                        cam.position.y - owner.getWCY());
    }

}