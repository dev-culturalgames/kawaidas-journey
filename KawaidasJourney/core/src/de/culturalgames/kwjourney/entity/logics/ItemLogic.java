package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.cmds.Commands;
import de.venjinx.ejx.cmds.LifecycleCommand;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class ItemLogic extends EJXLogic {

    public static final String SFX_collected = ":sfx:collected";

    public enum ItemCategory {

        // items
        UNDEFINED("undefined", -1), ITEM("item", 0), COIN("coin", 1),
        POTION("potion", 2),  AMMO("ammo", 3), CONTINUE("continue", 4),
        POWERUP("powerUp", 5), COIN_MAGNET("coinMagnet", 6),
        TOOL("tool", 7), NOTE("note", 8), SECRET("secret", 9);

        public final String name;
        public final int id;

        private ItemCategory(String name, int id) {
            this.name = name;
            this.id = id;
        }

        public static final ItemCategory get(int id) {
            for (ItemCategory s : ItemCategory.values())
                if (s.id == id) return s;
            return null;
        }

        public static final ItemCategory get(String name) {
            for (ItemCategory s : ItemCategory.values())
                if (s.name.equals(name)) return s;
            return ItemCategory.UNDEFINED;
        }

        @Override
        public String toString() {
            return name + "(" + id + ")";
        }
    };

    public static final String itemCategory = "itemCategory";

    public static final String item_activatable = "item:activatable";
    public static final String item_amount = "item:amount";
    public static final String item_amountMax = "item:amount_max";
    public static final String item_category = "item:category";
    public static final String item_range = "view";
    public static final String item_duration = "item:duration";
    public static final String item_moveUI = "item:moveUI";

    public enum ItemProperty implements EJXPropertyDefinition {
        activatable(ItemLogic.item_activatable, PropertyType.boolProp, false),
        amount(ItemLogic.item_amount, PropertyType.intProp, 1),
        amountMax(ItemLogic.item_amountMax, PropertyType.intProp, 1),
        category(ItemLogic.item_category, PropertyType.stringProp, VJXString.STR_EMPTY),
        range(ItemLogic.item_range, PropertyType.floatProp, -1f),
        duration(ItemLogic.item_duration, PropertyType.floatProp, 0f),
        moveUI(ItemLogic.item_moveUI, PropertyType.boolProp, true)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private ItemProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final ItemProperty get(String name) {
            for (ItemProperty property : ItemProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private ItemCategory category;

    private float range;

    private int amount;
    private int amountMax;
    private float duration;

    private boolean isCollected;
    private boolean activatable;

    private boolean isEnemy;

    private boolean moveUI;
    private boolean getSucked;

    private MoveLogic moveLogic;

    private LifecycleCommand despawnCmd;

    public ItemLogic() {
        super(KJLogicType.ITEM);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(ItemProperty.values(), vjxProps,
                        properties);
        category = ItemCategory.get(vjxProps.get(ItemProperty.category, String.class));
        amount = vjxProps.get(ItemProperty.amount, Integer.class);
        activatable = vjxProps.get(ItemProperty.activatable, boolean.class);
        duration = vjxProps.get(ItemProperty.duration, Float.class);
        moveUI = vjxProps.get(ItemProperty.moveUI, boolean.class);

        range = vjxProps.get(ItemProperty.range, Float.class);
        range = range == -1 ? owner.getScaledWidth() / 2f : range;

        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        despawnCmd = Commands.getLifecycleCommand(owner, Lifecycle.DESPAWN);
    }

    @Override
    public void update(float deltaT) {
        if (getSucked && owner.isOnScreen() && !isCollected) {
            moveLogic.setDestination(owner.getTargetEntity().getWorldCenter());
        }

        if (owner.getActivity() == Activity.ACTIVATED) {
            tmpVec.set(listener.getPlayerEntity().getWorldCenter());
            owner.setPosition(tmpVec.x, tmpVec.y
                            + listener.getPlayerEntity().getScaledHeight() / 2);
        }
    }

    public void collectedBy(EJXEntity collector) {
        if (isCollected || collector == null)
            return;

        if (moveUI) {
            listener.moveUIItem(owner.getDefName(), owner.getWCX(), owner.getWCY());
        }

        owner.playSfx(owner.getDefName() + SFX_collected);

        isCollected = true;

        if (!activatable) {
            getSucked(false);
            owner.execSingleCmd(despawnCmd);
        } else {
            owner.setVisible(false);
            owner.setActivity(Activity.ACTIVATED);
            owner.getActor().addAction(Actions.sequence(Actions.delay(duration),
                            EJXActions.setLifecycle(Lifecycle.DESPAWN,
                                            owner)));
        }
    }

    public boolean isCollected() {
        return isCollected;
    }

    public void getSucked(boolean activate) {
        getSucked = activate;
    }

    public boolean isGettingSucked() {
        return getSucked;
    }

    public void setEnemy(boolean enemy) {
        isEnemy = enemy;
    }

    public boolean isEnemy() {
        return isEnemy;
    }

    public float getDuration() {
        return duration;
    }

    public void incr() {
        setAmount(amount + 1);
    }

    public void incr(int amount) {
        setAmount(this.amount + amount);
    }

    public void setAmount(int amount) {
        if (amount < 0) {
            if (amountMax < 0)
                amount = -1;
            else amount = 0;
            return;
        }
        if (amount <= amountMax || amountMax <= -1) this.amount = amount;
        else this.amount = amountMax;
    }

    public int getAmount() {
        return amount;
    }

    public void setMax(int max) {
        amountMax = max < 0 ? -1 : max;
    }

    public void incrMax() {
        amountMax = amountMax < 0 ? -1 : amountMax++;
    }

    public void decrMax() {
        amountMax = amountMax < 0 ? 0 : amountMax--;
    }

    public int getMax() {
        return amountMax;
    }

    public ItemCategory getCategory() {
        return category;
    }

    public float getRange() {
        return range;
    }

    @Override
    public void targetInRange(boolean inRange) {
        if (!owner.getTargetEntity().hasLogic(KJLogicType.INVENTORY)) return;
        if (!owner.getTargetEntity().isVisible())
            return;
        InventoryLogic inventory = (InventoryLogic) owner.getTargetEntity()
                        .getLogic(KJLogicType.INVENTORY);
        inventory.collect(this);
    }

}