package de.culturalgames.kwjourney.entity.logics;

import java.util.HashMap;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.actions.game.SuckCoinsAction;
import de.culturalgames.kwjourney.cmds.KJCommands;
import de.culturalgames.kwjourney.cmds.SecretCommand;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class InventoryLogic extends EJXLogic {

    public static final String inventory_range = "inventory:range";
    public static final String inventory_slots = "inventory:slots";
    public static final String inventory_items = "inventory:items";

    public enum InventoryProperty implements EJXPropertyDefinition {
        range(InventoryLogic.inventory_range, PropertyType.floatProp, 0f),
        slots(InventoryLogic.inventory_slots, PropertyType.intProp, 0),
        items(InventoryLogic.inventory_items, PropertyType.stringProp, VJXString.STR_EMPTY)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private InventoryProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final InventoryProperty get(String name) {
            for (InventoryProperty property : InventoryProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private int slots;
    private HashMap<ItemCategory, ItemLogic> items;

    private KJPlayer player;
    private EJXEntity pickedItem;

    private SuckCoinsAction suckAction;
    private boolean sucking = false;

    public InventoryLogic() {
        super(KJLogicType.INVENTORY);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(InventoryProperty.values(), vjxProps,
                        properties);

        slots = vjxProps.get(InventoryProperty.slots, Integer.class);

        items = new HashMap<>(slots);

        String itemsString = vjxProps.get(InventoryProperty.items, String.class);
        String[] itemsArray = itemsString.split(VJXString.SEP_LIST);
        ItemCategory category;
        ItemLogic itemLogic;
        for (String item : itemsArray) {
            category = ItemCategory.get(item);
            itemLogic = new ItemLogic();
            itemLogic.setMax(properties
                            .get(item + VJXString.SEP_DDOT + "amount_max", -1,
                                            Integer.class));
            itemLogic.setAmount(
                            properties.get(item + VJXString.SEP_DDOT + "amount",
                                            -1, Integer.class));
            items.put(category, itemLogic);
        }

        player = (KJPlayer) listener.getPlayer();
        suckAction = null;
    }

    @Override
    public void executeAnimEvent(String name, String string) {
        switch (name) {
            case AnimatedEntity.ANIM_EVENT_PICKUP:
                if (owner.getTargetEntity() != null) {
                    owner.getTargetEntity().onEnd();
                    owner.getTargetEntity().setLifecycle(Lifecycle.DESPAWN);
                }
                break;
            case AnimatedEntity.ANIM_EVENT_REPAIR:
                owner.getTargetEntity().onEnd();
                BrokenLogic bl = (BrokenLogic) owner.getTargetEntity()
                                .getLogic(KJLogicType.BROKEN);
                bl.finishRepair();
                break;
            default:
        }
    }

    public void collect(ItemLogic itmLogic) {
        if (itmLogic.isCollected())
            return;
        EJXEntity item = itmLogic.getOwner();
        float duration = itmLogic.getDuration();
        GameControl listener = owner.getListener();
        boolean collect = true;
        ItemCategory category = itmLogic.getCategory();
        switch (category) {
            // global items
            case COIN:
                incr(category, itmLogic.getAmount() * player.getCoinMultiply());
                break;
            case POTION:
                incr(category, itmLogic.getAmount());
                break;
            case TOOL:
                incr(category, itmLogic.getAmount());
                break;
            case CONTINUE:
                player.incr(category);
                break;
            case NOTE:
                player.incrCollNotes(itmLogic.getAmount());
                break;
            case AMMO:
                incr(category, itmLogic.getAmount());
                break;
            case POWERUP:
                duration += player.getGodDuration();
                if (!owner.hasLogic(KJLogicType.LIFE)) break;
                ((LifeLogic) owner.getLogic(KJLogicType.LIFE))
                .setInvulnerable(true, duration);
                break;
            case COIN_MAGNET:
                float range = itmLogic.getRange();
                range += player.getMagnetRange();
                duration += player.getMagnetDuration();

                suckCoins(range, duration);
                break;
            case SECRET:
                String sString = item.getDef().getCustomProperties().get("secret", VJXString.STR_EMPTY,
                                String.class);
                SecretCommand secret = KJCommands.getSecretCommand(
                                listener.getGame(), sString, item);
                listener.execSingleCmd(secret);
                collect = false;
                break;
            default:
                break;
        }

        if (collect)
            itmLogic.collectedBy(owner);
    }

    private int solarTrack;

    private void suckCoins(float range, float duration) {
        if (duration <= 0) return;

        suck(true);

        if (suckAction != null) {
            suckAction.setDuration(suckAction.getTimeLeft() + duration);
        } else {
            suckAction = KJActions.suckCoins(duration, range, owner);
            suckAction.reset();
            owner.getActor().addAction(suckAction);
            solarTrack = player.getEntity().addAnimation("solarPanel", true);
            listener.getAudioControl().playDynSFX("solarPanel0:sfx:active");
        }
    }

    public void suck(boolean turnOn) {
        if (sucking && turnOn) return;
        sucking = turnOn;
        if (owner == listener.getPlayerEntity()) {
            if (!turnOn) {
                suckAction = null;
                player.getEntity().stopAnim("solarPanel", solarTrack);
                listener.getAudioControl().stopSound("solarPanel0:sfx:active");
            }
        }
    }

    public SuckCoinsAction getSuckAction() {
        return suckAction;
    }

    public boolean isSucking() {
        return sucking;
    }

    public void incr(ItemCategory item, int amount) {
        getItem(item).incr(amount);
        listener.getMenu().updateHUD();
    }

    public void setAmount(ItemCategory item, int amount) {
        if (hasItem(item)) getItem(item).setAmount(amount);
        listener.getMenu().updateHUD();
    }

    public int getAmount(ItemCategory item) {
        if (hasItem(item)) return getItem(item).getAmount();
        return 0;
    }

    public void setAmountMax(ItemCategory item, int max) {
        if (hasItem(item)) getItem(item).setMax(max);
        listener.getMenu().updateHUD();
    }

    public int getAmountMax(ItemCategory item) {
        if (hasItem(item)) return getItem(item).getMax();
        return 0;
    }

    public ItemLogic getItem(ItemCategory item) {
        return items.get(item);
    }

    public boolean hasItem(ItemCategory item) {
        return items.containsKey(item);
    }

    public void clear(ItemCategory item) {
        if (!hasItem(item)) return;

        items.get(item).setAmount(0);
    }

    public void setPickedItem(EJXEntity item) {
        pickedItem = item;
    }

    public EJXEntity getPickedItem() {
        return pickedItem;
    }

    public boolean hasPickedItem() {
        return pickedItem != null;
    }

    public boolean canRepair(EJXEntity entity) {
        if (!hasPickedItem())
            return false;
        int id0 = pickedItem.getDef().getCustomProperties()
                        .get("part", int.class);
        int id1 = entity.getDef().getCustomProperties()
                        .get("part", int.class);
        return id0 == id1;
    }
}