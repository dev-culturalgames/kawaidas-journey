package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.esotericsoftware.spine.Bone;

import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.LifeLogic.DamageType;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.PlayerEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class AttackLogic extends EJXLogic implements CollisionListener {

    public static final String attack_ammo = "attack:ammo";
    public static final String attack_ammo_max = "attack:ammo_max";
    public static final String attack_damage = "attack:damage";
    public static final String attack_directionX = "attack:directionX";
    public static final String attack_directionY = "attack:directionY";
    public static final String attack_isEnemy = "attack:isEnemy";
    public static final String attack_jump = "attack:jump";
    public static final String attack_killsInstant = "attack:killsInstant";
    public static final String attack_melee = "attack:melee";
    public static final String attack_power = "attack:power";
    public static final String attack_range = "attack:range";
    public static final String attack_ranged = "attack:ranged";

    public enum AttackProperty implements EJXPropertyDefinition {
        ammo(AttackLogic.attack_ammo, PropertyType.intProp, 0),
        ammoMax(AttackLogic.attack_ammo_max, PropertyType.intProp, 0),
        damage(AttackLogic.attack_damage, PropertyType.intProp, 1),
        directionX(AttackLogic.attack_directionX, PropertyType.floatProp, 1f),
        directionY(AttackLogic.attack_directionY, PropertyType.floatProp, .15f),
        isEnemy(AttackLogic.attack_isEnemy, PropertyType.boolProp, true),
        killsInstant(AttackLogic.attack_killsInstant, PropertyType.boolProp, false),
        jump(AttackLogic.attack_jump, PropertyType.boolProp, false),
        melee(AttackLogic.attack_melee, PropertyType.boolProp, false),
        power(AttackLogic.attack_power, PropertyType.floatProp, 2f),
        range(AttackLogic.attack_range, PropertyType.floatProp, 0f),
        ranged(AttackLogic.attack_ranged, PropertyType.boolProp, false)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private AttackProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final AttackProperty get(String name) {
            for (AttackProperty property : AttackProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private boolean isEnemy;
    private boolean instantKill;
    private boolean jump;
    private boolean melee;
    private boolean ranged;

    private boolean throwing;
    private boolean attacking;

    private float power;
    private int damage;

    private Vector2 direction;
    public Waypoint attackWP;

    private MoveLogic moveLogic;
    private JumpLogic jumpLogic;
    private InventoryLogic inventoryLogic;

    private SequenceAction attackAction;
    private SequenceAction attackMoveAction;

    private EJXEntity target;
    private LifeLogic targetLife;

    public AttackLogic() {
        super(KJLogicType.ATTACK);
        direction = new Vector2();
        attackWP = new Waypoint("attack_target");
        attackWP.setUpdateActivity(false);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(AttackProperty.values(), vjxProps, properties);

        isEnemy = vjxProps.get(AttackProperty.isEnemy, boolean.class);

        instantKill = vjxProps.get(AttackProperty.killsInstant, Boolean.class);
        jump = vjxProps.get(AttackProperty.jump, Boolean.class);
        melee = vjxProps.get(AttackProperty.melee, Boolean.class);
        ranged = vjxProps.get(AttackProperty.ranged, Boolean.class);

        power = vjxProps.get(AttackProperty.power, Float.class);
        direction.x = vjxProps.get(AttackProperty.directionX, Float.class);
        direction.y = vjxProps.get(AttackProperty.directionY, Float.class);

        damage = vjxProps.get(AttackProperty.damage, Integer.class);

        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        jumpLogic = (JumpLogic) owner.getLogic(KJLogicType.JUMP);
        inventoryLogic = (InventoryLogic) owner.getLogic(KJLogicType.INVENTORY);

        jump = jump && jumpLogic != null;

        attackAction = Actions.sequence();
        attackMoveAction = new SequenceAction();
    }

    @Override
    public void update(float deltaT) {
        if (!owner.isAlive()) return;
        // entity has no target to attack
        if (target == null) {
            return;
        }

        // target died, is untargetable
        if (!target.isAlive() || targetLife.isInvulnerable()) {
            if (!owner.isActivity(Activity.EXECUTE)) {
                stopAttack();
                if (owner.isActivity(Activity.IDLE))
                    if (moveLogic != null) moveLogic.returnHome();
            }
        }

        if (attacking) {
            // entity move to target
            if (moveLogic != null) {
                moveLogic.setDestination(attackWP.getCenter(tmpVec));
            }
        } else {
            stopAttack();
        }
    }

    public void updateWP() {
        updateWP(target);
    }

    public void updateWP(EJXEntity target) {
        if (target == null)
            return;
        attackWP.setCenter(target.getWorldCenter());
    }

    public void attack(boolean move, boolean updateActivity) {
        attack(owner.getTargetEntity(), move, updateActivity);
    }

    public void attack(EJXEntity target, boolean updateActivity) {
        attack(target, false, updateActivity);
    }

    public void attack(EJXEntity target, boolean move, boolean updateActivity) {
        if (move) {
            attackMove(target);
            return;
        }

        if (target == null || target == owner.getParent()) return;
        if (!target.isAlive()) return;

        LifeLogic life = (LifeLogic) target.getLogic(KJLogicType.LIFE);
        if (life == null) return;

        if (jump) {
            if (life.canBlock()) {
                jumpLogic.jump(true);
                life.block(owner);
                return;
            }
        }

        owner.setTargetEntity(target);
        if (!life.takeDamage(getDamage(), DamageType.melee)) return;

        moveLogic.stop();
        owner.setLifecycle(Lifecycle.STILL);

        if (jump) {
            jumpLogic.jump(true);
        } else {
            owner.setActivity(Activity.EXECUTE);

            AnimatedEntity aObj = (AnimatedEntity) owner;
            float delay = 0;
            aObj.setAnimation("idle", true);
            aObj.setAnimation("attack", false);
            delay = aObj.getAnimDuration("attack");

            attackAction.reset();
            attackAction.addAction(Actions.delay(delay));
            if (updateActivity) attackAction.addAction(
                            EJXActions.setActivity(Activity.IDLE, owner));
            owner.getActor().addAction(attackAction);
        }
    }

    public void attackMove(EJXEntity target) {
        if (target == null || target == owner.getParent()) return;
        if (!target.isAlive()) {
            owner.setActivity(Activity.IDLE);
            moveLogic.setWaypoint(moveLogic.nextWaypoint());
            return;
        }
        if (attacking) return;

        attacking = true;
        this.target = target;
        targetLife = (LifeLogic) target.getLogic(KJLogicType.LIFE);

        attackWP.setCenter(target.getWorldCenter());

        if (moveLogic != null) moveLogic.setSpeedMod(1f, 1f);

        owner.setTargetEntity(target);
        owner.setOrientation(target);
        owner.setActivity(Activity.EXECUTE);

        AnimatedEntity aObj = (AnimatedEntity) owner;
        float delay = 0;
        aObj.setAnimation("startAttack", false);
        delay = aObj.getAnimDuration("startAttack");

        owner.getActor().removeAction(attackMoveAction);

        attackMoveAction.reset();
        attackMoveAction.addAction(Actions.delay(delay));
        attackMoveAction.addAction(
                        EJXActions.setLifecycle(Lifecycle.MOVE, owner));

        attackMoveAction.addAction(EJXActions.setWPAction(attackWP, owner));
        attackMoveAction.addAction(
                        EJXActions.setActivity(Activity.EXECUTE, owner));
        attackMoveAction.addAction(
                        EJXActions.setAnimation("attacking", true, aObj));

        owner.getActor().addAction(attackMoveAction);
    }

    public void stopAttack() {
        stopAttack(true);
    }

    public void stopAttack(boolean updateActivity) {
        if (!attacking) return;

        attacking = false;
        target = null;
        targetLife = null;

        if (owner.isAlive()) {
            if (moveLogic != null) {
                moveLogic.setSpeedMod(.5f, .5f);
                moveLogic.setWaypoint(null);
            }
            AnimatedEntity aObj = (AnimatedEntity) owner;
            float delay = 0;
            aObj.setAnimation("stopAttack", false);
            delay = aObj.getAnimDuration("stopAttack");

            owner.getActor().removeAction(attackAction);
            owner.getActor().removeAction(attackMoveAction);

            SequenceAction sa = Actions.sequence();
            if (updateActivity) {
                sa.addAction(Actions.delay(delay));
                sa.addAction(EJXActions.setActivity(Activity.IDLE, owner));
                owner.getActor().addAction(sa);
            }
        }
    }

    public float throwCocos() {
        target = null;
        targetLife = null;
        return throwCocos(direction.x, direction.y, true);
    }

    public float throwCocos(Vector2 dir) {
        target = null;
        targetLife = null;
        return throwCocos(dir.x, dir.y, true);
    }

    public float throwCocos(EJXEntity target) {
        direction.x = vjxProps.get(AttackProperty.directionX, Float.class);
        direction.y = vjxProps.get(AttackProperty.directionY, Float.class);
        this.target = target;
        targetLife = (LifeLogic) target.getLogic(KJLogicType.LIFE);
        owner.setOrientation(target);
        return throwCocos(direction.x, direction.y, true);
    }

    public float throwCocos(boolean updateActivity) {
        target = null;
        targetLife = null;
        return throwCocos(direction.x, direction.y, updateActivity);
    }

    public float throwCocos(Vector2 dir, boolean updateActivity) {
        target = null;
        targetLife = null;
        return throwCocos(dir.x, dir.y, updateActivity);
    }

    public float throwCocos(EJXEntity target, boolean updateActivity) {
        direction.x = vjxProps.get(AttackProperty.directionX, Float.class);
        direction.y = vjxProps.get(AttackProperty.directionY, Float.class);
        this.target = target;
        targetLife = (LifeLogic) target.getLogic(KJLogicType.LIFE);
        owner.setOrientation(target);
        return throwCocos(direction.x, direction.y, updateActivity);
    }

    private float throwCocos(float dirX, float dirY, boolean updateActivity) {
        if (throwing || owner.isActivity(Activity.DISABLED) || !owner.isAlive())
            return 0;

        if (inventoryLogic != null) {
            int ammo = inventoryLogic.getAmount(ItemCategory.AMMO);
            if (ammo == 0) return 0;
        }

        spawning = false;
        throwing = true;

        if (moveLogic != null && moveLogic.isGrounded()) {
            moveLogic.stop();
        }
        AnimatedEntity aObj = (AnimatedEntity) owner;
        direction.set(dirX, dirY);

        float duration = 0;
        String throwName = "throw";

        if (owner.getPhysics() == null || owner.getPhysics().isGrounded()) {
            duration = aObj.getAnimDuration(throwName);
            aObj.setAnimation(throwName, false);
        } else {
            Activity activity = owner.getActivity();
            if (activity == Activity.RISE || activity == Activity.FALL)
                throwName = activity.name + "_throw";
            else if (activity == Activity.CLIMB_IDLE
                            || activity == Activity.CLIMB_RUN
                            || activity == Activity.CLIMB_WALK)
                throwName = "climb_throw";
            duration = aObj.getAnimDuration(throwName);
            aObj.setAnimation(throwName, false);
        }

        Activity oldActivity = owner.getActivity();
        owner.setActivity(Activity.EXECUTE);

        SequenceAction sa = Actions.sequence();
        sa.addAction(Actions.delay(duration));

        if (updateActivity)
            sa.addAction(EJXActions.setActivity(oldActivity, owner));
        sa.addAction(EJXActions.setAnimation(Activity.IDLE.name, true, owner));
        sa.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                throwing = false;
            }
        }));
        owner.getActor().addAction(sa);
        return duration;
    }

    private boolean spawning;

    private void spawnCoco() {
        if (spawning)
            return;

        if (inventoryLogic != null) {
            int ammo = inventoryLogic.getAmount(ItemCategory.AMMO);
            if (ammo == 0) return;
            if (ammo > 0) inventoryLogic.incr(ItemCategory.AMMO, -1);
        }

        spawning = true;
        EJXEntity ammo = listener.createEntity("coconut0_shell");

        Bone b = ((AnimatedEntity) owner).getBone("hand_right");
        tmpVec.set(b.getWorldX(), b.getWorldY());
        tmpVec.sub(ammo.getOriginX(), ammo.getOriginY());
        ammo.setBirthPlace(tmpVec);

        int i = owner.getOrientation() == Orientation.LEFT ? -1 : 1;

        listener.addEntity(ammo, owner);
        listener.getLevel().spawnEntity(ammo);
        ammo.setTargetEntity(target);
        ammo.onScreenChanged(true);

        AttackLogic attackLogic = (AttackLogic) ammo
                        .getLogic(KJLogicType.ATTACK);
        attackLogic.setEnemy(isEnemy());

        MoveLogic moveLogic = (MoveLogic) ammo.getLogic(VJXLogicType.MOVE);
        float R, beta, v0, vX0, vY0;
        float g = B2DWorld.GRAVITY.y * B2DWorld.WORLD_UNIT * moveLogic.getGravityScale();
        if (target == null) {
            R = power * B2DWorld.WORLD_UNIT;
            beta = (float) Math.toRadians(direction.angle());

            v0 = R * g;
            v0 /= Math.sin(2 * beta);
            v0 = (float) Math.sqrt(v0);

            vY0 = (float) (v0 * Math.sin(beta));
            vX0 = (float) (v0 * Math.cos(beta));

            tmpVec.set(power * direction.x * i, power * direction.y);
            tmpVec.scl(B2DWorld.WORLD_UNIT);
            tmpVec.add(ammo.getWorldCenter());

            moveLogic.setDestination(tmpVec);
            moveLogic.startMove();
            moveLogic.setVelocity(vX0 * i, vY0);
        } else {
            float h0 = Math.abs(owner.getWCY() - target.getWCY());
            R = Math.abs(owner.getWCX() - target.getWCX());

            v0 = power * B2DWorld.WORLD_UNIT;

            beta = (float) (Math.pow(R, 2) + Math.pow(h0, 2));
            beta = (float) Math.sqrt(beta) * 2f;
            beta = h0 / beta;
            beta = .5f - beta;
            beta = (float) Math.sqrt(beta);
            beta = (float) Math.asin(beta);

            v0 = (float) (Math.pow(R, 2) + Math.pow(h0, 2));
            v0 = (float) Math.sqrt(v0) * g;
            v0 -= g * h0;
            v0 = (float) Math.sqrt(v0);

            vY0 = (float) (v0 * Math.sin(beta));
            vX0 = (float) (v0 * Math.cos(beta));

            moveLogic.setDestination(target.getWorldCenter());
            moveLogic.startMove();
            moveLogic.setVelocity(vX0 * i, vY0);
        }
        target = null;
        targetLife = null;
    }

    public void stopThrow() {
        throwing = false;
    }

    public int getDamage() {
        return damage;
    }

    public boolean hasMelee() {
        return melee;
    }

    public boolean hasRanged() {
        return ranged;
    }

    public boolean killsInstant() {
        return instantKill;
    }

    public void setEnemy(boolean isEnemy) {
        this.isEnemy = isEnemy;
    }

    public boolean isEnemy() {
        return isEnemy;
    }

    public boolean isAttacking() {
        return attacking;
    }

    public EJXEntity getTarget() {
        return target;
    }

    @Override
    public void onDamage(int oldHP, int newHP) {
        if (newHP > 0) {
            if (moveLogic != null)
                moveLogic.stop();
            stopAttack(false);
        }
    }

    @Override
    public void executeAnimEvent(String name, String string) {
        switch (name) {
            case AnimatedEntity.ANIM_EVENT_THROW:
                spawnCoco();
                break;
            default:
        }
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (!owner.isAlive() || !other.isAlive() || !other.isVisible()
                        || !owner.isVisible()
                        || owner.isActivity(Activity.DISABLED))
            return false;

        LifeLogic lifeLogic = (LifeLogic) other.getLogic(KJLogicType.LIFE);
        if (lifeLogic == null) return false;

        short thisCat = thisFix.getFilterData().categoryBits;
        short otherCat = otherFix.getFilterData().categoryBits;
        boolean otherHit = B2DBit.HITBOX.isCategory(otherCat);

        if (isEnemy() == lifeLogic.isEnemy() || !otherHit
                        || owner == other.getParent()
                        || other == owner.getParent())
            return false;

        if (owner == owner.getListener().getPlayerEntity())
            if (B2DBit.DANGER.isCategory(thisCat)) {
                owner.setTargetEntity(other);
            }

        if (B2DBit.TRIGGER.isCategory(thisCat)
                        && owner.getTargetEntity() == other) {
            if (thisFix.getUserData().toString().contains("passive"))
                return false;

            attack(other, true);
            return true;
        }
        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        return true;
    }

    @Override
    public void drawShapeDebug(ShapeRenderer shapeRenderer) {
        // draw target
        if (owner.getTargetEntity() != null) {
            if (owner instanceof PlayerEntity)
                shapeRenderer.setColor(Color.LIGHT_GRAY);
            else
                shapeRenderer.setColor(Color.RED);
            shapeRenderer.line(owner.getLocalCenter(tmpVec),
                            owner.getTargetEntity().getLocalCenter(
                                            LevelUtil.TMP_VEC0));
        }
    }
}