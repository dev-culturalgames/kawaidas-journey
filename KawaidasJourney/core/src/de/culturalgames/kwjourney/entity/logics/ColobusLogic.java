package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.logics.EJXLogic;

public class ColobusLogic extends EJXLogic {

    private AttackLogic attackLogic;

    public ColobusLogic() {
        super(KJLogicType.COLOBUS);
    }

    @Override
    public void init(MapProperties properties) {
        attackLogic = (AttackLogic) owner.getLogic(KJLogicType.ATTACK);
    }

    @Override
    public void targetInView(boolean inView) {
        if (inView) {
            attackLogic.throwCocos(owner.getTargetEntity());
        }
    }
}