package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class ChickenLogic extends EJXLogic {

    public static final String chicken_pickDelay = "chicken:pickDelay";
    public static final String chicken_pickRandom = "chicken:pickRandom";

    public enum ChickenProperty implements EJXPropertyDefinition {
        pickDelay(ChickenLogic.chicken_pickDelay, PropertyType.floatProp, 1f),
        pickRandom(ChickenLogic.chicken_pickRandom, PropertyType.floatProp, 1f),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private ChickenProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final ChickenProperty get(String name) {
            for (ChickenProperty property : ChickenProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private float pickTime;
    private float pickDelay;
    private float pickRandom;

    private float nextPickTime;

    private MoveLogic moveLogic;
    private AttackLogic attackLogic;

    private EJXEntity target;
    private LifeLogic targetLife;

    public ChickenLogic() {
        super(KJLogicType.CHICKEN);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(ChickenProperty.values(), vjxProps,
                        properties);
        pickDelay = vjxProps.get(ChickenProperty.pickDelay, Float.class);
        pickRandom = vjxProps.get(ChickenProperty.pickRandom, Float.class);

        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        attackLogic = (AttackLogic) owner.getLogic(KJLogicType.ATTACK);
        target = owner.getListener().getPlayerEntity();
        targetLife = (LifeLogic) target.getLogic(KJLogicType.LIFE);
    }

    @Override
    public void update(float deltaT) {
        if (!owner.isAlive()) return;
        if (moveLogic.isStill()) return;
        if (owner.isActivity(Activity.EXECUTE)) return;

        if (owner.isTargetInView()) {
            if (targetLife.isInvulnerable() || !target.isAlive()) {
                moveLogic.returnHome();
                pickTime = 0;
            } else {
                if (!owner.isTargetInRange()) {
                    moveLogic.setDestination(target.getWorldCenter());
                }
            }

            // if target is in range attack logic takes over
            if (attackLogic.isAttacking()) {
                attackLogic.updateWP();
            }
        } else {
            if (owner.isActivity(Activity.IDLE)) {
                pickTime += deltaT;
                if (pickTime >= nextPickTime) {
                    pick();
                    pickTime -= nextPickTime;
                    nextPickTime = pickDelay + MathUtils.random(pickRandom);
                }
            } else pickTime = 0;
        }
    }

    public void pick() {
        owner.setActivity(Activity.EXECUTE);

        AnimatedEntity aObj = (AnimatedEntity) owner;
        float delay = 0;
        aObj.setAnimation("pick", false);
        delay = aObj.getAnimDuration("pick");

        SequenceAction sa = Actions.sequence();
        sa.addAction(Actions.delay(delay));
        sa.addAction(KJActions.setActivity(Activity.IDLE, owner));
        owner.getActor().addAction(sa);
    }

    @Override
    public void targetInView(boolean inView) {
        if (moveLogic.isStill()) return;

        // target enters view start moving
        if (inView && !targetLife.isInvulnerable()) {
            if (!owner.isActivity(Activity.EXECUTE)) {
                moveLogic.setDestination(target.getWorldCenter());
                moveLogic.startMove();
            }
        } else {
            // target leaves view move back home
            moveLogic.returnHome();
        }
    }

    @Override
    public void targetInRange(boolean inRange) {
        if (moveLogic.isStill()) return;

        // target enters range start attacking
        if (inRange && !targetLife.isInvulnerable()) {
            attackLogic.attackMove(target);
        } else {
            // target leaves range stop attack
            attackLogic.stopAttack();
        }
    }
}