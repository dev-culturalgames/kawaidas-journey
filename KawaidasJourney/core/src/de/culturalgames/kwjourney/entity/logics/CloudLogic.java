package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class CloudLogic extends EJXLogic {

    public static final String cloud_delay = "cloud:delay";
    public static final String cloud_delayRandomize = "cloud:delay_randomize";
    public static final String cloud_rangeX = "cloud:rangeX";
    public static final String cloud_rangeXRandomize = "cloud:rangeXRandomize";
    public static final String cloud_rangeY = "cloud:rangeY";
    public static final String cloud_rangeYRandomize = "cloud:rangeYRandomize";
    public static final String cloud_waypoints = "cloud:waypoints";

    public enum CloudProperty implements EJXPropertyDefinition {
        delay(CloudLogic.cloud_delay, PropertyType.floatProp, 5f),
        delayRandomize(CloudLogic.cloud_delayRandomize, PropertyType.floatProp, 10f),
        rangeX(CloudLogic.cloud_rangeX, PropertyType.floatProp, 512f),
        rangeXRandomize(CloudLogic.cloud_rangeXRandomize, PropertyType.floatProp, 1024f),
        rangeY(CloudLogic.cloud_rangeY, PropertyType.floatProp, 512f),
        rangeYRandomize(CloudLogic.cloud_rangeYRandomize, PropertyType.floatProp, 1024f),
        waypoints(CloudLogic.cloud_waypoints, PropertyType.stringProp, VJXString.STR_EMPTY),
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private CloudProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final CloudProperty get(String name) {
            for (CloudProperty property : CloudProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private float delay;
    private float delayRandomize;
    private float rangeX;
    private float rangeXRandomize;
    private float rangeY;
    private float rangeYRandomize;
    private Waypoint destination;

    private float timer;
    private float nextTime;

    private MoveLogic moveLogic;

    public CloudLogic() {
        super(KJLogicType.CLOUD);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(CloudProperty.values(), vjxProps, properties);
        rangeX = vjxProps.get(CloudProperty.rangeX, Float.class);
        rangeXRandomize = vjxProps.get(CloudProperty.rangeXRandomize, Float.class);

        rangeY = vjxProps.get(CloudProperty.rangeY, Float.class);
        rangeYRandomize = vjxProps.get(CloudProperty.rangeYRandomize, Float.class);

        delay = vjxProps.get(CloudProperty.delay, Float.class);
        delayRandomize = vjxProps.get(CloudProperty.delayRandomize, Float.class);

        destination = new Waypoint(owner.getName() + "_dest");
        destination.setCenter(-1000, owner.getWCY());

        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        moveLogic.addWaypoint(destination);
    }

    @Override
    public void update(float deltaT) {
        super.update(deltaT);
        timer += deltaT;
        if (timer >= nextTime) {
            setNewWP();
            timer -= nextTime;
            nextTime = delay + MathUtils.random(delayRandomize);
        }
    }

    @Override
    public void positionReached() {
        super.positionReached();
        setNewWP();
    }

    private void setNewWP() {
        //                xMod *= -1;
        float yMod = MathUtils.random(rangeYRandomize) - rangeY;
        float xMod = MathUtils.random(rangeXRandomize) - rangeX;
        destination.setCenter(owner.getWorldCenter().x + xMod,
                        owner.getWorldCenter().y + yMod);
        moveLogic.setWaypoint(destination);
    }
}
