package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.LevelUtil;

public class PipeLogic extends EJXLogic {

    public static final String pipe_pickable = "pipe:pickable";

    public enum PipesProperty implements EJXPropertyDefinition {
        pickable(pipe_pickable, PropertyType.boolProp, false)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private PipesProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final PipesProperty get(String name) {
            for (PipesProperty property : PipesProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private boolean pickable;

    private ContextLogic contextLogic;

    public PipeLogic() {
        super(KJLogicType.PIPE);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(PipesProperty.values(), vjxProps,
                        properties);

        pickable = vjxProps.get(PipesProperty.pickable, Boolean.class);
        contextLogic = (ContextLogic) owner.getTargetEntity()
                        .getLogic(KJLogicType.CONTEXT);
    }

    public boolean isPickable() {
        return pickable;
    }

    @Override
    public void targetInRange(boolean inRange) {
        super.targetInRange(inRange);
        if (!pickable) return;

        if (inRange) {
            if (contextLogic.canTriggerContext(ActionContext.REPAIR, owner))
                contextLogic.setContext(ActionContext.REPAIR, owner);
            else contextLogic.setContext(ActionContext.NONE, owner);
        } else {
            if (contextLogic.getContext() == ActionContext.REPAIR)
                contextLogic.setContext(ActionContext.NONE, owner);
        }
    }

    @Override
    public void onDespawn() {
        super.onDespawn();
        contextLogic.setContext(ActionContext.NONE, owner);
    }
}
