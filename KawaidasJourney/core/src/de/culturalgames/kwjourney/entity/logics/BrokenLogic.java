package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.util.EJXTypes.ActionContext;

public class BrokenLogic extends EJXLogic {

    private ContextLogic contextLogic;

    public BrokenLogic() {
        super(KJLogicType.BROKEN);
    }

    @Override
    public void init(MapProperties properties) {
        contextLogic = (ContextLogic) owner.getTargetEntity()
                        .getLogic(KJLogicType.CONTEXT);
    }

    @Override
    public void targetInRange(boolean inRange) {
        super.targetInRange(inRange);
        if (inRange) {
            if (contextLogic.canTriggerContext(ActionContext.BROKEN, owner))
                contextLogic.setContext(ActionContext.BROKEN, owner);
            else contextLogic.setContext(ActionContext.NONE, owner);
        } else {
            contextLogic.setContext(ActionContext.NONE, owner);
        }
    }

    public void startRepair() {
        owner.removeLogic(VJXLogicType.SPAWN);
    }

    public void finishRepair() {
        owner.removeLogic(KJLogicType.BROKEN);
        String newFrameName = "pipes/" + owner.getDefName().split("_")[0];
        owner.setFrame(listener.getGame().assets.getRegion("global_decoration",
                        newFrameName));
        contextLogic.setContext(ActionContext.NONE, owner);
    }
}
