package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.Fixture;

import de.culturalgames.kwjourney.cmds.KJCommands;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.PlayerEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.scenegraph.Waypoints;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.VJXDirection;
import de.venjinx.ejx.util.VJXData;

public class BonusCloudLogic extends EJXLogic {

    private EJXEntity playerEntity;
    private boolean bonus;
    private boolean triggered;
    private boolean delayedTriggered;
    private Command[] triggerCmds;
    private String flySfx;

    private long currentSoundID = -1;

    public BonusCloudLogic() {
        super(KJLogicType.BONUS_CLOUD);
    }

    @Override
    public void init(MapProperties properties) {
        bonus = properties.get("bonus", false, Boolean.class);
        Waypoint wpReturn = Waypoints.getWaypoint("bonus_return");

        PlayerEntity target = listener.getPlayerEntity();
        flySfx = "beamCloud0:sfx:fly_normal";
        if (bonus) {
            flySfx = "beamCloud0:sfx:fly_bonus";
            triggerCmds = new Command[] {
                            KJCommands.getControlsCommand(listener.getGame(), false),
                            KJCommands.getBindCommand(owner, target),
                            KJCommands.getAnimCommand(target, Activity.IDLE.name, true),
                            KJCommands.getWPCommand(owner, "bonus_return", false) };

            if (wpReturn != null) {
                wpReturn.setOnReachCmds(new Command[] {
                                KJCommands.getUnbindCommand(owner, target),
                                KJCommands.getJumpCommand(target, 1f, true),
                                //                                Commands.getEntityFlowCommand(EntityFlow.DISABLE, owner),
                                KJCommands.getEnableCommand(owner, false),
                                KJCommands.getControlsCommand(listener.getGame(), true),
                                //                                Commands.getEntityFlowCommand(EntityFlow.GHOST, owner),
                                KJCommands.getGhostCommand(owner, true),
                                KJCommands.getAppearCommand((AnimatedEntity) owner,
                                                false, .25f),
                                KJCommands.getLifecycleCommand(owner, Lifecycle.DESPAWN)
                });
            }
        }
        playerEntity = owner.getListener().getPlayerEntity();
    }

    @Override
    public void update(float deltaT) {
        if (owner.isActivity(Activity.FLY)) {
            if (currentSoundID == -1)
                currentSoundID = owner.playSfx(flySfx, true);
        } else {
            if (currentSoundID != -1) {
                owner.stopSfx(flySfx, currentSoundID);
                currentSoundID = -1;
            }
        }
        if (delayedTriggered && playerEntity.isActivity(Activity.IDLE)) {
            trigger();
        }
    }

    @Override
    public void onTrigger(boolean triggered, VJXData thisData,
                    EJXEntity other, VJXData otherData) {
        delayedTriggered = false;
        if (other != playerEntity) return;

        float height = thisData.getHeight() * B2DWorld.B2D_TO_WORLD;
        Fixture f = owner.getPhysics().getFixture(thisData);
        tmpVec.set(B2DWorld.getFixtureCenter(f, false));
        tmpVec.x = playerEntity.getWCX();
        tmpVec.y -= height / 2f;
        float distance = owner.getPhysics().getDistance(tmpVec, VJXDirection.UP,
                        height * 2, B2DBit.TRIGGER);
        if (distance < 0) return;

        delayedTriggered = triggered;
        if (other.getActivity() != Activity.FALL) return;
        if (triggered) {
            trigger();
        }
    }

    private void trigger() {
        if (triggered) return;
        triggered = true;
        owner.execMultiCmds(triggerCmds);
    }
}