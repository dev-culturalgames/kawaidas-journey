package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.LifeLogic.DamageType;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;

public class CocoLogic extends EJXLogic implements CollisionListener {

    // Declare variable names for loading from Tiled properties
    //      Identifier                         Tiled
    // <logicName>_<varName> <-> "<logicNamy>:<varName>"
    public static final String template_strVar = "template:strVar";
    //    public static final String template_intVar = "template:intVar";
    //    public static final String template_floatVar = "template:floatVar";
    //    public static final String template_boolVar = "template:boolVar";

    public enum CocoProperty implements EJXPropertyDefinition {
        strValue(template_strVar, PropertyType.stringProp, VJXString.STR_EMPTY);
        //        intValue(template_intVar, PropertyType.intProp, 0),
        //        floatValue(template_floatVar, PropertyType.floatProp, 0f),
        //        boolValue(template_boolVar, PropertyType.boolProp, false);

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private CocoProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final CocoProperty get(String name) {
            for (CocoProperty property : CocoProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private MoveLogic moveLogic;
    private AttackLogic attackLogic;

    public CocoLogic() {
        super(KJLogicType.COCO);
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(CocoProperty.values(), vjxProps,
                        properties);
        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        attackLogic = (AttackLogic) owner.getLogic(KJLogicType.ATTACK);
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (attackLogic == null) { return false; }
        if (!other.isVisible() || !owner.isVisible() || !other.isAlive())
            return false;

        short otherCat = otherFix.getFilterData().categoryBits;

        if (owner == other.getParent()
                        || other == owner.getParent())
            return false;

        if (B2DBit.SOLID.isCategory(otherCat)
                        || B2DBit.FERTILE.isCategory(otherCat)) {
            bounceOff();
            return true;
        }

        LifeLogic lifeLogic = (LifeLogic) other.getLogic(KJLogicType.LIFE);
        if (lifeLogic == null) return false;

        if (B2DBit.HITBOX.isCategory(otherCat)
                        && attackLogic.isEnemy() != lifeLogic.isEnemy()) {
            lifeLogic.takeDamage(1, DamageType.ranged);
            lifeLogic.setInDanger(false);
            bounceOff();
            return true;
        }
        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        return false;
    }

    private void bounceOff() {
        if (moveLogic != null) {
            moveLogic.setVelocity(0, 768f);
            owner.getPhysics().setGhost(true);
            owner.playSfx(owner.getDefName() + VJXString.SEP_DDOT + "sfx:hit");
        }
    }
}