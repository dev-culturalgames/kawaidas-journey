package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.cmds.Commands;
import de.venjinx.ejx.entity.logics.EJXLogic;

public class TutorLogic extends EJXLogic {

    public TutorLogic() {
        super(KJLogicType.TUTOR);
    }

    @Override
    public void init(MapProperties properties) {
        Commands.insertOnCommand(VJXCallback.onDialogueEnd,
                        "this:delay:0.5f\nthis:disappear:1\nthis:despawn",
                        properties, 0);
    }

    @Override
    public void onRespawn() {
        super.onRespawn();
        owner.setVisible(false);
    }
}