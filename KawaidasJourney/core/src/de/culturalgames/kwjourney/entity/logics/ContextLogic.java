package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Array;

import de.culturalgames.kwjourney.KJLevel;
import de.culturalgames.kwjourney.KawaidaHUD;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.ui.menus.MapMenu;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.CollisionObject;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics;
import de.venjinx.ejx.entity.logics.EJXLogics.EJXLogicType;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.VJXDirection;

public class ContextLogic extends EJXLogic implements CollisionListener {

    private ActionContext currentContext = ActionContext.NONE;

    private Box2DLogic physicsLogic;
    private InventoryLogic inventoryLogic;
    private PlantLogic plantLogic;

    public ContextLogic() {
        super(KJLogicType.CONTEXT);
    }

    @Override
    public void init(MapProperties properties) {
        inventoryLogic = (InventoryLogic) owner.getLogic(KJLogicType.INVENTORY);
        plantLogic = (PlantLogic) owner.getLogic(KJLogicType.PLANT);
        physicsLogic = owner.getPhysics();
    }

    public void setContext(ActionContext context, EJXEntity target) {
        if (owner != listener.getPlayerEntity()) return;
        if (!owner.isAlive()) return;

        if (context == ActionContext.NONE) {
            currentContext = ActionContext.NONE;
            owner.setTargetEntity(null);

            checkContext(ActionContext.ALL);
            if (currentContext == ActionContext.NONE)
                showContext(context, false, target);
            return;
        }

        if (currentContext.prio > context.prio) return;

        if (target != null || owner.getTargetEntity() == null)
            owner.setTargetEntity(target);
        else {
        }

        if (showContext(context, true, target)) currentContext = context;
    }

    public ActionContext getContext() {
        return currentContext;
    }

    public boolean checkContext(ActionContext context) {
        if (context == null || listener == null) return false;

        EJXEntity newTarget = null;
        if (context == ActionContext.NONE) {
            showContext(context, false, null);
            return false;
        }

        newTarget = owner.getTargetEntity();
        float dist = 0, minX = Float.MAX_VALUE, targetRange;

        EJXLogicType logic = null;
        if (context.isLogic) logic = EJXLogics.getType(context.name);

        if (logic != null) {

            Array<EJXEntity> entities = listener.getEntitiesByLogic(logic);
            for (EJXEntity entity : entities) {
                if (!entity.isWorldObject()
                                || !canTriggerContext(context, entity))
                    continue;

                dist = entity.getDistance(owner);
                targetRange = entity.getRange();
                if (dist <= targetRange && dist < minX) {
                    minX = dist;
                    newTarget = entity;
                }
            }

            if (newTarget != null) {
                setContext(context, newTarget);
                return true;
            } else return checkContext(
                            ActionContext.getByPrio(context.prio - 1));
        } else {

            boolean touchesContex = false;
            if (plantLogic == null)
                plantLogic = (PlantLogic) owner.getLogic(KJLogicType.PLANT);
            if (plantLogic != null)
                touchesContex |= context == ActionContext.PLANT && plantLogic.touchesPlant();
            if (touchesContex) {
                setContext(context, null);
                return true;
            } else return checkContext(
                            ActionContext.getByPrio(context.prio - 1));
        }
    }

    public boolean canTriggerContext(ActionContext logic, EJXEntity entity) {
        switch (logic) {
            case REPAIR:
                if (!entity.hasLogic(KJLogicType.PIPE)) return false;
                return ((PipeLogic) entity.getLogic(KJLogicType.PIPE))
                                .isPickable()
                                && !inventoryLogic.hasPickedItem();
            case BROKEN:
                return inventoryLogic.canRepair(entity);
            case EXTINGUISH:
                return entity.isAlive() && entity.hasLogic(KJLogicType.FIRE);
            default:
                return true;
        }
    }

    private boolean showContext(ActionContext context, boolean on,
                    EJXEntity contextEntity) {
        if (context == ActionContext.JUMP_DOWN) return on;
        return listener.getUI(KawaidaHUD.class).showContextActionBtn(context,
                        on, contextEntity, false);
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {

        if (other instanceof CollisionObject && other.getLayer().getName()
                        .equals(MapMenu.LAYER_path)) {
            short otherCat = otherFix.getFilterData().categoryBits;
            KJLevel level = (KJLevel) listener.getLevel();
            if (level.isGroundDeadly() && B2DBit.SOLID.isCategory(otherCat)) {
                if (owner.hasLogic(KJLogicType.LIFE))
                    ((LifeLogic) owner.getLogic(KJLogicType.LIFE)).die();
            }
        }

        if (owner.getBodyFixture() == thisFix) {
            if (physicsLogic.touchesB2DBit(B2DBit.PASSABLE, VJXDirection.DOWN))
                setContext(ActionContext.JUMP_DOWN, null);
            return true;
        }
        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        if (owner.getBodyFixture() == thisFix
                        && currentContext == ActionContext.JUMP_DOWN) {
            if (!physicsLogic.touchesB2DBit(B2DBit.PASSABLE, VJXDirection.DOWN))
                setContext(ActionContext.NONE, null);
            return true;
        }
        return false;
    }
}
