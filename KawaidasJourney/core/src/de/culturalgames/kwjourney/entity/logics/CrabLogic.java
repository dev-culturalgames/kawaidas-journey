package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.scenegraph.Waypoints.Waypoint;
import de.venjinx.ejx.util.EJXTypes.Orientation;

public class CrabLogic extends EJXLogic {

    private MoveLogic moveLogic;

    public CrabLogic() {
        super(KJLogicType.CRAB);
    }

    @Override
    public void init(MapProperties properties) {
        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);

        if (moveLogic.isStill())
            return;

        if (moveLogic.getWaypointCount() == 0) {
            String name = owner.getName();
            Waypoint wp0 = new Waypoint(name + "_wp0");
            wp0.setCenter(owner.getWorldCenter().add(-500, 0));

            Waypoint wp1 = new Waypoint(name + "_wp1");
            wp1.setCenter(owner.getWorldCenter().add(500, 0));

            if (owner.getOrientation() == Orientation.RIGHT) {
                moveLogic.addWaypoint(wp1);
                moveLogic.addWaypoint(wp0);
            } else {
                moveLogic.addWaypoint(wp0);
                moveLogic.addWaypoint(wp1);
            }
        }
    }
};