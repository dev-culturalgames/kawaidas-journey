package de.culturalgames.kwjourney.entity.logics;

import de.venjinx.ejx.entity.logics.EJXLogics;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class KJLogics extends EJXLogics {

    public enum KJLogicType implements EJXLogicType {
        ANT("ant"), ATTACK("attack"),
        BONUS("bonus"),
        BONUS_CLOUD("bonusCloud"), BROKEN("broken"),
        BUMPER("bumper"), STORY("character"),
        CHICKEN("chicken"), CLIMB("climb"), CLOUD("cloud"),
        COCO("coco"), COLOBUS("colobus"), CONTEXT("context"), CRAB("crab"),
        CROW("crow"), DIALOGUE("dialogue"), DEATHWALL("deathWall"),
        EXTINGUISH("extinguish"), FERTILE("fertile"), FIRE("fire"), HINT("hint"),
        INVENTORY("inventory"), ITEM("item"), JUMP("jump"), LIFE("life"),
        MARABU("marabu"), MOSQUITO("mosquito"), NOTE("note"), PICKUP("pickUp"),
        PIPE("pipe"), PLANT("plant"), PLASTIC_BAG("plasticBag"),
        STEAMGHOST("steamGhost"),
        TELEPORT("teleport"), TRAMPOLINE("trampoline"),
        TRAP("trap"), TRASH("trash"), TUTOR("tutor"), VEHICLE("vehicle"),
        WIND("wind")
        ;

        public final String name;

        private KJLogicType(String name) {
            this.name = name;
        }

        public EJXLogicType get(String name) {
            for (KJLogicType logic : KJLogicType.values())
                if (logic.name.equals(name))
                    return logic;
            VJXLogger.log(LogCategory.WARNING, "Logic '" + name + "' not found!");
            return null;
        }

        @Override
        public String getName() {
            return name;
        }
    }
}