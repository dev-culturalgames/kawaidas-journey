package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.maps.MapProperties;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;

public class MosquitoLogic extends EJXLogic {

    private MoveLogic moveLogic;
    private AttackLogic attackLogic;

    private EJXEntity target;
    private LifeLogic targetLife;

    public MosquitoLogic() {
        super(KJLogicType.MOSQUITO);
    }

    @Override
    public void init(MapProperties properties) {
        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);
        attackLogic = (AttackLogic) owner.getLogic(KJLogicType.ATTACK);

        target = owner.getListener().getPlayerEntity();
        targetLife = (LifeLogic) target.getLogic(KJLogicType.LIFE);
    }

    @Override
    public void update(float deltaT) {
        if (!owner.isAlive()) return;
        if (moveLogic.isStill()) return;

        if (owner.isTargetInView()) {
            if (targetLife.isInvulnerable() || !target.isAlive()) {
                moveLogic.returnHome();
            } else {
                if (!owner.isTargetInRange()) {
                    moveLogic.setDestination(target.getWorldCenter());
                }
            }

            // if target is in range attack logic takes over
            if (attackLogic.isAttacking()) {
                attackLogic.updateWP();
            }
        }
    }

    @Override
    public void targetInView(boolean inView) {
        if (moveLogic.isStill()) return;

        // target enters view start moving
        if (inView && !targetLife.isInvulnerable()) {
            moveLogic.setDestination(target.getWorldCenter());
            moveLogic.startMove();
        } else {
            // target leaves view move back home
            moveLogic.returnHome();
        }
    }

    @Override
    public void targetInRange(boolean inRange) {
        if (moveLogic.isStill()) return;

        // target enters range start attacking
        if (inRange && !targetLife.isInvulnerable()) {
            attackLogic.attackMove(target);
        } else {
            // target leaves range stop attack
            attackLogic.stopAttack();
        }
    }
}