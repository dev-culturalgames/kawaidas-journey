package de.culturalgames.kwjourney.entity.logics;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.KawaidaHUD;
import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.controllers.CollisionListener;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.logics.Box2DLogic;
import de.venjinx.ejx.entity.logics.EJXLogic;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.stages.LevelStage.B2DWorld;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.B2DBit;
import de.venjinx.ejx.util.EJXTypes.EJXPropertyDefinition;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.PropertyType;
import de.venjinx.ejx.util.EJXTypes.VJXDirection;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.LevelUtil;
import de.venjinx.ejx.util.VJXData;

public class ClimbLogic extends EJXLogic implements CollisionListener {

    private static final String SFX_jump_down = ":sfx:jump_down";

    public static final String anim_climbDown = "climb_down";
    public static final String anim_climbUp = "climb_up";

    public static final String climb_speedX = "climb:speedX";
    public static final String climb_speedY = "climb:speedY";

    public enum ClimbProperty implements EJXPropertyDefinition {
        speedX(ClimbLogic.climb_speedX, PropertyType.floatProp, 1f),
        speedY(ClimbLogic.climb_speedY, PropertyType.floatProp, 1f)
        ;

        public final String name;
        public final PropertyType type;
        public final Object dfltValue;

        private ClimbProperty(final String name, final PropertyType type,
                        Object dfltValue) {
            this.name = name;
            this.type = type;
            this.dfltValue = dfltValue;
        }

        public static final ClimbProperty get(String name) {
            for (ClimbProperty property : ClimbProperty.values())
                if (property.name.equals(name)) return property;
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public PropertyType getType() {
            return type;
        }

        @Override
        public Object getDefault() {
            return dfltValue;
        }
    }

    private Orientation alignOrientation;

    private Runnable delayedStart = new Runnable() {
        @Override
        public void run() {
            alignClimbX(false);
            //            physicsLogic.clearGrounded();
            updateVelocity(climbDir.set(0, 1), true);
        }
    };

    private void alignClimbX(boolean now) {
        float climbCollX = 0, climbCollY, distance;
        Fixture alignFixture;
        float moveX = owner.getWCX();
        alignOrientation = owner.getOrientation();
        climbCollY = B2DWorld.getFixtureWorldCenter(climbTopFixture).y;
        if (climbAlignLeftCounter >= 0) {
            alignFixture = climbAlignLeftFixture;
            climbCollX = B2DWorld.getFixtureWorldPosition(alignFixture).x;
            if (alignOrientation == Orientation.RIGHT) {
                climbAlignLeft = false;
                climbAlignRight = true;
                climbCollX += B2DWorld.getFixtureWidth(alignFixture);
            } else {
                climbAlignLeft = true;
                climbAlignRight = false;
            }
        } else if (climbAlignRightCounter >= 0) {
            alignFixture = climbAlignRightFixture;
            climbCollX = B2DWorld.getFixtureWorldPosition(alignFixture).x;
            if (alignOrientation == Orientation.RIGHT) {
                climbAlignLeft = true;
                climbAlignRight = false;
            } else {
                climbAlignLeft = false;
                climbAlignRight = true;
                climbCollX += B2DWorld.getFixtureWidth(alignFixture);
            }
        }

        tmpVec.set(climbCollX, climbCollY);
        if (climbAlignLeft) {
            distance = physicsLogic.getDistance(tmpVec,
                            VJXDirection.LEFT.direction, 64f, B2DBit.CLIMBABLE);
            moveX -= distance >= 1 ? distance : 0;
        } else if (climbAlignRight) {
            distance = physicsLogic.getDistance(tmpVec,
                            VJXDirection.RIGHT.direction, 64f,
                            B2DBit.CLIMBABLE);
            moveX += distance >= 1 ? distance : 0;
        }

        if (now) {
            owner.setPosition(moveX - owner.getScaledWidth() / 2f,
                            owner.getWorldPosition().y, true, true);
        }
        else moveLogic.setDestination(moveX, owner.getWCY() + 64f);
        owner.setOrientation(alignOrientation);
    }

    private Runnable climbStarted = new Runnable() {
        @Override
        public void run() {
            climbAlignLeft = false;
            climbAlignRight = false;
            moveLogic.stop();
            setIdle();
            updateActivity(velocity);
            listener.getPlayerInput().setIngameControl(true);
        }
    };

    private Runnable stopMove = new Runnable() {
        @Override
        public void run() {
            if (isClimbing())
                return;
            physicsLogic.setIgnorePassable(false);
            physicsLogic.detach();
            owner.setActivity(Activity.IDLE);
            moveLogic.setIgnoreGravity(false);
        }
    };

    private Runnable startMove = new Runnable() {
        @Override
        public void run() {
            updateVelocity(climbDir, true);
            moveLogic.setDestination(owner.getWCX(),
                            owner.getWCY() - climbUpHeight);
            moveLogic.startMove();
            moveLogic.setVelocity(0, -climbUpSpeed);
        }
    };

    private Runnable startClimb = new Runnable() {

        @Override
        public void run() {
            startClimb(VJXDirection.DOWN.direction);
        }
    };

    private float speedX;
    private float speedY;
    private boolean climbing;
    private boolean climbDown;
    private boolean climbUp;
    private Vector2 climbDir;
    private Vector2 velocity;

    private MoveLogic moveLogic;
    private Box2DLogic physicsLogic;

    private Fixture climbStartFixture;
    private Fixture climbTopFixture;
    private Fixture climbStopTopFixture;
    private Fixture climbBottomFixture;
    private Fixture climbStopBottomFixture;
    private int touchesClimbStart;
    private int touchesClimbTop;
    private int touchesClimbStopTop;
    private int touchesClimbBottom;
    private int touchesClimbStopBottom;

    private Fixture climbAlignLeftFixture;
    private Fixture climbAlignRightFixture;
    private int climbAlignLeftCounter = 0;
    private int climbAlignRightCounter = 0;
    private boolean climbAlignLeft = true;
    private boolean climbAlignRight = true;

    private float climbUpSpeed;
    private float climbUpHeight;

    public ClimbLogic() {
        super(KJLogicType.CLIMB);
        climbDir = new Vector2();
        velocity = new Vector2();
    }

    @Override
    public void init(MapProperties properties) {
        LevelUtil.convertProperties(ClimbProperty.values(), vjxProps, properties);

        speedX = vjxProps.get(ClimbProperty.speedX, Float.class);
        speedY = vjxProps.get(ClimbProperty.speedY, Float.class);
        moveLogic = (MoveLogic) owner.getLogic(VJXLogicType.MOVE);

        if (owner.getPhysics() != null) physicsLogic = owner.getPhysics();

        updateFixtureLinks();
    }

    @Override
    public void fixturesCreated() {
        updateFixtureLinks();
    }

    @Override
    public void update(float deltaT) {
        if (owner.isActivity(Activity.RISE) || owner.isActivity(Activity.FALL))
            stopClimbing();

        if (isClimbing()) {
            physicsLogic.detach();
            if (!touchesClimb()) {
                stopClimbing();
                owner.setActivity(Activity.IDLE);
                return;
            }
        }

        switch (owner.getLifecycle()) {
            case MOVE:
                switch (owner.getActivity()) {
                    case RISE:
                    case FALL:
                    case CLIMB_IDLE:
                    case IDLE:
                        climbDown = false;
                        climbUp = false;
                        break;
                    case WALK:
                    case RUN:
                        //                        startClimb(climbDir);
                        break;
                    case CLIMB_WALK:
                    case CLIMB_RUN:
                        updateClimbDir();
                        if (!canClimb(climbDir)) {
                            moveLogic.stop();
                            owner.setActivity(Activity.CLIMB_IDLE);
                        } else {
                            moveLogic.moveDirection(climbDir, true);
                            updateVelocity(climbDir, false);
                            moveLogic.move(deltaT);
                        }
                        break;
                    default:
                }
            default:
        }
        if (isClimbing() && !owner.isActivity(Activity.EXECUTE)
                        && !owner.isActivity(Activity.DISABLED))
            updateActivity(moveLogic.getVelocity());
    }

    @Override
    public void onDamage(int oldHP, int newHP) {
        if (!climbing) return;
        listener.getPlayerInput().setIngameControl(true);
        stopClimbing();
    }

    private void updateFixtureLinks() {
        climbStartFixture = physicsLogic.getFixture("climb_start");
        climbTopFixture = physicsLogic.getFixture("climb_top");
        climbStopTopFixture = physicsLogic.getFixture("climb_stop_top");
        climbBottomFixture = physicsLogic.getFixture("climb_bottom");
        climbStopBottomFixture = physicsLogic.getFixture("climb_stop_bottom");

        climbAlignLeftFixture = physicsLogic.getFixture("climb_align_left");
        climbAlignRightFixture = physicsLogic.getFixture("climb_align_right");
    }

    public boolean canClimb(Vector2 dir) {
        return canClimb(dir, true);
    }

    public boolean canClimb(Vector2 dir, boolean checkStart) {
        if (owner.isActivity(Activity.DISABLED)) return false;
        if (!touchesClimb()) return false;

        boolean canClimbX = canClimbX(dir.x);
        boolean canClimbY = canClimbY(dir.y);
        boolean canClimb = canClimbX && (dir.y == 0 || canClimbY);
        canClimb |= canClimbY && (Math.abs(dir.x) < .5f || canClimbX);

        //        VJXLogger.log(owner + " can climbX " + canClimbX + ", climbY "
        //                        + canClimbY + ", dir " + dir + " -> " + canClimb);
        if (checkStart) return canClimb && touchesClimbStart > 0;
        else return canClimb;
    }

    public void startClimb(final Vector2 direction) {
        //        VJXLogger.log(owner + " start climb " + climbing + ", " + direction, 1);
        if (climbing || direction.y == 0 || owner.isActivity(Activity.EXECUTE))
            return;
        if (touchesClimbTop > 0) {
            if (Math.abs(direction.y) >= Math.abs(direction.x)) {
                if (owner.isActivity(Activity.RISE)
                                || moveLogic.isGrounded() && direction.y <= 0)
                    return;

                if (touchesClimbStopTop == 0) {
                    climbUp(physicsLogic.getBodyHeight() + 32f);
                    return;
                }

                owner.setLifecycle(Lifecycle.MOVE);
                climbDown = false;
                climbUp = false;
                climbing = true;
                moveLogic.stop();
                moveLogic.setIgnoreGravity(true);
                updateClimbDir(direction);
                if (moveLogic.isGrounded()) {
                    listener.getPlayerInput().setIngameControl(false);
                    owner.setActivity(Activity.EXECUTE);
                    AnimatedEntity aObj = (AnimatedEntity) owner;
                    aObj.setAnimation("climb_start", false);

                    float dur = aObj.getAnimDuration("climb_start");
                    SequenceAction sa = Actions.sequence();
                    sa.addAction(Actions.delay(.1f));
                    sa.addAction(Actions.run(delayedStart));
                    sa.addAction(Actions.delay(dur - .1f));
                    sa.addAction(Actions.run(climbStarted));
                    owner.getActor().addAction(sa);
                    direction.x = 0;
                } else {
                    alignClimbX(true);
                    updateActivity(velocity);
                }
                physicsLogic.setIgnorePassable(true);
            }
        } else {
            if (physicsLogic.isGrounded())
                stopClimbing();
        }
    }

    public void stopClimbing() {
        if (!climbing) return;

        climbDown = false;
        climbing = false;
        climbDir.setZero();

        if (owner.isActivity(Activity.RISE)) return;
        moveLogic.setIgnoreGravity(false);
        physicsLogic.setIgnorePassable(false);
    }

    public boolean isClimbing() {
        return climbing;
    }

    public void climbDown(boolean startClimbing) {
        Fixture f = owner.getPhysics().getClosesHit(owner.getWorldCenter(),
                        tmpVec.set(0, -1), 128);
        float height = 32f;
        if (f != null) {
            VJXData d = (VJXData) f.getUserData();
            height = d.getHeight() * B2DWorld.B2D_TO_WORLD;
        }
        climbDown(physicsLogic.getBodyHeight() + height, startClimbing);
    }

    public boolean climbingDown() {
        return climbDown;
    }

    public boolean climbingUp() {
        return climbUp;
    }

    private void climbDown(float climbHeight, boolean startClimbing) {
        climbDown = true;
        moveLogic.setIgnoreGravity(true);
        owner.setLifecycle(Lifecycle.MOVE);
        owner.setActivity(Activity.EXECUTE);
        owner.playSfx(owner.getDefName() + SFX_jump_down);
        ((AnimatedEntity) owner).setAnimation(anim_climbDown, false);
        physicsLogic.setIgnorePassable(true);

        climbDir.set(0f, -1f);
        moveLogic.setSpeedMod(1f, 1f);

        float climbTime = ((AnimatedEntity) owner).getAnimDuration(anim_climbDown);
        climbUpSpeed = climbHeight / (climbTime / 2f);
        climbUpHeight = climbHeight;

        SequenceAction s = Actions.sequence();
        s.addAction(Actions.delay(0.01f));
        s.addAction(Actions.run(startMove));
        s.addAction(Actions.delay(climbTime / 2f - 0.02f));
        s.addAction(Actions.run(stopMove));
        if (startClimbing) s.addAction(Actions.run(startClimb));
        owner.getActor().addAction(s);
        listener.getUI(KawaidaHUD.class)
        .showContextActionBtn(ActionContext.JUMP_DOWN, false);

        listener.camCatchYDown(true);
    }

    private void climbUp(float climbHeight) {
        climbUp = true;
        stopClimbing();
        owner.setLifecycle(Lifecycle.MOVE);
        owner.setActivity(Activity.EXECUTE);
        owner.playSfx(owner.getDefName() + SFX_jump_down);
        ((AnimatedEntity) owner).setAnimation(anim_climbUp, false);

        tmpVec.set(0f, 1f);
        moveLogic.setIgnoreGravity(true);
        updateVelocity(tmpVec, true);

        climbHeight += 2;
        moveLogic.setDestination(owner.getWCX(), owner.getWCY() + climbHeight);
        moveLogic.startMove();

        float climbTime = ((AnimatedEntity) owner).getAnimDuration(anim_climbUp) / 2f;
        float climbSpeed = climbHeight / climbTime;
        moveLogic.setVelocity(0, climbSpeed);

        SequenceAction s = Actions.sequence();
        s.addAction(KJActions.delay(climbTime));
        s.addAction(KJActions.startMove(owner, true, true));
        s.addAction(Actions.run(stopMove));
        owner.getActor().addAction(s);
    }

    private void climbEnded() {
        stopClimbing();
        owner.setActivity(Activity.EXECUTE);
        AnimatedEntity aObj = (AnimatedEntity) owner;
        aObj.setAnimation("climb_end", false);

        float dur = aObj.getAnimDuration("climb_end");
        SequenceAction sa = Actions.sequence();
        sa.addAction(Actions.delay(dur));
        sa.addAction(KJActions.setActivity(Activity.IDLE, owner));
        owner.getActor().addAction(sa);
    }

    private boolean canClimbX(float dirX) {
        return false;
    }

    private boolean canClimbY(float dirY) {
        if (dirY == 0) return false;

        if (dirY > 0) {
            return touchesClimbTop > 0;
        } else {
            return touchesClimbBottom > 0;
        }
    }

    private void updateClimbDir() {
        moveLogic.getDestination(tmpVec);
        tmpVec.sub(owner.getWorldCenter());
        updateClimbDir(tmpVec);
    }

    private void updateClimbDir(Vector2 direction) {
        climbDir.set(direction);
        climbDir.y = Math.abs(climbDir.y) >= Math.abs(climbDir.x) ? climbDir.y : 0;
        climbDir.nor();
    }

    private void updateVelocity(Vector2 direction, boolean force) {
        velocity.x = climbDir.x * moveLogic.getSpeedModX() * speedX
                        * B2DWorld.WORLD_UNIT;
        velocity.y = climbDir.y * moveLogic.getSpeedModY() * speedY
                        * B2DWorld.WORLD_UNIT;
        velocity.x = canClimbX(direction.x) || force ? velocity.x : 0;
        velocity.y = canClimbY(direction.y) || force ? velocity.y : 0;

        if (climbAlignLeft)
            velocity.x = -512;
        if (climbAlignRight)
            velocity.x = 512;
        moveLogic.setVelocity(velocity);
    }

    private void updateActivity(Vector2 velocity) {
        if (owner.isActivity(Activity.EXECUTE)
                        || owner.isActivity(Activity.DISABLED))
            return;

        AnimatedEntity aObj = (AnimatedEntity) owner;
        if (!velocity.isZero()) {
            tmpVec.set(moveLogic.getSpeedModX(), moveLogic.getSpeedModY());
            tmpVec.nor();

            if (moveLogic.getSpeedModY() > .5f)
                owner.setActivity(Activity.CLIMB_RUN);
            else owner.setActivity(Activity.CLIMB_WALK);

            if (velocity.y >= 0)
                aObj.setAnimation(owner.getActivity().name + VJXString.POST_up,
                                true);
            else aObj.setAnimation(owner.getActivity().name + VJXString.POST_down,
                            true);
        } else setIdle();
    }

    private boolean touchesClimb() {
        return touchesClimbStart > 0 || touchesClimbBottom > 0 || touchesClimbTop > 0;
    }

    private void setIdle() {
        moveLogic.stop();
        owner.setLifecycle(Lifecycle.STILL);
        owner.setActivity(Activity.CLIMB_IDLE);
        AnimatedEntity aObj = (AnimatedEntity) owner;
        aObj.setAnimation(Activity.CLIMB_IDLE.name, true);
    }

    private void updateTouchCount(Fixture fixture, int amount) {
        if (fixture == climbStartFixture) {
            touchesClimbStart += amount;
        }
        if (fixture == climbTopFixture) {
            touchesClimbTop += amount;
        }
        if (fixture == climbStopTopFixture) {
            touchesClimbStopTop += amount;
        }
        if (fixture == climbBottomFixture) {
            touchesClimbBottom += amount;
        }
        if (fixture == climbStopBottomFixture) {
            touchesClimbStopBottom += amount;
        }
    }

    private void updateAlign(Fixture fixture, boolean align) {
        if (fixture == climbAlignLeftFixture) {
            climbAlignLeftCounter += align ? 1 : -1;
        }
        if (fixture == climbAlignRightFixture) {
            climbAlignRightCounter += align ? 1 : -1;
        }
    }

    @Override
    public boolean checkBeginCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        short otherCat = otherFix.getFilterData().categoryBits;
        short thisCat = thisFix.getFilterData().categoryBits;

        if (B2DBit.CLIMBABLE.isCategory(thisCat)) { return true; }
        if (!B2DBit.SIDE.isCategory(thisCat)) return false;

        if (B2DBit.CLIMBABLE.isCategory(otherCat)) {
            updateTouchCount(thisFix, 1);
            updateAlign(thisFix, false);
            return true;
        }

        if (!climbing) return false;

        if (B2DBit.SOLID.isCategory(otherCat)) {
            if (climbDir.y < 0) {
                climbEnded();
                return true;
            }
            return false;
        }

        return false;
    }

    @Override
    public boolean checkEndCollision(Fixture thisFix, Fixture otherFix,
                    Contact contact, EJXEntity other) {
        short otherCat = otherFix.getFilterData().categoryBits;
        if (B2DBit.CLIMBABLE.isCategory(otherCat)) {
            updateTouchCount(thisFix, -1);
            updateAlign(thisFix, true);
            return true;
        }

        if (!climbing) return false;

        if (B2DBit.PASSABLE.isCategory(otherCat)
                        && thisFix == climbStopTopFixture
                        && touchesClimbStopTop == 0) {
            moveLogic.stop();
            if (climbDir.y > 0) {
                VJXData fixtureData = (VJXData) otherFix.getUserData();
                float height = physicsLogic.getBodyHeight()
                                + fixtureData.getHeight() * 100f;
                float yPos = B2DWorld.getFixtureWorldCenter(otherFix).y;
                yPos += B2DWorld.getFixtureHeight(otherFix) / 2f;

                listener.camCatchYUp(false);
                listener.snapCamToPlatform(yPos);
                climbUp(height);
                return true;
            }
        }

        return false;
    }

    @Override
    public void drawBatchDebug(Batch batch, BitmapFont font) {
        if (!owner.isOnScreen()) return;

        tmpVec.set(B2DWorld.getFixtureWorldCenter(climbStartFixture));
        font.draw(batch, "" + touchesClimbStart, tmpVec.x - 4, tmpVec.y + 6);

        tmpVec.set(B2DWorld.getFixtureWorldCenter(climbTopFixture));
        font.draw(batch, "" + touchesClimbTop, tmpVec.x - 4, tmpVec.y + 6);

        tmpVec.set(B2DWorld.getFixtureWorldCenter(climbStopTopFixture));
        font.draw(batch, "" + touchesClimbStopTop, tmpVec.x - 4, tmpVec.y + 6);

        tmpVec.set(B2DWorld.getFixtureWorldCenter(climbBottomFixture));
        font.draw(batch, "" + touchesClimbBottom, tmpVec.x - 4, tmpVec.y + 6);

        tmpVec.set(B2DWorld.getFixtureWorldCenter(climbStopBottomFixture));
        font.draw(batch, "" + touchesClimbStopBottom, tmpVec.x - 4, tmpVec.y + 6);

        //        tmpVec.set(B2DWorld.getFixtureWorldCenter(climbAlignLeftFixture));
        //        font.draw(batch, "" + climbAlignLeft, tmpVec.x - 4, tmpVec.y + 6);
        //
        //        tmpVec.set(B2DWorld.getFixtureWorldCenter(climbAlignRightFixture));
        //        font.draw(batch, "" + climbAlignRight, tmpVec.x - 4, tmpVec.y + 6);
    }
}