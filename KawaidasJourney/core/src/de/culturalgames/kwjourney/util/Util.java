package de.culturalgames.kwjourney.util;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Util {
    private static Logger logger = Logger.getLogger("");

    public static float round(float f, int precision) {
        float pFix = (float) Math.pow(10, precision);
        return Math.round(f * pFix) / pFix;
    }

    public static <T> Set<T> differenceSet(Set<T> setA, Set<T> setB) {
        Set<T> tmpSet = new HashSet<T>();
        tmpSet.addAll(setB);        // tmpSet = B

        tmpSet.removeAll(setA);     // setB = B without A
        setA.removeAll(setB);       // setA = A without B

        setA.addAll(tmpSet);        // setA = Difference A, B

        return setA;
    }

    public static int matchPowerOfTwo(int value) {
        return (int) Math.pow(2, Math.ceil(Math.log10(value) / Math.log10(2)));
    }

    public static int log2(int value) {
        if (value == 0)
            return 0;
        return (int) (Math.log10(matchPowerOfTwo(value)) / Math.log10(2));
    }

    public static void log(Level level, String msg) {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        String traceLink =
                "(" + trace[2].getFileName() + ":" + trace[2].getLineNumber()
                + ")";
        String methodName = trace[2].getClassName() + "." + trace[2].getMethodName();
        logger.logp(level, methodName, traceLink, msg);
    }

    public static void printStackTrace() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        StackTraceElement s;
        System.out.println("--------------------Stacktrace--------------------");
        for (int i = 1; i < stackTrace.length - 1; i++) {
            s = stackTrace[i];
            System.out.print(s.getClassName() + "." + s.getMethodName());
            System.out.println("(" + s.getFileName() + ":" + s.getLineNumber() + ")");
        }
        System.out.println("--------------------End trace---------------------");
    }

    public static void print2DArray(float[][] floatArray) {
        for (int j = floatArray[0].length - 1; j >= 0; j--) {
            for (float[] element : floatArray)
                System.out.print(element[j] + " ");
            System.out.println();
        }
    }

    public static String intToBinString(int i, int binL, int decL) {
        if (decL < 0) decL = Integer.toString(Integer.MIN_VALUE).length();

        String binary = Integer.toBinaryString(i), s = "";

        for (int j = 0; j < binary.length(); j += 4)
            if (binary.length() - j - 4 >= 0)
                s = " " + binary.substring(binary.length() - j - 4, binary.length() - j) + s;
            else s = " " + binary.substring(0, binary.length() - j) + s;
        s = String.format("%" + binL + "s", s);
        if (decL > 0)
            s += String.format(" - " + "%" + decL + "s", i);
        return s;
    }
}
