package de.culturalgames.kwjourney.util;

public class KJTexts {

    // static helper strings for building up dynamic text ids
    public static final String ENCY_ = "ency_";
    public static final String HELP_ = "help_";
    public static final String HINT_ = "hint_";
    public static final String LOADING_LEVEL_ = "loading_level_";
    public static final String MENU_ENCY_ = "menu_ency_";
    public static final String PROJECT_ = "project_";
    public static final String SECRET_ = "secret_";
    public static final String STORY_ = "story_";

    // static text ids
    public static final String BTN_ABOUT = "btn_about";
    public static final String BTN_BACK = "btn_back";
    public static final String BTN_BUY = "btn_buy";
    public static final String BTN_CANCEL = "btn_cancel";
    public static final String BTN_COLLECT = "btn_collect";
    public static final String BTN_COLLECT_SUNS = "btn_collect_suns";
    public static final String BTN_CONFIRM = "btn_confirm";
    public static final String BTN_CONTINUE = "btn_continue";
    public static final String BTN_CREDITS = "btn_credits";
    public static final String BTN_ENCY = "btn_ency";
    public static final String BTN_FEEDBACK = "btn_feedback";
    public static final String BTN_HOME = "btn_home";
    public static final String BTN_LEARN_MORE = "btn_learn_more";
    public static final String BTN_MAP = "btn_map";
    public static final String BTN_OK = "btn_ok";
    public static final String BTN_OUTRO0 = "btn_outro0";
    public static final String BTN_OUTRO1 = "btn_outro1";
    public static final String BTN_OUTRO2 = "btn_outro2";
    public static final String BTN_OUTRO3 = "btn_outro3";
    public static final String BTN_REPORT_BUG = "btn_report_bug";
    public static final String BTN_RESTART = "btn_restart";
    public static final String BTN_SHARE = "btn_share";
    public static final String BTN_SHOP_BASIC = "btn_shop_basic";
    public static final String BTN_SHOP_PRIME = "btn_shop_prime";
    public static final String BTN_SHOP_SKIN = "btn_shop_skin";
    public static final String BTN_SOUNDTRACK = "btn_soundtrack";
    public static final String BTN_TESTER = "btn_tester";
    public static final String BTN_UNLOCK = "btn_unlock";
    public static final String BTN_WATCH_AD = "btn_watch_ad";
    public static final String BTN_WATCH_AD_0 = "btn_watch_ad_0";
    public static final String BTN_WATCH_AD_1 = "btn_watch_ad_1";
    public static final String BTN_WATCH_AD_2 = "btn_watch_ad_2";
    public static final String BTN_WATCH_AD_3 = "btn_watch_ad_3";

    public static final String CONFIRM_ACTIVATE_ITEM_TITLE = "confirm_activate_item_title";
    public static final String CONFIRM_ACTIVATE_ITEM_TEXT = "confirm_activate_item_text";
    public static final String CONFIRM_BUY_ITEM_TITLE = "confirm_buy_item_title";
    public static final String CONFIRM_BUY_ITEM_TEXT = "confirm_buy_item_text";
    public static final String CONFIRM_CLOUD_TITLE = "confirm_cloud_title";
    public static final String CONFIRM_CLOUD_TEXT = "confirm_cloud_text";
    public static final String CONFIRM_CONFLICTING_DATA_TITLE = "confirm_conflicting_data_title";
    public static final String CONFIRM_CONFLICTING_DATA_TEXT = "confirm_conflicting_data_text";
    public static final String CONFIRM_DEACTIVATE_ITEM_TITLE = "confirm_deactivate_item_title";
    public static final String CONFIRM_DEACTIVATE_ITEM_TEXT = "confirm_deactivate_item_text";
    public static final String CONFIRM_NO_TOTEMS_TITLE = "confirm_no_totems_title";
    public static final String CONFIRM_NO_TOTEMS_TEXT = "confirm_no_totems_text";
    public static final String CONFIRM_RESET_GAME_TITLE = "confirm_reset_game_title";
    public static final String CONFIRM_RESET_GAME_TEXT = "confirm_reset_game_text";
    public static final String CONFIRM_RESETORE_SHOP_TITLE = "confirm_restore_shop_title";
    public static final String CONFIRM_RESTORE_SHOP_TEXT = "confirm_restore_shop_text";

    public static final String HUD_READY_LABEL = "hud_ready_label";

    public static final String MENU_CREDITS_TITLE = "menu_credits_title";
    public static final String MENU_CREDITS_TEXT = "menu_credits_text";
    public static final String MENU_CREDITS_IMG0_TITLE = "menu_credits_image0_title";
    public static final String MENU_CREDITS_IMG1_TITLE = "menu_credits_image1_title";
    public static final String MENU_ENCY_TITLE = "menu_ency_title";
    public static final String MENU_ENCY_TOPICS_TITLE = "menu_ency_topics_title";
    public static final String MENU_ENCY_CHARACTERS_TITLE = "menu_ency_characters_title";
    public static final String MENU_ENCY_PLACES_TITLE = "menu_ency_places_title";
    public static final String MENU_ENCY_PLANTS_TITLE = "menu_ency_plants_title";
    public static final String MENU_ENCY_OBJECTS_TITLE = "menu_ency_objects_title";
    public static final String MENU_ENCY_SWAHILI_TITLE = "menu_ency_swahili_title";
    public static final String MENU_END_HOME_IS_TITLE = "menu_end_home_is_title";
    public static final String MENU_LOADING_CHAPTER_TITLE = "menu_loading_chapter_title";
    public static final String MENU_LOADING_LEVEL_TITLE = "menu_loading_level_title";
    public static final String MENU_PROJECTS_TITLE = "menu_projects_title";
    public static final String MENU_SETTINGS_TITLE = "menu_settings_title";
    public static final String MENU_SHOP_BASIC_TITLE = "menu_shop_basic_title";
    public static final String MENU_SHOP_PRIME_TITLE = "menu_shop_prime_title";
    public static final String MENU_SHOP_SKIN_TITLE = "menu_shop_skin_title";

    public static final String INFO_CLOUD_NOT_AVAILABLE_TITLE = "info_cloud_not_available_title";
    public static final String INFO_CLOUD_WIP_TITLE = "info_cloud_wip_title";
    public static final String INFO_CLOUD_WIP_TEXT = "info_cloud_wip_text";
    public static final String INFO_FETCH_STATES_FAILED_TITLE = "info_fetch_states_failed_title";
    public static final String INFO_NO_INET_TITLE = "info_no_inet_title";
    public static final String INFO_NO_INET_TEXT = "info_no_inet_text";
    public static final String INFO_NOT_ENOUGH_STARS_PRE = "info_not_enough_stars_pre";
    public static final String INFO_NOT_ENOUGH_STARS_POST = "info_not_enough_stars_post";
    public static final String INFO_PURCHASE_INSTALL_FAILED_TITLE = "info_purchase_install_failed_title";
    public static final String INFO_PURCHASE_INSTALL_FAILED_TEXT = "info_purchase_install_failed_text";
    public static final String INFO_PURCHASE_ERROR_TITLE = "info_purchase_error_title";
    public static final String INFO_PURCHASE_ERROR_TEXT = "info_purchase_error_text";
    public static final String INFO_PURCHASE_CANCELLED_TITLE = "info_purchase_cancelled_title";
    public static final String INFO_PURCHASE_CANCELLED_TEXT = "info_purchase_cancelled_text";
    public static final String INFO_PURCHASE_SUCCESS_TITLE = "info_purchase_success_title";
    public static final String INFO_PURCHASE_SUCCESS_TEXT = "info_purchase_success_text";
    public static final String INFO_RESOLUTION_FAILED_TITLE = "info_resolution_failed_title";
    public static final String INFO_RESOLUTION_FAILED_TEXT = "info_resolution_failed_text";
    public static final String INFO_TEST_SHOP_TITLE = "info_test_shop_title";
    public static final String INFO_TEST_SHOP_TEXT = "info_test_shop_text";
    public static final String INFO_UNLOCKED_CHAPTER = "info_unlocked_chapter";

    public static final String POPUP_BUY_HINT_LABEL = "popup_buy_hint_support_label";
    public static final String POPUP_BUY_INFINITE_LABEL = "popup_buy_infinite_label";
    public static final String POPUP_EMPTY_TITLE = "popup_empty_title";
    public static final String POPUP_WIN_TITLE = "popup_win_title";
    public static final String POPUP_LOSE_TITLE = "popup_lose_title";
    public static final String POPUP_PAUSE_TITLE = "popup_pause_title";
    public static final String POPUP_REWARD_TITLE = "popup_reward_title";
    public static final String POPUP_WIN_STAR0_LABEL = "popup_win_star0_label";
    public static final String POPUP_WIN_STAR1_BONUS_LABEL = "popup_win_star1_bonus_label";
    public static final String POPUP_WIN_STAR1_DAMAGE_LABEL = "popup_win_star1_damage_label";

    public static final String TEXT_ACTIVE = "text_active";
    public static final String TEXT_FREE = "text_free";
    public static final String TEXT_FULL = "text_full";
    public static final String TEXT_SOLD_OUT = "text_sold_out";

    public static final String VIDEO_OUTRO0_TITLE = "video_outro0_title";
    public static final String VIDEO_OUTRO1_TITLE = "video_outro1_title";
    public static final String VIDEO_OUTRO2_TITLE = "video_outro2_title";
    public static final String VIDEO_OUTRO3_TITLE = "video_outro3_title";
}