package de.culturalgames.kwjourney.util;

public class KJTypes {

    public static abstract class KJString {

        public static final String OFFER_skin_ = "offer_skin_";

        public static final String ACTOR_SPEAKER0 = "speaker0";
        public static final String ACTOR_SPEAKER1 = "speaker1";

        // rewards
        public static final String REWARD_test = "test";
        public static final String REWARD_totem = "totem";
        public static final String REWARD_coco = "coco";
        public static final String REWARD_banana = "banana";
        public static final String REWARD_sun = "sun";

        public static final String AD_test = "ad_test";
        public static final String AD_continue = "ad_continue";
        public static final String AD_double_suns = "ad_double_suns";

        // object types
        public static final String OBJECT_offer = "offer";
        public static final String OBJECT_lvlBtn = "lvlBtn";
        public static final String OBJECT_lvlPath = "lvlPath";
        public static final String OBJECT_mapDeco = "mapDeco";
        public static final String OBJECT_note = "note";

        // paths
        public static final String PATH_icons = "icons/";
        public static final String PATH_icons_map = "icons/map/";
        public static final String PATH_icons_secrets = "icons/secrets/";
        public static final String PATH_icons_projects = "icons/projects/";
        public static final String PATH_icons_ency = "icons/ency/";
        public static final String PATH_icons_shop = "icons/shop/";
    }
}