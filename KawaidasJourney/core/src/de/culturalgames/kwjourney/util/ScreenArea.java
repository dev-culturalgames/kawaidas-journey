package de.culturalgames.kwjourney.util;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public class ScreenArea {

    public Vector2 position;
    public Vector2 dimension;

    private Texture tex;

    public ScreenArea() {
        position = new Vector2();
        dimension = new Vector2();
    }

    public ScreenArea(Vector2 pos, Vector2 dim) {
        position = pos;
        dimension = dim;
    }

    public void setPosition(float x, float y) {
        position.x = x;
        position.y = y;
    }

    public void setDimension(float x, float y) {
        dimension.x = x;
        dimension.y = y;
    }

    public void setPosition(Vector2 pos) {
        position = pos;
    }

    public void setDimension(Vector2 dim) {
        dimension = dim;
    }

    public boolean contains(Vector2 v) {
        if (v == null) return false;

        if (v.x >= position.x && v.x <= position.x + dimension.x
                        && v.y >= position.y && v.y <= position.y + dimension.y) {
            return true;
        }

        return false;
    }

    public void scale(float scl) {
        position.scl(scl);
        dimension.scl(scl);
    }

    public void scale(Vector2 scl) {
        position.scl(scl);
        dimension.scl(scl);
    }

    public void scale(float sclX, float sclY) {
        position.scl(sclX, sclY);
        dimension.scl(sclX, sclY);
    }

    public void setTexture(Texture tex) {
        this.tex = tex;
    }

    public Texture getTexture() {
        return tex;
    }
}