package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.entity.logics.InventoryLogic;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class IncrItemCommand extends Command {

    private static final String VALID_COMMAND = " incr_itm:<string_itemName>(:<int_amount>)";
    private static final String NO_INVENTORY_LOGIC = "Object does not have an InventoryLogic.";
    private static final String ITEM_NOT_FOUND = " Target item object not found.";
    private static final String ITEM_NOT_SET = "Target item object not set.";

    private InventoryLogic inventoryLogic;
    private ItemCategory item;
    private int amount;

    @Override
    public float execute(SequenceAction sequence) {
        if (inventoryLogic == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, NO_INVENTORY_LOGIC);
            return 0f;
        }

        if (item == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, ITEM_NOT_SET);
            return 0f;
        }

        inventoryLogic.incr(item, amount);
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);

        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1_2
                            + VALID_COMMAND);
            return -1;
        }

        item = ItemCategory.get(split[2]);
        if (item == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + ITEM_NOT_FOUND);
            return -1;
        }

        if (split.length > 3) {
            if (!split[3].matches(EJXRegEx.matchFloat)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                + EJXError.ERROR_INVALID_PARAM_INT);
                return -1;
            }
            amount = Integer.parseInt(split[3]);

            if (split.length > 4) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_2 + cmdString);
            }
        }
        return 0;
    }

    @Override
    public void setExecutor(EJXEntity executor) {
        super.setExecutor(executor);
        if (!executor.hasLogic(KJLogicType.INVENTORY))
            VJXLogger.log(LogCategory.WARNING, NO_INVENTORY_LOGIC);
        else inventoryLogic = (InventoryLogic) executor.getLogic(KJLogicType.INVENTORY);
    }

    public void setItem(ItemCategory item) {
        this.item = item;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + item
                        + VJXString.SEP_LIST_SPACE + amount;
    }
}