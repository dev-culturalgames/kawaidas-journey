package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.KawaidaHUD;
import de.venjinx.ejx.EJXGameScreen;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.vjx.ui.IngameHUD.Transition;

public class SetControlsCommand extends Command {

    private static final String VALID_COMMAND = " controls:(:<boolean_activate>:<boolean_videoMode>)";

    private boolean activateControls;
    private boolean showVideoMode;

    private Runnable execRun = new Runnable() {

        @Override
        public void run() {
            EJXGameScreen screen = gameControl.getGame().getScreen();
            gameControl.getPlayerInput().setIngameControl(activateControls);
            screen.getMenu().getUI(KawaidaHUD.class).toggleHUD(activateControls);

            if (activateControls) {
                screen.getMenu().toggleOverlay(Transition.VIDEO_SEQUENCE, false);
                return;
            } else {
                if (showVideoMode) {
                    screen.getMenu().toggleOverlay(Transition.VIDEO_SEQUENCE,
                                    showVideoMode);
                }
            }
        }
    };

    @Override
    public float execute(SequenceAction sequence) {
        sequence.addAction(Actions.run(execRun));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);

        activateControls = true;
        if (split.length > 2) {
            if (!split[2].matches(EJXRegEx.matchBool)) {
                VJXLogger.log(LogCategory.ERROR,
                                ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + EJXError.ERROR_INVALID_PARAM_BOOL);
                return -1;
            }
            activateControls = Boolean.parseBoolean(split[2]);

            showVideoMode = true;
            if (split.length > 3) {
                if (!split[2].matches(EJXRegEx.matchBool)) {
                    VJXLogger.log(LogCategory.ERROR,
                                    ERROR_PARSING_COMMAND + cmdString);
                    VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                    + EJXError.ERROR_INVALID_PARAM_BOOL);
                    return -1;
                }
                showVideoMode = Boolean.parseBoolean(split[3]);

                if (split.length > 4) {
                    VJXLogger.log(LogCategory.WARNING,
                                    EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
                }
            }
        }
        return 0;
    }

    public void setActivateControls(boolean activateControls) {
        this.activateControls = activateControls;
    }

    public void setShowVideoMode(boolean showVideoMode) {
        this.showVideoMode = showVideoMode;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + activateControls
                        + VJXString.SEP_DDOT_SPACE + showVideoMode;
    }
}