package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.PlantLogic;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class PlantCommand extends Command {

    private static final String VALID_COMMAND = " plant";
    private static final String NO_PLANT_LOGIC = "Object does not have a PlantLogic.";

    private PlantLogic plantLogic;

    @Override
    public float execute(SequenceAction sequence) {
        if (plantLogic == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, NO_PLANT_LOGIC);
            return 0f;
        }
        sequence.addAction(plantLogic.plant(true));
        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length > 2) {
            VJXLogger.log(LogCategory.WARNING,
                            EJXError.WARNING_PARAMS_COUNT_0 + cmdString);
        }
        return 0;
    }

    @Override
    public void setExecutor(EJXEntity executor) {
        super.setExecutor(executor);
        if (!executor.hasLogic(KJLogicType.PLANT))
            VJXLogger.log(LogCategory.WARNING, NO_PLANT_LOGIC);
        else plantLogic = (PlantLogic) executor.getLogic(KJLogicType.PLANT);
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND;
    }
}