package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.actions.KJActions;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class AttachCommand extends Command {

    private static final String VALID_COMMAND = " attach|detach(:<string_objectName>)";

    private boolean attach;
    private EJXEntity target;

    @Override
    public float execute(SequenceAction sequence) {
        if (attach) {
            if (target == null) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
                VJXLogger.log(LogCategory.WARNING, TARGET_NOT_SET);
                return 0f;
            }
            sequence.addAction(KJActions.attachTo(executor, target));
        } else sequence.addAction(KJActions.detach(executor));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);

        if (split[1].equals(CMD.DETACH.name)) {
            attach = false;

            if (split.length > 2) {
                VJXLogger.log(LogCategory.WARNING,
                                EJXError.WARNING_PARAMS_COUNT_0 + cmdString);
            }
            return 0;
        } else attach = true;

        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1
                            + VALID_COMMAND);
            return -1;
        } else {
            target = gameControl.getEntityByName(split[2]);
            if (target == null) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + TARGET_NOT_FOUND);
                return -1;
            }

            if (split.length > 3) {
                VJXLogger.log(LogCategory.WARNING,
                                EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
            }
        }
        return 0;
    }

    public void setAttach(boolean attach) {
        this.attach = attach;
    }

    public void setAttachTo(EJXEntity attachTo) {
        target = attachTo;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + attach
                        + VJXString.SEP_LIST_SPACE + target;
    }
}