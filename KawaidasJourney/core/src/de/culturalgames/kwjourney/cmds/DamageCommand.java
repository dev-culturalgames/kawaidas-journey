package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.LifeLogic;
import de.culturalgames.kwjourney.entity.logics.LifeLogic.DamageType;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class DamageCommand extends Command {

    private static final String VALID_COMMAND = " damage:(<int_damage>)";
    private static final String NO_LIFE_LOGIC = "Object does not have a LifeLogic.";

    private LifeLogic lifeLogic;
    private int damage = 1;

    @Override
    public float execute(SequenceAction sequence) {
        if (lifeLogic == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, NO_LIFE_LOGIC);
            return 0f;
        }
        lifeLogic.takeDamage(damage, DamageType.melee);
        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length == 3) {
            if (!split[2].matches(EJXRegEx.matchInteger)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + EJXError.ERROR_INVALID_PARAM_INT);
                return -1;
            }
            damage = Integer.parseInt(split[2]);
        }

        if (split.length > 3) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
        }
        return 0;
    }

    @Override
    public void setExecutor(EJXEntity executor) {
        super.setExecutor(executor);
        if (!executor.hasLogic(KJLogicType.LIFE))
            VJXLogger.log(LogCategory.WARNING, NO_LIFE_LOGIC);
        else lifeLogic = (LifeLogic) executor.getLogic(KJLogicType.LIFE);
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + damage;
    }
}