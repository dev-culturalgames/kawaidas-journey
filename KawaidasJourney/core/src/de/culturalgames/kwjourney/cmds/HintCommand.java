package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.entity.logics.HintLogic;
import de.culturalgames.kwjourney.entity.logics.HintLogic.Hint;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class HintCommand extends Command {

    private static final String VALID_COMMAND = " hint:<string_hintName>";
    private static final String HINT_NOT_FOUND = " Target hint object not found.";
    private static final String HINT_NOT_SET = " Target hint object not set.";

    private Hint hint;
    private String hintName;

    @Override
    public float execute(SequenceAction sequence) {
        hint = HintLogic.getHint(hintName);
        if (hint == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, HINT_NOT_SET);
            return 0f;
        }
        sequence.addAction(Actions.delay(hint.getDelay()));
        sequence.addAction(KJActions.showHint(hint, gameControl.getGame()));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_1
                            + VALID_COMMAND);
            return -1;
        }

        hintName = split[2];
        if (gameControl.getEntityByName(hintName) == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1 + HINT_NOT_FOUND);
            return -1;
        }

        if (split.length > 3) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
        }
        return 0;
    }

    public void setHint(Hint hint) {
        this.hint = hint;
        hintName = hint.getObject().getName();
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + hint;
    }
}