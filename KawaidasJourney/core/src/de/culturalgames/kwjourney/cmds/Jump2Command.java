package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.entity.logics.JumpLogic;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class Jump2Command extends Command {

    private static final String VALID_COMMAND = " jump2:<float_height>:<float_speed>(:<boolean_force>)";
    private static final String NO_JUMP_LOGIC = "Object does not have a JumpLogic.";

    private JumpLogic jumpLogic;
    private float height;
    private float speed;
    private boolean force;
    private Interpolation interpolation;

    @Override
    public float execute(SequenceAction sequence) {
        if (jumpLogic == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, NO_JUMP_LOGIC);
            return 0f;
        }

        jumpLogic.newJump(height * 256f, speed, interpolation, force);
        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);

        if (split.length < 4) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAMS_COUNT_2_3
                            + VALID_COMMAND);
            return -1;
        }

        if (!split[2].matches(EJXRegEx.matchFloat)) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + EJXError.ERROR_INVALID_PARAM_FLOAT);
            return -1;
        }
        height = Float.parseFloat(split[2]);

        if (!split[3].matches(EJXRegEx.matchFloat)) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                            + EJXError.ERROR_INVALID_PARAM_FLOAT);
            return -1;
        }
        speed = Float.parseFloat(split[3]);

        force = false;
        if (split.length > 4) {
            if (!split[4].matches(EJXRegEx.matchBool)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS3
                                + EJXError.ERROR_INVALID_PARAM_BOOL);
                return -1;
            }
            force = Boolean.parseBoolean(split[4]);

            if (split.length > 5) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_3 + cmdString);
            }
        }
        return 0;
    }

    @Override
    public void setExecutor(EJXEntity executor) {
        super.setExecutor(executor);

        jumpLogic = (JumpLogic) executor.getLogic(KJLogicType.JUMP);
        if (!executor.hasLogic(KJLogicType.JUMP))
            VJXLogger.log(LogCategory.WARNING, NO_JUMP_LOGIC);
        else jumpLogic = (JumpLogic) executor.getLogic(KJLogicType.JUMP);
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public void setInterpolation(Interpolation interpolation) {
        this.interpolation = interpolation;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + height
                        + VJXString.SEP_LIST_SPACE + speed
                        + VJXString.SEP_LIST_SPACE + force;
    }
}
