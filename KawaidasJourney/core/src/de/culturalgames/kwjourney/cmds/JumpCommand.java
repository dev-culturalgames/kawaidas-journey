package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.actions.KJActions;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class JumpCommand extends Command {

    private static final String VALID_COMMAND = " jump:<float_height>(:<boolean_force>)";

    private float height = 1f;
    private boolean force = false;

    @Override
    public float execute(SequenceAction sequence) {
        sequence.addAction(KJActions.jump(executor, height, force));
        return 0;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR,
                            EJXError.ERROR_PARAMS_COUNT_1_2 + VALID_COMMAND);
            return -1;
        }

        if (!split[2].matches(EJXRegEx.matchFloat)) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + EJXError.ERROR_INVALID_PARAM_FLOAT);
            return -1;
        }
        height = Float.parseFloat(split[2]);

        force = false;
        if (split.length > 3) {
            if (!split[3].matches(EJXRegEx.matchBool)) {
                VJXLogger.log(LogCategory.ERROR,
                                ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                + EJXError.ERROR_INVALID_PARAM_BOOL);
                return -1;
            }
            force = Boolean.parseBoolean(split[3]);

            if (split.length > 4) {
                VJXLogger.log(LogCategory.WARNING,
                                EJXError.WARNING_PARAMS_COUNT_2 + cmdString);
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + height
                        + VJXString.SEP_LIST_SPACE + force;
    }
}