package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.LifeLogic;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class DieCommand extends Command {

    private static final String VALID_COMMAND = " die";
    private static final String NO_LIFE_LOGIC = "Object does not have a LifeLogic.";

    private LifeLogic lifeLogic;

    @Override
    public float execute(SequenceAction sequence) {
        if (lifeLogic == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, NO_LIFE_LOGIC);
            return 0f;
        }
        lifeLogic.die();
        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length > 2) {
            VJXLogger.log(LogCategory.WARNING,
                            EJXError.WARNING_PARAMS_COUNT_0 + cmdString);
        }
        return 0;
    }

    @Override
    public void setExecutor(EJXEntity executor) {
        super.setExecutor(executor);
        if (!executor.hasLogic(KJLogicType.LIFE))
            VJXLogger.log(LogCategory.WARNING, NO_LIFE_LOGIC);
        else lifeLogic = (LifeLogic) executor.getLogic(KJLogicType.LIFE);
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND;
    }
}