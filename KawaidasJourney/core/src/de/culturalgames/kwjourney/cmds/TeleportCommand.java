package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.actions.KJActions;
import de.venjinx.ejx.EJXCamera;
import de.venjinx.ejx.EJXCamera.CamMode;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.util.EJXTypes.Activity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class TeleportCommand extends Command {

    private static final String VALID_COMMAND = " teleport:<string_position>(:<float_speed>)";

    private Vector2 position = new Vector2();
    private float speed = 1f;

    @Override
    public float execute(SequenceAction sequence) {
        EJXCamera cam = (EJXCamera) gameControl.getLevel().getCamera();
        cam.setMode(CamMode.POSITION_LOCK, CamMode.POSITION_LOCK);
        cam.setLockOffset(cam.position.x - executor.getWCX(),
                        cam.position.y - executor.getWCY());
        sequence.addAction(KJActions.setActivity(Activity.EXECUTE, executor));
        sequence.addAction(KJActions.visible(false, executor));
        sequence.addAction(KJActions.setGhost(true, executor));
        sequence.addAction(KJActions.translate(position, speed, executor));
        sequence.addAction(KJActions.setGhost(false, executor));
        sequence.addAction(KJActions.visible(true, executor));
        sequence.addAction(KJActions.setActivity(Activity.IDLE, executor));
        sequence.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                ((EJXCamera) gameControl.getLevel().getCamera()).setMode(
                                CamMode.FORWARD_FOCUS, CamMode.FORWARD_FOCUS);
            }
        }));
        return 0;
    }

    public void setPosition(float x, float y) {
        position.set(x, y);
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR,
                            EJXError.ERROR_PARAMS_COUNT_1_2 + VALID_COMMAND);
            return -1;
        }

        position = EJXUtil.parsePosition(split[2], position,
                        gameControl.getLevel());
        if (position == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + EJXError.ERROR_INVALID_PARAM_POSITION);
            return -1;
        }

        position.x -= executor.getWidth() / 2f;
        position.y -= executor.getHeight() / 2f;

        speed = 1f;
        if (split.length > 3) {
            if (!split[3].matches(EJXRegEx.matchFloat)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                + EJXError.ERROR_INVALID_PARAM_FLOAT);
                return -1;
            }
            speed = Float.parseFloat(split[3]);

            if (split.length > 4) {
                VJXLogger.log(LogCategory.WARNING,
                                EJXError.WARNING_PARAMS_COUNT_2 + cmdString);
            }
        }

        return 0;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + position
                        + VJXString.SEP_LIST_SPACE + speed;
    }
}