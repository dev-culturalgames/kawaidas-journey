package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.actions.object.AttackAction;
import de.culturalgames.kwjourney.entity.logics.AttackLogic;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class ThrowCommand extends Command {

    private static final String VALID_COMMAND = " throw(:<object_target>)";
    private static final String NO_ATTACK_LOGIC = "Object does not have an AttackLogic.";

    private EJXEntity target;
    private AttackLogic attackLogic;

    private boolean updateActivity = false;

    @Override
    public float execute(SequenceAction sequence) {
        if (attackLogic == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, NO_ATTACK_LOGIC);
            return 0f;
        }

        AttackAction a = KJActions.attackThrow(executor, updateActivity);
        if (target != null) a.setAttackTarget(target);
        sequence.addAction(a);
        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length >= 3) {
            target = gameControl.getEntityByName(split[2]);
            if (target == null) {
                VJXLogger.log(LogCategory.ERROR,
                                ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + TARGET_NOT_FOUND);
                return -1;
            }

            if (split.length > 3) {
                VJXLogger.log(LogCategory.WARNING,
                                EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
            }
        }
        return 0;
    }

    @Override
    public void setExecutor(EJXEntity executor) {
        super.setExecutor(executor);
        if (!executor.hasLogic(KJLogicType.ATTACK))
            VJXLogger.log(LogCategory.WARNING, NO_ATTACK_LOGIC);
        else attackLogic = (AttackLogic) executor.getLogic(KJLogicType.ATTACK);
    }

    public void setTarget(EJXEntity target) {
        this.target = target;
    }

    public void setUpdateActivity(boolean updateActivity) {
        this.updateActivity = updateActivity;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + target;
    }
}