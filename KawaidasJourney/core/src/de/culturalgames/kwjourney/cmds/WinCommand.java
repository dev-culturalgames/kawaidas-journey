package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.actions.KJActions;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.cmds.Command;

public class WinCommand extends Command {

    private static final String VALID_COMMAND = " win";

    @Override
    public float execute(SequenceAction sequence) {
        EJXGame game = gameControl.getGame();
        gameControl.getPlayerInput().setIngameControl(false);
        game.getScreen().getMenu().getUI().toggleHUD(false);

        sequence.addAction(KJActions.showPopup(POPUP.WIN, game));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        return 0;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND;
    }
}