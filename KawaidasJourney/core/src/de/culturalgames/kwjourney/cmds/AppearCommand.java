package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.cmds.Commands.CMD;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class AppearCommand extends Command {

    private static final String VALID_COMMAND = " appear|disappear(:<float_duration>)";

    private boolean appear = true;
    private float duration = 0f;

    @Override
    public float execute(SequenceAction sequence) {
        if (execType != ExecutorType.THIS) {
            sequence = Actions.sequence();
            executor.getActor().addAction(sequence);
        }
        if (appear) ((AnimatedEntity) executor).appear(duration, sequence);
        else((AnimatedEntity) executor).disappear(duration, sequence);
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        appear = split[1].equals(CMD.APPEAR.name);

        duration = 0f;
        if (split.length >= 3) {
            if (!split[2].matches(EJXRegEx.matchFloat)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + EJXError.ERROR_INVALID_PARAM_FLOAT);
                return -1;
            }
            duration = Float.parseFloat(split[2]);

            if (split.length > 3) {
                VJXLogger.log(LogCategory.WARNING,
                                EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
            }
        }
        return 0;
    }

    public void setAppear(boolean appear) {
        this.appear = appear;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + appear
                        + VJXString.SEP_LIST_SPACE + duration;
    }
}