package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.content.Secrets;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class SecretCommand extends Command {

    private static final String VALID_COMMAND = " secret:<string_secretName>";
    private static final String SECRET_NOT_FOUND = " Target secret object not found.";
    private static final String SECRET_NOT_SET = " Target secret object not set.";

    private Secret secret;

    @Override
    public float execute(SequenceAction sequence) {
        if (secret == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, SECRET_NOT_SET);
            return 0f;
        }

        EJXGame game = gameControl.getGame();
        sequence.addAction(KJActions.showSecret(secret, executor, game));
        game.getScreen().getMenu().getUI().toggleHUD(false);
        return 0f;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR,
                            EJXError.ERROR_PARAMS_COUNT_1 + VALID_COMMAND);
            return -1;
        }

        secret = Secrets.getSecret(split[2]);
        if (secret == null) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                            + SECRET_NOT_FOUND);
            return -1;
        }

        if (split.length > 3) {
            VJXLogger.log(LogCategory.WARNING,
                            EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
        }
        return 0;
    }

    public void setSecret(Secret secret) {
        this.secret = secret;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + secret;
    }
}