package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.KJLevel;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class GrowDecoCommand extends Command {

    private static final String VALID_COMMAND = " growDeco:<string_position>(:<float_range>)";
    private static final String LEVEL_NOT_SET = " KJLevel not set.";

    private KJLevel level;
    private Vector2 position = new Vector2();
    private float range = -1;

    @Override
    public float execute(SequenceAction sequence) {
        if (level == null) {
            VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_COMMAND_NOT_EXECUTED + cmdString);
            VJXLogger.log(LogCategory.WARNING, LEVEL_NOT_SET);
            return 0f;
        }
        level.growDeco(position, range);
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        if (split.length < 3) {
            VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.ERROR,
                            EJXError.ERROR_PARAMS_COUNT_1_2 + VALID_COMMAND);
            return -1;
        } else {
            Vector2 pos = EJXUtil.parsePosition(split[2], position, level);
            if (pos == null) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + EJXError.ERROR_INVALID_PARAM_POSITION);
                return -1;
            }

            if (split.length >= 4) {
                if (!split[3].matches(EJXRegEx.matchFloat)) {
                    VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                    VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS2
                                    + EJXError.ERROR_INVALID_PARAM_FLOAT);
                    return -1;
                }
                range = Float.parseFloat(split[3]);

                if (split.length > 4) {
                    VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_2 + cmdString);
                }
                return 0;
            }
        }
        return 0;
    }

    @Override
    public void setController(GameControl listener) {
        super.setController(listener);

        if (!(listener.getLevel() instanceof KJLevel)) {
            VJXLogger.log(LogCategory.WARNING, ERROR_PARSING_COMMAND + cmdString);
            VJXLogger.log(LogCategory.WARNING, LEVEL_NOT_SET);
        } else level = (KJLevel) listener.getLevel();
    }

    public void setRange(float range) {
        this.range = range;
    }

    public void setPosition(float x, float y) {
        position.set(x, y);
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + position
                        + VJXString.SEP_LIST_SPACE + range;
    }
}