package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.math.Interpolation;

import de.culturalgames.kwjourney.content.Secrets;
import de.culturalgames.kwjourney.entity.logics.HintLogic.Hint;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.cmds.Commands;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.entity.EJXEntity;

public class KJCommands extends Commands {

    public static SetControlsCommand getControlsCommand(EJXGame game,
                    boolean active) {
        SetControlsCommand cmd = command(SetControlsCommand.class);
        cmd.setController(game.gameControl);
        cmd.setActivateControls(active);
        return cmd;
    }

    public static AppearCommand getAppearCommand(AnimatedEntity executor,
                    boolean appear, float duration) {
        AppearCommand cmd = command(AppearCommand.class);
        cmd.setController(executor.getListener());
        cmd.setExecutor(executor);
        cmd.setAppear(appear);
        cmd.setDuration(duration);
        return cmd;
    }

    public static Jump2Command getJump2Command(EJXEntity executor, float height,
                    float speed, Interpolation interpolation, boolean force) {
        Jump2Command cmd = command(Jump2Command.class);
        cmd.setController(executor.getListener());
        cmd.setExecutor(executor);
        cmd.setForce(force);
        cmd.setHeight(height);
        cmd.setSpeed(speed);
        cmd.setInterpolation(interpolation);
        return cmd;
    }

    public static JumpCommand getJumpCommand(EJXEntity executor, float height,
                    boolean force) {
        JumpCommand cmd = command(JumpCommand.class);
        cmd.setController(executor.getListener());
        cmd.setExecutor(executor);
        cmd.setForce(force);
        cmd.setHeight(height);
        return cmd;
    }

    public static TeleportCommand getPortCommand(EJXEntity executor, float x,
                    float y, float speed) {
        TeleportCommand cmd = command(TeleportCommand.class);
        cmd.setController(executor.getListener());
        cmd.setExecutor(executor);
        cmd.setPosition(x, y);
        cmd.setSpeed(speed);
        return cmd;
    }

    public static StoryCommand getStoryCommand(EJXGame game, String dialogue,
                    String cond, EJXEntity executor) {
        StoryCommand cmd = command(StoryCommand.class);
        cmd.setController(game.gameControl);
        cmd.setExecutor(executor);
        cmd.setDialogueName(dialogue);
        cmd.setCondition(cond);
        return cmd;
    }

    public static SecretCommand getSecretCommand(EJXGame game,
                    String secretName, EJXEntity owner) {
        SecretCommand cmd = command(SecretCommand.class);
        cmd.setController(game.gameControl);
        cmd.setExecutor(owner);
        cmd.setSecret(Secrets.getSecret(secretName));
        return cmd;
    }

    public static HintCommand getHintCommand(EJXGame game, Hint hint) {
        HintCommand cmd = command(HintCommand.class);
        cmd.setController(game.gameControl);
        cmd.setHint(hint);
        return cmd;
    }

    public static BonusCommand getBonusCommand(EJXGame game, boolean found) {
        BonusCommand cmd = command(BonusCommand.class);
        cmd.setController(game.gameControl);
        cmd.setBonusFound(found);
        return cmd;
    }

    public static GrowDecoCommand getGrowDecoCommand(EJXGame game, float x,
                    float y, float range) {
        GrowDecoCommand cmd = command(GrowDecoCommand.class);
        cmd.setController(game.gameControl);
        cmd.setRange(range);
        cmd.setPosition(x, y);
        return cmd;
    }
}