package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.entity.logics.DialogLogic;
import de.culturalgames.kwjourney.entity.logics.DialogLogic.Dialog;
import de.venjinx.ejx.EJXGameScreen;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.entity.AnimatedEntity;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class StoryCommand extends Command {

    private static final String dialogue = "dialogue";
    private static final String dialogueCond = "dialogueCond";

    private static final String VALID_COMMAND = " dialogue(:<string_dialogName>)";
    //    private static final String NO_STORY_LOGIC = "Object does not have a StoryLogic.";

    private String dialogName;
    private String condition;

    @Override
    public float execute(SequenceAction sequence) {
        return newExec(sequence);
    }

    private float newExec(SequenceAction sequence) {
        String currentDialogName = dialogName;
        if (dialogName.contains(VJXString.SEP_LIST)) {
            String[] split = dialogName.split(VJXString.SEP_LIST);
            String[] splitCond = condition.split(VJXString.SEP_LIST);
            currentDialogName = checkDialogueCond(split, splitCond);
        }

        Dialog dialog = DialogLogic.getDialog(currentDialogName);
        if (dialog != null) {
            EJXGameScreen screen = gameControl.getGame().getScreen();
            gameControl.getPlayerInput().setIngameControl(false);
            screen.getMenu().getUI().toggleHUD(false);

            // TODO localization handling old story logic
            if (dialog.getParticipants().isEmpty()) {
                dialog.addParticipant("speaker0", executor);
                if (executor.getTargetEntity() != null)
                dialog.addParticipant("speaker1", executor.getTargetEntity());
            }

            if (!executor.isVisible()) {
                AppearCommand appearCommand = KJCommands.getAppearCommand(
                                (AnimatedEntity) executor, true, 1f);
                executor.execSingleCmd(appearCommand);
            }

            if (dialog.getDelay() > 0)
                sequence.addAction(Actions.delay(dialog.getDelay()));

            sequence.addAction(KJActions.showStory(dialog, gameControl.getGame()));

            if (!dialog.isRepeat()) {
                DialogLogic.removeDialog(dialog.getName());
            }
        }
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;

        String[] split = cmdString.split(VJXString.SEP_DDOT);
        dialogName = VJXString.STR_EMPTY;
        if (split.length >= 3) {
            dialogName = split[2];
        }

        if (dialogName.isEmpty()) {
            MapProperties props = executor.getDef().getCustomProperties();
            dialogName = props.get(dialogue, String.class);
            condition = props.get(dialogueCond, String.class);
        }

        if (split.length > 3) {
            VJXLogger.log(LogCategory.WARNING,
                            EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
        }
        return 0;
    }

    private String checkDialogueCond(String[] names, String[] conds) {
        if (conds.length != names.length - 1) {
            return names[0];
        }
        for (int i = 0; i < conds.length; i++)
            if (!gameControl.getGame().getScreen().getLevel().isProgressFinished())
                return names[i];
        return names[names.length - 1];
    }

    public void setDialogueName(String dialogue) {
        dialogName = dialogue;
    }

    public void setCondition(String cond) {
        condition = cond;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + dialogName;
    }
}