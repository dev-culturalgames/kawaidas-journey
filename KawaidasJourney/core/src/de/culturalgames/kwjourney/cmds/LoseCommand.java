package de.culturalgames.kwjourney.cmds;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.actions.KJActions;
import de.venjinx.ejx.cmds.Command;
import de.venjinx.ejx.util.EJXTypes.EJXError;
import de.venjinx.ejx.util.EJXTypes.EJXRegEx;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class LoseCommand extends Command {

    private static final String VALID_COMMAND = " lose(:<boolean_showContinue>)";

    private boolean showContinue = true;

    @Override
    public float execute(SequenceAction sequence) {
        sequence.addAction(KJActions.showPopup(POPUP.LOSE,
                        gameControl.getGame(), showContinue, false));
        return 0;
    }

    @Override
    protected int parse(String cmdString) {
        this.cmdString = cmdString;
        String[] split = cmdString.split(VJXString.SEP_DDOT);

        showContinue = true;
        if (split.length >= 3) {
            if (!split[2].matches(EJXRegEx.matchBool)) {
                VJXLogger.log(LogCategory.ERROR, ERROR_PARSING_COMMAND + cmdString);
                VJXLogger.log(LogCategory.ERROR, EJXError.ERROR_PARAM_POS1
                                + EJXError.ERROR_INVALID_PARAM_BOOL);
                return -1;
            }
            showContinue = Boolean.parseBoolean(split[2]);

            if (split.length > 3) {
                VJXLogger.log(LogCategory.WARNING, EJXError.WARNING_PARAMS_COUNT_1 + cmdString);
            }
        }
        return 0;
    }

    public void setShowContinue(boolean showContinue) {
        this.showContinue = showContinue;
    }

    @Override
    public String toString() {
        return executor + VALID_COMMAND + VJXString.SEP_DDOT_SPACE + showContinue;
    }
}