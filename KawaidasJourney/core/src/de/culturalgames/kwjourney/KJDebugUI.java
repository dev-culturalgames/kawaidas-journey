package de.culturalgames.kwjourney;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.entity.logics.InventoryLogic;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.LifeLogic;

public class KJDebugUI extends Window implements InputProcessor {

    private KJGame game;
    private KJMenu menu;
    private KJPlayer player;

    private TextButton restartBtn;
    private CheckBox godCheck;
    private TextButton lifeBtn;
    private TextButton winBtn;

    private boolean update;

    public KJDebugUI(final KJGame game) {
        super("Kawaida debug", game.getScreen().getDebugStage().getSkin());
        this.game = game;
        menu = game.getScreen().getMenu(KJMenu.class);
        player = (KJPlayer) game.getPlayer();

        restartBtn = new TextButton("Restart", getSkin());
        restartBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.getScreen().restartLevel();
            }
        });

        godCheck = new CheckBox("God mode (g)", getSkin());
        godCheck.addListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (update) {
                    if (player.getEntity() == null) return;
                    LifeLogic life = (LifeLogic) player.getEntity()
                                    .getLogic(KJLogicType.LIFE);
                    life.setInvulnerable(!life.isInvulnerable());
                }
            }
        });

        lifeBtn = new TextButton("Banana (b)", getSkin());
        lifeBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (player.getEntity() == null) return;
                InventoryLogic inventory = (InventoryLogic) player.getEntity()
                                .getLogic(KJLogicType.INVENTORY);
                inventory.incr(ItemCategory.POTION, 1);
            }
        });

        winBtn = new TextButton("Win (o)", getSkin());
        winBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menu.showPopup(POPUP.WIN.id);
            }
        });

        Table buttons = new Table();
        buttons.add(restartBtn).prefWidth(100).pad(0, 10, 0, 10);
        buttons.add(winBtn).prefWidth(100).pad(0, 10, 0, 10);
        buttons.row();
        buttons.add(godCheck).prefWidth(100).pad(0, 10, 0, 10);
        buttons.add(lifeBtn).prefWidth(100).pad(0, 10, 0, 10);

        Table mainTable = new Table();
        mainTable.add(buttons);
        //        mainTable.setDebug(true, true);

        add(mainTable).expand().fill();
        pack();
    }

    @Override
    public void act(float delta) {
        if (!game.getScreen().getLevel().isActive()) return;
        update = false;
        LifeLogic lLogic = (LifeLogic) player.getEntity().getLogic(KJLogicType.LIFE);
        godCheck.setChecked(lLogic.isInvulnerable());
        update = true;
    }

    @Override
    public boolean keyDown(int keyCode) {
        if (keyCode == Keys.G) {
            godCheck.setChecked(!godCheck.isChecked());
            return true;
        }

        if (player.getEntity() == null) return false;

        if (keyCode == Keys.B) {
            InventoryLogic inventory = (InventoryLogic) player.getEntity()
                            .getLogic(KJLogicType.INVENTORY);
            inventory.incr(ItemCategory.POTION, 1);
            return true;
        }

        if (keyCode == Keys.L) {
            LifeLogic lLogic = (LifeLogic) player.getEntity().getLogic(KJLogicType.LIFE);
            lLogic.die();
            return true;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer,
                    int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}