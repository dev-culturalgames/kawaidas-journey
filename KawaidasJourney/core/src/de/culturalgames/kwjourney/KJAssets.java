package de.culturalgames.kwjourney;

public class KJAssets {

    // ui sounds
    public static final String SFX_APPLAUSE = "sfx:applause";
    public static final String SFX_BACK = "sfx:back";
    public static final String SFX_CLICK = "sfx:click";
    public static final String SFX_PBAR = "sfx:fill";
    public static final String SFX_LOSE = "sfx:lose";
    public static final String SFX_POPUP_OPEN = "sfx:popup_open";
    public static final String SFX_POPUP_CLOSE = "sfx:popup_close";
    public static final String SFX_SF_SPAWN = "sfx:sf_spawn";
    public static final String SFX_SF_SPAWNED = "sfx:sf_spawned";
    public static final String SFX_WIN_VO_0 = "sfx:win_vo_0";
    public static final String SFX_WIN_VO_1 = "sfx:win_vo_1";
    public static final String SFX_WIN_VO_2 = "sfx:win_vo_2";

    // music
    public static final String MUSIC_MAIN = "music:main";
    public static final String MUSIC_INTRO = "music:intro";
    public static final String MUSIC_OUTRO0 = "music:outro0";
    public static final String MUSIC_OUTRO1 = "music:outro1";
    public static final String MUSIC_OUTRO2 = "music:outro2";
    public static final String MUSIC_OUTRO3 = "music:outro3";
    public static final String MUSIC_CREDITS = "music:credits";
    public static final String MUSIC_END_SCREEN = "music:endScreen";

    // video sequences names
    public static final String VIDEO_INTRO = "intro";
    public static final String VIDEO_OUTRO0 = "outro0";
    public static final String VIDEO_OUTRO1 = "outro1";
    public static final String VIDEO_OUTRO2 = "outro2";
    public static final String VIDEO_OUTRO3 = "outro3";

    // image paths
    public static final String PATH_IMAGE_LOADING_DFLT = "images/loading/loading_bg_dflt.jpg";
    public static final String PATH_IMAGE_UNLOADING_DFLT = "images/loading/unloading_bg_dflt.jpg";

    public static final String PATH_IMAGE_SPLASH = "images/menu/splash.jpg";
    public static final String PATH_IMAGE_KJ_LOGO = "images/kj_logo.png";
    public static final String PATH_IMAGE_MENU_START = "images/menu/menu_start.jpg";
    public static final String PATH_IMAGE_MAP_EASTAFRICA = "images/menu/map_eastAfrica.jpg";
    public static final String PATH_IMAGE_CREDITS0 = "images/menu/credits_kawaida_passion.jpg";
    public static final String PATH_IMAGE_CREDITS1 = "images/menu/credits_kickstarter.jpg";

    // images
    public static final String IMAGE_KJ_LOGO = "image_kj_logo";
    public static final String IMAGE_MENU_START = "image_menu_start";
    public static final String IMAGE_CREDITS0 = "image_credits_kawaida_passion";
    public static final String IMAGE_CREDITS1 = "image_credits_kickstarter";
    public static final String IMAGE_MAP_EASTAFRICA = "image_map_eastAfrica";

    public static final String ATLAS_GLOBAL_UI = "global_ui";
    public static final String ATLAS_GLOBAL_HUD = "global_hud";
    public static final String ATLAS_GLOBAL_HOW = "global_how";
    public static final String ATLAS_SHOP_CHARACTER = "shop_character";

    // sprite sheets
    public static final String PATH_ATLAS_GLOBAL_UI = "spriteSheets/global/global_ui.atlas";
    public static final String PATH_ATLAS_GLOBAL_HUD = "spriteSheets/global/global_hud.atlas";
    public static final String PATH_ATLAS_GLOBAL_HOW = "spriteSheets/global/global_how.atlas";
    public static final String PATH_ATLAS_SHOP_CHARACTER = "spriteSheets/shop/shop_character.atlas";

    // buttons
    public static final String BUTTON_TRANSPARENT_256 = "btn_transparent_256";
    public static final String BUTTON_BG_TRANS_00 = "bg_trans_00";
    public static final String BUTTON_BG_TRANS_80 = "bg_trans_80";
    public static final String BUTTON_BACK = "controls/btn_back";
    public static final String BUTTON_BLUE = "controls/btn_blue";
    public static final String BUTTON_CLOSE = "controls/btn_close";
    public static final String BUTTON_CONTINUE = "controls/btn_continue";
    public static final String BUTTON_CYAN = "controls/btn_cyan";
    public static final String BUTTON_GREEN = "controls/btn_green";
    public static final String BUTTON_GREEN_SMALL = "controls/btn_green_small";
    public static final String BUTTON_HELP = "controls/btn_help";
    public static final String BUTTON_NEXT = "controls/btn_next";
    public static final String BUTTON_PROJECTS = "controls/btn_projects";
    public static final String BUTTON_RED = "controls/btn_red";
    public static final String BUTTON_SETTINGS = "controls/btn_settings";
    public static final String BUTTON_SHARE = "controls/btn_share";
    public static final String BUTTON_SOUNDTRACK = "controls/btn_soundtrack";
    public static final String BUTTON_START = "controls/btn_start";
    public static final String BUTTON_PAGES = "controls/btn_pages";
    public static final String BUTTON_PAGES_ACTIVE = "controls/btn_pages_active";
    public static final String BUTTON_WHITE_SMALL = "controls/btn_white_small";
    public static final String BUTTON_WHITE_SMALL_ACTIVE = "controls/btn_white_small_active";
    public static final String BUTTON_WHITE_THIN = "controls/btn_white_thin";
    public static final String BUTTON_YELLOW = "controls/btn_yellow";
    public static final String BUTTON_YELLOW_SMALL = "controls/btn_yellow_small";

    // flag buttons
    public static final String BUTTON_FLAG_ENG = "controls/flags/btn_flag_eng";
    public static final String BUTTON_FLAG_GER = "controls/flags/btn_flag_ger";
    public static final String BUTTON_FLAG_TZ = "controls/flags/btn_flag_tz";

    // settings
    public static final String ICON_CLOUD_ACTIVE = "icons/settings/ico_cloud_active";
    public static final String ICON_CLOUD_INACTIVE = "icons/settings/ico_cloud_inactive";
    public static final String ICON_CLOUD_DISABLED = "icons/settings/ico_cloud_disabled";
    public static final String ICON_DEBUG = "icons/settings/ico_debug";
    public static final String ICON_FX_OFF = "icons/settings/ico_fx0_off";
    public static final String ICON_FX_ON = "icons/settings/ico_fx0_on";
    public static final String ICON_LANGUAGE = "icons/settings/ico_language0";
    public static final String ICON_MUSIC = "icons/settings/ico_music";
    public static final String ICON_OFF_RED = "icons/settings/ico_off_red";
    public static final String ICON_RESET = "icons/settings/ico_reset";
    public static final String ICON_RESTORE = "icons/settings/ico_restore";

    // social
    public static final String ICON_DISCORD = "social/ico_discord_white";
    public static final String ICON_FACEBOOK = "social/ico_facebook_white";
    public static final String ICON_INSTAGRAM = "social/ico_instagram_white";
    public static final String ICON_TWITTER = "social/ico_twitter_white";
    public static final String ICON_YOUTUBE = "social/ico_youtube_white";

    // social links
    public static final String LINK_SOUNDTRACK = "https://culturalgames.bandcamp.com/album/kawaidas-journey-soundtrack";
    public static final String LINK_KAWAIDA = "https://www.kawaidasjourney.de/";
    public static final String LINK_DISCORD = "https://discordapp.com/invite/uftcZBF";
    public static final String LINK_FACEBOOK = "https://www.facebook.com/kawaidasjourney";
    public static final String LINK_INSTAGRAM = "https://www.instagram.com/kawaidasjourney/";
    public static final String LINK_TWITTER = "https://twitter.com/kawaidasjourney";
    public static final String LINK_YOUTUBE = "https://www.youtube.com/playlist?list=PLGoF9yMGVRzKh_9stePVA9oQgFWd__RnL";

    // icons
    public static final String ICON_ANANAS = "icons/ico_ananas";
    public static final String ICON_EMBLEM_BG = "icons/ico_emblem_bg";
    public static final String ICON_FORWARD = "icons/ico_forward";
    public static final String ICON_LOADING = "icons/ico_loading";
    public static final String ICON_LOSE = "icons/ico_lose";
    public static final String ICON_LOCK = "icons/ico_lock";
    public static final String ICON_LOCK_OPEN = "icons/ico_lock_open";
    public static final String ICON_STAR = "icons/ico_star";
    public static final String ICON_STAR_EMPTY = "icons/ico_star_empty";
    public static final String ICON_REPORT_BUG = "icons/ico_reportBug";
    public static final String ICON_STRIPE = "icons/ico_stripe";
    public static final String ICON_SECRET_NEW = "icons/secrets/ico_secret_new";
    public static final String ICON_SECRETS_PROGRESS = "icons/secrets/ico_secrets_progress";
    public static final String ICON_SUN_DBL_WIN = "icons/ico_sun_double_win";
    public static final String ICON_WHITE_MEDIUM = "icons/ico_white_medium";
    public static final String ICON_WIN_PBAR_BG = "icons/ico_pBar_bg";
    public static final String ICON_WIN_PBAR_FILL = "icons/ico_pBar_full";

    // map
    public static final String MAP_PATH_WHITE = "icons/map/map_path_white";

    // shop
    public static final String SHOP_PRICE_AVAILABLE = "icons/shop/shop_priceTag_available";
    public static final String SHOP_PRICE_PRIME = "icons/shop/shop_priceTag_prime";
    public static final String SHOP_PRICE_SOLAR = "icons/shop/shop_priceTag_solar";
    public static final String SHOP_PRICE_SOLD = "icons/shop/shop_priceTag_solar";

    // offers
    public static final String OFFER_SUN_DBL = "offers/prime/offer_sun_double";
    public static final String OFFER_UPGRADE_START_EMPTY = "offers/upgradeBar/upgradeBar_start_empty";
    public static final String OFFER_UPGRADE_START_FILLED = "offers/upgradeBar/upgradeBar_start_filled";
    public static final String OFFER_UPGRADE_MID_EMPTY = "offers/upgradeBar/upgradeBar_mid_empty";
    public static final String OFFER_UPGRADE_MID_FILLED = "offers/upgradeBar/upgradeBar_mid_filled";
    public static final String OFFER_UPGRADE_END = "offers/upgradeBar/upgradeBar_end";
    public static final String OFFER_UPGRADE_COMPLETED = "offers/upgradeBar/upgradeBar_completed";

    // projects menu

    // encyclopedia menu

    // secrets

    // map menu

    // hud
    // icons
    public static final String ICON_BANANA = "ico_banana";
    public static final String ICON_COCO = "ico_coco";
    public static final String ICON_JOY_KNOB = "ico_joy_knob";
    public static final String ICON_JUMP = "ico_jump";
    public static final String ICON_SUN = "ico_sun";
    public static final String ICON_SWIPE_DOWN = "ico_swipe_down";
    public static final String ICON_SWIPE_LEFT_RIGHT = "ico_swipe_left_right";
    public static final String ICON_TOTEM = "ico_totem";
    // generated from code
    public static final String ICON_LVL_PBAR_BG = "ico_lvl_pBar_bg";
    public static final String ICON_LVL_PBAR_FILL = "ico_lvl_pBar_fill";
    public static final String ICON_LVL_PBAR_OVERLAY = "ico_lvl_pBar_overlay";

    // context icons
    public static final String ICON_CONTEXT_PLANT = "context/ico_context_plant";
    public static final String ICON_CONTEXT_EXTINGUISH = "context/ico_context_extinguish";
    public static final String ICON_CONTEXT_TELEPORT = "context/ico_context_teleport";
    public static final String ICON_CONTEXT_TRASH = "context/ico_context_trash";
    public static final String ICON_CONTEXT_REPAIR = "context/ico_context_repair";
    public static final String ICON_CONTEXT_BROKEN = "context/ico_context_broken";

    // buttons
    public static final String BUTTON_BACKGROUND = "controls/btn_background";
    public static final String BUTTON_PLANT_BG = "controls/btn_context_plant_bg";
    public static final String BUTTON_EXTINGUISH_BG = "controls/btn_context_extinguish_bg";
    public static final String BUTTON_TELEPORT_BG = "controls/btn_context_teleport_bg";
    public static final String BUTTON_TRASH_BG = "controls/btn_context_trash_bg";
    public static final String BUTTON_REPAIR_BG = "controls/btn_context_repair_bg";
    public static final String BUTTON_BROKEN_BG = "controls/btn_context_repair_bg";
    public static final String BUTTON_PAUSE = "controls/btn_pause";
}