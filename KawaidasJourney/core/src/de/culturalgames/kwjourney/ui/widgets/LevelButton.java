package de.culturalgames.kwjourney.ui.widgets;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.actions.VisibleAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame.GameStats;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.api.APILoader;
import de.culturalgames.kwjourney.api.PurchaseHandler.ShopItem;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.save.KJCloudSave;
import de.culturalgames.kwjourney.ui.ShopMenu;
import de.culturalgames.kwjourney.ui.menus.LoadingMenu;
import de.culturalgames.kwjourney.ui.menus.MapMenu;
import de.culturalgames.kwjourney.ui.menus.MapMenu.MAPS;
import de.culturalgames.kwjourney.ui.popups.InfoPopup;
import de.culturalgames.kwjourney.util.KJTexts;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTresor.Lockable;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;

public class LevelButton extends KJIconTextButton implements Lockable {

    private static final String levelButton_mainTable = "levelButton_table";
    private static final String levelButton_icon = "levelButton_icon";

    private static final String stars_table = "stars_table";
    private static final String star0 = "star0";
    private static final String star1 = "star1";
    private static final String star2 = "star2";

    private KJMenu menu;

    private LockIcon lockIcon;
    private Drawable[] drawables;
    private List<Image> path;

    private String levelPath;
    private int chapterNr;

    private boolean isSubMap;
    private boolean locked;
    private int starsNeeded;

    private boolean finished;
    private boolean available;
    private Image[] stars;

    private boolean isDev;
    private boolean chapterBegin;
    private String nextBtnName;
    private String lastLevel;
    private String mapName;
    private String reward;

    private float pathAnimTime = 2f;

    private ClickListener startLevelListener;
    private ClickListener unlockLevelListener;

    public LevelButton(String name, MenuStage menu, MapProperties props, String map) {
        super(name, VJXString.STR_EMPTY, menu.getSkin());
        setName(name);
        path = new ArrayList<>();

        this.menu = (KJMenu) menu;
        mapName = map;

        loadButtonProps(props, menu.getGame());

        setVisible(chapterBegin || menu.getGame().isDevMode() || isDev);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (!finished && !locked && isVisible() && getActions().size == 0)
            addAction(KJActions.bounce(.85f, 1.15f, .5f));
    }

    public void updateButton(final EJXGame game) {
        MapMenu mapMenu = (MapMenu) game.getScreen().getMenu()
                        .getMenu(MENU.MAP.id);
        LevelButton nextBtn = mapMenu.getLevelButton(nextBtnName);
        LocalSavegame savegame = game.getSave();
        boolean finished = false;
        if (this.finished) {
            if (nextBtn != null) {
                setPathVisible(true);
                nextBtn.setAvailable(true);
            }
            finished = true;
        } else {
            finished = savegame.getBool(GameStats.LVL_FINISH.name + getName(), false);
            if (finished) {
                clearActions();
                SequenceAction pathAnim = Actions.sequence();
                pathAnim.addAction(EJXActions.bounceIn(1.25f, .25f));
                pathAnim.addAction(Actions.delay(.4f));
                if (nextBtn != null) {
                    pathAnimation(pathAnim, nextBtn);
                }
                addAction(pathAnim);
            } else {
                if (starsNeeded())
                    checkLock(game);
                setPathVisible(false);
            }
            if (isDev) finished = true;
        }

        this.finished = finished;

        setVisible(finished || chapterBegin || available);

        // if game in dev mode set next level avaialable
        if (!finished && game.isDevMode()) {
            setVisible(true);
            if (getListeners().contains(unlockLevelListener, true)) {
                removeListener(unlockLevelListener);
                addListener(startLevelListener);
            }
        }

        if (isSubMap) {
            nextBtn = mapMenu.getLevelButton(lastLevel);
            if (nextBtn != null) {
                this.finished = nextBtn.isFinished();
                setPathVisible(this.finished);
            }
            return;
        }
        checkStars(game.getSave());

        checkReward(game);
    }

    private void checkReward(EJXGame game) {
        if (!reward.isEmpty()) {

            if (finished) {
                KJMenu kjMenu = game.getScreen().getMenu(KJMenu.class);
                VJXOffer offer = APILoader.getOffer(KJString.OFFER_skin_ + reward);
                if (offer == null) offer = APILoader.getOffer(reward);
                ShopMenu shop = (ShopMenu) kjMenu.getMenu(MENU.SHOP_SKINS.id);
                ShopButton shopButton = shop.getOfferButtons()
                                .get(offer.getIdentifier());

                if (shopButton.getLockIcon().isLocked()) {
                    if (menu.isLocked(shopButton.getLockIcon().getSaveString()))
                        kjMenu.showReward(offer.getIdentifier(), 0,
                                        shopButton.getLockIcon());
                    kjMenu.unlock(shopButton, shopButton.getLockIcon());
                }
            }
        }
    }

    private boolean checkLock(EJXGame game) {
        if (!menu.isLocked(lockIcon.getSaveString())) return false;
        int currentStars = game.getSave().getInt(GameStats.STARS.name, 0);
        if (game.isDevMode()) currentStars = starsNeeded;
        lockIcon.setStarsFound(Integer.toString(currentStars));
        if (!lockIcon.isLocked()) {
            menu.lock(this, lockIcon);
        }
        return true;
    }

    private void checkStars(LocalSavegame savegame) {
        boolean bon = savegame.getBool(GameStats.LVL_BONUS.name + getName(),
                        false);
        boolean sun = savegame.getBool(GameStats.LVL_SUNS.name + getName(),
                        false);

        if (finished) stars[0].setDrawable(drawables[1]);
        else stars[0].setDrawable(drawables[0]);

        if (bon) stars[1].setDrawable(drawables[3]);
        else stars[1].setDrawable(drawables[2]);

        if (sun) stars[2].setDrawable(drawables[5]);
        else stars[2].setDrawable(drawables[4]);
    }

    public void setFinished(boolean finished) {
        setVisible(finished);
        this.finished = finished;
        setPathVisible(finished);
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isAvailable() {
        return available;
    }

    public boolean isFinished() {
        return finished;
    }

    public boolean isLocked() {
        return menu.isLocked(lockIcon.getSaveString());
    }

    public void addPath(Image lp) {
        path.add(lp);
    }

    public String getNextBtnName() {
        return nextBtnName;
    }

    public boolean starsNeeded() {
        return starsNeeded > 0;
    }

    public boolean isSubMap() {
        return isSubMap;
    }

    public boolean isChapterBegin() {
        return chapterBegin;
    }

    public String getLevelPath() {
        return levelPath;
    }

    public int getChapterNr() {
        return chapterNr;
    }

    public LockIcon getLockIcon() {
        return lockIcon;
    }

    private void loadButtonProps(MapProperties props, EJXGame game) {
        chapterNr = props.get("chapter", 0, Integer.class);
        isSubMap = props.get("subMap", false, Boolean.class);
        starsNeeded = props.get("starsNeeded", 0, Integer.class);

        nextBtnName = props.get("nextPoint", String.class);
        if (nextBtnName == null || nextBtnName.isEmpty())
            mapName = MAPS.TZ.name;
        chapterBegin = props.get("chapterBegin", false, Boolean.class);
        isDev = props.get("isDev", false, Boolean.class);
        reward = props.get("reward", VJXString.STR_EMPTY, String.class);

        String iconName = props.get(VJXString.tileName, String.class);
        if (iconName != null) {
            Table imgTable = new Table();
            imgTable.setName(levelButton_mainTable);

            Image img = new Image(getSkin()
                            .getDrawable(KJString.PATH_icons_map + iconName));
            img.setName(levelButton_icon);
            setSize(img.getWidth(), img.getHeight());
            imgTable.add(img);
            mainTable.add(imgTable).expandX().top();
        }

        float w = props.get(VJXString.width, float.class);
        float h = props.get(VJXString.height, float.class);
        setSize(w, h);

        FileHandle file = props.get("levelPath", FileHandle.class);
        if (file != null)
            levelPath = file.path();
        else levelPath = null;

        lastLevel = props.get("lastLevel", String.class);

        initClickListener(props, game);

        locked = false;
        if (starsNeeded()) {
            String saveName = GameStats.CHAPTER_LOCKED.name + getName();
            lockIcon = new LockIcon(getSkin(), saveName, startLevelListener, unlockLevelListener);
            lockIcon.setScale(.75f);
            lockIcon.setRotation(10);
            lockIcon.setPosition(getWidth() / 2f - lockIcon.getWidth() / 2f,
                            getHeight() / 2f - lockIcon.getHeight() / 2f);

            lockIcon.showStarsNeeded(true);
            lockIcon.setStarsNeeded(Integer.toString(starsNeeded));
        }
        addListener(startLevelListener);
        finished = game.getSave().getBool(GameStats.LVL_FINISH.name + getName(),
                        false);

        if (!isSubMap)
            initStars(getSkin());

        setFont(EJXFont.TITLE_26);
        centerText();
    }

    private void initClickListener(final MapProperties props, final EJXGame game) {

        if (levelPath != null) {
            final FileHandle loadingImageFile = props.get("loading_background",
                            null, FileHandle.class);
            final String path;
            if (loadingImageFile != null) {
                if (loadingImageFile.path().isEmpty() || !loadingImageFile.exists())
                    path = KJAssets.PATH_IMAGE_LOADING_DFLT;
                else
                    path = loadingImageFile.path();
            } else path = KJAssets.PATH_IMAGE_LOADING_DFLT;

            startLevelListener = new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    KJMenu mc = (KJMenu) game.getScreen().getMenu();
                    LoadingMenu loadingMenu = (LoadingMenu) mc
                                    .getMenu(MENU.LOADING.id);
                    game.sfx.playDynSFX(KJAssets.SFX_CLICK);

                    if (isSubMap) {
                        MapMenu mapMenu = (MapMenu) mc.getMenu(MENU.MAP.id);
                        String mapName = EJXUtil.shortNameTMX(levelPath);
                        mapMenu.setMap(MAPS.get(mapName));
                        //                        updateButton(game);
                    } else {
                        game.assets.addResource(MenuStage.img_loading_bg_lvl,
                                        new AssetDescriptor<>(path,
                                                        Texture.class));
                        game.assets.finishLoading();
                        game.getScreen().getLevel().setLevelName(getName());
                        mc.setReturnMap(mapName);
                        mc.getScreen().loadLevel("data/" + levelPath, true);

                        // TODO localization check this for better usage
                        TextControl texts = TextControl.getInstance();
                        String name = KJTexts.LOADING_LEVEL_ + getName();

                        String textID = name + TextControl._TITLE;
                        String title = props.get("title", textID, String.class);
                        title = texts.get(textID, title);

                        textID = name + TextControl._SUBTITLE;
                        String subtitle= props.get("subtitle", textID, String.class);
                        subtitle = texts.get(textID, subtitle);

                        Drawable bgImage = game.assets.getDrawable(
                                        MenuStage.img_loading_bg_lvl);
                        loadingMenu.setTitle(getName(), title, subtitle, bgImage);

                        game.getScreen().getDebugStage().getDebugUI().resetStats();
                    }
                }
            };

            if (isDev) addListener(startLevelListener);

            final Runnable unlock = new Runnable() {

                @Override
                public void run() {
                    VJXOffer offer = APILoader.getOffer(ShopItem.UNLOCK_CHAPTER.name);
                    offer.setAmount(offer.getAmount() - 1);
                    offer.incrLvl();
                    ((KJCloudSave) game.getSave()).saveOffer(offer);
                    game.getScreen().getMenu(KJMenu.class).unlock(LevelButton.this, lockIcon);
                    game.getSave().write();
                }
            };

            unlockLevelListener = new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    TextControl texts = TextControl.getInstance();
                    InfoPopup infoD = (InfoPopup) game.getScreen().getMenu()
                                    .getPopup(POPUP.INFO.id);
                    int currentStars = game.getSave().getInt(GameStats.STARS.name, 0);
                    infoD.setTitle(getName().toUpperCase());
                    if (currentStars >= starsNeeded) {
                        infoD.setText(texts.get(KJTexts.INFO_UNLOCKED_CHAPTER));
                        infoD.setButtonText(texts.get(KJTexts.BTN_UNLOCK));
                        infoD.setOnConfirm(unlock);
                    } else {
                        infoD.setText(texts
                                        .get(KJTexts.INFO_NOT_ENOUGH_STARS_PRE)
                                        + Integer.toString(starsNeeded)
                                        + texts.get(KJTexts.INFO_NOT_ENOUGH_STARS_POST));
                        infoD.resetButtonText();
                        infoD.setOnConfirm(null);
                    }
                    game.getScreen().getMenu().showPopup(POPUP.INFO.id);
                }
            };
        }
    }

    private void initStars(Skin skin) {
        drawables = new Drawable[6];

        drawables[0] = skin.getDrawable(KJAssets.ICON_STAR_EMPTY);
        drawables[1] = skin.getDrawable(KJAssets.ICON_STAR);
        drawables[2] = skin.getDrawable(KJAssets.ICON_STAR_EMPTY);
        drawables[3] = skin.getDrawable(KJAssets.ICON_STAR);
        drawables[4] = skin.getDrawable(KJAssets.ICON_STAR_EMPTY);
        drawables[5] = skin.getDrawable(KJAssets.ICON_STAR);

        Table starsTable = new Table();
        starsTable.setName(stars_table);

        stars = new Image[3];
        stars[0] = new Image(drawables[0]);
        stars[0].setName(star0);
        stars[1] = new Image(drawables[2]);
        stars[1].setName(star1);
        stars[2] = new Image(drawables[4]);
        stars[2].setName(star2);

        stars[0].setPosition(-5, -10);

        stars[1].setOrigin(Align.center);
        stars[1].setRotation(180);
        stars[1].setPosition(getWidth() / 2 - stars[1].getWidth() / 2f, -15);

        stars[2].setPosition(getWidth() - stars[1].getWidth() + 5, -10);

        starsTable.addActor(stars[0]);
        starsTable.addActor(stars[1]);
        starsTable.addActor(stars[2]);

        stack.add(starsTable);
    }

    private void pathAnimation(final SequenceAction pathAnim,
                    final LevelButton nextBtn) {
        if (path.isEmpty()) return;
        float delay = pathAnimTime / path.size();

        for (Image lp : path) {
            pathAnim.addAction(Actions.delay(delay));

            VisibleAction va = Actions.visible(true);
            va.setTarget(lp);

            pathAnim.addAction(va);
        }
        pathAnim.addAction(Actions.delay(delay));
        pathAnim.addAction(Actions.run(new Runnable() {

            @Override
            public void run() {
                showButton(nextBtn);
            }
        }));
    }

    private void showButton(LevelButton next) {
        next.clearActions();
        if (!isSubMap) {
            next.setScale(.75f);
            next.setColor(1f, 1f, 1f, 0f);
            next.setVisible(true);
        }

        SequenceAction sa = Actions.sequence();
        AlphaAction alphaA = Actions.alpha(1f, .25f);
        SequenceAction bounce = EJXActions.bounceIn(1.25f, .25f);

        sa.addAction(alphaA);
        sa.addAction(bounce);
        next.addAction(sa);
    }

    public float setPathVisible(boolean visible) {
        for (Image lp : path)
            lp.setVisible(visible);
        return 0;
    }
}