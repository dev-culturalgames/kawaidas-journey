package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;

import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.widgets.TextButton;
import de.venjinx.vjx.ui.widgets.VJXIcon;

public class KJIconTextButton extends TextButton {

    private static final String _icon = "_icon";
    private static final String _icon_table = "_icon_table";
    private static final String iconName = "KJIconTextButton" + _icon;
    private static final String tableName = "KJIconTextButton" + _icon_table;

    protected VJXIcon icon;
    private Table iconTable;

    private int currentAlign = Align.center;
    private float offsetX = 0f;
    private float offsetY = 0f;

    public KJIconTextButton(String name, String text, Skin skin) {
        this(name, text, skin, null);
    }

    public KJIconTextButton(String name, String text, Skin skin,
                    Drawable iconDrawable) {
        super(name, text, skin);

        stack.removeActor(mainTable);

        icon = new VJXIcon(VJXString.STR_EMPTY, iconDrawable, skin);
        icon.setName(iconName);

        iconTable = new Table();
        iconTable.setName(tableName);
        iconTable.addActor(icon);
        stack.add(iconTable);

        stack.add(mainTable);
        alignIcon(currentAlign);
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        if (icon == null) return;
        icon.setName(name + _icon);
        iconTable.setName(name + _icon_table);
    }

    public void setIcon(Drawable d) {
        icon.setIconDrawable(d);
        alignIcon(currentAlign);
    }

    public void setIconText(String text) {
        icon.setText(text);
    }

    public void setIconSize(float width, float height) {
        icon.setSize(width, height);
        alignIcon(currentAlign, false);
    }

    public void setIconPosition(float x, float y) {
        icon.setPosition(x + offsetX, y + offsetY);
    }

    public void setIconOffset(float x, float y) {
        offsetX = x;
        offsetY = y;
        icon.setPosition(icon.getX() + x, icon.getY() + y);
    }

    public void setIconVisible(boolean visible) {
        icon.setVisible(visible);
    }

    public void setIconEnabled(boolean enabled) {
        icon.setEnabled(enabled);
    }

    public void alignIcon(int align) {
        alignIcon(align, true);
    }

    public void alignIcon(int align, boolean internal) {
        currentAlign = align;
        float iw = icon.getWidth();
        float ih = icon.getHeight();
        float w = getWidth();
        float h = getHeight();

        switch (currentAlign) {
            case Align.center:
                icon.setPosition(w / 2f - iw / 2f, h / 2f - ih / 2f);
                break;
            case Align.top:
                ih = internal ? ih : ih / 2f;
                icon.setPosition(w / 2f - iw / 2f, h - ih);
                break;
            case Align.bottom:
                ih = internal ? 0 : -ih / 2f;
                icon.setPosition(w / 2f - iw / 2f, ih);
                break;
            case Align.left:
                iw = internal ? 0 : -iw / 2f;
                icon.setPosition(iw, h / 2f - ih / 2f);
                break;
            case Align.right:
                iw = internal ? iw : iw / 2f;
                icon.setPosition(w - iw, h / 2f - ih / 2f);
                break;
            case Align.bottomLeft:
                iw = internal ? 0 : -iw / 2f;
                ih = internal ? 0 : -ih / 2f;
                icon.setPosition(iw, ih);
                break;
            case Align.bottomRight:
                iw = internal ? iw : iw / 2f;
                ih = internal ? 0 : -ih / 2f;
                icon.setPosition(w - iw, ih);
                break;
            case Align.topLeft:
                iw = internal ? 0 : -iw / 2f;
                ih = internal ? ih : ih / 2f;
                icon.setPosition(iw, h - ih);
                break;
            case Align.topRight:
                iw = internal ? iw : iw / 2f;
                ih = internal ? ih : ih / 2f;
                icon.setPosition(w - iw, h - ih);
                break;
        }
        setIconOffset(offsetX, offsetY);
    }

    public VJXIcon getIcon() {
        return icon;
    }

    public void centerIconText() {
        icon.centerText();
    }
}