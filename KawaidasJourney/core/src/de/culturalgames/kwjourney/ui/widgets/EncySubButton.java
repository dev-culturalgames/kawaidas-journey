package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class EncySubButton extends KJIconTextButton {

    private static final String STR_QM = "?";

    private Secret secret;
    private Image newIcon;
    private ClickListener listener;

    public EncySubButton(Skin skin) {
        super(KJAssets.BUTTON_WHITE_SMALL, STR_QM, skin);

        setSize(160, 160);
        setIconSize(160, 160);

        newIcon = new Image(skin.getDrawable(KJAssets.ICON_SECRET_NEW));
        newIcon.setPosition(getWidth() - newIcon.getWidth(),
                        getHeight() - newIcon.getHeight());
        addActor(newIcon);
    }

    public void setListener(ClickListener listener) {
        if (this.listener != null) removeListener(this.listener);
        this.listener = listener;
        addListener(listener);
    }

    public void setSecret(Secret secret, LocalSavegame savegame) {
        this.secret = secret;

        String saveString = secret.getKsString();
        if (savegame.getBool(saveString, false)) {
            updateIconImage();

            setText(VJXString.STR_EMPTY);
            centerText();
            setTouchable(Touchable.enabled);
            newIcon.setVisible(savegame.getBool(saveString + VJXString.POST_new,
                            false));
        } else {
            setIconVisible(false);

            setText(STR_QM);
            centerText();
            setTouchable(Touchable.disabled);
            newIcon.setVisible(false);
        }
    }

    public Secret getSecret() {
        return secret;
    }

    public void unlock() {
        updateIconImage();

        setText(VJXString.STR_EMPTY);
        setTouchable(Touchable.enabled);
    }

    private void updateIconImage() {
        Drawable d = getSkin().getDrawable(secret.getIconName());
        if (d != null) {
            setIconVisible(true);
            setIcon(d);
        } else setIconVisible(false);
    }

    public void setIsNew(boolean isNew) {
        newIcon.setVisible(isNew);
    }

    @Override
    public void layout() {
        super.layout();

        if (newIcon != null)
            newIcon.setPosition(getWidth() - newIcon.getWidth(),
                            getHeight() - newIcon.getHeight());
    }
}