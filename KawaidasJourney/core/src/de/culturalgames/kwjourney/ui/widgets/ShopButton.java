package de.culturalgames.kwjourney.ui.widgets;

import java.util.HashMap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.pay.Information;
import com.badlogic.gdx.pay.PurchaseManager;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KJGame.GameStats;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.api.PurchaseHandler;
import de.culturalgames.kwjourney.api.PurchaseHandler.ShopItem;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.api.VJXOffer.VJXOfferType;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.ui.ShopMenu;
import de.culturalgames.kwjourney.ui.popups.InfoPopup;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTresor.Lockable;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class ShopButton extends Button implements Lockable {

    private KJMenu menu;

    private EJXLabel activeLabel;
    private VJXOffer offer;
    private PriceTag priceTag;
    private Image offerIcon;
    private UpgradeBar upgradeBar;
    private LockIcon lockIcon;

    private ClickListener buyListener;
    private ClickListener totemListener;

    public ShopButton(final MenuStage mc, final VJXOffer offer) {
        super(mc.getSkin().get(EJXSkin.style_transparent_256, ButtonStyle.class));
        setName("btn_" + offer.getIdentifier());
        getStyle().pressedOffsetY = -10;
        pack();
        menu = (KJMenu) mc;
        this.offer = offer;
        String path = "offers/";
        if (offer.isPremium()) path += "prime/";
        else if (offer.getIdentifier().contains("skin")) path += "skins/";
        else path += "solar/";
        path += offer.getIdentifier();

        offer.setDrawableName(path);
        offerIcon = new Image(mc.getSkin().getDrawable(path));
        offerIcon.pack();
        offerIcon.setPosition(getWidth() / 2 - offerIcon.getWidth() / 2,
                        getHeight() / 2 - offerIcon.getHeight() / 2);
        addActor(offerIcon);

        priceTag = new PriceTag(mc, offer);
        priceTag.setScale(.7f);
        priceTag.setPosition(getWidth() / 2 - priceTag.getWidth() / 2 + 15, 0);
        addActor(priceTag);

        final TextControl texts = TextControl.getInstance();
        activeLabel = new EJXLabel(mc.getSkin(),
                        texts.get(KJTexts.TEXT_ACTIVE),
                        EJXFont.TITLE_32);
        activeLabel.setPosition(getWidth() / 2 - activeLabel.getWidth() / 2,
                        activeLabel.getHeight());
        addActor(activeLabel);

        if (offer.hasLevels()) {
            upgradeBar = new UpgradeBar(mc.getSkin(), offer);
            upgradeBar.setPosition(
                            getWidth() / 2f - upgradeBar.getWidth() / 2f - 24,
                            192 - upgradeBar.getHeight() / 2f);
            addActor(upgradeBar);
        }

        if (offer.isReward()) {
            final EJXGame game = mc.getGame();
            ClickListener lockedListener = new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    InfoPopup infoD = (InfoPopup) game.getScreen().getMenu()
                                    .getPopup(POPUP.INFO.id);
                    infoD.setTitle(offer.getTitle());
                    infoD.setText(offer.getUnlockText());
                    infoD.resetButtonText();
                    infoD.setOnConfirm(null);
                    game.getScreen().getMenu().showPopup(POPUP.INFO.id);
                }
            };

            String saveName = GameStats.SKIN_LOCKED.name + offer.getSkin();
            lockIcon = new LockIcon(mc.getSkin(), saveName, null, lockedListener);
            lockIcon.setScale(.75f);
            lockIcon.setRotation(10);
            lockIcon.setPosition(getWidth() / 2f - lockIcon.getWidth() / 2f,
                            getHeight() / 2f - lockIcon.getHeight() / 2f);

            if (menu.isLocked(saveName)) {
                lockIcon.setLocked(true);
                addActor(lockIcon);
                addListener(lockedListener);
            }
            activeLabel.setVisible(false);
        }

        if (offer.getIdentifier().equals(ShopItem.TOTEM.name)) {
            totemListener = new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    EJXGame game = menu.getGame();
                    InfoPopup infoD = (InfoPopup) game.getScreen().getMenu()
                                    .getPopup(POPUP.INFO.id);
                    infoD.setTitle(offer.getTitle());
                    infoD.setText(offer.getUnlockText());
                    infoD.resetButtonText();
                    infoD.setOnConfirm(null);
                    game.getScreen().getMenu().showPopup(POPUP.INFO.id);
                }
            };
        }

        buyListener = new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y,
                            int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                KJGame g = (KJGame) menu.getGame();
                g.sfx.playDynSFX(KJAssets.SFX_CLICK);
                KJPlayer player = (KJPlayer) g.getPlayer();
                if (offer.isActivatable()) {
                    if (offer.getAmount() == 0) {
                        if (offer.isActive()) {
                            // deactivate current skin and set default
                            if (!offer.getSkin().isEmpty()) {
                                player.setSkin("kawaida0");
                                offer.setActive(false);
                                g.getSave().savePlayer(g.getPlayer(), false);
                            }
                        } else {
                            if (!offer.getSkin().isEmpty()) {
                                String skin = player.getSkin();
                                // deactivate current skin
                                if (!skin.isEmpty() && !skin
                                                .equals(KJPlayer.dflSkin)) {
                                    ShopMenu sMenu = (ShopMenu) menu
                                                    .getMenu(MENU.SHOP_SKINS.id);
                                    HashMap<String, VJXOffer> offers = g
                                                    .getPurchaseHandler()
                                                    .getOffers();
                                    VJXOffer o = offers.get(
                                                    PurchaseHandler.OFFER_SKIN
                                                    + skin);
                                    o.setActive(false);
                                    ShopButton b = sMenu.getOfferButtons()
                                                    .get(o.getIdentifier());
                                    b.update(player, g.getPurchaseManager());
                                }
                                // set new skin
                                player.setSkin(offer.getSkin());
                                offer.setActive(true);
                                g.getSave().savePlayer(player, true);
                            }
                        }
                        update(player, g.getPurchaseManager());
                    } else menu.showBuyDialogue(ShopButton.this);
                } else menu.showBuyDialogue(ShopButton.this);
            }
        };

        if (getLockIcon() != null) {
            getLockIcon().setUnlockedListener(buyListener);
            if (!getLockIcon().isLocked()) addListener(buyListener);
        }
        else addListener(buyListener);
    }

    @Override
    public void setColor(Color color) {
        super.setColor(color);
        offerIcon.setColor(color);
    }

    public void update(KJPlayer player, PurchaseManager purchasManager) {
        TextControl texts = TextControl.getInstance();
        activeLabel.setText(texts.get(KJTexts.TEXT_ACTIVE));
        if (offer.isPremium()) {
            activeLabel.setVisible(false);
            String offerName = offer.getIdentifier();
            if (offer.getOfferType() == VJXOfferType.UPGRADE
                            && offer.getMaxAmount() > 1) {
                upgradeBar.update();

                if (offer.isMaxLevel()) {
                    setTouchable(Touchable.disabled);
                    setColor(Color.GRAY);
                    priceTag.setVisible(false);
                    return;
                }
                offerName = offer.getLevelIdentifier();
            }

            Information offerInfo = purchasManager.getInformation(offerName);
            if (offerInfo == null) {
                VJXLogger.log("Update " + offerName + " - Info null.");
                setTouchable(Touchable.disabled);
                setColor(Color.GRAY);
                priceTag.setVisible(false);
                return;
            } else if (offerInfo.equals(Information.UNAVAILABLE)) {
                if (offer.getAmount() != 0) {
                    VJXLogger.log(offerName + " - Info unavailable: "
                                    + offerInfo.getLocalName() + " " + " lPrice "
                                    + offerInfo.getLocalPricing() + " currency "
                                    + offerInfo.getPriceCurrencyCode() + " cPrice "
                                    + offerInfo.getPriceInCents());
                    VJXLogger.log(offerName + " - Description: "
                                    + offerInfo.getLocalDescription());
                } else activeLabel.setVisible(
                                offer.getOfferType() == VJXOfferType.ITEM);
                setTouchable(Touchable.disabled);
                setColor(Color.GRAY);
                priceTag.setVisible(false);
            } else {
                setTouchable(Touchable.enabled);
                setColor(Color.WHITE);
                priceTag.setVisible(true);
                priceTag.setPrice(offerInfo.getLocalPricing(), false);
            }
            return;
        }

        if (offer.isReward()) {
            if (!menu.isLocked(lockIcon.getSaveString())) {
                if (lockIcon.isLocked()) {
                    removeListener(lockIcon.getLockedListener());
                    addListener(lockIcon.getUnlockedListener());
                    lockIcon.setLocked(false);
                    lockIcon.remove();
                }
                if (offer.isActivatable()) {
                    setTouchable(Touchable.enabled);
                    setColor(Color.WHITE);
                }
            } else {
                if (!lockIcon.isLocked()) {
                    menu.lock(this, lockIcon);
                }
            }
            checkPrice(player.getCoins());
            priceTag.setVisible(false);
            activeLabel.setVisible(offer.isActivatable() && offer.isActive());
            return;
        }

        if (offer.getAmount() == 0) {
            if (!offer.isActivatable()) {
                setTouchable(Touchable.disabled);
            } else {
                setTouchable(Touchable.enabled);
                setColor(Color.WHITE);
            }
            priceTag.setVisible(false);
        } else {
            checkPrice(player.getCoins());

            if (offer.getIdentifier().equals(ShopItem.TOTEM.name)) {
                if (player.getAmount(ItemCategory.CONTINUE) == player
                                .getAmount(ItemCategory.CONTINUE, true)) {
                    setColor(Color.GRAY);
                    priceTag.setPrice(texts.get(KJTexts.TEXT_FULL), false);
                    priceTag.setAvailable(false);
                    removeListener(buyListener);
                    addListener(totemListener);
                } else {
                    priceTag.setPrice(offer.getIntPrice(), true);
                    removeListener(totemListener);
                    addListener(buyListener);
                }
            } else {
                priceTag.setPrice(offer.getIntPrice(), true);
            }
            priceTag.setVisible(true);
        }

        activeLabel.setVisible(offer.isActivatable() && offer.isActive());

        if (offer.getOfferType() == VJXOfferType.UPGRADE
                        && offer.getMaxAmount() > 1)
            upgradeBar.update();
    }

    public VJXOffer getOffer() {
        return offer;
    }

    public Drawable getOfferDrawable() {
        return offerIcon.getDrawable();
    }

    public PriceTag getPriceTag() {
        return priceTag;
    }

    public LockIcon getLockIcon() {
        return lockIcon;
    }

    private void checkPrice(int playerCoins) {
        if (offer.getIntPrice() <= playerCoins) {
            setTouchable(Touchable.enabled);
            setColor(Color.WHITE);
            priceTag.setAvailable(true);
        } else {
            setTouchable(Touchable.disabled);
            setColor(Color.GRAY);
            priceTag.setAvailable(false);
        }
    }
}