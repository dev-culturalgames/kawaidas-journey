package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.util.KJTexts;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class EncyButton extends KJIconTextButton {

    public static final String titleSW = "titleSW";
    public static final String missingTitleSW = "<missing sw title>";

    private EJXLabel score;
    private EJXLabel newCount;
    private EJXLabel titleLabel;

    public EncyButton(Skin skin, MapProperties props, ClickListener listener) {
        super(KJAssets.BUTTON_WHITE_THIN, VJXString.STR_EMPTY, skin,
                        skin.getDrawable(KJString.PATH_icons_ency
                                        + props.get("image", String.class)));

        if (listener != null)
            addListener(listener);

        mainTable.clear();
        mainTable.add(icon);
        mainTable.row();

        String name = props.get(VJXString.name, VJXString.STR_EMPTY, String.class);
        name = KJTexts.MENU_ENCY_ + name;
        setName(name);

        String swTitleTxt = props.get(titleSW, missingTitleSW, String.class);
        EJXLabel swTitle = new EJXLabel(skin, swTitleTxt.toUpperCase(), EJXFont.TEXT_36_GREEN);

        TextControl texts = TextControl.getInstance();
        String titleTxt = texts.get(name + TextControl._TITLE);
        titleLabel = new EJXLabel(skin, titleTxt.toUpperCase(), EJXFont.TEXT_36);

        score = new EJXLabel(skin, VJXString.STR_EMPTY, EJXFont.TEXT_36);

        mainTable.add(swTitle).expandX().padTop(15);
        mainTable.row();
        mainTable.add(titleLabel).expandX();
        mainTable.row();
        mainTable.add(score).expandX();
        mainTable.row();
        mainTable.add().fill().expand();

        newCount = new EJXLabel(skin, "+0", EJXFont.TITLE_64);
        mainTable.addActor(newCount);
    }

    public void setScore(int foundAmount, int totalAmount) {
        score.setText(foundAmount + VJXString.SEP_SLASH + totalAmount);
    }

    public void setNewLabel(int count) {
        newCount.setVisible(count != 0);
        newCount.setText(VJXString.STR_PLUS + Integer.toString(count));
        newCount.setPosition(getWidth() - newCount.getWidth(),
                        getHeight() - newCount.getHeight());
    }

    public void setTitle(String title) {
        titleLabel.setText(title.toUpperCase());
    }
}