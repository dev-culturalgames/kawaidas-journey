package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.ui.UIWidget;
import de.venjinx.ejx.controllers.AudioControl;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class LanguageSelect extends UIWidget {

    private Table languageBtnsTable;
    private KJIconTextButton languageBtn;

    private boolean languagesVisible = false;

    public LanguageSelect(final KJMenu mc) {
        super(mc, "ui_language_select");
        final AudioControl audioControl = mc.getGame().sfx;
        ClickListener languageBtnListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);

                mc.getGame().sfx.playDynSFX(KJAssets.SFX_CLICK);
                showLanguages(!languagesVisible);
            }
        };
        languageBtn = new KJIconTextButton(
                        KJAssets.BUTTON_FLAG_ENG, VJXString.STR_EMPTY,
                        getSkin());
        languageBtn.addListener(languageBtnListener);
        add(languageBtn);

        languageBtnsTable = new Table();

        ClickListener setEnglishListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                TextControl.getInstance().setLocale(TextControl.LOCALE_EN_US);
                mc.updateLanguage();
                mc.getGame().saveSettings();
                languageBtn.setBGDrawable(KJAssets.BUTTON_FLAG_ENG);
                showLanguages(false);
            }
        };
        KJIconTextButton selectEnglish = new KJIconTextButton(
                        KJAssets.BUTTON_FLAG_ENG, VJXString.STR_EMPTY,
                        getSkin());
        selectEnglish.addListener(setEnglishListener);

        ClickListener setGermanListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                TextControl.getInstance().setLocale(TextControl.LOCALE_DE_DE);
                mc.updateLanguage();
                mc.getGame().saveSettings();
                languageBtn.setBGDrawable(KJAssets.BUTTON_FLAG_GER);
                showLanguages(false);
            }
        };
        KJIconTextButton selectGerman = new KJIconTextButton(
                        KJAssets.BUTTON_FLAG_GER, VJXString.STR_EMPTY,
                        getSkin());
        selectGerman.addListener(setGermanListener);

        ClickListener setSwahiliListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                TextControl.getInstance().setLocale(TextControl.LOCALE_SW_TZ);
                mc.updateLanguage();
                mc.getGame().saveSettings();
                languageBtn.setBGDrawable(KJAssets.BUTTON_FLAG_TZ);
                showLanguages(false);
            }
        };
        KJIconTextButton selectSwahili = new KJIconTextButton(
                        KJAssets.BUTTON_FLAG_TZ, VJXString.STR_EMPTY,
                        getSkin());
        selectSwahili.addListener(setSwahiliListener);

        languageBtnsTable.add(selectEnglish);
        languageBtnsTable.row();
        languageBtnsTable.add(selectGerman);
        languageBtnsTable.row();
        languageBtnsTable.add(selectSwahili);
        languageBtnsTable.setPosition(languageBtn.getWidth() / 2f, 200);
    }

    public void setLanguageFlag(String locale) {
        switch (locale) {
            case TextControl.LOCALE_SW_TZ:
                languageBtn.setBGDrawable(KJAssets.BUTTON_FLAG_TZ);
                break;
            case TextControl.LOCALE_EN_US:
                languageBtn.setBGDrawable(KJAssets.BUTTON_FLAG_ENG);
                break;
            case TextControl.LOCALE_DE_DE:
                languageBtn.setBGDrawable(KJAssets.BUTTON_FLAG_GER);
                break;
            default:
                break;
        }
    }

    public void showLanguages(boolean show) {
        languagesVisible = show;
        if (show) {
            addActor(languageBtnsTable);
        } else languageBtnsTable.remove();
    }
}