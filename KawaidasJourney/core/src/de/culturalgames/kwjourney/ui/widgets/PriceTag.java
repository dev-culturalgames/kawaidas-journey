package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.ui.UIWidget;
import de.venjinx.ejx.stages.MenuStage;

public class PriceTag extends UIWidget {

    private PriceTagText priceTag;
    private Drawable availableDrawable;
    private Drawable notAvailableDrawable;

    public PriceTag(MenuStage mc, VJXOffer offer) {
        super(mc, "priceTag");
        if (offer.isPremium())
            availableDrawable = getSkin().getDrawable(KJAssets.SHOP_PRICE_PRIME);
        else availableDrawable = getSkin().getDrawable(KJAssets.SHOP_PRICE_AVAILABLE);
        notAvailableDrawable = getSkin().getDrawable(KJAssets.SHOP_PRICE_SOLD);

        setBackground(availableDrawable);
        pack();
        setOrigin(getWidth() / 2, getHeight() / 2);
        setTransform(true);
        setRotation(350);

        priceTag = new PriceTagText(mc, offer);
        priceTag.setPosition(getWidth() / 2f - priceTag.getWidth() / 2 + 25,
                        getHeight() / 2 - priceTag.getHeight() / 2);
        addActor(priceTag);
    }

    public void setPrice(int price, boolean showIcon) {
        priceTag.setPrice(price, showIcon);
        priceTag.setPosition(getWidth() / 2f - priceTag.getWidth() / 2 + 25,
                        getHeight() / 2 - priceTag.getHeight() / 2);
    }

    public void setPrice(String price, boolean showIcon) {
        priceTag.setPrice(price, showIcon);
        priceTag.setPosition(getWidth() / 2f - priceTag.getWidth() / 2 + 25,
                        getHeight() / 2 - priceTag.getHeight() / 2);
    }

    public void setAvailable(boolean available) {
        if (available) setBackground(availableDrawable);
        else setBackground(notAvailableDrawable);
    }

    @Override
    public void setColor(Color color) {
        super.setColor(color);
        priceTag.setColor(color);
    }
}