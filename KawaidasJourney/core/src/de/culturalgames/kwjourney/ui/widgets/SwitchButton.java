package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.culturalgames.kwjourney.KJAssets;

public class SwitchButton extends KJIconTextButton {

    private boolean on;
    private Table stateOverlayTable;
    private Image offOverlayImage;

    public SwitchButton(Skin skin, String text, Drawable icon) {
        super(KJAssets.BUTTON_GREEN_SMALL, text, skin, icon);

        stateOverlayTable = new Table();
        offOverlayImage = new Image(skin.getDrawable(KJAssets.ICON_OFF_RED));
        stateOverlayTable.add(offOverlayImage).expand();
        stateOverlayTable.setSize(getWidth(), getHeight());

        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                switchState();
            }
        });
    }

    public void setState(boolean on) {
        this.on = on;
        updateActor();
    }

    public void switchState() {
        on = !on;
        updateActor();
    }

    private void updateActor() {
        if (on) removeActor(stateOverlayTable);
        else addActor(stateOverlayTable);
    }

    public boolean enabled() {
        return on;
    }
}