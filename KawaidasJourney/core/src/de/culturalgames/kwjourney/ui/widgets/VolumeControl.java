package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJLevel;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.ui.UIWidget;
import de.venjinx.ejx.controllers.AudioControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class VolumeControl extends UIWidget {

    private AudioControl audioControl;
    private SwitchButton musicSwitch;
    private SwitchButton sfxSwitch;

    public VolumeControl(final MenuStage mc) {
        super(mc, "VolumeController");
        audioControl = mc.getGame().sfx;
        ClickListener musicListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                if (audioControl.isMusicOn()) {
                    audioControl.setMusicVolume(0f);
                    mc.stopMusic();
                } else {
                    audioControl.setMusicVolume(1f);
                    KJLevel level = mc.getGame().getScreen().getLevel(KJLevel.class);
                    if (mc.getCurrentMenuId() == MENU.GAME.id)
                        level.playMusic();
                    else mc.playMusic(KJAssets.MUSIC_MAIN);
                }
                mc.getGame().saveSettings();
            }
        };

        musicSwitch = new SwitchButton(getSkin(), VJXString.STR_EMPTY,
                        getSkin().getDrawable(KJAssets.ICON_MUSIC));
        musicSwitch.setState(audioControl.isMusicOn());
        musicSwitch.addListener(musicListener);

        ClickListener soundListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (audioControl.isSoundOn()) {
                    audioControl.playDynSFX(KJAssets.SFX_CLICK);
                    audioControl.setSoundVolume(0f);
                    audioControl.stopAllSounds();
                } else {
                    audioControl.setSoundVolume(1f);
                    audioControl.playDynSFX(KJAssets.SFX_CLICK);
                }
                mc.getGame().saveSettings();
            }
        };

        sfxSwitch = new SwitchButton(getSkin(), VJXString.STR_EMPTY,
                        getSkin().getDrawable(KJAssets.ICON_FX_ON));
        sfxSwitch.setState(audioControl.isSoundOn());
        sfxSwitch.addListener(soundListener);

        Table t1 = new Table();
        t1.add(musicSwitch);
        t1.add(sfxSwitch);
        add(t1).fill().expand().top();
    }

    public void updateButtons() {
        sfxSwitch.setState(audioControl.isSoundOn());
        musicSwitch.setState(audioControl.isMusicOn());
    }
}