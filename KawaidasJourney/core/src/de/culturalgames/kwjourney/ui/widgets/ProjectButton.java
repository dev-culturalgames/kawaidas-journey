package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.culturalgames.kwjourney.KJAssets;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class ProjectButton extends Button {

    private EJXLabel titleLabel;

    public ProjectButton(Skin skin, String title, Drawable drawable,
                    ClickListener listener) {
        ButtonStyle style = new ButtonStyle();
        style.pressedOffsetY = -10;
        setStyle(style);

        if (listener != null) addListener(listener);

        Stack stack = new Stack();

        Table bgTable = new Table();
        Image bgImage = new Image(skin.getDrawable(KJAssets.BUTTON_WHITE_THIN));
        bgTable.add(bgImage);
        stack.add(bgTable);

        Table fgTable = new Table();
        if (drawable != null) {
            Table imgTable = new Table();
            Image img = new Image(drawable);
            imgTable.add(img);
            fgTable.add(imgTable).expandX().top().height(256).padTop(25);
        }
        fgTable.row();

        titleLabel = new EJXLabel(skin, title, EJXFont.TEXT_36);
        fgTable.add(titleLabel).expandX().padTop(15);
        fgTable.row();
        fgTable.add().fill().expand();

        stack.add(fgTable);
        add(stack);
    }

    public void setTitle(String title) {
        titleLabel.setText(title);
    }
}