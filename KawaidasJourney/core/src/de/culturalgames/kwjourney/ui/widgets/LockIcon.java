package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import de.culturalgames.kwjourney.KJAssets;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class LockIcon extends Stack {

    private static final String mainTable = "LockIcon_mainTable";
    private static final String lockImage = "LockIcon_image";
    private static final String lockStar = "LockIcon_star";

    private Skin skin;
    private Image lockIcon;
    private Image starIcon;
    private EJXLabel starsCount, starsNeeded, separatorLabel;

    private ClickListener lockedListener;
    private ClickListener unlockedListener;

    private boolean locked = false;

    private final Runnable updateIcon = new Runnable() {

        @Override
        public void run() {
            lockIcon.setDrawable(skin.getDrawable(KJAssets.ICON_LOCK_OPEN));
        }
    };

    public LockIcon(Skin skin, String name, ClickListener unlockedListener,
                    ClickListener lockedListener) {
        this.skin = skin;
        setName(name);
        setTransform(true);
        setOrigin(Align.center);

        this.unlockedListener = unlockedListener;
        this.lockedListener = lockedListener;

        Table layer0 = new Table();
        layer0.setName(mainTable);
        add(layer0);

        lockIcon = new Image(skin.getDrawable(KJAssets.ICON_LOCK));
        lockIcon.setName(lockImage);
        layer0.add(lockIcon);

        starIcon = new Image(skin.getDrawable(KJAssets.ICON_STAR));
        lockIcon.setName(lockStar);
        starIcon.setOrigin(Align.center);
        starIcon.setScale(1.25f);
        starIcon.pack();
        layer0.addActor(starIcon);

        Table t = new Table();

        starsCount = new EJXLabel(skin, VJXString.STR_EMPTY, EJXFont.TITLE_32);
        separatorLabel = new EJXLabel(skin, VJXString.SEP_SLASH, EJXFont.TITLE_26);
        starsNeeded = new EJXLabel(skin, VJXString.STR_EMPTY, EJXFont.TITLE_26);

        t.add(starsCount).expandX().right();
        t.add(separatorLabel).bottom().padBottom(1);
        t.add(starsNeeded).expandX().bottom().padBottom(1).left();

        layer0.addActor(t);
        pack();
        starIcon.setPosition(getWidth() / 2f - starIcon.getWidth() / 2f,
                        getHeight() / 2f - starIcon.getHeight() / 2f - 8);
        t.setPosition(getWidth() / 2f - t.getWidth() / 2f, 40);

        showStarsNeeded(false);
    }

    public void showStarsNeeded(boolean show) {
        starsCount.setVisible(show);
        separatorLabel.setVisible(show);
        starsNeeded.setVisible(show);
    }

    public void setStarsFound(String found) {
        starsCount.setText(found);
    }

    public void setStarsNeeded(String total) {
        starsNeeded.setText(total);
    }

    public void setLocked(boolean locked) {
        setLocked(locked, 0f);
    }

    public void setLocked(boolean locked, float delay) {
        this.locked = locked;
        if (locked) {
            clearActions();
            setColor(1, 1, 1, 1);
            setScale(1);
            lockIcon.setDrawable(skin.getDrawable(KJAssets.ICON_LOCK));
        } else {
            SequenceAction sa = Actions.sequence();
            sa.addAction(Actions.delay(delay + .5f));
            sa.addAction(Actions.run(updateIcon));
            sa.addAction(Actions.delay(.5f));
            sa.addAction(Actions.scaleTo(2f, 2f, .5f));
            sa.addAction(Actions.alpha(0, .5f));
            sa.addAction(Actions.delay(.5f));
            sa.addAction(Actions.removeActor());
            addAction(sa);
        }
    }

    public String getSaveString() {
        return getName();
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLockedListener(ClickListener lockedListener) {
        this.lockedListener = lockedListener;
    }

    public ClickListener getLockedListener() {
        return lockedListener;
    }

    public void setUnlockedListener(ClickListener unlockedListener) {
        this.unlockedListener = unlockedListener;
    }

    public ClickListener getUnlockedListener() {
        return unlockedListener;
    }
}