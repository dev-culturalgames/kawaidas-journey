package de.culturalgames.kwjourney.ui.widgets;

import java.util.List;
import java.util.Set;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame.GameStats;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.content.Secrets;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.culturalgames.kwjourney.ui.menus.EncySubMenu;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.TextButton;

public class EncySubButtons extends Table {

    private KJMenu menu;
    private final EncySubMenu subMenu;
    private EncySubButton[] buttons;
    private TextButton[] pageButtons;
    List<String> secretNames;
    private ClickListener[] listeners;

    private int currentPage = 0;
    private int pageCount = 0;
    private int currentSelection = 0;

    public EncySubButtons(EncySubMenu subMenu, final EJXGame game) {
        super();
        this.subMenu = subMenu;
        menu = game.getScreen().getMenu(KJMenu.class);
        buttons = new EncySubButton[9];

        Set<String> sCats = Secrets.getSecretCategoriesNames();
        int maxSecrets = 0;
        for (String s : sCats) {
            maxSecrets = Math.max(maxSecrets, Secrets.getSecretCount(s));
        }
        listeners = new ClickListener[9];

        for (int i = 0; i < 9; i++) {
            final int index = i;
            listeners[i] = new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x,
                                float y) {
                    super.clicked(event, x, y);
                    game.sfx.playDynSFX(KJAssets.SFX_CLICK);
                    select(index, true);
                }
            };
        }

        Table buttonTable = new Table();
        for (int i = 0; i < 9; i++) {
            buttons[i] = new EncySubButton(menu.getSkin());
            buttonTable.add(buttons[i]).size(buttons[i].getWidth()).pad(5, 10, 5, 10);
            if (i % 3 == 2)
                buttonTable.row();
        }

        add(buttonTable);
        row();

        Table pages = new Table();
        pageCount = maxSecrets / 10 + 1;
        pageButtons = new TextButton[pageCount];
        for (int i = 0; i < pageCount; i++) {
            final int index = i;
            pageButtons[i] = EJXSkin.createTButton(KJAssets.BUTTON_PAGES, menu.getSkin());
            pageButtons[i].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    game.sfx.playDynSFX(KJAssets.SFX_CLICK);
                    setPage(index);
                }
            });
            pages.add(pageButtons[i]).pad(0, 8, 0, 8).top();
        }
        add(pages).padTop(15);
        pack();
    }

    public void setCategory(String encyCategory) {
        setCategory(encyCategory, 0);
    }

    public void setCategory(String encyCategory, int pageNr) {
        if (encyCategory == null) return;

        secretNames = Secrets.getSecretList(encyCategory);

        int maxCount = secretNames.size() / 9 + 1;
        for (int i = 0; i < pageCount; i++) {
            pageButtons[i].setVisible(i < maxCount);
        }

        setPage(pageNr);
    }

    public void setPage(int pageNr) {
        if (currentPage != pageNr) {
            pageButtons[currentPage].setBGDrawable(KJAssets.BUTTON_PAGES);
            clearSelection();
        }

        currentPage = pageNr;
        pageButtons[currentPage].setBGDrawable(KJAssets.BUTTON_PAGES_ACTIVE);
        LocalSavegame savegame = menu.getGame().getSave();
        int offset = pageNr * 9;
        int end = offset + 9;
        end = secretNames.size() < end ? secretNames.size() : end;
        boolean selected = false;
        for (int i = 0; i < 9; i++) {
            buttons[i].setVisible(true);

            // check if a secret exists for this button
            if (i + offset < end) {
                for (int j = 0; j < secretNames.size(); j++) {
                    Secret secret = Secrets.getSecret(secretNames.get(j));
                    if (secret.getPosition() - offset == i) {
                        buttons[i].setSecret(secret, savegame);
                        buttons[i].setListener(listeners[i]);

                        if (!selected && savegame.getBool(secret.getKsString(), false)) {
                            select(secret.getPosition(), false);
                            selected = true;
                        }

                        // unlock if develop mode is active
                        if (menu.getGame().isDevMode()) {
                            buttons[i].unlock();
                        }

                        break;
                    }
                }
            } else {
                buttons[i].setVisible(false);
            }
        }
    }

    public void select(int index, boolean checkNew) {
        buttons[currentSelection].setBGDrawable(KJAssets.BUTTON_WHITE_SMALL);
        if (index > -1) {
            int buttonIndex = index - currentPage * 9;
            EncySubButton b = buttons[buttonIndex];
            Secret secret = b.getSecret();
            currentSelection = buttonIndex;
            buttons[currentSelection].setBGDrawable(KJAssets.BUTTON_WHITE_SMALL_ACTIVE);
            if (checkNew) {
                LocalSavegame savegame = menu.getGame().getSave();
                String saveString = secret.getKsString() + "_new";
                boolean isNew = savegame.getBool(saveString, false);
                if (isNew) {
                    savegame.putBool(saveString, false);

                    saveString = GameStats.NEW_SECRETS.name;
                    int newSecrets = savegame.getInt(saveString, 1);
                    savegame.putInt(saveString, newSecrets - 1);

                    saveString = Secrets.PRE_secrets + secret.getCategory() + VJXString.POST_new;
                    int newCatSecrets = savegame.getInt(saveString, 0);
                    savegame.putInt(saveString, newCatSecrets - 1);

                    String reward = secret.getReward();
                    if (reward != null)
                        menu.showReward(reward, secret.getRewardAmount());
                    else savegame.write();

                    buttons[currentSelection].setIsNew(false);
                }
            }
            subMenu.showEncy(secret);
        } else subMenu.showEncy(null);
    }

    public void clearSelection() {
        select(-1, false);
    }
}