package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

import de.culturalgames.kwjourney.KJAssets;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class KJTextArea extends Table {

    private Image bgImage;
    private EJXLabel titleSW;
    private EJXLabel titleEng;
    private EJXLabel text;
    private Table layer0;
    private Table layer1;
    private ScrollPane scrollArea;
    private Table scrollTable;

    private Image stripe;
    private boolean addStripe;

    public KJTextArea(Skin skin) {
        this(skin, 600, 512, true, true);
    }

    public KJTextArea(Skin skin, float width, float height, boolean addBG,
                    boolean addStripe) {
        setSize(width, height);
        Stack stack = new Stack();

        if (addBG) {
            bgImage = new Image(skin.getDrawable(KJAssets.ICON_WHITE_MEDIUM));
            bgImage.setSize(width, height);
        }

        stripe = new Image(skin.getDrawable(KJAssets.ICON_STRIPE));
        this.addStripe = addStripe;

        layer0 = new Table();
        stack.add(layer0);

        layer1 = new Table();
        stack.add(layer1);

        titleSW = new EJXLabel(skin, VJXString.STR_EMPTY, EJXFont.TEXT_48_GREEN, Align.top);
        titleEng = new EJXLabel(skin, VJXString.STR_EMPTY, EJXFont.TEXT_48, Align.top);

        stripe.setWidth(getWidth() - 90);

        text = new EJXLabel(skin, VJXString.STR_EMPTY, EJXFont.TEXT_36,
                        Align.topLeft);
        text.setWrap(true);

        scrollTable = new Table();
        scrollArea = new ScrollPane(scrollTable);
        scrollArea.setOverscroll(false, false);

        update(true);

        add(stack);
    }

    public void setTitle(String titleSW, String titleEng) {
        this.titleSW.setText(titleSW);
        this.titleEng.setText(titleEng);
    }

    public void setTitleAlign(int align) {
        titleSW.setAlignment(align);
        titleEng.setAlignment(align);
    }

    private void update(boolean addTitle) {
        update(55, 35, 55, addTitle);
    }

    public void update(float padX, float padTop, float padBot, boolean addTitle) {
        scrollTable.clear();
        scrollTable.add(text).width(getWidth() - padX * 2)
        .pad(0, padX, 0, padX).expandX().top();

        layer0.clear();
        if (bgImage != null)
            layer0.add(bgImage).width(getWidth()).height(getHeight());

        layer1.clear();
        layer1.setSize(getWidth(), getHeight());

        float height = getHeight() - titleEng.getHeight() - titleSW.getHeight()
                        - padTop - padBot;
        if (addTitle) {
            layer1.add(titleSW).expandX().fillX().pad(padTop, padX, 0, padX);
            layer1.row();
            layer1.add(titleEng).expandX().fillX().pad(0, padX, 0, padX);
            layer1.row();
            layer1.add(scrollArea).size(getWidth(), height).expand().top().pad(0, 0, padBot, 0);

            stripe.setWidth(getWidth() - padX * 2 + 20);
            stripe.setPosition(padX - 10, height + padBot);
        } else {
            layer1.add(scrollArea).size(getWidth(), height).expand().top()
            .pad(padTop, 0, padBot, 0);
        }
    }

    public void setText(String text) {
        setText(text, VJXString.STR_EMPTY);
    }

    public void setText(String text0, String text1) {
        text.setText(VJXString.SEP_NEW_LINE_N + text0 + text1);
        if (addStripe && !text0.isEmpty()) layer1.addActor(stripe);
        else stripe.remove();
        pack();
        scrollArea.setScrollY(25);
    }
}