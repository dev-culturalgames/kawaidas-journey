package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.venjinx.ejx.util.VJXGraphix;
import de.venjinx.ejx.util.VJXLogger;

public class ScratchWidget extends Stack {

    private Actor hiddenActor;
    private Pixmap mask;

    private Pixmap scratchPixmap;
    private Texture scratchTexture;
    private TextureRegion scratchTexRegion;
    private Image scratchArea;

    private int totalFill = 0;
    private int currenAlpha = 0;

    public ScratchWidget(Actor a) {
        hiddenActor = a;
        setPosition(a.getX(), a.getY());

        scratchPixmap = new Pixmap((int) hiddenActor.getWidth(),
                        (int) hiddenActor.getHeight(), Format.RGBA8888);

        scratchTexture = new Texture(scratchPixmap);
        scratchTexRegion = new TextureRegion(scratchTexture);

        scratchArea = new Image(new TextureRegionDrawable(scratchTexRegion));
        totalFill = (int) scratchArea.getWidth()
                        * (int) scratchArea.getHeight();
        resetScratch();

        add(scratchArea);
        pack();

        scratchArea.addListener(new DragListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                            int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                scratchStamp((int) x, (int) y);
                return true;
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y,
                            int pointer) {
                super.touchDragged(event, x, y, pointer);
                scratchStamp((int) x, (int) y);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer,
                            int button) {
                super.touchUp(event, x, y, pointer, button);
                Color tmpColor0 = new Color();
                int count = 0;
                for (int mx = 0; mx < scratchPixmap.getWidth(); mx++) {
                    for (int my = 0; my < scratchPixmap.getHeight(); my++) {
                        //                        tmpColor1.set(alphaMask.getPixel(mx, my));
                        //                        maskAlpha = tmpColor1.a;
                        //                if (maskAlpha <= 0) continue;
                        //                if (maskAlpha >= threshold) maskAlpha = 1;

                        tmpColor0.set(scratchPixmap.getPixel(mx, my));
                        if (tmpColor0.a == 0) count++;
                        //                VJXLogger.log("source alpha " + tmpColor0.a);
                        //                        srcAlpha = tmpColor0.a;
                        //                if (srcAlpha <= 0) continue;

                        //                        maskAlpha = maskAlpha > srcAlpha ? srcAlpha : maskAlpha;
                        //                        newAlpha = srcAlpha - maskAlpha;

                        //                removedAlpha += maskAlpha;
                        //                        tmpColor0.a = newAlpha;
                        //                        if (srcAlpha != 0 && newAlpha <= 0) count++;
                        //                        source.setColor(tmpColor0);
                        //                        source.drawPixel(x + mx, y + my);
                    }
                }
                VJXLogger.log("really cleared " + count / (float) totalFill);
            }
        });
    }

    public void setActor(Actor a) {
        hiddenActor = a;
        updateScratchSize();
    }

    public void updateScratchSize() {
        //        VJXLogger.log("update scratch size " + hiddenActor.getWidth() + ", "
        //                        + hiddenActor.getHeight());
        setPosition(hiddenActor.getX(), hiddenActor.getY());
        scratchPixmap.dispose();
        scratchPixmap = new Pixmap((int) hiddenActor.getWidth(),
                        (int) hiddenActor.getHeight(), Format.RGBA8888);
        scratchArea.setSize(scratchPixmap.getWidth(),
                        scratchPixmap.getHeight());
        totalFill = (int) scratchArea.getWidth()
                        * (int) scratchArea.getHeight();
        VJXLogger.log("total " + totalFill);
        resetScratch();
    }

    public void resetScratch() {
        //        VJXLogger.log("reset scratch");
        scratchPixmap.setBlending(Blending.None);
        scratchPixmap.setColor(Color.BLACK);
        scratchPixmap.fill();
        updateScratchImage();
        currenAlpha = 0;
    }

    public void clearScratch() {
        VJXLogger.log("clear scratch");
        scratchPixmap.setBlending(Blending.None);
        scratchPixmap.setColor(Color.CLEAR);
        scratchPixmap.fill();
        updateScratchImage();
    }

    public void scratchCircle(int x, int y, int radius) {
        scratchPixmap.setBlending(Blending.None);
        scratchPixmap.setColor(0);
        scratchPixmap.fillCircle(x, (int) hiddenActor.getHeight() - y, radius);

        updateScratchImage();
    }

    public void setScratchStamp(Pixmap stamp) {
        //        VJXLogger.log("set stamp");
        mask = stamp;
    }

    public void scratchStamp(int x, int y) {
        currenAlpha += VJXGraphix.alphaStamp(x, y, scratchPixmap, mask,
                        0);

        float tmp = currenAlpha / (float) totalFill;


        VJXLogger.log("area cleared " + currenAlpha + " / " + totalFill);
        VJXLogger.log("Scratch " + tmp + " cleared");
        VJXLogger.log("");
        updateScratchImage();
    }

    private void updateScratchImage() {
        //        VJXLogger.log("update image");
        scratchTexture.dispose();
        scratchTexture = new Texture(scratchPixmap);
        scratchTexRegion.setRegion(scratchTexture);
    }

}
