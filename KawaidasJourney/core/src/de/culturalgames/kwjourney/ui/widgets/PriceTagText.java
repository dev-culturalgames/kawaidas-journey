package de.culturalgames.kwjourney.ui.widgets;

import java.text.DecimalFormat;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.ui.UIWidget;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class PriceTagText extends UIWidget {

    private EJXLabel text;
    private Image priceImg;

    public PriceTagText(MenuStage mc) {
        this(mc, null);
    }

    public PriceTagText(MenuStage mc, VJXOffer offer) {
        super(mc, "PriceTagText");
        text = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.TITLE_32);
        if (offer != null) {
            priceImg = new Image(getSkin().getDrawable(KJAssets.ICON_SUN));
            priceImg.setVisible(true);
            if (offer.getIntPrice() <= 0)
                setPrice(TextControl.getInstance().get(KJTexts.TEXT_FREE), false);
            else
                setPrice(offer.getIntPrice(), false);
        }
    }

    public void setPrice(int price, boolean showIcon) {
        DecimalFormat myFormatter = new DecimalFormat("#,###");
        String stringPrice = myFormatter.format(price);
        setPrice(stringPrice, showIcon);
    }

    public void setPrice(String price, boolean showIcon) {
        clear();
        text.setText(price);
        add(text).padRight(6);
        if (priceImg != null && showIcon) {
            add(priceImg).size(priceImg.getWidth(), priceImg.getHeight());
        }
    }

    @Override
    public void setColor(Color color) {
        super.setColor(color);
        text.setColor(color);
        priceImg.setColor(color);
    }
}
