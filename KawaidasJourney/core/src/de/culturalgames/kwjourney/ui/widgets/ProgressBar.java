package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.culturalgames.kwjourney.KJAssets;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.VJXIcon;

public class ProgressBar extends Table {

    private static final String X = "x";

    private Image background;
    private Image fill;
    private Image overlay;
    private VJXIcon icon;
    private EJXLabel multiplierLabel;

    private int value;
    private int maxValue;

    public ProgressBar(MenuStage mc, Drawable background, Drawable fill) {
        this(mc, background, fill, null);
    }

    public ProgressBar(MenuStage mc, Drawable background, Drawable fill,
                    Drawable overlay) {
        this.background = new Image(background);
        this.fill = new Image(fill);
        Table t = new Table();
        t.add(this.fill);

        if (overlay == null) this.overlay = new Image();
        this.overlay = new Image(overlay);

        Stack stack = new Stack();
        stack.add(this.background);
        stack.add(t);
        stack.add(this.overlay);

        add(stack);
        pack();

        icon = new VJXIcon(VJXString.STR_EMPTY,
                        mc.getSkin().getDrawable(KJAssets.ICON_SUN),
                        mc.getSkin());
        multiplierLabel = new EJXLabel(mc.getSkin(), VJXString.STR_EMPTY,
                        EJXFont.TITLE_26);
        addActor(icon);
        addActor(multiplierLabel);

        icon.setPosition(-icon.getWidth() / 2f,
                        getHeight() / 2f - icon.getHeight() / 2f);
        multiplierLabel.setPosition(icon.getWidth() + 5,
                        getHeight() / 2f - multiplierLabel.getHeight() / 2f);
    }

    public void setEmpty() {
        setEmpty(0f);
    }

    public void setEmpty(float duration) {
        if (duration == 0f || value == 0f) {
            fill.clearActions();
            fill.setScaleX(0f);
        } else fill.addAction(Actions.scaleTo(0f, 1f, duration));
    }

    public void setValue(float percent) {
        value = (int) (maxValue * percent);
        icon.setText(Integer.toString(value));
        icon.centerText();
        fill.setScaleX(0);
        if (percent != 0) {
            fill.setScaleX(percent);
        }
    }

    public void setValue(int value) {
        this.value = value;
        icon.setText(Integer.toString(value));
        icon.centerText();
        fill.setScaleX(0);
        if (value != 0) {
            fill.setScaleX(value / (float) maxValue);
        }
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
        fill.setScaleX(0);
        if (value != 0) {
            fill.setScaleX(value / (float) maxValue);
        }
    }

    public void setMultiplier(float multiplier) {
        multiplierLabel.setText(X + Float.toString(multiplier));
    }

    public void showMultiplier(boolean show) {
        if (show) addActor(multiplierLabel);
        else multiplierLabel.remove();
    }

    public void showIcon(boolean show) {
        if (show) addActor(icon);
        else icon.remove();
    }

    public VJXIcon getIcon() {
        return icon;
    }

    public void setFillColor(Color fillColor) {
        fill.setColor(fillColor);
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x + icon.getWidth() / 2f, y);
    }
}