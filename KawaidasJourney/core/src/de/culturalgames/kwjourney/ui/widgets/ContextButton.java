package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJPlayerControl;
import de.culturalgames.kwjourney.KawaidaHUD;
import de.culturalgames.kwjourney.entity.logics.ContextLogic;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.venjinx.ejx.EJXPlayer;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.ActionContext;
import de.venjinx.ejx.util.EJXTypes.ClickType;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class ContextButton extends KJIconTextButton {

    private ActionContext currentContex = ActionContext.NONE;
    private EJXEntity contextEntity;

    public ContextButton(Skin skin, final MenuStage mc) {
        super(KJAssets.BUTTON_PLANT_BG, VJXString.STR_EMPTY, skin);
        final EJXPlayer player = mc.getGame().getPlayer();
        final KJPlayerControl input = (KJPlayerControl) mc.getGame().gameControl
                        .getPlayerInput();

        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ContextLogic contextLogic = (ContextLogic) player.getEntity()
                                .getLogic(KJLogicType.CONTEXT);
                input.execContextAction(contextLogic.getContext());
                mc.getUI(KawaidaHUD.class).updateTools();
            }
        });

        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                            int pointer, int button) {
                ContextLogic contextLogic = (ContextLogic) player.getEntity()
                                .getLogic(KJLogicType.CONTEXT);
                input.execContextAction(contextLogic.getContext(), ClickType.DOWN);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer,
                            int button) {
                ContextLogic contextLogic = (ContextLogic) player.getEntity()
                                .getLogic(KJLogicType.CONTEXT);
                input.execContextAction(contextLogic.getContext(), ClickType.UP);
            }
        });
        title.setStyle(skin.get(EJXFont.TITLE_48, LabelStyle.class));
        title.setPosition(getWidth() / 2 - title.getWidth() / 2, 0);
        setTransform(true);
    }

    public void setContext(ActionContext context, EJXEntity contextEntity) {
        currentContex = context;
        this.contextEntity = contextEntity;
        switch (context) {
            case PLANT:
                updateContextImage(KJAssets.ICON_CONTEXT_PLANT, true);
                setBGDrawable(KJAssets.BUTTON_PLANT_BG);
                break;
            case EXTINGUISH:
                updateContextImage(KJAssets.ICON_CONTEXT_EXTINGUISH, false);
                setBGDrawable(KJAssets.BUTTON_EXTINGUISH_BG);
                break;
            case TELEPORT:
                updateContextImage(KJAssets.ICON_CONTEXT_TELEPORT, false);
                setBGDrawable(KJAssets.BUTTON_TELEPORT_BG);
                break;
            case TRASH:
                updateContextImage(KJAssets.ICON_CONTEXT_TRASH, false);
                setBGDrawable(KJAssets.BUTTON_TRASH_BG);
                break;
            case REPAIR:
                if (contextEntity != null)
                    updateContextImage(contextEntity.getFrame());
                else updateContextImage(KJAssets.ICON_CONTEXT_REPAIR, false);
                setBGDrawable(KJAssets.BUTTON_REPAIR_BG);
                break;
            case BROKEN:
                if (contextEntity != null)
                    updateContextImage(contextEntity.getFrame());
                else updateContextImage(KJAssets.ICON_CONTEXT_BROKEN, false);
                setBGDrawable(KJAssets.BUTTON_REPAIR_BG);
                break;
            default:
                break;
        }
    }

    private void updateContextImage(AtlasRegion frame) {
        setIcon(new TextureRegionDrawable(frame));
        title.setVisible(false);
        icon.pack();
        icon.setPosition(getWidth() / 2f - icon.getWidth() / 2f,
                        getHeight() / 2f - icon.getHeight() / 2f);
    }

    private void updateContextImage(String drawableName, boolean showCount) {
        setIcon(getSkin().getDrawable(drawableName));
        title.setVisible(showCount);
        icon.pack();
        icon.setPosition(getWidth() / 2f - icon.getWidth() / 2f,
                        getHeight() / 2f - icon.getHeight() / 2f);
    }

    public ActionContext getContext() {
        return currentContex;
    }

    public EJXEntity getContextEntity() {
        return contextEntity;
    }
}