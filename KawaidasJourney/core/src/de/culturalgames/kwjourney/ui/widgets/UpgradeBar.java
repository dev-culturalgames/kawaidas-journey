package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class UpgradeBar extends Table {

    private VJXOffer offer;
    private Image startImg;

    private Stack endStack;
    private Image endImg;
    private Image completedImg;

    private Image[] midImages;

    private Drawable startEmptyDrawable;
    private Drawable startFilledDrawable;
    private Drawable midEmptyDrawable;
    private Drawable midFilledDrawable;

    private EJXLabel upgradeLabel;

    public UpgradeBar(Skin skin, VJXOffer offer) {
        super(skin);
        setName("ui_upgradeBar");
        this.offer = offer;

        startEmptyDrawable = skin.getDrawable(KJAssets.OFFER_UPGRADE_START_EMPTY);
        startFilledDrawable = skin.getDrawable(KJAssets.OFFER_UPGRADE_START_FILLED);
        startImg = new Image(startEmptyDrawable);

        midEmptyDrawable = skin.getDrawable(KJAssets.OFFER_UPGRADE_MID_EMPTY);
        midFilledDrawable = skin.getDrawable(KJAssets.OFFER_UPGRADE_MID_FILLED);
        midImages = new Image[offer.getMaxAmount() - 1];

        add(startImg);
        for (int i = 0; i < midImages.length; i++) {
            midImages[i] = new Image(midEmptyDrawable);
            add(midImages[i]);
        }

        endStack = new Stack();
        endImg = new Image(skin.getDrawable(KJAssets.OFFER_UPGRADE_END));
        endStack.add(endImg);
        endStack.pack();

        upgradeLabel = new EJXLabel(skin, offer.getLabelText(), EJXFont.TITLE_26);
        endStack.add(upgradeLabel);
        addActor(endStack);

        completedImg = new Image(skin.getDrawable(KJAssets.OFFER_UPGRADE_COMPLETED));

        update();

        endStack.setPosition(getWidth() - 13f,
                        getHeight() / 2f - endImg.getHeight() / 2f);
    }

    public void update() {
        int lvl = offer.getLevel();
        if (lvl >= 1) {
            startImg.setDrawable(startFilledDrawable);
            for (int i = 1; i < lvl; i++) {
                midImages[i - 1].setDrawable(midFilledDrawable);
            }
            if (lvl == offer.getMaxAmount()) {
                upgradeLabel.remove();
                endStack.add(completedImg);
            }
        } else {
            startImg.setDrawable(startEmptyDrawable);
            for (int i = 0; i < midImages.length; i++) {
                midImages[i].setDrawable(midEmptyDrawable);
            }
            completedImg.remove();
            endStack.add(upgradeLabel);
        }

        pack();
    }
}