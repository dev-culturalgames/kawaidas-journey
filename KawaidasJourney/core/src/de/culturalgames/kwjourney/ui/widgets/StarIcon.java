package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;

import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.animation.SpineActor.SpineActorListener;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class StarIcon extends Table {

    private Skin skin;
    private SpineActor star;
    private Image icon;
    private EJXLabel titleLabel;

    public StarIcon(MenuStage mc, String title) {
        skin = mc.getSkin();
        star = new SpineActor(VJXString.STR_EMPTY);
        star.usePremAlpha(false);
        titleLabel = new EJXLabel(skin, title, EJXFont.TITLE_32);
        icon = new Image();

        Table t = new Table();
        t.add(icon);
        t.add(titleLabel);

        add(star);
        row();
        add(t);

        SkeletonData skeletonData = mc.getGame().assets.loadSkeletonData(
                        "starfruit0", "data/ui/decoration/", "global_ui");

        Skeleton skel = new Skeleton(skeletonData);
        AnimationStateData stateData = new AnimationStateData(skeletonData);
        AnimationState state = new AnimationState(stateData);
        star.setAnimationState(state);
        star.setSkeleton(skel);
        star.setSize(256, 256);
        star.updateSkeletonScale(256, 256);
        star.setAnimation("empty", true);
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        star.setName(name);
    }

    public void setTitle(String title) {
        titleLabel.setText(title);
    }

    public void setIcon(String iconName) {
        setIcon(skin.getDrawable(iconName));
    }

    public void setIcon(Drawable iconDrawable) {
        icon.setDrawable(iconDrawable);
    }

    public boolean isEmpty() {
        return star.isAtAnim("empty");
    }

    public void setAnimListener(SpineActorListener listener) {
        star.setAnimListener(listener);
    }

    public void setAnim(String animName, boolean loop) {
        star.setAnimation(animName, loop);
    }
}