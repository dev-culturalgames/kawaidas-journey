package de.culturalgames.kwjourney.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.culturalgames.kwjourney.KJAssets;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class ProgressIcon extends Stack {
    private Image backgroundImage;
    private EJXLabel text0, text1, separatorLabel;

    public ProgressIcon(Skin skin) {
        super();
        Table layer0 = new Table();
        add(layer0);

        backgroundImage = new Image(skin.getDrawable(KJAssets.ICON_SECRETS_PROGRESS));
        layer0.add(backgroundImage);

        Table layer1 = new Table();
        add(layer1);

        Table t = new Table();
        t.setTransform(true);
        t.setSize(128, 128);
        t.setOrigin(t.getWidth() / 2f, t.getHeight() / 2f);
        t.setRotation(-8f);

        text0 = new EJXLabel(skin, VJXString.STR_EMPTY, EJXFont.TITLE_64);
        separatorLabel = new EJXLabel(skin, VJXString.SEP_SLASH, EJXFont.TITLE_48);
        text1 = new EJXLabel(skin, VJXString.STR_EMPTY, EJXFont.TITLE_48);

        t.add(text0).expandX().right();
        t.add(separatorLabel).bottom().padBottom(4);
        t.add(text1).expandX().bottom().padBottom(4).left();

        layer1.addActor(t);
        pack();
        t.setPosition(getWidth() / 2f - t.getWidth() / 2f,
                        getHeight() / 2f - t.getHeight() / 2f + 5);
    }

    public void setText0(String text) {
        text0.setText(text);
    }

    public void setSeparator(String separator) {
        separatorLabel.setText(separator);
    }

    public void setText1(String text) {
        text1.setText(text);
    }
}