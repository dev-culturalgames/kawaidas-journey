package de.culturalgames.kwjourney.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KawaidaHUD;
import de.culturalgames.kwjourney.KawaidaHUD.UIElement;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXMenuTable;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.VJXIcon;

public abstract class IngamePopup extends EJXMenuTable {

    protected KawaidaHUD hud;
    protected Table bananas;
    protected Cell<Table> bananaCell;
    protected VJXIcon sunsIcon;
    protected Cell<VJXIcon> sunCell;
    protected VJXIcon totemIcon;
    protected Cell<VJXIcon> totemCell;
    protected Table top;
    protected Table content;
    private EJXLabel titleLabel;

    public IngamePopup(MenuStage mc, String name) {
        this(mc, name, VJXString.STR_EMPTY);
    }

    public IngamePopup(MenuStage mc, String name, String title) {
        this(mc, name, title, null);
    }

    public IngamePopup(MenuStage mc, String name, String title,
                    ClickListener backListener) {
        super(mc, name);
        hud = mc.getUI(KawaidaHUD.class);
        content = new Table();

        createTop(title);
        initContent();

        add(content).fill().expand();

        setTransform(true);
        setOrigin(mc.getWidth() / 2f, mc.getHeight() / 2f);

        menuControl.getUI(KawaidaHUD.class).addBananas();
        menuControl.getUI(KawaidaHUD.class).addSuns();
    }

    private void createTop(String title) {
        top = new Table();
        Table tLeft = new Table();
        Table tRight = new Table();

        bananas = (Table) hud.getUIElement(UIElement.BANANA);
        bananaCell = tLeft.add(bananas).expandX().left();

        totemIcon = (VJXIcon) hud.getUIElement(UIElement.TOTEM);
        totemCell = tRight.add(totemIcon).expandX().right().padRight(10);

        sunsIcon = (VJXIcon) hud.getUIElement(UIElement.SUN);
        sunCell = tRight.add(sunsIcon).padLeft(5).padRight(5);

        titleLabel = new EJXLabel(getSkin(), title);

        top.add(tLeft).top().width(320);
        top.add(titleLabel).expandX();
        top.add(tRight).top().width(320);

        content.add(top).expandX().fillX().pad(20, 25, 0, 25);
        content.row();
    }

    public abstract void initContent();

    public void showBananas(boolean show) {
        bananas.setVisible(show);
        bananaCell.setActor(bananas);
    }

    public void showSuns(boolean show) {
        sunsIcon.clearActions();
        sunsIcon.setColor(1, 1, 1, show ? 1 : 0);
        sunCell.setActor(sunsIcon);
    }

    public void showTotems(boolean show) {
        totemIcon.setVisible(show);
        totemCell.setActor(totemIcon);
    }

    public void removeTop() {
        top.remove();
    }

    public void setTitle(String title) {
        titleLabel.setText(title);
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        hud.toggleHUD(false);
        g.getScreen().getLevel().setActive(false);
        g.gameControl.getPlayerInput().setIngameControl(false);
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
        g.gameControl.getPlayerInput().setIngameControl(true);
        g.getScreen().getLevel().setActive(true);
        hud.toggleHUD(true);
    }

    @Override
    public Table getContent() {
        return content;
    }

    @Override
    public void lvlLoaded() {
    }

    @Override
    public void updateLanguage(TextControl texts) {
    }

    @Override
    public void updateLayout(Skin skin) {
    }
}