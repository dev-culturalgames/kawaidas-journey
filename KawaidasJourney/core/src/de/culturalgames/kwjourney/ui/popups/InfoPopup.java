package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.ui.MenuPopup;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.TextButton;

public class InfoPopup extends MenuPopup {

    private Table table;
    private EJXLabel titleLabel;
    private EJXLabel textLabel;
    private TextButton okButton;
    private Runnable onConfirm;

    private SpineActor monkeyActor;

    public InfoPopup(MenuStage mc) {
        super(mc, "ui_popup_info");
    }

    @Override
    public void initContent() {
        ClickListener okListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                menuControl.hidePopup();
                if (onConfirm != null) onConfirm.run();
            }
        };

        table = new Table();
        table.setBackground(getSkin().getDrawable(KJAssets.ICON_WHITE_MEDIUM));

        titleLabel = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.TITLE_64);
        textLabel = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.TEXT_36);
        textLabel.setWrap(true);

        table.add(titleLabel).top().padTop(50);
        table.row();
        table.add(textLabel).top().expand();
        table.row();

        okButton = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        TextControl.getInstance().get(KJTexts.BTN_OK),
                        getSkin(), okListener);
        table.add(okButton).expand();

        content.add(table);
    }

    public void initMonkey(EJXGame game) {
        if (monkeyActor == null) {
            FileHandle file = Gdx.files
                            .internal("objects/character/monkey0/monkey0.json");
            monkeyActor = game.factory.createSpineActor("monkey", 128, 128,
                            "global_ui", file, null);
            monkeyActor.usePremAlpha(false);
            monkeyActor.setAnimation("repair", true);
            monkeyActor.setSkin("kawaida0");
            monkeyActor.updateSkeletonScale(128, 128);
            monkeyActor.setPosition(0, 20);
        }

        table.addActor(monkeyActor);
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        if (monkeyActor != null) monkeyActor.remove();
    }

    public void setTitle(String title) {
        titleLabel.setText(title);
    }

    public void setText(String text) {
        textLabel.setText(text);
    }

    public void setButtonText(String buttonText) {
        okButton.setText(buttonText);
    }

    public void resetButtonText() {
        setButtonText(TextControl.getInstance().get(KJTexts.BTN_OK));
    }

    public void setOnConfirm(Runnable onConfirm) {
        this.onConfirm = onConfirm;
    }

    @Override
    public void enterCallback() {
        audioControl.playDynSFX(KJAssets.SFX_CLICK);
        menuControl.hidePopup();
    }

    @Override
    public void updateLanguage(TextControl texts) {
        okButton.setText(texts.get(KJTexts.BTN_OK));
    }
}