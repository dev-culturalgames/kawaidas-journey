package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KJLevel;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.api.APILoader;
import de.culturalgames.kwjourney.api.AdMobController;
import de.culturalgames.kwjourney.api.PurchaseHandler.ShopItem;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.ui.IngamePopup;
import de.culturalgames.kwjourney.ui.widgets.KJIconTextButton;
import de.culturalgames.kwjourney.util.KJTexts;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.TextButton;

public class BuyLosePopup extends IngamePopup {

    private final KJPlayer player;
    private final VJXOffer totemOffer;
    private EJXLabel titleLabel;
    private EJXLabel textLabel;
    private Cell<KJIconTextButton> watchAdCell;
    private KJIconTextButton watchAdBtn;
    private ClickListener watchAdListener;
    private TextButton buyTotemBtn;

    public BuyLosePopup(MenuStage mc) {
        super(mc, "ui_popup_buy_lose");
        player = (KJPlayer) mc.getGame().getPlayer();
        totemOffer = APILoader.getOffer(ShopItem.TOTEM.name);
    }

    @Override
    public void initContent() {
        TextControl textControl = TextControl.getInstance();
        Button backBtn = EJXSkin.createTButton(KJAssets.BUTTON_BACK, getSkin());
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuControl.showPopup(POPUP.LOSE.id, false);
            }
        });
        backBtn.setPosition(0, menuControl.getHeight() - backBtn.getHeight());
        content.addActor(backBtn);

        titleLabel = new EJXLabel(getSkin(),
                        textControl.get(KJTexts.CONFIRM_NO_TOTEMS_TITLE),
                        EJXFont.TITLE_64);
        textLabel = new EJXLabel(getSkin(),
                        textControl.get(KJTexts.CONFIRM_NO_TOTEMS_TEXT),
                        EJXFont.TEXT_36);
        textLabel.setWrap(true);

        Table subTable = new Table();

        watchAdBtn = ((KJMenu) menuControl).getWatchAdButton();
        watchAdCell = subTable.add(watchAdBtn).expand();

        ClickListener buyTotemListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);

                int price = totemOffer.getIntPrice();
                player.incrCoins(-price);

                KJLevel level = (KJLevel) menuControl.getScreen().getLevel();
                level.continueAfterDeath(false);
            }
        };
        buyTotemBtn = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        textControl.get(KJTexts.BTN_BUY), getSkin(),
                        buyTotemListener);
        subTable.add(buyTotemBtn).expand();

        Table mid = new Table();
        mid.setBackground(getSkin().getDrawable(KJAssets.ICON_WHITE_MEDIUM));
        mid.add(titleLabel).top().padTop(50);
        mid.row();
        mid.add(textLabel).top().expand();
        mid.row();
        mid.add(subTable).bottom().padBottom(30);

        watchAdListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                VJXLogger.log("show continue");
                menuControl.getGame().sfx.playDynSFX(KJAssets.SFX_CLICK);
                ((KJGame) menuControl.getGame()).getAdControl().showAd(KJString.AD_continue);
                watchAdBtn.setEnabled(false);
            }
        };

        content.add(mid).expand();
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        super.entered(g, mc);

        watchAdBtn.setVisible(true);
        watchAdBtn.setEnabled(false);

        AdMobController adControl = ((KJGame) g).getAdControl();
        if (adControl != null) {
            watchAdBtn.setEnabled(adControl.isLoaded(KJString.AD_continue));
        }

        watchAdBtn.setFont(EJXFont.TITLE_32);
        watchAdBtn.setText(TextControl.getInstance().get(KJTexts.BTN_WATCH_AD));
        watchAdBtn.centerText();

        watchAdBtn.setIcon(null);
        watchAdBtn.setIconText(VJXString.STR_EMPTY);
        watchAdBtn.clearListeners();
        watchAdBtn.addListener(watchAdListener);

        watchAdCell.setActor(watchAdBtn);

        buyTotemBtn.setEnabled(player.getCoins() >= totemOffer.getIntPrice());
    }

    @Override
    public void enterCallback() {
    }

    @Override
    public void updateLanguage(TextControl texts) {
        titleLabel.setText(texts.get(KJTexts.CONFIRM_NO_TOTEMS_TITLE));
        textLabel.setText(texts.get(KJTexts.CONFIRM_NO_TOTEMS_TEXT));
        buyTotemBtn.setText(texts.get(KJTexts.BTN_BUY));
    }
}