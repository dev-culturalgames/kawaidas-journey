package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.entity.logics.InventoryLogic;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.ui.IngamePopup;
import de.culturalgames.kwjourney.ui.widgets.KJIconTextButton;
import de.culturalgames.kwjourney.ui.widgets.VolumeControl;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.DebugStage;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.TextButton;

public class PausePopup extends IngamePopup {

    private TextButton mapBut;
    private TextButton restBut;
    private TextButton resumeBtn;

    private VolumeControl volControl;

    public PausePopup(KJMenu menu) {
        super(menu, "ui_popup_pause", TextControl.getInstance()
                        .get(KJTexts.POPUP_PAUSE_TITLE));
    }

    @Override
    public void initContent() {
        TextControl txt = TextControl.getInstance();
        mapBut = EJXSkin.createTButton(KJAssets.BUTTON_RED,
                        txt.get(KJTexts.BTN_MAP),
                        getSkin(), menuControl.backToMapListener);

        restBut = EJXSkin.createTButton(KJAssets.BUTTON_YELLOW,
                        txt.get(KJTexts.BTN_RESTART),
                        getSkin(), new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.stopAllSounds();
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                menuControl.getScreen().restartLevel();
            }
        });

        resumeBtn = EJXSkin.createTButton(KJAssets.BUTTON_CONTINUE,
                        txt.get(KJTexts.BTN_CONTINUE),
                        getSkin(), new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                menuControl.hidePopup(false);
                hud.toggleHUD(true);
                KJPlayer p = (KJPlayer) menuControl.getGame().getPlayer();
                InventoryLogic inventory = (InventoryLogic) p.getEntity()
                                .getLogic(KJLogicType.INVENTORY);
                sunsIcon.setText(Integer.toString(inventory.getAmount(ItemCategory.COIN)));
            }
        });

        volControl = new VolumeControl(menuControl);

        final DebugStage debug = menuControl.getScreen().getDebugStage();
        ClickListener debugListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                debug.toggleDebugView();
            }
        };
        KJIconTextButton debugSwitch =
                new KJIconTextButton(KJAssets.BUTTON_YELLOW_SMALL, VJXString.STR_EMPTY, getSkin(),
                                     getSkin().getDrawable(KJAssets.ICON_DEBUG));
        debugSwitch.addListener(debugListener);

        Table bottom = new Table();
        bottom.add(mapBut);
        bottom.add(restBut);
        bottom.add(volControl);

        content.add(resumeBtn).pad(50, 25, 75, 25);
        content.row();
        if (menuControl.getGame().isDevMode()) {
            content.add(bottom).expandX().top().pad(0, 25, 0, 25);
            content.row();
            content.add(debugSwitch).expand().top();
        } else {
            content.add(bottom).expand().top().pad(0, 25, 20, 25);
        }
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        super.entered(g, mc);
        showBananas(true);

        KJPlayer p = (KJPlayer) g.getPlayer();
        sunsIcon.setText(Integer.toString(p.getCoins()));
        showSuns(true);

        totemIcon.setText(Integer.toString(p.getAmount(ItemCategory.CONTINUE)));
        showTotems(true);

        volControl.updateButtons();
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
        hud.restoreHUD();
        g.gameControl.getPlayerInput().setIngameControl(true);
        g.getScreen().getLevel().setActive(true);
    }

    @Override
    public void enterCallback() {
        audioControl.playDynSFX(KJAssets.SFX_CLICK);
        menuControl.hidePopup();
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.POPUP_PAUSE_TITLE));
        mapBut.setText(texts.get(KJTexts.BTN_MAP));
        restBut.setText(texts.get(KJTexts.BTN_RESTART));
        resumeBtn.setText(texts.get(KJTexts.BTN_CONTINUE));
    }
}