package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.ui.MenuPopup;
import de.culturalgames.kwjourney.ui.widgets.KJTextArea;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.TextButton;

public class HelpPopup extends MenuPopup {

    protected KJTextArea textArea;
    protected TextButton learnBtn;
    protected String curUrl;

    public HelpPopup(KJMenu mc) {
        super(mc, "ui_popup_help");
    }

    @Override
    public void initContent() {
        textArea = new KJTextArea(getSkin(), 1024, 640, true, true);
        textArea.update(55, 35, 110, true);
        Table table = new Table();
        table.add(textArea);
        content.add(table).expand().fill();

        learnBtn = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        TextControl.getInstance().get(KJTexts.BTN_LEARN_MORE),
                        getSkin(), new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(curUrl);
            }
        });

        learnBtn.setPosition(textArea.getWidth() / 2 - learnBtn.getWidth() / 2,
                        -learnBtn.getHeight() / 2 + 35);
        learnBtn.setFont(EJXFont.TITLE_36);
        learnBtn.centerText();
        textArea.addActor(learnBtn);
    }

    @Override
    public void enterCallback() {
        audioControl.playDynSFX(KJAssets.SFX_CLICK);
        menuControl.hidePopup();
    }

    public void setHelp(String name, String url) {
        TextControl texts = TextControl.getInstance();
        textArea.setTitle(VJXString.STR_EMPTY, texts.get(name + TextControl._TITLE));
        textArea.setText(texts.get(name + TextControl._TEXT));
        curUrl = url;
        learnBtn.setVisible(curUrl != null);
    }

    @Override
    public void updateLanguage(TextControl texts) {
        learnBtn.setText(texts.get(KJTexts.BTN_LEARN_MORE));
    }
}