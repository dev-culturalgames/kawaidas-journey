package de.culturalgames.kwjourney.ui.popups;

import java.util.ArrayDeque;
import java.util.Collection;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Skeleton;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.entity.logics.DialogLogic.Dialog;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.entity.logics.StoryLogic;
import de.culturalgames.kwjourney.ui.IngamePopup;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.controllers.GameControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.entity.EntityFactory;
import de.venjinx.ejx.entity.logics.EJXLogics.VJXLogicType;
import de.venjinx.ejx.entity.logics.MoveLogic;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.Orientation;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.TextButton;

public class StoryPopup extends IngamePopup {

    private SpineActor portraitActor;
    private EJXLabel text;
    private Table dialogTable;
    private TextButton nextBtn;

    private AnimationState portraitAnimState;
    private Skeleton portraitSkeleton;

    private EJXEntity object;

    private ArrayDeque<String> dialogParts;
    private Dialog currentDialog;
    private StoryLogic currentLogic;

    private boolean otherOnLeftSide = false;

    public StoryPopup(final KJMenu mc) {
        super(mc, "ui_popup_story");
        dialogParts = new ArrayDeque<>();
    }

    @Override
    public void initContent() {
        removeTop();

        dialogTable = new Table(getSkin());
        dialogTable.setName("ui_txt_dialog");
        dialogTable.setBackground(getSkin().getDrawable(KJAssets.ICON_WHITE_MEDIUM));
        dialogTable.setSize(menuControl.getWidth() - 512, 256);
        dialogTable.setPosition(64, 64);

        text = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.TEXT_36, Align.left);
        text.setWrap(true);

        ClickListener nextListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                next(false);
            }
        };

        nextBtn = EJXSkin.createTButton(KJAssets.BUTTON_NEXT, getSkin());
        nextBtn.addListener(nextListener);
        nextBtn.setPosition(menuControl.getWidth() / 2 - nextBtn.getWidth() / 2, 0);

        dialogTable.add(text).expand().left().top().pad(40, 40, 20, 40)
        .width(menuControl.getWidth() - 512 - 80);

        portraitActor = new SpineActor(KJString.ACTOR_SPEAKER1);
        portraitActor.setSize(640, 640);
        portraitActor.setPosition(0, 0);
        portraitActor.setTouchable(Touchable.disabled);

        content.addActor(dialogTable);
        content.addActor(portraitActor);
        content.addActor(nextBtn);

        content.addListener(nextListener);
        content.setTouchable(Touchable.enabled);
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
        super.exited(g, mc);
        object.onEnd();

        EJXEntity participant;
        StoryLogic logic;
        Collection<String> participants = currentDialog.getParticipants();

        for (String name : participants) {
            participant = menuControl.getGame().gameControl.getEntityByName(name);
            logic = (StoryLogic) participant.getLogic(KJLogicType.STORY);
            logic.stopVoice();
            logic.onDialogEnd();
        }
    }

    @Override
    public void enterCallback() {
        audioControl.stopAllSounds();
        audioControl.playDynSFX(KJAssets.SFX_CLICK);
        next(false);
    }

    public void init() {
        portraitSkeleton = new Skeleton(EntityFactory.getSkeleton("portraits"));
        portraitAnimState = EntityFactory.newAnimState("portraits");
        portraitActor.setAnimationState(portraitAnimState);
        portraitActor.setPosition(0, 0);
        portraitActor.flipX(false);
    }

    public void initStory(Dialog dialog) {
        otherOnLeftSide = false;
        currentDialog = dialog;

        GameControl listener = menuControl.getGame().gameControl;
        Collection<String> participants = dialog.getParticipants();
        EJXEntity participant;
        StoryLogic logic;
        MoveLogic mLogic;

        for (String name : participants) {
            participant = listener.getEntityByName(name);
            logic = (StoryLogic) participant.getLogic(KJLogicType.STORY);
            if (logic != null) {
                logic.onDialogStart();
                mLogic = (MoveLogic) participant.getLogic(VJXLogicType.MOVE);
                if (mLogic != null) {
                    mLogic.stop(true);
                }
            }
        }

        object = dialog.getObject();
        dialogParts.clear();
        dialogParts.addAll(dialog.getParts());

        next(true);
    }

    private void next(boolean init) {
        if (dialogParts.isEmpty()) {
            menuControl.hidePopup(false);

            if (currentLogic != null) {
                audioControl.stopVoiceOver();
            }
            return;
        }

        GameControl listener = menuControl.getGame().gameControl;
        String part[] = dialogParts.removeFirst().split(VJXString.SEP_DDOT);

        text.setText(part[1]);
        text.layout();

        // TODO remove this when localization finished
        String speakerName = currentDialog.getParticipant(part[0]);
        EJXEntity speaker = listener.getEntityByName(speakerName);

        // keep this
        if (speaker == null) speaker = listener.getEntityByName(part[0]);
        if (speaker == null) {
            VJXLogger.log(LogCategory.ERROR, "Dialog speaker not found: "
                            + currentDialog.getName() + " - " + part[0]);
            menuControl.hidePopup(false);
            return;
        }

        StoryLogic logic = (StoryLogic) speaker.getLogic(KJLogicType.STORY);
        logic.playVoice();

        if (currentLogic == logic) return;
        currentLogic = logic;

        String skName = speaker.getDef().getValue(SourceProperty.tileName,
                        VJXString.STR_EMPTY, String.class);
        KJPlayer p = (KJPlayer) menuControl.getGame().getPlayer();
        if (speaker == p.getEntity()) skName = p.getSkin();
        portraitSkeleton.setSkin(skName);
        portraitSkeleton.setToSetupPose();
        portraitActor.setSkeleton(portraitSkeleton);
        portraitActor.setAnimation("talk", true);

        portraitActor.setPosition(0, 0);
        portraitActor.flipX(false);
        setDialogueTable(true);

        EJXEntity playerEntity = menuControl.getGame().getPlayer().getEntity();
        if (speaker != playerEntity)
            otherOnLeftSide = speaker.getWCX() < playerEntity.getWCX();
        else otherOnLeftSide = speaker.getOrientation() == Orientation.RIGHT;

        if (otherOnLeftSide) {
            portraitActor.setPosition(0, 0);
            portraitActor.flipX(false);
            speaker.setOrientation(Orientation.RIGHT);
            setDialogueTable(true);
        } else {
            portraitActor.setPosition(menuControl.getWidth() - portraitActor.getWidth(), 0);
            portraitActor.flipX(true);
            speaker.setOrientation(Orientation.LEFT);
            setDialogueTable(false);
        }

        portraitActor.setScale(1.25f);
        portraitActor.setDebug(getDebug());
        return;
    }

    private void setDialogueTable(boolean right) {
        if (right) dialogTable.setPosition(
                        menuControl.getWidth() - 64 - dialogTable.getWidth(),
                        64);
        else dialogTable.setPosition(64, 64);
    }
}