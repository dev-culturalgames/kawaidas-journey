package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.ui.IngamePopup;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;

public class EmptyPopup extends IngamePopup {

    public EmptyPopup(MenuStage mc) {
        super(mc, "ui_popup_empty", TextControl.getInstance()
                        .get(KJTexts.POPUP_EMPTY_TITLE));
    }

    @Override
    public void initContent() {
        Table table = new Table();
        table.setBackground(getSkin().getDrawable(KJAssets.ICON_WHITE_MEDIUM));
        content.add(table);
    }

    @Override
    public void enterCallback() {
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.POPUP_EMPTY_TITLE));
    }
}