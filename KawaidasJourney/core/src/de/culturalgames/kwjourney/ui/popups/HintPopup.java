package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.KawaidaHUD;
import de.culturalgames.kwjourney.KawaidaHUD.UIElement;
import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.entity.logics.ContextLogic;
import de.culturalgames.kwjourney.entity.logics.HintLogic.Hint;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.ui.IngamePopup;
import de.culturalgames.kwjourney.ui.widgets.ContextButton;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.stages.LevelStage;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class HintPopup extends IngamePopup {

    private Table hintTable;
    private Table hintActorTable;
    private EJXLabel text;
    private Actor showActor;
    private Group oldParent;

    private Vector2 tmpVec = new Vector2();
    private Vector2 oldPosition;
    private Vector2 oldSize;
    private float oldScaleX;
    private float oldScaleY;
    //    private float oldRotation;
    private EJXEntity object;
    private Hint hint;

    private LevelStage level;

    public HintPopup(KJMenu mc) {
        super(mc, "ui_popup_hint");
        oldPosition = new Vector2();
        oldSize = new Vector2();
        level = mc.getScreen().getLevel();
    }

    @Override
    public void initContent() {
        removeTop();

        hintTable = new Table();
        hintTable.setBackground(getSkin().getDrawable(KJAssets.ICON_WHITE_MEDIUM));
        text = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.TEXT_36);
        text.setWrap(true);

        hintActorTable = new Table();

        hintTable.add(text).expand().fill().pad(25).top();
        hintTable.add(hintActorTable).size(256);

        content.add(hintTable).size(menuControl.getWidth() / 2,
                        menuControl.getHeight() / 3);
        addListener(menuControl.uiBackListener);
    }

    public void setHint(Hint hint) {
        float maxSize, scl = 1, w, h;
        this.hint = hint;
        showActor = hint.getTarget();
        if (showActor != null) {
            EJXEntity entity = (EJXEntity) showActor.getUserObject();
            if (entity != null) {
                entity.setDrawOffScreen(true);
                level.removeEntity(entity);
                oldPosition.set(entity.getX(), entity.getY());
                oldScaleX = entity.getScaleX();
                oldScaleY = entity.getScaleY();
                //                oldRotation = entity.getRotation();

                w = entity.getScaledWidth();
                h = entity.getScaledHeight();
                oldSize.set(w, h);
                maxSize = Math.max(w, h);
                if (maxSize > 256)
                    scl = 256 / maxSize;

                //                entity.setScale(scl * oldScaleX, scl * oldScaleY);
                entity.setPosition(128 - w * scl * oldScaleX / 2f,
                                128 - h * scl * oldScaleY / 2f);
                showActor.setSize(256, 256);
                showActor.setScale(scl * oldScaleX, scl * oldScaleY);
                showActor.setPosition(128 - w * scl * oldScaleX / 2f,
                                128 - h * scl * oldScaleY / 2f);
                //                entity.setRotation(0);
            } else {
                oldParent = showActor.getParent();
                oldPosition.set(showActor.getX(), showActor.getY());
                oldScaleX = showActor.getScaleX();
                oldScaleY = showActor.getScaleY();

                w = showActor.getWidth() * oldScaleX;
                h = showActor.getHeight() * oldScaleY;
                maxSize = Math.max(w, h);
                if (maxSize > 256) scl = 256 / maxSize;

                w *= scl * oldScaleX;
                h *= scl * oldScaleY;
                showActor.setScale(scl * oldScaleX, scl * oldScaleY);
                showActor.setPosition(128 - w / 2f, 128 - h / 2f);
            }
            hintActorTable.addActor(showActor);
        }

        object = hint.getObject();
        text.setText(hint.getText());
        text.layout();
    }

    @Override
    public void enterCallback() {
        audioControl.playDynSFX(KJAssets.SFX_CLICK);
        menuControl.hidePopup();
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        super.entered(g, mc);
        ((KJPlayer) g.getPlayer()).foundHint(hint);
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
        super.exited(g, mc);
        if (showActor == null) { return; }

        tmpVec.set(0, 0);
        showActor.localToStageCoordinates(tmpVec);
        EJXEntity entity = (EJXEntity) showActor.getUserObject();
        if (entity != null) {
            entity.setDrawOffScreen(false);
            menuControl.getViewport().project(tmpVec);
            tmpVec.y = menuControl.getViewport().getWorldHeight() - tmpVec.y;
            menuControl.getGame().getScreen().getLevel().getViewport()
            .unproject(tmpVec);
            //            entity.setPosition(tmpVec.x, tmpVec.y);
            //            entity.setScale(oldScaleX, oldScaleY);

            if (entity != menuControl.getGame().getPlayer().getEntity()) {
                SequenceAction sa = Actions.sequence();
                sa.addAction(KJActions.bounce(oldScaleX, oldScaleY, .85f, 1.1f, .3f));
                entity.getActor().addAction(sa);
            }
            //            entity.setScale(oldScaleX, oldScaleY);
            entity.setPosition(oldPosition.x, oldPosition.y);
            //            entity.setRotation(oldRotation);

            showActor.setSize(oldSize.x, oldSize.y);
            showActor.setOrigin(showActor.getWidth() / 2,
                            showActor.getHeight() / 2);
            showActor.remove();

            level.addEntity(entity, entity.getLayer());
            level.addEntity(menuControl.getGame().getPlayer().getEntity());
        } else {
            showActor.setPosition(tmpVec.x, tmpVec.y);
            showActor.setOrigin(showActor.getWidth() / 2,
                            showActor.getHeight() / 2);
            showActor.clearActions();

            if (oldParent != null) {
                SequenceAction sa = Actions.sequence();
                sa.addAction(Actions.moveTo(oldPosition.x, oldPosition.y, .5f));
                sa.addAction(KJActions.bounce(.85f, 1.1f, .3f));
                showActor.addAction(sa);
                oldParent.addActor(showActor);
            } else showActor.remove();

            if (hint.reactivateActor())
                showActor.setTouchable(Touchable.enabled);

            ContextButton cBtn = (ContextButton) menuControl
                            .getUI(KawaidaHUD.class)
                            .getUIElement(UIElement.CONTEXT0);
            if (hint.getTarget() == cBtn)
                ((ContextLogic) menuControl.getGame().getPlayer().getEntity()
                                .getLogic(KJLogicType.CONTEXT)).checkContext(
                                                cBtn.getContext());
        }
        object.onEnd();
    }
}