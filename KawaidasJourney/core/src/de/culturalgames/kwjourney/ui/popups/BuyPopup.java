package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.pay.PurchaseManager;
import com.badlogic.gdx.pay.Transaction;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.api.PurchaseHandler;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.ui.MenuPopup;
import de.culturalgames.kwjourney.ui.widgets.KJTextArea;
import de.culturalgames.kwjourney.ui.widgets.ProgressIcon;
import de.culturalgames.kwjourney.ui.widgets.ShopButton;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.EJXPlayer;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.TextButton;

public class BuyPopup extends MenuPopup {

    public enum BUY_STATE {
        BUY, ACT, DEACT
    }

    private EJXLabel offerType, hintLabel;
    private KJTextArea textArea;
    private ProgressIcon offerLevel;
    private Table cLeft;
    private Table cRight;

    private KJMenu kjMenu;
    private PurchaseHandler pHandler;
    private PurchaseManager pManager;
    private ShopButton currentShopButton;
    private VJXOffer currentOffer;
    private TextButton buyButton;
    private TextButton backButton;
    private BUY_STATE buyState;
    private Group offerParent;
    private float offerX, offerY;
    private int buttonZIndex;
    private Cell<Table> iconCell;

    public BuyPopup(KJMenu mc) {
        super(mc, "ui_popup_buy");
        kjMenu = mc;
        pHandler = ((KJGame) mc.getGame()).getPurchaseHandler();
        pManager = ((KJGame) mc.getGame()).getPurchaseManager();
    }

    @Override
    public void initContent() {
        Table t = new Table();
        cLeft = new Table();

        iconCell = cLeft.add(t).width(256).pad(0, 25, 0, 25);

        cRight = new Table();

        t = new Table();
        offerType = new EJXLabel(getSkin(), VJXString.STR_EMPTY,
                        EJXFont.TEXT_32_BLUE);
        t.add(offerType).expandX().left();
        t.row();

        textArea = new KJTextArea(getSkin(), 718, 360, false, true);
        textArea.setTitleAlign(Align.topLeft);
        textArea.update(0, 0, 0, true);
        t.add(textArea).expandX().left();
        t.row();

        Table btns = new Table();
        ClickListener listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                menuControl.hidePopup();
            }
        };
        backButton = EJXSkin.createTButton(KJAssets.BUTTON_RED,
                        TextControl.getInstance().get(KJTexts.BTN_BACK),
                        getSkin(), listener);
        btns.add(backButton);

        listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                buyClicked();
            }
        };
        buyButton = EJXSkin.createTButton(KJAssets.BUTTON_GREEN,
                        TextControl.getInstance().get(KJTexts.BTN_BUY),
                        getSkin(), listener);
        btns.add(buyButton);

        t.add(btns);
        t.row();

        hintLabel = new EJXLabel(getSkin(),
                        TextControl.getInstance()
                        .get(KJTexts.POPUP_BUY_HINT_LABEL),
                        EJXFont.TEXT_24_GREEN);
        t.add(hintLabel);

        cRight.add(t);

        t = new Table();
        t.setBackground(getSkin().getDrawable(KJAssets.ICON_WHITE_MEDIUM));
        t.add(cLeft);
        t.add(cRight).pad(40, 0, 40, 75);

        content.add(t);

        offerLevel = new ProgressIcon(getSkin());
        offerLevel.setTransform(true);
        offerLevel.setScale(.9f);
        content.addActor(offerLevel);
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        audioControl.playDynSFX(KJAssets.SFX_POPUP_OPEN);
        offerLevel.setPosition(mc.getWidth() - offerLevel.getWidth(),
                        mc.getHeight() - offerLevel.getHeight());
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
        super.exited(g, mc);
        audioControl.playDynSFX(KJAssets.SFX_POPUP_CLOSE);
        currentShopButton.setPosition(offerX, offerY);
        currentShopButton.setTouchable(Touchable.enabled);
        offerParent.addActorAt(buttonZIndex, currentShopButton);
    }

    public void setOffer(ShopButton shopButton, EJXPlayer player, Skin skin) {
        shopButton.setTouchable(Touchable.disabled);
        offerParent = shopButton.getParent();
        buttonZIndex = shopButton.getZIndex();
        currentOffer = shopButton.getOffer();
        offerX = shopButton.getX();
        offerY = shopButton.getY();

        currentShopButton = shopButton;
        iconCell.setActor(shopButton);

        String type = currentOffer.getOfferType().name;
        type = TextControl.getInstance().get(TextControl.OFFER_TYPE_ + type);
        offerType.setText(type);

        TextControl texts = TextControl.getInstance();
        int current = pHandler.getPurchasedAmount(currentOffer);
        int max = currentOffer.getMaxAmount();
        if (max > 0) {
            offerLevel.setText0(Integer.toString(max - current));
            offerLevel.setSeparator(VJXString.SEP_SLASH);
            offerLevel.setText1(Integer.toString(max));
        } else {
            offerLevel.setText0(texts.get(KJTexts.POPUP_BUY_INFINITE_LABEL));
            offerLevel.setSeparator(VJXString.STR_EMPTY);
            offerLevel.setText1(VJXString.STR_EMPTY);
        }

        textArea.setTitle(currentOffer.getTitleSW(), currentOffer.getTitle());
        textArea.setText(currentOffer.getText());

        buyState = BUY_STATE.BUY;
        hintLabel.setVisible(currentOffer.isPremium());
    }

    private Runnable onConfirmBuy = new Runnable() {

        @Override
        public void run() {
            String identifier = currentOffer.getLevelIdentifier();
            if (!currentOffer.isPremium()) {
                Transaction transaction = new Transaction();
                transaction.setIdentifier(identifier);
                pHandler.handlePurchase(transaction);
            } else {
                menuControl.showWaiting(true);
                pManager.purchase(identifier);
            }
        }
    };

    private void buyClicked() {
        switch (buyState) {
            case BUY:
                TextControl texts = TextControl.getInstance();
                kjMenu.showConfirm(texts.get(KJTexts.CONFIRM_BUY_ITEM_TITLE),
                                texts.get(KJTexts.CONFIRM_BUY_ITEM_TEXT),
                                onConfirmBuy, null);
                break;
            default:
        }
    }

    @Override
    public void updateLanguage(TextControl texts) {
        backButton.setText(texts.get(KJTexts.BTN_BACK));
        buyButton.setText(texts.get(KJTexts.BTN_BUY));
        hintLabel.setText(texts.get(KJTexts.POPUP_BUY_HINT_LABEL));
    }
}