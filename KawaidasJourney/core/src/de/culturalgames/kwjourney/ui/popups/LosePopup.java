package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KJLevel;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.api.AdMobController;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.ui.IngamePopup;
import de.culturalgames.kwjourney.util.KJTexts;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.TextButton;

public class LosePopup extends IngamePopup {

    private TextButton mapBtn;
    private TextButton restartBtn;
    private TextButton continueBtn;
    private boolean continueBlock;

    public LosePopup(final KJMenu mc) {
        super(mc, "ui_popup_lose", TextControl.getInstance()
                        .get(KJTexts.POPUP_LOSE_TITLE));
    }

    public void setContinueBlock(boolean block) {
        continueBlock = block;
    }

    @Override
    public void initContent() {
        TextControl txt = TextControl.getInstance();
        mapBtn = EJXSkin.createTButton(KJAssets.BUTTON_RED,
                        txt.get(KJTexts.BTN_MAP),
                        getSkin(), menuControl.backToMapListener);

        restartBtn = EJXSkin.createTButton(KJAssets.BUTTON_YELLOW,
                        txt.get(KJTexts.BTN_RESTART),
                        getSkin(), new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.stopAllSounds();
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                menuControl.getScreen().restartLevel();
            }
        });

        continueBtn = EJXSkin.createTButton(KJAssets.BUTTON_GREEN,
                        txt.get(KJTexts.BTN_CONTINUE),
                        getSkin(), new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);

                KJPlayer player = (KJPlayer) menuControl.getGame().getPlayer();
                if (player.getAmount(ItemCategory.CONTINUE) > 0) {
                    KJLevel level = (KJLevel) menuControl.getScreen().getLevel();
                    level.continueAfterDeath(false);
                } else {
                    menuControl.showPopup(POPUP.BUY_LOSE.id);
                }
            }
        });

        Table mid = new Table();
        Image loseImg = new Image(getSkin().getDrawable(KJAssets.ICON_LOSE));
        mid.add(loseImg);

        Table bottom = new Table();
        bottom.add(mapBtn);
        bottom.add(restartBtn);
        bottom.add(continueBtn);

        content.add(mid).expand();
        content.row();
        content.add(bottom).expand().top().pad(0, 25, 20, 25);
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        super.entered(g, mc);
        audioControl.stopAllSounds();
        audioControl.playDynSFX(KJAssets.SFX_LOSE);
        menuControl.stopMusic();

        KJPlayer p = (KJPlayer) g.getPlayer();
        sunsIcon.setText(Integer.toString(p.getAmount(ItemCategory.COIN)));
        showSuns(true);

        totemIcon.setText(Integer.toString(p.getAmount(ItemCategory.CONTINUE)));
        showTotems(true);

        if (continueBlock) {
            continueBtn.setVisible(true);
            continueBtn.setTouchable(Touchable.enabled);
        } else {
            continueBtn.setVisible(false);
            continueBtn.setTouchable(Touchable.disabled);
        }
    }

    @Override
    public void enterCallback() {
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.POPUP_LOSE_TITLE));
        mapBtn.setText(texts.get(KJTexts.BTN_MAP));
        restartBtn.setText(texts.get(KJTexts.BTN_RESTART));
        continueBtn.setText(texts.get(KJTexts.BTN_CONTINUE));
    }
}