package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.ui.MenuPopup;
import de.culturalgames.kwjourney.ui.widgets.LockIcon;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.TextButton;

public class RewardPopup extends MenuPopup {
    private EJXLabel title;

    private Image rewardImage;
    private EJXLabel rewardText;
    private Table rewardTable;
    private TextButton collectButton;
    private LockIcon lockIcon;

    private Cell<EJXLabel> cell;

    public RewardPopup(final MenuStage mc) {
        super(mc, "ui_popup_reward");
    }

    @Override
    public void initContent() {
        TextControl texts = TextControl.getInstance();
        Table t = new Table();
        t.setBackground(getSkin().getDrawable(KJAssets.ICON_WHITE_MEDIUM));

        title = new EJXLabel(getSkin(), texts.get(KJTexts.POPUP_REWARD_TITLE),
                        EJXFont.TITLE_64);

        rewardTable = new Table();
        rewardImage = new Image();
        rewardText = new EJXLabel(getSkin());
        rewardTable.add(rewardImage);
        rewardTable.row();
        cell = rewardTable.add(rewardText);

        t.add(title).padTop(50);
        t.row();
        t.add(rewardTable).size(192);
        t.row();

        ClickListener collectListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                menuControl.hidePopup();
                EJXGame game = menuControl.getGame();
                game.getSave().savePlayer(game.getPlayer(), true);
            }
        };
        collectButton = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        texts.get(KJTexts.BTN_COLLECT),
                        getSkin(), collectListener);
        t.add(collectButton).expand();

        content.add(t);
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        if (lockIcon != null) {
            lockIcon.setPosition(
                            rewardTable.getWidth() / 2f
                            - lockIcon.getWidth() / 2f,
                            rewardTable.getHeight() / 2f
                            - lockIcon.getHeight() / 2f);
            rewardTable.addActor(lockIcon);
        }
    }

    public void setLockIcon(LockIcon lockIcon) {
        this.lockIcon = lockIcon;
    }

    public void setRewardDrawable(Drawable d) {
        setRewardDrawable(d, 0);
    }

    public void setRewardDrawable(Drawable d, int amount) {
        rewardImage.setDrawable(d);
        if (amount > 0) {
            rewardText.setText(VJXString.STR_PLUS + amount);
            cell.setActor(rewardText);
        } else {
            rewardText.setText(VJXString.STR_EMPTY);
            cell.setActor(null);
        }
    }

    @Override
    public void updateLanguage(TextControl texts) {
        title.setText(texts.get(KJTexts.POPUP_REWARD_TITLE));
        collectButton.setText(texts.get(KJTexts.BTN_COLLECT));
    }
}