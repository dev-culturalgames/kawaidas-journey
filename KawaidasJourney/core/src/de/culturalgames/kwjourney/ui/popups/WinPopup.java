package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.SnapshotArray;
import com.esotericsoftware.spine.Event;
import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KJLevel;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.actions.KJActions;
import de.culturalgames.kwjourney.actions.game.WinProgressAction;
import de.culturalgames.kwjourney.api.AdMobController;
import de.culturalgames.kwjourney.entity.logics.InventoryLogic;
import de.culturalgames.kwjourney.entity.logics.ItemLogic.ItemCategory;
import de.culturalgames.kwjourney.entity.logics.KJLogics.KJLogicType;
import de.culturalgames.kwjourney.save.KJCloudSave;
import de.culturalgames.kwjourney.ui.IngamePopup;
import de.culturalgames.kwjourney.ui.widgets.KJIconTextButton;
import de.culturalgames.kwjourney.ui.widgets.ProgressBar;
import de.culturalgames.kwjourney.ui.widgets.StarIcon;
import de.culturalgames.kwjourney.util.KJTexts;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.animation.SpineActor.SpineActorListener;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.IngameHUD.Transition;
import de.venjinx.vjx.ui.widgets.TextButton;

public class WinPopup extends IngamePopup implements SpineActorListener {

    private static final float[] WATCH_AD_MULTIPLIERS = { 1f, 1.2f, 1.5f, 2f };
    private static final String[] WATCH_AD_TEXTS = new String[4];
    private static final String X = "x";

    private static final String STAR0 = "star0";
    private static final String STAR1 = "star1";
    private static final String STAR2 = "star2";
    private static final String EMPTY = "empty";
    private static final String APPEAR = "appear";
    private static final String IDLE = "idle";

    private StarIcon star0;
    private StarIcon star1;
    private StarIcon star2;

    private ProgressBar pBar;
    private Cell<KJIconTextButton> watchAdCell;
    private KJIconTextButton watchAdBtn;
    private ClickListener watchAdListener;
    private KJIconTextButton collectBtn;
    private TextButton continueBtn;

    private int currentPlayerCoins;
    private int playerMultiplier;
    private int collectedCoins;
    private boolean bonusFound;
    private boolean sunsFound;
    private boolean collectClicked;
    private int rewardCount;
    private float rewardMultiplier;

    public WinPopup(final MenuStage mc) {
        super(mc, "ui_popup_win", TextControl.getInstance()
                        .get(KJTexts.POPUP_WIN_TITLE));
        WATCH_AD_TEXTS[0] = TextControl.getInstance().get(KJTexts.BTN_WATCH_AD_0);
        WATCH_AD_TEXTS[1] = TextControl.getInstance().get(KJTexts.BTN_WATCH_AD_1);
        WATCH_AD_TEXTS[2] = TextControl.getInstance().get(KJTexts.BTN_WATCH_AD_2);
        WATCH_AD_TEXTS[3] = TextControl.getInstance().get(KJTexts.BTN_WATCH_AD_3);
    }

    @Override
    public void initContent() {
        createMid();
        createBottom(menuControl);
    }

    private void createMid() {
        Table mid = new Table();

        star0 = new StarIcon(menuControl, TextControl.getInstance()
                        .get(KJTexts.POPUP_WIN_STAR0_LABEL));
        star0.setName(STAR0);
        star0.setAnimListener(this);
        star1 = new StarIcon(menuControl, VJXString.STR_EMPTY);
        star1.setName(STAR1);
        star1.setAnimListener(this);
        star2 = new StarIcon(menuControl, VJXString.STR_EMPTY);
        star2.setName(STAR2);
        star2.setAnimListener(this);

        mid.add(star0).top();
        mid.add(star1).top();
        mid.add(star2).top();

        content.add(mid).expand();
        content.row();
    }

    private void createBottom(final MenuStage mc) {
        TextControl texts = TextControl.getInstance();
        ClickListener collectListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                collectClicked = true;
                collectSuns();
            }
        };

        collectBtn = new KJIconTextButton(KJAssets.BUTTON_BLUE,
                        texts.get(KJTexts.BTN_COLLECT_SUNS),
                        getSkin(), getSkin().getDrawable(KJAssets.ICON_SUN_DBL_WIN));
        collectBtn.setFont(EJXFont.TITLE_26);
        collectBtn.centerText();
        //        collectBtn.setIconSize(96, 96);
        collectBtn.alignIcon(Align.topRight);
        collectBtn.setIconOffset(0, 16);
        collectBtn.addListener(collectListener);

        watchAdBtn = ((KJMenu) mc).getWatchAdButton();
        watchAdListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                mc.getGame().sfx.playDynSFX(KJAssets.SFX_CLICK);
                ((KJGame) mc.getGame()).getAdControl().showAd(KJString.AD_double_suns);
                watchAdBtn.setEnabled(false);
            }
        };

        ClickListener contListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                continueTriggered();
            }
        };
        continueBtn = EJXSkin.createTButton(KJAssets.BUTTON_GREEN,
                        texts.get(KJTexts.BTN_CONTINUE),
                        getSkin(), contListener);

        Table bottom = new Table();
        bottom.add(collectBtn);
        watchAdCell = bottom.add(watchAdBtn);
        bottom.add(continueBtn);

        content.add(bottom).expand().top().pad(0, 25, 20, 25);

        Drawable background = mc.getSkin().getDrawable(KJAssets.ICON_WIN_PBAR_BG);
        Drawable fill = mc.getSkin().getDrawable(KJAssets.ICON_WIN_PBAR_FILL);
        pBar = new ProgressBar(menuControl, background, fill);
        content.addActor(pBar);
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        super.entered(g, mc);
        audioControl.stopAllSounds();
        audioControl.playDynSFX(KJAssets.SFX_APPLAUSE);
        audioControl.playDynSFX(KJAssets.SFX_SF_SPAWN);

        clearActions();

        SnapshotArray<Actor> actors = getChildren();
        for (int i = 0; i < actors.size; i++) {
            Actor a = actors.get(i);
            if (a.getName() == null)
                continue;
            if (a.getName().contains(WinProgressAction.flyingSun)) {
                a.remove();
                i--;
            }
        }

        KJPlayer p = (KJPlayer) g.getPlayer();
        sunsIcon.setText(Integer.toString(p.getCoins()));
        showSuns(true);

        totemIcon.setText(Integer.toString(p.getAmount(ItemCategory.CONTINUE)));
        showTotems(true);

        collectClicked = false;

        pBar.setVisible(false);
        continueBtn.setVisible(false);
        collectBtn.setVisible(true);

        KJPlayer player = (KJPlayer) g.getPlayer();
        currentPlayerCoins = player.getCoins();
        playerMultiplier = player.getCoinMultiply();
        if (playerMultiplier == 2) {
            collectBtn.setIconEnabled(true);
        } else {
            collectBtn.setIconEnabled(false);
        }
        rewardCount = 0;
        rewardMultiplier = WATCH_AD_MULTIPLIERS[rewardCount];

        watchAdBtn.setIconEnabled(false);
        updateWatchAdText(((KJGame) g).getAdControl());
        watchAdCell.setActor(watchAdBtn);

        KJLevel lvl = (KJLevel) g.getScreen().getLevel();
        InventoryLogic inventory = (InventoryLogic) player.getEntity()
                        .getLogic(KJLogicType.INVENTORY);

        collectedCoins = inventory.getAmount(ItemCategory.COIN) / playerMultiplier;
        int totLvlSuns = lvl.getTotalSuns();

        bonusFound = lvl.bonusFound();
        sunsFound = collectedCoins == totLvlSuns;

        if (lvl.isBossLevel()) {
            star1.setTitle(TextControl.getInstance()
                            .get(KJTexts.POPUP_WIN_STAR1_DAMAGE_LABEL));
        } else star1.setTitle(TextControl.getInstance()
                        .get(KJTexts.POPUP_WIN_STAR1_BONUS_LABEL));

        star2.setTitle(collectedCoins + VJXString.SEP_SLASH + totLvlSuns);

        star0.setAnim(APPEAR, false);
        star1.setAnim(EMPTY, false);
        star2.setAnim(EMPTY, false);

        menuControl.getGame().getScreen().getDebugStage().getDebugUI()
        .updateStats(true);
        mc.toggleOverlay(Transition.VIDEO_SEQUENCE, false);
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
        g.getScreen().getLevel().setRunBackground(true);
        g.getScreen().getLevel().setActive(false, .1f);
    }

    @Override
    public void enterCallback() {
        if (!collectClicked) {
            audioControl.playDynSFX(KJAssets.SFX_CLICK);
            collectClicked = true;
            collectSuns();
        } else {
            continueTriggered();
        }
    }

    public void reward() {
        rewardCount++;
        rewardMultiplier = WATCH_AD_MULTIPLIERS[rewardCount];
        watchAdBtn.setIconEnabled(true);
    }

    public void updateWatchAdText(AdMobController adControl) {
        watchAdBtn.setVisible(!collectClicked);
        watchAdBtn.setEnabled(false);

        watchAdBtn.setIcon(getSkin().getDrawable(KJAssets.ICON_EMBLEM_BG));
        watchAdBtn.setIconText(X + WATCH_AD_MULTIPLIERS[rewardCount]);
        watchAdBtn.centerIconText();

        int multID = 0;
        if (rewardCount < 3) {
            // reload ad
            if (adControl != null) {
                adControl.loadAd(KJString.AD_double_suns);

                watchAdBtn.setEnabled(
                        adControl.isLoaded(KJString.AD_double_suns));
            }
            multID = rewardCount + 1;
        } else {
            rewardCount = multID = 3;
        }

        watchAdBtn.setFont(EJXFont.TITLE_26);
        watchAdBtn.setText(WATCH_AD_TEXTS[rewardCount] + WATCH_AD_MULTIPLIERS[multID]);
        watchAdBtn.centerText();
        watchAdBtn.clearListeners();
        watchAdBtn.addListener(watchAdListener);

        float multiplier = playerMultiplier * WATCH_AD_MULTIPLIERS[rewardCount];
        collectBtn.setText(TextControl.getInstance()
                        .get(KJTexts.BTN_COLLECT_SUNS) + multiplier);
    }

    private void collectSuns() {
        Vector2 tmpVec = new Vector2(0, 0);
        collectBtn.localToStageCoordinates(tmpVec);
        pBar.setPosition(tmpVec.x, tmpVec.y + collectBtn.getHeight() / 2f
                        - pBar.getHeight() / 2f);

        pBar.setVisible(true);
        continueBtn.setVisible(true);
        collectBtn.setVisible(false);
        watchAdBtn.setVisible(false);

        float tmpMultiplier = rewardMultiplier * playerMultiplier;
        KJLevel lvl = (KJLevel) menuControl.getScreen().getLevel();
        int collectedSuns = (int) Math.ceil(collectedCoins * tmpMultiplier);
        pBar.setEmpty();
        pBar.setValue(collectedSuns);
        pBar.setMaxValue((int) Math.ceil(lvl.getTotalSuns() * tmpMultiplier));
        pBar.setMultiplier(tmpMultiplier);

        if (collectedSuns > 0) {
            audioControl.playDynSFX(KJAssets.SFX_PBAR, false);
            pBar.setEmpty(2f);
            addAction(KJActions.winProgressAction(menuControl.getGame(),
                            pBar.getIcon(), pBar.getIcon().getLabel(),
                            sunsIcon.getLabel(), collectedSuns,
                            currentPlayerCoins, 2f / collectedSuns,
                            tmpMultiplier));
        }

        EJXGame g = menuControl.getGame();
        ((KJPlayer) g.getPlayer()).setCoins(currentPlayerCoins + collectedSuns);

        KJCloudSave saveGame = (KJCloudSave) g.getSave();
        saveGame.saveAll(g, bonusFound, sunsFound);

        collectedCoins = 0;
    }

    @Override
    public void start(SpineActor actor, String anim) {
    }

    @Override
    public void interrupt(SpineActor actor, String anim) {
    }

    @Override
    public void end(SpineActor actor, String anim) {
    }

    @Override
    public void dispose(SpineActor actor, String anim) {
    }

    @Override
    public void complete(SpineActor actor, String anim) {
        switch (anim) {
            case APPEAR:
                updateAppear(actor);
                break;
            case IDLE:
                updateIdle(actor);
                break;
        }
    }

    private void updateIdle(SpineActor actor) {
        switch (actor.getName()) {
            case STAR0:
                if (bonusFound && star1.isEmpty()) {
                    star1.setAnim(APPEAR, false);
                    audioControl.playDynSFX(KJAssets.SFX_SF_SPAWN);
                } else if (sunsFound && star2.isEmpty()) {
                    star2.setAnim(APPEAR, false);
                    audioControl.playDynSFX(KJAssets.SFX_SF_SPAWN);
                }
                break;
            case STAR1:
                if (sunsFound && star2.isEmpty()) {
                    audioControl.playDynSFX(KJAssets.SFX_SF_SPAWN);
                    star2.setAnim(APPEAR, false);
                }
                break;
        }
    }

    private void updateAppear(SpineActor actor) {
        actor.setAnimation(IDLE, true);
        audioControl.playDynSFX(KJAssets.SFX_SF_SPAWNED);
        if (actor.getName() == STAR0)
            audioControl.playDynSFX(KJAssets.SFX_WIN_VO_0);
        else if (actor.getName() == STAR1)
            audioControl.playDynSFX(KJAssets.SFX_WIN_VO_1);
        else audioControl.playDynSFX(KJAssets.SFX_WIN_VO_2);
    }

    private void continueTriggered() {
        audioControl.stopAllSounds();
        audioControl.playDynSFX(KJAssets.SFX_CLICK);

        KJLevel level = (KJLevel) menuControl.getScreen().getLevel();
        if (level.isEndLevel()) {
            menuControl.hidePopup(false);
            ((KJMenu) menuControl).switchMenu(MENU.END.id, false);
        } else {
            menuControl.getScreen().unloadLevel(false);
        }
    }

    @Override
    public void event(SpineActor actor, String animy, Event event) {
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.POPUP_WIN_TITLE));
        star0.setTitle(TextControl.getInstance().get(KJTexts.POPUP_WIN_STAR0_LABEL));
        WATCH_AD_TEXTS[0] = texts.get(KJTexts.BTN_WATCH_AD_0);
        WATCH_AD_TEXTS[1] = texts.get(KJTexts.BTN_WATCH_AD_1);
        WATCH_AD_TEXTS[2] = texts.get(KJTexts.BTN_WATCH_AD_2);
        WATCH_AD_TEXTS[3] = texts.get(KJTexts.BTN_WATCH_AD_3);
        continueBtn.setText(texts.get(KJTexts.BTN_CONTINUE));
    }
}