package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.culturalgames.kwjourney.ui.IngamePopup;
import de.culturalgames.kwjourney.ui.widgets.KJTextArea;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.entity.EJXEntity;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.Lifecycle;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class SecretPopup extends IngamePopup {

    private EJXLabel secretTypeDiscovered;
    private KJTextArea textArea;
    private Image secretIcon;
    private Secret secret;
    private Table cLeft;
    private Table cRight;
    private EJXEntity object;

    public SecretPopup(KJMenu mc) {
        super(mc, "ui_popup_secret");
    }

    @Override
    public void initContent() {
        removeTop();

        secretTypeDiscovered = new EJXLabel(getSkin(), VJXString.STR_EMPTY,
                        EJXFont.TEXT_32_BLUE);

        textArea = new KJTextArea(getSkin(), 540,
                        565 - secretTypeDiscovered.getHeight(), false, true);
        textArea.setTitleAlign(Align.topLeft);
        textArea.update(0, 0, 75, true);

        secretIcon = new Image();

        cLeft = new Table();
        cLeft.add(secretIcon).size(384).padLeft(25);

        cRight = new Table();
        cRight.add(secretTypeDiscovered).expandX().top().padTop(75).left();
        cRight.row();
        cRight.add(textArea).left().padRight(75);

        Table t = new Table();
        t.setBackground(getSkin().getDrawable(KJAssets.ICON_WHITE_MEDIUM));
        t.add(cLeft);
        t.add(cRight);

        content.add(t).padBottom(20);

        addListener(menuControl.uiBackListener);
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        super.entered(g, mc);
        audioControl.playDynSFX(KJAssets.SFX_POPUP_OPEN);
        ((KJPlayer) g.getPlayer()).foundSecret(secret, g.getSave());
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
        super.exited(g, mc);
        audioControl.playDynSFX(KJAssets.SFX_POPUP_CLOSE);
        object.onEnd();
        object.setLifecycle(Lifecycle.DESPAWN);
    }

    @Override
    public void enterCallback() {
        audioControl.playDynSFX(KJAssets.SFX_CLICK);
        menuControl.hidePopup();
    }

    public void setSecret(Secret s, EJXEntity triggerObject) {
        TextControl texts = TextControl.getInstance();
        secret = s;
        object = triggerObject;

        String name = KJTexts.SECRET_ + s.getName();
        String subtitle = texts.get(name + TextControl._SUBTITLE);
        secretTypeDiscovered.setText(subtitle.toUpperCase());

        String title = texts.get(name + TextControl._TITLE);
        textArea.setTitle(s.getTitleSW().toUpperCase(), title.toUpperCase());

        String text = texts.get(name + TextControl._TEXT);
        textArea.setText(text);
        Drawable d = menuControl.getSkin().getDrawable(s.getIconName());
        if (d != null) {
            secretIcon.setDrawable(d);
        } else {
            VJXLogger.log("drawable '" + s.getIconName() + "' not found!");
        }
    }
}