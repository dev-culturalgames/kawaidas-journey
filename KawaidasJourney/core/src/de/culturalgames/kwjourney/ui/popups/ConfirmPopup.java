package de.culturalgames.kwjourney.ui.popups;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.ui.MenuPopup;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.TextButton;

public class ConfirmPopup extends MenuPopup {

    private EJXLabel titleLabel;
    private EJXLabel textLabel;

    private TextButton confirmBtn;
    private TextButton cancelBtn;

    private Runnable onConfirm;
    private Runnable onCancel;

    public ConfirmPopup(MenuStage mc) {
        super(mc, "ui_popup_confirm");
    }

    @Override
    public void initContent() {
        ClickListener confirmListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                menuControl.hidePopup(onConfirm);
            }
        };

        ClickListener cancelListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                menuControl.hidePopup(onCancel);
            }
        };

        Table table = new Table();
        table.setBackground(getSkin().getDrawable(KJAssets.ICON_WHITE_MEDIUM));

        titleLabel = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.TITLE_64);
        textLabel = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.TEXT_36);
        textLabel.setWrap(true);

        table.add(titleLabel).top().padTop(50);
        table.row();
        table.add(textLabel).top().expand();
        table.row();

        Table subTable = new Table();
        cancelBtn = EJXSkin.createTButton(KJAssets.BUTTON_RED,
                        TextControl.getInstance().get(KJTexts.BTN_CANCEL),
                        getSkin(), cancelListener);
        confirmBtn = EJXSkin.createTButton(KJAssets.BUTTON_GREEN,
                        TextControl.getInstance().get(KJTexts.BTN_CONFIRM),
                        getSkin(), confirmListener);
        subTable.add(cancelBtn).expand();
        subTable.add(confirmBtn).expand();
        table.add(subTable).bottom().padBottom(30);

        content.add(table);
    }

    public void setTitle(String title) {
        titleLabel.setText(title);
    }

    public void setText(String text) {
        textLabel.setText(text);
    }

    public void setOnConfirm(Runnable onConfirm) {
        this.onConfirm = onConfirm;
    }

    public void setOnCancel(Runnable onCancel) {
        this.onCancel = onCancel;
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
        onCancel = null;
        onConfirm = null;
    }

    @Override
    public void updateLanguage(TextControl texts) {
        cancelBtn.setText(texts.get(KJTexts.BTN_CANCEL));
        confirmBtn.setText(texts.get(KJTexts.BTN_CONFIRM));
    }
}