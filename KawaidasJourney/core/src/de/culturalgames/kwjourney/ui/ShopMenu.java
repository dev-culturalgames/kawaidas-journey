package de.culturalgames.kwjourney.ui;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.pay.PurchaseManager;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.KJPlayer;
import de.culturalgames.kwjourney.api.APILoader;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.ui.widgets.ShopButton;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.SourceProperty;
import de.venjinx.ejx.util.EJXTypes.SpatialProperty;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.VJXIcon;

public abstract class ShopMenu extends ScrollSubMenu {

    private KJPlayer player;
    private KJMenu kjMenu;
    private HashMap<String, ShopButton> items;
    protected SpineActor keeper;
    private VJXIcon sunsIcon;
    private TiledMap shopMap;

    public ShopMenu(MenuStage mc, String name, String title, TiledMap map) {
        super(mc, name, title);
        player = (KJPlayer) mc.getGame().getPlayer();
        kjMenu = (KJMenu) mc;
        items = new HashMap<>();
        shopMap = map;
        init(map);
    }

    public void resetShop() {
        for (ShopButton button : items.values()) {
            if (button.getOffer().isReward())
                kjMenu.lock(button, button.getLockIcon());
        }
    }

    public HashMap<String, ShopButton> getOfferButtons() {
        return items;
    }

    public void updateSuns() {
        sunsIcon.setText(Integer.toString(player.getCoins()));
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        updateSuns();
        scroll.updateVisualScroll();
        keeper.setVisible(true);
        updateItems();
    }

    @Override
    protected void initOverlay(String title) {
        overlayLayer = new Table();
        overlayLayer.setFillParent(true);

        header = new Table();
        header.setName(getName() + "_header");

        backBtn = EJXSkin.createTButton(KJAssets.BUTTON_BACK, getSkin());
        backBtn.addListener(menuControl.uiBackListener);
        header.add(backBtn).padLeft(25);

        headTitle = new EJXLabel(getSkin(), title.toUpperCase());
        header.add(headTitle).expandX();

        sunsIcon = new VJXIcon(VJXString.STR_EMPTY,
                        getSkin().getDrawable(KJAssets.ICON_SUN), getSkin());
        sunsIcon.setSize(96, 96);
        header.add(sunsIcon).size(128).padRight(25);

        header.pack();
        overlayLayer.add(header).expand().fillX().top();
    }

    @Override
    protected void initScrollContent() {
        contentLayer.add(content).expand().fill();
    }

    protected Actor initRightActor() {
        return EJXSkin.createTButton(KJAssets.BUTTON_BLUE, "PRIME SHOP", getSkin(),
                        new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                keeper.setVisible(false);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                kjMenu.switchMenu(MENU.SHOP_PRIME);
            }
        });
    }

    private void updateItems() {
        PurchaseManager pManager = ((KJGame) menuControl.getGame())
                        .getPurchaseManager();
        for (ShopButton sb : items.values()) {
            sb.update(player, pManager);
        }
    }

    private void init(TiledMap map) {
        MapProperties mapProps = shopMap.getProperties();
        int mapWidth = mapProps.get(VJXString.width, Integer.class);
        int tileWidth = mapProps.get("tileWidth", Integer.class);
        int width = mapWidth * tileWidth;
        MapLayers layers = shopMap.getLayers();

        Table scrollTable = new Table();
        scrollContent.add(scrollTable).size(width, getHeight());
        initObjects(layers.get("back"));

        final KJGame g = (KJGame) menuControl.getGame();
        initItems(g, layers.get("items"));

        initShopKeeper();

        initObjects(layers.get("table"));

        initObjects(layers.get("front"));
    }

    private void initItems(KJGame g, MapLayer layer) {
        MapObjects objects = layer.getObjects();
        if (objects == null)
            return;
        for (MapObject o : objects) {
            MapProperties props = o.getProperties();

            String type = props.get(VJXString.type, String.class);
            if (type != null) {
                switch (type) {
                    case VJXString.TILED_object:
                        initObject(props);
                        break;
                    case KJString.OBJECT_offer:
                        initOffer(g, props);
                        break;
                }
            }
        }
    }

    private void initShopKeeper() {
        MapObject keeperObject = null;
        for (MapLayer l : shopMap.getLayers()) {
            keeperObject = l.getObjects().get("shopKeeper");
            if (keeperObject != null) break;
        }

        float x;
        if (keeperObject != null) {
            MapProperties props = keeperObject.getProperties();
            String skeletonName = props.get(DefinitionProperty.skeleton.name,
                            VJXString.STR_EMPTY, String.class);
            String defName = props.get(DefinitionProperty.defName.name,
                            VJXString.STR_EMPTY, String.class);
            float w = props.get(SpatialProperty.width.name, 0f, Float.class);
            float h = props.get(SpatialProperty.height.name, 0f, Float.class);
            x = props.get(SpatialProperty.x.name, 0f, Float.class);
            float y = props.get(SpatialProperty.y.name, 0f, Float.class);
            float sclX = props.get(SpatialProperty.scaleX.name, 0f, Float.class);
            float sclY = props.get(SpatialProperty.scaleY.name, 0f, Float.class);
            boolean flipX = props.get(SpatialProperty.flipX.name, false, Boolean.class);

            String src = props.get(SourceProperty.source.name,
                            VJXString.STR_EMPTY, String.class)
                            + VJXString.SEP_SLASH + skeletonName + ".json";
            FileHandle file = Gdx.files.internal(src);
            keeper = menuControl.getGame().factory.createSpineActor(
                            "shopKeeper", w, h, KJAssets.ATLAS_SHOP_CHARACTER,
                            file, null);

            keeper.setScale(sclX, sclY);
            keeper.setPosition(x, y);
            keeper.setSkin(defName);
            keeper.flipX(flipX);
            keeper.setAnimation("idle", true);
            keeper.setTouchable(Touchable.disabled);
            scrollContent.addActor(keeper);
        }
    }

    private void initOffer(final KJGame g, MapProperties props) {
        String item = props.get(VJXString.tileName, String.class);
        final VJXOffer o = APILoader.getOffer(item);
        if (o != null) {
            final ShopButton shopBtn = new ShopButton(menuControl, o);
            float x = props.get(VJXString.x, float.class);
            float y = props.get(VJXString.y, float.class);
            shopBtn.setPosition(x, y);
            scrollContent.addActor(shopBtn);
            items.put(o.getIdentifier(), shopBtn);
        }
    }

    private void initObjects(MapLayer layer) {
        MapObjects objects = layer.getObjects();
        if (objects != null)
            for (MapObject o : objects)
                initObject(o.getProperties());
    }

    private void initObject(MapProperties props) {
        Image img;
        float x = props.get(SpatialProperty.x.name, float.class);
        float y = props.get(SpatialProperty.y.name, float.class);
        boolean flipX = props.get(SpatialProperty.flipX.name, boolean.class);
        boolean flipY = props.get(SpatialProperty.flipY.name, boolean.class);
        String name = props.get(VJXString.tileName, String.class);
        img = new Image(getSkin().getDrawable(KJString.PATH_icons_shop + name));
        img.pack();
        img.setOrigin(img.getWidth() / 2, img.getHeight() / 2);
        if (flipX)
            img.setScaleX(-1);
        if (flipY)
            img.setScaleY(-1);
        img.setPosition(x, y);
        img.setTouchable(Touchable.disabled);
        scrollContent.addActor(img);
    }
}