package de.culturalgames.kwjourney.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.venjinx.ejx.stages.MenuStage;

public abstract class UIWidget extends Table {
    public UIWidget(MenuStage mc, String name) {
        super(mc.getSkin());
        setName(name);
    }
}