package de.culturalgames.kwjourney.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.vjx.ui.EJXMenuTable;

public abstract class MenuPopup extends EJXMenuTable {

    protected Table content;

    public MenuPopup(MenuStage mc, String name) {
        super(mc, name);
        content = new Table();
        initContent();

        content.pack();
        add(content).fill().expand();

        setTransform(true);
        setOrigin(mc.getWidth() / 2f, mc.getHeight() / 2f);
    }

    public abstract void initContent();

    @Override
    public void entered(EJXGame g, MenuStage mc) {
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
    }

    @Override
    public void enterCallback() {
    }

    @Override
    public Table getContent() {
        return content;
    }

    @Override
    public void lvlLoaded() {
    }

    @Override
    public void updateLanguage(TextControl texts) {
    }

    @Override
    public void updateLayout(Skin skin) {
    }
}