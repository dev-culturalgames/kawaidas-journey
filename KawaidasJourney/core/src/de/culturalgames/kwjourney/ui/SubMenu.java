package de.culturalgames.kwjourney.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.MenuTable;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public abstract class SubMenu extends MenuTable {

    private Table transBG;

    protected Table overlayLayer;
    protected Table header;

    protected Button backBtn;
    protected EJXLabel headTitle;
    private Button helpBtn;

    protected Table contentLayer;
    protected Table content;

    public SubMenu(MenuStage mc, String name) {
        this(mc, name, VJXString.STR_EMPTY);
    }

    public SubMenu(final MenuStage mc, String name, String title) {
        super(mc, name);
        setBackground(mc.getGame().assets.getDrawable(KJAssets.IMAGE_MENU_START));

        Stack stack = new Stack();

        transBG = new Table();
        transBG.setFillParent(true);
        transBG.setBackground(getSkin().getDrawable(KJAssets.BUTTON_BG_TRANS_80));
        addActor(transBG);

        initOverlay(title);

        contentLayer = new Table();
        contentLayer.setSize(mc.getWidth(), mc.getHeight());

        content = new Table();
        content.setFillParent(false);
        content.setName(getName() + "_content");

        initContent();

        stack.add(contentLayer);
        stack.add(overlayLayer);
        add(stack).expand().fill();
    }

    protected void initOverlay(String title) {
        overlayLayer = new Table();
        overlayLayer.setFillParent(true);

        header = new Table();
        header.setName(getName() + "_header");

        backBtn = EJXSkin.createTButton(KJAssets.BUTTON_BACK, getSkin(),
                        menuControl.uiBackListener);
        backBtn.addListener(menuControl.uiBackListener);
        header.add(backBtn).padLeft(25);

        headTitle = new EJXLabel(getSkin(), title.toUpperCase());
        header.add(headTitle).expandX();

        helpBtn = EJXSkin.createTButton(KJAssets.BUTTON_HELP, getSkin());
        header.add(helpBtn).padRight(25);

        header.pack();
        overlayLayer.add(header).expand().fillX().top();
    }

    public void setTitle(String title) {
        headTitle.setText(title.toUpperCase());
    }

    public void addHelpBtnListener(ClickListener clickListener) {
        if (clickListener != null) helpBtn.addListener(clickListener);
    }

    public void showHelp(boolean show) {
        helpBtn.setVisible(show);
    }

    protected abstract void initContent();
}