package de.culturalgames.kwjourney.ui.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.MenuTable;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class CreditsMenu extends MenuTable {

    private Button closeBtn;
    private ScrollPane scroll;
    private Table scrollTable;
    private EJXLabel title, text;

    private EJXLabel imgTitle0, imgTitle1;
    private Image image0, image1;

    private Table socialTable;
    private Image social0, social1, social2, social3, social4;

    private float scrollDelay = 1f;

    public CreditsMenu(KJMenu mc) {
        super(mc, "ui_menu_credits");
        TextControl textControl = TextControl.getInstance();

        scrollTable = new Table();
        scroll = new ScrollPane(scrollTable);
        scroll.setStyle(new ScrollPaneStyle());
        scroll.setOverscroll(false, false);
        add(scroll).expand().fill();

        title = new EJXLabel(getSkin(), textControl.get(KJTexts.MENU_CREDITS_TITLE));

        text = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.TITLE_32);
        text.setWrap(true);
        text.setText(textControl.get(KJTexts.MENU_CREDITS_TEXT));

        imgTitle0 = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.TITLE_32);
        imgTitle0.setText(textControl.get(KJTexts.MENU_CREDITS_IMG0_TITLE));

        image0 = new Image(mc.getGame().assets.getDrawable(KJAssets.IMAGE_CREDITS0));

        imgTitle1 = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.TITLE_32);
        imgTitle1.setText(textControl.get(KJTexts.MENU_CREDITS_IMG1_TITLE));

        image1 = new Image(mc.getGame().assets.getDrawable(KJAssets.IMAGE_CREDITS1));

        closeBtn = EJXSkin.createTButton(KJAssets.BUTTON_CLOSE, getSkin());
        closeBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_BACK);
                if (menuControl.getScreen().getLevel().dataIsPrepared()) {
                    menuControl.stopMusic();
                    menuControl.getScreen().unloadLevel(false);
                } else {
                    menuControl.restoreUI();
                    menuControl.playMusic(KJAssets.MUSIC_MAIN);
                }
            }
        });

        closeBtn.setPosition(menuControl.getWidth() - 153,
                        menuControl.getHeight() - 128);

        createSocial();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        scrollDelay = scroll.isTouchFocusTarget() ? 1f : scrollDelay - delta;

        if (scrollDelay <= 0f) {
            float scrollPercent = scroll.getScrollPercentY();
            float newScroll = scrollPercent + .01f * delta;
            newScroll = newScroll > 1f ? 1f : newScroll;
            scroll.setScrollPercentY(newScroll);
        }
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        menuControl.playMusic(KJAssets.MUSIC_CREDITS);
        scroll.setScrollPercentY(0);
        scrollDelay = 1f;
    }

    private void createSocial() {
        ClickListener discordListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_DISCORD);
            }
        };

        ClickListener facebookListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_FACEBOOK);
            }
        };

        ClickListener instagramListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_INSTAGRAM);
            }
        };

        ClickListener twitterListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_TWITTER);
            }
        };

        ClickListener youtubeListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_YOUTUBE);
            }
        };

        socialTable = new Table();
        socialTable.setName("ui_social_links");
        social0 = new Image(getSkin().getDrawable(KJAssets.ICON_DISCORD));
        social0.addListener(discordListener);
        social1 = new Image(getSkin().getDrawable(KJAssets.ICON_FACEBOOK));
        social1.addListener(facebookListener);
        social2 = new Image(getSkin().getDrawable(KJAssets.ICON_INSTAGRAM));
        social2.addListener(instagramListener);
        social3 = new Image(getSkin().getDrawable(KJAssets.ICON_TWITTER));
        social3.addListener(twitterListener);
        social4 = new Image(getSkin().getDrawable(KJAssets.ICON_YOUTUBE));
        social4.addListener(youtubeListener);
    }

    @Override
    public void updateLanguage(TextControl texts) {
        title.setText(texts.get(KJTexts.MENU_CREDITS_TITLE));
        text.setText(texts.get(KJTexts.MENU_CREDITS_TEXT));
        imgTitle0.setText(texts.get(KJTexts.MENU_CREDITS_IMG0_TITLE));
        imgTitle1.setText(texts.get(KJTexts.MENU_CREDITS_IMG1_TITLE));
    }

    @Override
    public void updateLayout(Skin skin) {
        scrollTable.clear();
        socialTable.clear();

        socialTable.add(social0);
        socialTable.add(social1).padLeft(25);
        socialTable.add(social2).padLeft(25);
        socialTable.add(social3).padLeft(25);
        socialTable.add(social4).padLeft(25);

        scrollTable.add(title).expandX().padTop(300);
        scrollTable.row();
        scrollTable.add(text).width(768).expandX();
        scrollTable.row();
        scrollTable.add(imgTitle0).expandX().padTop(96);
        scrollTable.row();
        scrollTable.add(image0).expandX().padTop(25);
        scrollTable.row();
        scrollTable.add(imgTitle1).expandX().padTop(128);
        scrollTable.row();
        scrollTable.add(image1).expandX().padTop(25);
        scrollTable.row();
        scrollTable.add(socialTable).expandX().padTop(50).padBottom(25);
        scrollTable.row();

        addActor(closeBtn);
    }
}