package de.culturalgames.kwjourney.ui.menus;

import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.ui.ShopMenu;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.controllers.TextControl;

public class ShopMenuPrime extends ShopMenu {

    public ShopMenuPrime(KJMenu mc) {
        super(mc, "ui_menu_shop_prime",
                        TextControl.getInstance().get(KJTexts.MENU_SHOP_PRIME_TITLE),
                        mc.mapPrimeShop);
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.MENU_SHOP_PRIME_TITLE));
    }
}