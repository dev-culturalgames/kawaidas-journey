package de.culturalgames.kwjourney.ui.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.KJScreen;
import de.culturalgames.kwjourney.api.AdMobController;
import de.culturalgames.kwjourney.ui.SubMenu;
import de.culturalgames.kwjourney.ui.popups.InfoPopup;
import de.culturalgames.kwjourney.ui.widgets.KJIconTextButton;
import de.culturalgames.kwjourney.ui.widgets.LanguageSelect;
import de.culturalgames.kwjourney.ui.widgets.VolumeControl;
import de.culturalgames.kwjourney.util.KJTexts;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.save.CloudSaveHandler;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXGameService;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.TextButton;

public class SettingsMenu extends SubMenu {

    private static final String HELP_about = "about";

    private KJGame game;
    private CloudSaveHandler cloudHandler;
    private EJXGameService gameService;

    private KJMenu kjMenu;
    private TextButton aboutBtn;
    private TextButton creditsBtn;
    private KJIconTextButton cloudServiceBtn;
    private VolumeControl volumeController;
    private LanguageSelect languageSelectBtn;
    private KJIconTextButton watchAdBtn;
    private ClickListener watchAdListener;
    private Cell<KJIconTextButton> watchAdCell;

    private Table disabledIconTable;
    private Image disabledImage;

    private final Runnable resetRun = new Runnable() {
        @Override
        public void run() {
            game.resetSave();
        }
    };

    private final Runnable restoreRun = new Runnable() {
        @Override
        public void run() {
            if (game.getPurchaseManager().installed()) {
                menuControl.showWaiting(true);
                game.getPurchaseManager().purchaseRestore();
            }
        }
    };

    public SettingsMenu(MenuStage menu) {
        super(menu, "ui_menu_settings", TextControl.getInstance()
              .get(KJTexts.MENU_SETTINGS_TITLE));
        showHelp(false);
    }

    @Override
    protected void initContent() {
        final TextControl texts = TextControl.getInstance();
        game = (KJGame) menuControl.getGame();
        kjMenu = (KJMenu) menuControl;

        volumeController = new VolumeControl(menuControl);

        ClickListener aboutListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event,
                                float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                kjMenu.showHelp(HELP_about, KJAssets.LINK_KAWAIDA);
            }
        };
        aboutBtn = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                                         texts.get(KJTexts.BTN_ABOUT), getSkin(),
                                         aboutListener);

        ClickListener creditsListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                kjMenu.switchMenu(MENU.CREDITS);
            }
        };
        creditsBtn = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                                           texts.get(KJTexts.BTN_CREDITS), getSkin(),
                                           creditsListener);

        ClickListener resetBtnListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                kjMenu.showConfirm(texts.get(KJTexts.CONFIRM_RESET_GAME_TITLE),
                                   texts.get(KJTexts.CONFIRM_RESET_GAME_TEXT),
                                   resetRun, null);
            }
        };
        KJIconTextButton resetBtn = new KJIconTextButton(KJAssets.BUTTON_YELLOW_SMALL,
                                                         VJXString.STR_EMPTY, getSkin(),
                                                         getSkin().getDrawable(KJAssets.ICON_RESET));
        resetBtn.addListener(resetBtnListener);

        ClickListener restoreShopListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                kjMenu.showConfirm(texts.get(KJTexts.CONFIRM_RESETORE_SHOP_TITLE),
                                   texts.get(KJTexts.CONFIRM_RESTORE_SHOP_TEXT),
                                   restoreRun, null);
            }
        };
        KJIconTextButton restoreShopBtn = new KJIconTextButton(KJAssets.BUTTON_YELLOW_SMALL,
                                                               VJXString.STR_EMPTY, getSkin(),
                                                               getSkin().getDrawable(KJAssets.ICON_RESTORE));
        restoreShopBtn.addListener(restoreShopListener);

        languageSelectBtn = new LanguageSelect(kjMenu);

        initGPSStuff();

        Table t1 = new Table();
        t1.add(aboutBtn).expandX();
        t1.row();
        t1.add(creditsBtn).expandX().padBottom(25);

        Table t2 = new Table();
        t2.add(cloudServiceBtn).expand();
        t2.add(resetBtn).expand();
        t2.add(restoreShopBtn).expand();
        t2.add(volumeController).expand();

        Table t3 = new Table();
        if (game.isDevMode()) {
            t2.add(languageSelectBtn).expand();
            //            t3.add(initLanguageSelect());
            t3.add(initDebug());
            t3.row();
        }
        t3.add(createSocialLinks()).expandX();

        content.add(t1);
        content.row();
        content.add(t2);
        content.row();
        content.add(t3).expand().top().padTop(50);
        contentLayer.add(content).expand().fill().padTop(header.getHeight());
    }

    public void updateConnectButton() {
        if (gameService.isConnected())
            cloudServiceBtn.setIcon(getSkin().getDrawable(KJAssets.ICON_CLOUD_ACTIVE));
        else cloudServiceBtn.setIcon(getSkin().getDrawable(KJAssets.ICON_CLOUD_INACTIVE));

        if (!cloudHandler.isAvailable())
            cloudServiceBtn.addActor(disabledIconTable);
        else disabledImage.remove();
    }

    private void initGPSStuff() {
        cloudHandler = game.getCloudHandler();
        gameService = game.getGameService();

        ClickListener gpsListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                if (!cloudHandler.isAvailable()) {
                    TextControl texts = TextControl.getInstance();
                    ((KJMenu) menuControl).showInfo(texts.get(
                                                              KJTexts.INFO_CLOUD_WIP_TITLE),
                                                    texts.get(KJTexts.INFO_CLOUD_WIP_TEXT));
                    ((InfoPopup) menuControl.getPopup(POPUP.INFO.id))
                    .initMonkey(game);
                    return;
                }
                if (!gameService.isConnected()) {
                    if (!cloudHandler.isAvailable()) {
                        TextControl texts = TextControl.getInstance();
                        ((KJMenu) menuControl).showInfo(texts.get(
                                                                  KJTexts.INFO_CLOUD_WIP_TITLE),
                                                        texts.get(KJTexts.INFO_CLOUD_WIP_TEXT));
                        ((InfoPopup) menuControl.getPopup(POPUP.INFO.id))
                        .initMonkey(game);
                    } else {
                        ((KJScreen) game.getScreen()).setLoadOnConnected(true);
                        gameService.connectService();
                    }
                } else gameService.disconnectCloud();
            }
        };
        cloudServiceBtn = new KJIconTextButton(KJAssets.BUTTON_YELLOW_SMALL,
                                               VJXString.STR_EMPTY, getSkin(),
                                               getSkin().getDrawable(KJAssets.ICON_CLOUD_INACTIVE));
        cloudServiceBtn.addListener(gpsListener);
        cloudServiceBtn.setFont(EJXFont.TITLE_32);
        cloudServiceBtn.centerText();

        disabledIconTable = new Table();
        disabledImage = new Image(getSkin().getDrawable(KJAssets.ICON_OFF_RED));
        disabledIconTable.add(disabledImage).expand();
        disabledIconTable.setSize(cloudServiceBtn.getWidth(), cloudServiceBtn.getHeight());
    }

    private Table initDebug() {
        Table t = new Table();
        ClickListener showBtnListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                cloudHandler.showSavedGamesUI();
            }
        };
        KJIconTextButton showBtn = new KJIconTextButton(KJAssets.BUTTON_YELLOW_SMALL,
                                                        VJXString.STR_EMPTY, getSkin(),
                                                        getSkin().getDrawable(KJAssets.ICON_RESTORE));
        showBtn.addListener(showBtnListener);

        ClickListener deleteBtnListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                cloudHandler.deleteSave(game.getSave().getName());
            }
        };
        KJIconTextButton deleteBtn = new KJIconTextButton(KJAssets.BUTTON_YELLOW_SMALL,
                                                          VJXString.STR_EMPTY, getSkin(),
                                                          getSkin().getDrawable(KJAssets.BUTTON_CLOSE));
        deleteBtn.addListener(deleteBtnListener);

        watchAdBtn = ((KJMenu) menuControl).getWatchAdButton();
        watchAdListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                game.sfx.playDynSFX(KJAssets.SFX_CLICK);
                game.getAdControl().showAd(KJString.AD_test);
                watchAdBtn.setEnabled(false);
            }
        };

        t.add(showBtn);
        t.add(deleteBtn);
        watchAdCell = t.add(watchAdBtn);
        return t;
    }

    private Table createSocialLinks() {
        ClickListener discordListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_DISCORD);
            }
        };

        ClickListener facebookListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_FACEBOOK);
            }
        };

        ClickListener instagramListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_INSTAGRAM);
            }
        };

        ClickListener twitterListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_TWITTER);
            }
        };

        ClickListener youtubeListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_YOUTUBE);
            }
        };

        Table t = new Table();
        t.setName("ui_social_links");
        Image social0 = new Image(getSkin().getDrawable(KJAssets.ICON_DISCORD));
        social0.addListener(discordListener);
        Image social1 = new Image(getSkin().getDrawable(KJAssets.ICON_FACEBOOK));
        social1.addListener(facebookListener);
        Image social2 = new Image(getSkin().getDrawable(KJAssets.ICON_INSTAGRAM));
        social2.addListener(instagramListener);
        Image social3 = new Image(getSkin().getDrawable(KJAssets.ICON_TWITTER));
        social3.addListener(twitterListener);
        Image social4 = new Image(getSkin().getDrawable(KJAssets.ICON_YOUTUBE));
        social4.addListener(youtubeListener);

        t.add(social0);
        t.add(social1).padLeft(25);
        t.add(social2).padLeft(25);
        t.add(social3).padLeft(25);
        t.add(social4).padLeft(25);

        return t;
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        updateConnectButton();
        volumeController.updateButtons();

        if (!game.isDevMode()) return;
        watchAdBtn.setVisible(true);
        watchAdBtn.setEnabled(false);

        watchAdBtn.setFont(EJXFont.TITLE_32);
        watchAdBtn.setText(TextControl.getInstance().get(KJTexts.BTN_WATCH_AD));
        watchAdBtn.centerText();

        watchAdBtn.setIcon(null);
        watchAdBtn.setIconText(VJXString.STR_EMPTY);
        watchAdBtn.clearListeners();
        watchAdBtn.addListener(watchAdListener);

        AdMobController adControl = ((KJGame) g).getAdControl();
        if (adControl != null)
            adControl.loadAd(KJString.AD_test);
        watchAdCell.setActor(watchAdBtn);
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.MENU_SETTINGS_TITLE));
        aboutBtn.setText(texts.get(KJTexts.BTN_ABOUT));
        creditsBtn.setText(texts.get(KJTexts.BTN_CREDITS));
        languageSelectBtn.setLanguageFlag(texts.getCurrentLocaleName());
    }
}