package de.culturalgames.kwjourney.ui.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.content.Secrets;
import de.culturalgames.kwjourney.content.Secrets.Secret;
import de.culturalgames.kwjourney.ui.SubMenu;
import de.culturalgames.kwjourney.ui.widgets.EncySubButtons;
import de.culturalgames.kwjourney.ui.widgets.KJTextArea;
import de.culturalgames.kwjourney.ui.widgets.ProgressIcon;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.TextButton;

public class EncySubMenu extends SubMenu {

    private ProgressIcon secretsProgress;
    private TextButton learnMore;

    private String curUrl;
    private String curEncyCategory;

    private EncySubButtons buttonArea;

    private KJTextArea textArea;

    public EncySubMenu(MenuStage mc) {
        super(mc, "ui_menu_ency_sub", VJXString.STR_EMPTY);
        showHelp(false);
    }

    @Override
    protected void initContent() {
        secretsProgress = new ProgressIcon(getSkin());
        learnMore = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        TextControl.getInstance().get(KJTexts.BTN_LEARN_MORE),
                        getSkin(), new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x,
                            float y) {
                super.clicked(event, x, y);
                if (curUrl != null) {
                    audioControl.playDynSFX(KJAssets.SFX_CLICK);
                    Gdx.net.openURI(curUrl);
                }
            }
        });

        content.add(create());
        contentLayer.add(content).expand().fill().padTop(header.getHeight());
    }

    public Table create() {
        buttonArea = new EncySubButtons(this, menuControl.getGame());
        textArea = new KJTextArea(getSkin());
        textArea.setTitleAlign(Align.topLeft);

        Table left = new Table();
        Table right = new Table();

        left.add(buttonArea);
        right.add(textArea);

        Table center = new Table();
        center.add(left).expandY().fillX().top().left().pad(0, 25, 0, 10);
        center.add(right).expand().top().left().pad(0, 10, 0, 0);

        learnMore.setPosition(textArea.getWidth() / 2 - learnMore.getWidth() / 2,
                        -learnMore.getHeight() / 2);
        right.addActor(learnMore);

        secretsProgress.setTransform(true);
        secretsProgress.setScale(.8f);
        secretsProgress.setPosition(
                        textArea.getWidth() - secretsProgress.getWidth() * .8f + 30,
                        textArea.getHeight() - secretsProgress.getHeight() * .8f / 2f);

        right.addActor(secretsProgress);
        return center;
    }

    public void setEncyCategory(MapProperties def) {
        curEncyCategory = def.get(VJXString.name, String.class);
        setTitle(TextControl.getInstance().get(KJTexts.MENU_ENCY_
                        + curEncyCategory + TextControl._TITLE));
        buttonArea.setCategory(curEncyCategory, 0);

        int total = Secrets.getSecretCount(curEncyCategory);
        int found = total;
        if (!menuControl.getGame().isDevMode()) {
            LocalSavegame ks = menuControl.getGame().getSave();
            found = ks.getInt(Secrets.PRE_secrets + curEncyCategory, 0);
        }

        secretsProgress.setText0(Integer.toString(found));
        secretsProgress.setText1(Integer.toString(total));
    }

    @Override
    public void exited(EJXGame game, MenuStage mc) {
        super.exited(game, mc);
        learnMore.setVisible(false);
        learnMore.setTouchable(Touchable.disabled);

        textArea.setTitle(VJXString.STR_EMPTY, VJXString.STR_EMPTY);
        textArea.setText(VJXString.STR_EMPTY);
        buttonArea.clearSelection();
    }

    public void showEncy(Secret secret) {
        TextControl texts = TextControl.getInstance();
        if (secret != null) {
            String titleSW = secret.getTitleSW();
            String title = texts.get(KJTexts.SECRET_ + secret.getName() + TextControl._TITLE);
            textArea.setTitle(titleSW.toUpperCase(), title.toUpperCase());
            textArea.setText(texts.get(KJTexts.ENCY_ + secret.getName() + TextControl._TEXT));

            curUrl = secret.getUrl();
            if (curUrl == null) {
                learnMore.setVisible(false);
                learnMore.setTouchable(Touchable.disabled);
            } else {
                learnMore.setVisible(true);
                learnMore.setTouchable(Touchable.enabled);
            }
        } else {
            learnMore.setVisible(false);
            learnMore.setTouchable(Touchable.disabled);

            textArea.setTitle(VJXString.STR_EMPTY, VJXString.STR_EMPTY);
            textArea.setText(VJXString.STR_EMPTY);
        }
    }

    @Override
    public void updateLanguage(TextControl texts) {
        learnMore.setText(texts.get(KJTexts.BTN_LEARN_MORE));
    }
}