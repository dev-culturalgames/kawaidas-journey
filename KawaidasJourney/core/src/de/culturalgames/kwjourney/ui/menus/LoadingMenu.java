package de.culturalgames.kwjourney.ui.menus;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.vjx.ui.LoadingUI;
import de.venjinx.vjx.ui.MenuTable;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class LoadingMenu extends MenuTable implements LoadingUI {

    private Table content;
    private Image loadingIcon;
    private Drawable loadingDrawable;
    private Drawable forwardDrawable;
    private EJXLabel chapLabel;
    private EJXLabel titleLabel;
    private EJXLabel subTitleLabel;

    private Button continueBtn;

    private float fullRotationTime = .2f;
    private float rotateStep = 360f / fullRotationTime;
    private float rotation = 0;

    private boolean initialized = false;

    public LoadingMenu(final MenuStage mc) {
        super(mc, "titleScreen");
    }

    public void initLevelLoadUI(Skin skin) {
        content = new Table();
        add(content).expand().fill();

        ClickListener listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (menuControl.getScreen().getLevel().isInitialized())
                    menuControl.getScreen().startLevel();
            }
        };

        continueBtn = new Button(skin.getDrawable(KJAssets.BUTTON_BG_TRANS_00));
        continueBtn.addListener(listener);
        continueBtn.setTouchable(Touchable.disabled);
        continueBtn.setFillParent(true);

        chapLabel = new EJXLabel(skin, VJXString.STR_EMPTY, EJXFont.CHAPTER);
        chapLabel.pack();

        titleLabel = new EJXLabel(skin, VJXString.STR_EMPTY, EJXFont.KANKIN_128);
        titleLabel.pack();

        subTitleLabel = new EJXLabel(skin, VJXString.STR_EMPTY,
                        EJXFont.KANKIN_SMALL);
        subTitleLabel.pack();

        loadingDrawable = skin.getDrawable(KJAssets.ICON_LOADING);
        forwardDrawable = skin.getDrawable(KJAssets.ICON_FORWARD);

        loadingIcon = new Image(loadingDrawable);
        loadingIcon.setPosition(menuControl.getWidth() - (loadingIcon.getWidth() + 25),
                        25);
        loadingIcon.setOrigin(loadingIcon.getWidth() / 2f,
                        loadingIcon.getHeight() / 2f);
        loadingIcon.setVisible(false);
        loadingIcon.pack();

        chapLabel.setTouchable(Touchable.disabled);
        titleLabel.setTouchable(Touchable.disabled);
        subTitleLabel.setTouchable(Touchable.disabled);
        loadingIcon.setTouchable(Touchable.disabled);
        initialized = true;
    }

    public void setTitle(String lvlNr, String title, String subTitle,
                    Drawable bgImage) {
        TextControl texts = TextControl.getInstance();
        String chapter = texts.get(KJTexts.MENU_LOADING_CHAPTER_TITLE);
        String level = texts.get(KJTexts.MENU_LOADING_LEVEL_TITLE);
        String[] split = lvlNr.split(VJXString.SEP_DASH);
        if (split.length == 2)
            chapLabel.setText(chapter + split[0] + level + split[1]);
        else chapLabel.setText(VJXString.STR_EMPTY);
        titleLabel.setText(title);
        subTitleLabel.setText(subTitle);
        setBackground(bgImage);
    }

    @Override
    public void startLoading(boolean showIcon) {
        loadingIcon.setDrawable(loadingDrawable);
        loadingIcon.setRotation(0);
        loadingIcon.setVisible(showIcon);
        continueBtn.setTouchable(Touchable.disabled);
    }

    @Override
    public void startUnloading(boolean showIcon) {
        loadingIcon.setDrawable(loadingDrawable);
        loadingIcon.setRotation(0);
        loadingIcon.setVisible(showIcon);
        continueBtn.setTouchable(Touchable.disabled);
    }

    @Override
    public void stopLoading() {
        loadingIcon.setDrawable(forwardDrawable);
        loadingIcon.setRotation(0);
        continueBtn.setTouchable(Touchable.enabled);
    }

    @Override
    public void updateLoading(float delta) {
        rotation -= rotateStep * delta;

        if (loadingIcon != null)
            loadingIcon.setRotation(rotation);
    }

    @Override
    public void enterCallback() {
        if (continueBtn.isTouchable())
            menuControl.getScreen().startLevel();
    }

    @Override
    public void updateLayout(Skin skin) {
        if (!initialized) return;

        content.clear();
        content.addActor(continueBtn);
        content.add(chapLabel);
        content.row();
        content.add(titleLabel).pad(50, 0, 25, 0);
        content.row();
        content.add(subTitleLabel);
        content.addActor(loadingIcon);
    }
}