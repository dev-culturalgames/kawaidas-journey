package de.culturalgames.kwjourney.ui.menus;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.api.APILoader;
import de.culturalgames.kwjourney.api.PurchaseHandler.ShopItem;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.content.Secrets;
import de.culturalgames.kwjourney.ui.ScrollSubMenu;
import de.culturalgames.kwjourney.ui.ShopMenu;
import de.culturalgames.kwjourney.ui.widgets.EncyButton;
import de.culturalgames.kwjourney.ui.widgets.LockIcon;
import de.culturalgames.kwjourney.ui.widgets.ShopButton;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;

public class EncyMenu extends ScrollSubMenu {

    private static final String HELP_ency = "ency";
    private static final String PROPERTY_position = "position";

    private KJMenu kjMenu;
    private EncyButton[] buttons;
    private int[][] encys;
    private EncySubMenu subMenu;
    private Object[] secretCats;

    public EncyMenu(MenuStage menu) {
        super(menu, "ui_menu_ency", TextControl.getInstance()
                        .get(KJTexts.MENU_ENCY_TITLE));
        kjMenu = (KJMenu) menu;
        subMenu = new EncySubMenu(menu);

        addHelpBtnListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                kjMenu.showHelp(HELP_ency);
            }
        });
    }

    public EncySubMenu getSubMenu() {
        return subMenu;
    }

    @Override
    protected void initScrollContent() {
        secretCats = Secrets.getSecretCategories().toArray();

        encys = new int[secretCats.length][2];
        buttons = new EncyButton[secretCats.length];

        int position;
        for (int i = 0; i < secretCats.length; i++) {
            final MapProperties def = (MapProperties) secretCats[i];
            ClickListener listener = new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x,
                                float y) {
                    super.clicked(event, x, y);
                    audioControl.playDynSFX(KJAssets.SFX_CLICK);
                    subMenu.setEncyCategory(def);
                    kjMenu.switchMenu(MENU.ENCYSUB);
                }
            };
            position = def.get("position", Integer.class);
            buttons[position] = new EncyButton(getSkin(), def, listener);
        }
        for (int i = 0; i < secretCats.length; i++) {
            scrollContent.add(buttons[i]).pad(0, 15, 0, 15);
        }
        contentLayer.add(content).expand().fill().padTop(header.getHeight());
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        LocalSavegame ks = g.getSave();
        for (int i = 0; i < encys.length; i++) {
            final MapProperties props = (MapProperties) secretCats[i];
            String name = props.get(VJXString.name, VJXString.STR_EMPTY, String.class);
            String saveString = Secrets.PRE_secrets + name;
            int position = props.get(PROPERTY_position, Integer.class);
            if (g.isDevMode())
                encys[position][0] = Secrets.getSecretCount(name);
            else {
                encys[position][0] = ks.getInt(saveString, 0);
            }

            encys[position][1] = Secrets.getSecretCount(name);
            buttons[position].setScore(encys[position][0], encys[position][1]);

            int newCount = ks.getInt(saveString + VJXString.POST_new, 0);
            buttons[position].setNewLabel(newCount);
        }
        checkGraduated();
    }

    private void checkGraduated() {
        int totalSecretCount = Secrets.getSecretsCount();
        int secretsFound = Secrets
                        .getFoundSecretsCount(menuControl.getGame().getSave());
        if (secretsFound != totalSecretCount) return;

        ShopMenu shop = (ShopMenu) menuControl.getMenu(MENU.SHOP_SKINS.id);
        VJXOffer offer = APILoader.getOffer(ShopItem.SKIN_GRADUATE.name);
        ShopButton shopButton = shop.getOfferButtons().get(offer.getIdentifier());
        LockIcon icon = shopButton.getLockIcon();
        if (icon.isLocked()) {
            kjMenu.unlock(shopButton, icon);
            kjMenu.showReward(offer.getIdentifier(), 0, icon);
        }
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.MENU_ENCY_TITLE));
        for (EncyButton button : buttons) {
            button.setTitle(texts.get(button.getName() + TextControl._TITLE));
        }
    }
}