package de.culturalgames.kwjourney.ui.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.ui.menus.MapMenu.MAPS;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.MenuTable;
import de.venjinx.vjx.ui.widgets.TextButton;

public class StartMenu extends MenuTable {

    private Image logo;
    private TextButton soundtrackBtn;

    public StartMenu(final KJMenu mc) {
        super(mc, "ui_menu_start");
        setBackground(mc.getGame().assets.getDrawable(KJAssets.IMAGE_MENU_START));
        logo = new Image(mc.getGame().assets.getDrawable(KJAssets.IMAGE_KJ_LOGO));
        logo.setPosition(280, 320);

        soundtrackBtn = EJXSkin.createTButton(KJAssets.BUTTON_SOUNDTRACK,
                        TextControl.getInstance().get(KJTexts.BTN_SOUNDTRACK),
                        getSkin(), null);
        soundtrackBtn.setFont(EJXFont.TITLE_32);
        soundtrackBtn.setTextOffset(85, 60);
        soundtrackBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI(KJAssets.LINK_SOUNDTRACK);
            }
        });
        Button projectsBtn = EJXSkin.createTButton(KJAssets.BUTTON_PROJECTS, getSkin());
        projectsBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                mc.switchMenu(MENU.PROJ);
            }
        });

        Button mapBtn = EJXSkin.createTButton(KJAssets.BUTTON_START, getSkin());
        mapBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                ((MapMenu) menuControl.getMenu(MENU.MAP.id)).setMap(MAPS.TZ);
                mc.switchMenu(MENU.MAP);
            }
        });

        Button settingsBtn = EJXSkin.createTButton(KJAssets.BUTTON_SETTINGS, getSkin());
        settingsBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                mc.switchMenu(MENU.SETT);
            }
        });

        Button shareBtn = EJXSkin.createTButton(KJAssets.BUTTON_SHARE, getSkin());
        shareBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                menuControl.getGame().getGameService().share();
            }
        });

        addActor(logo);

        Table col0 = new Table();
        col0.add(settingsBtn).expand().top().left()
        .pad(MenuStage.PAD_Y, MenuStage.PAD_X, 0, 0);
        col0.row();
        col0.add(shareBtn).expand().bottom().left().pad(0, MenuStage.PAD_X,
                        MenuStage.PAD_Y, 0);

        Table col1 = new Table();
        col1.add(mapBtn).expand().bottom().padBottom(150);

        Table col2 = new Table();
        col2.add(projectsBtn).expand().top().right().pad(-10, 0, 0, -10);
        col2.row();
        col2.add(soundtrackBtn).expand().bottom().right().pad(0, 0,
                        MenuStage.PAD_Y, MenuStage.PAD_X);

        add(col0).fill().width(500).expandY();
        add(col1).fill().expand();
        add(col2).fill().width(500).expandY();
    }

    @Override
    public void enterCallback() {
        audioControl.playDynSFX(KJAssets.SFX_CLICK);
        ((KJMenu) menuControl).switchMenu(MENU.MAP);
    }

    @Override
    public void updateLanguage(TextControl texts) {
        soundtrackBtn.setText(texts.get(KJTexts.BTN_SOUNDTRACK));
    }
}