package de.culturalgames.kwjourney.ui.menus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.pay.PurchaseManager;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.SnapshotArray;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KJGame.GameStats;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.api.APILoader;
import de.culturalgames.kwjourney.api.PurchaseHandler;
import de.culturalgames.kwjourney.api.PurchaseHandler.ShopItem;
import de.culturalgames.kwjourney.api.VJXOffer;
import de.culturalgames.kwjourney.save.KJCloudSave;
import de.culturalgames.kwjourney.ui.widgets.KJIconTextButton;
import de.culturalgames.kwjourney.ui.widgets.LevelButton;
import de.culturalgames.kwjourney.util.KJTexts;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.actions.EJXActions;
import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.entity.EntityFactory;
import de.venjinx.ejx.entity.logics.MoveLogic.MoveProperty;
import de.venjinx.ejx.save.LocalSavegame;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.DefinitionProperty;
import de.venjinx.ejx.util.EJXTypes.SpatialProperty;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.MenuTable;
import de.venjinx.vjx.ui.widgets.EJXLabel;
import de.venjinx.vjx.ui.widgets.TextButton;

public class MapMenu extends MenuTable {

    public static final String LAYER_map_clouds = "map_clouds";
    public static final String LAYER_map_deco = "map_deco";
    public static final String LAYER_map_elements = "map_elements";
    public static final String LAYER_map_level = "map_level";
    public static final String LAYER_path = "path";

    public enum MAPS {
        TZ("map_tz"), ST("map_st"), DAR("map_dar");
        public final String name;

        private MAPS(String name) {
            this.name = name;
        }

        public static MAPS get(String name) {
            for (MAPS map : MAPS.values()) {
                if (map.name.equals(name)) return map;
            }
            return null;
        }
    }

    public enum CHAPTER {
        SZ("zanzibar", 1), ST("stoneTown", 2),
        DAR("darEsSalaam", 3), SER("serengeti", 4);

        public final String name;
        public final int chapterID;

        private CHAPTER(String name, int chapterID) {
            this.name = name;
            this.chapterID = chapterID;
        }

        public static CHAPTER get(int chapterID) {
            for (CHAPTER map : CHAPTER.values()) {
                if (map.chapterID == chapterID) return map;
            }
            return null;
        }

        public static CHAPTER get(String name) {
            for (CHAPTER map : CHAPTER.values()) {
                if (map.name.equals(name)) return map;
            }
            return null;
        }
    }

    private HashMap<MAPS, Stack> maps;
    private HashMap<CHAPTER, List<LevelButton>> chapters;
    private HashMap<CHAPTER, LevelButton> chapterBtns;
    private HashMap<MAPS, LevelButton> firstChapterLevels;

    private MAPS currentMapID;
    private Stack currentMap;

    private ScrollPane scrollPane;

    private Image eaMap;

    private Cell<TextButton> btnCell;
    private Button backBtn;
    private TextButton homeBtn, encyBtn;
    private KJIconTextButton shopBtn;
    private KJIconTextButton bugBtn;
    private KJIconTextButton feedbackBtn;
    private KJIconTextButton testerBtn;
    private RunnableAction stopIntroAction;
    private Runnable stopIntro;

    private EJXLabel newSecretLabel;

    private boolean zoomed = false;
    private float scrollX = 0;

    private boolean fading;
    private String returnMapName;

    private boolean updateAnims = false;

    private SpineActor unlockAnimActor;

    public MapMenu(final KJMenu mc) {
        super(mc, "ui_sub_menu_map");
        TextControl textControl = TextControl.getInstance();

        maps = new HashMap<>();
        chapterBtns = new HashMap<>();
        chapters = new HashMap<>();
        firstChapterLevels = new HashMap<>();

        backBtn = EJXSkin.createTButton(KJAssets.BUTTON_BACK, getSkin());
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                setMap(MAPS.TZ);
            }
        });

        homeBtn = EJXSkin.createTButton(KJAssets.BUTTON_CYAN,
                        textControl.get(KJTexts.BTN_HOME), getSkin(),
                        menuControl.uiBackListener);

        encyBtn = EJXSkin.createTButton(KJAssets.BUTTON_YELLOW,
                        textControl.get(KJTexts.BTN_ENCY), getSkin(),
                        new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x,
                            float y) {
                                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                mc.switchMenu(MENU.ENCY);
            }
        });
        encyBtn.setFont(EJXFont.TITLE_32);
        encyBtn.centerText();

        newSecretLabel = new EJXLabel(getSkin(), "+1");
        newSecretLabel.setPosition(encyBtn.getWidth() - 40,
                        encyBtn.getHeight() - 40);
        encyBtn.addActor(newSecretLabel);

        bugBtn = new KJIconTextButton(KJAssets.BUTTON_BLUE,
                        textControl.get(KJTexts.BTN_REPORT_BUG), getSkin());
        bugBtn.setFont(EJXFont.TITLE_28);
        bugBtn.alignIcon(Align.left);
        bugBtn.centerText();
        bugBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI("https://discordapp.com/invite/uftcZBF");
            }
        });

        feedbackBtn = new KJIconTextButton(KJAssets.BUTTON_GREEN,
                        textControl.get(KJTexts.BTN_FEEDBACK), getSkin());
        feedbackBtn.setFont(EJXFont.TITLE_32);
        feedbackBtn.alignIcon(Align.left);
        feedbackBtn.centerText();
        feedbackBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI("https://docs.google.com/forms/d/e/1FAIpQLSfKMSrAcF-MUA1jNWohuDuL2q3tGnu9JUZ3THt6j_y7YoaUHw/viewform?c=0&w=1");
            }
        });

        testerBtn = new KJIconTextButton(KJAssets.BUTTON_GREEN,
                        textControl.get(KJTexts.BTN_TESTER), getSkin());
        testerBtn.setFont(EJXFont.TITLE_32);
        testerBtn.alignIcon(Align.left);
        testerBtn.centerText();
        testerBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                Gdx.net.openURI("http://www.kawaidasjourney.de/?gameTester");
            }
        });

        shopBtn = new KJIconTextButton(KJAssets.BUTTON_YELLOW,
                        textControl.get(KJTexts.BTN_SHOP_BASIC), getSkin(),
                        getSkin().getDrawable(KJAssets.ICON_ANANAS));
        shopBtn.alignIcon(Align.left);
        shopBtn.centerText();

        shopBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);

                KJGame game = (KJGame) mc.getGame();
                PurchaseManager pManager = game.getPurchaseManager();
                PurchaseHandler pHandler = game.getPurchaseHandler();
                if (!pManager.installed()) {
                    game.getScreen().getMenu().showWaiting(true);
                    pManager.install(pHandler, pHandler.getConfig(), true);
                }
                mc.switchMenu(MENU.SHOP_SUNS);
            }
        });

        scrollPane = new ScrollPane(currentMap);
        scrollPane.setFillParent(true);
        scrollPane.setStyle(new ScrollPaneStyle());
        scrollPane.setOverscroll(false, false);
        scrollPane.setName(getName() + "_scrollPane");
        addActor(scrollPane);

        btnCell = add(homeBtn).top().left().padLeft(25);
        add(shopBtn).expand().top().right().padRight(25);
        //        row();
        //        add();
        //        add(bugBtn).expand().top().right().padRight(25);
        //        row();
        //        add();
        //        add(testerBtn).expand().top().right().padRight(25);
        //        row();
        //        add();
        //        add(feedbackBtn).expand().top().right().padRight(25);
        row();
        add(encyBtn).bottom().left().padLeft(25);
        scrollPane.layout();

        createMaps();
        scrollX = maps.get(MAPS.TZ).getWidth() - menuControl.getWidth();

        stopIntro = new Runnable() {
            @Override
            public void run() {
                stopIntro();
            }
        };

        pack();

        eaMap = new Image(mc.getGame().assets.getDrawable(KJAssets.IMAGE_MAP_EASTAFRICA));
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (updateAnims) for (MAPS m : MAPS.values()) {
            updateAnims(m);
        }
    }

    public void setReturnName(String mapName) {
        returnMapName = mapName;
    }

    public void setMap(MAPS map) {
        if (currentMapID == map) return;

        currentMapID = map;
        currentMap = maps.get(map);

        if (map == MAPS.TZ) {
            scrollPane.setActor(currentMap);
            scrollPane.pack();
            scrollPane.setScrollX(scrollX);

            btnCell.setActor(homeBtn);
        } else {
            scrollX = scrollPane.getScrollX();
            scrollPane.setActor(currentMap);
            scrollPane.pack();

            btnCell.setActor(backBtn);
        }
        scrollPane.updateVisualScroll();
    }

    public HashMap<MAPS, Stack> createMaps() {
        createMap(MAPS.TZ);

        float w = menuControl.getWidth();
        float h = menuControl.getHeight();
        FileHandle file = Gdx.files.internal("data/ui/decoration/chapter_lock0.json");
        unlockAnimActor = menuControl.getGame().factory.createSpineActor("chapter_lock0",
                        w, h, KJAssets.ATLAS_GLOBAL_UI, file);
        unlockAnimActor.setScale(1f);
        unlockAnimActor.setTouchable(Touchable.disabled);
        unlockAnimActor.usePremAlpha(false);
        return maps;
    }

    public HashMap<MAPS, Stack> getMaps() {
        return maps;
    }

    public Stack getMap(MAPS mapID) {
        return maps.get(mapID);
    }

    public HashMap<CHAPTER, List<LevelButton>> getChapters() {
        return chapters;
    }

    public int getChapterCount() {
        return chapters.size();
    }

    public int getLevelCount() {
        int levelCount = 0;
        for (List<LevelButton> levels : chapters.values()) {
            levelCount += levels.size();
        }
        return levelCount;
    }

    public LevelButton getFirstLevel(MAPS map) {
        return firstChapterLevels.get(map);
    }

    public LevelButton getLevelButton(String name) {
        if (name == null || name.isEmpty()) return null;
        for (List<LevelButton> levelBtns : chapters.values()) {
            for (LevelButton levelBtn : levelBtns) {
                if (name.contains(levelBtn.getName())) return levelBtn;
            }
        }
        for (LevelButton chapterButton : chapterBtns.values()) {
            if (name.contains(chapterButton.getName())) return chapterButton;
        }
        return null;
    }

    public void unlockChapter(final KJCloudSave savegame) {
        List<LevelButton> chapterLevels;
        CHAPTER chapter;
        VJXOffer offer = APILoader.getOffer(ShopItem.UNLOCK_CHAPTER.name);
        chapter = CHAPTER.get(offer.getLevel());
        if (chapter == null) return;

        float delay = unlockAnimActor.getAnimDuration("unlock");
        chapterLevels = chapters.get(chapter);
        for (LevelButton lb : chapterLevels) {
            boolean finished = savegame.getBool(
                            GameStats.LVL_FINISH.name + lb.getName(),
                            false);

            lb.clearActions();
            if (!finished) {
                lb.setFinished(true);
                showLevelButton(lb, delay);
                savegame.saveLevel(lb.getName(), false, false);
            }
        }

        // unlock next chapter btn
        final LevelButton nextChapterBtn = chapterBtns
                        .get(CHAPTER.get(offer.getLevel() + 1));
        if (nextChapterBtn == null) return;
        if (nextChapterBtn.getActions().size == 0)
            showLevelButton(nextChapterBtn, delay);

        ((KJMenu) menuControl).unlock(nextChapterBtn,
                        nextChapterBtn.getLockIcon(), delay + 1f);
    }

    public void showFireworks() {
        float delay = unlockAnimActor.getAnimDuration("unlock");
        SequenceAction fireworkSequence = Actions.sequence();
        fireworkSequence.addAction(Actions.alpha(1, 0f));
        fireworkSequence.addAction(Actions.delay(delay));
        fireworkSequence.addAction(Actions.alpha(0, .5f));
        fireworkSequence.addAction(Actions.delay(.5f));
        fireworkSequence.addAction(Actions.removeActor());

        unlockAnimActor.setAnimation("unlock", false);
        unlockAnimActor.addAction(fireworkSequence);
        addActor(unlockAnimActor);
    }

    private void showLevelButton(LevelButton lb, float delay) {
        lb.setScale(.5f);
        lb.setColor(1, 1, 1, 0);
        lb.setVisible(true);
        SequenceAction sa = Actions.sequence();
        Action bounce = EJXActions.bounceIn(1.25f, .5f);
        Action alpha = Actions.alpha(1f, .5f);
        sa.addAction(Actions.delay(delay));
        sa.addAction(alpha);
        sa.addAction(bounce);
        lb.addAction(sa);
    }

    public void resetLevels() {
        for (List<LevelButton> levels : chapters.values()) {
            for (LevelButton lb : levels) {
                lb.setFinished(false);
                lb.setAvailable(false);
                lb.setVisible(lb.isChapterBegin());
            }
        }

        for (LevelButton chapterBtn : chapterBtns.values()) {
            ((KJMenu) menuControl).lock(chapterBtn, chapterBtn.getLockIcon());
            chapterBtn.setFinished(false);
            chapterBtn.setAvailable(false);
        }
    }

    private void createMap(MAPS mapID) {
        TiledMap map = menuControl.maps.get(mapID.name);
        int tilesX = map.getProperties().get("width", 0, Integer.class);
        int tilesY = map.getProperties().get("height", 0, Integer.class);
        int tilesW = map.getProperties().get("tileWidth", 0, Integer.class);
        int tilesH = map.getProperties().get("tileHeight", 0, Integer.class);
        float w = tilesX * tilesW;
        float h = tilesY * tilesH;

        Stack mapStack = new Stack();
        mapStack.setName(mapID.name + VJXString.POST_stack);

        Image mapImage = new Image(menuControl.getSkin(), mapID.name);
        mapImage.setName(mapID.name + VJXString.POST_background);
        mapStack.add(mapImage);

        Table mapElements = new Table();
        mapElements.setName("map_elements");
        mapElements.setName(mapID.name + VJXString.SEP_USCORE
                        + LAYER_map_elements);
        mapElements.setSize(w, h);
        mapStack.add(mapElements);
        mapStack.pack();

        for (MapLayer l : map.getLayers()) {
            if (l.getName().contains("chapter")) {
                createChapter(l, mapElements, mapID);
            }
        }

        createMapDeco(mapID, mapElements);
        maps.put(mapID, mapStack);
    }

    private void createChapter(MapLayer layer, Table mapElements, MAPS map) {
        int chapterNr = layer.getProperties().get("chapter", 0, Integer.class);
        CHAPTER chapter = CHAPTER.get(chapterNr);

        List<LevelButton> chapterLevels;
        if (!chapters.containsKey(chapter)) {
            chapterLevels = new ArrayList<>();
            chapters.put(chapter, chapterLevels);
        } else chapterLevels = chapters.get(chapter);

        String name = chapter.name + VJXString.SEP_USCORE + LAYER_map_level;
        Table chapterTable = new Table();
        chapterTable.setSize(mapElements.getWidth(), mapElements.getHeight());
        chapterTable.setName(name);
        createLevels(layer, chapterTable, chapterLevels, map.name);
        mapElements.addActor(chapterTable);
    }

    private void createLevels(MapLayer layer, Table layerTable,
                    List<LevelButton> chapterLevels, String mapName) {
        Skin skin = menuControl.getSkin();
        String path_white_icon = KJAssets.MAP_PATH_WHITE;

        if (layer.getObjects() != null) {
            for (MapObject o : layer.getObjects()) {
                String type = o.getProperties().get(VJXString.type, String.class);
                if (type.equals(KJString.OBJECT_lvlBtn)) {
                    LevelButton lb = createLvlButton(o.getName(),
                                    o.getProperties(), mapName);
                    layerTable.addActor(lb);
                    if (lb.starsNeeded()) {
                        String name = EJXUtil.shortNameTMX(lb.getLevelPath());
                        if (lb.isSubMap()) {
                            MAPS mapID = MAPS.get(name);
                            createMap(mapID);
                        } else {
                            chapterLevels.add(lb);
                            if (lb.isChapterBegin()) {
                                firstChapterLevels.put(MAPS.get(mapName), lb);
                            }
                        }
                        if (lb.getChapterNr() > 0)
                            chapterBtns.put(CHAPTER.get(lb.getChapterNr()), lb);
                    } else {
                        chapterLevels.add(lb);
                        if (lb.isChapterBegin()) {
                            firstChapterLevels.put(MAPS.get(mapName), lb);
                        }
                    }
                } else {
                    if (type.equals(KJString.OBJECT_lvlPath)) {
                        Image lp = createLvlPath(o.getProperties(),
                                        skin.getDrawable(path_white_icon));
                        layerTable.addActor(lp);
                        LevelButton lb = getLevelButton(lp.getName());
                        if (lb != null) {
                            lb.addPath(lp);
                            lp.setVisible(false);
                        }
                    }
                }
            }
        }
    }

    private void createMapDeco(MAPS mapID, Table mapElements) {
        TiledMap map = menuControl.maps.get(mapID.name);

        MapLayer layer = map.getLayers().get(LAYER_map_deco);
        Table decoTable = loadLayer(layer);
        decoTable.setSize(mapElements.getWidth(), mapElements.getHeight());
        decoTable.setName(mapID.name + VJXString.SEP_USCORE + LAYER_map_deco);
        mapElements.addActor(decoTable);

        layer = map.getLayers().get(LAYER_map_clouds);
        decoTable = loadLayer(layer);
        decoTable.setSize(mapElements.getWidth(), mapElements.getHeight());
        decoTable.setName(mapID.name + VJXString.SEP_USCORE + LAYER_map_clouds);
        mapElements.addActor(decoTable);
    }

    private Table loadLayer(MapLayer layer) {
        Table decoTable = new Table();
        if (layer == null) return decoTable;
        EntityFactory factory = menuControl.getGame().factory;

        if (layer.getObjects() != null) {
            for (MapObject o : layer.getObjects()) {
                String type = o.getProperties().get(VJXString.type,
                                String.class);
                switch (type) {
                    case KJString.OBJECT_mapDeco:
                        decoTable.addActor(createMapDecoObject(o));
                        break;
                    case VJXString.utility:
                        String subCategory = o.getProperties().get(
                                        VJXString.subcategory,
                                        VJXString.STR_EMPTY, String.class);
                        if (subCategory.equals("point")) {
                            float x = o.getProperties().get(SpatialProperty.x.name, 0f, Float.class);
                            float y = o.getProperties().get(SpatialProperty.y.name, 0f, Float.class);
                            factory.createWaypoint(o.getName(), new Vector2(x, y),
                                            o.getProperties());
                        }
                        if (subCategory.equals("hide")) {
                            MapProperties props = o.getProperties();
                            int w = props.get(SpatialProperty.width.name, 0, Integer.class);
                            int h = props.get(SpatialProperty.height.name, 0, Integer.class);
                            float x = props.get(SpatialProperty.x.name, 0f, Float.class);
                            float y = props.get(SpatialProperty.y.name, 0f, Float.class);
                            Pixmap overlayPM = new Pixmap(w, h, Format.RGBA8888);
                            overlayPM.setColor(Color.BLACK);
                            overlayPM.fill();
                            Texture t = new Texture(overlayPM);
                            Image i = new Image(t);
                            i.pack();
                            i.setPosition(x, 720 - y);
                            decoTable.addActor(i);
                        }
                        break;
                }
            }
        }
        return decoTable;
    }

    private LevelButton createLvlButton(String name, MapProperties props, String mapName) {
        float x = props.get(VJXString.x, float.class);
        float y = props.get(VJXString.y, float.class);
        LevelButton lb = new LevelButton(name, menuControl, props, mapName);
        lb.setPosition(x, y);
        lb.setTransform(true);
        lb.setOrigin(lb.getWidth() / 2f, lb.getHeight() / 2f);
        return lb;
    }

    private Image createLvlPath(MapProperties props, Drawable pathIcon) {
        float x = props.get(VJXString.x, float.class);
        float y = props.get(VJXString.y, float.class);
        float sx = props.get(VJXString.scaleX, float.class);
        float sy = props.get(VJXString.scaleY, float.class);
        String name = props.get("lvlBtn", String.class);
        Image img = new Image(pathIcon);
        img.setName(name + VJXString.POST_path);
        img.setTouchable(Touchable.disabled);
        img.setScale(sx, sy);
        img.setPosition(x, y);
        return img;
    }

    private SpineActor createMapDecoObject(MapObject mapObject) {
        EntityFactory factory = menuControl.getGame().factory;
        MapProperties properties = mapObject.getProperties();
        float x = properties.get(SpatialProperty.x.name, Float.class);
        float y = properties.get(SpatialProperty.y.name, Float.class);
        float sclX = properties.get(SpatialProperty.scaleX.name, Float.class);
        float sclY = properties.get(SpatialProperty.scaleY.name, Float.class);

        float w = properties.get(SpatialProperty.width.name, Float.class);
        float h = properties.get(SpatialProperty.height.name, Float.class);

        boolean flipX = properties.get(SpatialProperty.flipX.name, Boolean.class);
        boolean flipY = properties.get(SpatialProperty.flipY.name, Boolean.class);

        float r = properties.get(SpatialProperty.rotation.name, Float.class);
        String defName = properties.get(DefinitionProperty.defName.name, String.class);
        FileHandle file = Gdx.files.internal(
                        "data/ui/decoration/" + defName + ".json");
        SpineActor actor = factory.createSpineActor(mapObject.getName(), w, h,
                        KJAssets.ATLAS_GLOBAL_UI, file, properties);
        actor.setScale(sclX, sclY);
        actor.setPosition(x, y);
        actor.setAnimation("idle", true, true);
        actor.usePremAlpha(false);
        actor.flipX(flipX);
        actor.flipY(flipY);
        actor.setRotation(r);

        String waypoints = properties.get(MoveProperty.waypoints.name,
                        VJXString.STR_EMPTY, String.class);
        if (!waypoints.isEmpty()) {
            if (waypoints.contains(VJXString.ACTOR_THIS)) {
                factory.createWaypoint(actor.getName(), new Vector2(x, y),
                                properties);
                waypoints = waypoints.replace(VJXString.ACTOR_THIS,
                                actor.getName());
            }
            actor.setVisible(false);
            float animDur = actor.getAnimDuration("idle");
            boolean loop = properties.get("loop", false, Boolean.class);
            boolean disappear = properties.get("disappear", true, Boolean.class);
            float delay = properties.get("delay", 0f, Float.class);
            delay += disappear ? animDur : 0;
            actor.setVisible(!disappear);
            actor.addAction(EJXActions.followPathAction(waypoints, disappear,
                            loop, delay));
            actor.setOrigin(actor.getWidth() / 2f, actor.getHeight() / 2f);
        }

        if (actor.getName().contains("bird") || actor.getName().contains("cloud")) {
            float speedX, speedY, randomSpeed;
            speedX = properties.get("speedX", 1f, Float.class);
            speedY = properties.get("speedY", 1f, Float.class);
            randomSpeed = properties.get("randomSpeed", 0f, Float.class);
            randomSpeed = MathUtils.random(randomSpeed) - randomSpeed / 2f;
            speedX += randomSpeed;
            speedY += randomSpeed;
            properties.put("speedX", speedX);
            properties.put("speedY", speedY);
            actor.setUserObject(properties);
        }

        actor.setTouchable(Touchable.disabled);
        return actor;
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        LocalSavegame ks = g.getSave();
        int newSecrets = ks.getInt(GameStats.NEW_SECRETS.name, 0);
        if (newSecrets > 0) {
            newSecretLabel.setText(VJXString.STR_PLUS + newSecrets);
            newSecrets = 0;
        } else newSecretLabel.setText(VJXString.STR_EMPTY);

        scrollPane.setScrollX(scrollX);
        scrollPane.updateVisualScroll();

        if (returnMapName != null) {
            setMap(MAPS.get(returnMapName));
            returnMapName = null;
        } else {
            if (!zoomed) startIntro();
            else stopIntro();
        }

        for (Collection<LevelButton> levels : getChapters().values()) {
            for (LevelButton levelButton : levels) {
                levelButton.updateButton(g);
            }
        }

        for (LevelButton lb : chapterBtns.values()) {
            lb.updateButton(g);
        }

        if (!updateAnims) {
            for (MAPS m : MAPS.values()) {
                maps.get(m).pack();
            }
            updateAnims = true;
        }
    }

    @Override
    public void exited(EJXGame g, MenuStage mc) {
        super.exited(g, mc);
        scrollX = scrollPane.getScrollX();
        scrollPane.clearActions();
        currentMap.clearActions();
        eaMap.clearActions();
    }

    @Override
    public void enterCallback() {
        if (fading) stopIntro();
    }

    private void updateAnims(MAPS mapID) {
        Stack map = maps.get(mapID);

        Table elements = map.findActor(mapID.name + VJXString.SEP_USCORE + LAYER_map_deco);
        updateLayer(elements);

        elements = map.findActor(mapID.name + VJXString.SEP_USCORE + LAYER_map_clouds);
        updateLayer(elements);
    }

    private void updateLayer(Table elements) {
        SnapshotArray<Actor> children = elements.getChildren();
        float speedX, speedY, dirX, dirY;
        for (Actor a : children) {
            if (a.getUserObject() != null) {
                MapProperties props = (MapProperties) a.getUserObject();
                speedX = props.get("speedX", 1f, float.class);
                speedY = props.get("speedY", 1f, float.class);
                dirX = props.get("dirX", 1f, float.class);
                dirY = props.get("dirY", 1f, float.class);
                a.setPosition(a.getX() + speedX * dirX, a.getY() + speedY * dirY);
            }

            if (a.getX() < -a.getWidth())
                a.setPosition(elements.getWidth() + a.getWidth(), a.getY());
            if (a.getX() > elements.getWidth() + a.getWidth())
                a.setPosition(-a.getWidth(), a.getY());

            if (a.getY() < -a.getHeight())
                a.setPosition(a.getX(), elements.getHeight() + a.getHeight());
            if (a.getY() > elements.getHeight() + getHeight())
                a.setPosition(a.getX(), -a.getHeight());
        }
    }

    private void startIntro() {
        float duration = 2f;
        zoomed = true;

        homeBtn.setVisible(false);
        homeBtn.setTouchable(Touchable.disabled);
        shopBtn.setVisible(false);
        shopBtn.setTouchable(Touchable.disabled);
        encyBtn.setVisible(false);
        encyBtn.setTouchable(Touchable.disabled);

        stopIntroAction = Actions.run(stopIntro);
        currentMap.clearActions();
        currentMap.addAction(Actions.sequence(Actions.alpha(1f, 0f),
                        Actions.delay(duration + 1.5f), stopIntroAction));

        currentMap.findActor(currentMapID.name + VJXString.SEP_USCORE
                        + LAYER_map_elements).setVisible(false);

        eaMap.setPosition(0, 0);
        eaMap.getColor().a = 1;
        eaMap.addAction(Actions.sequence(
                        Actions.parallel(Actions.scaleTo(1f, 1f, 0f),
                                        Actions.moveTo(0f, 0f)),
                        Actions.delay(.5f),
                        Actions.parallel(
                                        Actions.scaleTo(4f, 4f, duration,
                                                        Interpolation.fade),
                                        Actions.moveTo(-3100f, -1050f, duration,
                                                        Interpolation.fade)),
                        Actions.fadeOut(1f)));
        eaMap.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                stopIntro();
            }
        });
        addActor(eaMap);
        fading = true;
    }

    private void stopIntro() {
        scrollPane.clearActions();

        eaMap.clearActions();
        eaMap.getColor().a = 0;
        eaMap.remove();

        currentMap.clearActions();
        currentMap.addAction(Actions.alpha(1));

        currentMap.findActor(currentMapID.name + VJXString.SEP_USCORE
                        + LAYER_map_elements).setVisible(true);

        shopBtn.setTouchable(Touchable.enabled);
        shopBtn.setVisible(true);

        encyBtn.setTouchable(Touchable.enabled);
        encyBtn.setVisible(true);

        homeBtn.setTouchable(Touchable.enabled);
        homeBtn.setVisible(true);
        fading = false;
    }

    @Override
    public void updateLanguage(TextControl texts) {
        encyBtn.setText(texts.get(KJTexts.BTN_ENCY));
        homeBtn.setText(texts.get(KJTexts.BTN_HOME));
        shopBtn.setText(texts.get(KJTexts.BTN_SHOP_BASIC));
        feedbackBtn.setText(texts.get(KJTexts.BTN_FEEDBACK));
        bugBtn.setText(texts.get(KJTexts.BTN_REPORT_BUG));
        testerBtn.setText(texts.get(KJTexts.BTN_TESTER));
    }
}