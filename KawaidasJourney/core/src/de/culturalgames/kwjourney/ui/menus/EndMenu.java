package de.culturalgames.kwjourney.ui.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.ui.SubMenu;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.video.VideoSequence;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.TextButton;

public class EndMenu extends SubMenu {

    private final KJMenu kjMenu;

    private TextButton outro0Btn;
    private TextButton outro1Btn;
    private TextButton outro2Btn;
    private TextButton outro3Btn;

    private VideoSequence currentVideo;
    private VideoSequence outro0;
    private VideoSequence outro1;
    private VideoSequence outro2;
    private VideoSequence outro3;

    public EndMenu(MenuStage menu) {
        super(menu, "ui_menu_end", TextControl.getInstance()
                        .get(KJTexts.MENU_END_HOME_IS_TITLE));
        kjMenu = (KJMenu) menu;
        setBackground(menu.getGame().assets.getDrawable(KJAssets.IMAGE_MENU_START));
    }

    @Override
    public void initContent() {
        TextControl textControl = TextControl.getInstance();
        final Runnable onCloseOutro = new Runnable() {

            @Override
            public void run() {
                kjMenu.switchMenu(MENU.CREDITS.id, false);
                kjMenu.getScreen().getLoader().unloadRessource(
                                currentVideo.getName());
            }
        };

        FileHandle file = Gdx.files.internal("video/outro/outro0/outro0.json");
        if (file != null && file.exists()) {
            outro0 = new VideoSequence(KJAssets.VIDEO_OUTRO0,
                            textControl.get(KJTexts.VIDEO_OUTRO0_TITLE),
                            file);
        }

        file = Gdx.files.internal("video/outro/outro1/outro1.json");
        if (file != null && file.exists()) {
            outro1 = new VideoSequence(KJAssets.VIDEO_OUTRO1,
                            textControl.get(KJTexts.VIDEO_OUTRO1_TITLE),
                            file);
        }

        file = Gdx.files.internal("video/outro/outro2/outro2.json");
        if (file != null && file.exists()) {
            outro2 = new VideoSequence(KJAssets.VIDEO_OUTRO2,
                            textControl.get(KJTexts.VIDEO_OUTRO2_TITLE),
                            file);
        }

        file = Gdx.files.internal("video/outro/outro3/outro3.json");
        if (file != null && file.exists()) {
            outro3 = new VideoSequence(KJAssets.VIDEO_OUTRO3,
                            textControl.get(KJTexts.VIDEO_OUTRO3_TITLE),
                            file);
        }

        ClickListener outro0Listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                initVideo(outro0, KJAssets.MUSIC_OUTRO0, onCloseOutro);
            }
        };
        outro0Btn = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        textControl.get(KJTexts.BTN_OUTRO0), getSkin(),
                        outro0Listener);

        ClickListener outro1Listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                initVideo(outro1, KJAssets.MUSIC_OUTRO1, onCloseOutro);
            }
        };
        outro1Btn = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        textControl.get(KJTexts.BTN_OUTRO1), getSkin(),
                        outro1Listener);

        ClickListener outro2Listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                initVideo(outro2, KJAssets.MUSIC_OUTRO2, onCloseOutro);
            }
        };
        outro2Btn = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        textControl.get(KJTexts.BTN_OUTRO2), getSkin(),
                        outro2Listener);

        ClickListener outro3Listener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                initVideo(outro3, KJAssets.MUSIC_OUTRO3, onCloseOutro);
            }
        };
        outro3Btn = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        textControl.get(KJTexts.BTN_OUTRO3), getSkin(),
                        outro3Listener);

        Table t1 = new Table();
        t1.add(outro0Btn).expand();
        t1.add(outro1Btn).expand();
        t1.row();
        t1.add(outro2Btn).expand();
        t1.add(outro3Btn).expand();

        content.add(t1).expand().fill();
        contentLayer.add(content).expand().fill().padTop(header.getHeight());

        showHelp(false);
        backBtn.remove();
    }

    private void initVideo(VideoSequence video, final String music,
                    final Runnable onCloseOutro) {
        currentVideo = video;

        EJXGame g = kjMenu.getGame();
        FileHandle file = video.getFile();
        g.assets.addResource(video.getName(),
                        new AssetDescriptor<>(
                                        file.pathWithoutExtension()
                                        + ".atlas",
                                        TextureAtlas.class));
        g.assets.finishLoading();

        VideoMenu videoMenu = (VideoMenu) kjMenu.getMenu(MENU.VIDEO.id);
        videoMenu.startVideo(video, music, onCloseOutro);
        videoMenu.initMonkey();

        kjMenu.switchMenu(MENU.VIDEO.id, false);
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        menuControl.playMusic(KJAssets.MUSIC_END_SCREEN);
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.MENU_END_HOME_IS_TITLE));

        outro0Btn.setText(texts.get(KJTexts.BTN_OUTRO0));
        outro1Btn.setText(texts.get(KJTexts.BTN_OUTRO1));
        outro2Btn.setText(texts.get(KJTexts.BTN_OUTRO2));
        outro3Btn.setText(texts.get(KJTexts.BTN_OUTRO3));

        outro0.setTitle(texts.get(KJTexts.VIDEO_OUTRO0_TITLE));
        outro1.setTitle(texts.get(KJTexts.VIDEO_OUTRO1_TITLE));
        outro2.setTitle(texts.get(KJTexts.VIDEO_OUTRO2_TITLE));
        outro3.setTitle(texts.get(KJTexts.VIDEO_OUTRO3_TITLE));
    }
}