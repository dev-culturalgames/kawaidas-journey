package de.culturalgames.kwjourney.ui.menus;

import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.ui.ShopMenu;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.controllers.TextControl;

public class ShopMenuSkins extends ShopMenu {

    public ShopMenuSkins(KJMenu mc) {
        super(mc, "ui_menu_shop_skins",
                        TextControl.getInstance().get(KJTexts.MENU_SHOP_SKIN_TITLE),
                        mc.mapSkinShop);
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.MENU_SHOP_SKIN_TITLE));
    }
}