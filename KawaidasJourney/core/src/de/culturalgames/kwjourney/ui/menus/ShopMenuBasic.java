package de.culturalgames.kwjourney.ui.menus;

import com.badlogic.gdx.pay.PurchaseManager;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJGame;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.ui.ShopMenu;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.widgets.TextButton;

public class ShopMenuBasic extends ShopMenu {

    private TextButton skinShopBtn;
    private TextButton primeShopBtn;

    public ShopMenuBasic(final KJMenu mc) {
        super(mc, "ui_menu_shop_basic",
                        TextControl.getInstance().get(KJTexts.MENU_SHOP_BASIC_TITLE),
                        mc.mapShop);
        Table t = new Table();

        primeShopBtn = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        TextControl.getInstance().get(KJTexts.BTN_SHOP_PRIME),
                        getSkin(), new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                keeper.setVisible(false);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                mc.switchMenu(MENU.SHOP_PRIME);
                //                mc.showMessageToUser(
                //                                textControl.getLine(KTxtControl.LABEL_INFO_TEST_SHOP),
                //                                textControl.getLine(KTxtControl.TEXT_INFO_TEST_SHOP));
            }
        });
        t.add(primeShopBtn);

        skinShopBtn = EJXSkin.createTButton(KJAssets.BUTTON_BLUE,
                        TextControl.getInstance().get(KJTexts.BTN_SHOP_SKIN),
                        getSkin(), new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                keeper.setVisible(false);
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                mc.switchMenu(MENU.SHOP_SKINS);
            }
        });

        t.add(skinShopBtn);

        overlayLayer.row();
        overlayLayer.add(t).expandX().bottom().left().pad(0, 20, 5, 0);

        keeper.setAnimation("idle_noPlant", true);
    }

    public void setPrimeAvailable(boolean available) {
        primeShopBtn.setEnabled(available);
    }

    @Override
    public void entered(EJXGame g, MenuStage mc) {
        super.entered(g, mc);
        KJGame game = (KJGame) g;
        PurchaseManager pManager = game.getPurchaseManager();
        primeShopBtn.setEnabled(pManager.installed());
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.MENU_SHOP_BASIC_TITLE));
        primeShopBtn.setText(texts.get(KJTexts.BTN_SHOP_PRIME));
        skinShopBtn.setText(texts.get(KJTexts.BTN_SHOP_SKIN));
    }
}