package de.culturalgames.kwjourney.ui.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.esotericsoftware.spine.AnimationState.AnimationStateListener;
import com.esotericsoftware.spine.AnimationState.TrackEntry;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Event;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.venjinx.ejx.EJXGame;
import de.venjinx.ejx.animation.SpineActor;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.video.VideoSequence;
import de.venjinx.vjx.ui.EJXSkin;
import de.venjinx.vjx.ui.MenuTable;
import de.venjinx.vjx.ui.widgets.EJXLabel;

public class VideoMenu extends MenuTable implements AnimationStateListener {

    private final KJMenu kjMenu;

    private EJXLabel title;
    private VideoSequence video;

    private SpineActor monkeyActor;

    private Button closeVideoButton;
    private Runnable onClose;
    private Runnable onComplete;

    public VideoMenu(MenuStage menu) {
        super(menu, "ui_menu_video");

        kjMenu = (KJMenu) menu;
        Table content = new Table();

        closeVideoButton = EJXSkin.createTButton(KJAssets.BUTTON_CLOSE, getSkin());
        closeVideoButton.setPosition(menu.getWidth() - 153, menu.getHeight() - 128);
        closeVideoButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                kjMenu.getGame().sfx.playDynSFX(KJAssets.SFX_BACK);
                if (onClose != null) onClose.run();
            }
        });

        title = new EJXLabel(getSkin(), VJXString.STR_EMPTY, EJXFont.KANKIN_60);

        add(content).expand().fill();
    }

    public void startVideo(VideoSequence video, String music, Runnable onClose) {
        removeVideo(false);

        this.onClose = onClose;
        this.video = video;
        video.init(kjMenu.getGame(), kjMenu.getViewport().getWorldWidth(),
                        kjMenu.getViewport().getWorldHeight(), false);
        video.getActor().usePremAlpha(true);
        addActor(video.getActor());

        kjMenu.playMusic(music);

        title.remove();
        if (!video.getTitle().isEmpty()) {
            title.setText(video.getTitle());
            title.pack();
            title.setPosition(menuControl.getWidth() / 2 - title.getWidth() / 2,
                            menuControl.getHeight() - title.getHeight() - 25);
            addActor(title);
        }

        addActor(closeVideoButton);
    }

    public void initMonkey() {
        closeVideoButton.remove();
        EJXGame game = kjMenu.getGame();
        FileHandle file = Gdx.files
                        .internal("objects/character/monkey0/monkey0.json");
        monkeyActor = game.factory.createSpineActor("monkey", 1280,
                        720, "global_character", file, null);
        monkeyActor.setAnimation(video.getName(), true);
        monkeyActor.updateSkeletonScale(1280, 720);
        monkeyActor.setSkin(game.getPlayer().getEntity().getSkin());

        Bone kawaidaBone = video.getActor().getSkeleton().findBone("kawaida");
        monkeyActor.setPosition(kawaidaBone.getX(), kawaidaBone.getY());
        monkeyActor.setScale(kawaidaBone.getScaleX(), kawaidaBone.getScaleY());
        addActor(monkeyActor);
        addActor(closeVideoButton);
    }

    private void removeVideo(boolean fade) {
        if (video == null || video.getActor() == null) return;
        if (fade) {
            video.getActor().addAction(Actions.sequence(Actions.alpha(0f, 1f),
                            Actions.removeActor()));
            if (video.getMonkey() != null)
                video.getMonkey().addAction(Actions.sequence(
                                Actions.alpha(0f, 1f), Actions.removeActor()));
            ;
        } else {
            video.getActor().remove();
            if (video.getMonkey() != null) video.getMonkey().remove();
        }

        video = null;
        closeVideoButton.remove();
    }

    @Override
    public void enterCallback() {
        kjMenu.getGame().sfx.playDynSFX(KJAssets.SFX_BACK);
        if (onClose != null) onClose.run();
    }

    public void setOnCompleteRunnable(Runnable onComplete) {
        this.onComplete = onComplete;
    }

    @Override
    public void start(TrackEntry entry) {
    }

    @Override
    public void interrupt(TrackEntry entry) {
    }

    @Override
    public void end(TrackEntry entry) {
    }

    @Override
    public void dispose(TrackEntry entry) {
    }

    @Override
    public void complete(TrackEntry entry) {
        if (onComplete != null) onComplete.run();
    }

    @Override
    public void event(TrackEntry entry, Event event) {
    }
}