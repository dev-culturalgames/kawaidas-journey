package de.culturalgames.kwjourney.ui.menus;

import de.culturalgames.kwjourney.KJMenu;
import de.venjinx.vjx.ui.MenuTable;

public class EmptyMenu extends MenuTable {
    public EmptyMenu(final KJMenu mc) {
        super(mc, "ui_menu_empty");
    }
}