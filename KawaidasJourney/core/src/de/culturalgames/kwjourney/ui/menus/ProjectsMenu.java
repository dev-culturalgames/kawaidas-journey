package de.culturalgames.kwjourney.ui.menus;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;

import de.culturalgames.kwjourney.KJAssets;
import de.culturalgames.kwjourney.KJMenu;
import de.culturalgames.kwjourney.ui.ScrollSubMenu;
import de.culturalgames.kwjourney.ui.widgets.ProjectButton;
import de.culturalgames.kwjourney.util.KJTexts;
import de.culturalgames.kwjourney.util.KJTypes.KJString;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.EJXUtil;
import de.venjinx.ejx.util.LevelUtil;

public class ProjectsMenu extends ScrollSubMenu {

    private static final String buttonText = "buttonText";
    private static final String hasImage = "hasImage";
    public static final String HELP_proj = "projects";
    public static final String POST_proj = "_proj";

    private KJMenu kjMenu;
    private Array<ProjectButton> buttons;

    public ProjectsMenu(MenuStage menu) {
        super(menu, "ui_menu_projects", TextControl.getInstance()
                        .get(KJTexts.MENU_PROJECTS_TITLE));
        kjMenu = (KJMenu) menu;
        addHelpBtnListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                audioControl.playDynSFX(KJAssets.SFX_CLICK);
                kjMenu.showHelp(HELP_proj);
            }
        });
    }

    @Override
    protected void initScrollContent() {
        buttons = new Array<>();
        for (int i = 0; i < LevelUtil.getProjectDefs().size(); i++) {
            for (final MapProperties mp : LevelUtil.getProjectDefs()) {
                int position = mp.get("position", Integer.class);
                if (i != position) continue;
                final String name = mp.get(VJXString.name, String.class);
                String title = mp.get(buttonText, String.class);
                if (title == null) title = TextControl.getInstance()
                                .get(VJXString.PRE_but + POST_proj
                                                + VJXString.SEP_USCORE + name);
                String source = mp.get(VJXString.source, String.class);
                Drawable d = null;
                if (mp.get(hasImage, true, boolean.class)) {
                    d = getSkin().getDrawable(KJString.PATH_icons_projects
                                    + EJXUtil.shortNameImage(source));
                }
                ClickListener listener = new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x,
                                    float y) {
                        audioControl.playDynSFX(KJAssets.SFX_CLICK);
                        kjMenu.showProject(name, mp);
                    };
                };
                ProjectButton b = new ProjectButton(getSkin(), title, d, listener);
                b.setName(KJTexts.PROJECT_ + name);
                buttons.add(b);
                scrollContent.add(b)
                .pad(0, 15, 0, 15);
            }
        }
        contentLayer.add(content).expand().fill().padTop(header.getHeight());
    }

    @Override
    public void updateLanguage(TextControl texts) {
        setTitle(texts.get(KJTexts.MENU_PROJECTS_TITLE));
        for (ProjectButton projectButton : buttons) {
            projectButton.setTitle(texts
                            .get(projectButton.getName() + TextControl._TITLE));
        }
    }
}