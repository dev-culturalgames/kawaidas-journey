package de.culturalgames.kwjourney.ui;

import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.venjinx.ejx.stages.MenuStage;

public abstract class ScrollSubMenu extends SubMenu {

    protected ScrollPane scroll;
    protected Table scrollContent;

    public ScrollSubMenu(MenuStage menu, String name, String title) {
        super(menu, name, title);
    }

    @Override
    protected void initContent() {
        scrollContent = new Table();
        scrollContent.setName(getName() + "_scrollTable");

        scroll = new ScrollPane(scrollContent);
        scroll.setStyle(new ScrollPaneStyle());
        scroll.setOverscroll(false, false);
        scroll.setName(getName() + "_scrollPane");
        initScrollContent();

        if (this instanceof ShopMenu)
            content.add(scroll).width(menuControl.getWidth())
            .height(menuControl.getHeight());
        else content.add(scroll);
    }

    protected abstract void initScrollContent();
}