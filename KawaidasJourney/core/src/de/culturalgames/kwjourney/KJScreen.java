package de.culturalgames.kwjourney;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import de.culturalgames.kwjourney.KJGame.GameStats;
import de.culturalgames.kwjourney.KJMenu.MENU;
import de.culturalgames.kwjourney.KJMenu.POPUP;
import de.culturalgames.kwjourney.api.APILoader;
import de.culturalgames.kwjourney.content.Secrets;
import de.culturalgames.kwjourney.save.KJCloudSave;
import de.culturalgames.kwjourney.ui.menus.LoadingMenu;
import de.culturalgames.kwjourney.ui.menus.SettingsMenu;
import de.culturalgames.kwjourney.util.KJTexts;
import de.venjinx.ejx.EJXGameScreen;
import de.venjinx.ejx.controllers.TextControl;
import de.venjinx.ejx.save.CloudSaveHandler;
import de.venjinx.ejx.stages.MenuStage;
import de.venjinx.ejx.util.EJXFont;
import de.venjinx.ejx.util.EJXGameService;
import de.venjinx.ejx.util.EJXGameService.GameServiceListener;
import de.venjinx.ejx.util.EJXTimer;
import de.venjinx.ejx.util.EJXTypes.VJXString;
import de.venjinx.ejx.util.VJXLogger;
import de.venjinx.ejx.util.VJXLogger.LogCategory;

public class KJScreen extends EJXGameScreen implements GameServiceListener {

    private CloudSaveHandler cloudHandler;
    private EJXGameService gameService;
    private KJMenu kjMenu;

    private float timePassed = 0f;
    private float delay = 1.5f;
    private boolean idle = true;

    private String lvlPath = VJXString.STR_EMPTY;
    private boolean internal = true;

    private int loadingMark = EJXTimer.getMark();
    private boolean loadingMenu = false;
    private boolean loadingLevel = false;
    private boolean unloadingLevel = false;
    private boolean levelLoadQueued = false;
    private boolean loadOnConnected = false;

    private Drawable unloadingBG;

    private Runnable activateCloud = new Runnable() {

        @Override
        public void run() {
            gameService.connectService();
        }
    };

    private Runnable cancelCloud = new Runnable() {

        @Override
        public void run() {
            onMenuLoaded();
        }
    };

    public KJScreen(KJGame game) {
        super(game);
        cloudHandler = game.getCloudHandler();
        gameService = game.getGameService();

        menu = kjMenu = new KJMenu(game);
        level = new KJLevel(game, kjMenu);

        customStages.add(level);
        customStages.add(menu);

        levelRenderer.setLevel(level);

        input.setPlayerInput(new KJPlayerControl());
        input.init(game, menu, level);

        ((InputMultiplexer) Gdx.input.getInputProcessor()).addProcessor(kjMenu);

        loader.setLoadingUI(menu.getMenu(MENU.LOADING.id));
    }

    public void setLoadOnConnected(boolean loadOnConnected) {
        this.loadOnConnected = loadOnConnected;
    }

    @Override
    public void pause() {
        super.pause();
        game.sfx.pauseEntitySounds();
        menu.pauseMusic();

        if (!game.isDevMode() && level.isActive())
            menu.showPopup(POPUP.PAUSE.id, false);
    }

    @Override
    public void resume() {
        super.resume();
        game.sfx.resumeEntitySounds();
        if (menu.getCurrentMenuId() == MENU.GAME.id) {
            ((KJLevel) level).playMusic();
        } else {
            menu.resumeMusic();
        }
    }

    @Override
    public void postProcess(float delta) {
        timePassed += delta;

        if (loadingMenu) {
            loadMenu();
        } else if (loadingLevel) {
            loadLevel();
        }

        if (unloadingLevel && !loader.isLoading()) {
            if (!level.isInitialized()) {
                VJXLogger.log(LogCategory.LOAD, "Screen level unloaded.");
                input.getPlayerInput().setIngameControl(false);

                unloadingLevel = false;
                game.sfx.playMusic(KJAssets.MUSIC_MAIN);
                loader.addAction(Actions.sequence(Actions.delay(.5f),
                                Actions.run(new Runnable() {
                                    @Override
                                    public void run() {
                                        game.assets.unloadResource(MenuStage.img_unloading_bg_lvl);
                                        game.assets.finishLoading();
                                        loader.setActive(false);
                                    }
                                })));
            }
        }
    }

    @Override
    public void startLoadMenu() {
        EJXTimer.setMark(loadingMark);
        timePassed = 0f;
        loadingMenu = true;

        loader.setActive(true);
        menu.setRunBackground(false, .5f);
    }

    private void loadMenu() {
        if (idle) {
            if (!menu.isInitialized() && !menu.dataIsPrepared()) {
                VJXLogger.logIncr(LogCategory.LOAD, "Screen start loading menu...");
                VJXLogger.logIncr(LogCategory.LOAD, "Screen start preparing menu data...");
                loader.loadGlobalDefs();
                Secrets.loadSecretCategoryDefs();
                Secrets.loadSecretDefs();
                APILoader.loadIAPShopDefs();
                loader.loadInterface(new KJSkin(new EJXFont()));
                idle = false;
                VJXLogger.logDecr(LogCategory.LOAD,
                                "Screen prepared menu. Time: "
                                                + EJXTimer.timeSince(loadingMark) + "ms.");
                return;
            }

            if (menu.isInitialized() && timePassed >= delay) {
                VJXLogger.logDecr(LogCategory.LOAD, "Screen loaded menu. Took: "
                                + EJXTimer.timeSince(loadingMark) + "ms.");
                setLoadOnConnected(true);
                idle = true;
                loadingMenu = false;
                menu.setActive(true);

                if (gameService.isAutoConnect()) {
                    gameService.connectService();
                } else {
                    KJCloudSave saveGame = (KJCloudSave) cloudHandler.getSave();
                    if (saveGame.getBool(GameStats.GAME_FIRST_START.name, true)) {
                        game.resetSave();
                        game.saveSettings();

                        if (cloudHandler.isAvailable()) {
                            TextControl texts = TextControl.getInstance();
                            getMenu(KJMenu.class).showConfirm(texts
                                            .get(KJTexts.CONFIRM_CLOUD_TITLE),
                                            texts.get(KJTexts.CONFIRM_CLOUD_TEXT),
                                            activateCloud, cancelCloud);
                            return;
                        }
                    }
                    onMenuLoaded();
                }
                return;
            }
        } else {
            if (menu.dataIsPrepared()) {
                loader.loadStage(menu);
                idle = true;
            }
        }
    }

    @Override
    public void onMenuLoaded() {
        loader.addAction(Actions.sequence(Actions.delay(.5f),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                loader.setActive(false);
                            }
                        })));

        game.gameLoaded(levelLoadQueued);
    }

    @Override
    public void restartLevel() {
        loadLevel(lvlPath, internal);
    }

    @Override
    public void loadLevel(String path, boolean internal) {
        this.internal = internal;
        lvlPath = path;

        if (loadingMenu) {
            levelLoadQueued = true;
            return;
        }

        startLoadLevel();
    }

    public void startLoadLevel() {
        VJXLogger.logIncr(LogCategory.LOAD,
                        "Screen load level: " + level.getLevelName());
        EJXTimer.setMark(loadingMark);
        timePassed = 0f;
        loadingLevel = true;
        levelLoadQueued = false;

        kjMenu.hidePopup(false);
        kjMenu.stopMusic();
        kjMenu.switchMenu(MENU.LOADING.id, kjMenu.getCurrentMenuId() != MENU.GAME.id);

        level.reset();

        loader.setActive(true);
    }

    private void loadLevel() {
        if (idle) {
            if (!level.isInitialized() && !level.dataIsPrepared()) {
                timePassed = 0;
                idle = false;

                loader.loadMap(lvlPath, internal);
                VJXLogger.log(LogCategory.LOAD,
                                "Screen level map loaded. Took: "
                                                + EJXTimer.timeSince(loadingMark)
                                                + "ms.");
                return;
            }

            if (level.isInitialized()) {
                VJXLogger.logDecr(LogCategory.LOAD, "Screen level: "
                                + level.getLevelName() + " loaded. Took: "
                                + EJXTimer.timeSince(loadingMark) + "ms.");
                loadingLevel = false;
            }
        } else {
            if (level.dataIsPrepared()) {
                idle = true;
                loader.loadStage(level);
                loader.setActive(true);
                return;
            }
        }
    }

    @Override
    public void startLevel() {
        long loadingTime = System.currentTimeMillis();
        VJXLogger.log(LogCategory.LOAD, "Screen start level.");
        level.start();

        loader.setActive(false);
        VJXLogger.logDecr(LogCategory.LOAD, "Screen started level. Took: "
                        + (System.currentTimeMillis() - loadingTime) + "ms.");
        VJXLogger.log(LogCategory.LOAD, "");
    }

    @Override
    public void unloadLevel(final boolean isEnd) {
        menu.hidePopup(false);
        menu.stopMusic();
        unloadingLevel = true;

        game.assets.unloadResource(MenuStage.img_loading_bg_lvl);
        game.assets.addResource(MenuStage.img_unloading_bg_lvl,
                        new AssetDescriptor<>(
                                        KJAssets.PATH_IMAGE_UNLOADING_DFLT,
                                        Texture.class));
        game.assets.finishLoading();
        unloadingBG = game.assets.getDrawable(MenuStage.img_unloading_bg_lvl);

        kjMenu.switchMenu(MENU.LOADING.id, false);
        LoadingMenu lMenu = (LoadingMenu) menu.getMenu(MENU.LOADING.id);
        lMenu.setTitle(VJXString.STR_EMPTY, VJXString.STR_EMPTY,
                        VJXString.STR_EMPTY, unloadingBG);
        lMenu.startUnloading(false);

        level.getRoot().clearActions();
        level.setActive(false, .1f);

        loader.setActive(true);
        loader.addAction(Actions.sequence(
                        Actions.delay(1.5f),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                loader.unloadStage(level);
                                kjMenu.restoreUI();
                            }
                        })));
        game.getPlayer().clearCurrentLevelProgress();
    }

    @Override
    public void onConnected() {
        ((SettingsMenu) menu.getMenu(MENU.SETT.id)).updateConnectButton();

        if (loadOnConnected) {
            cloudHandler.loadGame();
            loadOnConnected = false;
        }
    }

    @Override
    public void onDisconnected() {
        ((SettingsMenu) menu.getMenu(MENU.SETT.id)).updateConnectButton();

        if (menu.getCurrentMenuId() == -1) {
            onMenuLoaded();
        }
    }

    @Override
    public void onSaved() {
    }
}