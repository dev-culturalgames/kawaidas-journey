package de.kawaida.workshops.tza.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import de.kawaida.workshops.tza.WorkshopGameTZA;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration lwjglCfg = new LwjglApplicationConfiguration();

        lwjglCfg.title = "Kawaida - Game Workshop Tansania";
        lwjglCfg.width = 960;
        lwjglCfg.height = 540;
        lwjglCfg.foregroundFPS = 60;

        // Start Game
        new LwjglApplication(new WorkshopGameTZA(), lwjglCfg);
    }
}