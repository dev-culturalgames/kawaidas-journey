package de.kawaida.workshops.tza.config;

import com.badlogic.gdx.math.Vector2;

public class GameCfg {
	
	public static Vector2 RES = new Vector2(960f, 540f);
	public static Vector2 APP_RES = new Vector2(960f, 540f);
	public static Vector2 SCALE = new Vector2(1f, 1f);
	
	public static int FPS = 60;
	public static float ASPECT = (float) RES.x / RES.y;
	
	public static String printCfg() {
		String str = "";
		str += "Resolution    : " + RES + "\n";
		str += "App Resolution: " + APP_RES + "\n";
		str += "Scale         : " + SCALE + "\n";
		str += "Target FPS    : " + FPS + "\n";
		str += "Aspect Ratio  : " + ASPECT + "\n";
		
		return str;
	}
}