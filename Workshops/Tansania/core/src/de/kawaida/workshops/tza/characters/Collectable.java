package de.kawaida.workshops.tza.characters;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import de.kawaida.workshops.tza.config.GameCfg;

public abstract class Collectable {
	
	BodyDef bodyDef;
	Body body;
	
	Shape shape;
	FixtureDef fixDef;
	Fixture fixture;
	
	Texture tex;

	Vector2 maxSize;
	Vector2 origin;
	Vector2 center;
	Vector2 drawPos;
	Vector2 frameOffset;
	Vector2 frameSize;
	
	public int dropArea;
	
	public Collectable(World world) {
		bodyDef = new BodyDef();
		fixDef = new FixtureDef();
		
		// collectable physics 2d body
		bodyDef.position.set(0f, 0f);
		bodyDef.type = BodyType.DynamicBody;
		body = world.createBody(bodyDef);
		
		drawPos = new Vector2();
	}
	
	public Body getBody() {
		return body;
	}
	
	public Texture getTex() {
		return tex;
	}
	
	public Vector2 getDrawPosition() {
		Vector2 v = new Vector2(body.getPosition()).scl(100f);
		v.scl(1 / GameCfg.SCALE.x, 1 / GameCfg.SCALE.y);
		return drawPos.set(v).sub(tex.getWidth() / 2, tex.getHeight() / 2);
	}
	
	public abstract void update(float t);
}