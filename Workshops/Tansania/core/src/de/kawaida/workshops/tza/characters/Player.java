package de.kawaida.workshops.tza.characters;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.sun.javafx.collections.SetListenerHelper;

import de.kawaida.workshops.tza.animation.Animation;
import de.kawaida.workshops.tza.animation.AnimationSet;
import de.kawaida.workshops.tza.config.GameCfg;
import de.kawaida.workshops.tza.config.Types;
import de.kawaida.workshops.tza.config.Vars.Direction;
import de.kawaida.workshops.tza.config.Vars.Status;
import de.kawaida.workshops.tza.controllers.TextureManager;
import de.kawaida.workshops.tza.entity.EntityUserData;

public class Player {
	
	// Animation
	TextureAtlas ta;
	AnimationSet animations;
	Animation currentAnimation;
	Vector2 maxSize;
	Vector2 frameSize;
	Vector2 frameOffset;
	Vector2 targetPos;
	
	float frameW, frameH, frameOffX, frameOffY;
	
	// Physics body
	BodyDef bodyDef;
	Body body;
	PolygonShape bodySensor;
	PolygonShape bodyBound;

	Body feet;
	
	PolygonShape feetShape;
	FixtureDef fixDef;
	Fixture fixture;
	
	Vector2 origin;
	Vector2 center;
	Vector2 drawPos;
	
	// Properties
	Status status;
	Direction faceDirection;
	float moveSpeed = 4f;
	public int score = 0;
	public int life = 3;
	
	public Texture tex;
	
	public Player(World world) {
		super();
		maxSize = new Vector2(96, 96);
		frameSize = new Vector2();
		frameOffset = new Vector2();
		origin = new Vector2();
		center = new Vector2();
		drawPos = new Vector2();
		targetPos = new Vector2();
		moveDir = new Vector2();

		ta = TextureManager.loadSpriteSheet("sprites/characters/player/player_all.pack", "player_all");
		animations = new AnimationSet(ta);

		center.set(200, 200);
		origin.set(center).sub(maxSize.x / 2, maxSize.y / 2);
		
		bodyDef = new BodyDef();
		fixDef = new FixtureDef();
		
		tex = new Texture("sprites/characters/player/masai.png");
		
		// player bound
		bodyDef.position.set(center.x / 100f * GameCfg.SCALE.x, center.y / 100f * GameCfg.SCALE.y);
		bodyDef.type = BodyType.DynamicBody;
		body = world.createBody(bodyDef);
		body.setSleepingAllowed(false);

		bodyBound = new PolygonShape();
		bodyBound.setAsBox(tex.getWidth() / 200f * GameCfg.SCALE.x, tex.getHeight() / 200f * GameCfg.SCALE.y);
//		bodyBound.setAsBox(maxSize.x / 200f * GameCfg.SCALE.x, maxSize.y / 200f * GameCfg.SCALE.y);

		fixDef.shape = bodyBound;
		fixDef.filter.categoryBits = Types.PLAYER;
		fixDef.filter.maskBits = Types.SOLID | Types.COLLECTABLE;
		fixDef.isSensor = false;
		
		fixture = body.createFixture(fixDef);
		fixture.setUserData(new EntityUserData("playerBody"));
		bodyBound = (PolygonShape) body.getFixtureList().get(0).getShape();
		
		// player feet sensor
		feetShape = new PolygonShape();
		feetShape.setAsBox(.05f * GameCfg.SCALE.x, .04f * GameCfg.SCALE.y,
							new Vector2(0, -tex.getHeight() / 200f * GameCfg.SCALE.y), 0);

		fixDef.shape = feetShape;
		fixDef.filter.categoryBits = Types.PLAYER;
		fixDef.filter.maskBits = Types.SOLID;
		fixDef.isSensor = true;
		
		fixture = body.createFixture(fixDef);
		fixture.setUserData(new EntityUserData("playerFeet"));

		setStatus(Status.PLAYER_IDLE);
		setFaceDirection(Direction.RIGHT);
	}
	
	public void update(float t) {
//		animations.getCurrentAnimation().update(t);
		
		updatePosition();
	}
	
	private void updatePosition() {
		center.set(body.getPosition()).scl(100f);
		center.scl(1 / GameCfg.SCALE.x, 1 / GameCfg.SCALE.y);
		origin.set(center).sub(tex.getWidth() / 2, tex.getHeight() / 2);
		
		frameSize.set(animations.getCurrentAnimation().getFrame().getRegionWidth(),
				animations.getCurrentAnimation().getFrame().getRegionHeight());
		
//		frameOffset.set(animations.getCurrentAnimation().getFrame().offsetX,
//							maxSize.y - frameSize.y - animations.getCurrentAnimation().getFrame().offsetY);
		frameOffset.set(0, 0);

		drawPos.set(origin.x + frameOffset.x, origin.y + frameOffset.y);
		
		if (faceDirection == Direction.LEFT)
			if (center.x <= targetPos.x)
				body.setLinearVelocity(0, body.getLinearVelocity().y);
		
		if (faceDirection == Direction.RIGHT)
			if (center.x >= targetPos.x)
				body.setLinearVelocity(0, body.getLinearVelocity().y);
	}
	
	public void setStatus(Status status) {
		this.status = status;
		
		switch(status) {
			case PLAYER_IDLE:	
								animations.setAnimation("player_idle");
								break;
			case PLAYER_WALK:	
								animations.setAnimation("player_walk");
								break;
		}
//		animations.getCurrentAnimation().createAnimSensors(center, origin, body);
//		animations.getCurrentAnimation().reset();
	}
	
	private Vector2 moveDir;
	
	public void moveTo(Vector2 target) {
		moveDir.set(target).sub(center);
		targetPos.set(target);
		if (moveDir.x < 0) {
			setFaceDirection(Direction.LEFT);
			body.setLinearVelocity(-moveSpeed, body.getLinearVelocity().y);
		} else {
			setFaceDirection(Direction.RIGHT);
			body.setLinearVelocity(moveSpeed, body.getLinearVelocity().y);
		}
	}
	
	public void setFaceDirection(Direction direction) {
		faceDirection = direction;
//		animations.getCurrentAnimation().setFaceDirection(direction);
	}
	
	public Vector2 getPosition() {
		return center;
	}
	
	public Vector2 getDrawPosition() {
		return drawPos;
	}
	
	public AtlasRegion getFrame() {
		return animations.getCurrentAnimation().getFrame();
	}
	
	public Body getBody() {
		return body;
	}
	
	public float getMoveSpeed() {
		return moveSpeed;
	}
}