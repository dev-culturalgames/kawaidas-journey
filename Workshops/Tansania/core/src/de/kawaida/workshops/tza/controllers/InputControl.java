package de.kawaida.workshops.tza.controllers;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

import de.kawaida.workshops.tza.config.GameCfg;
import de.kawaida.workshops.tza.config.InputConfig;

public class InputControl implements InputProcessor {

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.A) {
			InputConfig.setKey(InputConfig.MOVE_LEFT_KEY, true);
		}
		if (keycode == Keys.D) {
			InputConfig.setKey(InputConfig.MOVE_RIGHT_KEY, true);
		}
		if (keycode == Keys.Y) {
			InputConfig.setKey(InputConfig.ACKNOWLEDGE, true);
		}
		if (keycode == Keys.N) {
			InputConfig.setKey(InputConfig.DECLINE, true);
		}
		if (keycode == Keys.SPACE) {
			InputConfig.setKey(InputConfig.JUMP_KEY, true);
		}
		if (keycode == Keys.SHIFT_LEFT) {
			InputConfig.setKey(InputConfig.COLOR_KEY, true);
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.A) {
			InputConfig.setKey(InputConfig.MOVE_LEFT_KEY, false);
		}
		if (keycode == Keys.D) {
			InputConfig.setKey(InputConfig.MOVE_RIGHT_KEY, false);
		}
		if (keycode == Keys.Y) {
			InputConfig.setKey(InputConfig.ACKNOWLEDGE, false);
		}
		if (keycode == Keys.N) {
			InputConfig.setKey(InputConfig.DECLINE, false);
		}
		if (keycode == Keys.SPACE) {
			InputConfig.setKey(InputConfig.JUMP_KEY, false);
		}
		if (keycode == Keys.SHIFT_LEFT) {
			InputConfig.setKey(InputConfig.COLOR_KEY, false);
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		InputConfig.setKey(InputConfig.TOUCH, true);
		InputConfig.touchPos.set(screenX, (GameCfg.RES.y - screenY));
		InputConfig.touchPos.scl(1 / GameCfg.SCALE.x, 1 / GameCfg.SCALE.y);
		
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		InputConfig.setKey(InputConfig.TOUCH, false);
		
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}