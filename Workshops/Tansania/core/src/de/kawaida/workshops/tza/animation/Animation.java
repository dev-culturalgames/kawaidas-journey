package de.kawaida.workshops.tza.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import de.kawaida.workshops.tza.config.Types;
import de.kawaida.workshops.tza.config.Vars.Direction;
import de.kawaida.workshops.tza.entity.EntityUserData;

public class Animation {
	
	private String name;
	private Vector2 maxFrameSize;
	private AtlasRegion[] frames;
	private Fixture[] fixtures;
	private float time;
	private float delay;
	private int frameCount;
	private int startFrame, endFrame;
	private int currentFrame;
	private int loopStart, loopEnd;
	private int timesPlayed;
	private boolean forward;
	
	public Animation() {
		maxFrameSize = new Vector2();
		delay = 1 / 5f;
		frames = new AtlasRegion[0];
		fixtures = new Fixture[0];
	}
	
	public Animation(String name) {
		this(new AtlasRegion[0], 1 / 5, name);
	}

	public Animation(AtlasRegion[] frames, String name) {
		this(frames, 1 / 5f, name);
	}
	
	public Animation(AtlasRegion[] frames, float delay, String name) {
		maxFrameSize = new Vector2();
		setFrames(frames, delay);
		this.name = name;
	}
	
	public void setFrames(AtlasRegion[] frames, float delay) {
		this.delay = delay;
		this.frames = frames;

		float w, h;
		for (int i = startFrame; i < endFrame; i++) {
			w = frames[i].getRegionWidth();
			h = frames[i].getRegionHeight();
			
			maxFrameSize.x = w > maxFrameSize.x ? w : maxFrameSize.x;
			maxFrameSize.y = h > maxFrameSize.y ? h : maxFrameSize.y;
		}
		reset();
	}
	
	public void update(float t) {
		if (delay <= 0) return;
		time += t;
		while(time >= delay) {
			step();
		}
	}
	
	private void step() {
		time -= delay;

		fixtures[currentFrame].setSensor(true);
		
		currentFrame += forward ? 1 : -1;
		if (currentFrame == loopStart) {
			forward = true;
			currentFrame = loopStart;
		}
		
		if (currentFrame == loopEnd - 1) {
			forward = false;
			currentFrame = loopEnd - 1;
			timesPlayed++;
		}
		
		fixtures[currentFrame].setSensor(false);
	}
	
	public void reset() {
		time = 0;
		currentFrame = loopStart;
		timesPlayed = 0;
		forward = true;
	}
	
	public void setFaceDirection(Direction direction) {
		reset();
		loopStart = direction == Direction.RIGHT ? loopStart : frameCount / 2;
		loopEnd = direction == Direction.RIGHT ? frameCount / 2 : loopEnd;
		currentFrame = loopStart;
		timesPlayed = 0;
	}
	
	public void createAnimSensors(Vector2 center, Vector2 origin, Body body) {
		if (frames == null) return;

		AtlasRegion frame;
		float frameOffX, frameOffY, halfTexWidth, halfTexHeight;
		Vector2 frameCenter = new Vector2();
		fixtures = new Fixture[frames.length];
		
		float scaleX = (Gdx.graphics.getWidth() / 960f);
		float scaleY = (Gdx.graphics.getHeight() / 540f);

		for (int i = 0; i < frames.length; i++) {
			frame = frames[i];
			halfTexWidth = frame.getRegionWidth() / 2f * scaleX;
			halfTexHeight = frame.getRegionHeight() / 2f * scaleY;
			
			frameOffX = frame.offsetX * scaleX;
			frameOffY = 96 * scaleY - halfTexHeight * 2f - frame.offsetY * scaleY;
			
			frameCenter.set(origin.x * scaleX, origin.y * scaleY);
			frameCenter.add(frameOffX, frameOffY);
			frameCenter.add(halfTexWidth, halfTexHeight);
			frameCenter.sub(center.x * scaleX, center.y * scaleY);
			
			PolygonShape bodySensor = new PolygonShape();
			bodySensor.setAsBox(halfTexWidth / 100f, halfTexHeight / 100f, frameCenter.scl(.01f), 0);

			FixtureDef fixDef = new FixtureDef();
			fixDef.shape = bodySensor;
			fixDef.filter.categoryBits = Types.PLAYER;
			fixDef.filter.maskBits = Types.SOLID | Types.COLLECTABLE;
			fixDef.isSensor = true;
			
			Fixture fixture = body.createFixture(fixDef);
			fixture.setUserData(new EntityUserData("playerSensor_" + i));
			fixtures[i] = fixture;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public AtlasRegion[] getFrames() {
		return frames;
	}
	
	public AtlasRegion getFrame() {
		return frames[currentFrame];
	}
	
	public void setStartFrame(int frameNr) {
		startFrame = frameNr;
		frameCount = endFrame - startFrame;
	}
	
	public int getStartFrame() {
		return startFrame;
	}
	
	public void setEndFrame(int frameNr) {
		endFrame = frameNr;
		frameCount = endFrame - startFrame;
	}
	
	public int getEndFrame() {
		return endFrame;
	}
	
	public void setLoopStart(int frameNr) {
		loopStart = frameNr;
	}
	
	public int getLoopStart() {
		return loopStart;
	}
	
	public void setLoopEnd(int frameNr) {
		loopEnd = frameNr;
	}
	
	public int getLoopEnd() {
		return loopEnd;
	}
	
	public int getTimesPlayed() {
		return timesPlayed;
	}
	
	public int getFrameCount() {
		return endFrame - startFrame;
	}
	
	@Override
	public String toString() {
		String s = name + "(" + getFrameCount() + ")"  + ":\n  start    : " + startFrame + "\n  end      : " + endFrame;
		s += "\n  loopStart: " + loopStart + "\n  loopEnd  : " + loopEnd + "\n  forward  : " + forward;
		
		return s;
	}
}