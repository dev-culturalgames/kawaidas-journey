package de.kawaida.workshops.tza.config;

import com.badlogic.gdx.math.Vector2;

public class InputConfig {

	public static boolean[] keys;
	public static boolean[] pKeys;
	
	public static int NUM_KEYS = 7;
	public static int TOUCH = 0;
	public static int MOVE_LEFT_KEY = 1;
	public static int MOVE_RIGHT_KEY = 2;
	public static int JUMP_KEY = 3;
	public static int COLOR_KEY = 4;
	public static int ACKNOWLEDGE = 5;
	public static int DECLINE = 6;
	
	public static Vector2 touchPos = new Vector2();
	
	static {
		keys = new boolean[NUM_KEYS];
		pKeys = new boolean[NUM_KEYS];
	}
	
	public static void update() {
		for (int i = 0; i < NUM_KEYS; i++) {
			pKeys[i] = keys[i];
		}
	}
	
	public static void setKey(int i, boolean b) {
		keys[i] = b;
	}
	
	public static boolean isDown(int i) {
		return keys[i];
	}
	
	public static boolean isPressed(int i) {
		return keys[i] && !pKeys[i];
	}
	
	public static boolean isReleased(int i) {
		return !keys[i] && pKeys[i];
	}
}