package de.kawaida.workshops.tza.characters;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.workshops.tza.WorkshopGameTZA;
import de.kawaida.workshops.tza.config.GameCfg;
import de.kawaida.workshops.tza.config.Types;
import de.kawaida.workshops.tza.entity.EntityUserData;

public class Gem extends Collectable {
	
	public Gem(World world) {
		super(world);
		tex = new Texture("sprites/characters/collectable/apple.png");
		
		shape = new PolygonShape();
		((PolygonShape) shape).setAsBox(tex.getWidth() / 200f * GameCfg.SCALE.x,
										tex.getHeight() / 200f * GameCfg.SCALE.y);

		fixDef.shape = shape;
		fixDef.filter.categoryBits = Types.COLLECTABLE;
		fixDef.filter.maskBits = Types.SOLID | Types.PLAYER;
		fixDef.restitution = .75f;
		fixDef.isSensor = false;
		
		fixture = body.createFixture(fixDef);
		fixture.setUserData(new EntityUserData("gem"));
	}
	
	@Override
	public void update(float t) {
		if (body.getPosition().y < 0)
			WorkshopGameTZA.bodiesToRemove.add(body);
	}
}