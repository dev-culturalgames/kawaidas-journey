package de.kawaida.workshops.tza.controllers;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import de.kawaida.workshops.tza.WorkshopGameTZA;
import de.kawaida.workshops.tza.entity.EntityUserData;
import de.kawaida.workshops.tza.screens.IntroScreen;

public class CollisionControl implements ContactListener {
	
	private int playerOnGround = 0;
	private WorkshopGameTZA game;
	
	public CollisionControl(WorkshopGameTZA game) {
		this.game = game;
	}

	@Override
	public void beginContact(Contact contact) {
		Fixture player, collectable;
		Fixture fa = contact.getFixtureA();
		Fixture fb = contact.getFixtureB();
		EntityUserData faData = (EntityUserData) fa.getUserData();
		EntityUserData fbData = (EntityUserData) fb.getUserData();
		
		if (faData.name.contains("gem") && fbData.name.contains("ground")) {
			fa.setSensor(true);
			return;
		}
		
		if (fbData.name.contains("gem") && faData.name.contains("ground")) {
			fb.setSensor(true);
			return;
		}
		
		if (faData.name.contains("enemy") && fbData.name.contains("ground")) {
			fa.setSensor(true);
			return;
		}
		
		if (fbData.name.contains("enemy") && faData.name.contains("ground")) {
			fb.setSensor(true);
			return;
		}
		
		if (faData.name.contains("player")) {
			player = fa;
			collectable = fb;
		} else {
			player = fb;
			collectable = fa;
		}

		Body b = player.getBody();
		if (faData.name.contains("player") && fbData.name.contains("ground") ||
				fbData.name.contains("player") && faData.name.contains("ground")) {
			playerOnGround++;
//			b.setLinearVelocity(b.getLinearVelocity().x, 0);
			return;
		}

		EntityUserData cData = (EntityUserData) collectable.getUserData();
		
		if (cData.name.contains("gem") && !player.isSensor()) {
			WorkshopGameTZA.bodiesToRemove.add(collectable.getBody());
			game.player.score += 10;
		}
		
		if (cData.name.contains("enemy") && !player.isSensor()) {
			WorkshopGameTZA.bodiesToRemove.add(collectable.getBody());
			
			game.player.life -= 1;
			if (game.player.life <= 0) {
				IntroScreen.gameOver = true;
				game.player.getBody().setLinearVelocity(0f, 0f);
			}
		}
	}

	@Override
	public void endContact(Contact contact) {
		Fixture fa = contact.getFixtureA();
		Fixture fb = contact.getFixtureB();
		EntityUserData faData = (EntityUserData) fa.getUserData();
		EntityUserData fbData = (EntityUserData) fb.getUserData();

		
		if (faData.name.contains("playerFeet") && fbData.name.contains("ground") ||
				fbData.name.contains("playerFeet") && faData.name.contains("ground")) {
			playerOnGround--;
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		
	}
	
	public boolean isPlayerOnGround() {
		return playerOnGround > 0;
	}

}