package de.kawaida.workshops.tza.config;

public class Types {

	public static final short SOLID = 0x1;
	public static final short PLAYER = 0x2;
	public static final short COLLECTABLE = 0x4;
}